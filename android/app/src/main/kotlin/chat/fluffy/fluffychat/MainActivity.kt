package com.pageme.pageme
import io.flutter.plugin.common.MethodChannel
import android.net.Uri
import java.io.File
import androidx.core.content.FileProvider
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import com.pageme.pageme.BuildConfig
import android.content.Context
import androidx.multidex.MultiDex

class MainActivity : FlutterActivity() {
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun provideFlutterEngine(context: Context): FlutterEngine? {
        return provideEngine(this)
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        // do nothing, because the engine was been configured in provideEngine
        setupMethodChannel(flutterEngine)
    }

    private fun setupMethodChannel(flutterEngine: FlutterEngine) {
        val applicationId = when (BuildConfig.FLAVOR) {
            "pageme_business" -> "com.pageme.business"
            "pageme_public" -> "com.pageme.public"
            else -> BuildConfig.APPLICATION_ID
        }
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, "$applicationId/android_file_provider")
            .setMethodCallHandler { call, result ->
                if (call.method == "getUriForFile") {
                    val path = call.argument<String>("path")
                    val authority = call.argument<String>("authority")
                    if (path != null && authority != null) {
                        result.success(getUriForFile(this@MainActivity, path, authority).toString())
                    } else {
                        result.error("INVALID_ARGUMENTS", "Invalid or missing arguments", null)
                    }
                } else {
                    result.notImplemented()
                }
            }
    }

    private fun getUriForFile(context: Context, path: String, authority: String): Uri {
        val file = File(path)
        return FileProvider.getUriForFile(context, authority, file)
    }

    companion object {
        var engine: FlutterEngine? = null
        fun provideEngine(context: Context): FlutterEngine {
            val eng = engine ?: FlutterEngine(context, emptyArray(), true, false)
            engine = eng
            return eng
        }
    }
}


