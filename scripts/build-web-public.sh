#!/bin/sh -ve
flutter config --enable-web
flutter clean
flutter pub get

# Extract version from pubspec.yaml
VERSION=$(awk '/version:/ {print $2; exit}' pubspec.yaml)

flutter build web --release --verbose --source-maps --no-tree-shake-icons -t lib/main_public.dart
cp -R web/public/* build/web/
rm -r build/web/public/
rm -r build/web/business/

# Use the extracted version in the tar command
tar czf "pageme_public_${VERSION}.tar.gz" -C build/web/ .
