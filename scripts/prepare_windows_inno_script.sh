#!/bin/bash

# Extract app name, version, and publisher from pubspec.yaml
app_name=$(grep "name:" ../pubspec.yaml | awk '{print $2}')
version=$(grep "version:" ../pubspec.yaml | awk '{print $2}')
publisher=$(grep "publisher:" ../pubspec.yaml | awk '{print $2}')

# Replace app name, version, and publisher in the Inno Setup script
sed -i "s/{#MyAppName}/${app_name}/g" pageme_business_inno_script_test.iss
sed -i "s/{#MyAppVersion}/${version}/g" pageme_business_inno_script_test.iss
sed -i "s/{#MyAppPublisher}/${publisher}/g" pageme_business_inno_script_test.iss

# Collect all .dll files from build/windows/runner/Release and assets/windows folders
dll_files=""
for dll in ../build/windows/runner/Release/*.dll; do
  dll_files+="Source: \"$dll\"; DestDir: \"{app}\\$app_name\"; Flags: ignoreversion\n"
done

for dll in ../assets/windows/*.dll; do
  dll_files+="Source: \"$dll\"; DestDir: \"{app}\\$app_name\"; Flags: ignoreversion\n"
done

# Replace the files placeholder in the Inno Setup script
sed -i "/{#MyAppDLLFiles}/c\\$dll_files" pageme_business_inno_script_test.iss

# Run Inno Setup compiler
# Assuming Inno Setup compiler is available in the system PATH
# Replace "iscc" with the full path to the Inno Setup compiler if necessary
iscc pageme_business_inno_script_test.iss
