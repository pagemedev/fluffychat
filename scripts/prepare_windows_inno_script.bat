@echo off
setlocal enabledelayedexpansion

REM Extract app name, version, and publisher from pubspec.yaml
for /f "tokens=1,2" %%a in ('findstr /b /c:"name:" ..\pubspec.yaml') do set app_name=%%b
for /f "tokens=1,2" %%a in ('findstr /b /c:"version:" ..\pubspec.yaml') do set version=%%b
for /f "tokens=1,2" %%a in ('findstr /b /c:"publisher:" ..\pubspec.yaml') do set publisher=%%b

echo App name: %app_name%
echo Version: %version%
echo Publisher: %publisher%

REM Replace app name, version, and publisher in the Inno Setup script
type NUL > tmp.iss
for /f "usebackq delims=" %%a in (`type pageme_business_inno_script_test.iss`) do (
    set "line=%%a"
    set "line=!line:{#MyAppName}=%app_name%!"
    set "line=!line:{#MyAppVersion}=%version%!"
    set "line=!line:{#MyAppPublisher}=%publisher%!"
    echo !line! >> tmp.iss
)

move /Y tmp.iss pageme_business_inno_script_test.iss

REM Collect all .dll files from build/windows/runner/Release and assets/windows folders
echo. > dll_list.txt
for /r "..\build\windows\runner\Release" %%f in (*.dll) do echo Source: "%%f"; DestDir: "{app}\%app_name%"; Flags: ignoreversion^ >> dll_list.txt
for /r "..\assets\windows" %%f in (*.dll) do echo Source: "%%f"; DestDir: "{app}\%app_name%"; Flags: ignoreversion^ >> dll_list.txt

REM Replace the files placeholder in the Inno Setup script
type NUL > tmp.iss
for /f "usebackq delims=" %%a in (`type pageme_business_inno_script_test.iss`) do (
    set "line=%%a"
    if "!line!" == "{#MyAppDLLFiles}" (
        type dll_list.txt >> tmp.iss
    ) else (
        echo !line! >> tmp.iss
    )
)

move /Y tmp.iss pageme_business_inno_script_test.iss
del dll_list.txt

REM Run Inno Setup compiler
REM Assuming Inno Setup compiler is available in the system PATH
REM Replace "iscc" with the full path to the Inno Setup compiler if necessary
iscc pageme_business_inno_script_test.iss
