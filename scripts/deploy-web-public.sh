#!/bin/bash

rollback() {
    echo "An error occurred! Rolling back..."
    ssh $REMOTE_SERVER "cp -R $WEB_DOMAIN_BACKUP/* $WEB_DOMAIN/"
}

VERSION=$(awk '/version:/ {print $2; exit}' pubspec.yaml) || { echo "Version extraction failed"; exit 1; }
if [[ -z "$VERSION" ]]; then echo "Version extraction failed"; exit 1; fi

BACKUP_SUFFIX=$(date +%Y%m%d%H%M%S)
FILE_NAME="pageme_public_${VERSION}.tar.gz"
REMOTE_SERVER="public-webapp-pageme-prod002"
REMOTE_PATH="/web/nginx-proxy/data/sites-available"
WEB_DOMAIN="/web/nginx-proxy/data/web-domain"
WEB_DOMAIN_BACKUP="/web/nginx-proxy/data/web-domain-backup_$BACKUP_SUFFIX"

# Confirmation Prompt
read -p "Are you sure you want to deploy $FILE_NAME to $REMOTE_SERVER? [y/n]: " confirm
if [[ $confirm != 'y' ]]; then
  echo "Deployment aborted."
  exit 1
fi

# Debug: Show what will happen next
echo "Starting to deploy $FILE_NAME to $REMOTE_SERVER..."

# Create the remote directory if it doesn't exist
echo "Ensuring that the target directory exists on the remote server..."
ssh $REMOTE_SERVER "mkdir -p $REMOTE_PATH"

# Use scp to transfer the file
echo "Transferring file to remote server..."
scp $FILE_NAME $REMOTE_SERVER:$REMOTE_PATH || { echo "SCP failed"; exit 1; }

# Debug: Show what will happen next
echo "Creating backup on remote server."
ssh $REMOTE_SERVER "mkdir -p $WEB_DOMAIN_BACKUP"

# Copy the contents to the backup folder
echo "Backing up current web-domain..."
ssh $REMOTE_SERVER "cp -R $WEB_DOMAIN/* $WEB_DOMAIN_BACKUP" || { echo "Backup failed"; exit 1; }

# Only enable rollback after this point
trap rollback EXIT

ssh $REMOTE_SERVER << EOF
  set -e
  echo "Removing all files in $WEB_DOMAIN"
  rm -r $WEB_DOMAIN/*
  echo "Copying new tar.gz file to $WEB_DOMAIN"
  cp $REMOTE_PATH/$FILE_NAME $WEB_DOMAIN/
  echo "Untarring the file..."
  cd $WEB_DOMAIN
  tar -xvzf $FILE_NAME
EOF

echo "Disabling rollback. Deployment was successful."
# Disable rollback since everything went well
trap - EXIT

echo "Deployment successful"