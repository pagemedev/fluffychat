# Define the source and destination directories
$sourceDir = "assets\windows"
$destDir1 = "build\windows\runner\Debug"
$destDir2 = "build\windows\runner\Profile"
$destDir3 = "build\windows\runner\Release"

# Inform user that script has started
Write-Output "Script has started. Checking if destination directories exist..."

# Copy the contents of the source directory to the destination directories if they exist
if (Test-Path -path $destDir1) {
    Write-Output "Directory $destDir1 found. Beginning copy..."
    Get-ChildItem $sourceDir -Recurse | ForEach-Object {
        Copy-Item $_.FullName -Destination ($_.FullName.Replace($sourceDir, $destDir1))
    }
    Write-Output "Copying to $destDir1 completed."
} else {
    Write-Output "Directory $destDir1 does not exist. Skipping copy."
}

if (Test-Path -path $destDir2) {
    Write-Output "Directory $destDir2 found. Beginning copy..."
    Get-ChildItem $sourceDir -Recurse | ForEach-Object {
        Copy-Item $_.FullName -Destination ($_.FullName.Replace($sourceDir, $destDir2))
    }
    Write-Output "Copying to $destDir2 completed."
} else {
    Write-Output "Directory $destDir2 does not exist. Skipping copy."
}

if (Test-Path -path $destDir3) {
    Write-Output "Directory $destDir3 found. Beginning copy..."
    Get-ChildItem $sourceDir -Recurse | ForEach-Object {
        Copy-Item $_.FullName -Destination ($_.FullName.Replace($sourceDir, $destDir3))
    }
    Write-Output "Copying to $destDir3 completed."
} else {
    Write-Output "Directory $destDir3 does not exist. Skipping copy."
}

# Inform user that script has completed
Write-Output "Script has completed."
