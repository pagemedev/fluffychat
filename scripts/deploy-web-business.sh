#!/bin/bash

rollback() {
    echo "An error occurred! Rolling back..."
    ssh $REMOTE_SERVER "cp -R $WEB_DOMAIN_BACKUP/* $WEB_DOMAIN/"
}

VERSION=$(awk '/version:/ {print $2; exit}' pubspec.yaml) || { echo "Version extraction failed"; exit 1; }
if [[ -z "$VERSION" ]]; then echo "Version extraction failed"; exit 1; fi

FILE_NAME="pageme_business_${VERSION}.tar.gz"
REMOTE_SERVER="root@business-webapp-pageme-prod002"
WEB_DOMAIN="/web/nginx-proxy/data/web-domain"
BACKUP_SUFFIX=$(date +%Y%m%d%H%M%S)
WEB_DOMAIN_BACKUP="/web/nginx-proxy/data/web-domain-backup_$BACKUP_SUFFIX"
REMOTE_PATH="/web/nginx-proxy/data/sites-available"

scp $FILE_NAME $REMOTE_SERVER:$REMOTE_PATH || { echo "SCP failed"; exit 1; }

ssh $REMOTE_SERVER "mkdir -p $WEB_DOMAIN_BACKUP"

ssh $REMOTE_SERVER "cp -R $WEB_DOMAIN/* $WEB_DOMAIN_BACKUP" || { echo "Backup failed"; exit 1; }

# Only enable rollback after this point
trap rollback EXIT

ssh $REMOTE_SERVER << EOF
  set -e
  rm -r $WEB_DOMAIN/*
  cp $REMOTE_PATH/$FILE_NAME $WEB_DOMAIN/
  tar -xvzf $WEB_DOMAIN/$FILE_NAME
EOF

# Disable rollback since everything went well
trap - EXIT

echo "Deployment successful"
