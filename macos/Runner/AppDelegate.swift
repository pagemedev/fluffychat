import Cocoa
import FlutterMacOS

@NSApplicationMain
class AppDelegate: FlutterAppDelegate {
  override func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
    return false
  }

override func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
    print("applicationShouldHandleReopen called. hasVisibleWindows flag: \(flag)")  // Debug line

    if !flag {
        print("No visible windows. Looping through all windows.")  // Debug line

        for window in NSApp.windows {
            print("Current window: \(window.className), isVisible: \(window.isVisible)")  // Debug line

            if window.className == "NSColorPanel" {
                print("Skipping NSColorPanel.")
                continue
            }

            if !window.isVisible {
                print("Making window visible: \(window.className)")  // Debug line
                window.setIsVisible(true)
            }

            print("Making window key and ordering it to the front: \(window.className)")  // Debug line
            window.makeKeyAndOrderFront(self)
        }

        print("Activating the app, ignoring other apps.")  // Debug line
        NSApp.activate(ignoringOtherApps: true)
    }
    return true
}

}
