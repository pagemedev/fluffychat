import 'package:pageMe/pages/homeserver_picker/homeserver_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:pageMe/main_business.dart' as app_business;
import 'package:pageMe/main_public.dart' as app_public;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('Integration Test', () {
    testWidgets('Test if the app starts', (WidgetTester tester) async {
      app_public.main();
      await tester.pumpAndSettle();
      find.byWidgetPredicate((widget) => Widget is HomeserverPicker);
      await tester.pumpAndSettle();
    });
  });

  group('Integration Test', () {
    testWidgets('Test if the app starts', (WidgetTester tester) async {
      app_business.main();
      await tester.pumpAndSettle();
      find.byWidgetPredicate((widget) => Widget is HomeserverPicker);
      await tester.pumpAndSettle();
    });
  });
}
