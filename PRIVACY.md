# Privacy Policy

PageMe is available on Android and iOS. A Web version, as well as Desktop versions for Windows, Linux and macOS may follow.

*   [Information Collection and Use](#info-collection)
*   [Consent](#consent)
*   [Service Providers](#service-providers)
*   [Account Deregistration](#account-deregistration)
*   [Data Minimization](#data-minimization)
*   [Matrix](#1)
*   [Sentry](#2)
*   [Database](#3)
*   [Encryption](#4)
*   [App Permissions](#5)
*   [Push Notifications](#6)
*   [Stories](#7)
*   [Data Collection and Use](#8)
*   [User Rights](#9)

## Information Collection and Use<a id="info-collection"/>
PageMe collects data to provide, improve, protect, and promote our Services. Data collected is limited to what is necessary to achieve these purposes.

We collect information in a few different ways:

1. **When you give it to us or give us permission to obtain it.** For instance, when you create an account, you provide us with personal information. 

2. **We also get technical information when you use our Services.** Our servers receive and store information from your device, including your IP address, the type of device and software you use, and your location.

3. **Our third-party partners.** We may also get information about you from our third-party partners and combine it with information we already have about you.

We might use your information for the following:

1. **To provide, update, and maintain our Services.** This includes using your information to support you and to make our Services better for all of our users.

2. **To communicate with you.** For example, we may email you to tell you about changes or improvements to our Services.

3. **To protect PageMe and our users.** This includes actions designed to keep our Services secure and to prevent abuse and fraud.

4. **To promote and drive engagement with the PageMe Services.** For example, we may use your information to market our Services to you.

We will also respect your choices about how we use your information.

## Consent<a id="consent"/>
We request your consent before accessing or using your personal data, such as your email, User IDs, voice or sound recordings, and device or other IDs. We employ standard Android and Apple permissions requests for obtaining your consent. These requests are made at the launch of the app and before using any features that require access to sensitive information like your camera or gallery.

## Service Providers<a id="service-providers"/>
We do not share your data with third parties. Your personal information remains secure and private.

## Security<a id="security"/>
We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. We employ encryption technologies, like HTTPS, for transmitting your data securely. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.

## Account Deregistration<a id="account-deregistration"/>
To deregister or close an account, the authorized administrator of the account must send us a formal request via email. Upon receipt, we will process the deregistration, effectively closing the account and ceasing any data processing activities related to it.

## Data Minimization<a id="data-minimization"/>
We adhere to the principle of data minimization by using the matrix protocol, which only requires event IDs and room IDs to send information, minimizing data collection.

## Matrix<a id="1"/>
PageMe uses the Matrix protocol. This means that PageMe is just a client that can be connected to any compatible matrix server. The respective data protection agreement of the server selected by the user then applies.

For convenience, one or more servers are set as default that the PageMe developers consider trustworthy. The developers of PageMe do not guarantee their trustworthiness. Before the first communication, users are informed which server they are connecting to.

PageMe only communicates with the selected server, with sentry.io if enabled and with [OpenStreetMap](https://openstreetmap.org) to display maps.

More information is available at: [https://matrix.org](https://matrix.org)

## Sentry<a id="2"/>
PageMe uses Sentry for crash reports if the user allows it.

More information is available at: [https://sentry.io](https://sentry.io)

## Database<a id="3"/>
PageMe caches some data received from the server in a local database on the device of the user.

More information is available at: [https://pub.dev/packages/hive](https://pub.dev/packages/hive)

## Encryption<a id="4"/>
All communication of substantive content between PageMe and any server is done in secure way, using transport encryption to protect it.

PageMe is able to use End-To-End-Encryption as a tech preview.

## App Permissions<a id="5"/>

The permissions are the same on Android and iOS but may differ in the name. This are the Android Permissions:

#### Internet Access
PageMe needs to have internet access to communicate with the Matrix Server.

#### Vibrate
PageMe uses vibration for local notifications. More informations about this are at the used package:
[https://pub.dev/packages/flutter_local_notifications](https://pub.dev/packages/flutter_local_notifications)

#### Record Audio
PageMe can send voice messages in a chat and therefore needs to have the permission to record audio.

#### Write External Storage
The user is able to save received imagesState and therefore app needs this permission.

#### Read External Storage
The user is able to send imagesState from the device's file system.

#### Location
PageMe makes it possible to share the current location via the chat. When the user shares their location, PageMe uses the device location service and sends the geo-data via Matrix.

## Push Notifications<a id="6"/>
PageMe uses the Firebase Cloud Messaging service for push notifications on Android and iOS. This takes place in the following steps:
1. The matrix server sends the push notification to the PageMe Push Gateway (powered by Sygnal).
2. The PageMe Push Gateway forwards the message in a different format to Firebase Cloud Messaging
3. Firebase Cloud Messaging waits until the user's device is online again
4. The device receives the push notification from Firebase Cloud Messaging and displays it as a notification

PageMe uses the Apple Push Notification service (APNS) for push notifications on iOS. This takes place in the following steps:
1. The matrix server sends the push notification to the Sygnal Gateway
2. The Sygnal Gateway forwards the message in a different format to APNS
3. APNS waits until the user's device is online again
4. The device receives the push notification from APNS and displays it as a notification

The source code of the push gateway can be viewed here:
[https://github.com/matrix-org/sygnal](https://github.com/matrix-org/sygnal)

`event_id_only` is used as the format for the push notification. A typical push notification therefore only contains:
- Event ID
- Room ID
- Unread Count
- Information about the device that is to receive the message

A typical push notification could look like this:
```json
{
  "notification": {
    "event_id": "$3957tyerfgewrf384",
    "room_id": "!slw48wfj34rtnrf:example.com",
    "counts": {
      "unread": 2,
      "missed_calls": 1
    },
    "devices": [
      {
        "app_id": "com.pageme.pageme",
        "pushkey": "<Current Push Key>",
        "pushkey_ts": 12345678,
        "data": {},
        "tweaks": {
          "sound": "bing"
        }
      }
    ]
  }
}
```

PageMe sets the `event_id_only` flag at the Matrix Server. This server is then responsible to send the correct data.

## Stories<a id="7"/>

PageMe supports stories which is a feature similar to WhatsApp status or Instagram stories. However it is just a different GUI for the same room-related communication. More information about the feature can be found here:

https://github.com/krillefear/matrix-doc/blob/main/proposals/3588-stories-as-rooms.md

Stories are basically:

- End to end encrypted rooms
- Read-only rooms with only one admin who can post stuff (while there is no technical limitation to have multiple admins)

By default:

- The user has to invite all contacts manually to a story room
- The user can only invite contacts (matrix users the user shares a DM room with) to the story room
- The story room is created when the first story is posted
- User can mute and leave story rooms

The user is informed in the app that in theory all contacts can see each other in the story room. The user must give consent here. However the user is at any time able to create a group chat and invite all of their contacts to this chat in any matrix client which has the same result.


## User Rights<a id="9"/>

You have the right to:

- **Know what personal data we hold about you**, and to make sure it's correct and up to date.

- **Request a copy of your personal data**, or ask us to restrict processing your personal data or delete it.

- **Object to our continued processing of your personal data.**

You can exercise these rights at any time by sending an email to [support@pageme.com](mailto:support@pageme.com).

If you're not happy with how we are processing your personal data, please let us know by sending an email to [support@pageme.com](mailto:support@pageme.com). We will review and investigate your complaint, and try to get back to you within a reasonable time frame. You can also complain to your local data protection authority. They will be able to advise you how to submit a complaint.

