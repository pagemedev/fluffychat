# FluffyChat AppImage

FluffyChat is provided as AppImage too. To Download, visit fluffychat.im.

## Building

- Ensure you install `appimagetool`

```shell
flutter build linux

# copy binaries to appimage dir
cp -r build/linux/{x64,arm64}/release/bundle appimage/FluffyChat.AppDir
cd appimage

# prepare AppImage imagesState
cp FluffyChat.desktop FluffyChat.AppDir/
mkdir -p FluffyChat.AppDir/usr/share/icons
cp ../assets/logo.png FluffyChat.AppDir/usr/share/icons/fluffychat.png

# build the AppImage
appimagetool FluffyChat.AppDir
```
