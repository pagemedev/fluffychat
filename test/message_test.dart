import 'package:flutter_test/flutter_test.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/chat/message/message.dart';

import 'utils/test_client.dart';

void main() {
  group('MessageState', () {
    late MessageState messageState;
    late Event event;
    late Event nextEvent;

    setUp(() async {
      messageState = MessageState();
      event = Event(
        eventId: 'event1',
        content: {'msgtype': 'm.text', 'body': 'Hello'},
        senderId: 'user1',
        type: 'm.room.message',
        room: Room(id: 'room1', client: await prepareTestClient()),
        status: EventStatus.sent,
        originServerTs: DateTime.now(),
      );
      nextEvent = Event(
        eventId: 'event2',
        content: {'msgtype': 'm.text', 'body': 'Hello'},
        senderId: 'user1',
        type: 'm.room.message',
        room: Room(id: 'room1', client: await prepareTestClient()),
        status: EventStatus.sent,
        originServerTs: DateTime.now().add(const Duration(minutes: 5)),
      );
    });

    test('isCommonEventType returns true for common event types', () {
      expect(messageState.isSupportedEventType('m.room.message'), isTrue);
      expect(messageState.isSupportedEventType('m.sticker'), isTrue);
      expect(messageState.isSupportedEventType('m.room.encrypted'), isTrue);
      expect(messageState.isSupportedEventType('m.call.invite'), isTrue);
    });

    test('isCommonEventType returns false for uncommon event types', () {
      expect(messageState.isSupportedEventType('m.room.member'), isFalse);
    });

/*
    test('isVerificationRequest returns true for verification request events', () {
      event.type = 'm.room.message';
      event.messageType = 'm.key.verification.request';
      expect(messageState.isVerificationRequest(event), isTrue);
    });
*/

    test('isVerificationRequest returns false for non-verification request events', () {
      expect(messageState.isVerificationRequest(event), isFalse);
    });

    test('shouldDisplayTime returns true when next event is null', () {
      expect(messageState.shouldDisplayTime(event, null), isTrue);
    });

    test('shouldDisplayTime returns false when events are on the same date', () {
      expect(messageState.shouldDisplayTime(event, nextEvent), isFalse);
    });

    test('isSameSender returns true when events have the same sender and displayTime is false', () {
      expect(messageState.isSameSender(event, nextEvent, false), isTrue);
    });

    test('isSameSender returns false when events have different senders', () {
      nextEvent.senderId = 'user2';
      expect(messageState.isSameSender(event, nextEvent, false), isFalse);
    });

    test('isSameSender returns false when displayTime is true', () {
      expect(messageState.isSameSender(event, nextEvent, true), isFalse);
    });

/*    test('isMediaMessage returns true for media messages', () {
      event.messageType = 'm.image';
      expect(messageState.isMediaMessage(event), isTrue);
    });*/

    test('isMediaMessage returns false for non-media messages', () {
      expect(messageState.isMediaMessage(event), isFalse);
    });
  });
}