//import 'package:pageMe/pages/homeserver_picker.dart';
//import 'package:pageMe/main.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/pages/homeserver_picker/homeserver_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'utils/test_client.dart';

//import 'utils/test_client.dart';

void main() {
  testWidgets('Test if the widget can be created', (WidgetTester tester) async {
    await tester.pumpWidget(
      PageMeApp(
        clients: [await prepareTestClient()],
        testWidget: const HomeserverPicker(),
        store: await SharedPreferences.getInstance(),
      ),
    );
  });
}
