import 'package:flutter/material.dart';
import 'package:pageMe/config/flavor_config.dart';

class EmptyPage extends StatelessWidget {
  const EmptyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
              child: isBusiness()
                  ? Image.asset(
                      'assets/pageme_business_logo.png',
                      width: 100,
                      height: 100,
                    )
                  : Image.asset('assets/pageme_public_logo.png', width: 100, height: 100),
            ),
            const SizedBox(
              height: 30,
            ),
            Text(
              'Welcome to ${FlavorConfig.applicationName}',
              style: const TextStyle(fontSize: 45, fontWeight: FontWeight.bold),
            ),
            const Text(
              "Don't be a product, be protected",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
            ),
            const Row(
              children: [],
            )
          ],
        ),
      ),
    );
  }
}
