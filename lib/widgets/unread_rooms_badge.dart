import 'package:flutter/material.dart';

import 'package:badges/badges.dart' as b;
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/themes.dart';

import 'matrix.dart';

class UnreadRoomsBadge extends StatelessWidget {
  final bool Function(Room) filter;
  final b.BadgePosition? badgePosition;
  final Widget? child;

  const UnreadRoomsBadge({
    Key? key,
    required this.filter,
    this.badgePosition,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Matrix.of(context).client.onSync.stream.where((syncUpdate) => syncUpdate.hasRoomUpdate),
        builder: (context, _) {
          final unreadFilteredRooms = Matrix.of(context).client.rooms.where(filter).where((r) => (r.isUnread || r.membership == Membership.invite));
          if (unreadFilteredRooms.isNotEmpty){
            Logs().i("Unread filtered rooms: ${unreadFilteredRooms.first.id}, name: ${unreadFilteredRooms.first.displayname}, ");
          }
          final unreadCount = unreadFilteredRooms.length;
          return b.Badge(
            alignment: Alignment.bottomRight,
            badgeContent: Text(
              unreadCount.toString(),
              style: TextStyle(
                color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onTertiary,
                fontSize: 12,
              ),
            ),
            showBadge: unreadCount != 0,
            animationType: b.BadgeAnimationType.scale,
            badgeColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.tertiary,
            position: badgePosition,
            elevation: 4,
            child: child,
          );
        });
  }
}
