import 'dart:async';

import 'package:flutter/material.dart';

import 'package:matrix/matrix.dart';
import 'package:pageMe/widgets/avatar.dart';


class FileUploadHeader extends StatefulWidget {
  final Stream<FileUploadUpdate> statusUpdates;
  const FileUploadHeader({Key? key, required this.statusUpdates}) : super(key: key);

  @override
  FileUploadHeaderState createState() => FileUploadHeaderState();
}

class FileUploadHeaderState extends State<FileUploadHeader> {

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<FileUploadUpdate>(
      stream: widget.statusUpdates,
      builder: (context, snapshot) {
        final bool isUploading = snapshot.hasData && snapshot.data?.status != "Upload Complete";
        return AnimatedContainer(
            duration: const Duration(milliseconds: 1000),
            curve: Curves.linear,
            height: isUploading ? 36 : 0,
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(color: Theme.of(context).secondaryHeaderColor),
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 21),
                  child: Avatar(
                    mxContent: snapshot.data?.currentParticipant.avatar,
                    size: 30,
                    name: snapshot.data?.currentParticipant.getLocalizedDisplayname(),
                  ),
                ),
                Text(
                  '${snapshot.data?.status} (${((snapshot.data?.participantIndex ?? 1) - 1) * (snapshot.data?.totalFiles ?? 1) + (snapshot.data?.fileIndex ?? 1)}/${((snapshot.data?.totalParticipant ?? 1) * (snapshot.data?.totalFiles ?? 1))})',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(
                  width: 5,
                ),
                SizedBox(
                  height: 20,
                  width: 20,
                  child: CircularProgressIndicator(
                    strokeWidth: 2,
                    value: ((snapshot.data?.participantIndex ?? 1) - 1) * (snapshot.data?.totalFiles ?? 1) +
                        (snapshot.data?.fileIndex ?? 1) / ((snapshot.data?.totalParticipant ?? 1) * (snapshot.data?.totalFiles ?? 1)),
                  ),
                ),
                const Spacer(),
                Row(
                  children: [
                    Text(
                      '${snapshot.data?.participantIndex}/${snapshot.data?.totalParticipant}',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    const Icon(
                      Icons.people_alt_outlined,
                      size: 16,
                    ),
                  ],
                )
              ],
            ));
      },
    );
  }
}

class FileUploadUpdate {
  final MatrixFile file;
  final String status;
  final int fileIndex;
  final int totalFiles;
  final int participantIndex;
  final int totalParticipant;
  final Room currentParticipant;

  FileUploadUpdate({
    required this.file,
    required this.status,
    required this.fileIndex,
    required this.totalFiles,
    required this.participantIndex,
    required this.totalParticipant,
    required this.currentParticipant,
  });
}
