import 'package:flutter/material.dart';

import 'avatar.dart';

class FakeChatListItem extends StatelessWidget {
  final String title;
  final String subtitle;
  final String avatarAssetPath;
  const FakeChatListItem({super.key, required this.title, required this.subtitle, required this.avatarAssetPath});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Stack(
        children: [
          ListTile(
            titleAlignment: ListTileTitleAlignment.titleHeight,
              isThreeLine: true,
              leading: SizedBox.square(
                dimension: Avatar.defaultSize,
                child: Material(
                  clipBehavior: Clip.hardEdge,
                  borderRadius: BorderRadius.circular(Avatar.defaultSize / 4),
                  child: Image.asset(
                    avatarAssetPath,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              title: Row(
                children: [
                  Expanded(
                    child: Text(
                      title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onBackground,
                      ),
                    ),
                  ),
                ],
              ),
              subtitle: Text(
                subtitle,
                softWrap: false,
                maxLines: 10,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onBackground,
                ),
              )),
        ],
      ),
    );
  }
}

class FakeChatListItemLoading extends StatelessWidget {
  const FakeChatListItemLoading({
    super.key,
    required this.titleColor,
    required this.context,
    required this.subtitleColor,
  });

  final Color titleColor;
  final BuildContext context;
  final Color subtitleColor;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: titleColor,
        child: CircularProgressIndicator(
          strokeWidth: 1,
          color: Theme.of(context).textTheme.bodyLarge!.color,
        ),
      ),
      title: Row(
        children: [
          Expanded(
            child: Container(
              height: 14,
              decoration: BoxDecoration(
                color: titleColor,
                borderRadius: BorderRadius.circular(3),
              ),
            ),
          ),
          const SizedBox(width: 36),
          Container(
            height: 14,
            width: 14,
            decoration: BoxDecoration(
              color: subtitleColor,
              borderRadius: BorderRadius.circular(14),
            ),
          ),
          const SizedBox(width: 12),
          Container(
            height: 14,
            width: 14,
            decoration: BoxDecoration(
              color: subtitleColor,
              borderRadius: BorderRadius.circular(14),
            ),
          ),
        ],
      ),
      subtitle: Container(
        decoration: BoxDecoration(
          color: subtitleColor,
          borderRadius: BorderRadius.circular(3),
        ),
        height: 12,
        margin: const EdgeInsets.only(right: 22),
      ),
    );
  }
}

