import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_file_extension.dart';

import 'matrix.dart';

class MxcImageFetch {
  static final Map<String, Uint8List> _imageDataCache = {};

  static Future<Uint8List?> getData({
    required Client client,
    Uri? uri,
    Event? event,
    double? width,
    double? height,
    bool isThumbnail = false,
    bool animated =false,
    ThumbnailMethod thumbnailMethod = ThumbnailMethod.scale,
  }) async {
    bool? isCached;
    if (uri != null) {
      const devicePixelRatio = 3;
      final realWidth = width == null ? null : width * devicePixelRatio;
      final realHeight = height == null ? null : height * devicePixelRatio;

      final httpUri = isThumbnail
          ? uri.getThumbnail(
              client,
              width: realWidth,
              height: realHeight,
              animated: animated,
              method: thumbnailMethod,
            )
          : uri.getDownloadLink(client);

      final Uri storeKey = isThumbnail ? httpUri : uri;

      if (isCached == null) {

        final cachedData = await client.database?.getFile(storeKey);
        if (cachedData != null) {
          isCached = true;
          return cachedData;
        }
        isCached = false;
      }

      final response = await http.get(httpUri);
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          return null;
        }
        throw Exception();
      }
      final remoteData = response.bodyBytes;
      await client.database?.storeFile(storeKey, remoteData, 0);

      return remoteData;
    }

    if (event != null) {
      final data = await event.downloadAndDecryptAttachment(
        getThumbnail: isThumbnail,
      );
      if (data.detectFileType is MatrixImageFile) {
        return data.bytes;
      }
    }
    return null;
  }
}
