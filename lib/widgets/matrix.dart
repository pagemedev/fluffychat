import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:desktop_notifications/desktop_notifications.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_hms_gms_availability/flutter_hms_gms_availability.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:matrix/encryption.dart';
import 'package:matrix/matrix.dart';
import 'package:open_filex/open_filex.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/pages/subscriptions/subscriptions_cubit.dart';
import 'package:pageMe/utils/client_manager.dart';
import 'package:pageMe/utils/localized_exception_extension.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/push_factorys/android_push_factory.dart';
import 'package:pageMe/utils/push_factorys/ios_push_factory.dart';
import 'package:pageMe/utils/uia_request_manager.dart';
import 'package:pageMe/utils/voip_plugin.dart';
import 'package:pageme_client/pageme_client.dart' as pageme;
import 'package:path/path.dart' as p;
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:serverpod_flutter/serverpod_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_html/html.dart' as html;
import 'package:url_launcher/url_launcher.dart';
import 'package:win_toast/win_toast.dart';
import 'package:window_manager/window_manager.dart';

import '../config/setting_keys.dart';
import '../pages/key_verification/key_verification_dialog.dart';
import '../utils/account_bundles.dart';
import '../utils/famedlysdk_store.dart';
import '../utils/logger_functions.dart';
import '../utils/matrix_sdk_extensions.dart/matrix_file_extension.dart';
import '../utils/push_factorys/huawei_push_factory.dart';
import 'local_notifications_extension.dart';

class Matrix extends StatefulWidget {
  final Widget? child;

  final List<Client> clients;
  final SharedPreferences store;

  final Map<String, String>? queryParameters;

  const Matrix({
    this.child,
    required this.store,
    required this.clients,
    this.queryParameters,
    Key? key,
  }) : super(key: key);

  @override
  MatrixState createState() => MatrixState();

  /// Returns the (nearest) Client instance of your application.
  static MatrixState of(BuildContext context) => Provider.of<MatrixState>(context, listen: false);
  static MatrixState watch(BuildContext context) => Provider.of<MatrixState>(context, listen: true);
}

class MatrixState extends State<Matrix> with WidgetsBindingObserver {
  int _activeClient = -1;

  String? activeBundle;
  late BuildContext navigatorContext;
  bool initializedWinToast = false;
  HomeserverSummary? loginHomeServerSummary;
  pageme.HomeServer? homeServer;
  XFile? loginAvatar;
  String? loginUsername;
  bool? loginRegistrationSupported;
  late final pageme.Client pagemeClient;
  SubscriptionsCubit? subscriptionsCubit;
  late ThemeData theme;
  late L10n? l10n;
  dynamic _backgroundPush;

  SharedPreferences get store => widget.store;

  Client get client {
    if (widget.clients.isEmpty) {
      widget.clients.add(getLoginClient());
    }
    if (_activeClient < 0 || _activeClient >= widget.clients.length) {
      return currentBundle!.first!;
    }
    return widget.clients[_activeClient];
  }

  String? get activeRoomId {
    final route = PageMeApp.router.routeInformationProvider.value.uri.path;
    if (!route.startsWith('/rooms/')) return null;
    return route.split('/')[2];
  }

  bool get webrtcIsSupported => kIsWeb || PlatformInfos.isMobile || PlatformInfos.isWindows || PlatformInfos.isMacOS;

  VoipPlugin? voipPlugin;

  bool get isMultiAccount => widget.clients.length > 1;

  int getClientIndexByMatrixId(String matrixId) => widget.clients.indexWhere((client) => client.userID == matrixId);

  late String currentClientSecret;
  RequestTokenResponse? currentThreepidCreds;

  void setActiveClient(Client? cl) {
    final i = widget.clients.indexWhere((c) => c == cl);
    if (i != -1) {
      _activeClient = i;
      // TODO: Multi-client VoiP support
      createVoipPlugin();
    } else {
      Logs().w('Tried to set an unknown client ${cl!.userID} as active');
    }
  }

  List<Client?>? get currentBundle {
    if (!hasComplexBundles) {
      return List.from(widget.clients);
    }
    final bundles = accountBundles;
    if (bundles.containsKey(activeBundle)) {
      return bundles[activeBundle];
    }
    return bundles.values.first;
  }

  Map<String?, List<Client?>> get accountBundles {
    final resBundles = <String?, List<_AccountBundleWithClient>>{};
    for (var i = 0; i < widget.clients.length; i++) {
      final bundles = widget.clients[i].accountBundles;
      for (final bundle in bundles) {
        if (bundle.name == null) {
          continue;
        }
        resBundles[bundle.name] ??= [];
        resBundles[bundle.name]!.add(_AccountBundleWithClient(
          client: widget.clients[i],
          bundle: bundle,
        ));
      }
    }
    for (final b in resBundles.values) {
      b.sort((a, b) => a.bundle!.priority == null
          ? 1
          : b.bundle!.priority == null
              ? -1
              : a.bundle!.priority!.compareTo(b.bundle!.priority!));
    }
    return resBundles.map((k, v) => MapEntry(k, v.map((vv) => vv.client).toList()));
  }

  bool get hasComplexBundles => accountBundles.values.any((v) => v.length > 1);

  Client? _loginClientCandidate;

  Client getLoginClient() {
    if (widget.clients.isNotEmpty && !client.isLogged()) {
      return client;
    }

    final candidate = _loginClientCandidate ??= ClientManager.createClient('${FlavorConfig.applicationName}-${DateTime.now().millisecondsSinceEpoch}')
      ..onLoginStateChanged.stream.where((l) => l == LoginState.loggedIn).first.then((_) async {
        if (!widget.clients.contains(_loginClientCandidate)) {
          widget.clients.add(_loginClientCandidate!);
        }
        await ClientManager.addClientNameToStore(_loginClientCandidate!.clientName, store);
        _registerSubs(_loginClientCandidate!.clientName);
        _loginClientCandidate = null;
        _loginClientCandidate = null;
        PageMeApp.router.go('/rooms');
      });
    return candidate;
  }

  Future<void> checkFirstTimeUser() async {
    if (client.userID != null) firstTimeUser = await Store().getItemBool(SettingKeys.firstTimeUser + client.userID!, true);
  }

  Client? getClientByName(String name) => widget.clients.firstWhereOrNull((c) => c.clientName == name);

  List<Map<String, dynamic>?>? get shareContent => _shareContent;

  set shareContent(List<Map<String, dynamic>?>? content) {
    _shareContent = content;
    onShareContentChanged.add(_shareContent);
  }

  List<Map<String, dynamic>?>? _shareContent = [];

  final StreamController<List<Map<String, dynamic>?>?> onShareContentChanged = StreamController.broadcast();

  File? wallpaper;

  void _initWithStore() async {
    try {
      if (client.isLogged()) {
        // TODO: Figure out how this works in multi account
        final statusMsg = store.getString(SettingKeys.ownStatusMessage);
        if (statusMsg?.isNotEmpty ?? false) {
          Logs().v('Send cached status message: "$statusMsg"');
          await client.setPresence(
            client.userID!,
            PresenceType.online,
            statusMsg: statusMsg,
          );
        }
      }
    } catch (e, s) {
      client.onLoginStateChanged.addError(e, s);
      rethrow;
    }
  }

  final onRoomKeyRequestSub = <String, StreamSubscription>{};
  final onKeyVerificationRequestSub = <String, StreamSubscription>{};
  final onNotification = <String, StreamSubscription>{};
  final onLoginStateChanged = <String, StreamSubscription<LoginState>>{};
  final onUiaRequest = <String, StreamSubscription<UiaRequest>>{};
  StreamSubscription<html.Event>? onFocusSub;
  StreamSubscription<html.Event>? onBlurSub;

  String? _cachedPassword;
  Timer? _cachedPasswordClearTimer;

  String? get cachedPassword => _cachedPassword;

  set cachedPassword(String? p) {
    Logs().d('Password cached');
    _cachedPasswordClearTimer?.cancel();
    _cachedPassword = p;
    _cachedPasswordClearTimer = Timer(const Duration(minutes: 10), () {
      _cachedPassword = null;
      Logs().d('Cached Password cleared');
    });
  }

  bool webHasFocus = true;

  final windowsNotifications = PlatformInfos.isWindows ? WinToast.instance() : null;
  final linuxNotifications = PlatformInfos.isLinux ? NotificationsClient() : null;
  final Map<String, int> linuxNotificationIds = {};

  void initialisePageMeServer() async {
    pagemeClient = pageme.Client(
      kDebugMode ? FlavorConfig.serverpodEndpoint : FlavorConfig.serverpodEndpoint,
    )..connectivityMonitor = FlutterConnectivityMonitor();
  }

  String getInstallationPath() {
    final scriptUrl = Platform.script;
    return p.dirname(scriptUrl.toFilePath(windows: true));
  }

  void handleOpenFileNotificationAction(String? downloadPath) async {
    if (downloadPath == null) {
      return;
    }
    Logs().v('Payload is $downloadPath');
    final OpenResult result;
    try {
      if (await Permission.manageExternalStorage.isGranted) {
        result = await OpenFilex.open(downloadPath);
        Logs().v('opening file ${result.message}, ${result.type.name}');
      } else {
        await Permission.manageExternalStorage.request();
        result = await OpenFilex.open(downloadPath);
        Logs().v('opening file ${result.message}, ${result.type.name}');
      }
    } on Exception catch (e) {
      Logs().v('Error opening file from background notification: $e');
    }
  }

  void handleOpenFileLocationNotificationAction(String? downloadPath) async {
    if (downloadPath == null) {
      return;
    }
    Logs().v('Payload is $downloadPath');
    final OpenResult result;
    try {
      if (await Permission.manageExternalStorage.isGranted) {
        result = await OpenFilex.open(p.dirname(downloadPath));
        Logs().v('opening file ${result.message}, ${result.type.name}');
      } else {
        await Permission.manageExternalStorage.request();
        result = await OpenFilex.open(p.dirname(downloadPath));
        Logs().v('opening file ${result.message}, ${result.type.name}');
      }
    } on Exception catch (e) {
      Logs().v('Error opening file from background notification: $e');
    }
  }

  Future<void> _initializeWinToast() async {
    Logs().i('[Windows Notifications: Initializing]');
    //final installationPath = getInstallationPath();
    //final iconPath = p.join(installationPath, 'assets\\pageme_business_logo.png');
    //Logs().i( iconPath);
    final ret = await windowsNotifications!.initialize(
      aumId: '${FlavorConfig.windowsPackageFamilyName}!pageMe',
      displayName: FlavorConfig.applicationName,
      iconPath: '', // Optional: Path to your app's icon
      clsid: FlavorConfig.applicationGUID,
    );
    setState(() {
      initializedWinToast = ret;
    });

    windowsNotifications!.setActivatedCallback((notification) async {
      final Map<String, dynamic> args = jsonDecode(notification.argument);
      switch (args['action']) {
        case 'openChat':
          final isVisible = await windowManager.isVisible();
          final isFocused = await windowManager.isFocused();
          if (!isVisible) {
            await windowManager.show();
          }
          if (!isFocused) {
            await windowManager.focus();
          }
          navigatorContext.go('/rooms/${args['roomId']}');
          break;
        case 'openFile':
          Logs().v(args.toString());
          handleOpenFileNotificationAction(args['downloadPath']);
          break;
        case 'openFileLocation':
          handleOpenFileLocationNotificationAction(args['downloadPath']);
          break;
        case 'markAsRead':
          final room = client.getRoomById(args['roomId']);
          if (room != null) {
            final event = Event.fromJson(args['event'], room);
            await room.setReadMarker(event.eventId, mRead: event.eventId);
          }
          break;
      }
    });
    Logs().i('[Windows Notifications: Initialized result: $ret]');
  }

  void initLoadingDialog() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      LoadingDialog.defaultTitle = l10n!.loadingPleaseWait;
      LoadingDialog.defaultBackLabel = l10n!.close;
      LoadingDialog.defaultOnError = (e) => (e as Object?)!.toLocalizedString(context);
    });
  }

  Future<void> initConfig() async {
    try {
      final configJsonString = utf8.decode((await http.get(Uri.parse('config.json'))).bodyBytes);
      final configJson = json.decode(configJsonString);
      FlavorConfig.loadFromJson(configJson);
    } on FormatException catch (_) {
      Logs().v('[ConfigLoader] config.json not found');
    } catch (e) {
      Logs().v('[ConfigLoader] config.json not found', e);
    }
  }

  void _registerSubs(String name) async {
    final c = getClientByName(name);
    if (c == null) {
      Logs().w('Attempted to register subscriptions for non-existing client $name');
      return;
    }
    onRoomKeyRequestSub[name] ??= c.onRoomKeyRequest.stream.listen((RoomKeyRequest request) async {
      if (widget.clients.any(((cl) => cl.userID == request.requestingDevice.userId && cl.identityKey == request.requestingDevice.curve25519Key))) {
        Logs().i('[Key Request] Request is from one of our own clients, forwarding the key...');
        await request.forwardKey();
      }
    });
    onKeyVerificationRequestSub[name] ??= c.onKeyVerificationRequest.stream.listen((KeyVerification request) async {
      var hidPopup = false;
      request.onUpdate = () {
        if (!hidPopup && {KeyVerificationState.done, KeyVerificationState.error}.contains(request.state)) {
          Navigator.of(navigatorContext).pop('dialog');
        }
        hidPopup = true;
      };
      request.onUpdate = null;
      hidPopup = true;
      await KeyVerificationDialog(request: request).show(navigatorContext);
    });
    onLoginStateChanged[name] ??= c.onLoginStateChanged.stream.listen((state) async {
      loggerWarning(logMessage: "Inside Matrix, onLoginStateChanged: ${state.toString()}");
      final loggedInWithMultipleClients = widget.clients.length > 1;
      if (loggedInWithMultipleClients && state != LoginState.loggedIn) {
        await _cancelSubs(c.clientName);
        widget.clients.remove(c);
        await ClientManager.removeClientNameFromStore(c.clientName, store);
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(l10n!.oneClientLoggedOut),
          ),
        );

        if (state != LoginState.loggedIn) {
          loggerWarning(logMessage: "Inside Matrix not logged in section, onLoginStateChanged: ${state.toString()}");
          PageMeApp.router.go(Uri(path: '/rooms', queryParameters: PageMeApp.router.routeInformationProvider.value.uri.queryParameters).toString());
        }
      } else {
        await checkFirstTimeUser();
        if (mounted) {
          if (isBusiness()) {
            _handleBusinessNavigation(state);
          } else {
            if (PlatformInfos.canSubscribe) {
              await _handleSubscriptionsNavigation(state);
            } else {
              _handlePublicNavigation(state);
            }
          }
        }
      }
    });
    onUiaRequest[name] ??= c.onUiaRequest.stream.listen(uiaRequestHandler);
    Logs().i("Inside Matrix, onUiaRequest");
    if (PlatformInfos.isWeb || PlatformInfos.isLinux || Platform.isWindows || PlatformInfos.isMacOS) {
      await c.onSync.stream.first.then((s) {
        html.Notification.requestPermission();
        onNotification[name] ??= c.onEvent.stream
            .where((e) => e.type == EventUpdateType.timeline && [EventTypes.Message, EventTypes.Sticker, EventTypes.Encrypted].contains(e.content['type']) && e.content['sender'] != c.userID)
            .listen(showLocalEventNotification);
      });
    }
  }

  void _handleBusinessNavigation(LoginState state) {
    PageMeApp.router.go(Uri(
            path: state == LoginState.loggedIn
                ? firstTimeUser
                    ? '/onboard'
                    : '/rooms'
                : '/server_select',
            queryParameters: PageMeApp.router.routeInformationProvider.value.uri.queryParameters)
        .toString());
  }

  void _handlePublicNavigation(LoginState state) {
    PageMeApp.router.go(Uri(
            path: state == LoginState.loggedIn
                ? firstTimeUser
                    ? '/onboard'
                    : '/rooms'
                : '/login',
            queryParameters: PageMeApp.router.routeInformationProvider.value.uri.queryParameters)
        .toString());
  }

  Future<void> _handleSubscriptionsNavigation(LoginState state) async {
    subscriptionsCubit ??= BlocProvider.of<SubscriptionsCubit>(context);
    bool? isSubscribed;
    if (state == LoginState.loggedIn) {
      await subscriptionsCubit!.login(appUserId: client.userID!).whenComplete(() => isSubscribed = subscriptionsCubit!.state.isSubscribed);
    } else {
      await subscriptionsCubit!.logout();
    }

    Logs().i("isLoggedIn : $state\nisSubscribed: ${subscriptionsCubit!.state.isSubscribed}\nisFirstTimeUser: $firstTimeUser");

    PageMeApp.router.go(Uri(
            path: state == LoginState.loggedIn
                ? isSubscribed ?? false
                    ? firstTimeUser
                        ? '/onboard'
                        : '/rooms'
                    : '/subscriptions'
                : '/login',
            queryParameters: PageMeApp.router.routeInformationProvider.value.uri.queryParameters)
        .toString());
  }

  Future<void> _cancelSubs(String name) async {
    await onRoomKeyRequestSub[name]?.cancel();
    onRoomKeyRequestSub.remove(name);
    await onKeyVerificationRequestSub[name]?.cancel();
    onKeyVerificationRequestSub.remove(name);
    await onLoginStateChanged[name]?.cancel();
    onLoginStateChanged.remove(name);
    await onNotification[name]?.cancel();
    onNotification.remove(name);
  }

  void initMatrix() async {
    // Display the app lock
    if (PlatformInfos.isMobile) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        if (PlatformInfos.isLinux) {
          await SharedPreferences.getInstance().then((prefs) => prefs.getString(SettingKeys.appLockKey)).then((lock) async {
            if (lock?.isNotEmpty ?? false) {
              AppLock.of(context)!.enable();
              await AppLock.of(context)!.showLockScreen();
            }
          });
        } else {
          await const FlutterSecureStorage().read(key: SettingKeys.appLockKey).then((lock) async {
            if (lock?.isNotEmpty ?? false) {
              AppLock.of(context)!.enable();
              await AppLock.of(context)!.showLockScreen();
            }
          });
        }
      });
    }

    _initWithStore();

    for (final c in widget.clients) {
      _registerSubs(c.clientName);
    }

    if (kIsWeb) {
      onFocusSub = html.window.onFocus.listen((_) => webHasFocus = true);
      onBlurSub = html.window.onBlur.listen((_) => webHasFocus = false);
    }

    if (PlatformInfos.isMobile) {
      //PushSetup().pushSetup(client, context, navigatorContext, widget.router);
      await FlutterHmsGmsAvailability.isHmsAvailable.then((t) async {
        Logs().i("Huawei Services: $t");
        if (t) {
          _backgroundPush = HuaweiPushFactory(
            client,
            this,
            onPushError: (errorMsg, {Uri? link}) => Timer(
              const Duration(seconds: 1),
              () {
                final banner = SnackBar(
                  content: Text(errorMsg),
                  duration: const Duration(seconds: 30),
                  action: link == null
                      ? null
                      : SnackBarAction(
                          label: l10n!.link,
                          onPressed: () => launchUrl(Uri.parse(link.toString())),
                        ),
                );
                ScaffoldMessenger.of(navigatorContext).showSnackBar(banner);
              },
            ),
          );
        } else {
          if (PlatformInfos.isIOS) {
            _backgroundPush = IOSPushFactory(
              client,
              this,
              onPushError: (errorMsg, {Uri? link}) => Timer(
                const Duration(seconds: 1),
                () {
                  final banner = SnackBar(
                    content: Text(errorMsg),
                    duration: const Duration(seconds: 30),
                    action: link == null
                        ? null
                        : SnackBarAction(
                            label: l10n!.link,
                            onPressed: () => launchUrl(Uri.parse(link.toString())),
                          ),
                  );
                  ScaffoldMessenger.of(navigatorContext).showSnackBar(banner);
                },
              ),
            );
          } else {
            _backgroundPush = AndroidPushFactory(
              client,
              this,
              onPushError: (errorMsg, {Uri? link}) => Timer(
                const Duration(seconds: 1),
                () {
                  final banner = SnackBar(
                    content: Text(errorMsg),
                    duration: const Duration(seconds: 30),
                    action: link == null
                        ? null
                        : SnackBarAction(
                            label: l10n!.link,
                            onPressed: () => launchUrl(Uri.parse(link.toString())),
                          ),
                  );
                  ScaffoldMessenger.of(navigatorContext).showSnackBar(banner);
                },
              ),
            );
          }
        }
      });
    }
  }

  void createVoipPlugin() async {
    if (store.getBool(SettingKeys.experimentalVoip) == false) {
      voipPlugin = null;
      return;
    }
    voipPlugin = webrtcIsSupported ? VoipPlugin(this, theme.brightness) : null;
  }

  bool _firstStartup = true;

  bool firstTimeUser = true;

  void initContextProviders() {
    theme = Theme.of(context);
    l10n = L10n.of(context);
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);
    initialisePageMeServer();
    initMatrix();
    if (PlatformInfos.isWeb) {
      unawaited(initConfig().then((_) => initSettings()));
    } else {
      unawaited(initSettings());
    }
    if (PlatformInfos.isWindows) {
      unawaited(_initializeWinToast());
    }
    initLoadingDialog();
  }

  @override
  void didChangeDependencies() async {
    initContextProviders();
    createVoipPlugin();
    if (isPublic()) {
      subscriptionsCubit = BlocProvider.of<SubscriptionsCubit>(context);
    }
    super.didChangeDependencies();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    Logs().v('AppLifecycleState = $state');
    final foreground = state != AppLifecycleState.detached && state != AppLifecycleState.paused;
    client.backgroundSync = foreground;
    client.syncPresence = foreground ? null : PresenceType.unavailable;
    client.requestHistoryOnLimitedTimeline = !foreground;
    if (_firstStartup) {
      _firstStartup = false;
      _backgroundPush?.setupPush();
    }
  }

  Future<void> initSettings() async {
    final path = store.getString(SettingKeys.wallpaper);
    if (path != null) wallpaper = File(path);

    FlavorConfig.fontSizeFactor = double.tryParse(store.getString(SettingKeys.fontSizeFactor) ?? '') ?? FlavorConfig.fontSizeFactor;

    FlavorConfig.renderHtml = store.getBool(SettingKeys.renderHtml) ?? FlavorConfig.renderHtml;

    FlavorConfig.hideRedactedEvents = store.getBool(SettingKeys.hideRedactedEvents) ?? FlavorConfig.hideRedactedEvents;

    FlavorConfig.minimalChatListMode = store.getBool(SettingKeys.minimalChatListMode) ?? FlavorConfig.minimalChatListMode;

    FlavorConfig.hideUnknownEvents = store.getBool(SettingKeys.hideUnknownEvents) ?? FlavorConfig.hideUnknownEvents;

    FlavorConfig.separateChatTypes = store.getBool(SettingKeys.separateChatTypes) ?? FlavorConfig.separateChatTypes;

    FlavorConfig.autoplayImages = store.getBool(SettingKeys.autoplayImages) ?? FlavorConfig.autoplayImages;

    /* FlavorConfig.sendTypingNotifications =
        store.getBool(SettingKeys.sendTypingNotifications) ??
            FlavorConfig.sendTypingNotifications;*/

    FlavorConfig.sendOnEnter = store.getBool(SettingKeys.sendOnEnter) ?? FlavorConfig.sendOnEnter;

    FlavorConfig.experimentalVoip = store.getBool(SettingKeys.experimentalVoip) ?? FlavorConfig.experimentalVoip;
    if (!kIsWeb) {
      if (Platform.isIOS || Platform.isAndroid || Platform.isWindows) requestPermissions();
    }
  }

  void requestPermissions() async {
    // You can request multiple permissions at once.
    final Map<Permission, PermissionStatus> statuses = await [
      Permission.phone,
      Permission.nearbyWifiDevices,
      Permission.storage,
      Permission.photos,
      Permission.videos,
      Permission.mediaLibrary,
      Permission.storage,
      Permission.microphone,
      Permission.camera,
      Permission.locationWhenInUse,
      Permission.notification,
      Permission.manageExternalStorage,
      Permission.accessMediaLocation,
    ].request();
    if (kDebugMode) {
      print(statuses.toString());
    }
  }

  @override
  void dispose() async {
    WidgetsBinding.instance.removeObserver(this);

    onRoomKeyRequestSub.values.map((s) => s.cancel());
    onKeyVerificationRequestSub.values.map((s) => s.cancel());
    onLoginStateChanged.values.map((s) => s.cancel());
    onNotification.values.map((s) => s.cancel());
    client.httpClient.close();
    await onFocusSub?.cancel();
    await onBlurSub?.cancel();
    _backgroundPush?.onLogin?.cancel();
    await linuxNotifications?.close();
    await cancelForegroundNotifications();

    super.dispose();
  }

  Future<void> cancelForegroundNotifications() async {
    for (final int id in foregroundNotificationIds) {
      await FlutterLocalNotificationsPlugin().cancel(id);
    }
    foregroundNotificationIds.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (_) => this,
      child: widget.child,
    );
  }
}

class FixedThreepidCreds extends ThreepidCreds {
  FixedThreepidCreds({
    required String sid,
    required String clientSecret,
    String? idServer,
    String? idAccessToken,
  }) : super(
          sid: sid,
          clientSecret: clientSecret,
          idServer: idServer,
          idAccessToken: idAccessToken,
        );

  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['sid'] = sid;
    data['client_secret'] = clientSecret;
    if (idServer != null) data['id_server'] = idServer;
    if (idAccessToken != null) data['id_access_token'] = idAccessToken;
    return data;
  }
}

class _AccountBundleWithClient {
  final Client? client;
  final AccountBundle? bundle;

  _AccountBundleWithClient({this.client, this.bundle});
}
