import 'package:matrix/matrix.dart';

import 'package:flutter/material.dart';


import '../config/flavor_config.dart';
import 'mxc_image.dart';

class Avatar extends StatelessWidget {
  final Uri? mxContent;
  final String? name;
  final double size;
  final void Function()? onTap;
  static const double defaultSize = 44;
  final Client? client;
  final double fontSize;
  final bool isSpace;
  final BorderRadius? spaceBorderRadius;

  const Avatar({
    this.mxContent,
    this.name,
    this.size = defaultSize,
    this.onTap,
    this.client,
    this.fontSize = 18,
    this.isSpace = false,
    Key? key,
    this.spaceBorderRadius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var fallbackLetters = '@';
    final name = this.name;
    if (name != null) {
      if (name.runes.length >= 2) {
        fallbackLetters = String.fromCharCodes(name.runes, 0, 2);
      } else if (name.runes.length == 1) {
        fallbackLetters = name;
      }
    }
    final noPic = mxContent == null || mxContent.toString().isEmpty || mxContent.toString() == 'null';
    final textWidget = Center(
      child: Text(
          fallbackLetters.length == 2 ? "${fallbackLetters.substring(0,1).toUpperCase()}${fallbackLetters.substring(1,2).toLowerCase()}" : fallbackLetters.toUpperCase(),
        style: TextStyle(
          color: noPic ? Theme.of(context).colorScheme.onSecondary : null, //name?.darkColor : null,
          fontSize: fontSize,
        ),
      ),
    );
    final borderRadius = isSpace ? spaceBorderRadius ?? BorderRadius.circular(FlavorConfig.borderRadius - 4) : BorderRadius.circular(size / 2);
    final container = Material(
      elevation: 1,
      shadowColor: Colors.black,
      shape: RoundedRectangleBorder(
        borderRadius: borderRadius,
      ),
      child: ClipRRect(
        borderRadius: borderRadius,
        child: Container(
          width: size,
          height: size,
          color: isBusiness()
              ? noPic
                  ? Theme.of(context).colorScheme.secondary
                  : Colors.transparent
              : noPic
                  ? Theme.of(context).colorScheme.secondary
                  : Colors.transparent,
          child: noPic
              ? textWidget
              : MxcImage(
                  key: Key(mxContent.toString()),
                  uri: mxContent,
                  fit: BoxFit.cover,
                  width: size,
                  height: size,
                  placeholder: (_) => textWidget,
                  cacheKey: mxContent.toString(),
                ),
        ),
      ),
    );
    if (onTap == null) return container;
    return InkWell(
      onTap: onTap,
      borderRadius: borderRadius,
      child: container,
    );
  }
}
