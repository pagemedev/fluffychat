import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';

import '../pages/chat/events/image_bubble.dart';
import '../pages/chat/events/video_player.dart';

class TimestampedImage extends StatelessWidget {
  final Event event;
  final bool longPressSelect;
  final VoidCallback? onTap;
  final Size timeStampPadding;

  const TimestampedImage({
    super.key,
    required this.event,
    required this.longPressSelect,
    required this.onTap,
    required this.timeStampPadding,
  });

  @override
  Widget build(BuildContext context) {
    //Logs().i( '[TimestampedImage] ${timeStampPadding.toString()}');
    return Stack(
      children: [
        ImageBubble(
          event,
          width: event.getImageDimensions()?.width ?? 400,
          height: event.getImageDimensions()?.height ?? 300,
          longPressSelect: longPressSelect,
          onTap: onTap,
          fit: BoxFit.contain,
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            height: 16,
            width: timeStampPadding.width + 8,
            decoration: const BoxDecoration(
              color: Colors.black54,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(6),
                topLeft: Radius.circular(6),
              ),
            ),
          ),
        ),
//TODO ImageFormatStamp
/*
        Positioned(
          top: 0,
          left: 0,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            height: 16,
            width: timeStampPadding.width + 8,
            decoration: const BoxDecoration(
              color: Colors.black54,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(6),
                topLeft: Radius.circular(6),
              ),
            ),
          ),
        )
*/
      ],
    );
  }
}

class TimestampedVideo extends StatelessWidget {
  final Event event;
  final bool longPressSelect;
  final VoidCallback? onTap;
  final Size timeStampPadding;

  const TimestampedVideo({
    super.key,
    required this.event,
    required this.longPressSelect,
    required this.onTap,
    required this.timeStampPadding,
  });

  @override
  Widget build(BuildContext context) {
    //Logs().i( '[TimestampedImage] ${timeStampPadding.toString()}');
    return Stack(
      children: [
        EventVideoPlayer(
          event,
          width: event.getImageDimensions()?.width,
          height: event.getImageDimensions()?.height,
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            height: 16,
            width: timeStampPadding.width + 8,
            decoration: const BoxDecoration(
              color: Colors.black54,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(6),
                topLeft: Radius.circular(6),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
