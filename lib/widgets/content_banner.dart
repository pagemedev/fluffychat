
import 'package:flutter/material.dart';

import 'package:matrix/matrix.dart';

import '../utils/platform_infos.dart';
import 'matrix.dart';
import 'mxc_image.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class ContentBanner extends StatelessWidget {
  final Uri? mxContent;
  final double height;
  final IconData defaultIcon;
  final void Function()? onEdit;
  final bool enableViewer;
  final Client? client;
  final double opacity;
  final String heroTag;

  const ContentBanner(
      {this.mxContent,
      this.height = 400,
      this.defaultIcon = Icons.account_circle_outlined,
      this.onEdit,
      this.client,
      this.opacity = 0.75,
      this.heroTag = 'content_banner',
      Key? key,
      this.enableViewer = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final onEdit = this.onEdit;
    return Container(
      height: height,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Theme.of(context).secondaryHeaderColor,
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            child: Opacity(
              opacity: opacity,
              child: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                return GestureDetector(
                  onTap: ()
                  {
                    if (!enableViewer) return;
                    showDialog(
                      context: Matrix.of(context).navigatorContext,
                      useRootNavigator: false,
                      builder: (_) => ProfileImageViewer(mxContent: mxContent),
                    );
                  },
                  child: Hero(
                    tag: heroTag,
                    child: MxcImage(
                      cacheKey: mxContent?.toString(),
                      placeholder: (context) => Center(
                          child: Icon(
                        defaultIcon,
                        size: 50,
                      )),
                      key: Key(mxContent?.toString() ?? 'NoKey'),
                      uri: mxContent,
                      animated: true,
                      fit: BoxFit.cover,
                      height: 400,
                      width: 800,
                    ),
                  ),
                );
              }),
            ),
          ),
          if (onEdit != null)
            Container(
              margin: const EdgeInsets.all(8),
              alignment: Alignment.bottomRight,
              child: FloatingActionButton(
                mini: true,
                onPressed: onEdit,
                backgroundColor: Theme.of(context).colorScheme.primary,
                foregroundColor: Theme.of(context).colorScheme.onPrimary,
                child: const Icon(Icons.camera_alt_outlined),
              ),
            ),
        ],
      ),
    );
  }
}

class ProfileImageViewer extends StatefulWidget {
  final Uri? mxContent;
  const ProfileImageViewer({Key? key, this.mxContent}) : super(key: key);

  @override
  State<ProfileImageViewer> createState() => _ProfileImageViewerState();
}

class _ProfileImageViewerState extends State<ProfileImageViewer> {

  static const maxScaleFactor = 1.5;

  /// Go back if user swiped it away
  void onInteractionEnds(ScaleEndDetails endDetails) {
    if (PlatformInfos.usesTouchscreen == false) {
      if (endDetails.velocity.pixelsPerSecond.dy >
          MediaQuery.sizeOf(context).height * maxScaleFactor) {
        Navigator.of(context, rootNavigator: false).pop();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: Navigator.of(context).pop,
          color: Colors.white,
          tooltip: L10n.of(context)!.close,
        ),
        backgroundColor: const Color(0x44000000),
      ),
      body: InteractiveViewer(
        minScale: 1.0,
        maxScale: 10.0,
        onInteractionEnd: onInteractionEnds,
        child: Center(
          child: Hero(
            tag: 'content',
            child: MxcImage(
              placeholder: (context) => const Center(
                  child: Icon(
                    Icons.person_outline_outlined,
                size: 50,
              )),
              key: Key(widget.mxContent?.toString() ?? 'NoKey'),
              uri: widget.mxContent,
              animated: true,
              isThumbnail: false,
              fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );
  }
}
