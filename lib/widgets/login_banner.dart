import 'package:flutter/material.dart';

class LoginBanner extends StatelessWidget {
  const LoginBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Theme.of(context).primaryColor.withOpacity(0.8),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 12, 8, 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              height: 80,
              child: Image.asset('assets/PageMe white logo-01-small.png', color: Theme.of(context).primaryColor,),
            ),
            Container(
              height: 80,
              width: 2,
              color: Theme.of(context).primaryColor,
            ),
            SizedBox(
              height: 80,
              child: Image.asset('assets/page_me_four_words.png', color: Theme.of(context).primaryColor,),
            ),
          ],
        ),
      ),
    );
  }
}
