import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../config/themes.dart';
import 'matrix.dart';

class SettingsSwitchListTile extends StatefulWidget {
  final bool defaultValue;
  final String storeKey;
  final String title;
  final Function(bool)? onChanged;

  const SettingsSwitchListTile.adaptive({
    Key? key,
    this.defaultValue = false,
    required this.storeKey,
    required this.title,
    this.onChanged,
  }) : super(key: key);

  @override
  SettingsSwitchListTileState createState() => SettingsSwitchListTileState();
}

class SettingsSwitchListTileState extends State<SettingsSwitchListTile> {
  late MatrixState _matrixState;

  @override
  void didChangeDependencies() {
    _matrixState = Matrix.of(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoListTile(
        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
        title: Text(widget.title),
        trailing: Switch.adaptive(
          value: _matrixState.store.getBool(widget.storeKey) ?? widget.defaultValue,
          onChanged: (bool newValue) async {
            widget.onChanged?.call(newValue);
            await _matrixState.store.setBool(widget.storeKey, newValue);
            setState(() {});
          },
        ),
      );
  }
}

class SettingsNotificationsSwitchListTile extends StatefulWidget {
  final bool value;
  final String title;
  final Function(bool)? onChanged;

  const SettingsNotificationsSwitchListTile.adaptive({
    Key? key,
    required this.value,
    required this.title,
    this.onChanged,
  }) : super(key: key);

  @override
  SettingsNotificationsSwitchListTileState createState() => SettingsNotificationsSwitchListTileState();
}

class SettingsNotificationsSwitchListTileState extends State<SettingsNotificationsSwitchListTile> {
  @override
  Widget build(BuildContext context) {
    return  CupertinoListTile(
        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
        title: Text(widget.title),
        trailing: Switch.adaptive(
          value: widget.value,
          onChanged: (bool newValue) async {
            widget.onChanged?.call(newValue);
            setState(() {});
          },
        ),
      );

  }
}
