import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';


import 'package:pageMe/widgets/layouts/empty_page.dart';
import 'package:pageMe/widgets/matrix.dart';

import '../../pages/subscriptions/subscriptions_cubit.dart';
import '../../utils/logger_functions.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

class LoadingView extends StatefulWidget {
  const LoadingView({Key? key}) : super(key: key);

  @override
  State<LoadingView> createState() => _LoadingViewState();
}

class _LoadingViewState extends State<LoadingView> {
  late MatrixState matrixState;
  late SubscriptionsCubit subscriptionsCubit;

  Future<void> autoLogin(BuildContext context) async {
    Logs().v('LoadingView - autoLogin started');
    try {
      if (matrixState.client.isLogged()) {
        if (isPublic()) {
          try {
            await subscriptionsCubit.login(appUserId: matrixState.client.userID!);
          } on Exception catch (e) {
            Logs().e("LoadingView - login: ${e.toString()}");
          }
          Logs().v('LoadingView - autoLogin complete');
          final bool isSubscribed = subscriptionsCubit.state.isSubscribed;
          //Logs().i( 'LoadingView - isSubscribed: $isSubscribed, ConnectionState: ${snapshot.connectionState}');
          context.go(Uri(
            pathSegments: [
              matrixState.widget.clients.any((client) => client.onLoginStateChanged.value == LoginState.loggedIn)
                  ? isSubscribed
                      ? '/rooms'
                      : '/subscriptions'
                  : '/home'
            ],
            queryParameters: GoRouterState.of(context).uri.queryParameters,
          ).toString());
        } else {
          context.go(
              Uri(
                pathSegments: [matrixState.widget.clients.any((client) => client.onLoginStateChanged.value == LoginState.loggedIn) ? '/rooms' : '/home'],
                queryParameters: GoRouterState.of(context).uri.queryParameters,
              ).toString()
          );
        }
        //return true;
      } else {
        Logs().e('LoadingView - autoLogin failed');
        context.go(
          Uri(
            pathSegments: ['/home'],
            queryParameters: GoRouterState.of(context).uri.queryParameters,
          ).toString()
        );
      }
    } on Exception catch (e) {
      Logs().e("LoadingView - ${e.toString()}");
      context.go(
          Uri(
            pathSegments: ['/home'],
            queryParameters: GoRouterState.of(context).uri.queryParameters,
          ).toString()
      );
    }
  }

  @override
  void didChangeDependencies() async {
    matrixState = Matrix.of(context);
    if (isPublic()) {
      subscriptionsCubit = BlocProvider.of<SubscriptionsCubit>(context);
    }

    if (isBusiness()) {
      Logs().i('test');
      WidgetsBinding.instance.addPostFrameCallback(
        (_) => context.go(
            Uri(
              pathSegments: [matrixState.widget.clients.any((client) => client.onLoginStateChanged.value == LoginState.loggedIn) ? '/rooms' : '/home'],
              queryParameters: GoRouterState.of(context).uri.queryParameters,
            ).toString()
        ),
      );
    } else {
      await autoLogin(context);
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();

    return const EmptyPage(loading: true);
  }
}
