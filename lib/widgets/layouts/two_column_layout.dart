import 'package:flutter/material.dart';
import 'package:flutter_resizable_container/flutter_resizable_container.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:resizable_widget/resizable_widget.dart';

import '../../config/themes.dart';

class TwoColumnLayout extends StatelessWidget {
  final Widget mainView;
  final Widget sideView;
  final bool displayNavigationRail;

  const TwoColumnLayout({
    Key? key,
    required this.mainView,
    required this.sideView,
    required this.displayNavigationRail,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    const double outerPadding = 12;
    const double innerPadding = 8;
    return ScaffoldMessenger(
      child: Scaffold(
        backgroundColor: PageMeThemes.isDarkMode(context) ? Colors.black : const Color(0xFF818ba1),
        body: Padding(
          padding: EdgeInsets.only(
            left: outerPadding,
            right: outerPadding,
            bottom: PlatformInfos.isIOS ? (outerPadding + 4) : outerPadding,
            top: PlatformInfos.isIOS ? (outerPadding + 10) : outerPadding,
          ),
          child: /*ResizableWidget(
            isHorizontalSeparator: false, // optional
            isDisabledSmartHide: false, // optional
            separatorColor: Colors.white12, // optional
            separatorSize: 4, // optional
            percentages: [0.35, 0.65], // optional
            onResized: (infoList) => // optional
                print(infoList.map((x) => '(${x.size}, ${x.percentage}%)').join(", ")),
            children: [
              // required
              Padding(
                padding: const EdgeInsets.only(right: innerPadding),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(color: Theme.of(context).colorScheme.background, borderRadius: BorderRadius.circular(FlavorConfig.borderRadius)),
                  width: 360.0 + (displayNavigationRail ? 200 : 0),
                  child: mainView,
                ),
              ),
          Expanded(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
                child: sideView,
              ),)
            ],
          ),*/
          Row(

            children: [

              Padding(
                padding: const EdgeInsets.only(right: innerPadding),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(color: Theme.of(context).colorScheme.background, borderRadius: BorderRadius.circular(FlavorConfig.borderRadius)),
                  width: 360.0 + (displayNavigationRail ? 200 : 0),
                  child: mainView,
                ),
              ),
              Divider(
                height: 1.0,
                color: Theme.of(context).dividerColor,
              ),
              Expanded(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
                  child: sideView,
                ),
              ),
            ],
          )
        ),
      ),
    );
  }
}
