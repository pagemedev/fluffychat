import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';



import 'package:pageMe/config/themes.dart';

import '../../config/flavor_config.dart';

class SideViewLayout extends StatelessWidget {
  final Widget mainView;
  final Widget? sideView;

  const SideViewLayout({Key? key, required this.mainView, this.sideView}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    const double innerPadding = 8;
    var currentUrl = GoRouterState.of(context).uri.path;
    if (!currentUrl.endsWith('/')) currentUrl += '/';
    final hideSideView = currentUrl.split('/').length == 4;
    final sideView = this.sideView;
    return sideView == null
        ? mainView
        : MediaQuery.sizeOf(context).width < PageMeThemes.columnWidth * 3.5 && !hideSideView
            ? sideView
            : Row(
                children: [
                  Expanded(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
                      child: mainView,
                    ),
                  ),
/*                  Container(
                    width: 1.0,
                    color: Theme.of(context).dividerColor,
                  ),*/
                  Padding(
                    padding: const EdgeInsets.only(left: innerPadding),
                    child: AnimatedContainer(
                      duration: const Duration(milliseconds: 300),
                      clipBehavior: Clip.antiAlias,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
                      ),
                      width: hideSideView ? 0 : 360.0,
                      child: hideSideView ? null : sideView,
                    ),
                  ),
                ],
              );
  }
}
