import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../config/flavor_config.dart';
import '../../config/themes.dart';

class EmptyPage extends StatelessWidget {
  final bool loading;
  static const double _width = 200;
  const EmptyPage({this.loading = false, Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _width = min(MediaQuery.sizeOf(context).width, EmptyPage._width);
    return Scaffold(
      // Add invisible appbar to make status bar on Android tablets bright.
      appBar: AppBar(
        automaticallyImplyLeading: false,
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(2.0),
            child: Container(
                height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
                color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black //Theme.of(context).colorScheme.primary,
              //indent: 12,
              //endIndent: 12,
            ),
          )),
      extendBodyBehindAppBar: true,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Container(
                height: 75,
                width: 75,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Theme.of(context).colorScheme.primaryContainer
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 14.0, right: 14, top: 16, bottom: 12),
                  child: SvgPicture.asset('assets/pageme_chat_bubble.svg',),
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              if (!loading)
                Text(
                  'Welcome to ${FlavorConfig.applicationName}',
                  style: const TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                ),
              if (!loading)
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Don't be a product, be protected",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  ),
                ),
              if (!loading)
                Row(
                  children: [
                  ],
                )
            ],
          ),
          if (loading)
            Center(
              child: SizedBox(
                width: _width,
                child: const LinearProgressIndicator(),
              ),
            ),
        ],
      ),
    );
  }
}
