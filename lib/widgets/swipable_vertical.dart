import 'package:flutter/material.dart';
import 'dart:math' as maths;

import 'package:matrix/matrix.dart';

class SwipeableVertical extends StatefulWidget {
  final Widget? child;
  final VoidCallback? onSwipeStart;
  final VoidCallback? onSwipeLeft;
  final VoidCallback? onSwipeRight;
  final VoidCallback? onSwipeCancel;
  final VoidCallback? onSwipeEnd;
  final double threshold;
  final Size? childSize;

  const SwipeableVertical({
    super.key,
    required this.child,
    this.onSwipeStart,
    this.onSwipeLeft,
    this.onSwipeRight,
    this.onSwipeCancel,
    this.onSwipeEnd,
    this.threshold = 64.0,
    this.childSize,
  });

  @override
  State<StatefulWidget> createState() {
    return _SwipeableVerticalState();
  }
}

class _SwipeableVerticalState extends State<SwipeableVertical> with TickerProviderStateMixin {
  ValueNotifier<double> _dragExtent = ValueNotifier<double>(0.0);
  late AnimationController _moveController;
  late Animation<Offset> _moveAnimation;
  bool _pastLeftThreshold = false;
  bool _pastRightThreshold = false;
  bool _releasePastThreshold = false;

  get math => null; // Added this new variable

  @override
  void initState() {
    super.initState();
    _moveController = AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    );
    _moveAnimation = Tween<Offset>(begin: Offset.zero, end: const Offset(0.0, 1.0)).animate(_moveController);
    _moveController.animateTo(0.0);
  }

  @override
  void dispose() {
    _moveController.dispose();
    _dragExtent.dispose();
    super.dispose();
  }

  void _handleDragStart(DragStartDetails details) {
    if (widget.onSwipeStart != null) {
      widget.onSwipeStart!();
    }
  }

  void _handleDragUpdate(DragUpdateDetails details) {
    final delta = details.primaryDelta;
    final oldDragExtent = _dragExtent.value;
    _dragExtent.value -= delta ?? 0;

    // Prevent downward swipes from start
    if (_dragExtent.value < 0) _dragExtent.value = 0;

    if (oldDragExtent.sign != _dragExtent.value.sign) {
      setState(() {
        _updateMoveAnimation();
      });
    }

    final movePastThresholdPixels = widget.threshold;
    double newPos;
    if (widget.childSize != null) {
      newPos = (_dragExtent.value / widget.childSize!.height).clamp(0.0, 1.0);
    } else {
      newPos = (_dragExtent.value / movePastThresholdPixels).clamp(0.0, 1.0);
    }

    if (_dragExtent.value.abs() > movePastThresholdPixels) {
      _releasePastThreshold = true; // Set to true if widget is past threshold

      final n = _dragExtent.value.abs() / movePastThresholdPixels;
      final reducedThreshold = maths.pow(n, 0.3);

      final adjustedPixelPos = movePastThresholdPixels * reducedThreshold;
      newPos = adjustedPixelPos / context.size!.height;

      if (_dragExtent.value > 0 && !_pastRightThreshold) {
        _pastRightThreshold = true;

        if (widget.onSwipeRight != null) {
          widget.onSwipeRight!();
        }
      }
    } else {
      _releasePastThreshold = false; // Set to false if widget is before threshold

      if (_pastRightThreshold) {
        if (widget.onSwipeCancel != null) {
          widget.onSwipeCancel!();
        }
      }
      _pastRightThreshold = false;
    }

    _moveController.value = newPos;
  }

  void _handleDragEnd(DragEndDetails details) {
    _moveController.animateTo(0.0, duration: const Duration(milliseconds: 200), curve: Curves.easeIn);
    _dragExtent.value = 0.0;

    if (widget.onSwipeEnd != null && _releasePastThreshold) {
      // Changed this line
      widget.onSwipeEnd!();
    }

    _releasePastThreshold = false; // Reset to false after each swipe
  }

  void _updateMoveAnimation() {
    final double end = _dragExtent.value.isNegative ? -1.0 : 1.0;
    _moveAnimation = Tween<Offset>(begin: const Offset(0.0, 0.0), end: Offset(0.0, -end)).animate(_moveController);
  }

  double checkScale(double scale) {
    if (scale >= -1 && scale < 0) {
      return -scale;
    } else if (scale >= 0 && scale <= 1) {
      return scale;
    } else {
      return 1.0;
    }
  }

  Widget _buildSwipeableBackground(BuildContext context, AnimationController moveController, ValueNotifier<double> dragExtent, double threshold) {
    return AnimatedBuilder(
      animation: Listenable.merge([moveController, dragExtent]),
      builder: (context, child) {
        final double scale = checkScale(dragExtent.value / threshold);
        final Color color = dragExtent.value.abs() > widget.threshold ? Colors.redAccent : Theme.of(context).colorScheme.onBackground;
        return Align(
          alignment: Alignment.center,
          child: Transform.scale(
            scale: scale,
            child: ColorFiltered(
              colorFilter: ColorFilter.mode(color, BlendMode.modulate),
              child: Icon(
                Icons.delete,
                size: 30,
                color: color,
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[
      _buildSwipeableBackground(context, _moveController, _dragExtent, widget.threshold),
      SlideTransition(
        position: _moveAnimation,
        child: widget.child,
      ),
    ];

    return SizedBox.fromSize(
      size: widget.childSize,
      child: GestureDetector(
        onVerticalDragStart: _handleDragStart,
        onVerticalDragUpdate: _handleDragUpdate,
        onVerticalDragEnd: _handleDragEnd,
        behavior: HitTestBehavior.opaque,
        child: Stack(
          fit: StackFit.expand,
          children: children,
        ),
      ),
    );
  }
}
