import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/utils/date_time_extension.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';
import 'package:path/path.dart' as p;
import 'package:desktop_lifecycle/desktop_lifecycle.dart';
import 'package:desktop_notifications/desktop_notifications.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:http/http.dart' as http;
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_html/html.dart' as html;


import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:win_toast/win_toast.dart';
import 'package:window_manager/window_manager.dart';

import '../config/setting_keys.dart';
import '../pages/chat/chat.dart';
import '../utils/logger_functions.dart';
import '../utils/matrix_sdk_extensions.dart/client_stories_extension.dart';
import '../utils/platform_infos.dart';
import '../utils/voip/callkeep_manager.dart';
import 'mxc_image_fetch.dart';

extension LocalNotificationsExtension on MatrixState {
  void showLocalEventNotification(EventUpdate eventUpdate) async {
    final roomId = eventUpdate.roomID;
    if (activeRoomId == roomId) {
      if (kIsWeb && webHasFocus) return;
      if ((PlatformInfos.isLinux || PlatformInfos.isWindows) && DesktopLifecycle.instance.isActive.value) {
        Logs().v('showLocalNotification - currently in active room and has window in active');
        return;
      }
    }
    final room = client.getRoomById(roomId);
    if (room == null) {
      Logs().w('Can not display notification for unknown room $roomId');
      return;
    }
    if (room.notificationCount == 0) return;
    final event = Event.fromJson(eventUpdate.content, room);
    final String title = room.getLocalizedDisplayname(MatrixLocals(L10n.of(context)!));
    final body = await event.calcLocalizedBody(
      MatrixLocals(L10n.of(context)!),
      withSenderNamePrefix: !room.isDirectChat || room.lastEvent?.senderId == client.userID,
      plaintextBody: true,
      hideReply: true,
      hideEdit: true,
      removeMarkdown: true,
    );
    final icon = event.senderFromMemoryOrFallback.avatarUrl?.getThumbnail(client, width: 64, height: 64, method: ThumbnailMethod.crop) ??
        room.avatar?.getThumbnail(client, width: 64, height: 64, method: ThumbnailMethod.crop);
    if (PlatformInfos.isWeb) {
      _displayWebNotification(title, body, icon);
    } else if (PlatformInfos.isLinux) {
      await _displayLinuxNotification(room, title, body, roomId, event);
    } else if (PlatformInfos.isWindows) {
      await _displayWindowsEventNotification(room, roomId, title, body, eventUpdate);
    } else if (PlatformInfos.isMacOS) {
      await _displayMacOSNotification(room, title, body, roomId, event);
    }
  }

  Future<void> displayWindowsDownloadNotification(
    String title,
    String body,
    String? downloadPath,
  ) async {
    Logs().v('displayWindowsDownloadNotification called');
    if (initializedWinToast) {
      try {
        await windowsNotifications!.showToast(
          toast: Toast(
            duration: ToastDuration.short,
            launch: jsonEncode({'action': 'openFile'}),
            children: [
              ToastChildAudio(source: ToastAudioSource.defaultSound),
              ToastChildVisual(
                binding: ToastVisualBinding(
                  children: [
                    ToastVisualBindingChildText(
                      text: title,
                      id: 1,
                    ),
                    ToastVisualBindingChildText(
                      text: body,
                      id: 2,
                    ),
                  ],
                ),
              ),
              ToastChildActions(children: [
                ToastAction(
                  content: "Open file",
                  arguments: jsonEncode({'action': 'openFile', 'downloadPath': '$downloadPath'}),
                  activationType: ToastActionActivationType.background,
                ),
                ToastAction(
                  content: 'Open file location',
                  arguments: jsonEncode({'action': 'openFileLocation', 'downloadPath': downloadPath}),
                  activationType: ToastActionActivationType.background,
                ),
              ]),
            ],
          ),
        );
      } catch (error, stacktrace) {
        loggerError(logMessage: 'Error showing Windows toast notification: $error, $stacktrace');
      }
    } else {
      final ret = await windowsNotifications!.initialize(
        aumId: FlavorConfig.appId,
        displayName: FlavorConfig.applicationName,
        iconPath: '', // Optional: Path to your app's icon
        clsid: FlavorConfig.applicationGUID, // Replace with a valid UUID
      );
      setState(() {
        initializedWinToast = ret;
      });
    }
  }

  Future<void> _displayWindowsEventNotification(Room room, String roomId, String title, String body, EventUpdate eventUpdate) async {
    Logs().v('_displayWindowsNotification called');
    if (initializedWinToast) {
      final appIconUrl = room.avatar?.getThumbnail(
        room.client,
        width: 56,
        height: 56,
      );
      File? appIconFile;
      if (appIconUrl != null) {
        final tempDirectory = await getApplicationDocumentsDirectory();
        Directory avatarDirectory = await Directory(p.join(tempDirectory.path, '${FlavorConfig.applicationName}\\')).create();
        avatarDirectory = await Directory(p.join(avatarDirectory.path, 'notiavatars\\')).create();
        appIconFile = File(p.join(avatarDirectory.path, Uri.encodeComponent('$appIconUrl.png')));
        if (await appIconFile.exists() == false) {
          final response = await http.get(appIconUrl);
          await appIconFile.writeAsBytes(response.bodyBytes);
        }
      }
      try {
        await windowsNotifications!.showToast(
          toast: Toast(
            duration: ToastDuration.short,
            launch: jsonEncode({'action': 'openChat', 'roomId': roomId}),
            children: [
              ToastChildAudio(source: ToastAudioSource.defaultSound),
              ToastChildVisual(
                binding: ToastVisualBinding(
                  children: [
                    ToastVisualBindingChildText(
                      text: title,
                      id: 1,
                    ),
                    ToastVisualBindingChildText(
                      text: body,
                      id: 2,
                    ),
                    if (appIconUrl != null)
                      ToastVisualBindingChildImage(
                        src: appIconFile!.path,
                        addImageQuery: true,
                        id: 1,
                        placement: ToastImagePlacement.appLogoOverride,
                        crop: true,
                      ),
                  ],
                ),
              ),
              ToastChildActions(children: [
                ToastAction(
                  content: L10n.of(context)!.openChat,
                  arguments: jsonEncode({'action': 'openChat', 'roomId': roomId}),
                  activationType: ToastActionActivationType.foreground,
                ),
                ToastAction(
                  content: L10n.of(context)!.markAsRead,
                  arguments: jsonEncode({'action': 'markAsRead', 'roomId': roomId, 'event': eventUpdate.content}),
                  activationType: ToastActionActivationType.background,
                ),
              ]),
            ],
          ),
        );
      } catch (error, stacktrace) {
        loggerError(logMessage: 'Error showing Windows toast notification: $error, $stacktrace');
      }
    } else {
      final ret = await windowsNotifications!.initialize(
        aumId: FlavorConfig.appId,
        displayName: FlavorConfig.applicationName,
        iconPath: '', // Optional: Path to your app's icon
        clsid: FlavorConfig.applicationGUID, // Replace with a valid UUID
      );
      setState(() {
        initializedWinToast = ret;
      });
    }
  }

  Future<void> onSelectNotification(NotificationResponse? response) async {
    final isVisible = await windowManager.isVisible();
    final isFocused = await windowManager.isFocused();
    if (!isVisible) {
      windowManager.show();
    }
    if (!isFocused) {
      windowManager.focus();
    }

    try {
      final String? roomId = response?.payload.toString().split(' ')[1];
      Logs().v('[Push] Attempting to go to room $roomId...');
      if (roomId == null) {
        return;
      }
      await client.roomsLoading;
      await client.accountDataLoading;
      context.go('/rooms/${roomId}');
    } catch (e, s) {
      Logs().e('[Push] Failed to open room', e, s);
    }
  }

  Future<void> _displayMacOSNotification(Room room, String title, String body, String roomId, Event event) async {
    final appIconUrl = room.avatar?.getThumbnail(
      room.client,
      width: 56,
      height: 56,
    );
    File? appIconFile;
    if (appIconUrl != null) {
      final tempDirectory = await getApplicationSupportDirectory();
      final avatarDirectory = await Directory('${tempDirectory.path}/notiavatars/').create();
      appIconFile = File('${avatarDirectory.path}/${Uri.encodeComponent(appIconUrl.toString())}');

      if (await appIconFile.exists() == false) {
        final response = await http.get(appIconUrl);
        await appIconFile.writeAsBytes(response.bodyBytes);
      }
    }

    if (activeRoomId == roomId && activeRoomId != null && client.syncPresence == null) {
      Logs().v('Room is in foreground. Stop LocalNotificationsExtension here.');
      return;
    }

    // Initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    await flutterLocalNotificationsPlugin.initialize(
      const InitializationSettings(
        macOS: DarwinInitializationSettings(),
      ),
      onDidReceiveNotificationResponse: (notificationResponse) async => onSelectNotification(notificationResponse),
      //onDidReceiveBackgroundNotificationResponse: onSelectNotificationBackground,
    );

    Logs().v('LocalNotificationsExtension got notification event of type ${event.type}.');

    if (event.type.startsWith('m.call')) {
      // make sure bg sync is on (needed to update hold, unhold events)
      // prevent over write from app life cycle change
      client.backgroundSync = true;
    }

    if (event.type == EventTypes.CallInvite) {
      CallKeepManager().initialize();
    } else if (event.type == EventTypes.CallHangup) {
      client.backgroundSync = false;
    }

    if (event.type.startsWith('m.call') && event.type != EventTypes.CallInvite) {
      Logs().v('LocalNotification is a m.call but not invite. Do not display.');
      return;
    }

    if ((event.type.startsWith('m.call') && event.type != EventTypes.CallInvite) || event.type == 'org.matrix.call.sdp_stream_metadata_changed') {
      Logs().v('LocalNotification was for a call, but not call invite.');
      return;
    }

    Logs().w('LocalNotificationsExtension got notification event.');

    L10n? l10n = L10n.of(context);

    if (window.locale.languageCode == "und") {
      final Locale locale = Locale.fromSubtags(countryCode: "za", languageCode: 'en', scriptCode: window.locale.scriptCode);
      l10n ??= await L10n.delegate.load(locale);
    } else {
      l10n ??= await L10n.delegate.load(window.locale);
    }
    final matrixLocals = MatrixLocals(l10n);

    // Calculate the body
    final body = event.type == EventTypes.Encrypted
        ? "New message in PageMe"
        : event.messageType == CustomMessageTypes.quillText
            ? event.calcQuillTextToPlainText(locals: MatrixLocals(l10n)) ?? 'Sent a composed message'
            : await event.calcLocalizedBody(
                matrixLocals,
                plaintextBody: true,
                withSenderNamePrefix: false,
                hideReply: true,
                hideEdit: true,
                removeMarkdown: true,
              );

    Future<int> mapRoomIdToInt(String roomId) async {
      final store = await SharedPreferences.getInstance();
      final idMap = Map<String, int>.from(jsonDecode(store.getString(SettingKeys.notificationCurrentIds) ?? '{}'));
      int? currentInt;
      try {
        currentInt = idMap[roomId];
      } catch (_) {
        currentInt = null;
      }
      if (currentInt != null) {
        return currentInt;
      }
      var nCurrentInt = 0;
      while (idMap.values.contains(nCurrentInt)) {
        nCurrentInt++;
      }
      idMap[roomId] = nCurrentInt;
      await store.setString(SettingKeys.notificationCurrentIds, json.encode(idMap));
      return nCurrentInt;
    }

    final id = await mapRoomIdToInt(event.room.id);

    final List<Room> filteredRooms = client.rooms.where(
      (room) {
        return room.notificationCount != 0;
      },
    ).toList();

    int totalUnreadMessages = 0;

    for (final room in filteredRooms) {
      totalUnreadMessages += room.notificationCount;
    }

    final macOSPlatformChannelSpecifics = DarwinNotificationDetails(
      presentBadge: true,
      threadIdentifier: id.toString(),
      interruptionLevel: InterruptionLevel.active,
    );

    final platformChannelSpecifics = NotificationDetails(
      macOS: macOSPlatformChannelSpecifics,
    );

    await flutterLocalNotificationsPlugin.show(
      id,
      event.room.getLocalizedDisplayname(),
      body,
      platformChannelSpecifics,
      payload: '${event.eventId} ${event.roomId} ${FlavorConfig.applicationName}',
    );

    await FlutterAppBadger.updateBadgeCount(totalUnreadMessages);

    Logs().v('LocalNotificationsExtension has been completed!');
  }

  Future<void> _displayLinuxNotification(Room room, String title, String body, String roomId, Event event) async {
    Logs().v('LocalNotificationsExtension got a linux notification');
    final appIconUrl = room.avatar?.getThumbnail(
      room.client,
      width: 56,
      height: 56,
    );
    File? appIconFile;
    if (appIconUrl != null) {
      final tempDirectory = await getApplicationSupportDirectory();
      final avatarDirectory = await Directory('${tempDirectory.path}/notiavatars/').create();
      appIconFile = File('${avatarDirectory.path}/${Uri.encodeComponent(appIconUrl.toString())}');

      if (await appIconFile.exists() == false) {
        final response = await http.get(appIconUrl);
        await appIconFile.writeAsBytes(response.bodyBytes);
      }
    }
    final notification = await linuxNotifications!.notify(
      title,
      body: body,
      replacesId: linuxNotificationIds[roomId] ?? 0,
      appName: FlavorConfig.applicationName,
      appIcon: appIconFile?.path ?? '',
      actions: [
        NotificationAction(
          DesktopNotificationActions.openChat.name,
          L10n.of(context)!.openChat,
        ),
        NotificationAction(
          DesktopNotificationActions.seen.name,
          L10n.of(context)!.markAsRead,
        ),
      ],
      hints: [
        NotificationHint.soundName('message-new-instant'),
      ],
    );
    notification.action.then((actionStr) {
      final action = DesktopNotificationActions.values.singleWhere((a) => a.name == actionStr);
      switch (action) {
        case DesktopNotificationActions.seen:
          room.setReadMarker(event.eventId, mRead: event.eventId);
          break;
        case DesktopNotificationActions.openChat:
          windowManager.focus();
          context.go('/rooms/${room.id}');
          break;
      }
    });
    linuxNotificationIds[roomId] = notification.id;
  }
}

void _displayWebNotification(String title, String body, Uri? icon) {
  html.AudioElement()
    ..src = 'assets/assets/sounds/WoodenBeaver_stereo_message-new-instant.ogg'
    ..autoplay = true
    ..load();
  html.Notification(
    title,
    body: body,
    icon: icon.toString(),
  );
}

enum DesktopNotificationActions { seen, openChat }
