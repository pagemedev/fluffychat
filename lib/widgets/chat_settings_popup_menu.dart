import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/chat/cupertino_widgets_bottom_sheet.dart';
import 'package:pageMe/pages/chat/edit_widgets_dialog.dart';
import 'package:pageMe/pages/chat/widgets_bottom_sheet.dart';

import '../pageme_app.dart';
import '../utils/conditonal_wrapper.dart';
import '../utils/platform_infos.dart';
import 'm2_popup_menu_button.dart';
import 'matrix.dart';

class ChatSettingsPopupMenu extends StatefulWidget {
  final Room room;
  final bool displayChatDetails;

  const ChatSettingsPopupMenu(this.room, this.displayChatDetails, {Key? key}) : super(key: key);

  @override
  _ChatSettingsPopupMenuState createState() => _ChatSettingsPopupMenuState();
}

class _ChatSettingsPopupMenuState extends State<ChatSettingsPopupMenu> {
  StreamSubscription? notificationChangeSub;

  @override
  void dispose() async {
    unawaited(notificationChangeSub?.cancel());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    notificationChangeSub ??= Matrix.of(context).client.onAccountData.stream.where((u) => u.type == 'm.push_rules').listen(
          (u) => setState(() {}),
        );
    final items = <PopupMenuEntry<String>>[
/*      PopupMenuItem<String>(
        value: 'widgets',
        child: Row(
          children: [
            Icon(
              Icons.widgets_outlined,
              color: Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.white,
            ),
            const SizedBox(width: 12),
            Text(L10n.of(context)!.matrixWidgets),
          ],
        ),
      )*/
      widget.room.pushRuleState == PushRuleState.notify
          ? PopupMenuItem<String>(
              value: 'mute',
              child: Row(
                children: [
                  Icon(
                    Icons.notifications_off_outlined,
                    color: Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.white,
                  ),
                  const SizedBox(width: 12),
                  Text(L10n.of(context)!.muteChat),
                ],
              ),
            )
          : PopupMenuItem<String>(
              value: 'unmute',
              child: Row(
                children: [
                  Icon(
                    Icons.notifications_on_outlined,
                    color: Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.white,
                  ),
                  const SizedBox(width: 12),
                  Text(L10n.of(context)!.unmuteChat),
                ],
              ),
            ),
      const PopupMenuItem<String>(
        value: 'todos',
        child: Row(
          children: [
            Icon(Icons.task_alt_outlined),
            SizedBox(width: 12),
            Text("ToDo lists"),
          ],
        ),
      ),
      PopupMenuItem<String>(
        value: 'leave',
        child: Row(
          children: [
            const Icon(
              Icons.delete_outlined,
              color: Colors.red,
            ),
            const SizedBox(width: 12),
            Text(L10n.of(context)!.leave),
          ],
        ),
      ),
    ];
    if (widget.displayChatDetails) {
      items.insert(
        0,
        PopupMenuItem<String>(
          value: 'details',
          child: Row(
            children: [
              Icon(
                Symbols.settings,
                color: Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.white,
              ),
              const SizedBox(width: 12),
              Text(widget.room.isDirectChat ? "Chat Options" : "Space Options"),
            ],
          ),
        ),
      );
    }
    return Stack(
      alignment: Alignment.center,
      children: [
        ConditionalKeyBoardShortcuts(
          enabled: !PlatformInfos.isMobile,
          keysToPress: {LogicalKeyboardKey.controlLeft, LogicalKeyboardKey.keyI},
          helpLabel: widget.room.isSpace ? "Space Options": "Chat Options",
          onKeysPressed: _showChatDetails,
          child: Container(),
        ),
/*        ConditionalKeyBoardShortcuts(
          enabled: !PlatformInfos.isMobile,
          keysToPress: {LogicalKeyboardKey.controlLeft, LogicalKeyboardKey.keyW},
          helpLabel: L10n.of(context)!.matrixWidgets,
          onKeysPressed: _showWidgets,
          child: Container(),
        ),*/
        M2PopupMenuButton(
          icon: Icon(
            Icons.more_vert,
            color: Theme.of(context).colorScheme.onBackground,
          ),
          onSelected: (String choice) async {
            switch (choice) {
              case 'widgets':
                if (widget.room.widgets.isNotEmpty) {
                  _showWidgets();
                } else {
                  await showDialog(
                    context: context,
                    builder: (context) => EditWidgetsDialog(room: widget.room),
                    useRootNavigator: false,
                  );
                }
                break;
              case 'leave':
                final confirmed = await showOkCancelAlertDialog(
                  useRootNavigator: false,
                  context: context,
                  title: L10n.of(context)!.areYouSure,
                  okLabel: L10n.of(context)!.ok,
                  cancelLabel: L10n.of(context)!.cancel,
                );
                if (confirmed == OkCancelResult.ok) {
                  final success = await showFutureLoadingDialog(context: context, future: () => widget.room.leave());
                  if (success.error == null) {
                    context.go('/rooms');
                  }
                }
                break;
              case 'mute':
                await showFutureLoadingDialog(context: context, future: () => widget.room.setPushRuleState(PushRuleState.mentionsOnly));
                break;
              case 'unmute':
                await showFutureLoadingDialog(context: context, future: () => widget.room.setPushRuleState(PushRuleState.notify));
                break;
              case 'todos':
                context.go('/rooms/${widget.room.id}/tasks');
                break;
              case 'details':
                _showChatDetails();
                break;
            }
          },
          itemBuilder: (BuildContext context) => items,
        ),
      ],
    );
  }

  void _showWidgets() => [TargetPlatform.iOS, TargetPlatform.macOS].contains(Theme.of(context).platform)
      ? showCupertinoModalPopup(
          context: context,
          builder: (context) => CupertinoWidgetsBottomSheet(room: widget.room),
        )
      : showModalBottomSheet(
          context: context,
          builder: (context) => WidgetsBottomSheet(room: widget.room),
        );

  void _showChatDetails() {
    if (PageMeApp.router.routeInformationProvider.value.uri.path.endsWith('/details')) {
      context.go('/rooms/${widget.room.id}');
    } else {
      context.go('/rooms/${widget.room.id}/details');
    }
  }
}
