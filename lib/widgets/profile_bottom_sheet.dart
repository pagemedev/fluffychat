import 'dart:math';

import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';


import 'package:pageMe/config/themes.dart';
import 'package:pageMe/widgets/content_banner.dart';
import 'package:pageMe/widgets/matrix.dart';
import '../utils/localized_exception_extension.dart';

class ProfileBottomSheet extends StatelessWidget {
  final String userId;
  final BuildContext outerContext;
  const ProfileBottomSheet({
    required this.userId,
    required this.outerContext,
    Key? key,
  }) : super(key: key);

  void _startDirectChat(BuildContext context) async {
    final client = Matrix.of(context).client;
    final navigatorState = Navigator.of(context, rootNavigator: false);
    final result = await showFutureLoadingDialog<String>(
      context: context,
      future: () => client.startDirectChat(userId),
    );
    if (result.error == null) {
      context.go("/rooms/${result.result!}");
      navigatorState.pop();
      return;
    }
    Logs().e("_startDirectChat - result.error: ${result.error}");
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: min(MediaQuery.sizeOf(context).width, PageMeThemes.columnWidth * 1.5),
        child: Material(
          elevation: 4,
          child: SafeArea(
            child: Scaffold(
              extendBodyBehindAppBar: true,
              body: FutureBuilder<Profile>(
                  future: Matrix.of(context).client.getProfileFromUserId(userId),
                  builder: (context, snapshot) {
                    final profile = snapshot.data;
                    return Column(
                      children: [
                        Expanded(
                          child: profile == null
                              ? Container(
                                  alignment: Alignment.center,
                                  color: Theme.of(context).secondaryHeaderColor,
                                  child: snapshot.hasError ? Text(snapshot.error!.toLocalizedString(context)) : const CircularProgressIndicator.adaptive(strokeWidth: 2),
                                )
                              :  ContentBanner(
                                  mxContent: profile.avatarUrl,
                                  defaultIcon: Icons.account_circle_outlined,
                                  opacity: 1,
                                  enableViewer: profile.avatarUrl != null ?  true : false,
                                  client: Matrix.of(context).client,
                                  heroTag: 'profile-bottom-sheet',
                                ),
                        ),
                        ListTile(
                          title: Text(profile?.displayName ?? userId.localpart ?? ''),
                          subtitle: Text(userId),
                          trailing: const Icon(Icons.account_box_outlined),
                        ),
                        Container(
                          width: double.infinity,
                          padding: const EdgeInsets.all(12),
                          child: ElevatedButton.icon(
                            onPressed: () => _startDirectChat(context),
                            label: Text(L10n.of(context)!.newChat),
                            icon: const Icon(Icons.send_outlined),
                          ),
                        ),
                        const SizedBox(height: 8),
                      ],
                    );
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
