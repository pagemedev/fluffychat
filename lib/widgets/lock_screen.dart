import 'dart:async';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:lottie/lottie.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/widgets/app_lock.dart';

import '../config/themes.dart';


class LockScreen extends StatefulWidget {
  const LockScreen({super.key});

  @override
  State<LockScreen> createState() => _LockScreenState();
}

class _LockScreenState extends State<LockScreen> with SingleTickerProviderStateMixin {
  String? _errorText;
  int _coolDownSeconds = 5;
  bool _inputBlocked = false;
  late final AnimationController animationController;
  final TextEditingController _textEditingController = TextEditingController();

  void tryUnlock(BuildContext context) async {
    setState(() {
      _errorText = null;
    });
    if (_textEditingController.text.length < 4) return;

    final enteredPin = int.tryParse(_textEditingController.text);
    if (enteredPin == null || _textEditingController.text.length != 4) {
      setState(() {
        _errorText = "Invalid input!";
      });
      _textEditingController.clear();
      return;
    }

    if (AppLock.of(context).unlock(enteredPin.toString())) {
      await animationController.forward();
      setState(() {
        _inputBlocked = false;
        _errorText = null;
      });
      _textEditingController.clear();
      return;
    }

    setState(() {
      _errorText = 'Wrong pin entered! Try again in $_coolDownSeconds seconds...';
      _inputBlocked = true;
    });
    await Future.delayed(Duration(seconds: _coolDownSeconds)).then((_) {
      setState(() {
        _inputBlocked = false;
        _coolDownSeconds *= 2;
        _errorText = null;
      });
    });
    _textEditingController.clear();
  }

  @override
  void initState() {
    animationController = AnimationController(vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: AdaptiveTheme(
        light: isBusiness() ? PageMeThemes.lightMaterial : PageMeThemes.lightMaterialPublic,
        dark: isBusiness() ? PageMeThemes.darkMaterial : PageMeThemes.darkMaterialPublic,
        initial: AdaptiveThemeMode.system,
        builder: (theme, darkTheme) => MaterialApp(
          title: FlavorConfig.applicationName,
          themeMode: ThemeMode.system,
          theme: isBusiness() ? PageMeThemes.lightMaterial : PageMeThemes.lightMaterialPublic,
          darkTheme: isBusiness() ? PageMeThemes.darkMaterial : PageMeThemes.darkMaterialPublic,
          localizationsDelegates: L10n.localizationsDelegates,
          supportedLocales: L10n.supportedLocales,
          home: Builder(
            builder: (context) => Scaffold(
              appBar: AppBar(
                title: Text(L10n.of(context)!.pleaseEnterYourPin),
                centerTitle: true,
              ),
              extendBodyBehindAppBar: true,
              body: Center(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: PageMeThemes.columnWidth,
                    ),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 25.0),
                            child: SizedBox(
                              height: 150,
                              child: LottieBuilder.asset(
                                'assets/unlocked-animation-dark.json',
                                repeat: false,
                                addRepaintBoundary: true,
                                fit: BoxFit.fitHeight,
                                controller: animationController,
                                frameRate: FrameRate(60),
                                onLoaded: (composition) {
                                  animationController.duration = composition.duration;
                                },
                              ),
                            ),
                          ),
                        ),
                        TextField(
                          controller: _textEditingController,
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.number,
                          obscureText: true,
                          autofocus: true,
                          textAlign: TextAlign.center,
                          readOnly: _inputBlocked,
                          onChanged: (_) => tryUnlock(context),
                          onSubmitted: (_) => tryUnlock(context),
                          style: const TextStyle(fontSize: 40),
                          decoration: InputDecoration(
                            errorText: _errorText,
                            hintText: '****',
                          ),
                        ),
                        if (_inputBlocked)
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: LinearProgressIndicator(),
                          ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
