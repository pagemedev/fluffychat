import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:matrix/matrix.dart';

import '../../utils/platform_infos.dart';
import 'context_menu.dart';
import 'context_menu_builder.dart';

Future<void> showContextMenu(
    Offset offset,
    BuildContext context,
    ContextMenuBuilder builder,
    verticalPadding,
    width,
    ) async {
  await showModal(
    context: context,
    configuration: const FadeScaleTransitionConfiguration(
      barrierColor: Colors.transparent,
    ),
    builder: (context) => ContextMenu(
      position: offset,
      builder: builder,
      verticalPadding: verticalPadding,
      width: width,
    ),
  );
}

/// The [ContextMenuArea] is the way to use a [ContextMenu]
///
/// It listens for right click and long press and executes [showContextMenu]
/// with the corresponding location [Offset].

class CustomContextMenuArea extends StatefulWidget {
  /// The widget displayed inside the [ContextMenuArea]
  final Widget child;

  /// Builds a [List] of items to be displayed in an opened [ContextMenu]
  ///
  /// Usually, a [ListTile] might be the way to go.
  final ContextMenuBuilder builder;

  /// The padding value at the top an bottom between the edge of the [ContextMenu] and the first / last item
  final double verticalPadding;

  /// The width for the [ContextMenu]. 320 by default according to Material Design specs.
  final double width;

  const CustomContextMenuArea({
    Key? key,
    required this.child,
    required this.builder,
    this.verticalPadding = 8,
    this.width = 320,
  }) : super(key: key);

  @override
  State<CustomContextMenuArea> createState() => _CustomContextMenuAreaState();
}

class _CustomContextMenuAreaState extends State<CustomContextMenuArea> {


  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onExit: (_) async{
        if (PlatformInfos.isWeb) {
          //Logs().v("BrowserContextMenu - re-enabled");
          await BrowserContextMenu.enableContextMenu();
          //Logs().v("BrowserContextMenu - enabled: ${BrowserContextMenu.enabled}");
        }
      },
      onHover: (event) async{
        if (PlatformInfos.isWeb) {
          //Logs().v("BrowserContextMenu - temporarily disabled");
          await BrowserContextMenu.disableContextMenu();
          //Logs().v("BrowserContextMenu - enabled: ${BrowserContextMenu.enabled}");
        }
      },
      child: GestureDetector(
        onSecondaryTapDown: (details) async
        {
          if (!context.mounted) return;
          await showContextMenu(
            details.globalPosition,
            context,
            widget.builder,
            widget.verticalPadding,
            widget.width,
          );
        },
        child: widget.child,
      ),
    );
  }
}