import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';


import '../pages/new_chat/new_chat.dart';
import '../utils/platform_infos.dart';
import 'avatar.dart';
import 'matrix.dart';

class ProfileList extends StatelessWidget {
  final NewChatController controller;
  final bool neverScrollPhysics;

  const ProfileList({Key? key, required this.controller, this.neverScrollPhysics = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, size) {
        return ListView.builder(
          padding: EdgeInsets.zero,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          keyboardDismissBehavior: PlatformInfos.isIOS ? ScrollViewKeyboardDismissBehavior.onDrag : ScrollViewKeyboardDismissBehavior.manual,
          itemCount: controller.foundProfiles.length,
          itemBuilder: (BuildContext context, int i) {
            final foundProfile = controller.foundProfiles[i];
            return ListTile(
              onTap: () async {
                final roomID = await showFutureLoadingDialog(
                  context: context,
                  future: () async {
                    final client = Matrix.of(context).client;
                    final roomId = await client.startDirectChat(foundProfile.userId);
                    return roomId;
                  },
                );
                if (roomID.error == null) {
                  context.go('/rooms/${roomID.result!}');
                }
              },
              leading: Avatar(
                mxContent: foundProfile.avatarUrl,
                name: foundProfile.displayName ?? foundProfile.userId,
                //size: 24,
              ),
              title: Text(
                foundProfile.displayName ?? foundProfile.userId.localpart!,
                style: const TextStyle(),
                maxLines: 1,
              ),
              subtitle: Text(
                foundProfile.userId,
                maxLines: 1,
                style: const TextStyle(
                  fontSize: 12,
                ),
              ),
            );
          },
        );
      }
    );
  }
}
