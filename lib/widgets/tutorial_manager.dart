import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../config/flavor_config.dart';
import '../config/themes.dart';
import '../pageme_app.dart';
import '../pages/chat_list/navi_rail_item.dart';
import '../utils/global_key_provider.dart';
import 'matrix.dart';

class TutorialManager {
  final BuildContext context;
  final AsyncCallback? onFinish;
  late TutorialCoachMark tutorialCoachMark;

  late L10n? l10n;

  late GoRouteInformationProvider go;

  late ThemeData theme;

  late MatrixState matrixState;

  TutorialManager({required this.context, this.onFinish}) {
    matrixState = Matrix.of(context);
    theme = Theme.of(context);
    go = PageMeApp.router.routeInformationProvider;
    l10n = L10n.of(context);

    createTutorial();
  }

  void showTutorial() {
    try {
      Logs().i("[TutorialManager] - Starting tutorial");
      tutorialCoachMark.show(context: context, rootOverlay: true);

      //
    } catch (e) {
      Logs().e("showTutorial error: $e");
    }
  }

  void createTutorial() {
    try {
      Logs().i("[TutorialManager] - Creating tutorial");
      tutorialCoachMark = TutorialCoachMark(
        targets: _createTargets(),
        colorShadow: theme.colorScheme.primary,
        textSkip: "SKIP",
        alignSkip: Alignment.topRight,
        focusAnimationDuration: const Duration(milliseconds: 2000),
        paddingFocus: 10,
        opacityShadow: 0.3,
        hideSkip: false,
        imageFilter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
        onFinish: () async {
          if (onFinish != null) {
            await onFinish!();
          }
          if (go.value.uri.path != '/rooms/' || go.value.uri.path != '/rooms') {
            go.go("/rooms");
          }
        },
        onClickTarget: (target) {
          if (target.keyTarget == GlobalKeyProvider.menuButton) {
            (target.keyTarget?.currentWidget as IconButton).onPressed!();
            //widget.chatListController.scaffoldKey.currentState?.openDrawer();
          }
          if (target.identify == "settingsIcon") {
            (GlobalKeyProvider.settingsButton.currentWidget as NaviRailItem).onTap();
          }
          if (target.identify == "settingsAppearanceButton") {
            PageMeApp.router.go("/rooms");
          }
        },
        onClickTargetWithTapPosition: (target, tapDetails) {},
        onClickOverlay: (target) {
          if (target.identify == "menuButton") {
            (GlobalKeyProvider.menuButton.currentWidget as IconButton).onPressed!();
          }
          if (target.identify == "settingsIcon") {
            (GlobalKeyProvider.settingsButton.currentWidget as NaviRailItem).onTap();
          }
        },
        onSkip: () {
          //tutorialCoachMark.finish();
          if (go.value.uri.path != '/rooms/' || go.value.uri.path != '/rooms') {
            PageMeApp.router.go("/rooms");
          }
          return true;
        },
      );
      Logs().i("[TutorialManager] - Tutorial Created");
    } catch (e) {
      Logs().e("[TutorialManager] - Error occurred during creation: $e");
    }
  }

  List<TargetFocus> _createTargets() {
    final List<TargetFocus> targets = [];

    targets.add(
      TargetFocus(
        identify: "searchButton",
        alignSkip: Alignment.topCenter,
        keyTarget: GlobalKeyProvider.searchButton,
        enableOverlayTab: true,
        enableTargetTab: true,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  tutorialInstructionsContainer(
                    isBusiness()
                        ? "Use 'Search' to find public rooms, other users on the same server, and your existing chats."
                        : "Use 'Search' to find public rooms and look up existing chats you're part of.", context
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
    targets.add(
      TargetFocus(
        identify: "fabButton",
        keyTarget: GlobalKeyProvider.fabButton,
        alignSkip: Alignment.topCenter,
        shape: ShapeLightFocus.RRect,
        radius: FlavorConfig.borderRadius,
        enableOverlayTab: true,
        contents: [
          TargetContent(
            align: ContentAlign.top,
            builder: (context, controller) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: tutorialInstructionsContainer(isBusiness()
                    ? "Tap the 'New Chat' button to start a new chat or group.\n\nYou can also see a list of all users on your server."
                    : "Tap the 'New Chat' button to start a new chat or group.\n\nYou can also see a list of users you're already in rooms with.", context),
              );
            },
          ),
        ],
      ),
    );
    if (!PageMeThemes.isColumnMode(context)) {
      targets.add(
        TargetFocus(
          identify: "menuButton",
          keyTarget: GlobalKeyProvider.menuButton,
          alignSkip: Alignment.topCenter,
          enableOverlayTab: true,
          contents: [
            TargetContent(
              align: ContentAlign.bottom,
              builder: (context, controller) {
                return tutorialInstructionsContainer(
                    "Tap 'Menu' to open a sidebar.\n\nThis shows your 'Spaces', these are groups of chat rooms you're part of.\n\nScroll down to find extra options like 'Archive' and 'Settings'.", context);
              },
            ),
          ],
        ),
      );
    }
    targets.add(
      TargetFocus(
        identify: "settingsIcon",
        keyTarget: GlobalKeyProvider.settingsIcon,
        alignSkip: Alignment.topCenter,
        enableOverlayTab: true,
        contents: [
          TargetContent(
            align: ContentAlign.top,
            builder: (context, controller) {
              return tutorialInstructionsContainer("Tap 'Settings' for customization options.\n\nHere you can tweak how the app looks, manage notifications, and more.", context);
            },
          ),
        ],
      ),
    );
    targets.add(
      TargetFocus(
        identify: "settingsLogOutButton",
        keyTarget: GlobalKeyProvider.settingsLogOutButton,
        alignSkip: Alignment.topCenter,
        enableOverlayTab: true,
        shape: ShapeLightFocus.RRect,
        radius: FlavorConfig.borderRadius,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: tutorialInstructionsContainer(
                    isBusiness()
                        ? "Tap 'Logout' to be asked if you would like to sign out of your current account.\n\nAfter confirming, you'll be taken back to the server selection screen."
                        : "Tap 'Logout' to be asked if you would like to sign out of your current account.\n\nAfter confirming, you'll be redirected to the login screen.",
                    context),
              );
            },
          ),
        ],
      ),
    );
    targets.add(
      TargetFocus(
        identify: "settingsAppearanceButton",
        keyTarget: GlobalKeyProvider.settingsAppearanceButton,
        alignSkip: Alignment.topCenter,
        enableOverlayTab: true,
        paddingFocus: 5,
        shape: ShapeLightFocus.RRect,
        radius: FlavorConfig.borderRadius,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return tutorialInstructionsContainer("Tap 'Appearance' to change the app's look.\n\nChoose between Dark and Light mode to suit your preference.", context);
            },
          ),
        ],
      ),
    );

    return targets;
  }

  Widget tutorialInstructionsContainer(String tutorialStepInstructions, BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
        color: Theme.of(context).colorScheme.surface.withOpacity(0.8),
      ),
      child: Text(
        tutorialStepInstructions,
        style: TextStyle(color: Theme.of(context).colorScheme.onSurface, fontSize: 18),
      ),
    );
  }
}

/*
class TutorialManager extends StatefulWidget {
  final BuildContext context;
  final VoidCallback? onFinish;
  const TutorialManager({super.key, required this.context, this.onFinish});

  @override
  State<TutorialManager> createState() => _TutorialManagerState();
}
*/

/*
class _TutorialManagerState extends State<TutorialManager> {

  late TutorialCoachMark tutorialCoachMark;
  late NavigatorState navigator;

  late L10n? l10n;

  late InitializedVRouterSailor vrouter;

  late ThemeData theme;

  late MatrixState matrixState;

  @override
  void didChangeDependencies() {
    matrixState = Matrix.of(context);
    theme = Theme.of(context);
    vrouter = GoRouterState.of(context);
    l10n = L10n.of(context);
    navigator = Navigator.of(context);

    createTutorial();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    showTutorial();
    return const SizedBox.shrink();
  }

  void showTutorial() {
    tutorialCoachMark.show(context: context);
  }

  void createTutorial() {
    tutorialCoachMark = TutorialCoachMark(
      targets: _createTargets(),
      colorShadow: theme.colorScheme.primary,
      textSkip: "SKIP",
      alignSkip: Alignment.topRight,
      paddingFocus: 10,
      opacityShadow: 0.5,
      hideSkip: false,
      imageFilter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
      onFinish: () async {
        if (vrouter.path != '/rooms/' || vrouter.path != '/rooms') {
          context.go("/rooms");
        }

        if (widget.onFinish != null) {
          widget.onFinish!();
        }

      },
      onClickTarget: (target) {
        if (target.keyTarget == GlobalKeyProvider.menuButton) {
          (target.keyTarget?.currentWidget as IconButton).onPressed!();
          //widget.chatListController.scaffoldKey.currentState?.openDrawer();
        }
        if (target.identify == "settingsIcon") {
          (GlobalKeyProvider.settingsButton.currentWidget as NaviRailItem).onTap();
        }
        if (target.identify == "settingsAppearanceButton") {
          context.go("/rooms");
        }
      },
      onClickTargetWithTapPosition: (target, tapDetails) {},
      onClickOverlay: (target) {
        if (target.identify == "menuButton") {
          (GlobalKeyProvider.menuButton.currentWidget as IconButton).onPressed!();
        }
        if (target.identify == "settingsIcon") {
          (GlobalKeyProvider.settingsButton.currentWidget as NaviRailItem).onTap();
        }
      },
      onSkip: () {
        if (vrouter.path != '/rooms/' || vrouter.path != '/rooms') {
          context.go("/rooms");
        }
      },
    );
  }

  List<TargetFocus> _createTargets() {
    final List<TargetFocus> targets = [];

    targets.add(
      TargetFocus(
        identify: "searchButton",
        keyTarget: GlobalKeyProvider.searchButton,
        alignSkip: Alignment.topCenter,
        enableOverlayTab: true,
        enableTargetTab: true,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    isBusiness()
                        ? "Use 'Search' to find public rooms, other users on the same server, and your existing chats."
                        : "Use 'Search' to find public rooms and look up existing chats you're part of.",
                    style: const TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
    targets.add(
      TargetFocus(
        identify: "fabButton",
        keyTarget: GlobalKeyProvider.fabButton,
        alignSkip: Alignment.topCenter,
        enableOverlayTab: true,
        contents: [
          TargetContent(
            align: ContentAlign.top,
            builder: (context, controller) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      isBusiness()
                          ? "Tap the 'New Chat' button to start a new chat or group.\n\nYou can also see a list of all users on your server."
                          : "Tap the 'New Chat' button to start a new chat or group.\n\nYou can also see a list of users you're already in rooms with.",
                      style: const TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
    targets.add(
      TargetFocus(
        identify: "menuButton",
        keyTarget: GlobalKeyProvider.menuButton,
        alignSkip: Alignment.topCenter,
        enableOverlayTab: true,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return const Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Tap 'Menu' to open a sidebar.\n\nThis shows your 'Spaces', these are groups of chat rooms you're part of.\n\nScroll down to find extra options like 'Archive' and 'Settings'.",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
    targets.add(
      TargetFocus(
        identify: "settingsIcon",
        keyTarget: GlobalKeyProvider.settingsIcon,
        alignSkip: Alignment.topCenter,
        enableOverlayTab: true,
        contents: [
          TargetContent(
            align: ContentAlign.top,
            builder: (context, controller) {
              return const Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Tap 'Settings' for customization options.\n\nHere you can tweak how the app looks, manage notifications, and more.",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
    targets.add(
      TargetFocus(
        identify: "settingsLogOutButton",
        keyTarget: GlobalKeyProvider.settingsLogOutButton,
        alignSkip: Alignment.topCenter,
        enableOverlayTab: true,
        shape: ShapeLightFocus.RRect,
        radius: FlavorConfig.borderRadius,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      isBusiness()
                          ? "Tap 'Logout' to be asked if you would like to sign out of your current account.\n\nAfter confirming, you'll be taken back to the server selection screen."
                          : "Tap 'Logout' to be asked if you would like to sign out of your current account.\n\nAfter confirming, you'll be redirected to the login screen.",
                      style: const TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
    targets.add(
      TargetFocus(
        identify: "settingsAppearanceButton",
        keyTarget: GlobalKeyProvider.settingsAppearanceButton,
        alignSkip: Alignment.topCenter,
        enableOverlayTab: true,
        paddingFocus: 5,
        shape: ShapeLightFocus.RRect,
        radius: FlavorConfig.borderRadius,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return const Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Tap 'Appearance' to change the app's look.\n\nChoose between Dark and Light mode to suit your preference.",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );

    return targets;
  }
}
*/
