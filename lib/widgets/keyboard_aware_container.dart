import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/logger_functions.dart';


class KeyboardAwareContainer extends StatefulWidget {
  final double minHeight;
  final double maxHeight;
  final Widget? child;
  const KeyboardAwareContainer({Key? key, required this.maxHeight, required this.minHeight, this.child}) : super(key: key);

  @override
  KeyboardAwareContainerState createState() => KeyboardAwareContainerState();
}

class KeyboardAwareContainerState extends State<KeyboardAwareContainer> {

  @override
  Widget build(BuildContext context) {
    return KeyboardVisibilityBuilder(
        builder: (context, isKeyboardVisible) {
          return AnimatedContainer(
            duration: const Duration(milliseconds: 300),
            height: isKeyboardVisible ? widget.minHeight : widget.maxHeight,
            child: widget.child,
          );
        });
  }


}
