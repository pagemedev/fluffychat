import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/client_presence_extension.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/presence_extension.dart';
import 'package:pageMe/widgets/avatar.dart';



import 'matrix.dart';

class ContactsList extends StatefulWidget {
  final TextEditingController searchController;
  final bool neverScrollPhysics;

  const ContactsList({Key? key, required this.searchController, this.neverScrollPhysics = false}) : super(key: key);
  @override
  ContactsState createState() => ContactsState();
}

class ContactsState extends State<ContactsList> {
  StreamSubscription? _onSync;

  @override
  Future<void> dispose() async {
    super.dispose();
    await _onSync?.cancel();
  }

  DateTime _lastSetState = DateTime.now();
  Timer? _coolDown;

  void _updateView() {
    _lastSetState = DateTime.now();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    _onSync ??= client.onSync.stream.listen((_) {
      if (DateTime.now().millisecondsSinceEpoch - _lastSetState.millisecondsSinceEpoch < 1000) {
        _coolDown?.cancel();
        _coolDown = Timer(const Duration(seconds: 1), _updateView);
      } else {
        _updateView();
      }
    });
    final List<CachedPresence> searchContactList =
        client.contactList.where((p) => p.userid.toLowerCase().contains(widget.searchController.text.toLowerCase())).toList();

    if (searchContactList.isEmpty) {
      return const Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircularProgressIndicator(),
        ],
      );
    } else {
      return ListView.builder(
        padding: EdgeInsets.zero,
        physics: (widget.neverScrollPhysics) ? const NeverScrollableScrollPhysics() : const ScrollPhysics(),
        shrinkWrap: true,
        itemCount: searchContactList.length,
        itemBuilder: (_, i) => _ContactListTile(contact: searchContactList[i]),
      );
    }
  }
}

class _ContactListTile extends StatelessWidget {
  final CachedPresence contact;

  const _ContactListTile({Key? key, required this.contact}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Profile>(
        future: Matrix.of(context).client.getProfileFromUserId(contact.userid),
        builder: (context, snapshot) {
          final displayName = snapshot.data?.displayName ?? contact.userid.localpart ?? 'No valid MXID';
          final avatarUrl = snapshot.data?.avatarUrl;
          return ListTile(
            leading: SizedBox(
              width: Avatar.defaultSize,
              height: Avatar.defaultSize,
              child: Stack(
                children: [
                  Center(
                    child: Avatar(
                      mxContent: avatarUrl,
                      name: displayName,
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Icon(
                      Icons.circle,
                      color: contact.color,
                      size: 12,
                    ),
                  ),
                ],
              ),
            ),
            title: Text(displayName),
/*            subtitle: Text(contact.getLocalizedStatusMessage(context),
                style: contact.statusMsg?.isNotEmpty ?? false
                    ? TextStyle(
                        color: Theme.of(context).colorScheme.secondary,
                        fontWeight: FontWeight.bold,
                      )
                    : null)*/
            onTap: () => context.go('/rooms/${Matrix.of(context).client.getDirectChatFromUserId(contact.userid)!}'),
          );
        });
  }
}
