import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:matrix_link_text/link_text.dart';

import 'package:pageMe/config/themes.dart';
import 'package:pageMe/widgets/content_banner.dart';
import 'package:pageMe/widgets/matrix.dart';
import '../utils/localized_exception_extension.dart';
import '../utils/url_launcher.dart';

class PublicRoomBottomSheet extends StatefulWidget {
  final String? roomAlias;
  final BuildContext outerContext;
  final PublicRoomsChunk? chunk;
  final VoidCallback? onRoomJoined;

  PublicRoomBottomSheet({
    this.roomAlias,
    required this.outerContext,
    this.chunk,
    this.onRoomJoined,
    Key? key,
  }) : super(key: key) {
    assert(roomAlias != null || chunk != null);
  }

  @override
  State<PublicRoomBottomSheet> createState() => _PublicRoomBottomSheetState();
}

class _PublicRoomBottomSheetState extends State<PublicRoomBottomSheet> {
  late Client client;
  late NavigatorState navigator;
  late L10n? l10n;

  @override
  void didChangeDependencies() {
    client  = Matrix.of(context).client;
    navigator = Navigator.of(context);
    l10n = L10n.of(context);
    super.didChangeDependencies();
  }

  void _joinRoom(BuildContext context) async {
    final result = await showFutureLoadingDialog<String>(
      context: context,
      future: () => widget.chunk?.joinRule == 'knock'
          ? client.knockRoom(widget.chunk!.roomId)
          : client.joinRoom(widget.roomAlias ?? widget.chunk!.roomId),
    );
    if (result.error == null) {
      if (client.getRoomById(result.result!) == null) {
        await client.onSync.stream.firstWhere(
            (sync) => sync.rooms?.join?.containsKey(result.result) ?? false);
      }
      // don't open the room if the joined room is a space
      if (!client.getRoomById(result.result!)!.isSpace) {
        context.go("/rooms/${result.result!}");
      }
      navigator.pop();
      return;
    }
  }

  bool _testRoom(PublicRoomsChunk r) => r.canonicalAlias == widget.roomAlias;

  Future<PublicRoomsChunk> _search(BuildContext context) async {
    final chunk = widget.chunk;
    if (chunk != null) return chunk;
    final query = await Matrix.of(context).client.queryPublicRooms(
          server: widget.roomAlias!.domain,
          filter: PublicRoomQueryFilter(
            genericSearchTerm: widget.roomAlias,
          ),
        );
    if (!query.chunk.any(_testRoom)) {
      throw (l10n!.noRoomsFound);
    }
    return query.chunk.firstWhere(_testRoom);
  }

  @override
  Widget build(BuildContext context) {
    final roomAlias = widget.roomAlias;
    return Center(
      child: SizedBox(
        width: min(
            MediaQuery.sizeOf(context).width, PageMeThemes.columnWidth * 1.5),
        child: Material(
          elevation: 4,
          child: SafeArea(
            child: Scaffold(
              extendBodyBehindAppBar: true,
              appBar: AppBar(
                elevation: 0,
                titleSpacing: 0,
                backgroundColor:
                    Theme.of(context).scaffoldBackgroundColor.withOpacity(0.5),
                title: Text(
                  roomAlias ?? widget.chunk!.name ?? widget.chunk!.roomId,
                  overflow: TextOverflow.fade,
                ),
                leading: IconButton(
                  icon: const Icon(Icons.arrow_downward_outlined),
                  onPressed: Navigator.of(context, rootNavigator: false).pop,
                  tooltip: L10n.of(context)!.close,
                ),
                actions: [
                  TextButton.icon(
                    onPressed: () => _joinRoom(context),
                    label: Text(L10n.of(context)!.joinRoom),
                    icon: const Icon(Icons.login_outlined),
                  ),
                ],
              ),
              body: FutureBuilder<PublicRoomsChunk>(
                  future: _search(context),
                  builder: (context, snapshot) {
                    final profile = snapshot.data;
                    return ListView(
                      padding: EdgeInsets.zero,
                      children: [
                        if (profile == null)
                          Container(
                            height: 156,
                            alignment: Alignment.center,
                            color: Theme.of(context).secondaryHeaderColor,
                            child: snapshot.hasError
                                ? Text(
                                    snapshot.error!.toLocalizedString(context))
                                : const CircularProgressIndicator.adaptive(
                                    strokeWidth: 2),
                          )
                        else
                          ContentBanner(
                            mxContent: profile.avatarUrl,
                            height: 156,
                            defaultIcon: Icons.group_outlined,
                            client: Matrix.of(context).client,
                            heroTag: 'public-room-bottom-sheet',
                          ),
                        ListTile(
                          title: Text(profile?.name ??
                              roomAlias?.localpart ??
                              widget.chunk!.roomId.localpart ??
                              ''),
                          subtitle: Text(
                            '${L10n.of(context)!.participant}: ${profile?.numJoinedMembers ?? 0}',
                          ),
                          trailing: const Icon(Icons.account_box_outlined),
                        ),
                        if (profile?.topic?.isNotEmpty ?? false)
                          ListTile(
                            title: Text(
                              L10n.of(context)!.groupDescription,
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.secondary,
                              ),
                            ),
                            subtitle: LinkText(
                              text: profile!.topic!,
                              linkStyle:
                                  const TextStyle(color: Colors.blueAccent),
                              textStyle: TextStyle(
                                fontSize: 14,
                                color: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .color,
                              ),
                              onLinkTap: (url) async =>
                                  UrlLauncher(context, url.toString()).launchUrl(),
                            ),
                          ),
                      ],
                    );
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
