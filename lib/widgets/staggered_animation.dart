import 'package:flutter/material.dart';

class StaggeredAnimation extends StatefulWidget {
  final Widget firstChild;
  final Widget secondChild;
  final bool forwardCondition;
  final bool reverseCondition;
  const StaggeredAnimation({Key? key, required this.firstChild, required this.secondChild, required this.forwardCondition, required this.reverseCondition}) : super(key: key);

  @override
  StaggeredAnimationState createState() => StaggeredAnimationState();
}

class StaggeredAnimationState extends State<StaggeredAnimation> with SingleTickerProviderStateMixin {
  late final AnimationController _controller;
  late final Animation<Offset> _offsetAnimation1;
  late final Animation<Offset> _offsetAnimation2;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 600),
      vsync: this,
    );

    _offsetAnimation1 = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(0, 1.1),
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.0, 0.5, curve: Curves.easeInOut),
      ),
    );

    _offsetAnimation2 = Tween<Offset>(
      begin: const Offset(0, 1.1),
      end: Offset.zero,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.5, 1.0, curve: Curves.easeInOut),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.forwardCondition){
      _controller.forward();
    }
    if (widget.reverseCondition){
      _controller.reverse();
    }
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SlideTransition(
            position: _offsetAnimation1,
            child: widget.firstChild,
          ),
          SlideTransition(
            position: _offsetAnimation2,
            child: widget.secondChild,
          ),
        ],
      ),
    );
  }
}
