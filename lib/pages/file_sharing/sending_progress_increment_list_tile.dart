import 'package:flutter/material.dart';

class SendingProgressIncrementListTile extends StatelessWidget {
  const SendingProgressIncrementListTile({
    super.key,
    required TextStyle progressStyle,
    required this.currentProgress,
    required this.percentage,
  }) : _progressStyle = progressStyle;

  final TextStyle _progressStyle;
  final String currentProgress;
  final double? percentage;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      visualDensity: const VisualDensity(vertical: -4),
      minLeadingWidth: 5,
      minVerticalPadding: 0,
      leading: SizedBox.square(
        dimension: 16,
        child: CircularProgressIndicator(
          value: percentage,
          strokeWidth: 2,
        ),
      ),
      title: Text(currentProgress, style: _progressStyle),
      dense: true,
    );
  }
}
