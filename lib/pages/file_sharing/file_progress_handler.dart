import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../cubits/file_handler/file_handler_bloc.dart';
import 'file_progress_increment_list_tile.dart';

class FileProgressHandler extends StatelessWidget {
  const FileProgressHandler({
    super.key,
    required TextStyle progressStyle,
  }) : _progressStyle = progressStyle;

  final TextStyle _progressStyle;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<FileHandlerBloc, FileHandlerState>(
        builder: (context, fileHandlerState) {
          switch (fileHandlerState.processingStatus) {
            case ProcessingStatus.idle:
              return Container();
            case ProcessingStatus.initial:
              return Container();
            case ProcessingStatus.selecting:
              return FileProgressIncrementListTile(progressStyle: _progressStyle, currentProgress: 'Selection in progress...');
            case ProcessingStatus.processing:
              return FileProgressIncrementListTile(progressStyle: _progressStyle, currentProgress: 'Processing files...');
            case ProcessingStatus.compressing:
              return FileProgressIncrementListTile(progressStyle: _progressStyle, currentProgress: 'Generating previews...');
            case ProcessingStatus.ready:
              return Container();
            case ProcessingStatus.failure:
              return FileProgressIncrementListTile(progressStyle: _progressStyle, currentProgress: 'An error occurred: ${fileHandlerState.errorMessage ?? 'Unknown'}');
            case ProcessingStatus.cancelled:
              return Container();
            default:
              return Container();
          }
        },
      ),
    );
  }
}
