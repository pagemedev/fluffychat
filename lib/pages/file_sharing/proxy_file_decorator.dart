import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:pageMe/utils/string_color.dart';
import 'package:thumbnailer/thumbnailer.dart';
import '../../cubits/file_handler/file_handler_bloc.dart';
import '../../utils/platform_infos.dart';

class ProxyFileDecorator extends StatelessWidget {
  final double maxHeight;
  final Widget child;
  final int index;
  final Animation<double> animation;
  final FileState fileState;
  const ProxyFileDecorator({
    super.key,
    required this.child,
    required this.index,
    required this.animation,
    required this.fileState,
    required this.maxHeight,
  });

  @override
  Widget build(BuildContext context) {
    if (PlatformInfos.isMobile) {
      return _ProxyFileDecoratorMobile(
        index: index,
        animation: animation,
        fileState: fileState,
        maxHeight: maxHeight,
        key: key,
        child: child,
      );
    } else {
      return _ProxyFileDecoratorDesktop(
        index: index,
        maxHeight: maxHeight,
        animation: animation,
        fileState: fileState,
        key: key,
        child: child,
      );
    }
  }
}

class _ProxyFileDecoratorMobile extends StatelessWidget {
  final double maxHeight;
  final Widget child;
  final int index;
  final Animation<double> animation;
  final FileState fileState;
  const _ProxyFileDecoratorMobile({
    super.key,
    required this.child,
    required this.index,
    required this.animation,
    required this.fileState,
    required this.maxHeight,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget? child) {
        final double animValue = Curves.easeInOut.transform(animation.value);
        final double elevation = lerpDouble(0, 6, animValue)!;
        return SizedBox(
          height: maxHeight,
          child: Material(
            borderRadius: BorderRadius.circular(4),
            elevation: 4,
            color: Theme.of(context).colorScheme.surface,
            child: SizedBox.square(
              dimension: 124,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(
                    height: 18,
                  ),
                  Expanded(
                    child: Center(
                      child: Icon(
                        Thumbnailer.getIconDataForMimeType(fileState.file.mimeType) ?? Icons.file_copy,
                        size: 45,
                        color: fileState.file.mimeType.lightColor,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 6.0),
                    child: SizedBox(
                      height: 28,
                      child: GridTileBar(
                        title: Text(
                          fileState.file.name,
                          style: TextStyle(fontSize: 10, color: Theme.of(context).colorScheme.onSurface),
                        ),
                        subtitle: Text(
                          determineSizeFormat(),
                          style: TextStyle(fontSize: 10, color: Theme.of(context).colorScheme.onSurface),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
      child: child,
    );
  }

  String determineSizeFormat() {
    final size = fileState.file.size;
    if (size < 1000) {
      return "${size.toStringAsFixed(2)} B";
    } else if (size > 1000 && size < (1000 * 1000)) {
      return "${(size / 1000).toStringAsFixed(2)} KB";
    } else if (size > (1000 * 1000) && size < (1000 * 1000 * 200)) {
      return "${(size / (1000 * 1000)).toStringAsFixed(2)} MB";
    } else {
      return "File too large";
    }
  }

}

class _ProxyFileDecoratorDesktop extends StatelessWidget {
  final double maxHeight;
  final Widget child;
  final int index;
  final Animation<double> animation;
  final FileState fileState;
  const _ProxyFileDecoratorDesktop({
    super.key,
    required this.child,
    required this.index,
    required this.animation,
    required this.fileState,
    required this.maxHeight,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget? child) {
        final double animValue = Curves.easeInOut.transform(animation.value);
        final double elevation = lerpDouble(0, 6, animValue)!;
        return Stack(
          alignment: Alignment.center,
          children: [
            Hero(
              tag: fileState.thumbnailFile?.bytes.toString() ?? '',
              child: Material(
                borderRadius: BorderRadius.circular(4),
                elevation: 4,
                color: Theme.of(context).colorScheme.surface,
                child: SizedBox.square(
                  dimension: 124,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const SizedBox(
                        height: 18,
                      ),
                      Expanded(
                        child: Center(
                          child: Icon(
                            Thumbnailer.getIconDataForMimeType(fileState.file.mimeType) ?? Icons.file_copy,
                            size: 45,
                            color: fileState.file.mimeType.lightColor,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 6.0),
                        child: SizedBox(
                          height: 28,
                          child: GridTileBar(
                            title: Text(
                              fileState.file.name,
                              style: TextStyle(fontSize: 10, color: Theme.of(context).colorScheme.onSurface),
                            ),
                            subtitle: Text(
                              determineSizeFormat(),
                              style: TextStyle(fontSize: 10, color: Theme.of(context).colorScheme.onSurface),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        );
      },
      child: child,
    );
  }

  String determineSizeFormat() {
    final size = fileState.file.size;
    if (size < 1000) {
      return "${size.toStringAsFixed(2)} B";
    } else if (size > 1000 && size < (1000 * 1000)) {
      return "${(size / 1000).toStringAsFixed(2)} KB";
    } else if (size > (1000 * 1000) && size < (1000 * 1000 * 200)) {
      return "${(size / (1000 * 1000)).toStringAsFixed(2)} MB";
    } else {
      return "File too large";
    }
  }
}
