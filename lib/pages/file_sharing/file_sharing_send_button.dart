import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import '../../../config/flavor_config.dart';
import '../../../config/themes.dart';
import '../../../cubits/file_handler/file_handler_bloc.dart';
import '../../../cubits/sending_handler/sending_handler_bloc.dart';
import '../../../utils/platform_infos.dart';
import '../chat_list/chat_list.dart';

class FileSharingSendButton extends StatelessWidget {
  final ChatListController controller;
  const FileSharingSendButton({
    Key? key,
    required this.controller,
  }) : super(key: key);

  bool isReadyToSendFile(bool isReadyWithFiles) {
    return isReadyWithFiles;
  }

  BorderRadius _getBorderRadius() {
    return BorderRadius.circular(14);
  }

  void requestSendFileEvent(FileHandlerState state, BuildContext context) {
    final ChatListController controller = ChatList.of(context)!;
    if (!isReadyToSendFile(state.isReadyWithFiles)) {
      return;
    }

    if (state.isOnlyImageState && state.filesState is List<ImageState>) {
      Logs().v("state.isOnlyImageState && state.filesState is List<ImageState>");
      BlocProvider.of<SendingHandlerBloc>(context).add(
        SendImagesRequested(
          images: state.filesState as List<ImageState>,
          roomIds: controller.selectedShareRoomIds.toList(),
        ),
      );
    } else if (state.isOnlyShareEventState) {
      Logs().v("state.isOnlyShareEventState");
      BlocProvider.of<SendingHandlerBloc>(context).add(
        SendShareEventRequested(
          roomIds: controller.selectedShareRoomIds.toList(),
          shareEventState: state.shareEventState,
        ),
      );
    } else {
      BlocProvider.of<SendingHandlerBloc>(context).add(
        SendFilesRequested(
          files: state.filesState,
          roomIds: controller.selectedShareRoomIds.toList(),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileHandlerBloc, FileHandlerState>(
      builder: (context, state) {
        return BlocBuilder<SendingHandlerBloc, SendingHandlerState>(
          builder: (context, sendState) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4.0),
              child: Material(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  side: isBusiness() ? BorderSide.none : const BorderSide(color: Colors.black, width: 0.3),
                  borderRadius: _getBorderRadius(),
                ),
                shadowColor: Colors.black,
                clipBehavior: Clip.hardEdge,
                child: AnimatedContainer(
                    height: 48,
                    width: PlatformInfos.isDesktop
                        ? state.isReadyWithFiles
                            ? 48
                            : 0
                        : 48,
                    alignment: Alignment.center,
                    curve: Curves.fastLinearToSlowEaseIn,
                    decoration: BoxDecoration(
                      borderRadius: _getBorderRadius(),
                      color: (sendState.isSending && !sendState.isError)
                          ? Theme.of(context).colorScheme.tertiaryContainer
                          : controller.selectedShareRoomIds.isNotEmpty
                              ? PageMeThemes.isDarkMode(context)
                                  ? Theme.of(context).colorScheme.primary
                                  : Theme.of(context).colorScheme.tertiary
                              : Colors.grey,
                    ),
                    duration: const Duration(milliseconds: 300),
                    child: IconButton(
                      tooltip: (sendState.isSending && !sendState.isError) ? L10n.of(context)!.cancel : L10n.of(context)!.send,
                      icon: (sendState.isSending && !sendState.isError)
                          ? const Icon(
                              Icons.close,
                              color: Colors.redAccent,
                            )
                          : Icon(controller.selectedShareRoomIds.isNotEmpty ? Icons.send : Icons.send_outlined ),
                      color: controller.selectedShareRoomIds.isNotEmpty
                          ? PageMeThemes.isDarkMode(context)
                              ? Theme.of(context).colorScheme.onPrimary
                              : Theme.of(context).colorScheme.onTertiary
                          : PageMeThemes.isDarkMode(context)
                              ? Theme.of(context).colorScheme.onSecondary
                              : Theme.of(context).colorScheme.onTertiary,
                      onPressed: () => (sendState.isSending && !sendState.isError)
                          ? BlocProvider.of<SendingHandlerBloc>(context).add(const CancelSendEvent())
                          : controller.selectedShareRoomIds.isNotEmpty
                              ? requestSendFileEvent(state, context)
                              : {},
                    )),
              ),
            );
          },
        );
      },
    );
  }
}
