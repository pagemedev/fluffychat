import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:matrix/matrix.dart';
import 'package:thumbnailer/thumbnailer.dart';

import '../../cubits/file_handler/file_handler_bloc.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/matrix.dart';
import '../../widgets/swipable_vertical.dart';
import '../image_viewer/image_viewer_bytes_view.dart';

class VideoContentItem extends StatelessWidget {
  final double maxHeight;
  final VideoState videoState;
  final int index;
  const VideoContentItem({super.key, required this.maxHeight, required this.videoState, required this.index});

  @override
  Widget build(BuildContext context) {
    if (PlatformInfos.isMobile) {
      return _VideoContentItemMobile(
        maxHeight: maxHeight,
        videoState: videoState,
        index: index,
        key: key,
      );
    } else {
      return _VideoContentItemDesktop(
        maxHeight: maxHeight,
        videoState: videoState,
        index: index,
        key: key,
      );
    }
  }
}

class _VideoContentItemMobile extends StatelessWidget {
  final double maxHeight;
  final VideoState videoState;
  final int index;
  const _VideoContentItemMobile({super.key, required this.maxHeight, required this.videoState, required this.index});

  void _onTap(BuildContext context, Uint8List image) {
    showDialog(
      context: Matrix.of(context).navigatorContext,
      useRootNavigator: false,
      builder: (_) => ImageViewerBytesView(image: image),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double aspectRatio = videoState.thumbnailFile!.width! / (videoState.thumbnailFile!.height as int);
    final double width = (maxHeight * aspectRatio);
    return Padding(
      key: key, // add this line,
      padding: const EdgeInsets.only(right: 12),
      child: SizedBox(
        width: width,
        height: maxHeight,
        child: Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          children: [
            SwipeableVertical(
              childSize: Size(width, maxHeight),
              onSwipeEnd: () => BlocProvider.of<FileHandlerBloc>(context).add(
                FileRemoveRequested(removeAtIndex: index),
              ),
              threshold: maxHeight,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: GestureDetector(
                  //onLongPress: () => onLongPress(imageIndex),
                  onTap: () => _onTap(context, videoState.videoFile.bytes),
                  child: Image.memory(
                    videoState.thumbnailFile!.bytes,
                    fit: BoxFit.cover,
                    height: maxHeight,
                    width: width,
                    errorBuilder: (context, object, stack) {
                      Logs().e("Error occurred displaying image or thumbnail file, using Thumbnailer as fallback, $stack");
                      return Thumbnail(
                        mimeType: videoState.videoFile.mimeType,
                        dataResolver: () async => Future.delayed(Duration.zero).then((value) => videoState.videoFile.bytes),
                        widgetSize: 124,
                        decoration: WidgetDecoration(
                          backgroundColor: Colors.grey,
                          iconColor: Colors.blue,
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _VideoContentItemDesktop extends StatelessWidget {
  final double maxHeight;
  final VideoState videoState;
  final int index;
  const _VideoContentItemDesktop({super.key, required this.maxHeight, required this.videoState, required this.index});

  void _onTap(BuildContext context, Uint8List image) {
    showDialog(
      context: Matrix.of(context).navigatorContext,
      useRootNavigator: false,
      builder: (_) => ImageViewerBytesView(image: image),
    );
  }

  void onExit(int index, BuildContext context) {
    BlocProvider.of<FileHandlerBloc>(context).add(FileFocusRemoveRequested(removeFocusOnIndex: index));
  }

  void onEnter(int index, BuildContext context) {
    BlocProvider.of<FileHandlerBloc>(context).add(FileFocusRequested(focusOnIndex: index));
  }

  void onLongPress(int index, BuildContext context) {
    BlocProvider.of<FileHandlerBloc>(context).add(FileFocusRequested(focusOnIndex: index));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      key: key,
      padding: const EdgeInsets.only(right: 12),
      child: Material(
        clipBehavior: Clip.hardEdge,
        borderRadius: BorderRadius.circular(4),
        color: Colors.yellow,
        elevation: 4,
        child: GestureDetector(
          onTap: () => _onTap(context, videoState.videoFile.bytes),
          child: MouseRegion(
            onEnter: (_) => onEnter(index, context),
            onExit: (_) => onExit(index, context),
            onHover: (_) => onEnter(index, context),
            child: Stack(
              alignment: Alignment.center,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(4),
                  child: Image.memory(
                    videoState.thumbnailFile!.bytes,
                    isAntiAlias: true,
                    filterQuality: FilterQuality.high,
                    errorBuilder: (context, object, stack) {
                      Logs().e("Error occurred displaying image or thumbnail file, using Thumbnailer as fallback, $stack");
                      return Thumbnail(
                        mimeType: videoState.videoFile.mimeType,
                        //dataSize: state.filesState[filesIndex].file.size,
                        dataResolver: () => Future.delayed(Duration.zero).then((value) => videoState.videoFile.bytes),
                        //name: state.filesState[filesIndex].file.name,
                        widgetSize: 124,
                        decoration: WidgetDecoration(
                          backgroundColor: Colors.grey,
                          iconColor: Colors.blue,
                        ),
                      );
                    },
                  ),
                ),
                Positioned(
                  top: -10,
                  right: -10,
                  child: AnimatedOpacity(
                    duration: const Duration(milliseconds: 375),
                    curve: Curves.easeInCubic,
                    opacity: videoState.isFocused ? 0.9 : 0,
                    child: IconButton(
                      style: IconButton.styleFrom(
                        elevation: 4,
                      ),
                      onPressed: () => BlocProvider.of<FileHandlerBloc>(context).add(FileRemoveRequested(removeAtIndex: index)),
                      icon: Container(
                        decoration: BoxDecoration(color: Colors.redAccent, borderRadius: BorderRadius.circular(40)),
                        child: const Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Icon(
                            Icons.close,
                            size: 10,
                          ),
                        ),
                      ),
                      hoverColor: Colors.transparent,
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  child: AnimatedOpacity(
                    duration: const Duration(milliseconds: 375),
                    curve: Curves.easeInCubic,
                    opacity: videoState.isFocused ? 0.9 : 0,
                    child: ReorderableDragStartListener(
                      key: ValueKey(index),
                      index: index,
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(color: Colors.black54, borderRadius: BorderRadius.circular(4)),
                          child: const Padding(
                            padding: EdgeInsets.symmetric(vertical: 2),
                            child: Icon(
                              Icons.drag_indicator,
                              size: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
