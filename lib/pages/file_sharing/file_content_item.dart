import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pageMe/utils/string_color.dart';
import 'package:thumbnailer/thumbnailer.dart';

import '../../cubits/file_handler/file_handler_bloc.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/swipable_vertical.dart';

class FileContentItem extends StatelessWidget {
  final double maxHeight;
  final FileHandlerState fileHandlerState;
  final int filesIndex;
  const FileContentItem({
    super.key,
    this.maxHeight = 148,
    required this.fileHandlerState,
    required this.filesIndex,
  });

  @override
  Widget build(BuildContext context) {
    if (PlatformInfos.isMobile) {
      return _FileContentItemMobile(
        fileHandlerState: fileHandlerState,
        filesIndex: filesIndex,
        maxHeight: maxHeight,
        key: key,
      );
    } else {
      return _FileContentItemDesktop(
        maxHeight: maxHeight,
        fileHandlerState: fileHandlerState,
        filesIndex: filesIndex,
        key: key,
      );
    }
  }
}

class _FileContentItemMobile extends StatelessWidget {
  final double maxHeight;
  final FileHandlerState fileHandlerState;
  final int filesIndex;
  const _FileContentItemMobile({super.key, required this.fileHandlerState, required this.filesIndex, required this.maxHeight});

  String determineSizeFormat() {
    final size = fileHandlerState.filesState[filesIndex].file.size;
    if (size < 1000) {
      return "${size.toStringAsFixed(2)} B";
    } else if (size > 1000 && size < (1000 * 1000)) {
      return "${(size / 1000).toStringAsFixed(2)} KB";
    } else if (size > (1000 * 1000) && size < (1000 * 1000 * 200)) {
      return "${(size / (1000 * 1000)).toStringAsFixed(2)} MB";
    } else {
      return "File too large";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      key: key, // add this line,
      padding: const EdgeInsets.only(right: 12),
      child: SizedBox(
        height: maxHeight,
        child: Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          children: [
            SwipeableVertical(
              childSize: Size(100, maxHeight),
              onSwipeEnd: () => BlocProvider.of<FileHandlerBloc>(context).add(
                FileRemoveRequested(removeAtIndex: filesIndex),
              ),
              threshold: maxHeight,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: GestureDetector(
                  child: Hero(
                    tag: fileHandlerState.filesState[filesIndex].thumbnailFile?.bytes.toString() ?? '',
                    child: Material(
                      borderRadius: BorderRadius.circular(4),
                      elevation: 4,
                      color: Theme.of(context).colorScheme.surface,
                      child: SizedBox.square(
                        dimension: 124,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const SizedBox(
                              height: 18,
                            ),
                            Expanded(
                              child: Center(
                                child: Icon(
                                  Thumbnailer.getIconDataForMimeType(fileHandlerState.filesState[filesIndex].file.mimeType) ?? Icons.file_copy,
                                  size: 45,
                                  color: fileHandlerState.filesState[filesIndex].file.mimeType.lightColor,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 6.0),
                              child: SizedBox(
                                height: 24,
                                child: GridTileBar(
                                  title:
                                      Text(fileHandlerState.filesState[filesIndex].file.name, style: TextStyle(fontSize: 10, color: Theme.of(context).colorScheme.onSurface)),
                                  subtitle: Text(
                                    determineSizeFormat(),
                                    style: TextStyle(fontSize: 10, color: Theme.of(context).colorScheme.onSurface),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _FileContentItemDesktop extends StatelessWidget {
  final double maxHeight;
  final FileHandlerState fileHandlerState;
  final int filesIndex;

  const _FileContentItemDesktop({super.key, required this.maxHeight, required this.fileHandlerState, required this.filesIndex});

  void onExit(int index, BuildContext context) {
    BlocProvider.of<FileHandlerBloc>(context).add(FileFocusRemoveRequested(removeFocusOnIndex: index));
  }

  void onEnter(int index, BuildContext context) {
    BlocProvider.of<FileHandlerBloc>(context).add(FileFocusRequested(focusOnIndex: index));
  }

  void onLongPress(int index, BuildContext context) {
    BlocProvider.of<FileHandlerBloc>(context).add(FileFocusRequested(focusOnIndex: index));
  }

  void _onTap(BuildContext context, Uint8List file) {}

  @override
  Widget build(BuildContext context) {
    return Padding(
      key: key,
      padding: const EdgeInsets.only(right: 12),
      child: Material(
        borderRadius: BorderRadius.circular(4),
        elevation: 4,
        child: GestureDetector(
          onTap: () => _onTap(context, fileHandlerState.filesState[filesIndex].file.bytes),
          child: MouseRegion(
            onEnter: (_) => onEnter(filesIndex, context),
            onExit: (_) => onExit(filesIndex, context),
            onHover: (_) => onEnter(filesIndex, context),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Hero(
                  tag: fileHandlerState.filesState[filesIndex].thumbnailFile?.bytes.toString() ?? '',
                  child: Material(
                    borderRadius: BorderRadius.circular(4),
                    elevation: 4,
                    color: Theme.of(context).colorScheme.surface,
                    child: SizedBox.square(
                      dimension: 124,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const SizedBox(
                            height: 18,
                          ),
                          Expanded(
                            child: Center(
                              child: Icon(
                                Thumbnailer.getIconDataForMimeType(fileHandlerState.filesState[filesIndex].file.mimeType) ?? Icons.file_copy,
                                size: 45,
                                color: fileHandlerState.filesState[filesIndex].file.mimeType.lightColor,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 6.0),
                            child: SizedBox(
                              height: 28,
                              child: GridTileBar(
                                title: Text(
                                  fileHandlerState.filesState[filesIndex].file.name,
                                  style: TextStyle(fontSize: 10, color: Theme.of(context).colorScheme.onSurface),
                                ),
                                subtitle: Text(
                                  determineSizeFormat(),
                                  style: TextStyle(fontSize: 10, color: Theme.of(context).colorScheme.onSurface),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: -10,
                  right: -10,
                  child: AnimatedOpacity(
                    duration: const Duration(milliseconds: 375),
                    curve: Curves.easeInCubic,
                    opacity: fileHandlerState.filesState[filesIndex].isFocused ? 0.9 : 0,
                    child: IconButton(
                      style: IconButton.styleFrom(
                        elevation: 4,
                      ),
                      onPressed: () => BlocProvider.of<FileHandlerBloc>(context).add(FileRemoveRequested(removeAtIndex: filesIndex)),
                      icon: Container(
                        decoration: BoxDecoration(color: Colors.redAccent, borderRadius: BorderRadius.circular(40)),
                        child: const Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Icon(
                            Icons.close,
                            size: 10,
                          ),
                        ),
                      ),
                      hoverColor: Colors.transparent,
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  child: AnimatedOpacity(
                    duration: const Duration(milliseconds: 375),
                    curve: Curves.easeInCubic,
                    opacity: fileHandlerState.filesState[filesIndex].isFocused ? 0.9 : 0,
                    child: ReorderableDragStartListener(
                      key: ValueKey(filesIndex),
                      index: filesIndex,
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(color: Colors.black54, borderRadius: BorderRadius.circular(4)),
                          child: const Padding(
                            padding: EdgeInsets.symmetric(vertical: 2),
                            child: Icon(
                              Icons.drag_indicator,
                              size: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  String determineSizeFormat() {
    final size = fileHandlerState.filesState[filesIndex].file.size;
    if (size < 1000) {
      return "${size.toStringAsFixed(2)} B";
    } else if (size > 1000 && size < (1000 * 1000)) {
      return "${(size / 1000).toStringAsFixed(2)} KB";
    } else if (size > (1000 * 1000) && size < (1000 * 1000 * 200)) {
      return "${(size / (1000 * 1000)).toStringAsFixed(2)} MB";
    } else {
      return "File too large";
    }
  }
}
