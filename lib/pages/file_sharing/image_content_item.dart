import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:matrix/matrix.dart';
import 'package:thumbnailer/thumbnailer.dart';

import '../../cubits/file_handler/file_handler_bloc.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/matrix.dart';
import '../../widgets/swipable_vertical.dart';
import '../image_viewer/image_viewer_bytes_view.dart';

class ImageContentItem extends StatelessWidget {
  final double maxHeight;
  final ImageState imageState;
  final int index;
  const ImageContentItem({super.key, required this.maxHeight, required this.imageState, required this.index});

  @override
  Widget build(BuildContext context) {
    if (PlatformInfos.isMobile) {
      return _ImageContentItemMobile(
        maxHeight: maxHeight,
        imageState: imageState,
        index: index,
        key: key,
      );
    } else {
      return _ImageContentItemDesktop(
        maxHeight: maxHeight,
        imageState: imageState,
        index: index,
        key: key,
      );
    }
  }
}

class _ImageContentItemMobile extends StatelessWidget {
  final double maxHeight;
  final ImageState imageState;
  final int index;
  const _ImageContentItemMobile({super.key, required this.maxHeight, required this.imageState, required this.index});

  void _onTap(BuildContext context, Uint8List image) {
    showDialog(
      context: Matrix.of(context).navigatorContext,
      useRootNavigator: false,
      builder: (_) => ImageViewerBytesView(image: image),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double aspectRatio = imageState.imageFile.width! / (imageState.imageFile.height as int);
    final double width = (maxHeight * aspectRatio);
    return Padding(
      key: key, // add this line,
      padding: const EdgeInsets.only(right: 12),
      child: SizedBox(
        width: width,
        height: maxHeight,
        child: Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          children: [
            SwipeableVertical(
              childSize: Size(width, maxHeight),
              onSwipeEnd: () => BlocProvider.of<FileHandlerBloc>(context).add(
                FileRemoveRequested(removeAtIndex: index),
              ),
              threshold: maxHeight,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: GestureDetector(
                  //onLongPress: () => onLongPress(imageIndex),
                  onTap: () => _onTap(context, imageState.imageFile.bytes),
                  child: Hero(
                    tag: imageState.thumbnailFile?.bytes.toString() ?? '',
                    child: Image.memory(
                      imageState.thumbnailFile?.bytes ?? imageState.imageFile.bytes,
                      fit: BoxFit.cover,
                      height: maxHeight,
                      width: width,
                      errorBuilder: (context, object, stack) {
                        Logs().e("Error occurred displaying image or thumbnail file, using Thumbnailer as fallback, $stack");
                        return Thumbnail(
                          mimeType: imageState.imageFile.mimeType,
                          dataResolver: () => Future.delayed(Duration.zero).then((value) => imageState.imageFile.bytes),
                          widgetSize: 124,
                          decoration: WidgetDecoration(
                            backgroundColor: Colors.grey,
                            iconColor: Colors.blue,
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _ImageContentItemDesktop extends StatelessWidget {
  final double maxHeight;
  final ImageState imageState;
  final int index;
  const _ImageContentItemDesktop({super.key, required this.maxHeight, required this.imageState, required this.index});

  void _onTap(BuildContext context, Uint8List image) {
    showDialog(
      context: Matrix.of(context).navigatorContext,
      useRootNavigator: false,
      builder: (_) => ImageViewerBytesView(image: image),
    );
  }

  void onExit(int index, BuildContext context) {
    BlocProvider.of<FileHandlerBloc>(context).add(FileFocusRemoveRequested(removeFocusOnIndex: index));
  }

  void onEnter(int index, BuildContext context) {
    BlocProvider.of<FileHandlerBloc>(context).add(FileFocusRequested(focusOnIndex: index));
  }

  void onLongPress(int index, BuildContext context) {
    BlocProvider.of<FileHandlerBloc>(context).add(FileFocusRequested(focusOnIndex: index));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      key: key,
      padding: const EdgeInsets.only(right: 12),
      child: Material(
        borderRadius: BorderRadius.circular(4),
        elevation: 4,
        child: GestureDetector(
          onTap: () => _onTap(context, imageState.imageFile.bytes),
          child: MouseRegion(
            onEnter: (_) => onEnter(index, context),
            onExit: (_) => onExit(index, context),
            onHover: (_) => onEnter(index, context),
            child: Stack(
              alignment: Alignment.center,
              clipBehavior: Clip.none,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(4),
                  child: Image.memory(
                    imageState.imageFile.bytes,
                    isAntiAlias: true,
                    filterQuality: FilterQuality.high,
                    errorBuilder: (context, object, stack) {
                      Logs().e("Error occurred displaying image or thumbnail file, using Thumbnailer as fallback, $stack");
                      return Thumbnail(
                        mimeType: imageState.imageFile.mimeType,
                        //dataSize: state.filesState[filesIndex].file.size,
                        dataResolver: () => Future.delayed(Duration.zero).then((value) => imageState.imageFile.bytes),
                        //name: state.filesState[filesIndex].file.name,
                        widgetSize: 124,
                        decoration: WidgetDecoration(
                          backgroundColor: Colors.grey,
                          iconColor: Colors.blue,
                        ),
                      );
                    },
                  ),
                ),
                Positioned(
                  top: -10,
                  right: -10,
                  child: AnimatedOpacity(
                    duration: const Duration(milliseconds: 375),
                    curve: Curves.easeInCubic,
                    opacity: imageState.isFocused ? 0.9 : 0,
                    child: IconButton(
                      style: IconButton.styleFrom(
                        elevation: 4,
                      ),
                      onPressed: () => BlocProvider.of<FileHandlerBloc>(context).add(FileRemoveRequested(removeAtIndex: index)),
                      icon: Container(
                        decoration: BoxDecoration(color: Colors.redAccent, borderRadius: BorderRadius.circular(40)),
                        child: const Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Icon(
                            Icons.close,
                            size: 10,
                          ),
                        ),
                      ),
                      hoverColor: Colors.transparent,
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  child: AnimatedOpacity(
                    duration: const Duration(milliseconds: 375),
                    curve: Curves.easeInCubic,
                    opacity: imageState.isFocused ? 0.9 : 0,
                    child: ReorderableDragStartListener(
                      key: ValueKey(index),
                      index: index,
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(color: Colors.black54, borderRadius: BorderRadius.circular(4)),
                          child: const Padding(
                            padding: EdgeInsets.symmetric(vertical: 2),
                            child: Icon(
                              Icons.drag_indicator,
                              size: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
