import 'package:file_picker/file_picker.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';
import 'package:pageMe/pages/file_sharing/file_content_item.dart';
import 'package:pageMe/pages/file_sharing/image_content_item.dart';
import 'package:pageMe/pages/file_sharing/proxy_file_decorator.dart';
import 'package:pageMe/pages/file_sharing/proxy_image_decorator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pageMe/pages/file_sharing/share_event_content_item.dart';
import 'package:pageMe/pages/file_sharing/video_content_item.dart';
import 'package:pageMe/utils/platform_infos.dart';
import '../../cubits/sending_handler/sending_handler_bloc.dart';
import 'file_progress_handler.dart';
import 'sending_progress_handler.dart';

class FilesContent extends StatefulWidget {
  const FilesContent({super.key});

  @override
  State<FilesContent> createState() => _FilesContentState();
}

class _FilesContentState extends State<FilesContent> {
  final double maxHeight = 148;
  static const _progressStyle = TextStyle(
    fontSize: 16,
  );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileHandlerBloc, FileHandlerState>(
      buildWhen: (previous, next) {
        return true;
      },
      builder: (context, fileHandlerState) {
        return BlocBuilder<SendingHandlerBloc, SendingHandlerState>(
          builder: (context, sendingState) {
            return AnimatedSwitcher(
              duration: const Duration(milliseconds: 300),
              child: fileHandlerState.processingStatus != ProcessingStatus.ready
                  ? const FileProgressHandler(progressStyle: _progressStyle)
                  : (sendingState is SendingHandlerInProgress)
                      ? const SendingProgressHandler(progressStyle: _progressStyle)
                      : Row(
                          children: [
                            if (!fileHandlerState.isOnlyShareEventState)
                              IconButton(
                                  onPressed: () {
                                    if (fileHandlerState.isOnlyImageState) {
                                      BlocProvider.of<FileHandlerBloc>(context).add(const FileSelectionRequested(FileType.image));
                                    } else if (fileHandlerState.isOnlyVideoState) {
                                      BlocProvider.of<FileHandlerBloc>(context).add(const FileSelectionRequested(FileType.video));
                                    } else {
                                      BlocProvider.of<FileHandlerBloc>(context).add(const FileSelectionRequested(FileType.any));
                                    }
                                  },
                                  icon: const Icon(Icons.add_circle_outline_outlined)),
                            Expanded(
                              child: SizedBox(
                                height: 180,
                                child: ReorderableListView.builder(
                                  scrollDirection: fileHandlerState.isOnlyShareEventState ? Axis.vertical : Axis.horizontal,
                                  shrinkWrap: true,
                                  buildDefaultDragHandles: PlatformInfos.isMobile ? true : false,
                                  itemCount: fileHandlerState.isOnlyShareEventState ? fileHandlerState.shareEventState.length : fileHandlerState.filesState.length,
                                  padding: const EdgeInsets.all(6),
                                  proxyDecorator: (child, filesIndex, animation) {
                                    if (fileHandlerState.shareEventState.isNotEmpty) {
                                      final Map<String, dynamic> shareEventState = fileHandlerState.shareEventState[filesIndex];
                                      return ShareEventContentItem(
                                        shareEventCount: fileHandlerState.shareEventState.length,
                                        shareEventState: shareEventState,
                                        filesIndex: filesIndex,
                                        key: ValueKey(filesIndex),
                                      );
                                    } else {
                                      final fileState = fileHandlerState.filesState[filesIndex];
                                      if (fileState is ImageState) {
                                        return ProxyImageDecorator(
                                          index: filesIndex,
                                          animation: animation,
                                          imageState: fileState,
                                          maxHeight: maxHeight,
                                          child: child,
                                        );
                                      } else if (fileState is VideoState && fileState.thumbnailFile != null) {
                                        return ProxyVideoDecorator(
                                          index: filesIndex,
                                          animation: animation,
                                          videoState: fileState,
                                          maxHeight: maxHeight,
                                          child: child,
                                        );
                                      } else {
                                        return ProxyFileDecorator(
                                          index: filesIndex,
                                          animation: animation,
                                          fileState: fileState,
                                          maxHeight: maxHeight,
                                          child: child,
                                        );
                                      }
                                    }
                                  },
                                  onReorder: (int oldIndex, int newIndex) => BlocProvider.of<FileHandlerBloc>(context).add(
                                    FileReorderRequested(oldIndex: oldIndex, newIndex: newIndex),
                                  ),
                                  itemBuilder: (context, filesIndex) {
                                    if (fileHandlerState.shareEventState.isNotEmpty) {
                                      final Map<String, dynamic> shareEventState = fileHandlerState.shareEventState[filesIndex];
                                      return ShareEventContentItem(
                                        shareEventCount: fileHandlerState.shareEventState.length,
                                        shareEventState: shareEventState,
                                        filesIndex: filesIndex,
                                        key: ValueKey(filesIndex),
                                      );
                                    } else {
                                      final fileState = fileHandlerState.filesState[filesIndex];
                                      if (fileState is ImageState) {
                                        return ImageContentItem(
                                          key: ValueKey(filesIndex),
                                          imageState: fileState,
                                          index: filesIndex,
                                          maxHeight: maxHeight,
                                        );
                                      } else if (fileState is VideoState && fileState.thumbnailFile != null) {
                                        return VideoContentItem(
                                          key: ValueKey(filesIndex),
                                          videoState: fileState,
                                          index: filesIndex,
                                          maxHeight: maxHeight,
                                        );
                                      }
                                      else {
                                        return FileContentItem(
                                          key: ValueKey(filesIndex),
                                          filesIndex: filesIndex,
                                          fileHandlerState: fileHandlerState,
                                          maxHeight: maxHeight,
                                        );
                                      }
                                    }
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
            );
          },
        );
      },
    );
  }
}
