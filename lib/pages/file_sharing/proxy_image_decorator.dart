import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/string_color.dart';
import 'package:thumbnailer/thumbnailer.dart';

import '../../cubits/file_handler/file_handler_bloc.dart';
import '../../utils/platform_infos.dart';

class ProxyImageDecorator extends StatelessWidget {
  final double maxHeight;
  final Widget child;
  final int index;
  final Animation<double> animation;
  final ImageState imageState;
  const ProxyImageDecorator({
    super.key,
    required this.child,
    required this.index,
    required this.animation,
    required this.imageState,
    required this.maxHeight,
  });

  @override
  Widget build(BuildContext context) {
    if (PlatformInfos.isMobile) {
      return _ProxyImageDecoratorMobile(
        maxHeight: maxHeight,
        animation: animation,
        image: imageState.thumbnailFile ?? imageState.imageFile,
        key: key,
        index: index,
        child: child,
      );
    } else {
      return _ProxyImageDecoratorDesktop(
        maxHeight: maxHeight,
        animation: animation,
        image: imageState.thumbnailFile ?? imageState.imageFile,
        key: key,
        index: index,
        child: child,
      );
    }
  }
}

class ProxyVideoDecorator extends StatelessWidget {
  final double maxHeight;
  final Widget child;
  final int index;
  final Animation<double> animation;
  final VideoState videoState;
  const ProxyVideoDecorator({
    super.key,
    required this.child,
    required this.index,
    required this.animation,
    required this.videoState,
    required this.maxHeight,
  });

  @override
  Widget build(BuildContext context) {
    if (PlatformInfos.isMobile) {
      return _ProxyImageDecoratorMobile(
        maxHeight: maxHeight,
        animation: animation,
        image: videoState.thumbnailFile!,
        key: key,
        index: index,
        child: child,
      );
    } else {
      return _ProxyImageDecoratorDesktop(
        maxHeight: maxHeight,
        animation: animation,
        image: videoState.thumbnailFile!,
        key: key,
        index: index,
        child: child,
      );
    }
  }
}

class _ProxyImageDecoratorMobile extends StatelessWidget {
  final double maxHeight;
  final Widget child;
  final int index;
  final Animation<double> animation;
  final MatrixImageFile image;
  const _ProxyImageDecoratorMobile({
    super.key,
    required this.child,
    required this.index,
    required this.animation,
    required this.image,
    required this.maxHeight,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget? child) {
        final double aspectRatio = (image.width)! / (image.height as int);
        final double width = (maxHeight * aspectRatio);
        final double animValue = Curves.easeInOut.transform(animation.value);
        final double elevation = lerpDouble(0, 6, animValue)!;
        return SizedBox(
          height: maxHeight,
          width: width,
          child: Material(
            elevation: elevation,
            type: MaterialType.card,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: Image.memory(
                image.bytes ?? image.bytes,
                fit: BoxFit.cover,
                height: maxHeight,
                width: width,
                errorBuilder: (context, object, stack) {
                  Logs().e("Error occurred displaying image or thumbnail file, using Thumbnail package as fallback, $stack");
                  return Thumbnail(
                    mimeType: image.mimeType,
                    //dataSize: state.filesState[filesIndex].file.size,
                    dataResolver: () => Future.delayed(Duration.zero).then((value) => image.bytes),
                    //name: state.filesState[filesIndex].file.name,
                    widgetSize: 124,
                    decoration: WidgetDecoration(
                      backgroundColor: Colors.grey,
                      iconColor: Colors.blue,
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
      child: child,
    );
  }
}

class _ProxyImageDecoratorDesktop extends StatelessWidget {
  final double maxHeight;
  final Widget child;
  final int index;
  final Animation<double> animation;
  final MatrixImageFile image;
  const _ProxyImageDecoratorDesktop({
    super.key,
    required this.child,
    required this.index,
    required this.animation,
    required this.image,
    required this.maxHeight,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget? child) {
        final double aspectRatio = (image.width)! / (image.height as int);
        final double width = (maxHeight * aspectRatio);
        final double animValue = Curves.easeInOut.transform(animation.value);
        final double elevation = lerpDouble(0, 6, animValue)!;
        return SizedBox(
          height: maxHeight,
          width: width,
          child: Material(
              elevation: elevation,
              type: MaterialType.card,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Image.memory(
                  image?.bytes ?? image.bytes,
                  fit: BoxFit.cover,
                  height: maxHeight,
                  width: width,
                  errorBuilder: (context, object, stack) {
                    Logs().e("Error occurred displaying image or thumbnail file, using Thumbnailer as fallback, $stack");
                    return Thumbnail(
                      mimeType: image.mimeType,
                      //dataSize: state.filesState[filesIndex].file.size,
                      dataResolver: () => Future.delayed(Duration.zero).then((value) => image.bytes),
                      //name: state.filesState[filesIndex].file.name,
                      widgetSize: 124,
                      decoration: WidgetDecoration(
                        backgroundColor: Colors.grey,
                        iconColor: Colors.blue,
                      ),
                    );
                  },
                ),
              )),
        );
      },
      child: child,
    );
  }
}
