import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';
import 'package:pageMe/pages/chat/events/bubble_container.dart';

class ShareEventContentItem extends StatelessWidget {
  final Map<String, dynamic> shareEventState;
  final int filesIndex;
  final int shareEventCount;
  const ShareEventContentItem({super.key, required this.shareEventState, required this.filesIndex, required this.shareEventCount});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Material(
          color: Colors.transparent,
          child: SwipeableHorizontal(
            actionIcon: Icons.delete_outline,
            actionIconSize: 25,
            onSwipeEnd: () => BlocProvider.of<FileHandlerBloc>(context).add(FileRemoveRequested(removeAtIndex: filesIndex)),
            enabled: true,
            threshold: min(80, MediaQuery.sizeOf(context).width / 8),
            child: ListTile(
              visualDensity: const VisualDensity(vertical: -4),
              minLeadingWidth: 5,
              minVerticalPadding: 0,
              contentPadding: const EdgeInsets.symmetric(horizontal: 4),
              titleAlignment: ListTileTitleAlignment.top,
              trailing: SizedBox.square(
                dimension: 16,
                child: ReorderableDragStartListener(
                  index: filesIndex,
                  key: ValueKey(filesIndex),
                  child: const Icon(Icons.drag_indicator_outlined),
                ),
              ),
              title: SingleChildScrollView(
                child: Text(
                  shareEventState['body'],
                  style: const TextStyle(fontSize: 16),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              dense: true,
            ),
          ),
        ),
        if (filesIndex != shareEventCount -1)
        const Divider(),
      ],
    );
  }
}
