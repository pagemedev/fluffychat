import 'package:flutter/material.dart';

class FileProgressIncrementListTile extends StatelessWidget {
  const FileProgressIncrementListTile({
    super.key,
    required TextStyle progressStyle,
    required this.currentProgress,
  }) : _progressStyle = progressStyle;

  final TextStyle _progressStyle;
  final String currentProgress;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListTile(
        minLeadingWidth: 5,
        minVerticalPadding: 0,
        visualDensity: const VisualDensity(vertical: -4),
        leading: const SizedBox.square(
          dimension: 16,
          child: CircularProgressIndicator(strokeWidth: 2),
        ),
        title: Text(currentProgress, style: _progressStyle),
        dense: true,
      ),
    );
  }
}
