import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../cubits/file_handler/file_handler_bloc.dart';
import '../../../cubits/sending_handler/sending_handler_bloc.dart';
import '../chat/chat_input_row.dart';
import '../chat_list/chat_list.dart';
import 'file_sharing_action_buttons.dart';

class FileSharing extends StatelessWidget {
  final ChatListController controller;
  const FileSharing({super.key, required this.controller});

  void handleSentFiles({required FileHandlerState fileState, required BuildContext context, bool? isCancelled}) {
    if (fileState.isReadyWithFiles) {
      BlocProvider.of<FileHandlerBloc>(context).add(FileCompletionRequested(isCancelled: isCancelled ?? false));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SendingHandlerBloc, SendingHandlerState>(
      listener: (context, sendingState) {
        final fileState = BlocProvider.of<FileHandlerBloc>(context).state;
        if (sendingState.isSent) {
          handleSentFiles(fileState: fileState, context: context);
          controller.clearShareContent();
          controller.clearSelectedRooms();
        }
        else if (sendingState.isCancelled || sendingState.isError){
          handleSentFiles(fileState: fileState, context: context, isCancelled: true);
          controller.clearShareContent();
          controller.clearSelectedRooms();
        }
      },
      child: BlocBuilder<SendingHandlerBloc, SendingHandlerState>(
        builder: (context, sendingState) {
          return BlocBuilder<FileHandlerBloc, FileHandlerState>(
            buildWhen: (previous, next) {
              //Logs().v("previous.processingStatus: ${previous.processingStatus}, next.processingStatus: ${next.processingStatus}");
              return true;
            },
            builder: (context, fileState) {
              //Logs().v("ChatInputRow - fileState.isInitiated: ${fileState.isInitiated}");
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4.0),
                            child: AnimatedSwitcher(
                              duration: const Duration(milliseconds: 500),
                              reverseDuration: const Duration(milliseconds: 500),
                              switchInCurve: Curves.easeOutExpo,
                              switchOutCurve: Curves.easeInExpo,
                              transitionBuilder: (Widget child, Animation<double> animation) {
                                final Animation<Offset> offsetAnimation = Tween<Offset>(begin: const Offset(0.0, 2), end: Offset.zero).animate(animation);
                                return SlideTransition(
                                  position: offsetAnimation,
                                  child: child,
                                );
                              },
                              child: fileState.isInitiated ? const FilesContainer() : const SizedBox.shrink(),
                            ),
                          ),
                        ),
                        fileState.isInitiated
                            ? FileSharingActionButtons(
                                controller: controller,
                              )
                            : const SizedBox.shrink(),
                      ],
                    ),
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }
}
