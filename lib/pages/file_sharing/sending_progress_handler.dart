import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pageMe/pages/file_sharing/sending_progress_increment_list_tile.dart';

import '../../cubits/sending_handler/sending_handler_bloc.dart';

class SendingProgressHandler extends StatelessWidget {
  const SendingProgressHandler({
    super.key,
    required TextStyle progressStyle,
  }) : _progressStyle = progressStyle;

  final TextStyle _progressStyle;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SendingHandlerBloc, SendingHandlerState>(
      builder: (context, state) {
        if (state is SendingHandlerComplete) {
          return Container(); // Do not show progress widget when sending is complete
        } else if (state is SendingHandlerCancelled) {
          final String currentProgress = state.statusMessage;
          return SendingProgressIncrementListTile(progressStyle: _progressStyle, currentProgress: currentProgress, percentage: null); // Do not show progress widget when sending is complete
        } else if (state is SendingHandlerInProgress) {
          final String currentProgress = state.statusMessage;
          final double percentage = state.sentItemsInCurrentRoom / state.totalItemsInCurrentRoom;
          if (state.totalRooms == 1 && state.totalItemsInCurrentRoom == 1) {
            return SendingProgressIncrementListTile(progressStyle: _progressStyle, currentProgress: currentProgress, percentage: null);
          }
          return SendingProgressIncrementListTile(progressStyle: _progressStyle, currentProgress: currentProgress, percentage: percentage);
        } else if (state is SendingHandlerError) {
          return SendingProgressIncrementListTile(progressStyle: _progressStyle, currentProgress: 'An error occurred: ${state.message}', percentage: 1.0);
        } else {
          return Container();
        }
      },
    );
  }
}
