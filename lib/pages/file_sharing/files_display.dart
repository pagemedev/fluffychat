import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';
import 'package:pageMe/cubits/image_handler/image_handler_bloc.dart';
import 'package:pageMe/utils/string_color.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../../config/themes.dart';
import '../../cubits/sending_handler/sending_handler_bloc.dart';
import '../file_sharing/files_content.dart';

class FilesDisplay extends StatelessWidget {
  const FilesDisplay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileHandlerBloc, FileHandlerState>(
      buildWhen: (_, __) {
        return true;
      },
      builder: (context, filesState) {
        return BlocBuilder<SendingHandlerBloc, SendingHandlerState>(
          builder: (context, sendingState) {
            return Padding(
              padding: const EdgeInsets.all(6.0),
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 500),
                alignment: Alignment.center,
                curve: Curves.easeOutExpo,
                height: filesState.isInitiated
                    ? (filesState.isReadyWithFiles && sendingState is !SendingHandlerInProgress)
                        ? (filesState.isOnlyShareEventState)
                            ? (44 * filesState.shareEventState.length).toDouble()
                            : 136
                        : 36
                    : 0,
                clipBehavior: Clip.hardEdge,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(11),
                    color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.surfaceVariant.darken(10) : Theme.of(context).colorScheme.surface.darken(5)),
                child: const FilesContent(),
              ),
            );
          },
        );
      },
    );
  }
}
