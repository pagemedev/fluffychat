import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/settings/settings_section_title.dart';
import '../../config/themes.dart';
import '../../utils/localized_exception_extension.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/matrix.dart';
import '../../widgets/settings_switch_list_tile.dart';
import 'settings_notifications.dart';

class SettingsNotificationsView extends StatelessWidget {
  final SettingsNotificationsController controller;

  const SettingsNotificationsView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;
    return Scaffold(
      backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
      appBar: AppBar(
        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
        leading: const BackButton(),
        centerTitle: false,
        elevation: 0,
        title: Text(
          L10n.of(context)!.notifications,
          style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
        ),
      ),
      body: StreamBuilder(
          stream: Matrix.of(context).client.onAccountData.stream.where((event) => event.type == 'm.push_rules'),
          builder: (BuildContext context, _) {
            return Container(
              constraints: const BoxConstraints(
                maxWidth: 1000,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                      child: Material(
                        borderRadius: BorderRadius.circular(12),
                        color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                        elevation: 1,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: CupertinoListSection.insetGrouped(
                            margin: EdgeInsets.zero,
                            backgroundColor:
                                PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                            children: [
                              SettingsNotificationsSwitchListTile.adaptive(
                                value: !Matrix.of(context).client.allPushNotificationsMuted,
                                title: 'Notifications enabled',
                                onChanged: (_) => showFutureLoadingDialog(
                                  context: context,
                                  future: () => Matrix.of(context).client.setMuteAllPushNotifications(
                                        !Matrix.of(context).client.allPushNotificationsMuted,
                                      ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    (PlatformInfos.isIOS) ? const SettingsSectionTitle(title: 'Encryption') : const SizedBox(),
                    (PlatformInfos.isIOS)
                        ? FutureBuilder<bool>(
                            future: controller.getNotificationEncryption(),
                            builder: (context, snapshot) {
                              return Padding(
                                padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                                child: Material(
                                  borderRadius: BorderRadius.circular(12),
                                  color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                                  elevation: 1,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(12),
                                    child: CupertinoListSection.insetGrouped(
                                      margin: EdgeInsets.zero,
                                      backgroundColor:
                                      PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                                      children: [SettingsNotificationsSwitchListTile.adaptive(
                                        value: (snapshot.connectionState == ConnectionState.done) ? snapshot.data! : false,
                                        title: 'Send encrypted notifications',
                                        onChanged: (value) {
                                          if (snapshot.connectionState == ConnectionState.done) {
                                            controller.setNotificationEncryption(value);
                                          }
                                        },
                                      )],
                                    ),
                                  ),
                                ),
                              );
                            })
                        : const SizedBox(),
                    if (!Matrix.of(context).client.allPushNotificationsMuted) ...{
                      SettingsSectionTitle(
                        title: L10n.of(context)!.pushRules,
                      ),
                      Padding(
                        padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                        child: Material(
                          borderRadius: BorderRadius.circular(12),
                          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                          elevation: 1,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child: CupertinoListSection.insetGrouped(
                              margin: EdgeInsets.zero,
                              backgroundColor:
                                  PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                              children: [
                                for (var item in NotificationSettingsItem.items)
                                  SettingsNotificationsSwitchListTile.adaptive(
                                    value: controller.getNotificationSetting(item) ?? true,
                                    title: item.title(context),
                                    onChanged: (bool enabled) => controller.setNotificationSetting(item, enabled),
                                  ),
                              ],
                            ),
                          ),
                        ),
                      )
                    },
                    SettingsSectionTitle(title: L10n.of(context)!.devices),
                    FutureBuilder<List<Pusher>?>(
                      future: Matrix.of(context).client.getPushers(),
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          Center(
                            child: Text(
                              snapshot.error!.toLocalizedString(context),
                            ),
                          );
                        }
                        if (snapshot.connectionState != ConnectionState.done) {
                          const Center(child: CircularProgressIndicator.adaptive(strokeWidth: 2));
                        }
                        final pushers = snapshot.data ?? [];
                        return ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: pushers.length,
                          itemBuilder: (_, i) => Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                            child: Material(
                              color: backgroundColor, // Set the Material color
                              borderRadius: BorderRadius.circular(12),
                              elevation: 1, // Same as your Container
                              child: SizedBox(
                                width: double.infinity,
                                child: Padding(
                                  padding: const EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Icon(
                                        Icons.phone_android_outlined,
                                        color: contentColor,
                                        size: 24,
                                      ),
                                      Expanded(
                                        child: Padding(
                                            padding: const EdgeInsetsDirectional.fromSTEB(12, 0, 0, 0),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text(pushers[i].appDisplayName,
                                                    maxLines: 2,
                                                    softWrap: true,
                                                    style: TextStyle(
                                                      overflow: TextOverflow.ellipsis,
                                                      color: PageMeThemes.isDarkMode(context)
                                                          ? Theme.of(context).colorScheme.onTertiaryContainer
                                                          : Theme.of(context).colorScheme.onSurface,
                                                      fontSize: 16,
                                                      fontWeight: FontWeight.bold,
                                                    )),
                                                Text(pushers[i].appId,
                                                    maxLines: 2,
                                                    softWrap: true,
                                                    style: TextStyle(
                                                      overflow: TextOverflow.ellipsis,
                                                      color: PageMeThemes.isDarkMode(context)
                                                          ? Theme.of(context).colorScheme.onTertiaryContainer
                                                          : Theme.of(context).colorScheme.onSurface,
                                                      fontSize: 16,
                                                      fontWeight: FontWeight.w500,
                                                    )),
                                                Text(pushers[i].data.url.toString(),
                                                    maxLines: 2,
                                                    softWrap: true,
                                                    style: TextStyle(
                                                      overflow: TextOverflow.ellipsis,
                                                      color: PageMeThemes.isDarkMode(context)
                                                          ? Theme.of(context).colorScheme.onTertiaryContainer
                                                          : Theme.of(context).colorScheme.onSurface,
                                                      fontSize: 14,
                                                      fontWeight: FontWeight.w500,
                                                    ))
                                              ],
                                            )),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                    const SizedBox(
                      height: 16,
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }
}
