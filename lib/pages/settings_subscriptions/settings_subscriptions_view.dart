import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/utils/date_time_extension.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../config/themes.dart';
import '../../widgets/layouts/max_width_body.dart';
import '../subscriptions/subscriptions_cubit.dart';

class SettingsSubscriptionsView extends StatefulWidget {
  const SettingsSubscriptionsView({Key? key}) : super(key: key);

  @override
  State<SettingsSubscriptionsView> createState() => _SettingsSubscriptionsViewState();
}

class _SettingsSubscriptionsViewState extends State<SettingsSubscriptionsView> {
  bool _isInitialized = false;

  @override
  void initState() {
    unawaited(BlocProvider.of<SubscriptionsCubit>(context).fetchCustomerInfo());
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    if (!_isInitialized) {
      await BlocProvider.of<SubscriptionsCubit>(context).fetchCustomerInfo();
      _isInitialized = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.pop();
        return false;
      },
      child: BlocBuilder<SubscriptionsCubit, SubscriptionsState>(
        builder: (context, subscriptionsState) {
          final String? productId;
          String? managementURLString;
          Uri? managementURI;
          String? expirationDateTimeString;
          String? expirationTime;
          String? expirationDate;
          String? subscriptionName;
          String? subscriptionPrice;
          if (subscriptionsState.customerInfo != null) {
            if (subscriptionsState.customerInfo!.activeSubscriptions.isNotEmpty) {
              productId = subscriptionsState.customerInfo?.activeSubscriptions.first;
              managementURLString = subscriptionsState.customerInfo?.managementURL;
              managementURI = Uri.tryParse(managementURLString!);
              expirationDateTimeString = subscriptionsState.customerInfo?.latestExpirationDate;
              expirationTime = DateTime.tryParse(expirationDateTimeString!)?.toLocal().localizedTime(context);
              expirationDate = DateTime.tryParse(expirationDateTimeString)?.localizedDate(context);

              if (productId == subscriptionsState.monthlyPackage?.storeProduct.identifier) {
                subscriptionName = subscriptionsState.monthlyPackage?.storeProduct.title;
                subscriptionPrice = subscriptionsState.monthlyPackage?.storeProduct.priceString;
              } else if (productId == subscriptionsState.annualPackage?.storeProduct.identifier) {
                subscriptionName = subscriptionsState.annualPackage?.storeProduct.title;
                subscriptionPrice = subscriptionsState.annualPackage?.storeProduct.priceString;
              }
            }
          }

          return Scaffold(
              appBar: AppBar(
                centerTitle: false,
                backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
                title: Text("Subscriptions", style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
                bottom: PreferredSize(
                    preferredSize: subscriptionsState.isLoading ? const Size.fromHeight(4) : const Size.fromHeight(0),
                    child: subscriptionsState.isLoading
                        ? const LinearProgressIndicator(
                            color: Colors.white,
                          )
                        : const SizedBox()),
                leading: const BackButton(),
                elevation: 0,
                actions: [
                  IconButton(
                      onPressed: () async {
                        await BlocProvider.of<SubscriptionsCubit>(context).fetchCustomerInfo();
                      },
                      icon: const Icon(Icons.refresh))
                ],
              ),
              body: ListTileTheme(
                iconColor: Theme.of(context).textTheme.bodyText1!.color,
                child: MaxWidthBody(
                  withScrolling: true,
                  child: (subscriptionsState.customerInfo != null)
                      ? (subscriptionsState.customerInfo!.activeSubscriptions.isNotEmpty)
                          ? Column(
                              children: [
                                ListTile(
                                  title: const Text('Active Subscription'),
                                  subtitle: Text(subscriptionName ?? 'No information available'),
                                ),
                                const Divider(height: 1),
                                ListTile(
                                  title: const Text('Next payment'),
                                  subtitle: Text('$subscriptionPrice on $expirationDate: $expirationTime'),
                                ),
                                const Divider(height: 1),
                                ListTile(
                                  title: const Text('Management Link'),
                                  subtitle: Text(managementURLString ?? 'No information available'),
                                  onTap: () async => (managementURI != null) ? launchUrl(managementURI, mode: LaunchMode.externalApplication) : {},
                                  trailing: const Icon(Icons.open_in_new),
                                )
                              ],
                            )
                          : Column(
                              children: [
                                ListTile(
                                  title: const Text('No Active Subscription'),
                                  subtitle: const Text('Tap to select a subscription.'),
                                  onTap: () {
                                    context.go('/settings/subscription_information/subscriptions');
                                  },
                                  trailing: const Icon(Icons.payment),
                                ),
                                const Divider(height: 1),
                              ],
                            )
                      : const Center(
                          child: SizedBox.square(
                            dimension: 150,
                            child: CircularProgressIndicator(),
                          ),
                        ),
                ),
              ));
        },
      ),
    );
  }
}
