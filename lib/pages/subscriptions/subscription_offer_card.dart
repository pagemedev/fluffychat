import 'package:flutter/material.dart';
import 'package:purchases_flutter/models/store_product_wrapper.dart';

import '../../config/themes.dart';

class SubscriptionProductCard extends StatefulWidget {
  final StoreProduct storeProduct;
  final Function(StoreProduct? testOffer) onSelected;
  final bool isDisabled;
  final bool isSelected;

  const SubscriptionProductCard({
    Key? key,
    required this.onSelected,
    required this.isDisabled,
    required this.isSelected,
    required this.storeProduct,
  }) : super(key: key);

  @override
  SubscriptionProductCardState createState() => SubscriptionProductCardState();
}

class SubscriptionProductCardState extends State<SubscriptionProductCard> {


  void _toggleSelection() {
    if (!widget.isSelected) {
      widget.onSelected(widget.storeProduct);
    } else {
      widget.onSelected(null);
    }
  }

  @override
  Widget build(BuildContext context) {
    final StoreProduct product;
    final String productTitle ;
    final String productDescription;
    final String productPriceString;
    final bool isAnnual;
    final bool hasTrial;
    String? productTrialString;
    String? productTrialPeriodString;

    product = widget.storeProduct;
    productTitle = product.title.replaceFirst('plan (PageMe Public)', '');
    productDescription = product.description;
    isAnnual = product.subscriptionPeriod == 'P1Y';
    isAnnual ? productPriceString = '${(product.priceString)}/year' : productPriceString = '${(product.priceString)}/month';
    hasTrial = product.introductoryPrice != null;
    hasTrial
        ? product.introductoryPrice!.period == 'P1M'
        ? productTrialPeriodString = '1 month'
        : productTrialPeriodString = null
        : productTrialPeriodString = null;
    hasTrial
        ? productTrialString = 'Get you first $productTrialPeriodString free'
        : null;

    return SizedBox(
      width: 200,
      child: Hero(
        tag: widget.storeProduct.identifier,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: _toggleSelection,
            child: Card(
              elevation: widget.isDisabled
                  ? 1
                  : !widget.isSelected
                      ? 2
                      : 4,
              shadowColor: Colors.black,
              color: Theme.of(context).colorScheme.surface,
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: widget.isSelected ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.surface,
                      width: widget.isSelected ? 2 : 0),
                  borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  if (widget.isSelected)
                    Positioned(
                      right: 10,
                      top: 10,
                      child: Icon(
                        Icons.check_circle,
                        color: Theme.of(context).colorScheme.primaryContainer,
                      ),
                    ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        title: Text(productTitle, style: const TextStyle(fontWeight: FontWeight.bold)),
                        subtitle: Text(
                          productPriceString,
                        ),
                        selected: widget.isDisabled,
                        enabled: !widget.isDisabled,
                      ),
                      const Divider(
                        height: 1,
                      ),
/*                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: ListTile(
                          title: const Text('Entitlements:', style: TextStyle(fontWeight: FontWeight.w500)),
                          subtitle: Text(productDescription),
                          enabled: !widget.isDisabled,
                        ),
                      ),*/

                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: ElevatedButton(
                          onPressed: () {
                            _toggleSelection();
                          },
                          style: ButtonStyle(
                            shadowColor: MaterialStateProperty.resolveWith(
                              (states) {
                                return Colors.black;
                              },
                            ),
                            elevation: MaterialStateProperty.resolveWith(
                              (states) {
                                if (widget.isDisabled) {
                                  return 0;
                                }
                                if (!widget.isSelected) {
                                  return 2;
                                }
                                return 0;
                              },
                            ),
                            backgroundColor: MaterialStateProperty.resolveWith((states) {
                              if (widget.isDisabled) {
                                return Theme.of(context).colorScheme.surface;
                              }
                              if (!widget.isSelected) {
                                return Theme.of(context).colorScheme.primary;
                              }
                              return Colors.teal;
                            }),
                          ),
                          child: Text(widget.isSelected ? "Selected" : "Select",
                              style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: widget.isDisabled ? PageMeThemes.whiteBlackColor(context).withOpacity(0.4) : Colors.white,
                              ))),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
