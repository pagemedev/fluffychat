import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/subscriptions/store_config.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:purchases_flutter/purchases_flutter.dart';


import '../../utils/logger_functions.dart';

class SubscriptionsCubit extends Cubit<SubscriptionsState> {
  static final SubscriptionsCubit _singleton = SubscriptionsCubit._();
  bool _initialized = false;
  bool get initialized => _initialized;
  static const String subscriptionEntitlement = 'basic';

  factory SubscriptionsCubit() => _singleton;

  SubscriptionsCubit._()
      : super(const SubscriptionsState(
          isLoading: false,
          offerings: null,
          customerInfo: null,
          errorMessage: null,
          basicAnnual: null,
          basicMonthly: null,
          annualPackage: null,
          monthlyPackage: null,
          selectedProduct: null,
          isSubscribed: false,
          isLoggedIn: false,
        ));

  Future<void> initialize() async {
    if (_initialized) return;
    emit(state.copyWith(isLoading: true));
    Logs().i( 'Subscriptions Cubit - Initializing');
    _initStore();
    await initPlatformState();
    await fetchOfferings();
    emit(state.copyWith(isLoading: false));
    Logs().i( 'Subscriptions Cubit - Initialization Complete');
  }

  void setSelectedProduct(StoreProduct? storeProduct) {
    emit(state.copyWith(selectedProduct: storeProduct));
  }

  void _initStore() {
    if (Platform.isIOS || Platform.isMacOS) {
      StoreConfig(
        store: Store.appStore,
        apiKey: 'appl_ApNfGdmvzNyOIrhuLcBuLzdcIgr',
      );
    } else if (Platform.isAndroid) {
      StoreConfig(
        store: Store.playStore,
        apiKey: 'goog_RUuihkEnPnzvzpVjZWGiHfalVPh',
      );
    }
  }

  Future<void> initPlatformState() async {
    await Purchases.setDebugLogsEnabled(true);
    final PurchasesConfiguration configuration = PurchasesConfiguration(StoreConfig.instance!.apiKey);
    if (PlatformInfos.isIOS || PlatformInfos.isMacOS){
     configuration.usesStoreKit2IfAvailable =true;
    }
    await Purchases.configure(configuration).whenComplete(() => _initialized = true);
  }

  Future<void> fetchCustomerInfo() async {
    emit(state.copyWith(isLoading: true));
    final fetchedCustomerInfo = await Purchases.getCustomerInfo();
    Logs().i( fetchedCustomerInfo.originalAppUserId);
    setSubscriptionStatus(fetchedCustomerInfo);
    emit(state.copyWith(isLoading: false));
  }

  /// This is a login function that logs the user into RevenueCat. Once the user is logged in, the subscription status is checked,
  /// set and emitted. Then the if the user is not subscribed and there are no selectedProduct, the user is navigated to the subscriptions page.
  /// Otherwise is the user is not subscribed and selectedProduct is not null, a purchase is initiated.
  Future<void> login({required String appUserId}) async {
    emit(state.copyWith(isLoading: true));
    LogInResult? logInResult;
    Logs().i("Subscriptions login request: AppUserId - $appUserId");
    try {
      bool hasTimedOut = false;
      Timer(const Duration(seconds: 5), () => hasTimedOut = true);
      await Future.doWhile(() async {
        await Future.delayed(const Duration(milliseconds: 100));
        if (initialized == true || hasTimedOut) {
          return false;
        }
        return true;
      });
      Logs().v("Subscriptions: isConfigured $initialized");
      logInResult = await Purchases.logIn(appUserId);
      emit(
        state.copyWith(isLoggedIn: true),
      );
      Logs().i( 'SubscriptionsCubit(): login - isLoggedIn: ${state.isLoggedIn}');
      if (logInResult.created) {
        Logs().v( 'SubscriptionsCubit: login() - User was created for the first time on the RevenueCat Backend');
      }
      setSubscriptionStatus(logInResult.customerInfo);
    } on PlatformException catch (e) {
      final errorCode = PurchasesErrorHelper.getErrorCode(e);
      emit(state.copyWith(isLoggedIn: false));
      Logs().e("SubscriptionsCubit - login:  ${errorCode.toString()}");
    }
    emit(state.copyWith(isLoading: false));
  }

  Future<void> logout() async {
    emit(state.copyWith(isLoading: true));

    CustomerInfo customerInfo;
    try {
      customerInfo = await Purchases.logOut();
      setSubscriptionStatus(customerInfo);
      emit(state.copyWith(isLoggedIn: false));
    } on PlatformException catch (e) {
      final errorCode = PurchasesErrorHelper.getErrorCode(e);
      if (errorCode == PurchasesErrorCode.logOutWithAnonymousUserError) {
        loggerError(logMessage: 'Called logOut but the current user is anonymous.');
      } else {
        loggerError(logMessage: errorCode.toString());
      }
    } finally {
      emit(state.copyWith(isLoading: false));
    }
  }

  /// Sets and emits the new state of whether the user is subscribed.
  void setSubscriptionStatus(CustomerInfo customerInfo) {
    final isSubscribed = customerInfo.entitlements.all[subscriptionEntitlement]?.isActive ?? false;
    Logs().i( 'SubscriptionsController: setSubscriptionStatus - $isSubscribed');
    emit(state.copyWith(isSubscribed: isSubscribed, customerInfo: customerInfo));
  }

  Future<String> purchaseProduct() async {
    emit(state.copyWith(isLoading: true));
    {
      try {
        final customerInfo = await Purchases.purchaseStoreProduct(state.selectedProduct!);
        setSubscriptionStatus(customerInfo);
        return 'Payment successful';
      } on PlatformException catch (e) {
        final errorCode = PurchasesErrorHelper.getErrorCode(e);
        if (errorCode == PurchasesErrorCode.purchaseCancelledError) {
          loggerError(logMessage: 'User cancelled');
          return 'User cancelled';
        } else if (errorCode == PurchasesErrorCode.productAlreadyPurchasedError) {
          loggerError(logMessage: 'User already owns this product');
          final customerInfo = await Purchases.restorePurchases();
          setSubscriptionStatus(customerInfo);
          return 'User already owns this product, busy restoring purchase.';
        } else if (errorCode == PurchasesErrorCode.purchaseNotAllowedError) {
          loggerError(logMessage: 'User not allowed to purchase');
          return 'User not allowed to purchase';
        } else if (errorCode == PurchasesErrorCode.paymentPendingError) {
          loggerError(logMessage: 'Payment is pending');
          return 'Payment is pending';
        } else if (errorCode == PurchasesErrorCode.invalidReceiptError) {
          loggerError(logMessage: 'The purchased product was missing in the receipt');
          final customerInfo = await Purchases.restorePurchases();
          setSubscriptionStatus(customerInfo);
          return 'The purchased product was missing in the receipt';
        }else {
          loggerError(logMessage: errorCode.toString());
          return errorCode.toString();
        }
      } finally {
        emit(state.copyWith(isLoading: false));
      }
    }
  }

  Future<void> fetchOfferings() async {
    emit(state.copyWith(isLoading: true));
    Offerings? fetchedOfferings;
    Package? monthlyPackage;
    Package? annualPackage;
    StoreProduct? basicMonthly;
    StoreProduct? basicAnnual;
    try {
      fetchedOfferings = await Purchases.getOfferings();
      monthlyPackage = fetchedOfferings.current!.monthly;
      annualPackage = fetchedOfferings.current!.annual;
      basicMonthly = monthlyPackage!.storeProduct;
      basicAnnual = annualPackage!.storeProduct;
      emit(
        state.copyWith(
          offerings: fetchedOfferings,
          monthlyPackage: monthlyPackage,
          annualPackage: annualPackage,
          basicMonthly: basicMonthly,
          basicAnnual: basicAnnual,
        ),
      );
    } on PlatformException catch (e) {
      loggerError(logMessage: e.toString());
    } finally {
      emit(state.copyWith(isLoading: false));
    }
  }
}

class SubscriptionsState {
  final CustomerInfo? customerInfo;
  final Offerings? offerings;
  final bool isLoading;
  final String? errorMessage;
  final Package? monthlyPackage;
  final Package? annualPackage;
  final StoreProduct? basicMonthly;
  final StoreProduct? basicAnnual;
  final StoreProduct? selectedProduct;
  final bool isSubscribed;
  final bool isLoggedIn;

//<editor-fold desc="Data Methods">

  const SubscriptionsState({
    this.customerInfo,
    this.offerings,
    required this.isLoading,
    this.errorMessage,
    this.monthlyPackage,
    this.annualPackage,
    this.basicMonthly,
    this.basicAnnual,
    this.selectedProduct,
    required this.isSubscribed,
    required this.isLoggedIn,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SubscriptionsState &&
          runtimeType == other.runtimeType &&
          customerInfo == other.customerInfo &&
          offerings == other.offerings &&
          isLoading == other.isLoading &&
          errorMessage == other.errorMessage &&
          monthlyPackage == other.monthlyPackage &&
          annualPackage == other.annualPackage &&
          basicMonthly == other.basicMonthly &&
          basicAnnual == other.basicAnnual &&
          selectedProduct == other.selectedProduct &&
          isSubscribed == other.isSubscribed &&
          isLoggedIn == other.isLoggedIn);

  @override
  int get hashCode =>
      customerInfo.hashCode ^
      offerings.hashCode ^
      isLoading.hashCode ^
      errorMessage.hashCode ^
      monthlyPackage.hashCode ^
      annualPackage.hashCode ^
      basicMonthly.hashCode ^
      basicAnnual.hashCode ^
      selectedProduct.hashCode ^
      isSubscribed.hashCode ^
      isLoggedIn.hashCode;

  @override
  String toString() {
    return 'SubscriptionsState{ customerInfo: $customerInfo, offerings: $offerings, isLoading: $isLoading, errorMessage: $errorMessage, monthlyPackage: $monthlyPackage, annualPackage: $annualPackage, basicMonthly: $basicMonthly, basicAnnual: $basicAnnual, selectedProduct: $selectedProduct, isSubscribed: $isSubscribed, isLoggedIn: $isLoggedIn,}';
  }

  SubscriptionsState copyWith({
    CustomerInfo? customerInfo,
    Offerings? offerings,
    bool? isLoading,
    String? errorMessage,
    Package? monthlyPackage,
    Package? annualPackage,
    StoreProduct? basicMonthly,
    StoreProduct? basicAnnual,
    StoreProduct? selectedProduct,
    bool? isSubscribed,
    bool? isLoggedIn,
  }) {
    return SubscriptionsState(
      customerInfo: customerInfo ?? this.customerInfo,
      offerings: offerings ?? this.offerings,
      isLoading: isLoading ?? this.isLoading,
      errorMessage: errorMessage ?? this.errorMessage,
      monthlyPackage: monthlyPackage ?? this.monthlyPackage,
      annualPackage: annualPackage ?? this.annualPackage,
      basicMonthly: basicMonthly ?? this.basicMonthly,
      basicAnnual: basicAnnual ?? this.basicAnnual,
      selectedProduct: selectedProduct ?? this.selectedProduct,
      isSubscribed: isSubscribed ?? this.isSubscribed,
      isLoggedIn: isLoggedIn ?? this.isLoggedIn,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'customerInfo': customerInfo,
      'offerings': offerings,
      'isLoading': isLoading,
      'errorMessage': errorMessage,
      'monthlyPackage': monthlyPackage,
      'annualPackage': annualPackage,
      'basicMonthly': basicMonthly,
      'basicAnnual': basicAnnual,
      'selectedProduct': selectedProduct,
      'isSubscribed': isSubscribed,
      'isLoggedIn': isLoggedIn,
    };
  }

  factory SubscriptionsState.fromMap(Map<String, dynamic> map) {
    return SubscriptionsState(
      customerInfo: map['customerInfo'] as CustomerInfo,
      offerings: map['offerings'] as Offerings,
      isLoading: map['isLoading'] as bool,
      errorMessage: map['errorMessage'] as String,
      monthlyPackage: map['monthlyPackage'] as Package,
      annualPackage: map['annualPackage'] as Package,
      basicMonthly: map['basicMonthly'] as StoreProduct,
      basicAnnual: map['basicAnnual'] as StoreProduct,
      selectedProduct: map['selectedProduct'] as StoreProduct,
      isSubscribed: map['isSubscribed'] as bool,
      isLoggedIn: map['isLoggedIn'] as bool,
    );
  }

//</editor-fold>
}

/*



import 'dart:io';
import 'package:flutter/services.dart';
import 'package:pageMe/pages/subscriptions/store_config.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

import '../../utils/logger_functions.dart';

class SubscriptionsController {
  static final SubscriptionsController _instance = SubscriptionsController._internal();
  CustomerInfo? customerInfo;
  late final Offerings? offerings;
  Purchases? purchaserInfo;
  bool isLoading = true;
  String? errorMessage;
  String? selectedProduct;

  Offering? currentOffering;
  Package? monthlyPackage;
  Package? annualPackage;

  StoreProduct? basicMonthly;
  StoreProduct? basicYearly;

  factory SubscriptionsController() {
    return _instance;
  }

  SubscriptionsController._internal() {
    initialize();
  }

  Future<void> initialize() async {
    _initStore();
    _initPlatformState();
    await fetchData();
  }


  void _initStore() {
    if (Platform.isIOS || Platform.isMacOS) {
      StoreConfig(
        store: Store.appStore,
        apiKey: '',
      );
    } else if (Platform.isAndroid) {
      StoreConfig(
        store: Store.playStore,
        apiKey: 'goog_RUuihkEnPnzvzpVjZWGiHfalVPh',
      );
    }
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> _initPlatformState() async {
    await Purchases.setDebugLogsEnabled(true);

    final PurchasesConfiguration configuration = PurchasesConfiguration(StoreConfig.instance!.apiKey);

    await Purchases.configure(configuration);

    final fetchedCustomerInfo = await Purchases.getCustomerInfo();
    Logs().i( fetchedCustomerInfo.originalAppUserId);

    Purchases.addReadyForPromotedProductPurchaseListener((productID, startPurchase) async {
      print('Received readyForPromotedProductPurchase event for '
          'productID: $productID');

      try {
        final purchaseResult = await startPurchase.call();
        print('Promoted purchase for productID '
            '${purchaseResult.productIdentifier} completed, or product was'
            'already purchased. customerInfo returned is:'
            ' ${purchaseResult.customerInfo}');
      } on PlatformException catch (e) {
        print('Error purchasing promoted product: ${e.message}');
      }
    });
      customerInfo = fetchedCustomerInfo;

  }

  void createProducts(){
    Logs().i( 'Creating products - Complete');
    currentOffering = offerings!.current!;
    monthlyPackage = currentOffering!.monthly;
    Logs().i( offerings!.all.toString());
    annualPackage = currentOffering!.annual;
    basicMonthly = monthlyPackage!.storeProduct;
    basicYearly = annualPackage!.storeProduct;
    Logs().i( 'Creating products - Complete');
  }

  Future<void> fetchData() async {
    Logs().i( 'Fetching Info - Started');
    Offerings? fetchedOfferings;
    try {
      fetchedOfferings = await Purchases.getOfferings();
    } on PlatformException catch (e) {
      print(e);
    }
      offerings = fetchedOfferings;
    Logs().i( 'Fetching Info - Complete\n${offerings.toString()}');
  }

}
*/
