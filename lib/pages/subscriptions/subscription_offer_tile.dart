
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/platform_infos.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:purchases_flutter/models/store_product_wrapper.dart';

import '../../keys/pageme_client_appkey.dart';
import '../../widgets/matrix.dart';

class SubscriptionProductTile extends StatefulWidget {
  final StoreProduct storeProduct;
  final Function(StoreProduct? testOffer) onSelected;
  final String? subscriptionDescription;
  final bool isDisabled;
  final bool isSelected;
  final bool trialEnded;

  const SubscriptionProductTile({
    Key? key,
    required this.onSelected,
    required this.isDisabled,
    required this.isSelected,
    required this.storeProduct,
    required this.trialEnded,
    this.subscriptionDescription,
  }) : super(key: key);

  @override
  SubscriptionProductTileState createState() => SubscriptionProductTileState();
}

class SubscriptionProductTileState extends State<SubscriptionProductTile> {
  late final ExpansionTileController expansionTileController;
  late final MatrixState matrixState;

  void _toggleSelection() {
    if (!widget.isSelected) {
      widget.onSelected(widget.storeProduct);
    } else {
      widget.onSelected(null);
    }
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    properties.add(DiagnosticsProperty<bool>("isDisabled", widget.isDisabled));
    properties.add(DiagnosticsProperty<bool>("isSelected", widget.isSelected));
    super.debugFillProperties(properties);
  }

  @override
  void initState() {
    expansionTileController = ExpansionTileController();
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    matrixState = Matrix.of(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final StoreProduct product;
    final String productTitle;
    final String productDescription;
    final String productPriceString;
    final bool isAnnual;
    final bool hasTrial;
    final bool trialEnded;
    String? productTrialString;
    String? productTrialPeriodString;

    trialEnded = widget.trialEnded;
    product = widget.storeProduct;
    productTitle = product.title.replaceFirst('plan (PageMe Public)', '');
    productDescription = product.description;
    isAnnual = product.subscriptionPeriod == 'P1Y';
    isAnnual
        ? productPriceString = '${(product.priceString)}${trialEnded ? '/year' : '/year after trial ends'}'
        : productPriceString = '${(product.priceString)}${trialEnded ? '/month' : '/month after trial ends'}';
    hasTrial = product.introductoryPrice != null;
    hasTrial
        ? product.introductoryPrice!.period == 'P1M'
            ? productTrialPeriodString = '1 month'
            : productTrialPeriodString = null
        : productTrialPeriodString = null;
    hasTrial ? productTrialString = 'Get you first $productTrialPeriodString free' : null;

    return SizedBox(
      width: double.infinity,
      child: Hero(
        tag: widget.storeProduct.identifier,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: widget.isDisabled
                ? 1
                : !widget.isSelected
                    ? 2
                    : 4,
            shadowColor: Colors.black,
            color: Theme.of(context).colorScheme.surface,
            child: ExpansionTile(
              initiallyExpanded: widget.isSelected,
              controller: expansionTileController,
              leading: widget.isSelected ? null : const Icon(Icons.info_outline),
              title: Text(productTitle, style: const TextStyle(fontWeight: FontWeight.bold)),
              subtitle: Text(
                productPriceString,
              ),
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: widget.isSelected ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.surface,
                      width: widget.isSelected ? 2 : 0),
                  borderRadius: BorderRadius.circular(20)),
              trailing: Icon(
                widget.isSelected ? Icons.check_circle : Icons.radio_button_unchecked_outlined,
                color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryContainer,
              ),
              onExpansionChanged: (isExpanded) => _toggleSelection(),
              expandedAlignment: Alignment.topLeft,
              expandedCrossAxisAlignment: CrossAxisAlignment.start,
              childrenPadding: const EdgeInsets.only(left: 16, right: 16, bottom: 8),
              children: [
                const Text(
                  "What you get:",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 8,
                ),
                widget.subscriptionDescription != null
                    ? Text(
                        widget.subscriptionDescription!,
                        textAlign: TextAlign.left,
                      )
                    : const CircularProgressIndicator(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
