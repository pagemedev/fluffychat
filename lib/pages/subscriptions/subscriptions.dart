import 'dart:io';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageme_client/pageme_client.dart' as pageme;
import 'package:pageMe/pages/subscriptions/store_config.dart';
import 'package:pageMe/pages/subscriptions/subscriptions_cubit.dart';
import 'package:pageMe/pages/subscriptions/subscriptions_first_time_view.dart';
import 'package:pageMe/pages/subscriptions/subscriptions_view.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../../config/setting_keys.dart';
import '../../keys/pageme_client_appkey.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/matrix.dart';
import '../../utils/famedlysdk_store.dart' as store;

class Subscriptions extends StatefulWidget {
  final bool firstTimeView;

  const Subscriptions({
    Key? key,
    required this.firstTimeView,
  }) : super(key: key);

  @override
  SubscriptionsViewController createState() => SubscriptionsViewController();
}

class SubscriptionsViewController extends State<Subscriptions> {
  StoreProduct? selectedOffering;
  bool? firstTimeUser;
  bool isLoading = false;
  Future<dynamic>? profileFuture;
  Profile? profile;
  bool profileUpdated = false;
  late SubscriptionsCubit subscriptionsCubit;
  late MatrixState matrixState;
  late ThemeData theme;
  late L10n? l10n;
  String? subscriptionDescription;
  bool shouldCallBeforeLeave = true;

  @override
  void didChangeDependencies() {
    matrixState = Matrix.of(context);
    subscriptionsCubit = BlocProvider.of<SubscriptionsCubit>(context);
    theme = Theme.of(context);
    l10n = L10n.of(context);
    super.didChangeDependencies();
  }

  Future<String?> getSubscriptionDescription({required String identifier, required String store}) async {
    Logs().i("Get subscription for: $identifier and $store");
    final pageme.Subscription? subscription = await matrixState.pagemeClient.subscription.getSubscription(identifier, store, appKey: PageMeClientAppKey.appKey);
    if (subscription == null) {
      Logs().i("Could not find subscription in server-pod");
      return null;
    }
    return subscription.description;
  }

  void previousPage() {
    if (mounted) {
      setState(() {
        context.go('/login');
      });
    }
  }

  void nextPage() {
    if (mounted) {
      setState(() {
        context.go('/login/signup');
      });
    }
  }

  /// Checks the
  Future<bool> checkFirstTimeUser() async {
    final Client client = matrixState.client;
    final bool firstTimeUser;
    if (client.userID != null) {
      firstTimeUser = await store.Store().getItemBool(SettingKeys.firstTimeUser + client.userID!, true);
    } else {
      firstTimeUser = false;
    }
    return firstTimeUser;
  }

  Future<void> purchaseProduct({required bool isRenewal}) async {
    final whiteBlackColor = PageMeThemes.whiteBlackColor(context);
    final scaffoldMessengerState = ScaffoldMessenger.of(context);
    setState(() {
      isLoading = true;
    });
    final String result = await subscriptionsCubit.purchaseProduct();
    firstTimeUser = await checkFirstTimeUser();
    if (subscriptionsCubit.state.isSubscribed) {
      setState(() {
        shouldCallBeforeLeave = false;
      });
      context.go(isRenewal ? '/rooms' :  firstTimeUser! ? '/onboard' : '/rooms');
      setState(() {
        isLoading = false;
      });
    } else {
      scaffoldMessengerState.showSnackBar(
        SnackBar(
          margin: const EdgeInsets.only(bottom: 120, right: 22, left: 22),
          backgroundColor: theme.primaryColor,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                result,
                textAlign: TextAlign.center,
                style: TextStyle(color: whiteBlackColor),
              ),
            ],
          ),
        ),
      );
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<void> logoutAction() async {
    if (await showOkCancelAlertDialog(
          useRootNavigator: true,
          context: context,
          title: l10n!.areYouSureYouWantToLogout,
          okLabel: l10n!.yes,
          cancelLabel: l10n!.cancel,
        ) ==
        OkCancelResult.cancel) {
      return;
    }
    await showFutureLoadingDialog(
      context: context,
      future: () async {
        return await matrixState.client.logout();
      },
    );
  }

  String getStore() {
    final String store;
    if (PlatformInfos.isAndroid) {
      store = 'play_store';
    } else {
      store = 'app_store';
    }
    return store;
  }

  void selectOffering(StoreProduct? offering) async {
    if (offering != null) {
      setState(() {
        selectedOffering = offering;
        subscriptionsCubit.setSelectedProduct(selectedOffering);
      });
      await getSubscriptionDescription(identifier: offering.identifier.split(':').first, store: getStore()).then(
        (value) => setState(
          () {
            subscriptionDescription = value;
          },
        ),
      );
    } else {
      setState(() {
        selectedOffering = offering;
        subscriptionsCubit.setSelectedProduct(selectedOffering);
      });
    }
    Logs().i('Selected: ${selectedOffering.toString()}');
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.firstTimeView) {
      final client = Matrix.of(context).client;
      profileFuture ??= client
          .getProfileFromUserId(
        client.userID!,
        cache: !profileUpdated,
        getFromRooms: !profileUpdated,
      )
          .then((p) {
        if (mounted) setState(() => profile = p);
        return p;
      });
    }
    return WillPopScope(
      onWillPop: () async {
        previousPage();
        return false;
      },
      child: widget.firstTimeView
          ? SubscriptionsFirstTimeView(controller: this)
          : SubscriptionsView(
              controller: this,
            ),
    );
  }
}
