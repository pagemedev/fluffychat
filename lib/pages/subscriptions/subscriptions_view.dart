import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/subscriptions/subscription_offer_tile.dart';
import 'package:pageMe/pages/subscriptions/subscriptions.dart';
import 'package:pageMe/pages/subscriptions/subscriptions_cubit.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../../config/themes.dart';

class SubscriptionsView extends StatelessWidget {
  final SubscriptionsViewController controller;
  static const double assetWidth = 350;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 16.0);
  static const titleStyle = TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0);

  const SubscriptionsView({Key? key, required this.controller}) : super(key: key);

  Widget buildTitleSVGWidget(String title, String asset, [double width = 250]) {
    return Column(
      children: [
        buildSVG(asset, width),
        const SizedBox(
          height: 20,
        ),
        Text(
          title,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }

  Widget buildSVG(String assetName, [double width = assetWidth]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight),
        child: SvgPicture.asset('assets/$assetName', width: width),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (controller.shouldCallBeforeLeave) {
          return false;
        }
        return true;
      },
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
          controller.selectOffering(null);
        },
        child: Scaffold(
          extendBody: true,
          appBar: AppBar(
            scrolledUnderElevation: 0,
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            elevation: 0,
            //toolbarHeight: 60,
            centerTitle: true,
            title: SizedBox(
              width: 110,
              height: globalHeaderHeight,
              child: SvgPicture.asset(
                (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          body: Column(
            children: [
              SubscriptionHeading(
                controller: controller,
              ),
              Expanded(
                child: BlocBuilder<SubscriptionsCubit, SubscriptionsState>(
                  builder: (context, subscriptionsState) {
                    return (subscriptionsState.monthlyPackage != null && subscriptionsState.annualPackage != null)
                        ? ListView(
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            children: [
                              SubscriptionProductTile(
                                key: UniqueKey(),
                                trialEnded: true,
                                subscriptionDescription: controller.subscriptionDescription,
                                storeProduct: subscriptionsState.basicMonthly!,
                                onSelected: (offering) {
                                  controller.selectOffering(offering);
                                },
                                isSelected: ((controller.selectedOffering != null)
                                    ? controller.selectedOffering!.identifier == subscriptionsState.basicMonthly!.identifier
                                    : false),
                                isDisabled: (controller.selectedOffering != null)
                                    ? controller.selectedOffering!.identifier != subscriptionsState.basicMonthly!.identifier
                                    : false,
                              ),
                              SubscriptionProductTile(
                                key: UniqueKey(),
                                trialEnded: true,
                                subscriptionDescription: controller.subscriptionDescription,
                                storeProduct: subscriptionsState.basicAnnual!,
                                onSelected: (offering) {
                                  controller.selectOffering(offering);
                                },
                                isSelected: ((controller.selectedOffering != null)
                                    ? controller.selectedOffering!.identifier == subscriptionsState.basicAnnual!.identifier
                                    : false),
                                isDisabled: (controller.selectedOffering != null)
                                    ? controller.selectedOffering!.identifier != subscriptionsState.basicAnnual!.identifier
                                    : false,
                              ),
                            ],
                          )
                        : const Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: 8.0, left: 20, right: 20),
                                child: SizedBox(
                                  width: 50,
                                  height: 50,
                                  child: CircularProgressIndicator(),
                                ),
                              )
                            ],
                          );
                  },
                ),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Currently signed in as: ${controller.profile?.displayName ?? controller.profile?.userId}"),
                      TextButton(
                        onPressed: () async {
                          await controller.logoutAction();
                        },
                        child: Text(
                          L10n.of(context)!.logout,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
                child: SizedBox(
                  width: double.infinity,
                  height: 60,
                  child: ElevatedButton(
                    onPressed: controller.selectedOffering == null ? () {} : () async => controller.purchaseProduct(isRenewal: true),
                    style: ButtonStyle(
                      shadowColor: MaterialStateProperty.resolveWith(
                        (states) {
                          return Colors.black;
                        },
                      ),
                      elevation: MaterialStateProperty.resolveWith(
                        (states) {
                          if (controller.selectedOffering == null) {
                            return 0;
                          }
                          return 2;
                        },
                      ),
                      backgroundColor: MaterialStateProperty.resolveWith(
                        (states) {
                          if (controller.selectedOffering == null) {
                            return Theme.of(context).colorScheme.secondaryContainer;
                          }
                          return Theme.of(context).colorScheme.primaryContainer;
                        },
                      ),
                    ),
                    child: controller.isLoading
                        ? const LinearProgressIndicator(
                            color: Colors.white,
                            backgroundColor: Colors.black54,
                          )
                        : Text(
                            "Purchase subscription",
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              color: (controller.selectedOffering == null)
                                  ? Theme.of(context).colorScheme.onSecondaryContainer
                                  : Theme.of(context).colorScheme.onPrimaryContainer,
                            ),
                          ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SubscriptionHeading extends StatelessWidget {
  final SubscriptionsViewController controller;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 16.0);
  static const titleStyle = TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0);
  const SubscriptionHeading({Key? key, required this.controller}) : super(key: key);

  Widget buildSVG(String assetName, [double width = 250, Color? color]) {
    return SafeArea(
      child: SvgPicture.asset('assets/$assetName', width: width, color: color),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 0, left: 12, right: 12),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                text: 'Your ',
                style: titleStyle.copyWith(color: PageMeThemes.whiteBlackColor(context)),
                children: <TextSpan>[
                  TextSpan(text: 'subscription', style: titleStyle.copyWith(color: Theme.of(context).colorScheme.primary)),
                  const TextSpan(text: ' has'),
                  TextSpan(text: ' expired', style: titleStyle.copyWith(color: Theme.of(context).colorScheme.primary)),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5, left: 12, right: 12, bottom: globalHeaderHeight),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              'Choose a subscription plan!',
              textAlign: TextAlign.center,
              style: titleStyle.copyWith(fontSize: 18, fontWeight: FontWeight.w500),
            ),
          ),
        ),
        AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          height: (controller.selectedOffering == null) ? MediaQuery.sizeOf(context).height / 6 : 0,
          child: buildSVG('subscribe_pay.svg'),
        ),
      ],
    );
  }
}
