import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';


import 'package:pageMe/pages/settings_multiple_emotes/settings_multiple_emotes.dart';
import 'package:pageMe/widgets/matrix.dart';

class MultipleEmotesSettingsView extends StatelessWidget {
  final MultipleEmotesSettingsController controller;

  const MultipleEmotesSettingsView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final room = Matrix.of(context).client.getRoomById(controller.roomId!)!;
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        leading: const BackButton(),
        title: Text(
          L10n.of(context)!.emotePacks,
          style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
        ),
      ),
      body: StreamBuilder(
        stream: room.onUpdate.stream,
        builder: (context, snapshot) {
          final Map<String, Event?> packs = room.states['im.ponies.room_emotes'] ?? <String, Event>{};
          if (!packs.containsKey('')) {
            packs[''] = null;
          }
          final keys = packs.keys.toList();
          keys.sort();
          return ListView.separated(
              separatorBuilder: (BuildContext context, int i) => Container(),
              itemCount: keys.length,
              itemBuilder: (BuildContext context, int i) {
                final event = packs[keys[i]];
                String? packName = keys[i].isNotEmpty ? keys[i] : 'Default Pack';
                if (event != null && event.content['pack'] is Map) {
                  if ((event.content['pack'] as Map)['displayname'] is String) {
                    packName = (event.content['pack'] as Map)['displayname'];
                  } else if ((event.content['pack'] as Map)['name'] is String) {
                    packName = (event.content['pack'] as Map)['name'];
                  }
                }
                return ListTile(
                  title: Text(packName!),
                  onTap: () async {
                    context.go('/rooms/${room.id}/details/emotes/${keys[i]}');
                  },
                );
              });
        },
      ),
    );
  }
}
