import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageme_client/pageme_client.dart' as pageme;

import '../../config/themes.dart';
import 'homeserver_picker.dart';

class HomeServerPickerView extends StatelessWidget {
  final HomeserverPickerController controller;
  static const double assetWidth = 350;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 19.0);
  static const titleStyle = TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0);

  const HomeServerPickerView(this.controller, {Key? key}) : super(key: key);

  Widget buildImage(String assetName, [double? width = assetWidth, Color? color]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight),
        child: Image.asset('assets/$assetName', width: width, color: color),
      ),
    );
  }

  Widget buildSVG(String assetName, [double width = assetWidth, Color? color]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight),
        child: SvgPicture.asset('assets/$assetName', width: width, color: color),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();
    return Scaffold(
      appBar: !PageMeThemes.isColumnMode(context)
          ? AppBar(
              backgroundColor: Theme.of(context).colorScheme.background,
              elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
              toolbarHeight: 100,
              centerTitle: true,
              title: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 40),
                    child: SizedBox(
                      //height: globalHeaderHeight,
                      width: MediaQuery.sizeOf(context).width / 3,
                      child: SvgPicture.asset(
                        (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
              bottom: PageMeThemes.isColumnMode(context)
                  ? PreferredSize(
                      preferredSize: const Size.fromHeight(2.0),
                      child: Container(
                          height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
                          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black //Theme.of(context).colorScheme.primary,
                          ),
                    )
                  : null,
            )
          : PreferredSize(preferredSize: Size.zero, child: Container()),
      body: Stack(
        children: [
          if (PageMeThemes.isColumnMode(context))
            Image.asset(
              Theme.of(context).brightness == Brightness.light ? 'assets/light_background_alt.png' : 'assets/dark_background_alt.png',
              width: double.infinity,
              height: double.infinity,
              fit: BoxFit.cover,
            ),
          Padding(
            padding: PageMeThemes.isColumnMode(context) ? const EdgeInsets.all(8.0) : const EdgeInsets.all(0),
            child: Center(
              child: Material(
                elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
                borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                child: Container(
                  width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                    color: Theme.of(context).scaffoldBackgroundColor,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if (PageMeThemes.isColumnMode(context))
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: SizedBox(
                            width: PageMeThemes.columnWidth * 1.5 / 3,
                            //height: 150,
                            child: SvgPicture.asset(
                              (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                        ),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 16, right: 16, bottom: 40),
                            child: buildSVG(
                              'business.svg',
                              300,
                            ),
                          ),
                          if (controller.companies == null)
                            Container(
                              padding: bodyPadding,
                              child: const Text(
                                'Fetching our business servers.\nPlease wait...',
                                style: bodyStyle,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          if (controller.companies != null)
                            Container(
                              padding: bodyPadding,
                              child: Text(
                                (controller.companies!.isNotEmpty) ? 'Select your business from our list of available servers.' : 'Looks like we were unable to fetch any servers. Please try again...',
                                style: bodyStyle,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          (controller.companies != null || !controller.isFetching)
                              ? Padding(
                                  padding: const EdgeInsets.fromLTRB(35, 16, 35, 16),
                                  child: Container(
                                    padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4),
                                    decoration: BoxDecoration(color: Theme.of(context).colorScheme.primaryContainer, borderRadius: BorderRadius.circular(20)),
                                    child: (controller.companies!.isNotEmpty)
                                        ? DropdownButton<String?>(
                                            isExpanded: false,
                                            underline: Container(),
                                            dropdownColor: Theme.of(context).colorScheme.primaryContainer,
                                            borderRadius: BorderRadius.circular(20),
                                            style: PageMeThemes.loginTextFieldStyle.copyWith(color: Theme.of(context).colorScheme.onPrimaryContainer, fontSize: 20),
                                            hint: Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                                child: Text(
                                                  "Tap to expand list",
                                                  style: TextStyle(color: Theme.of(context).colorScheme.onPrimaryContainer),
                                                )),
                                            value: controller.dropDownValue,
                                            alignment: Alignment.center,
                                            iconEnabledColor: FlavorConfig.primaryColor,
                                            icon: (controller.companies!.isNotEmpty) ? const Icon(Icons.keyboard_arrow_down) : Container(),
                                            items: controller.companies!.map((pageme.CompanyData companyData) {
                                              return DropdownMenuItem(
                                                alignment: Alignment.center,
                                                value: companyData.homeServer!.baseUrl,
                                                child: Text(
                                                  companyData.company.name,
                                                ),
                                              );
                                            }).toList(),
                                            onChanged: (newValue) => controller.onCompanySelected(newValue),
                                          )
                                        : TextButton(
                                            onPressed: () async => await controller.fetchActiveCompanies(),
                                            child: Text(
                                              "Tap to refresh",
                                              style: TextStyle(color: Theme.of(context).colorScheme.onPrimaryContainer, fontSize: 18),
                                            )),
                                  ))
                              : const CircularProgressIndicator(),
                          if (isBusiness())
                            Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Text("Don't see your business?"),
                                  TextButton(
                                    onPressed: () async {
                                      await controller.contactUs();
                                      //controller.openServerRequestForm();
                                      FocusManager.instance.primaryFocus?.unfocus();
                                    },
                                    child: const Text(
                                      'Contact us for a server',
                                      style: TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                  )
                                ],
                              ),
                            )
                        ],
                      )),
                      Padding(
                        padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
                        child: SizedBox(
                          width: PageMeThemes.isColumnMode(context) ? MediaQuery.sizeOf(context).width / 3 : double.infinity,
                          height: 60,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                elevation: MaterialStateProperty.resolveWith(
                                  (states) {
                                    if (controller.homeserverController.text.isEmpty) {
                                      return 0;
                                    }
                                    return null;
                                  },
                                ),
                                backgroundColor: MaterialStateProperty.resolveWith(
                                  (states) {
                                    if (controller.homeserverController.text.isEmpty) {
                                      return Theme.of(context).colorScheme.secondaryContainer;
                                    }
                                    return Theme.of(context).colorScheme.primaryContainer;
                                  },
                                ),
                              ),
                              onPressed: () async {
                                if (controller.homeserverController.text.isEmpty) {
                                  return;
                                }
                                controller.isLoading ? () {} : controller.checkHomeserverAction();
                              },
                              child: controller.isLoading
                                  ? const LinearProgressIndicator(
                                      color: Colors.white,
                                      backgroundColor: Colors.black54,
                                    )
                                  : Text(
                                      "Next",
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                        color: controller.homeserverController.text.isEmpty ? Theme.of(context).colorScheme.onSecondaryContainer : Theme.of(context).colorScheme.onPrimaryContainer,
                                      ),
                                    )),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
