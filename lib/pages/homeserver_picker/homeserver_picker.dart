import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:open_mail_app/open_mail_app.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/keys/pageme_client_appkey.dart';
import 'package:pageMe/pages/homeserver_picker/homeserver_picker_view.dart';
import 'package:pageMe/pages/homeserver_picker/request_form.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:pageme_client/pageme_client.dart' as pageme;

import '../../utils/localized_exception_extension.dart';

class HomeserverPicker extends StatefulWidget {
  const HomeserverPicker({Key? key}) : super(key: key);

  @override
  HomeserverPickerController createState() => HomeserverPickerController();
}

class HomeserverPickerController extends State<HomeserverPicker> {
  bool isLoading = false;
  bool isFetching = false;
  final TextEditingController homeserverController = TextEditingController();
  final FocusNode homeserverFocusNode = FocusNode();
  String? error;
  bool displayServerList = false;
  String searchTerm = '';
  bool _isInitialized = false;
  final TextEditingController nameController = TextEditingController();
  Future<dynamic>? profileFuture;
  Profile? profile;
  bool profileUpdated = false;
  String? homeServerName;
  List<pageme.CompanyData>? companies;
  pageme.CompanyData? company;
  late MatrixState matrix;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const double assetWidth = 350;

  bool loading = false;

  static int sendAttempt = 0;

  String? dropDownValue; //FlavorConfig.homeServerOptions.first["address"]!;

  void updateProfile() => setState(() {
        profileUpdated = true;
        profile = profileFuture = null;
      });

  Future<void> contactUs() async {
    final EmailContent email = EmailContent(
      to: [
        'sales@pageme.co.za',
      ],
      subject: 'PageMe Business: Request for server',
      body: '''
Thank you for your interest in setting up a new server for PageMe Business, our comprehensive messaging solution for businesses. 

To assist you better, please provide the following details:

Name: 
Business Name:
Contact Number: 
Preferred Contact Time:
Business Sector: 
Any Specific Requirements (if any): 

We appreciate your request and aim to respond within 48 hours. 
For any immediate queries, feel free to contact us at +27 64 050 7724 during office hours (8am-4.30pm).

Best Regards,
PageMe Business
+27 64 050 7724
www.pageme.co.za
''',
    );
    loggerDebug(logMessage: 'test');
    final OpenMailAppResult result = await OpenMailApp.composeNewEmailInMailApp(nativePickerTitle: 'Select email app to compose', emailContent: email);
    if (!result.didOpen && !result.canOpen) {
      if (!mounted) return;
      await showOkAlertDialog(
        context: context,
        title: 'Open Mail App',
        message: "No mail apps installed",
      );
    } else if (!result.didOpen && result.canOpen) {
      if (!mounted) return;
      await showDialog(
        context: context,
        builder: (_) => MailAppPickerDialog(
          mailApps: result.options,
          emailContent: email,
        ),
      );
    }
  }

  Widget buildFullscreenImage() {
    return Image.asset(
      'assets/background_dark.png',
      fit: BoxFit.cover,
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
    );
  }

  Widget buildImage(String assetName, [double width = assetWidth]) {
    return Image.asset('assets/$assetName', width: width);
  }

  Widget buildSVG(String assetName, [double width = assetWidth]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight + (4 * globalHeaderPadding)),
        child: SvgPicture.asset('assets/$assetName', width: width),
      ),
    );
  }

  Widget buildSVGFooter(String assetName, [double width = assetWidth]) {
    return SvgPicture.asset('assets/$assetName', width: width);
  }

  Widget buildTitleWidget(Widget child) {
    return SafeArea(
        child: Padding(
      padding: const EdgeInsets.only(top: globalHeaderHeight + (4 * globalHeaderPadding)),
      child: child,
    ));
  }

  Widget buildTitleTextWidget(String title) {
    return SafeArea(
        child: Padding(
      padding: const EdgeInsets.only(top: globalHeaderHeight + (4 * globalHeaderPadding)),
      child: Text(
        title,
        style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
      ),
    ));
  }

  Widget buildTitleImageWidget(String title, String asset) {
    return SafeArea(
        child: Padding(
      padding: const EdgeInsets.only(top: globalHeaderHeight + (4 * globalHeaderPadding)),
      child: Column(
        children: [
          buildImage(asset),
          Text(
            title,
            style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    ));
  }

  Widget buildTitleSVGWidget(String title, String asset, [double width = 250]) {
    return SafeArea(
      child: Column(
        children: [
          buildSVG(asset, width),
          const SizedBox(
            height: 20,
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  void onCompanySelected(String? newValue) {
    company = companies?.singleWhere((element) => element.homeServer?.baseUrl == newValue);
    matrix.homeServer = company?.homeServer;
    setState(() {
      dropDownValue = newValue!;
      homeserverController.text = newValue;
      homeServerName = newValue.replaceFirst(".pageme.co.za", '').toUpperCase();
    });
  }

  void setServer(String server) => setState(() {
        homeserverController.text = server;
        searchTerm = '';
        homeserverFocusNode.unfocus();
        displayServerList = false;
      });

  void nextPage() {
    setState(() {
      if (isBusiness()) {
        context.go('/server_select/login');
      } else {
        context.go('/login');
      }
    });
  }

  /// Starts an analysis of the given homeserver. It uses the current domain and
  /// makes sure that it is prefixed with https. Then it searches for the
  /// well-known information and forwards to the login page depending on the
  /// login type.
  Future<void> checkHomeserverAction() async {
    setState(() {
      homeserverFocusNode.unfocus();
      error = null;
      isLoading = true;
      searchTerm = '';
      displayServerList = false;
    });

    try {
      homeserverController.text = homeserverController.text.trim().toLowerCase().replaceAll(' ', '-');
      var homeserver = Uri.parse(homeserverController.text);
      if (homeserver.scheme.isEmpty) {
        homeserver = Uri.https(homeserverController.text, '');
      }
      matrix.loginHomeServerSummary = await matrix.getLoginClient().checkHomeserver(homeserver);
      final ssoSupported = matrix.loginHomeServerSummary!.loginFlows.any((flow) => flow.type == 'm.login.sso');

      try {
        await matrix.getLoginClient().register();
        matrix.loginRegistrationSupported = true;
      } on MatrixException catch (e) {
        matrix.loginRegistrationSupported = e.requireAdditionalAuthentication;
      }

      if (!ssoSupported && matrix.loginRegistrationSupported == false) {
        // Server does not support SSO or registration. We can skip to login page:
        nextPage();
      } else {
        loggerError(logMessage: 'There should be no registration enabled for business servers. Check urgently');
        nextPage();
      }
    } catch (e) {
      Logs().e("checkHomeserverAction - $e");
      setState(() => error = (e).toLocalizedString(context));
    } finally {
      if (mounted) {
        setState(() => isLoading = false);
      }
    }
  }

  Future<List<pageme.CompanyData>> fetchActiveCompanies() async {
    setState(() {
      isFetching = true;
    });
    List<pageme.CompanyData>? result;
    try {
      result = await matrix.pagemeClient.company.getAllCompanies(appKey: PageMeClientAppKey.appKey);
    } on Exception catch (e) {
      Logs().e("Error fetching list of companies from serverpod. Error: $e");
    }
    if (result == null) {
      setState(() {
        companies = [];
        isFetching = false;
      });
      return [];
    } else {
      setState(() {
        companies = result;
        isFetching = false;
      });
      return companies!;
    }
  }

  void openServerRequestForm() async {
    await showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        constraints: BoxConstraints(
          maxHeight: MediaQuery.sizeOf(context).height*3/4
        ),
        builder: (context) {
          return const RequestForm();
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    matrix = Matrix.of(context);
    if (!_isInitialized) {
      await fetchActiveCompanies();
      _isInitialized = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    matrix.navigatorContext = context;
    return HomeServerPickerView(this);
  }
}
