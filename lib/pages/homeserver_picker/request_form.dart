import 'package:flutter/material.dart';


class RequestForm extends StatefulWidget {
  const RequestForm({super.key});

  @override
  RequestFormState createState() => RequestFormState();
}

class RequestFormState extends State<RequestForm> {
  final _formKey = GlobalKey<FormState>();

  // Define controllers for text fields
  final nameController = TextEditingController();
  final contactNumberController = TextEditingController();
  final emailController = TextEditingController();
  final businessNameController = TextEditingController();
  final companySizeController = TextEditingController();
  final preferredContactTimeController = TextEditingController();
  final specificRequirementsController = TextEditingController();

  @override
  void dispose() {
    // Dispose controllers when the widget is disposed
    nameController.dispose();
    contactNumberController.dispose();
    emailController.dispose();
    businessNameController.dispose();
    companySizeController.dispose();
    preferredContactTimeController.dispose();
    specificRequirementsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Request a PageMe Business Server', maxLines: 2),
          automaticallyImplyLeading: false,
        ),
        bottomNavigationBar:               Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: ElevatedButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                // Process data
              }
            },
            child: const Text('Submit Request'),
          ),
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextFormField(
                  controller: nameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your name';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: contactNumberController,
                  decoration: const InputDecoration(labelText: 'Contact Number'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your contact number';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: emailController,
                  decoration: InputDecoration(labelText: 'Email Address'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your email address';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: businessNameController,
                  decoration: InputDecoration(labelText: 'Business Name'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your business name';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: companySizeController,
                  decoration: InputDecoration(labelText: 'Size of Company'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the size of your company';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: preferredContactTimeController,
                  decoration: InputDecoration(labelText: 'Preferred Contact Time'),
                ),
                TextFormField(
                  controller: specificRequirementsController,
                  decoration: InputDecoration(labelText: 'Specific Requirements'),
                  maxLines: 3,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
