import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'package:pageMe/pages/new_group/new_group.dart';
import 'package:pageMe/widgets/layouts/max_width_body.dart';

class NewGroupView extends StatelessWidget {
  final NewGroupController controller;

  const NewGroupView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.of(context)!.createNewGroup, style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
      ),
      body: MaxWidthBody(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 25.0),
                child: Center(
                  child: GestureDetector(
                    onTap: () async => await controller.selectAvatarAction(),
                    child: Column(
                      children: [
                        Material(
                          borderRadius: BorderRadius.circular(44),
                          elevation: 4,
                          child: CircleAvatar(
                            radius: 44,
                            backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                            backgroundImage: (controller.file != null) ? MemoryImage(controller.file!.bytes) : null,
                            child: (controller.file != null) ?  null : const Icon(Icons.add_a_photo_outlined, size: 30,),
                          ),
                        ),
                        (controller.file != null) ? const Text("Remove"): Container()
                      ],
                    ),
                  ),
                ),
              ),
              const ListTile(
                title: Text("Group Name"),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: TextField(
                  controller: controller.nameController,
                  autofocus: true,
                  autocorrect: false,
                  textInputAction: TextInputAction.next,
                  onSubmitted: controller.submitAction,
                  decoration: InputDecoration(
                      //labelText: "Name",
                      //prefixIcon: const Icon(Icons.people_outlined),
                      hintText: L10n.of(context)!.enterAGroupName),
                ),
              ),
              const ListTile(
                title: Text("Group Topic (Optional)"),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: TextField(
                  controller: controller.topicController,
                  autofocus: true,
                  autocorrect: false,
                  textInputAction: TextInputAction.go,
                  onSubmitted: controller.submitAction,
                  decoration: const InputDecoration(
                      //labelText: "Topic",
                      //prefixIcon: Icon(Icons.people_outlined),
                      hintText: "Enter a topic"),
                ),
              ),
/*            SwitchListTile.adaptive(
                title: Text(L10n.of(context)!.groupIsPublic),
                value: controller.publicGroup,
                onChanged: controller.setPublicGroup,
              ),*/
              const ListTile(
                title: Text("Group Access"),
              ),
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: 2,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      selectedTileColor: Theme.of(context).colorScheme.primaryContainer.withOpacity(0.7),
                      //tileColor: index == controller.selectedIndex ? Colors.red : Colors.yellow,
                        trailing: Checkbox(
                          activeColor: Theme.of(context).colorScheme.primary,
                          value: index == controller.selectedIndex,
                          onChanged: (_) =>controller.setGroupAccess(index),
                        ),
                        title: Text(controller.listGroupAccessTitle[index]),
                        subtitle: Text(controller.listGroupAccessSubtitle[index]),
                        selected: index == controller.selectedIndex,
                        selectedColor:  Theme.of(context).colorScheme.onPrimaryContainer,
                        onTap: () => controller.setGroupAccess(index));
                  }),
              /*const Divider(),
              ListTile(
                title: Text(
                  "Advanced Settings",
                  style: TextStyle(
                    color:
                    Theme.of(context).colorScheme.secondary,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                trailing: Icon(controller.displaySettings
                    ? Icons.keyboard_arrow_down_outlined
                    : Icons.keyboard_arrow_right_outlined),
                onTap: controller.toggleDisplaySettings,
              ),
              const Divider(),*/
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: controller.submitAction,
        child: const Icon(Icons.arrow_forward_outlined),
      ),
    );
  }
}
