import 'dart:async';
import 'dart:math';

import 'package:dynamic_color/dynamic_color.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/archive/archive.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/pages/spaces_drawer/spaces_drawer_view.dart';
import 'package:pageMe/utils/logger_functions.dart';

import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/string_color.dart';
import 'package:qr_flutter/qr_flutter.dart';
import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../../widgets/matrix.dart';
import '../chat_list/chat_list.dart';
import '../subscriptions/subscriptions_cubit.dart';

class SpacesDrawer extends StatefulWidget {
  final ChatListController chatListController;
  const SpacesDrawer({
    Key? key,
    required this.chatListController,
  }) : super(key: key);

  @override
  SpacesDrawerController createState() => SpacesDrawerController();
}

class SpacesDrawerController extends State<SpacesDrawer> {
  Future<dynamic>? profileFuture;
  Profile? profile;
  late final ArchiveController archiveController;


  Future<void> displayQR(BuildContext context) async {
    await showModalBottomSheet(
        context: context,
        backgroundColor: Theme.of(context).backgroundColor,
        useRootNavigator: false,
        builder: (context) {
          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: const Icon(Icons.close_outlined),
                onPressed: Navigator.of(context).pop,
                tooltip: L10n.of(context)!.close,
              ),
              title: Text("${Matrix.of(context).client.userID}"),
            ),
            body: Center(
              child: Container(
                color: Colors.white,
                child: QrImageView(
                  data: 'https://matrix.to/#/${Matrix.of(context).client.userID}',
                  version: QrVersions.auto,
                  size: min(MediaQuery.sizeOf(context).width - 16, 200),
                ),
              ),
            ),
          );
        });
  }

  @override
  void initState() {
    archiveController = ArchiveController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    profileFuture ??= client
        .getProfileFromUserId(
      client.userID!,
      cache: true,
      getFromRooms: true,
    )
        .then((p) {
      if (mounted) setState(() => profile = p);
      return p;
    });
    return Container(
      width: widget.chatListController.isDrawerExpanded ? 200 : 88,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(topRight: Radius.circular(FlavorConfig.borderRadius), bottomRight: Radius.circular(FlavorConfig.borderRadius)),
          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.background.darken(10) : Theme.of(context).appBarTheme.backgroundColor?.darken(10)
      ),

      child: SpacesDrawerView(
        spacesDrawerController: this,
        chatListController: widget.chatListController,
        archiveController: archiveController,
      ),
    );
  }
}

enum AvatarAction { camera, file, remove }
