import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/pages/spaces_drawer/spaces_drawer.dart';


import '../../config/flavor_config.dart';
import '../../widgets/avatar.dart';
import '../../widgets/matrix.dart';

class DrawerTitle extends StatelessWidget {
  const DrawerTitle({
    Key? key,
    this.isExpanded = false,
    required this.spacesDrawerController,
  }) : super(key: key);

  final SpacesDrawerController spacesDrawerController;
  final bool isExpanded;

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    String? homeServerName;
    if (isBusiness()) {
      homeServerName = client.homeserver
          .toString()
          .replaceFirst('https://matrix.', '')
          .replaceFirst('.pageme.co.za', '')
          .toUpperCase();
    } else {
      homeServerName = 'PUBLIC';
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: (56 - 45) / 2),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(
            width: 12,
          ),
          Center(
            child: IconButton(
              style: IconButton.styleFrom(
                elevation: 3,
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(FlavorConfig.borderRadius))),
              onPressed: () => context.go('/settings/account'),
              icon: Material(
                elevation: 4,
                shadowColor: Colors.black,
                borderRadius:
                BorderRadius.circular(FlavorConfig.borderRadius),
                child: Avatar(
                  size: 45,
                  onTap: () => context.go('/settings/account'),
                  name: spacesDrawerController.profile?.displayName ?? '?',
                  mxContent: spacesDrawerController.profile?.avatarUrl,
                  spaceBorderRadius:
                      BorderRadius.circular(FlavorConfig.borderRadius),
                  isSpace: true,
                ),
              ),
            ),
          ),
          if (isExpanded)
            GestureDetector(
              onTap: () => context.go('/settings/account'),
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: SizedBox(
                  width: 75,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          spacesDrawerController.profile?.displayName ??
                              "Unknown User",
                          style: const TextStyle(fontWeight: FontWeight.bold),
                          maxLines: 2,
                        ),
                        Text(homeServerName ?? "-"),
                      ]),
                ),
              ),
            ),
/*          if (isExpanded && !PageMeThemes.isColumnMode(context))
            IconButton(
              icon: const Icon(Icons.qr_code),
              onPressed: () => spacesDrawerController.displayQR(context),
            )*/
        ],
      ),
    );
  }
}
