import 'package:flutter/material.dart';

class SpacesTitle extends StatelessWidget {
  const SpacesTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ListTile(
      title: Padding(
        padding: EdgeInsets.all(8.0),
        child: Text("Spaces"),
      ),
      subtitle: Padding(
        padding: EdgeInsets.all(8.0),
        child: Text("Spaces are a new way to group rooms and people"),
      ),
    );
  }
}