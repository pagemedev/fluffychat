import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/pages/spaces_drawer/spaces_drawer.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

class SpacesFooter extends StatelessWidget {
  final BuildContext context;
  final SpacesDrawerController spacesDrawerController;

  const SpacesFooter({
    Key? key,
    required this.context,
    required this.spacesDrawerController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 2,
          color: Theme.of(context).appBarTheme.backgroundColor,
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            MaterialButton(
                onPressed: () {
                  Scaffold.of(context).closeDrawer();
                  context.go('/settings');
                },
                child: Row(
                  children: [
                    const Icon(Icons.settings, size: 20),
                    const SizedBox(width: 5),
                    Text(L10n.of(context)!.settings),
                  ],
                )),

          ],
        )
      ],
    );
  }
}
