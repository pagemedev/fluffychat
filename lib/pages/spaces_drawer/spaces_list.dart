import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/pages/chat_list/spaces_entry.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/widgets/avatar.dart';

import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../../utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import '../../widgets/matrix.dart';
import '../../widgets/unread_rooms_badge.dart';
import '../chat_list/chat_list.dart';
import '../chat_list/navi_rail_item.dart';

class SpacesList extends StatelessWidget {
  const SpacesList({
    Key? key,
    this.isExpanded = false,
    required this.chatListController,
    required this.spaceHierarchy,
    required this.context,
    required this.currentIndex,
  }) : super(key: key);

  final ChatListController chatListController;
  final Map<SpacesEntry, dynamic> spaceHierarchy;
  final BuildContext context;
  final int currentIndex;
  final bool isExpanded;

  List<NavigationDestination> getNavigationDestinations(BuildContext context) {
    final badgePosition = BadgePosition.topEnd(top: -16, end: -12);
    return [
      if (FlavorConfig.separateChatTypes) ...[
        NavigationDestination(
          icon: UnreadRoomsBadge(
            badgePosition: badgePosition,
            filter: chatListController.getRoomFilterByActiveFilter(ActiveFilter.groups),
            child: const Icon(Icons.groups_outlined),
          ),
          selectedIcon: UnreadRoomsBadge(
            badgePosition: badgePosition,
            filter: chatListController.getRoomFilterByActiveFilter(ActiveFilter.groups),
            child: const Icon(Icons.groups),
          ),
          label: L10n.of(context)!.groups,
        ),
        NavigationDestination(
          icon: UnreadRoomsBadge(
            badgePosition: badgePosition,
            filter: chatListController.getRoomFilterByActiveFilter(ActiveFilter.messages),
            child: const Icon(Icons.chat_outlined),
          ),
          selectedIcon: UnreadRoomsBadge(
            badgePosition: badgePosition,
            filter: chatListController.getRoomFilterByActiveFilter(ActiveFilter.messages),
            child: const Icon(Icons.chat),
          ),
          label: L10n.of(context)!.messages,
        ),
      ] else
        NavigationDestination(
          icon: UnreadRoomsBadge(
            badgePosition: badgePosition,
            filter: chatListController.getRoomFilterByActiveFilter(ActiveFilter.allChats),
            child: const Icon(Icons.chat_outlined),
          ),
          selectedIcon: UnreadRoomsBadge(
            badgePosition: badgePosition,
            filter: chatListController.getRoomFilterByActiveFilter(ActiveFilter.allChats),
            child: Icon(Icons.chat, color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSecondary),
          ),
          label: L10n.of(context)!.chats,
        ),
      if (chatListController.spaces.isNotEmpty)
        NavigationDestination(
          icon: UnreadRoomsBadge(
            badgePosition: badgePosition,
            filter: chatListController.getRoomFilterByActiveFilter(ActiveFilter.spaces),
            child: const Icon(Icons.workspaces_outlined),
          ),
          selectedIcon: UnreadRoomsBadge(
              badgePosition: badgePosition,
              filter: chatListController.getRoomFilterByActiveFilter(ActiveFilter.spaces),
              child: Icon(Icons.workspaces,
                  color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSecondary)),
          label: 'Spaces',
        ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final allSpaces = Matrix.of(context).client.rooms.where((room) => room.isSpace);
    final rootSpaces = allSpaces
        .where(
          (space) => !allSpaces.any(
            (parentSpace) => parentSpace.spaceChildren.any((child) => child.roomId == space.id),
          ),
        )
        .toList();
    rootSpaces.sort((a, b) => a.getLocalizedDisplayname().compareTo(b.getLocalizedDisplayname()));
    final destinations = getNavigationDestinations(context);

    return SizedBox(
      width: isExpanded ? 200 : 88,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: destinations.length,
              itemBuilder: (context, i) {
                if (i < destinations.length) {
                  return NaviRailItem(
                    isExpanded: isExpanded,
                    isSelected: i == chatListController.selectedIndex,
                    onTap: () => chatListController.onDestinationSelected(i),
                    icon: destinations[i].icon,
                    selectedIcon: destinations[i].selectedIcon,
                    toolTip: destinations[i].label,
                    label: destinations[i].label,
                  );
                }
                return null;
              }),
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: rootSpaces.length,
              itemBuilder: (context, i) {
                final isSelected = chatListController.activeFilter == ActiveFilter.spaces && rootSpaces[i].id == chatListController.activeSpaceId;
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    NaviRailSpaceItem(
                      room: rootSpaces[i],
                      isExpanded: isExpanded,
                      toolTip: rootSpaces[i].getLocalizedDisplayname(
                        MatrixLocals(L10n.of(context)!),
                      ),
                      isSelected: isSelected,
                      onTap: () => chatListController.setActiveSpace(rootSpaces[i].id),
                      icon: Avatar(
                        isSpace: true,
                        mxContent: rootSpaces[i].avatar,
                        name: rootSpaces[i].getLocalizedDisplayname(
                          MatrixLocals(L10n.of(context)!),
                        ),
                        fontSize: 20,
                      ),
                      label: SizedBox(
                        width: 120,
                        child: Text(
                          rootSpaces[i].getLocalizedDisplayname(
                            MatrixLocals(L10n.of(context)!),
                          ),
                          style: const TextStyle(overflow: TextOverflow.ellipsis),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
          NaviRailItem(
            isExpanded: isExpanded,
            label: "New Space",
            toolTip: "New Space",
            backgroundColor: Theme.of(context).colorScheme.primary,
            icon: Icon(Icons.add, color: Theme.of(context).colorScheme.onPrimary),
            onTap: () {
              Scaffold.of(context).closeDrawer();
              context.go('/rooms/newspace');
            },
            isSelected: false,
          )
        ],
      ),
    );
  }
}
