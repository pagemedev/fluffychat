import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/pages/archive/archive.dart';
import 'package:pageMe/pages/chat_list/navi_rail_item.dart';
import 'package:pageMe/pages/chat_list/spaces_entry.dart';
import 'package:pageMe/pages/spaces_drawer/spaces_drawer.dart';
import 'package:pageMe/pages/spaces_drawer/spaces_footer.dart';
import 'package:pageMe/pages/spaces_drawer/spaces_list.dart';
import 'package:pageMe/pages/spaces_drawer/spaces_title.dart';

import '../../utils/global_key_provider.dart';
import '../chat_list/chat_list.dart';
import 'drawer_title.dart';

class SpacesDrawerView extends StatelessWidget {
  final ChatListController chatListController;
  final SpacesDrawerController spacesDrawerController;
  final ArchiveController archiveController;

  const SpacesDrawerView({
    Key? key,
    required this.chatListController,
    required this.spacesDrawerController,
    required this.archiveController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentIndex = chatListController.spacesEntries
        .indexWhere((space) => chatListController.activeSpacesEntry.runtimeType == space.runtimeType && (chatListController.activeSpaceId == space.getSpace(context)?.id));

    final Map<SpacesEntry, dynamic> spaceHierarchy = Map.fromEntries(chatListController.spacesEntries.map((e) => MapEntry(e, null)));

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        DrawerTitle(
          spacesDrawerController: spacesDrawerController,
          isExpanded: chatListController.isDrawerExpanded,
        ),
/*        Divider(
          height: 2,
          color: Theme.of(context).colorScheme.primaryContainer,
          indent: 12,
          endIndent: 12,
        ),*/
        Expanded(
          child: SpacesList(
            isExpanded: chatListController.isDrawerExpanded,
            chatListController: chatListController,
            spaceHierarchy: spaceHierarchy,
            context: context,
            currentIndex: currentIndex,
          ),
        ),
        Divider(
          height: 2,
          color: Theme.of(context).colorScheme.primaryContainer,
          indent: 12,
          endIndent: 12,
        ),
        const SizedBox(
          height: 16,
        ),
        NaviRailItem(
          isExpanded: chatListController.isDrawerExpanded,
          label: L10n.of(context)!.archive,
          toolTip: L10n.of(context)!.archive,
          backgroundColor: Theme.of(context).colorScheme.secondary,
          icon: Icon(Icons.archive_outlined, color: Theme.of(context).colorScheme.onSecondary),
          onTap: () {
            Scaffold.of(context).closeDrawer();
            context.go('/rooms/archive');
          },
          isSelected: false,
        ),
        NaviRailItem(
          key: GlobalKeyProvider.settingsButton,
          isExpanded: chatListController.isDrawerExpanded,
          label: L10n.of(context)!.settings,
          toolTip: L10n.of(context)!.settings,
          backgroundColor: Theme.of(context).colorScheme.primaryContainer,
          icon: Icon(
            key: GlobalKeyProvider.settingsIcon,
            Icons.settings_outlined,
            color: Theme.of(context).colorScheme.onPrimaryContainer,
          ),
          onTap: () {
            Scaffold.of(context).closeDrawer();
            context.go('/settings');
          },
          isSelected: false,
        ),
        if (PageMeThemes.isColumnMode(context))
          NaviRailItem(
            isExpanded: chatListController.isDrawerExpanded,
            label: (chatListController.isDrawerExpanded) ? "Collapse Sidebar" : "Expand Sidebar",
            toolTip: (chatListController.isDrawerExpanded) ? "Collapse Sidebar" : "Expand Sidebar",
            backgroundColor: Theme.of(context).colorScheme.primary,
            icon: Icon((chatListController.isDrawerExpanded) ? Icons.arrow_back : Icons.arrow_forward, color: Theme.of(context).colorScheme.onPrimary),
            onTap: () => chatListController.toggleDrawerExpansion(),
            isSelected: false,
          ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }
}
