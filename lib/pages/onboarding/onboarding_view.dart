import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/onboarding/onboarding.dart';

import '../../config/themes.dart';

class OnBoardingHeading extends StatelessWidget {
  final OnBoardingController controller;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 16.0);
  static const titleStyle = TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0);
  const OnBoardingHeading({Key? key, required this.controller}) : super(key: key);

  Widget buildSVG(String assetName, [double width = 250, Color? color]) {
    return SafeArea(
      child: SvgPicture.asset('assets/$assetName', width: width, color: color),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 0, left: 12, right: 12),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                text: 'Hi ',
                style: titleStyle.copyWith(color: PageMeThemes.whiteBlackColor(context)),
                children: <TextSpan>[
                  TextSpan(
                      text: (controller.profile?.displayName != null) ? "${controller.profile?.displayName ?? controller.profile?.userId}!" : "",
                      style: titleStyle.copyWith(color: Theme.of(context).colorScheme.primary)),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5, left: 12, right: 12, bottom: globalHeaderHeight),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              "Now that you're logged in, let's make sure your profile is just the way you like it!",
              textAlign: TextAlign.center,
              style: titleStyle.copyWith(fontSize: 18, fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ],
    );
  }
}

class OnBoardingView extends StatefulWidget {
  final OnBoardingController controller;
  static const double assetWidth = 350;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 19.0);
  static const companyStyle = TextStyle(fontSize: 22.0);
  static const titleStyle = TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0);



  const OnBoardingView(this.controller, {Key? key}) : super(key: key);

  @override
  State<OnBoardingView> createState() => _OnBoardingViewState();
}

class _OnBoardingViewState extends State<OnBoardingView> {
  final FocusNode _focusNode = FocusNode();
  bool _isFocused = false;

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    if (_focusNode.hasFocus != _isFocused) {
      setState(() {
        _isFocused = _focusNode.hasFocus;
      });
    }
  }

  @override
  void dispose() {
    _focusNode.removeListener(_onFocusChange);
    _focusNode.dispose();
    super.dispose();
  }

  Widget buildImage(String assetName, [double? width = OnBoardingView.assetWidth, Color? color]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: OnBoardingView.globalHeaderHeight),
        child: Image.asset('assets/$assetName', width: width, color: color),
      ),
    );
  }

  Widget buildSVG(String assetName, [double width = OnBoardingView.assetWidth, Color? color]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: OnBoardingView.globalHeaderHeight),
        child: SvgPicture.asset('assets/$assetName', width: width, color: color),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: Theme.of(context).colorScheme.background,
            elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
            toolbarHeight: isBusiness() ? 90 : null,
            centerTitle: true,
            title: Column(
              children: [
                SizedBox(
                  height: OnBoardingView.globalHeaderHeight,
                  child: SvgPicture.asset(
                    (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                    fit: BoxFit.fitHeight,
                  ),
                ),
                if (isBusiness())
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "${widget.controller.homeServerName}",
                      style: OnBoardingView.companyStyle.copyWith(color: Theme.of(context).colorScheme.primary),
                    ),
                  )
              ],
            ),
            bottom: PageMeThemes.isColumnMode(context)
                ? PreferredSize(
                    preferredSize: const Size.fromHeight(2.0),
                    child: Container(
                        height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
                        color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black //Theme.of(context).colorScheme.primary,
                        ),
                  )
                : null),
        body: Stack(
          children: [
            if (PageMeThemes.isColumnMode(context))
              Image.asset(
                Theme.of(context).brightness == Brightness.light ? 'assets/background_light.png' : 'assets/background_dark.png',
                width: double.infinity,
                height: double.infinity,
                fit: BoxFit.cover,
              ),
            Padding(
              padding: PageMeThemes.isColumnMode(context) ? const EdgeInsets.all(8.0) : const EdgeInsets.all(0),
              child: Center(
                child: Material(
                  elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
                  borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                  child: Container(
                    width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                      color: Theme.of(context).colorScheme.background,
                    ),
                    child: Column(
                      children: [
                        OnBoardingHeading(
                          controller: widget.controller,
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                                  child: TextField(
                                    style: const TextStyle(fontSize: 20),
                                    controller: widget.controller.nameController,
                                    textAlign: TextAlign.center,
                                    autofocus: false,
                                    autocorrect: false,
                                    focusNode: _focusNode,
                                    enableSuggestions: true, enableInteractiveSelection: true,
                                    //contentInsertionConfiguration: ContentInsertionConfiguration(onContentInserted: , allowedMimeTypes: [""]),
                                    textInputAction: TextInputAction.next,
                                    onChanged: (_) => widget.controller.onChanged(),
                                    onSubmitted: (displayName) => widget.controller.setDisplaynameAction,
                                    decoration:  InputDecoration(hintText: _isFocused ? null : "Please enter your display name", ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 16.0, left: 15, right: 15),
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                      elevation: MaterialStateProperty.resolveWith(
                                        (states) {
                                          if (widget.controller.nameController.text.isEmpty) {
                                            return 0;
                                          }
                                          return null;
                                        },
                                      ),
                                      backgroundColor: MaterialStateProperty.resolveWith(
                                        (states) {
                                          if (widget.controller.nameController.text.isEmpty) {
                                            return Theme.of(context).colorScheme.secondaryContainer;
                                          }
                                          return Theme.of(context).colorScheme.primary;
                                        },
                                      ),
                                    ),
                                    child: Text(
                                      'Set Display Name',
                                      style: TextStyle(
                                          color: widget.controller.nameController.text.isEmpty
                                              ? Theme.of(context).colorScheme.onSecondaryContainer
                                              : Theme.of(context).colorScheme.onPrimary),
                                    ),
                                    onPressed: () async => widget.controller.setDisplaynameAction(),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 18.0),
                                  child: SvgPicture.asset(
                                    'assets/arrow.svg',
                                    color: widget.controller.nameController.text.isEmpty ? PageMeThemes.whiteBlackColor(context).withOpacity(0.4) : null,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                ConstrainedBox(
                                  constraints: const BoxConstraints(minHeight: 150, maxHeight: 200),
                                  child: buildSVG('onboarding.svg'),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
                          child: SizedBox(
                            width: double.infinity,
                            height: 60,
                            child: ElevatedButton(
                                style: ButtonStyle(
                                  elevation: MaterialStateProperty.resolveWith(
                                    (states) {
                                      return null;
                                    },
                                  ),
                                  backgroundColor: MaterialStateProperty.resolveWith(
                                    (states) {
                                      return Theme.of(context).colorScheme.primaryContainer;
                                    },
                                  ),
                                ),
                                onPressed: () async {
                                  widget.controller.nextPage();
                                },
                                child: Text(
                                  "Next",
                                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Theme.of(context).colorScheme.onPrimaryContainer),
                                )),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
