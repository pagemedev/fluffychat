
import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:image_picker/image_picker.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pages/onboarding/onboarding_view.dart';

import 'package:pageMe/widgets/matrix.dart';

import '../../config/setting_keys.dart';
import '../../utils/famedlysdk_store.dart';
import '../../utils/platform_infos.dart';
import '../bootstrap/bootstrap_dialog.dart';
import '../settings/settings.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({Key? key}) : super(key: key);

  @override
  OnBoardingController createState() => OnBoardingController();
}

class OnBoardingController extends State<OnBoarding> {
  final TextEditingController nameController = TextEditingController();
  Future<dynamic>? profileFuture;
  Profile? profile;
  bool profileUpdated = false;
  String? homeServerName;
  int? pageNumber = 0;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const double assetWidth = 350;
  bool showBootStrap = false;

  Store? _store;
  Store get store => _store ??= Store();

  void setPageNumber(int i) {
    if (pageNumber != i) {
      setState(() {
        pageNumber = i;
      });
    }
  }

  void updateProfile() => setState(() {
        profileUpdated = true;
        profile = profileFuture = null;
      });

  void setAvatarAction() async {
    final actions = [
      if (PlatformInfos.isMobile)
        SheetAction(
          key: AvatarAction.camera,
          label: L10n.of(context)!.openCamera,
          isDefaultAction: true,
          icon: Icons.camera_alt_outlined,
        ),
      SheetAction(
        key: AvatarAction.file,
        label: L10n.of(context)!.openGallery,
        icon: Icons.photo_outlined,
      ),
      if (profile?.avatarUrl != null)
        SheetAction(
          key: AvatarAction.remove,
          label: L10n.of(context)!.removeYourAvatar,
          isDestructiveAction: true,
          icon: Icons.delete_outlined,
        ),
    ];
    final action = actions.length == 1
        ? actions.single.key
        : await showModalActionSheet<AvatarAction>(
            context: context,
            title: L10n.of(context)!.changeYourAvatar,
            actions: actions,
          );
    if (action == null) return;
    final matrix = Matrix.of(context);
    if (action == AvatarAction.remove) {
      final success = await showFutureLoadingDialog(
        context: context,
        future: () => matrix.client.setAvatar(null),
      );
      if (success.error == null) {
        updateProfile();
      }
      return;
    }
    MatrixFile file;

    final result = await ImagePicker().pickImage(
      source: action == AvatarAction.camera ? ImageSource.camera : ImageSource.gallery,
      imageQuality: 50,
    );
    if (result == null) return;
    file = MatrixFile(
      bytes: await result.readAsBytes(),
      name: result.path,
    );
    /*else {
      final result = await FilePicker.platform.pickFiles(type: FileType.image, allowMultiple: false);
      if (result == null || result.files.isEmpty) return;
      file = MatrixFile(
        bytes: result.files.first.bytes!,
        name: result.files.first.name,
      );
    }*/
    final success = await showFutureLoadingDialog(
      context: context,
      future: () => matrix.client.setAvatar(file),
    );
    if (success.error == null) {
      updateProfile();
    }
  }

  void setDisplaynameAction() async {
    if (nameController.text == '') return;
    final matrix = Matrix.of(context);
    final success = await showFutureLoadingDialog(
      context: context,
      future: () => matrix.client.setDisplayName(matrix.client.userID!, nameController.text),
    );
    if (success.error == null) {
      updateProfile();
      nameController.clear();
    }
  }

  /// Sets the first time user key to false and navigates to the /rooms page (ChatList)
  Future<void> onIntroEnd(context) async {
    final matrix = Matrix.of(context);
    await store.setItemBool(SettingKeys.firstTimeUser + matrix.client.userID!, false);
    context.go('/rooms');
  }

  void nextPage() {
    setState(() {
      context.go('/onboard/onboarding_profile_picture');
    });
  }


  Widget buildGlobalHeaderImage() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: globalHeaderPadding),
          child: SafeArea(
            child: SizedBox(
              height: globalHeaderHeight,
              child: SvgPicture.asset(
                (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("$homeServerName"),
        )
      ],
    );
  }

  Widget buildFullscreenImage() {
    return Image.asset(
      'assets/background_dark.png',
      fit: BoxFit.cover,
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
    );
  }

  Widget buildImage(String assetName, [double width = assetWidth]) {
    return Image.asset('assets/$assetName', width: width);
  }

  Widget buildSVG(String assetName, [double width = assetWidth]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight + (4 * globalHeaderPadding)),
        child: SvgPicture.asset('assets/$assetName', width: width),
      ),
    );
  }

  Widget buildSVGFooter(String assetName, [double width = assetWidth]) {
    return SvgPicture.asset('assets/$assetName', width: width);
  }

  Widget buildDisplayNameSelection() {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
/*          const ListTile(
            title: Text("Display Name"),
          )*/
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: TextField(
              controller: nameController,
              autofocus: false,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              onChanged: (_) => setState(() {}),
              onSubmitted: (displayName) => setDisplaynameAction,
              decoration: InputDecoration(
                  hintText: (profile?.displayName != null && profile?.displayName != '') ? profile?.displayName : "Please enter your display name"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 15, right: 15),
            child: ElevatedButton(
              style: ButtonStyle(
                elevation: MaterialStateProperty.resolveWith(
                  (states) {
                    if (nameController.text.isEmpty) {
                      return 0;
                    }
                    return null;
                  },
                ),
                backgroundColor: MaterialStateProperty.resolveWith(
                  (states) {
                    if (nameController.text.isEmpty) {
                      return Theme.of(context).colorScheme.surface;
                    }
                    return Theme.of(context).primaryColor;
                  },
                ),
              ),
              child: Text(
                'Change Display Name',
                style: TextStyle(color: nameController.text.isEmpty ? PageMeThemes.whiteBlackColor(context).withOpacity(0.4) : null),
              ),
              onPressed: () async => setDisplaynameAction(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18.0),
            child: SvgPicture.asset(
              'assets/arrow.svg',
              fit: BoxFit.fill,
            ),
          )
        ],
      ),
    );
  }



  void firstRunBootstrapAction() async {
    await BootstrapDialog(
      client: Matrix.of(context).client,
    ).show(context);
    checkBootstrap();
  }

  void checkBootstrap() async {
    final client = Matrix.of(context).client;
    if (!client.encryptionEnabled) return;
    await client.accountDataLoading;
    await client.userDeviceKeysLoading;
    if (client.prevBatch == null) {
      await client.onSync.stream.first;
    }
    final crossSigning = await client.encryption?.crossSigning.isCached() ?? false;
    final needsBootstrap = await client.encryption?.keyManager.isCached() == false || client.encryption?.crossSigning.enabled == false || crossSigning == false;
    final isUnknownSession = client.isUnknownSession;
    setState(() {
      showBootStrap = needsBootstrap || isUnknownSession;
    });
  }

  void onChanged() {
    setState(() {});
  }

  Future<void> setup() async {
    final client = Matrix.of(context).client;
    await client.roomsLoading;
    await client.accountDataLoading;
    if (client.prevBatch == null) {
      await client.onSync.stream.first;
      if (client.encryption?.keyManager.enabled == true) {
        if (await client.encryption?.keyManager.isCached() == false ||
            await client.encryption?.crossSigning.isCached() == false ||
            client.isUnknownSession && !mounted) {
          setState(() {
            showBootStrap = true;
          });
        }
      }
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => checkBootstrap());
    //setup();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    profileFuture ??= client
        .getProfileFromUserId(
      client.userID!,
      cache: !profileUpdated,
      getFromRooms: !profileUpdated,
    )
        .then((p) {
      if (mounted) setState(() => profile = p);
      return p;
    });
    homeServerName = Matrix.of(context).client.homeserver.toString().replaceFirst('https://matrix.', '');
    homeServerName = homeServerName?.replaceFirst('.pageme.co.za', '').toUpperCase();
    return OnBoardingView(
      this,
    );
  }
}
