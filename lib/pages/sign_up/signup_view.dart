import 'dart:typed_data';

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_svg/svg.dart';

import 'package:pageMe/config/themes.dart';
import '../../config/flavor_config.dart';
import '../../widgets/matrix.dart';
import '../subscriptions/subscriptions_cubit.dart';
import 'signup.dart';

class SignupPageView extends StatelessWidget {
  final SignupPageController controller;

  static const double assetWidth = 350;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 19.0);
  static const titleStyle = TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0);

  const SignupPageView(this.controller, {Key? key}) : super(key: key);

  Widget? customBuildCounter(
    BuildContext context, {
    required int currentLength,
    required bool isFocused,
    required int? maxLength,
  }) {
    return Text(
      "$currentLength/${SignupPageController.minPassLength}",
      style: TextStyle(
        color: isFocused ? Theme.of(context).primaryColor : Colors.grey,
      ),
    );
  }

  Widget buildTitleSVGWidget(String title, String asset, [double width = 250]) {
    return Column(
      children: [
        buildSVG(asset, width),
        const SizedBox(
          height: 20,
        ),
        Text(
          title,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }

  Widget buildSVG(String assetName, [double width = assetWidth]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight),
        child: SvgPicture.asset('assets/$assetName', width: width),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final columnModeBotNavPadding = (MediaQuery.sizeOf(context).width - PageMeThemes.columnWidth * 1.5) / 2;
    final formIsIncomplete = controller.usernameController.text.isEmpty ||
        controller.emailController.text.isEmpty ||
        controller.passwordController.text.isEmpty ||
        controller.password2Controller.text.isEmpty;
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        extendBody: PageMeThemes.isColumnMode(context),
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: 0,
          leading: const BackButton(),
          centerTitle: true,
          bottom: PageMeThemes.isColumnMode(context)
              ? PreferredSize(
                  preferredSize: const Size.fromHeight(2.0),
                  child: Container(
                      height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
                      color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black //Theme.of(context).colorScheme.primary,
                      ),
                )
              : null,
          title: SizedBox(
            width: 110,
            height: globalHeaderHeight,
            child: SvgPicture.asset(
              (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
              fit: BoxFit.fitHeight,
            ),
          ),
        ),
        bottomNavigationBar: Padding(
          padding: PageMeThemes.isColumnMode(context)
              ? EdgeInsets.only(left: columnModeBotNavPadding, right: columnModeBotNavPadding, bottom: 8)
              : const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
          child: SizedBox(
            width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
            height: 60,
            child: ElevatedButton(
              onPressed: () => controller.loading
                  ? null
                  : formIsIncomplete
                      ? null
                      : controller.signup(),
              style: ButtonStyle(
                elevation: MaterialStateProperty.resolveWith(
                  (states) {
                    if (formIsIncomplete) {
                      return 0;
                    }
                    return null;
                  },
                ),
                backgroundColor: MaterialStateProperty.resolveWith(
                  (states) {
                    if (formIsIncomplete) {
                      return Theme.of(context).colorScheme.secondaryContainer;
                    }
                    return Theme.of(context).colorScheme.primaryContainer;
                  },
                ),
              ),
              child: controller.loading
                  ? const LinearProgressIndicator(
                      color: Colors.white,
                      backgroundColor: Colors.black54,
                    )
                  : Text(
                      "Create Account",
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: (formIsIncomplete ? Theme.of(context).colorScheme.onSecondaryContainer : Theme.of(context).colorScheme.onPrimaryContainer),
                      ),
                    ),
            ),
          ),
        ),
        body: Stack(
          children: [
            if (PageMeThemes.isColumnMode(context))
              Image.asset(
                Theme.of(context).brightness == Brightness.light ? 'assets/background_light.png' : 'assets/background_dark.png',
                width: double.infinity,
                height: double.infinity,
                fit: BoxFit.cover,
              ),
            Padding(
              padding: PageMeThemes.isColumnMode(context) ? const EdgeInsets.all(8.0) : const EdgeInsets.all(0),
              child: Center(
                child: Material(
                  elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
                  borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                  color: Colors.transparent,
                  child: Container(
                    width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                      color: Theme.of(context).scaffoldBackgroundColor,
                      //color: Colors.transparent,
                    ),
                    child: Form(
                      key: controller.formKey,
                      child: ListView(
                        shrinkWrap: true,
                        //mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            padding: bodyPadding,
                            child: const Text(
                              'Create your new account.',
                              style: bodyStyle,
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: TextField(
                              canRequestFocus: true,
                              focusNode: controller.usernameFocusNode,
                              textInputAction: TextInputAction.next,
                              controller: controller.usernameController,
                              onSubmitted: (_) async => controller.signUpUsername(),
                              style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
                              decoration: PageMeThemes.loginTextFieldDecoration(
                                prefixIcon: Icon(
                                  Icons.account_box_outlined,
                                  color: Theme.of(context).colorScheme.onBackground,
                                ),
                                hintText: L10n.of(context)!.chooseAUsername,
                                errorText: controller.signupError,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: TextFormField(
                              readOnly: controller.loading,
                              autocorrect: false,
                              focusNode: controller.emailFocusNode,
                              textInputAction: TextInputAction.next,
                              controller: controller.emailController,
                              keyboardType: TextInputType.emailAddress,
                              style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
                              autofillHints: controller.loading ? null : [AutofillHints.username],
                              validator: controller.emailTextFieldValidator,
                              decoration: PageMeThemes.loginTextFieldDecoration(
                                prefixIcon: Icon(
                                  Icons.mail_outlined,
                                  color: Theme.of(context).colorScheme.onBackground,
                                ),
                                hintText: L10n.of(context)!.enterAnEmailAddress,
                                errorText: controller.error,
                                errorMaxLines: 4,
                                errorStyle: TextStyle(
                                  color: controller.emailController.text.isEmpty ? Colors.orangeAccent : Colors.orange,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
                            child: TextFormField(
                              readOnly: controller.loading,
                              autocorrect: false,
                              onChanged: controller.onPasswordType,
                              focusNode: controller.passwordFocusNode,
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (_) {
                                controller.formKey.currentState?.validate();
                                if (controller.displaySecondPasswordField) {
                                  FocusScope.of(context).requestFocus(controller.password2FocusNode);
                                }
                              },
                              style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
                              autofillHints: controller.loading ? null : [AutofillHints.newPassword],
                              controller: controller.passwordController,
                              buildCounter: customBuildCounter,
                              obscureText: !controller.showPassword,
                              validator: controller.password1TextFieldValidator,
                              decoration: PageMeThemes.loginTextFieldDecoration(
                                prefixIcon: Icon(
                                  Icons.vpn_key_outlined,
                                  color: Theme.of(context).colorScheme.onBackground,
                                ),
                                suffixIcon: IconButton(
                                  tooltip: L10n.of(context)!.showPassword,
                                  icon: Icon(
                                    controller.showPassword ? Icons.visibility_off_outlined : Icons.visibility_outlined,
                                    color: Theme.of(context).colorScheme.onBackground,
                                  ),
                                  onPressed: controller.toggleShowPassword,
                                ),
                                //errorStyle: const TextStyle(color: Colors.orange),
                                hintText: L10n.of(context)!.chooseAStrongPassword,
                              ),
                            ),
                          ),
                          if (controller.displaySecondPasswordField)
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
                              child: TextFormField(
                                readOnly: controller.loading,
                                focusNode: controller.password2FocusNode,
                                textInputAction: TextInputAction.next,
                                autocorrect: false,
                                onChanged: controller.onPassword2Type,
                                buildCounter: customBuildCounter,
                                onFieldSubmitted: (_) {
                                  if (controller.formKey.currentState!.validate()) {
                                    FocusScope.of(context).requestFocus(controller.createAccountFocusNode);
                                  }
                                },
                                style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
                                autofillHints: controller.loading ? null : [AutofillHints.newPassword],
                                controller: controller.password2Controller,
                                obscureText: !controller.showPassword,
                                validator: controller.password2TextFieldValidator,
                                decoration: PageMeThemes.loginTextFieldDecoration(
                                  prefixIcon: Icon(
                                    Icons.repeat_outlined,
                                    color: Theme.of(context).colorScheme.onBackground,
                                  ),
                                  hintText: L10n.of(context)!.repeatPassword,
                                  errorStyle: const TextStyle(color: Colors.orange),
                                ),
                              ),
                            ),
                          BlocProvider.of<SubscriptionsCubit>(context).state.selectedProduct != null
                              ? Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: ListTile(
                                    tileColor: Theme.of(context).colorScheme.surface,
                                    title: Text(
                                      BlocProvider.of<SubscriptionsCubit>(context).state.selectedProduct!.title.replaceFirst('plan (PageMe Public)', ''),
                                      style: const TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    subtitle: Text('Price: ${BlocProvider.of<SubscriptionsCubit>(context).state.selectedProduct!.priceString} pm'),
                                    leading: const Icon(Icons.check_circle, color: Colors.teal),
                                    trailing: TextButton(
                                      child: const Text(
                                        'Change',
                                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.teal),
                                      ),
                                      onPressed: () => controller.previousPage(),
                                    ), //
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20), side: const BorderSide(width: 2, color: Colors.teal)),
                                    //onTap: () => controller.previousPage(),
                                  ),
                                )
                              : const SizedBox(),
                          SizedBox(
                            height: 50,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text("Already have an account?"),
                                    TextButton(
                                      focusNode: controller.createAccountFocusNode,
                                      onPressed: () {
                                        controller.toLoginPage();
                                      },
                                      child: Text(L10n.of(context)!.login, style: const TextStyle(fontWeight: FontWeight.bold)),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
