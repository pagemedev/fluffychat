import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';

import 'package:pageMe/pages/sign_up/signup_view.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/widgets/matrix.dart';
import '../../pageme_app.dart';
import '../../utils/localized_exception_extension.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({Key? key}) : super(key: key);

  @override
  SignupPageController createState() => SignupPageController();
}

class SignupPageController extends State<SignupPage> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController password2Controller = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  final FocusNode usernameFocusNode = FocusNode();
  final FocusNode emailFocusNode = FocusNode();
  final FocusNode passwordFocusNode = FocusNode();
  final FocusNode password2FocusNode = FocusNode();
  final FocusNode createAccountFocusNode = FocusNode();

  String? signupError;
  String? error;
  bool loading = false;
  bool showPassword = false;
  bool noEmailWarningConfirmed = false;
  bool displaySecondPasswordField = false;
  late MatrixState matrix;
  late L10n? l10n;

  static const int minPassLength = 8;

  void toggleShowPassword() => setState(() => showPassword = !showPassword);

  String? get domain => PageMeApp.router.routeInformationProvider.value.uri.queryParameters['domain'];

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void didChangeDependencies() {
    matrix = Matrix.of(context);
    l10n = L10n.of(context);
    super.didChangeDependencies();
  }

  void previousPage() {
    setState(() {
      PageMeApp.router.go('/login/subscriptions');
    });
  }

  void toLoginPage() {
    if (mounted) {
      setState(() {
        context.go('/login');
/*        widget.pageViewController.previousPage(
          duration: const Duration(milliseconds: 500),
          curve: Curves.fastLinearToSlowEaseIn,
        )*/
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    usernameController.dispose();
    passwordController.dispose();
    password2Controller.dispose();
    emailController.dispose();
    usernameFocusNode.dispose();
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    password2FocusNode.dispose();
    createAccountFocusNode.dispose();
  }

  void onPasswordType(String text) {
    if (text.length >= minPassLength && !displaySecondPasswordField) {
      setState(() {
        displaySecondPasswordField = true;
      });
    } else {
      setState(() {});
    }
  }

  void onPassword2Type(String text) {
    if (text.length >= minPassLength) {
      setState(() {});
    }
  }

  String? password1TextFieldValidator(String? value) {
    if (value!.isEmpty) {
      return l10n!.chooseAStrongPassword;
    }
    if (value.length < minPassLength) {
      return l10n!.pleaseChooseAtLeastChars(minPassLength.toString());
    }
    return null;
  }

  String? password2TextFieldValidator(String? value) {
    if (value!.isEmpty) {
      return l10n!.repeatPassword;
    }
    if (value != passwordController.text) {
      return l10n!.passwordsDoNotMatch;
    }
    return null;
  }

  String? emailTextFieldValidator(String? value) {
    if (value!.isEmpty && !noEmailWarningConfirmed) {
      noEmailWarningConfirmed = true;
      return l10n!.noEmailWarning;
    }
    if (value.isNotEmpty && !value.contains('@')) {
      return l10n!.pleaseEnterValidEmail;
    }
    return null;
  }

  Future<void> signUpUsername() async {
    usernameController.text = usernameController.text.trim();
    final localpart = usernameController.text.toLowerCase().replaceAll(' ', '_');
    if (localpart.isEmpty) {
      setState(() {
        signupError = l10n!.pleaseChooseAUsername;
      });
      return;
    }

    setState(() {
      signupError = null;
      loading = true;
    });

    try {
      try {
        await matrix.getLoginClient().register(username: localpart);
      } on MatrixException catch (e) {
        if (!e.requireAdditionalAuthentication) rethrow;
      }
      setState(() {
        loading = false;
      });
      if (!mounted) return;
      matrix.loginUsername = usernameController.text;
    } catch (e, s) {
      Logs().d('Sign up failed', e, s);
      setState(() {
        signupError = e.toLocalizedString(context);
        loading = false;
      });
    }
  }

  void signup([_]) async {
    await signUpUsername();
    setState(() {
      error = null;
    });
    if (!formKey.currentState!.validate()) return;

    setState(() {
      loading = true;
    });

    try {
      final client = matrix.getLoginClient();
      final email = emailController.text;
      if (email.isNotEmpty) {
        matrix.currentClientSecret = DateTime.now().millisecondsSinceEpoch.toString();
        matrix.currentThreepidCreds = await client.requestTokenToRegisterEmail(matrix.currentClientSecret, email, 0);
      }

      final displayname = matrix.loginUsername!;
      final localPart = displayname.toLowerCase().replaceAll(' ', '_');

      await client.uiaRequestBackground(
        (auth) async {
          return client.register(
              username: localPart, password: passwordController.text, initialDeviceDisplayName: await PlatformInfos.clientName, auth: auth, kind: AccountKind.user);
        },
      );

      // Set displayname
      if (displayname != localPart) {
        await client.setDisplayName(
          client.userID!,
          displayname,
        );
      }
    } catch (e) {
      error = (e).toLocalizedString(context);
    } finally {
      if (mounted) {
        setState(() => loading = false);
      }
    }
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () async {
          previousPage();
          return false;
        },
        child: SignupPageView(this),
      );
}
