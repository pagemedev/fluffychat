import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/utils/global_key_provider.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/widgets/unread_rooms_badge.dart';
import 'package:searchbar_animation/searchbar_animation.dart';

import '../../config/themes.dart';
import '../../utils/platform_infos.dart';
import '../../utils/timestamp_setter.dart';
import '../../widgets/matrix.dart';
import 'chat_list.dart';
import 'client_chooser_button.dart';

/// This class represents an AppBar for a chat list. It is a custom AppBar
/// that is intended to be used in a chat application.
///
/// The AppBar changes its behavior and display based on the select mode
/// (i.e., normal, select, or share), which is passed as a parameter.
///
/// The class also requires a `ChatListController` and a `BuildContext` upon instantiation.
class ChatListAppBar extends StatelessWidget implements PreferredSizeWidget {
  /// Creates a `ChatListAppBar`.
  ///
  /// The [selectMode], [controller], and [context] arguments must not be null.
  const ChatListAppBar({Key? key, required this.selectMode, required this.controller, required this.context}) : super(key: key);

  final SelectMode selectMode;
  final ChatListController controller;
  final BuildContext context;

  @override
  Size get preferredSize => const Size.fromHeight(58.0);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return AppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: 56,
        leadingWidth: _calculateLeadingWidth(),
        leading: _buildLeading(),
        centerTitle: false,
        actions: _buildActions(constraints.maxWidth),
        bottom: _buildBottom(),
        title: _animateTitle(constraints),
        systemOverlayStyle: !PageMeThemes.isDarkMode(context) ? SystemUiOverlayStyle.dark : SystemUiOverlayStyle.light,
      );
    });
  }

  /// Calculates the width of the leading widget based on the select mode and context.
  double _calculateLeadingWidth() {
    final fileHandlerBlocState = context.watch<FileHandlerBloc>().state;
    final path = GoRouterState.of(context).path;

    if (selectMode == SelectMode.normal && !fileHandlerBlocState.isInitiated) {
      if (PageMeThemes.isColumnMode(context)) {
        return 0;
      } else {
        return 56;
      }
    } else if (path != '/rooms') {
      return 0;
    } else {
      return 56;
    }
  }

  /// Builds the actions for the AppBar in share mode.
  List<Widget>? _buildShareActions(double maxWidth) {
    return [
      SizedBox(
        key: GlobalKeyProvider.searchButton,
        height: 50,
        width: _calculateSearchBarSize(maxWidth),
        child: SearchBarAnimation(
          durationInMilliSeconds: 300,
          onPressButton: (isOpen) => controller.setSearchBarState(isOpen),
          hintText: "Search ${Matrix.of(context).client.homeserver.toString().replaceFirst('https://matrix.', '').replaceFirst('.pageme.co.za', '')}",
          textEditingController: controller.searchController,
          isOriginalAnimation: false,
          trailingWidget: Icon(
            Icons.search,
            color: Theme.of(context).colorScheme.onBackground,
            size: 22,
          ),
          enableBoxShadow: false,
          enableButtonShadow: false,
          buttonShadowColour: Colors.transparent,
          buttonElevation: 0,
          searchBoxBorderColour: Colors.transparent,
          secondaryButtonWidget: Icon(Icons.close, color: Theme.of(context).colorScheme.onBackground),
          buttonWidget: Icon(Icons.search, color: Theme.of(context).colorScheme.onBackground),
          buttonColour: Colors.transparent,
          searchBoxColour: Colors.transparent,
          buttonBorderColour: Colors.transparent,
          cursorColour: Theme.of(context).colorScheme.onBackground,
          enteredTextStyle: TextStyle(color: Theme.of(context).colorScheme.onBackground),
          onChanged: controller.onSearchEnter,
          isSearchBoxOnRightSide: true,
          enableKeyboardFocus: true,
          onCollapseComplete: () => controller.cancelSearch(unfocus: true),
        ),
      )
    ];
  }

  /// Builds the actions for the AppBar in select mode on a mobile platform.
  List<Widget> _buildSelectMobileActions() {
    // Your logic when selectMode is Select and platform is mobile
    return [
      if (controller.spaces.isNotEmpty)
        IconButton(
          tooltip: L10n.of(context)!.addToSpace,
          color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
          icon: const Icon(Icons.group_work_outlined),
          onPressed: controller.addToSpace,
        ),
      IconButton(
        tooltip: L10n.of(context)!.toggleUnread,
        color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
        icon: Icon(controller.anySelectedRoomNotMarkedUnread ? Icons.mark_chat_read_outlined : Icons.mark_chat_unread_outlined),
        onPressed: controller.toggleUnread,
      ),
      IconButton(
        tooltip: L10n.of(context)!.toggleFavorite,
        color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
        icon: Icon(controller.anySelectedRoomNotFavorite ? Icons.push_pin_outlined : Icons.push_pin),
        onPressed: controller.toggleFavouriteRoom,
      ),
      IconButton(
        icon: Icon(controller.anySelectedRoomNotMuted ? Icons.notifications_off_outlined : Icons.notifications_outlined),
        tooltip: L10n.of(context)!.toggleMuted,
        color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
        onPressed: controller.toggleMuted,
      ),
      IconButton(
        icon: const Icon(Icons.delete_outlined),
        color: Colors.red,
        tooltip: L10n.of(context)!.archive,
        onPressed: controller.archiveAction,
      ),
    ];
  }

  double _calculateSearchBarSize(double maxWidth) {
    if (controller.activeFilter == ActiveFilter.allChats || controller.activeSpaceId == null) {
      if (PageMeThemes.isColumnMode(context)) {
        return controller.isSearchBarOpen ? maxWidth : 50;
      } else {
        return controller.isSearchBarOpen ? maxWidth - 60 : 50;
      }
    } else {
      if (PageMeThemes.isColumnMode(context)) {
        return controller.isSearchBarOpen ? maxWidth - 48 : 50;
      }
      return controller.isSearchBarOpen ? maxWidth - 108 : 50;
    }
  }

  /// Builds the default actions for the AppBar.
  List<Widget> _buildDefaultActions(double maxWidth) {
    return [
      SizedBox(
        key: GlobalKeyProvider.searchButton,
        height: 50,
        width: _calculateSearchBarSize(maxWidth),
        child: SearchBarAnimation(
          durationInMilliSeconds: 300,
          onPressButton: (isOpen) => controller.setSearchBarState(isOpen),
          hintText: "Search ${Matrix.of(context).client.homeserver.toString().replaceFirst('https://matrix.', '').replaceFirst('.pageme.co.za', '')}",
          textEditingController: controller.searchController,
          isOriginalAnimation: false,
          trailingWidget: Icon(
            Icons.search,
            color: Theme.of(context).colorScheme.onBackground,
            size: 22,
          ),
          enableBoxShadow: false,
          enableButtonShadow: false,
          buttonShadowColour: Colors.transparent,
          buttonElevation: 0,
          searchBoxBorderColour: Colors.transparent,
          secondaryButtonWidget: Icon(Icons.close, color: Theme.of(context).colorScheme.onBackground),
          buttonWidget: Icon(Icons.search, color: Theme.of(context).colorScheme.onBackground),
          buttonColour: Colors.transparent,
          searchBoxColour: Colors.transparent,
          buttonBorderColour: Colors.transparent,
          cursorColour: Theme.of(context).colorScheme.onBackground,
          enteredTextStyle: TextStyle(color: Theme.of(context).colorScheme.onBackground),
          onChanged: controller.onSearchEnter,
          isSearchBoxOnRightSide: true,
          enableKeyboardFocus: true,
          onCollapseComplete: () => controller.cancelSearch(unfocus: true),
        ),
      ),
      if (controller.activeSpaceId != null && controller.activeFilter != ActiveFilter.allChats)
        IconButton(
          icon: const Icon(Icons.more_vert),
          tooltip: L10n.of(context)!.edit,
          onPressed: () => controller.editSpace(context, controller.activeSpaceId!),
        )
    ];
  }

  /// Builds the actions for the AppBar based on the select mode.
  List<Widget>? _buildActions(double maxWidth) {
    if (context.watch<FileHandlerBloc>().state.isInitiated) {
      return _buildShareActions(maxWidth);
    } else if (selectMode == SelectMode.select && PlatformInfos.isMobile) {
      return _buildSelectMobileActions();
    } else {
      return _buildDefaultActions(maxWidth);
    }
  }

  /// Builds the leading widget for the AppBar.
  Widget _buildLeading() {
    return BlocBuilder<FileHandlerBloc, FileHandlerState>(
      builder: (context, fileState) {
        return Padding(
          padding: const EdgeInsets.only(left: 16.0),
          child: Matrix.of(context).isMultiAccount ? ClientChooserButton(controller) : _buildIconButton(fileState),
        );
      },
    );
  }

  /// Builds an icon button for the AppBar.
  StatelessWidget? _buildIconButton(FileHandlerState fileState) {
    final badgePosition = BadgePosition.topEnd(top: -16, end: -12);
    final path = GoRouterState.of(context).path;
    if (path != '/rooms') {
      if (PageMeThemes.isColumnMode(context)) {
        return null;
      } else {
        return IconButton(
          key: GlobalKeyProvider.menuButton,
          tooltip: "Menu",
          icon: UnreadRoomsBadge(
              badgePosition: badgePosition,
              filter: controller.activeFilter == ActiveFilter.spaces
                  ? controller.getRoomFilterByActiveFilter(ActiveFilter.allChats)
                  : controller.getRoomFilterByActiveFilter(ActiveFilter.spaces),
              child: const Icon(Icons.menu)),
          onPressed: controller.scaffoldKey.currentState?.openDrawer,
        );
      }
    } else if (selectMode != SelectMode.normal || fileState.isInitiated) {
      return IconButton(
        tooltip: L10n.of(context)!.cancel,
        icon: const Icon(Icons.close_outlined),
        color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
        onPressed: () => controller.cancelAction(),
      );
    } else {
      if (PageMeThemes.isColumnMode(context)) {
        return null;
      } else {
        return IconButton(
          key: GlobalKeyProvider.menuButton,
          tooltip: "Menu",
          icon: UnreadRoomsBadge(
              badgePosition: badgePosition,
              filter: controller.activeFilter == ActiveFilter.spaces
                  ? controller.getRoomFilterByActiveFilter(ActiveFilter.allChats)
                  : controller.getRoomFilterByActiveFilter(ActiveFilter.spaces),
              child: const Icon(Icons.menu)),
          onPressed: controller.scaffoldKey.currentState?.openDrawer,
        );
      }
    }
  }

  /// Builds the bottom widget for the AppBar.
  PreferredSize _buildBottom() {
    return PreferredSize(
      preferredSize: const Size.fromHeight(2.0),
      child: Container(
        height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
        color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black,
      ),
    );
  }

  Widget _animateTitle(BoxConstraints constraints) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      height: controller.isSearchBarOpen ? 0 : null,
      width: controller.isSearchBarOpen ? 0 : null,
      child: _buildTitle(constraints),
    );
  }

  /// Builds the title for the AppBar based on the select mode.
  Widget _buildTitle(BoxConstraints constraints) {
    return BlocBuilder<FileHandlerBloc, FileHandlerState>(
      builder: (context, fileState) {
        if (fileState.isInitiated && PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms') {
          return _buildShareTitle();
        } else if (selectMode == SelectMode.select) {
          return _buildSelectTitle();
        } else {
          return _buildActiveFilterTitle(constraints);
        }
      },
    );
  }

  /// Builds the title for the AppBar in share mode.
  Widget _buildShareTitle() {
    return (controller.selectedShareRoomIds.isNotEmpty)
        ? Text(
            '${controller.selectedShareRoomIds.length.toString()} selected',
            style: TextStyle(
              color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
            ),
          )
        : Text(
            'Send to...',
            style: TextStyle(
              color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
            ),
          );
  }

  /// Builds the title for the AppBar in select mode.
  Widget _buildSelectTitle() {
    return Text(
      controller.selectedRoomIds.length.toString(),
      style: TextStyle(
        color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryContainer,
      ),
    );
  }

  /// Builds the title for the AppBar based on the active filter
  Widget _buildActiveFilterTitle(BoxConstraints constraints) {
    if (controller.activeFilter == ActiveFilter.allChats) {
      return _buildAllChatsTitle();
    } else {
      return _buildSpaceTitle(constraints);
    }
  }

  /// Builds the title for the AppBar when all chats are active.
  Widget _buildAllChatsTitle() {
    return Stack(
      children: [
        const Opacity(opacity: 0.0, child: TimeStampSetter()),
        GestureDetector(
          onTap: () {
            if (PageMeThemes.isColumnMode(context)) {
              context.go("/rooms");
            } else {
              controller.scaffoldKey.currentState?.openDrawer();
            }
          },
          child: ConstrainedBox(
            constraints: const BoxConstraints.tightFor(height: 25, width: 85),
            child: PageMeThemes.isDarkMode(context) ? SvgPicture.asset('assets/pageme_app_name_white.svg') : SvgPicture.asset('assets/pageme_app_name_black.svg'),
          ),
        ),
      ],
    );
  }

  /// Builds the title for the AppBar when a space is active.
  Widget _buildActiveSpaceTitle(BoxConstraints constraints) {
    return SizedBox(
      width: constraints.maxWidth,
      child: GestureDetector(
        onTap: () {
          if (PageMeThemes.isColumnMode(context)) {
            context.go("/rooms");
          } else {
            controller.scaffoldKey.currentState?.openDrawer();
          }
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 6,
              ),
              child: Icon(
                Icons.workspaces_outlined,
                color: Theme.of(context).colorScheme.onBackground,
              ),
            ),
            Icon(
              Icons.arrow_right,
              color: Theme.of(context).colorScheme.tertiary,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 1.0),
                child: SingleChildScrollView(
                  clipBehavior: Clip.antiAlias,
                  scrollDirection: Axis.horizontal,
                  child: Text(
                    Matrix.of(context).client.getRoomById(controller.activeSpaceId!)!.getLocalizedDisplayname(),
                    style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Builds the title for the AppBar when no space is active and not in All Chats.
  Widget _buildAllSpacesTitle() {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 1.0),
          child: SizedBox(
            width: PageMeThemes.isColumnMode(context) ? null : 150,
            child: Text('All Spaces', style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
          ),
        ),
      ],
    );
  }

  /// Builds the title for the AppBar when depending on the space.
  Widget _buildSpaceTitle(BoxConstraints constraints) {
    return (controller.activeSpaceId != null) ? _buildActiveSpaceTitle(constraints) : _buildAllSpacesTitle();
  }
}
