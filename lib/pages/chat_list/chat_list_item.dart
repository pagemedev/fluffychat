import 'dart:async';
import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/chat_list/chat_list.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';
import 'package:pageMe/utils/string_color.dart';

// ignore: unused_import
import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:pageMe/utils/room_status_extension.dart';
import '../../config/themes.dart';
import '../../utils/date_time_extension.dart';
import '../../widgets/avatar.dart';
import '../../widgets/context_menu/custom_context_menu.dart';
import '../../widgets/matrix.dart';
import '../chat/chat.dart';

enum ArchivedRoomAction { delete, rejoin }

enum InviteActions { accept, decline, block }

class ChatListItem extends StatelessWidget {
  final Room room;
  final bool activeChat;
  final bool selected;
  final Function? onForget;
  final void Function()? onTap;
  final void Function()? onLongPress;
  final ChatListController? controller;

  const ChatListItem(
    this.room, {
    this.activeChat = false,
    this.selected = false,
    this.onTap,
    this.onLongPress,
    this.onForget,
    Key? key,
    this.controller,
  }) : super(key: key);

  void clickAction(BuildContext context) async {
    if (onTap != null) return onTap!();
    if (activeChat) return;
    if (room.membership == Membership.invite) {
      final inviterId = room.getState(EventTypes.RoomMember, room.client.userID!)?.senderId;
      final inviteAction = await showModalActionSheet<InviteActions>(
        context: context,
        message: room.isDirectChat ? "📨 Invite private chat" : room.isSpace ? "📨 Invite to a space" : "📨 Invite group chat",
        title: room.getLocalizedDisplayname(MatrixLocals(L10n.of(context)!)),
        actions: [
          SheetAction(
            key: InviteActions.accept,
            label: L10n.of(context)!.accept,
            icon: Icons.check_outlined,
            isDefaultAction: true,
          ),
          SheetAction(
            key: InviteActions.decline,
            label: "Decline",
            icon: Icons.close_outlined,
            isDestructiveAction: true,
          ),
          SheetAction(
            key: InviteActions.block,
            label: "Block",
            icon: Icons.block_outlined,
            isDestructiveAction: true,
          ),
        ],
      );
      if (inviteAction == null) return;
      if (inviteAction == InviteActions.block) {
        context.go('/rooms/settings/security/ignorelist', extra: inviterId);
        return;
      }
      if (inviteAction == InviteActions.decline) {
        await showFutureLoadingDialog(
          context: context,
          future: room.leave,
        );
        return;
      }
      final joinResult = await showFutureLoadingDialog(
        context: context,
        future: () async {
          final waitForRoom = room.client.waitForRoomInSync(
            room.id,
            join: true,
          );
          await room.join();
          await waitForRoom;
        },
      );
      if (joinResult.error != null) return;
    }

    if (room.membership == Membership.ban) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            L10n.of(context)!.youHaveBeenBannedFromThisChat,
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
      return;
    }

    if (room.membership == Membership.leave) {
      final action = await showModalActionSheet<ArchivedRoomAction>(
        context: context,
        title: L10n.of(context)!.archivedRoom,
        message: L10n.of(context)!.thisRoomHasBeenArchived,
        actions: [
          SheetAction(
            label: L10n.of(context)!.rejoin,
            key: ArchivedRoomAction.rejoin,
          ),
          SheetAction(
            label: L10n.of(context)!.delete,
            key: ArchivedRoomAction.delete,
            isDestructiveAction: true,
          ),
        ],
      );
      if (action != null) {
        switch (action) {
          case ArchivedRoomAction.delete:
            await archiveAction(context);
            break;
          case ArchivedRoomAction.rejoin:
            await showFutureLoadingDialog(
              context: context,
              future: () => room.join(),
            );
            break;
        }
      }
    }

    if (room.membership == Membership.join) {
      context.go('/rooms/${room.id}');
    }
  }

  Future<void> archiveAction(BuildContext context) async {
    {
      if ([Membership.leave, Membership.ban].contains(room.membership)) {
        final success = await showFutureLoadingDialog(
          context: context,
          future: () => room.forget(),
        );
        if (success.error == null) {
          if (onForget != null) onForget!();
        }
        return;
      }
      final confirmed = await showOkCancelAlertDialog(
        useRootNavigator: false,
        context: context,
        title: L10n.of(context)!.areYouSure,
        okLabel: L10n.of(context)!.yes,
        cancelLabel: L10n.of(context)!.no,
      );
      if (confirmed == OkCancelResult.cancel) return;
      await showFutureLoadingDialog(context: context, future: () => room.leave());
      return;
    }
  }

  String getChatMessage(BuildContext context, Room room, MatrixLocals locals) {
    if (room.membership == Membership.invite) {
      return L10n.of(context)!.youAreInvitedToThisChat;
    } else if (room.lastEvent?.messageType == 'm.bad.encrypted') {
      return '';
    } else if (room.lastEvent?.messageType == CustomMessageTypes.quillText) {
      return room.lastEvent?.calcQuillTextToPlainText(locals: locals) ?? 'Sent a composed message';
    } else {
      return room.lastEvent?.calcLocalizedBodyFallback(
            locals,
            hideReply: true,
            hideEdit: true,
            plaintextBody: true,
            removeMarkdown: true,
            withSenderNamePrefix: !room.isDirectChat || room.directChatMatrixID != room.lastEvent?.senderId,
          ) ??
          L10n.of(context)!.emptyChat;
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle contextMenuTextStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w500,
      color: Theme.of(context).colorScheme.onBackground,
    );
    final Color contextMenuIconColor = Theme.of(context).colorScheme.onBackground;
    const double contextMenuIconSize = 20;
    final shareContent = Matrix.of(context).shareContent;
    final List<MatrixFile> shareFiles = [];
    final List<Map<String, dynamic>> shareEvents = [];
    if (shareContent != null && shareContent.isNotEmpty) {
      for (final Map<String, dynamic>? content in shareContent) {
        if (content!.tryGet<String>('msgtype') == FlavorConfig.appId && content.tryGet<MatrixFile>('file') != null) {
          shareFiles.add(content.tryGet<MatrixFile>('file')!);
        } else {
          shareEvents.add(content);
        }
      }
    }
    final isMuted = room.pushRuleState != PushRuleState.notify;
    final typingText = room.getLocalizedTypingText(context);
    final ownMessage = room.lastEvent?.senderId == Matrix.of(context).client.userID;
    final unread = room.isUnread || room.membership == Membership.invite;
    final unreadBubbleSize = unread || room.hasNewMessages
        ? room.notificationCount > 0
            ? 20.0
            : 14.0
        : 0.0;
    return CustomContextMenuArea(
      width: 200,
      verticalPadding: 0,
      builder: (c) {
        if (controller == null) return [];
        final List<Widget> contextMenuOptions = [];
        if (controller!.selectMode == SelectMode.normal) {
          contextMenuOptions
            ..add(ListTile(
              dense: true,
              title: Text(room.isDirectChat ? 'Chat Details' : 'Group Details',
                  style: contextMenuTextStyle.copyWith(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.w600)),
              trailing: Icon(
                Icons.account_box_outlined,
                size: contextMenuIconSize,
                color: contextMenuIconColor,
              ),
              onTap: () async {
                Navigator.of(c).pop();
                context.go('/rooms/${room.id}/large_details');
              },
            ))
            ..add(const Divider(
              height: 2,
              endIndent: 8,
              indent: 8,
            ));
          if (!room.isDirectChat && room.canInvite) {
            contextMenuOptions.add(
              ListTile(
                dense: true,
                title: Text('Invite people', style: contextMenuTextStyle),
                trailing: Icon(
                  Icons.person_add_outlined,
                  size: contextMenuIconSize,
                  color: contextMenuIconColor,
                ),
                onTap: () async {
                  Navigator.of(c).pop();
                  context.go('/rooms/${room.id}/invite');
                },
              ),
            );
          }
        }
        contextMenuOptions
          ..add(ListTile(
            dense: true,
            title: Text(
                (controller!.selectMode == SelectMode.select)
                    ? controller!.anySelectedRoomNotMarkedUnread
                        ? "Mark selected as unread"
                        : 'Mark selected as read'
                    : controller!.selectedRoomNotMarkedUnread(room.id)
                        ? "Mark as unread"
                        : 'Mark as read',
                style: contextMenuTextStyle),
            trailing: Icon(
              (controller!.selectMode == SelectMode.select)
                  ? controller!.anySelectedRoomNotMarkedUnread
                      ? Icons.mark_chat_read_outlined
                      : Icons.mark_chat_unread_outlined
                  : controller!.selectedRoomNotMarkedUnread(room.id)
                      ? Icons.mark_chat_read_outlined
                      : Icons.mark_chat_unread_outlined,
              size: contextMenuIconSize,
              color: contextMenuIconColor,
            ),
            onTap: () async {
              Navigator.of(c).pop();
              (controller!.selectMode == SelectMode.select) ? controller!.toggleUnread : controller!.toggleUnread(roomId: room.id);
            },
          ))
          ..add(ListTile(
            dense: true,
            title: Text(
                (controller!.selectMode == SelectMode.select)
                    ? controller!.anySelectedRoomNotFavorite
                        ? 'Pin selected Messages'
                        : 'Unpin selected Messages'
                    : controller!.selectedRoomNotFavorite(room.id)
                        ? 'Pin Message'
                        : 'Unpin Message',
                style: contextMenuTextStyle),
            trailing: Icon(
              (controller!.selectMode == SelectMode.select)
                  ? controller!.anySelectedRoomNotFavorite
                      ? Icons.push_pin_outlined
                      : Icons.push_pin
                  : controller!.selectedRoomNotFavorite(room.id)
                      ? Icons.push_pin_outlined
                      : Icons.push_pin,
              size: contextMenuIconSize,
              color: contextMenuIconColor,
            ),
            onTap: () async {
              Navigator.of(c).pop();
              (controller!.selectMode == SelectMode.select) ? controller!.toggleFavouriteRoom : controller!.toggleFavouriteRoom(roomId: room.id);
            },
          ))
          ..add(
            ListTile(
              dense: true,
              trailing: Icon(
                (controller!.selectMode == SelectMode.select)
                    ? controller!.anySelectedRoomNotMuted
                        ? Icons.notifications_off_outlined
                        : Icons.notifications_outlined
                    : controller!.selectedRoomNotMuted(room.id)
                        ? Icons.notifications_off_outlined
                        : Icons.notifications_outlined,
                size: contextMenuIconSize,
                color: contextMenuIconColor,
              ),
              title: (controller!.selectMode == SelectMode.select)
                  ? Text(controller!.anySelectedRoomNotMuted ? 'Mute selected' : 'Unmute selected', style: contextMenuTextStyle)
                  : Text(controller!.selectedRoomNotMuted(room.id) ? 'Mute' : 'Unmute', style: contextMenuTextStyle),
              onTap: () async {
                Navigator.of(c).pop();
                (controller!.selectMode == SelectMode.select) ? controller!.toggleMuted : controller!.toggleMuted(roomId: room.id);
              },
            ),
          )
          ..add(
            const Divider(
              height: 2,
              endIndent: 8,
              indent: 8,
            ),
          );
        if (controller!.spaces.isNotEmpty) {
          contextMenuOptions.add(ListTile(
            dense: true,
            title: Text(
              L10n.of(context)!.addToSpace,
              style: contextMenuTextStyle,
            ),
            trailing: Icon(
              Icons.group_work_outlined,
              size: contextMenuIconSize,
              color: contextMenuIconColor,
            ),
            onTap: () async {
              Navigator.of(c).pop();
              (controller!.selectMode == SelectMode.select) ? controller!.addToSpace : controller!.addToSpace(roomId: room.id);
            },
          ));
        }
        contextMenuOptions
          ..add(ListTile(
            dense: true,
            trailing: Icon(
              (controller!.selectMode == SelectMode.select) ? Icons.deselect : Icons.select_all,
              size: contextMenuIconSize,
              color: contextMenuIconColor,
            ),
            title: Text((controller!.selectMode == SelectMode.select) ? 'Clear Selection' : 'Select Multiple', style: contextMenuTextStyle),
            onTap: () async {
              Navigator.of(c).pop();
              (controller!.selectMode == SelectMode.select) ? controller!.cancelAction() : controller!.toggleSelection(room.id);
            },
          ))
          ..add(const Divider(
            height: 2,
            endIndent: 8,
            indent: 8,
          ))
          ..add(ListTile(
            dense: true,
            trailing: const Icon(
              Icons.delete_outlined,
              size: contextMenuIconSize,
              color: Colors.redAccent,
            ),
            title: Text(L10n.of(context)!.archive, style: contextMenuTextStyle.copyWith(color: Colors.redAccent, fontWeight: FontWeight.w600)),
            onTap: () async {
              Navigator.of(c).pop();
              controller!.archiveAction(roomId: room.id);
            },
          ));
        return [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: contextMenuOptions,
          )
        ];
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 4, left: 4, right: 4),
        child: Material(
          borderRadius: BorderRadius.circular(12),
          color: selected
              ? PageMeThemes.isDarkMode(context)
                  ? Theme.of(context).colorScheme.primaryContainer.withOpacity(0.5)
                  : Theme.of(context).colorScheme.tertiary.withOpacity(0.3)
              : activeChat
                  ? PageMeThemes.isDarkMode(context)
                      ? Theme.of(context).colorScheme.tertiary.withOpacity(0.4)
                      : Theme.of(context).colorScheme.primaryContainer.withOpacity(0.5)
                  : Colors.transparent,
          child: ListTile(
            contentPadding: const EdgeInsets.symmetric(horizontal: 12),
            minVerticalPadding: 0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            hoverColor:
                PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiary.withOpacity(0.2) : Theme.of(context).colorScheme.primaryContainer.withOpacity(0.2),
            splashColor:
                PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiary.withOpacity(0.6) : Theme.of(context).colorScheme.primaryContainer.withOpacity(0.6),
            tileColor: Colors.transparent,
            selected: selected,
            visualDensity: FlavorConfig.minimalChatListMode ? null : const VisualDensity(vertical: -0.5),
            onLongPress: onLongPress,
            leading: selected
                ? SizedBox(
                    width: Avatar.defaultSize,
                    height: Avatar.defaultSize,
                    child: Material(
                      color: Theme.of(context).colorScheme.primaryContainer,
                      borderRadius: BorderRadius.circular(Avatar.defaultSize),
                      child: const Icon(Icons.check, color: Colors.white),
                    ),
                  )
                : Avatar(
                    mxContent: room.avatar,
                    name: room.displayname,
                    onTap: onLongPress,
                  ),
            title: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    room.getLocalizedDisplayname(MatrixLocals(L10n.of(context)!)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).colorScheme.onBackground,
                    ),
                  ),
                ),
                if (isMuted)
                  const Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: Icon(
                      Icons.notifications_off_outlined,
                      size: 16,
                    ),
                  ),
                Padding(
                  padding: const EdgeInsets.only(left: 4.0),
                  child: Text(
                    room.timeCreated.localizedTimeShort(context),
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: unread ? FontWeight.w500 : null,
                      color: unread
                          ? PageMeThemes.isDarkMode(context)
                              ? Theme.of(context).colorScheme.primary
                              : Theme.of(context).colorScheme.tertiary
                          : Theme.of(context).colorScheme.onBackground,
                    ),
                  ),
                ),
                if (FlavorConfig.minimalChatListMode)
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: AnimatedContainer(
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.bounceInOut,
                      padding: const EdgeInsets.symmetric(horizontal: 7),
                      height: unreadBubbleSize,
                      width: room.notificationCount == 0 && !unread && !room.hasNewMessages ? 0 : (unreadBubbleSize - 9) * room.notificationCount.toString().length + 9,
                      decoration: BoxDecoration(
                        color: room.highlightCount > 0 || room.membership == Membership.invite
                            ? Colors.red
                            : room.notificationCount > 0
                                ? PageMeThemes.isDarkMode(context)
                                    ? Theme.of(context).colorScheme.primary
                                    : Theme.of(context).colorScheme.tertiary
                                : Theme.of(context).colorScheme.primary.withAlpha(100),
                        borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
                      ),
                      child: Center(
                        child: room.notificationCount > 0 || room.markedUnread
                            ? Text(
                                room.notificationCount.toString(),
                                style: TextStyle(
                                  color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onTertiary,
                                  fontSize: 13,
                                ),
                              )
                            : const SizedBox.shrink(),
                      ),
                    ),
                  )
              ],
            ),
            subtitle: FlavorConfig.minimalChatListMode
                ? null
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      if (typingText.isEmpty && ownMessage && room.lastEvent!.status.isSending) ...[
                        const SizedBox(
                          width: 16,
                          height: 16,
                          child: CircularProgressIndicator.adaptive(strokeWidth: 2),
                        ),
                        const SizedBox(width: 4),
                      ],
                      AnimatedContainer(
                        width: typingText.isEmpty ? 0 : 18,
                        clipBehavior: Clip.hardEdge,
                        decoration: const BoxDecoration(),
                        duration: const Duration(milliseconds: 300),
                        curve: Curves.bounceInOut,
                        padding: const EdgeInsets.only(right: 4),
                        child: Icon(
                          Icons.edit_outlined,
                          color: Theme.of(context).colorScheme.secondary,
                          size: 14,
                        ),
                      ),
                      Expanded(
                        child: typingText.isNotEmpty
                            ? Text(
                                typingText,
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.secondary,
                                ),
                                softWrap: false,
                              )
                            : Text(
                                getChatMessage(context, room, MatrixLocals(L10n.of(context)!)),
                                softWrap: false,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.onBackground,
                                ),
                              ),
                      ),
                      const SizedBox(width: 8),
                      if (room.isFavourite)
                        Padding(
                          padding: EdgeInsets.only(right: room.notificationCount > 0 ? 4.0 : 0.0),
                          child: RotationTransition(
                            turns: const AlwaysStoppedAnimation(45 / 360),
                            child: Icon(
                              Icons.push_pin,
                              size: 18,
                              color: Theme.of(context).colorScheme.secondary,
                            ),
                          ),
                        ),
                      AnimatedContainer(
                        duration: const Duration(milliseconds: 300),
                        curve: Curves.bounceInOut,
                        padding: const EdgeInsets.symmetric(horizontal: 7),
                        height: unreadBubbleSize,
                        width: room.notificationCount == 0 && !unread && !room.hasNewMessages ? 0 : (unreadBubbleSize - 9) * room.notificationCount.toString().length + 9,
                        decoration: BoxDecoration(
                          color: room.highlightCount > 0 || room.membership == Membership.invite
                              ? Colors.red
                              : room.notificationCount > 0
                                  ? PageMeThemes.isDarkMode(context)
                                      ? Theme.of(context).colorScheme.primary
                                      : Theme.of(context).colorScheme.tertiary
                                  : Theme.of(context).colorScheme.primary.withAlpha(100),
                          borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
                        ),
                        child: Center(
                          child: room.notificationCount > 0 || room.markedUnread
                              ? Text(
                                  room.notificationCount.toString(),
                                  style: TextStyle(
                                    color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onTertiary,
                                    fontSize: 13,
                                  ),
                                )
                              : const SizedBox.shrink(),
                        ),
                      ),
                    ],
                  ),
            onTap: () => (shareFiles.isNotEmpty) ? onLongPress!() : clickAction(context),
          ),
        ),
      ),
    );
  }
}
