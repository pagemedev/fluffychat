import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:collection/collection.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/logger_functions.dart';

import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../../utils/localized_exception_extension.dart';
import '../../utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import '../../widgets/avatar.dart';
import '../../widgets/context_menu/custom_context_menu.dart';
import '../../widgets/matrix.dart';
import '../chat/chat.dart';
import 'chat_list.dart';
import 'chat_list_item.dart';

class SpaceView extends StatefulWidget {
  final ChatListController controller;
  final ScrollController scrollController;
  const SpaceView(
    this.controller, {
    Key? key,
    required this.scrollController,
  }) : super(key: key);

  @override
  State<SpaceView> createState() => _SpaceViewState();
}

class _SpaceViewState extends State<SpaceView> {
  static final Map<String, Future<GetSpaceHierarchyResponse>> _requests = {};
  late final Client client;

  @override
  void didChangeDependencies() {
    client = Matrix.of(context).client;
    super.didChangeDependencies();
  }

  String? prevBatch;

  void _refresh() {
    setState(() {
      _requests.remove(widget.controller.activeSpaceId);
    });
  }

  Future<GetSpaceHierarchyResponse> getFuture(String activeSpaceId) => _requests[activeSpaceId] ??= Matrix.of(context).client.getSpaceHierarchy(
        activeSpaceId,
        maxDepth: 1,
        from: prevBatch,
      );

  void _onJoinSpaceChild(SpaceRoomsChunk spaceChild) async {
    final space = client.getRoomById(widget.controller.activeSpaceId!);
    if (client.getRoomById(spaceChild.roomId) == null) {
      final result = await showFutureLoadingDialog(
        context: context,
        future: () async {
          await client.joinRoom(
            spaceChild.roomId,
            serverName: space?.spaceChildren
                .firstWhereOrNull(
                  (child) => child.roomId == spaceChild.roomId,
                )
                ?.via,
          );
          if (client.getRoomById(spaceChild.roomId) == null) {
            // Wait for room actually appears in sync
            await client.waitForRoomInSync(spaceChild.roomId, join: true);
          }
        },
      );
      if (result.error != null) return;
      _refresh();
    }
    if (spaceChild.roomType == 'm.space') {
      if (spaceChild.roomId == widget.controller.activeSpaceId) {
        context.go('/rooms/${spaceChild.roomId}');
      } else {
        widget.controller.setActiveSpace(spaceChild.roomId);
      }
      return;
    }
    context.go('/rooms/${spaceChild.roomId}');
  }

  void _onSpaceChildContextMenu([
    SpaceRoomsChunk? spaceChild,
    Room? room,
  ]) async {
    final client = Matrix.of(context).client;
    final activeSpaceId = widget.controller.activeSpaceId;
    final activeSpace = activeSpaceId == null ? null : client.getRoomById(activeSpaceId);
    final action = await showModalActionSheet<SpaceChildContextAction>(
      context: context,
      title: spaceChild?.name ??
          room?.getLocalizedDisplayname(
            MatrixLocals(L10n.of(context)!),
          ),
      message: ((spaceChild?.topic ?? room?.topic) == null || (spaceChild?.topic ?? room?.topic) == '') ? null : spaceChild?.topic ?? room?.topic,
      actions: [
        if (room == null)
          SheetAction(
            key: SpaceChildContextAction.join,
            label: L10n.of(context)!.joinRoom,
            icon: Icons.send_outlined,
          ),
        if (spaceChild != null && (activeSpace?.canSendDefaultStates ?? false))
          SheetAction(
            key: SpaceChildContextAction.removeFromSpace,
            label: L10n.of(context)!.removeFromSpace,
            icon: Icons.delete_sweep_outlined,
          ),
        if (room != null)
          SheetAction(
            key: SpaceChildContextAction.leave,
            label: L10n.of(context)!.leave,
            icon: Icons.delete_outlined,
            isDestructiveAction: true,
          ),
      ],
    );
    if (action == null) return;

    switch (action) {
      case SpaceChildContextAction.join:
        _onJoinSpaceChild(spaceChild!);
        break;
      case SpaceChildContextAction.leave:
        await showFutureLoadingDialog(
          context: context,
          future: room!.leave,
        );
        break;
      case SpaceChildContextAction.removeFromSpace:
        await showFutureLoadingDialog(
          context: context,
          future: () => activeSpace!.removeSpaceChild(spaceChild!.roomId),
        );
        setState(() {});
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    final activeSpaceId = widget.controller.activeSpaceId;
    final allSpaces = client.rooms.where((room) => room.isSpace);
    if (activeSpaceId == null) {
      return _buildAllSpacesView(allSpaces, context);
    }
    return FutureBuilder<GetSpaceHierarchyResponse>(
      future: getFuture(activeSpaceId),
      builder: (context, snapshot) {
        final response = snapshot.data;
        final error = snapshot.error;
        if (error != null) {
          return _buildError(error);
        }
        if (response == null) {
          return const Center(child: CircularProgressIndicator.adaptive());
        }
        final parentSpace = allSpaces.firstWhereOrNull(
          (space) => space.spaceChildren.any((child) => child.roomId == activeSpaceId),
        );
        final List<SpaceRoomsChunk> directChatsInSpace;
        final List<SpaceRoomsChunk> spaceChildren;
        if (FlavorConfig.showDirectChatsInSpaces) {
          directChatsInSpace = getDirectChatsInSpace(client.getRoomById(activeSpaceId), client);
          spaceChildren = [...response.rooms, ...directChatsInSpace];
        } else {
          spaceChildren = response.rooms;
        }

        final canLoadMore = response.nextBatch != null;
        return WillPopScope(
          onWillPop: () async => _handleSystemPop(parentSpace),
          child: ListView.builder(
            itemCount: spaceChildren.length + 1 + (canLoadMore ? 1 : 0),
            controller: widget.scrollController,
            itemBuilder: (context, i) {
              if (i == 0) {
                return _buildSpaceHeader(context, snapshot);
              }
              i--;
              if (canLoadMore && i == spaceChildren.length) {
                return _buildLoadMoreItem(context, response);
              }
              //This is for when you are joined to the room in the space.
              final item = _buildJoinedRoomItem(i, spaceChildren);
              if (item is! SizedBox) {
                return item;
              }

              //This is here for when we arent joined to the room in the space.
              return _buildUnJoinedRoomOrSpaceItem(i, spaceChildren);
            },
          ),
        );
      },
    );
  }

  List<SpaceRoomsChunk> getDirectChatsInSpace(Room? space, Client client) {
    if (space != null) {
      final List<Room> rooms = client.rooms.where((room) {
        if (!room.isDirectChat) {
          return false;
        }
        if (room.summary.mHeroes != null &&
            room.summary.mHeroes!.any((userId) {
              final user = space.getState(EventTypes.RoomMember, userId)?.asUser;
              return user != null && user.membership == Membership.join;
            })) {
          return true;
        }
        return false;
      }).toList();

      return convertRoomsToChunks(rooms);
    } else {
      return [];
    }
  }

  List<SpaceRoomsChunk> convertRoomsToChunks(List<Room> rooms) {
    return rooms.map((room) {
      return SpaceRoomsChunk(
        avatarUrl: room.avatar, // assuming avatarUrl is in room.summary
        canonicalAlias: room.canonicalAlias, // assuming canonicalAlias is in room
        guestCanJoin: false, // not available in Room, hardcoding to false
        joinRule: room.getState(EventTypes.RoomJoinRules)?.content['join_rule'] as String?, // not available in Room, hardcoding to "public"
        name: room.getLocalizedDisplayname(), // assuming name is in room.summary.displayname
        numJoinedMembers: room.getParticipants([Membership.join]).length, // assuming numJoinedMembers is in room.summary.joinedMembersCount
        roomId: room.id,
        roomType: room.getState(EventTypes.RoomCreate)?.content.tryGet<String>('type'), // not available in Room, setting to null
        topic: room.topic, // not available in Room, setting to null
        worldReadable: false, // not available in Room, hardcoding to false
        childrenState: [], // not available in Room, setting to empty list
      );
    }).toList();
  }

  Widget _buildJoinedRoomItem(int i, List<SpaceRoomsChunk> spaceChildren) {
    final spaceChild = spaceChildren[i];
    final room = client.getRoomById(spaceChild.roomId);
    if (room != null && !room.isSpace) {
      return ChatListItem(
        room,
        controller: ChatList.of(context),
        onLongPress: () => _onSpaceChildContextMenu(spaceChild, room),
        activeChat: widget.controller.activeChat == room.id,
      );
    }
    return const SizedBox.shrink();
  }

// This function builds an item for an unjoined room or a space.
  Widget _buildUnJoinedRoomOrSpaceItem(int i, List<SpaceRoomsChunk> spaceChildren) {
    final spaceChild = spaceChildren[i];
    final room = client.getRoomById(spaceChild.roomId);
    final isSpace = spaceChild.roomType == 'm.space';
    final topic = spaceChild.topic?.isEmpty ?? true ? null : spaceChild.topic;
    return !isSpace ? _buildUnJoinedRoom(spaceChild, context, isSpace, room, topic) : const SizedBox.shrink();
  }

  ListView _buildAllSpacesView(Iterable<Room> allSpaces, BuildContext context) {
    final rootSpaces = allSpaces
        .where(
          (space) => !allSpaces.any(
            (parentSpace) => parentSpace.spaceChildren.any((child) => child.roomId == space.id),
          ),
        )
        .toList();
    Logs().i('Rootspaces length: ${rootSpaces.length}');
    final ChatListController? controller = ChatList.of(context);
    final TextStyle contextMenuTextStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w500,
      color: Theme.of(context).colorScheme.onBackground,
    );
    final Color contextMenuIconColor = Theme.of(context).colorScheme.onBackground;
    const double contextMenuIconSize = 20;
    return ListView.builder(
      itemCount: rootSpaces.length,
      controller: widget.scrollController,
      itemBuilder: (context, i) {
        final rootSpace = rootSpaces[i];
        final displayname = rootSpace.getLocalizedDisplayname(
          MatrixLocals(L10n.of(context)!),
        );
        return Material(
          color: Theme.of(context).colorScheme.background,
          child: buildContextMenuArea(controller, context, contextMenuTextStyle, contextMenuIconSize, rootSpace, contextMenuIconColor, displayname),
        );
      },
    );
  }

  CustomContextMenuArea buildContextMenuArea(
    ChatListController? controller,
    BuildContext context,
    TextStyle contextMenuTextStyle,
    double contextMenuIconSize,
    Room rootSpace,
    Color contextMenuIconColor,
    String displayname,
  ) {
    return CustomContextMenuArea(
      width: 200,
      builder: (c) {
        if (controller == null) {
          return [
            ListTile(
                title: Text(
                  'No Actions',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).colorScheme.onBackground),
                ),
                dense: true)
          ];
        }
        return [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                dense: true,
                title: Text('Space Details', style: contextMenuTextStyle.copyWith(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.w600)),
                trailing: Icon(
                  Icons.workspaces_outlined,
                  size: contextMenuIconSize,
                  color: Theme.of(context).colorScheme.primary,
                ),
                onTap: () async {
                  Navigator.of(c).pop();
                  context.go('/rooms/${rootSpace.id}/large_details');
                },
              ),
              const Divider(
                height: 2,
                endIndent: 8,
                indent: 8,
              ),
              ListTile(
                dense: true,
                title: Text('Invite People', style: contextMenuTextStyle),
                trailing: Icon(
                  Icons.person_add_outlined,
                  size: contextMenuIconSize,
                  color: contextMenuIconColor,
                ),
                onTap: () async {
                  Navigator.of(c).pop();
                  context.go('/rooms/${rootSpace.id}/invite');
                },
              ),
              const Divider(
                height: 2,
                endIndent: 8,
                indent: 8,
              ),
              ListTile(
                dense: true,
                title: Text('Leave Space', style: contextMenuTextStyle.copyWith(color: Colors.redAccent, fontWeight: FontWeight.w600)),
                trailing: Icon(
                  Icons.exit_to_app_outlined,
                  size: contextMenuIconSize,
                  color: Colors.redAccent,
                ),
                onTap: () async {
                  Navigator.of(c).pop();
                  final confirmed = await showOkCancelAlertDialog(
                    useRootNavigator: false,
                    context: context,
                    title: L10n.of(context)!.areYouSure,
                    okLabel: L10n.of(context)!.ok,
                    cancelLabel: L10n.of(context)!.cancel,
                  );
                  if (confirmed == OkCancelResult.ok) {
                    final success = await showFutureLoadingDialog(context: context, future: () => rootSpace.leave());
                    if (success.error == null) {
                      context.go('/rooms');
                    }
                  }
                },
              )
            ],
          )
        ];
      },
      child: ListTile(
        leading: Avatar(
          mxContent: rootSpace.avatar,
          name: displayname,
        ),
        title: Text(
          displayname,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        subtitle: Text(
          getAllSpaceTitle(context, rootSpace, displayname),
        ),
        onTap: () => clickActionAllSpaces(context, rootSpace),
        onLongPress: () => _onSpaceChildContextMenu(null, rootSpace),
        trailing: rootSpace.membership == Membership.invite
            ? AnimatedContainer(
                duration: const Duration(milliseconds: 300),
                curve: Curves.bounceInOut,
                padding: const EdgeInsets.symmetric(horizontal: 7),
                height: 14,
                width: (14 - 9) * rootSpace.notificationCount.toString().length + 9,
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
                ),
                child: const Center(
                  child: SizedBox.shrink(),
                ),
              )
            : const Icon(Icons.chevron_right_outlined),
      ),
    );
  }

  String getAllSpaceTitle(BuildContext context, Room room, String displayName) {
    if (room.membership == Membership.invite) {
      return "You are invited to this space";
    } else {
      return L10n.of(context)!.numChats(room.spaceChildren.length.toString());
    }
  }

  void clickActionAllSpaces(BuildContext context, Room room) async {
    if (room.membership == Membership.invite) {
      final inviterId = room.getState(EventTypes.RoomMember, room.client.userID!)?.senderId;
      final inviteAction = await showModalActionSheet<InviteActions>(
        context: context,
        message: room.isDirectChat
            ? "📨 Invite private chat"
            : room.isSpace
                ? "📨 Invite to a space"
                : "📨 Invite group chat",
        title: room.getLocalizedDisplayname(MatrixLocals(L10n.of(context)!)),
        actions: [
          SheetAction(
            key: InviteActions.accept,
            label: L10n.of(context)!.accept,
            icon: Icons.check_outlined,
            isDefaultAction: true,
          ),
          SheetAction(
            key: InviteActions.decline,
            label: "Decline",
            icon: Icons.close_outlined,
            isDestructiveAction: true,
          ),
          SheetAction(
            key: InviteActions.block,
            label: "Block",
            icon: Icons.block_outlined,
            isDestructiveAction: true,
          ),
        ],
      );
      if (inviteAction == null) return;
      if (inviteAction == InviteActions.block) {
        context.go('/rooms/settings/security/ignorelist', extra: inviterId);
        return;
      }
      if (inviteAction == InviteActions.decline) {
        await showFutureLoadingDialog(
          context: context,
          future: room.leave,
        );
        return;
      }
      final joinResult = await showFutureLoadingDialog(
        context: context,
        future: () async {
          final waitForRoom = room.client.waitForRoomInSync(
            room.id,
            join: true,
          );
          await room.join();
          await waitForRoom;
        },
      );
      if (joinResult.error != null) return;
    }

    if (room.membership == Membership.ban) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "You were banned from this space",
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
      return;
    }

    if (room.membership == Membership.leave) {
      final action = await showModalActionSheet<ArchivedRoomAction>(
        context: context,
        title: L10n.of(context)!.archivedRoom,
        message: L10n.of(context)!.thisRoomHasBeenArchived,
        actions: [
          SheetAction(
            label: L10n.of(context)!.rejoin,
            key: ArchivedRoomAction.rejoin,
          ),
          SheetAction(
            label: L10n.of(context)!.delete,
            key: ArchivedRoomAction.delete,
            isDestructiveAction: true,
          ),
        ],
      );
      if (action != null) {
        switch (action) {
          case ArchivedRoomAction.delete:
            await archiveAction(context, room);
            break;
          case ArchivedRoomAction.rejoin:
            await showFutureLoadingDialog(
              context: context,
              future: () => room.join(),
            );
            break;
        }
      }
    }

    if (room.membership == Membership.join) {
      widget.controller.setActiveSpace(room.id);
    }
  }

  Future<void> archiveAction(BuildContext context, Room room) async {
    {
      if ([Membership.leave, Membership.ban].contains(room.membership)) {
        final success = await showFutureLoadingDialog(
          context: context,
          future: () => room.forget(),
        );
        return;
      }
      final confirmed = await showOkCancelAlertDialog(
        useRootNavigator: false,
        context: context,
        title: L10n.of(context)!.areYouSure,
        okLabel: L10n.of(context)!.yes,
        cancelLabel: L10n.of(context)!.no,
      );
      if (confirmed == OkCancelResult.cancel) return;
      await showFutureLoadingDialog(context: context, future: () => room.leave());
      return;
    }
  }

  Widget _buildError(Object error) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(error.toLocalizedString(context)),
        ),
        IconButton(
          onPressed: _refresh,
          icon: const Icon(Icons.refresh_outlined),
        )
      ],
    );
  }

  Future<bool> _handleSystemPop(Room? parentSpace) async {
    if (parentSpace != null) {
      widget.controller.setActiveSpace(parentSpace.id);
      return false;
    }
    return true;
  }

  Widget _buildSpaceHeader(BuildContext context, AsyncSnapshot snapshot) {
    return Column(
      children: [
        ListTile(
            leading: BackButton(
              onPressed: () => widget.controller.onDestinationSelected(0),
            ),
            title: Text(L10n.of(context)!.allChats),
            trailing: IconButton(
              icon: snapshot.connectionState != ConnectionState.done
                  ? const SizedBox(
                      width: 24,
                      height: 24,
                      child: CircularProgressIndicator.adaptive(strokeWidth: 2),
                    )
                  : const Icon(Icons.refresh_outlined),
              onPressed: snapshot.connectionState != ConnectionState.done ? null : _refresh,
            )),
        Container(
          height: 1,
          color: Theme.of(context).colorScheme.tertiaryContainer.withOpacity(0.4),
        )
      ],
    );
  }

  Widget _buildLoadMoreItem(BuildContext context, GetSpaceHierarchyResponse response) {
    return ListTile(
      title: Text(L10n.of(context)!.loadMore),
      trailing: const Icon(Icons.chevron_right_outlined),
      onTap: () {
        prevBatch = response.nextBatch;
        _refresh();
      },
    );
  }

  ListTile _buildUnJoinedRoom(SpaceRoomsChunk spaceChild, BuildContext context, bool isSpace, Room? room, String? topic) {
    return ListTile(
      leading: Avatar(
        mxContent: spaceChild.avatarUrl,
        name: spaceChild.name,
      ),
      title: Row(
        children: [
          Expanded(
            child: Text(
              spaceChild.name ?? spaceChild.canonicalAlias ?? L10n.of(context)!.chat,
              maxLines: 1,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          if (!isSpace) ...[
            const Icon(
              Icons.people_outline,
              size: 16,
            ),
            const SizedBox(width: 4),
            Text(
              spaceChild.numJoinedMembers.toString(),
              style: const TextStyle(fontSize: 14),
            ),
          ],
        ],
      ),
      onTap: () => _onJoinSpaceChild(spaceChild),
      onLongPress: () => _onSpaceChildContextMenu(spaceChild, room),
      subtitle: Text(
        topic ?? (isSpace ? L10n.of(context)!.enterSpace : L10n.of(context)!.enterRoom),
        maxLines: 1,
        style: TextStyle(
          color: Theme.of(context).colorScheme.onBackground,
        ),
      ),
      trailing: isSpace ? const Icon(Icons.chevron_right_outlined) : null,
    );
  }
}

enum SpaceChildContextAction {
  join,
  leave,
  removeFromSpace,
}
