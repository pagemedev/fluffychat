import 'dart:async';
import 'dart:io';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
//import 'package:image_editor_plus/data/image_item.dart';
//import 'package:image_editor_plus/image_editor_plus.dart';

import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/chat_list/chat_list_view.dart';
import 'package:pageMe/pages/chat_list/spaces_entry.dart';
import 'package:pageMe/utils/localized_exception_extension.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/client_stories_extension.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/push_factorys/push_ultity.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:uni_links/uni_links.dart';

import '../../../utils/account_bundles.dart';
import '../../config/themes.dart';
import '../../cubits/file_handler/file_handler_bloc.dart';
import '../../cubits/sending_handler/sending_notification_handler.dart';
import '../../pageme_app.dart';
import '../../utils/matrix_sdk_extensions.dart/matrix_file_extension.dart';
import '../../utils/send_service.dart';
import '../../utils/tor_stub.dart';
import '../../utils/url_launcher.dart';
import '../../utils/voip/callkeep_manager.dart';
import '../../widgets/future_loading_dialog.dart';
import '../../widgets/matrix.dart';
import '../bootstrap/bootstrap_dialog.dart';
import '../settings_account/settings_account.dart';
import 'chat_list_provider.dart';

enum SelectMode { normal, share, select }

enum PopupMenuAction {
  settings,
  invite,
  newGroup,
  newSpace,
  setStatus,
  archive,
}

enum ActiveFilter {
  allChats,
  groups,
  messages,
  spaces,
}

class ChatList extends StatefulWidget {
  static BuildContext? contextForVoip;
  final String? activeChat;
  final bool displayNavigationRail;
  const ChatList({Key? key, this.displayNavigationRail = false, required this.activeChat}) : super(key: key);

  @override
  ChatListController createState() => ChatListController();

  static ChatListController? of(BuildContext context) {
    final ChatListControllerProvider? result = context.dependOnInheritedWidgetOfExactType<ChatListControllerProvider>();
    if (result == null) {
      throw FlutterError('ChatControllerProvider not found in the widget tree');
    }
    return result.chatListController;
  }
}

class ChatListController extends State<ChatList> with TickerProviderStateMixin, WidgetsBindingObserver, AutomaticKeepAliveClientMixin {
  bool _isInitialized = false;
  StreamSubscription? _intentDataStreamSubscription;
  StreamSubscription? _intentFileStreamSubscription;
  ChatService chatService = ChatService();
  StreamSubscription? _intentUriStreamSubscription;
  bool isSearchMode = false;
  Future<QueryPublicRoomsResponse>? publicRoomsResponse;
  String? searchServer;
  Timer? _coolDown;
  SearchUserDirectoryResponse? userSearchResult;
  QueryPublicRoomsResponse? roomSearchResult;
  bool isDrawerExpanded = false;
  bool isSearching = false;
  bool isOnBoarding = false;
  late MatrixState matrix;
  late FileHandlerBloc fileHandlerBloc;
  int counter = 0;

  void toggleIsOnBoarding(bool isOnBoarding) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        this.isOnBoarding = isOnBoarding;
      });
    });
  }

  void updateState() {
    setState(() {});
  }

  SpacesEntry? _activeSpacesEntry;

  SpacesEntry get activeSpacesEntry {
    final id = _activeSpacesEntry;
    return (id == null || !id.stillValid(context)) ? defaultSpacesEntry : id;
  }

  bool isTorBrowser = false;
  BoxConstraints? snappingSheetContainerSize;

  //String? get activeSpaceId => activeSpacesEntry.getSpace(context)?.id;

  String? activeSpaceId;

  final ScrollController scrollController = ScrollController();
  bool scrolledToTop = true;

  final StreamController<Client> _clientStream = StreamController.broadcast();

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  Stream<Client> get clientStream => _clientStream.stream;

  ActiveFilter activeFilter = FlavorConfig.separateChatTypes ? ActiveFilter.messages : ActiveFilter.allChats;

  // Note this controller gets disposed of by the SearchBar Widget. DO NOT DISPOSE OF IT HERE!
  // see /Users/pageme/.pub-cache/hosted/pub.dev/searchbar_animation-0.0.4/lib/src/searchbar.dart:16
  final TextEditingController searchController = TextEditingController();

  bool isSearchBarOpen = false;

  void setSearchBarState(bool isOpen) {
    setState(() {
      isSearchBarOpen = isOpen;
    });
  }

  void _search() async {
    final client = matrix.client;
    if (!isSearching) {
      setState(() {
        isSearching = true;
      });
    }
    SearchUserDirectoryResponse? userSearchResult;
    QueryPublicRoomsResponse? roomSearchResult;
    try {
      roomSearchResult = await client.queryPublicRooms(
        filter: PublicRoomQueryFilter(genericSearchTerm: searchController.text),
        limit: 20,
      );
      userSearchResult = await client.searchUserDirectory(
        searchController.text,
        limit: 20,
      );
    } catch (e, s) {
      Logs().w('Searching has crashed', e, s);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            e.toLocalizedString(context),
          ),
        ),
      );
    }
    if (!isSearchMode) return;
    setState(() {
      isSearching = false;
      this.roomSearchResult = roomSearchResult;
      this.userSearchResult = userSearchResult;
    });
  }

  void toggleDrawerExpansion() {
    setState(() {
      isDrawerExpanded = !isDrawerExpanded;
    });
  }

  void onSearchEnter(String text) {
    if (text.isEmpty) {
      cancelSearch(unfocus: false);
      return;
    }

    setState(() {
      isSearchMode = true;
    });
    _coolDown?.cancel();
    _coolDown = Timer(const Duration(milliseconds: 500), _search);
  }

  void cancelSearch({bool unfocus = true}) {
    searchController.clear();
    setState(() {

      isSearchMode = false;
      roomSearchResult = userSearchResult = null;
      isSearching = false;
    });
    if (unfocus) FocusManager.instance.primaryFocus?.unfocus();
  }

  void onDestinationSelected(int? i) {
    setState(() {
      activeFilter = getActiveFilterByDestination(i);
    });
  }

  ActiveFilter getActiveFilterByDestination(int? i) {
    switch (i) {
      case 1:
        if (FlavorConfig.separateChatTypes) {
          return ActiveFilter.messages;
        }
        resetActiveSpaceId();
        return ActiveFilter.spaces;
      case 2:
        resetActiveSpaceId();
        return ActiveFilter.spaces;
      case 0:
      default:
        if (FlavorConfig.separateChatTypes) {
          return ActiveFilter.groups;
        }
        return ActiveFilter.allChats;
    }
  }

  bool Function(Room) getRoomFilterByActiveFilter(ActiveFilter activeFilter) {
    switch (activeFilter) {
      case ActiveFilter.allChats:
        return (room) => !room.isSpace && !room.isStoryRoom;
      case ActiveFilter.groups:
        return (room) => !room.isSpace && !room.isDirectChat && !room.isStoryRoom;
      case ActiveFilter.messages:
        return (room) => !room.isSpace && room.isDirectChat && !room.isStoryRoom;
      case ActiveFilter.spaces:
        return (r) => r.isSpace;
    }
  }

  int get selectedIndex {
    switch (activeFilter) {
      case ActiveFilter.allChats:
        return 0;
      case ActiveFilter.groups:
        return 0;
      case ActiveFilter.messages:
        return 1;
      case ActiveFilter.spaces:
        return FlavorConfig.separateChatTypes ? 2 : 1;
    }
  }

  void resetActiveSpaceId() {
    setState(() {
      activeSpaceId = null;
    });
  }

  void setActiveSpace(String? spaceId) {
    setState(() {
      activeSpaceId = spaceId;
      activeFilter = ActiveFilter.spaces;
    });
  }

  List<Room> get filteredRooms => matrix.client.rooms.where(getRoomFilterByActiveFilter(activeFilter)).toList();

  void _onScroll() {
    final newScrolledToTop = scrollController.position.pixels <= 0;
    if (newScrolledToTop != scrolledToTop) {
      setState(() {
        scrolledToTop = newScrolledToTop;
      });
    }
  }

  void setActiveSpacesEntry(BuildContext context, SpacesEntry? spaceId) {
    setState(() => _activeSpacesEntry = spaceId);
  }

  void editSpace(BuildContext context, String spaceId) async {
    await matrix.client.getRoomById(spaceId)!.postLoad();
    if (mounted) {
      await context.push('/rooms/$spaceId/details');
    }
  }

  // Needs to match GroupsSpacesEntry for 'separate group' checking.
  List<Room> get spaces => matrix.client.rooms.where((r) => r.isSpace).toList();

  // Note that this could change due to configuration, etc.
  // Also be aware that _activeSpacesEntry = null is the expected reset method.
  SpacesEntry get defaultSpacesEntry => FlavorConfig.separateChatTypes ? DirectChatsSpacesEntry() : AllRoomsSpacesEntry();

  List<SpacesEntry> get spacesEntries {
    if (FlavorConfig.separateChatTypes) {
      return [defaultSpacesEntry, GroupsSpacesEntry(), ...spaces.map((space) => SpaceSpacesEntry(space)).toList()];
    }
    return [defaultSpacesEntry, ...spaces.map((space) => SpaceSpacesEntry(space)).toList()];
  }

  final selectedRoomIds = <String>{};
  final selectedShareRoomIds = <String>{};
  final selectedShareRoomNames = <String>{};
  bool? crossSigningCached;
  bool showChatBackupBanner = false;
  bool isSending = false;

  Future<void> checkBootstrap() async {
    final client = matrix.client;
    if (!client.encryptionEnabled) return;
    await client.accountDataLoading;
    await client.userDeviceKeysLoading;
    if (client.prevBatch == null) {
      await client.onSync.stream.first;
    }
    final crossSigning = await client.encryption?.crossSigning.isCached() ?? false;
    Logs().d("CrossSigning: $crossSigning"); // debug
    final needsBootstrap = await client.encryption?.keyManager.isCached() == false || client.encryption?.crossSigning.enabled == false || crossSigning == false;
    Logs().d("NeedsBootstrap: $needsBootstrap"); // debug
    final isUnknownSession = client.isUnknownSession;
    Logs().d("IsUnknownSession: $isUnknownSession"); // debug
    setState(() {
      showChatBackupBanner = needsBootstrap || isUnknownSession;
      Logs().d("ShowChatBackupBanner: $showChatBackupBanner"); // debug
    });
  }

  void firstRunBootstrapAction() async {
    final result = await BootstrapDialog(
      client: matrix.client,
    ).show(context);
    context.go('/rooms');
    await checkBootstrap();
  }

  String? get activeChat => GoRouterState.of(context).pathParameters['roomid'];

  SelectMode get selectMode => (fileHandlerBloc.state.isInitiated &&
          (PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms/' || PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'))
      ? SelectMode.share
      : selectedRoomIds.isEmpty
          ? SelectMode.normal
          : SelectMode.select;

  void toggleDrawer(GlobalKey<ScaffoldState> key) {
    if (key.currentState!.isDrawerOpen) {
      key.currentState!.closeDrawer();
    } else {
      key.currentState!.openDrawer();
    }
  }

  void _processIncomingSharedFiles(List<SharedMediaFile> files) {
    if (files.isEmpty) return;
    matrix.shareContent ??= [];
    for (final file in files) {
      final finalFile = File(file.path.replaceFirst('file://', ''));
      matrix.shareContent!.add({
        'msgtype': FlavorConfig.appId,
        'file': MatrixFile(
          bytes: finalFile.readAsBytesSync(),
          name: finalFile.path,
        ).detectFileType,
      });
    }
    context.go('/rooms');
    sendFiles();
  }

  void _processIncomingSharedText(String? text) {
    if (text == null) return;
    if (text.toLowerCase().startsWith(FlavorConfig.deepLinkPrefix) ||
        text.toLowerCase().startsWith(FlavorConfig.inviteLinkPrefix) ||
        (text.toLowerCase().startsWith(FlavorConfig.schemePrefix) && !RegExp(r'\s').hasMatch(text))) {
      Logs().v("_processIncomingSharedText - matched a deeplink, invite link or scheme\n$text");
      return _processIncomingUris(text);
    }
    matrix.shareContent = [
      {
        'msgtype': 'm.text',
        'body': text,
      }
    ];
    context.go('/rooms');
    sendFiles();
  }

  void _processIncomingUris(String? text) async {
    if (text == null) return;
    context.go('/rooms');
    WidgetsBinding.instance.addPostFrameCallback((_) {
      UrlLauncher(context, text).openMatrixToUrl();
    });
  }

  Future<void> _initReceiveSharingIntent() async {
    if (!PlatformInfos.isMobile) return;
    // For sharing images coming from outside the app while the app is in the memory
    _intentFileStreamSubscription = ReceiveSharingIntent.getMediaStream().listen(_processIncomingSharedFiles, onError: (e) => loggerError(logMessage: e.toString()));

    // For sharing images coming from outside the app while the app is closed
    await ReceiveSharingIntent.getInitialMedia().then(_processIncomingSharedFiles);

    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    _intentDataStreamSubscription = ReceiveSharingIntent.getTextStream().listen(_processIncomingSharedText, onError: (e) => loggerError(logMessage: e.toString()));

    // For sharing or opening urls/text coming from outside the app while the app is closed
    await ReceiveSharingIntent.getInitialText().then(_processIncomingSharedText);
    // For receiving shared Uris
    _intentUriStreamSubscription = linkStream.listen(_processIncomingUris);
    if (PageMeApp.gotInitialLink == false) {
      PageMeApp.gotInitialLink = true;
      await getInitialLink().then(_processIncomingUris);
    }
  }

  @override
  void initState() {
    //checkBootstrap();
    //_initReceiveSharingIntent();
    scrollController.addListener(_onScroll);
    _hackyWebRTCFixForWeb();
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    matrix = Matrix.of(context);
    fileHandlerBloc = BlocProvider.of<FileHandlerBloc>(context);
    if (!_isInitialized) {
      await _waitForFirstSync();
      await checkBootstrap();
      await _initReceiveSharingIntent();
      await CallKeepManager().initialize();
      await _checkTorBrowser();
      _isInitialized = true;
    }
    super.didChangeDependencies();
  }

  @override
  Future<void> dispose() async {
    if (fileHandlerBloc.state.isInitiated) {
      fileHandlerBloc.add(const FileCompletionRequested());
    }
    if (selectedShareRoomIds.isNotEmpty && selectedShareRoomNames.isNotEmpty) {
      clearSelectedRooms();
    }
    if (matrix.shareContent != null && matrix.shareContent!.isNotEmpty) {
      clearShareContent();
    }
    scrollController.removeListener(_onScroll);
    super.dispose();
    await _intentDataStreamSubscription?.cancel();
    await _intentFileStreamSubscription?.cancel();
    await _intentUriStreamSubscription?.cancel();
  }

  void toggleSelection(String roomId) {
    Logs().i('toggled: $roomId');
    setState(() => selectedRoomIds.contains(roomId) ? selectedRoomIds.remove(roomId) : selectedRoomIds.add(roomId));
  }

  void toggleShareSelection(String roomId) {
    final String roomName = matrix.client.getRoomById(roomId)!.getLocalizedDisplayname();
    setState(() {
      selectedShareRoomIds.contains(roomId) ? selectedShareRoomIds.remove(roomId) : selectedShareRoomIds.add(roomId);
      selectedShareRoomNames.contains(roomName) ? selectedShareRoomNames.remove(roomName) : selectedShareRoomNames.add(roomName);
    });
  }

  Future<void> toggleUnread({String? roomId}) async {
    if (roomId != null && selectedRoomIds.isEmpty) {
      selectedRoomIds.contains(roomId) ? selectedRoomIds.remove(roomId) : selectedRoomIds.add(roomId);
    }
    await showFutureLoadingDialog(
      context: context,
      future: () async {
        final markUnread = anySelectedRoomNotMarkedUnread;
        final client = matrix.client;
        for (final roomId in selectedRoomIds) {
          final room = client.getRoomById(roomId)!;
          if (room.markedUnread == markUnread) continue;
          await client.getRoomById(roomId)!.markUnread(markUnread);
        }
      },
    );
    cancelAction();
  }

  void sortShareContent(List<Map<String, dynamic>?> shareContent, List<MatrixFile> shareFiles, List<Map<String, dynamic>> shareEvents) {
    for (final Map<String, dynamic>? content in shareContent) {
      if (content!.tryGet<String>('msgtype') == FlavorConfig.appId && content.tryGet<MatrixFile>('file') != null) {
        shareFiles.add(content.tryGet<MatrixFile>('file')!);
      } else {
        shareEvents.add(content);
      }
    }
  }

  Future<void> handleShareFiles(List<MatrixFile> shareFiles, BuildContext context) async {
    fileHandlerBloc.add(MatrixFileAdditionRequested(shareFiles));
  }

  void onCancel() {
    setState(() {
      clearSelectedRooms();
      clearShareContent();
    });
    Navigator.maybePop(context);
  }

  Future<void> handleShareEvents(List<Map<String, dynamic>> shareEvents, Client client) async {
    fileHandlerBloc.add(ShareEventAdditionRequested(shareEvents: shareEvents));
    navigateToRooms();
    setState(() {
      isSending = true;
    });
    for (final String roomId in selectedShareRoomIds) {
      for (final Map<String, dynamic> file in shareEvents) {
        if (client.getRoomById(roomId) == null) return;
        client.getRoomById(roomId)!.sendEvent(file);
        await Future.delayed(const Duration(milliseconds: 50));
      }
    }
    clearSelectedRooms();
    setState(() {
      isSending = false;
    });
  }

  void navigateToRooms() {
    if (selectedShareRoomIds.length == 1) {
      context.go('/rooms/${selectedShareRoomIds.first}');
    } else {
      context.go('/rooms');
    }
  }

  void clearShareContent() {
    matrix.shareContent = null;
  }

  void clearSelectedRooms() {
    selectedShareRoomIds.clear();
    selectedShareRoomNames.clear();
  }

  Future<void> sendFiles() async {
    final client = matrix.client;
    final shareContent = matrix.shareContent;
    Logs().i(shareContent.toString());

    if (shareContent == null || shareContent.isEmpty) {
      return;
    }

    final List<MatrixFile> shareFiles = [];
    final List<Map<String, dynamic>> shareEvents = [];

    sortShareContent(shareContent, shareFiles, shareEvents);

    if (shareFiles.isNotEmpty) {
      await handleShareFiles(shareFiles, context);
    }

    if (shareEvents.isNotEmpty) {
      await handleShareEvents(shareEvents, client);
    }

    clearShareContent();
  }

  Future<void> toggleFavouriteRoom({String? roomId}) async {
    if (roomId != null && selectedRoomIds.isEmpty) {
      selectedRoomIds.contains(roomId) ? selectedRoomIds.remove(roomId) : selectedRoomIds.add(roomId);
    }
    await showFutureLoadingDialog(
      context: context,
      future: () async {
        final makeFavorite = anySelectedRoomNotFavorite;
        final client = matrix.client;
        for (final roomId in selectedRoomIds) {
          final room = client.getRoomById(roomId)!;
          if (room.isFavourite == makeFavorite) continue;
          await client.getRoomById(roomId)!.setFavourite(makeFavorite);
        }
      },
    );
    cancelAction();
  }

  Future<void> toggleMuted({String? roomId}) async {
    if (roomId != null && selectedRoomIds.isEmpty) {
      selectedRoomIds.contains(roomId) ? selectedRoomIds.remove(roomId) : selectedRoomIds.add(roomId);
    }
    await showFutureLoadingDialog(
      context: context,
      future: () async {
        final newState = anySelectedRoomNotMuted ? PushRuleState.mentionsOnly : PushRuleState.notify;
        final client = matrix.client;
        for (final roomId in selectedRoomIds) {
          final room = client.getRoomById(roomId)!;
          if (room.pushRuleState == newState) continue;
          await client.getRoomById(roomId)!.setPushRuleState(newState);
        }
      },
    );
    cancelAction();
  }

  Future<void> archiveAction({String? roomId}) async {
    if (roomId != null && selectedRoomIds.isEmpty) {
      toggleSelection(roomId);
    }
    final confirmed = await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.areYouSure,
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.ok;
    if (!confirmed) return;
    await showFutureLoadingDialog(
      context: context,
      future: () => _archiveSelectedRooms(),
    );
    setState(() {});
  }

  void setStatus() async {
    final input = await showTextInputDialog(
        useRootNavigator: false,
        context: context,
        title: L10n.of(context)!.setStatus,
        okLabel: L10n.of(context)!.ok,
        cancelLabel: L10n.of(context)!.cancel,
        textFields: [
          DialogTextField(
            hintText: L10n.of(context)!.statusExampleMessage,
          ),
        ]);
    if (input == null) return;
    await showFutureLoadingDialog(
      context: context,
      future: () => matrix.client.setPresence(
        matrix.client.userID!,
        PresenceType.online,
        statusMsg: input.single,
      ),
    );
  }

  Future<void> _archiveSelectedRooms() async {
    final client = matrix.client;
    while (selectedRoomIds.isNotEmpty) {
      final roomId = selectedRoomIds.first;
      try {
        await client.getRoomById(roomId)!.leave();
      } finally {
        toggleSelection(roomId);
      }
    }
  }

  Future<void> addToSpace({String? roomId}) async {
    final Client client = matrix.client;
    if (roomId != null && selectedRoomIds.isEmpty) {
      selectedRoomIds.contains(roomId) ? selectedRoomIds.remove(roomId) : selectedRoomIds.add(roomId);
    }
    final selectedSpace = await showConfirmationDialog<String>(
        context: context,
        title: L10n.of(context)!.addToSpace,
        message: L10n.of(context)!.addToSpaceDescription,
        fullyCapitalizedForMaterial: false,
        actions: matrix.client.rooms
            .where((r) => r.isSpace)
            .map(
              (space) => AlertDialogAction(
                key: space.id,
                label: space.getLocalizedDisplayname(),
              ),
            )
            .toList());
    if (selectedSpace == null) {
      selectedRoomIds.clear();
      return;
    }
    if (!mounted) return;
    final result = await showFutureLoadingDialog(
      context: context,
      onError: (exception) {
        if (exception is MatrixException){
          return (exception).errorMessage;
        }
        else{
          return exception.toString();
        }
      },
      future: () async {
        final space = client.getRoomById(selectedSpace)!;
        final userPowerLevel = space.ownPowerLevel;
        // Get the required power level to add a room to the space
        final requiredPowerLevel = space.powerForChangingStateEvent('m.room.child');
        Logs().i('requiredPowerLevel: $requiredPowerLevel, userPowerLevel: $userPowerLevel');
        if (userPowerLevel >= requiredPowerLevel) {
          if (space.canSendDefaultStates) {
            for (final roomId in selectedRoomIds) {
              await space.setSpaceChild(roomId);
            }
          }
        } else {
          throw 'Insufficient space permissions';
        }
      },
    );
    Logs().i('result: ${result.result}, error: ${result.error}');
    if (result.error == null) {
      if (!mounted) return;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            L10n.of(context)!.chatHasBeenAddedToThisSpace,
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    }
    setState(() => selectedRoomIds.clear());
  }

  bool get anySelectedRoomNotMarkedUnread => selectedRoomIds.any((roomId) => !matrix.client.getRoomById(roomId)!.markedUnread);

  bool selectedRoomNotMarkedUnread(String roomId) {
    return !matrix.client.getRoomById(roomId)!.markedUnread;
  }

  bool get anySelectedRoomNotFavorite => selectedRoomIds.any((roomId) => !matrix.client.getRoomById(roomId)!.isFavourite);

  bool selectedRoomNotFavorite(String roomId) {
    return !matrix.client.getRoomById(roomId)!.isFavourite;
  }

  bool get anySelectedRoomNotMuted => selectedRoomIds.any((roomId) => matrix.client.getRoomById(roomId)!.pushRuleState == PushRuleState.notify);

  bool selectedRoomNotMuted(String roomId) {
    return matrix.client.getRoomById(roomId)!.pushRuleState == PushRuleState.notify;
  }

  bool waitForFirstSync = false;

  Future<void> _waitForFirstSync() async {
    final client = matrix.client;
    await client.roomsLoading;
    await client.accountDataLoading;
    if (client.prevBatch == null) {
      await client.onSync.stream.first;

      // Display first login bootstrap if enabled
      if (client.encryption?.keyManager.enabled == true) {
        if (await client.encryption?.keyManager.isCached() == false || await client.encryption?.crossSigning.isCached() == false || client.isUnknownSession && !mounted) {
          if (!mounted) return;
          await BootstrapDialog(client: client).show(context);
        }
      }
    }
    if (!mounted) return;
    setState(() {
      waitForFirstSync = true;
    });
  }

  void cancelAction() {
    Logs().i('pressed');
    if (BlocProvider.of<FileHandlerBloc>(context, listen: false).state.isInitiated) {
      setState(() {
        BlocProvider.of<FileHandlerBloc>(context, listen: false).add(const FileCompletionRequested());
        selectedShareRoomIds.clear();
        selectedShareRoomNames.clear();
        matrix.shareContent = null;
      });
    } else {
      setState(() => selectedRoomIds.clear());
    }
  }

  Future<void> setActiveClient(Client client) async {
    Logs().i('setActiveClient: ${client.homeserver.toString()}');
    context.go('/rooms');
    setState(() {
      _activeSpacesEntry = null;
/*      snappingSheetController = SnappingSheetController();
      snappingSheetScrollContentController = ScrollController();*/
      selectedRoomIds.clear();
      matrix.setActiveClient(client);
      //PushSetup().pushSetupClientOnly(client);
    });
    _clientStream.add(client);
  }

  void setActiveBundle(String bundle) {
    Logs().i('setActiveBundle: ${bundle.toString()}');
    context.go('/rooms');
    setState(() {
      _activeSpacesEntry = null;
      selectedRoomIds.clear();
      matrix.activeBundle = bundle;
      if (!matrix.currentBundle!.any((client) => client == matrix.client)) {
        matrix.setActiveClient(matrix.currentBundle!.first);
      }
    });
  }

  void editBundlesForAccount(String? userId, String? activeBundle) async {
    final l10n = L10n.of(context)!;
    final client = matrix.widget.clients[matrix.getClientIndexByMatrixId(userId!)];
    final action = await showConfirmationDialog<EditBundleAction>(
      context: context,
      title: L10n.of(context)!.editBundlesForAccount,
      actions: [
        AlertDialogAction(
          key: EditBundleAction.addToBundle,
          label: L10n.of(context)!.addToBundle,
        ),
        if (activeBundle != client.userID)
          AlertDialogAction(
            key: EditBundleAction.removeFromBundle,
            label: L10n.of(context)!.removeFromBundle,
          ),
      ],
    );
    if (action == null) return;
    switch (action) {
      case EditBundleAction.addToBundle:
        if (!mounted) return;
        final bundle = await showTextInputDialog(context: context, title: l10n.bundleName, textFields: [DialogTextField(hintText: l10n.bundleName)]);
        if (bundle == null || bundle.isEmpty || bundle.single.isEmpty) return;
        if (!mounted) return;
        await showFutureLoadingDialog(
          context: context,
          future: () => client.setAccountBundle(bundle.single),
        );
        break;
      case EditBundleAction.removeFromBundle:
        if (!mounted) return;
        await showFutureLoadingDialog(
          context: context,
          future: () => client.removeFromAccountBundle(activeBundle!),
        );
    }
  }

  bool get displayBundles => matrix.hasComplexBundles && matrix.accountBundles.keys.length > 1;

  String? get secureActiveBundle {
    if (matrix.activeBundle == null || !matrix.accountBundles.keys.contains(matrix.activeBundle)) {
      return matrix.accountBundles.keys.first;
    }
    return matrix.activeBundle;
  }

  void resetActiveBundle() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        matrix.activeBundle = null;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    matrix.navigatorContext = context;
    if (!PageMeThemes.isColumnMode(context)) {
      isDrawerExpanded = true;
    }
    return SendingProgressNotificationHandler(
      child: ChatListControllerProvider(
        chatListController: this,
        child: ChatListView(this),
      ),
    );
  }

  void _hackyWebRTCFixForWeb() {
    ChatList.contextForVoip = context;
  }

  Future<void> _checkTorBrowser() async {
    if (!kIsWeb) return;
    final isTor = await TorBrowserDetector.isTorBrowser;
    isTorBrowser = isTor;
  }

  Future<void> dehydrate() => SettingsAccountController.dehydrateDevice(context);

  @override
  bool get wantKeepAlive => PageMeApp.router.routeInformationProvider.value.uri.path.startsWith("/settings");
}

enum EditBundleAction { addToBundle, removeFromBundle }
