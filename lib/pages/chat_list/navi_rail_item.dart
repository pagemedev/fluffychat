import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/chat_list/chat_list.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/string_color.dart';

import '../../config/themes.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../../widgets/context_menu/custom_context_menu.dart';

class NaviRailItem extends StatelessWidget {
  final String toolTip;
  final bool isSelected;
  final void Function() onTap;
  final Widget icon;
  final Widget? selectedIcon;
  final String? label;
  final Color? backgroundColor;
  final bool isExpanded;

  const NaviRailItem({
    required this.toolTip,
    required this.isSelected,
    required this.onTap,
    required this.icon,
    this.selectedIcon,
    Key? key,
    this.label,
    this.backgroundColor,
    this.isExpanded = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 64,
      child: Material(
        color: Colors.transparent,
        child: Stack(
          children: [
            Positioned(
              top: 16,
              bottom: 16,
              left: 0,
              child: AnimatedContainer(
                width: isSelected ? 4 : 0,
                duration: PageMeThemes.animationDuration,
                curve: PageMeThemes.animationCurve,
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.primary,
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(90),
                    bottomRight: Radius.circular(90),
                  ),
                ),
              ),
            ),
            InkWell(
              splashColor: Theme.of(context).colorScheme.primary.withAlpha(100),
              highlightColor: Theme.of(context).colorScheme.primary.withAlpha(100),
              onTap: onTap,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  const SizedBox(
                    width: 12,
                  ),
                  Center(
                    child: IconButton(
                      iconSize: 28,
                      onPressed: onTap,
                      tooltip: toolTip,
                      style: IconButton.styleFrom(
                        backgroundColor: isSelected
                            ? PageMeThemes.isDarkMode(context)
                                ? Theme.of(context).colorScheme.background.lighten()
                                : Theme.of(context).colorScheme.background.darken(20)
                            : null,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(FlavorConfig.borderRadius)),
                      ),
                      icon: Material(
                        elevation: 3,
                        shadowColor: Colors.black,
                        borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
                        color: isSelected ? Theme.of(context).colorScheme.primaryContainer : backgroundColor ?? Theme.of(context).colorScheme.background,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 8.0,
                            vertical: 8.0,
                          ),
                          child: isSelected ? selectedIcon ?? icon : icon,
                        ),
                      ),
                    ),
                  ),
                  if (isExpanded)
                    if (label != null) Text(label!)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class NaviRailSpaceItem extends StatelessWidget {
  final String toolTip;
  final bool isSelected;
  final void Function() onTap;
  final Widget icon;
  final Widget? selectedIcon;
  final Widget? label;
  final Color? backgroundColor;
  final bool isExpanded;
  final Room room;

  const NaviRailSpaceItem({
    required this.toolTip,
    required this.isSelected,
    required this.onTap,
    required this.icon,
    required this.label,
    this.selectedIcon,
    Key? key,
    this.backgroundColor,
    this.isExpanded = false,
    required this.room,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ChatListController? controller = ChatList.of(context);
    final TextStyle contextMenuTextStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w500,
      color: Theme.of(context).colorScheme.onBackground,
    );
    final Color contextMenuIconColor = Theme.of(context).colorScheme.onBackground;
    const double contextMenuIconSize = 20;
    if (PlatformInfos.isMobile) {
      return SizedBox(
        height: 64,
        child: Stack(
          children: [
            Positioned(
              top: 16,
              bottom: 16,
              left: 0,
              child: AnimatedContainer(
                width: isSelected ? 4 : 0,
                duration: PageMeThemes.animationDuration,
                curve: PageMeThemes.animationCurve,
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.primary,
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(90),
                    bottomRight: Radius.circular(90),
                  ),
                ),
              ),
            ),
            InkWell(
              splashColor: Theme.of(context).colorScheme.primary.withAlpha(100),
              highlightColor: Theme.of(context).colorScheme.primary.withAlpha(100),
              onTap: onTap,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  const SizedBox(
                    width: 12,
                  ),
                  Center(
                    child: IconButton(
                      onPressed: onTap,
                      tooltip: toolTip,
                      isSelected: isSelected,
                      style: IconButton.styleFrom(
                        backgroundColor: isSelected
                            ? PageMeThemes.isDarkMode(context)
                                ? Theme.of(context).colorScheme.background.lighten()
                                : Theme.of(context).colorScheme.background.darken(20)
                            : null,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(FlavorConfig.borderRadius)),
                      ),
                      selectedIcon: selectedIcon,
                      icon: icon,
                    ),
                  ),
                  if (isExpanded && label != null) label!
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return CustomContextMenuArea(
        width: 200,
        builder: (c) {
          return _buildContextMenuItems(contextMenuTextStyle, context, contextMenuIconSize, c, contextMenuIconColor, controller);
        },
        child: SizedBox(
          height: 64,
          child: Stack(
            children: [
              Positioned(
                top: 16,
                bottom: 16,
                left: 0,
                child: AnimatedContainer(
                  width: isSelected ? 4 : 0,
                  duration: PageMeThemes.animationDuration,
                  curve: PageMeThemes.animationCurve,
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.primary,
                    borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(90),
                      bottomRight: Radius.circular(90),
                    ),
                  ),
                ),
              ),
              InkWell(
                splashColor: Theme.of(context).colorScheme.primary.withAlpha(100),
                highlightColor: Theme.of(context).colorScheme.primary.withAlpha(100),
                onTap: onTap,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    const SizedBox(
                      width: 12,
                    ),
                    Center(
                      child: IconButton(
                        onPressed: onTap,
                        tooltip: toolTip,
                        isSelected: isSelected,
                        style: IconButton.styleFrom(
                          backgroundColor: isSelected
                              ? PageMeThemes.isDarkMode(context)
                                  ? Theme.of(context).colorScheme.background.lighten()
                                  : Theme.of(context).colorScheme.background.darken(20)
                              : null,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(FlavorConfig.borderRadius)),
                        ),
                        selectedIcon: selectedIcon,
                        icon: icon,
                      ),
                    ),
                    if (isExpanded && label != null) label!
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  List<Widget> _buildContextMenuItems(
      TextStyle contextMenuTextStyle, BuildContext context, double contextMenuIconSize, BuildContext c, Color contextMenuIconColor, ChatListController? controller) {
    if (controller == null) {
      return [
        ListTile(
            title: Text(
              'No Actions',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).colorScheme.onBackground),
            ),
            dense: true)
      ];
    }
    return [
      Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            dense: true,
            title: Text('Space Details', style: contextMenuTextStyle.copyWith(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.w600)),
            trailing: Icon(
              Icons.workspaces_outlined,
              size: contextMenuIconSize,
              color: Theme.of(context).colorScheme.primary,
            ),
            onTap: () async {
              Navigator.of(c).pop();
              context.go('/rooms/${room.id}/large_details');
            },
          ),
          const Divider(
            height: 2,
            endIndent: 8,
            indent: 8,
          ),
          ListTile(
            dense: true,
            title: Text('Invite People', style: contextMenuTextStyle),
            trailing: Icon(
              Icons.person_add_outlined,
              size: contextMenuIconSize,
              color: contextMenuIconColor,
            ),
            onTap: () async {
              Navigator.of(c).pop();
              context.go('/rooms/${room.id}/invite');
            },
          ),
          const Divider(
            height: 2,
            endIndent: 8,
            indent: 8,
          ),
          ListTile(
            dense: true,
            title: Text('Leave Space', style: contextMenuTextStyle.copyWith(color: Colors.redAccent, fontWeight: FontWeight.w600)),
            trailing: Icon(
              Icons.exit_to_app_outlined,
              size: contextMenuIconSize,
              color: Colors.redAccent,
            ),
            onTap: () async {
              if (PlatformInfos.isWeb) {
                BrowserContextMenu.enableContextMenu();
              }
              Navigator.of(c).pop();
              final confirmed = await showOkCancelAlertDialog(
                useRootNavigator: false,
                context: context,
                title: L10n.of(context)!.areYouSure,
                okLabel: L10n.of(context)!.ok,
                cancelLabel: L10n.of(context)!.cancel,
              );
              if (confirmed == OkCancelResult.ok) {
                final success = await showFutureLoadingDialog(context: context, future: () => room.leave());
                if (success.error == null) {
                  context.go('/rooms');
                }
              }
            },
          )
        ],
      )
    ];
  }
}
