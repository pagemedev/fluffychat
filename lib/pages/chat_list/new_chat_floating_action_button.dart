import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/config/themes.dart';


import '../../config/flavor_config.dart';
import 'chat_list.dart';


class StartChatFloatingActionButton extends StatelessWidget {
  final ChatListController controller;

  const StartChatFloatingActionButton({Key? key, required this.controller}) : super(key: key);

  void _onPressed(BuildContext context) {
    context.go('/rooms/newchat');
  }

  Widget icon(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 2, left: 0),
      child: SvgPicture.asset(
        'assets/pageme_chat_bubble.svg',
        width: 20,
        height: 20,
        color: isBusiness()
            ? PageMeThemes.isDarkMode(context)
                ? Theme.of(context).colorScheme.onPrimaryContainer
                : Theme.of(context).colorScheme.onPrimary
            : PageMeThemes.isDarkMode(context)
                ? Theme.of(context).colorScheme.onPrimaryContainer
                : Theme.of(context).colorScheme.onPrimary,
      ),
    );
  }

  String getLabel(BuildContext context) {
    return L10n.of(context)!.newChat;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      curve: Curves.easeInOut,
      width: controller.scrolledToTop ? 144 : 64,
      child: controller.scrolledToTop
          ? FloatingActionButton.extended(
              onPressed: () => _onPressed(context),
              icon: icon(context),
              heroTag: 'FAB',
              label: Text(
                getLabel(context),
                overflow: TextOverflow.fade,
              ),
            )
          : FloatingActionButton(
              onPressed: () => _onPressed(context),
              heroTag: 'FAB',
              child: icon(context),
            ),
    );
  }
}
