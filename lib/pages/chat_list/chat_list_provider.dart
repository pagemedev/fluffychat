import 'package:flutter/material.dart';

import 'chat_list.dart';

class ChatListControllerProvider extends InheritedWidget {
  final ChatListController chatListController;

  const ChatListControllerProvider({Key? key, required this.chatListController, required Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(ChatListControllerProvider oldWidget) {
    return chatListController != oldWidget.chatListController;
  }

  static ChatListController? of(BuildContext context) {
    final ChatListControllerProvider? result = context.dependOnInheritedWidgetOfExactType<ChatListControllerProvider>();
    return result?.chatListController;
  }
}