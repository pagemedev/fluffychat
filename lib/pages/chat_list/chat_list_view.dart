import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/pages/chat_list/chat_list_appbar.dart';
import 'package:pageMe/pages/spaces_drawer/spaces_drawer.dart';
import 'package:pageMe/widgets/file_upload_helper.dart';

import 'package:pageMe/pages/chat_list/chat_list.dart';
import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../../utils/conditonal_wrapper.dart';
import '../../utils/global_key_provider.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/matrix.dart';
import '../file_sharing/file_sharing.dart';
import 'chat_list_body.dart';
import '../empty_rooms/empty_rooms_floating_action_button.dart';
import 'new_chat_floating_action_button.dart';

class ChatListView extends StatelessWidget {
  final ChatListController controller;

  const ChatListView(this.controller, {Key? key}) : super(key: key);

  /// Handles a system-level "back" action based on the current state of the application.
  ///
  /// This method checks the current selection mode, active filter, drawer state, and search mode,
  /// and performs appropriate actions based on these states. It may cancel a selection action,
  /// change the active filter, close the navigation drawer, or cancel a search. If any of these
  /// actions are performed, it also instructs the Redirector to stop any ongoing redirection.
  ///
  /// @param redirector The Redirector responsible for handling redirections.
  /// @return A Future that completes when the operation is finished.
  Future<bool> handleSystemPop() async {
    if (handleSelectMode()) return false;
    if (handleActiveFilter()) return false;
    if (handleDrawerState()) return false;
    handleSearchMode();
    return true;
  }

  bool handleSelectMode() {
    final selMode = controller.selectMode;
    if (selMode != SelectMode.normal) {
      controller.cancelAction();
      return true;
    }
    return false;
  }

  bool handleActiveFilter() {
    if (controller.activeFilter != (FlavorConfig.separateChatTypes ? ActiveFilter.messages : ActiveFilter.allChats)) {
      controller.onDestinationSelected(FlavorConfig.separateChatTypes ? 1 : 0);
      return true;
    }
    return false;
  }

  bool handleDrawerState() {
    if (controller.scaffoldKey.currentState?.isDrawerOpen ?? false) {
      controller.scaffoldKey.currentState!.closeDrawer();
      return true;
    }
    return false;
  }

  void handleSearchMode() {
    if (controller.isSearchBarOpen) {
      controller.cancelSearch(unfocus: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    Logs().v("Current path: ${PageMeApp.router.routeInformationProvider.value.uri.path}");
    final client = Matrix.of(context).client;
    const double innerPadding = 8;
    return StreamBuilder<Object?>(
      stream: Matrix.of(context).onShareContentChanged.stream,
      builder: (_, __) {
        final selectMode = controller.selectMode;
        return WillPopScope(
          onWillPop: () async => handleSystemPop(),
          child: Container(
            color: PageMeThemes.isDarkMode(context) ? Colors.black : const Color(0xFF818ba1),
            child: Row(
              children: [
                if (PageMeThemes.isColumnMode(context) && PageMeThemes.getDisplayNavigationRail(context)) ...[
                  Padding(
                    padding: const EdgeInsets.only(right: innerPadding),
                    child: SpacesDrawer(
                      chatListController: controller,
                    ),
                  ),
                ],
                Expanded(
                  child: GestureDetector(
                    onTap: FocusManager.instance.primaryFocus?.unfocus,
                    excludeFromSemantics: true,
                    behavior: HitTestBehavior.translucent,
                    child: ClipRRect(
                      borderRadius: (PageMeThemes.isColumnMode(context)) ? BorderRadius.circular(FlavorConfig.borderRadius) : BorderRadius.zero,
                      child: Scaffold(
                        drawerEnableOpenDragGesture: PageMeThemes.isColumnMode(context) ? false : true,
                        key: controller.scaffoldKey,
                        drawer: SafeArea(
                          child: ClipRRect(
                            borderRadius: const BorderRadius.only(topRight: Radius.circular(20), bottomRight: Radius.circular(20)),
                            child: Drawer(
                              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                              elevation: 3,
                              width: controller.isDrawerExpanded ? 200 : 88,
                              child: SpacesDrawer(chatListController: controller),
                            ),
                          ),
                        ),
                        appBar: ChatListAppBar(
                          selectMode: selectMode,
                          context: context,
                          controller: controller,
                        ),

                        bottomNavigationBar:
                            (PageMeApp.router.routeInformationProvider.value.uri.path != '/rooms') ? const SizedBox.shrink() : FileSharing(controller: controller),
                        body: LayoutBuilder(
                          builder: (context, size) {
                            return Column(
                              children: [
                                AnimatedContainer(
                                  height: controller.showChatBackupBanner ? 54 : 0,
                                  duration: const Duration(milliseconds: 300),
                                  clipBehavior: Clip.hardEdge,
                                  curve: Curves.bounceInOut,
                                  decoration: const BoxDecoration(),
                                  child: Material(
                                    color: Theme.of(context).colorScheme.primaryContainer,
                                    child: ListTile(
                                      leading: Image.asset(
                                        'assets/backup.png',
                                        fit: BoxFit.contain,
                                        width: 44,
                                      ),
                                      title: Text(
                                        L10n.of(context)!.enableAutoBackups,
                                        style: TextStyle(color: Theme.of(context).colorScheme.onPrimaryContainer),
                                      ),
                                      trailing: Icon(
                                        Icons.chevron_right_outlined,
                                        color: Theme.of(context).colorScheme.onPrimaryContainer,
                                      ),
                                      onTap: controller.firstRunBootstrapAction,
                                    ),
                                  ),
                                ),
                                //FileUploadHeader(statusUpdates: controller.chatService.statusUpdates),
                                Expanded(child: ChatListViewBody(controller)),
                              ],
                            );
                          },
                        ),
                        floatingActionButton: controller.filteredRooms.isNotEmpty
                            ? StartChatFloatingActionButtonHandler(controller: controller, context: context, selectMode: selectMode)
                            : null,
                        // bottomNavigationBar: const ConnectionStatusHeader(),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class StartChatFloatingActionButtonHandler extends StatelessWidget {
  const StartChatFloatingActionButtonHandler({
    super.key,
    required this.controller,
    required this.context,
    required this.selectMode,
  });

  final ChatListController controller;
  final BuildContext context;
  final SelectMode selectMode;

  @override
  Widget build(BuildContext context) {
    // If selectMode is not normal, return null.
    if (selectMode != SelectMode.normal) return Container();

    // If in search mode, return null.
    if (controller.isSearchMode) return Container();

    // If no rooms are available, return null.
    if (controller.filteredRooms.isEmpty) return Container();

    // Otherwise, return a StartChatFloatingActionButton.
    return ConditionalKeyBoardShortcuts(
      enabled: !PlatformInfos.isMobile,
      keysToPress: {LogicalKeyboardKey.controlLeft, LogicalKeyboardKey.keyN},
      onKeysPressed: () => context.go('/newchat'),
      helpLabel: L10n.of(context)!.newChat,
      child: StartChatFloatingActionButton(
        key: GlobalKeyProvider.fabButton,
        controller: controller,
      ),
    );
  }
}
