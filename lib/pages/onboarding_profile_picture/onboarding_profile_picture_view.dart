import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:matrix/encryption.dart';
import 'package:pageMe/pages/onboarding_profile_picture/onboarding_profile_picture.dart';

import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../../widgets/content_banner.dart';
import '../../widgets/matrix.dart';

class OnBoardingProfilePictureHeading extends StatelessWidget {
  final OnBoardingProfilePictureController controller;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 16.0);

  static const titleStyle = TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0);
  const OnBoardingProfilePictureHeading({Key? key, required this.controller}) : super(key: key);

  Widget buildSVG(String assetName, [double width = 250, Color? color]) {
    return SafeArea(
      child: SvgPicture.asset('assets/$assetName', width: width, color: color),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 0, left: 12, right: 12),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                text: 'Make it more ',
                style: titleStyle.copyWith(color: PageMeThemes.whiteBlackColor(context)),
                children: <TextSpan>[
                  TextSpan(text: 'you!', style: titleStyle.copyWith(color: Theme.of(context).colorScheme.primary)),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5, left: 12, right: 12, bottom: globalHeaderHeight),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              "Lets make your profile more unique and add a profile picture.",
              textAlign: TextAlign.center,
              style: titleStyle.copyWith(fontSize: 18, fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ],
    );
  }
}

class OnBoardingProfilePictureView extends StatelessWidget {
  final OnBoardingProfilePictureController controller;
  static const double assetWidth = 350;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 19.0);
  static const companyStyle = TextStyle(fontSize: 22.0);
  static const titleStyle = TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0);

  const OnBoardingProfilePictureView(this.controller, {Key? key}) : super(key: key);

  Widget buildImage(String assetName, [double? width = assetWidth, Color? color]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight),
        child: Image.asset('assets/$assetName', width: width, color: color),
      ),
    );
  }

  Widget buildSVG(String assetName, [double width = assetWidth, Color? color]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight),
        child: SvgPicture.asset('assets/$assetName', width: width, color: color),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: Theme.of(context).colorScheme.background,
            elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
            toolbarHeight: isBusiness() ? 90 : null,
            centerTitle: true,

            title: Column(
              children: [
                SizedBox(
                  height: globalHeaderHeight,
                  width: 150,
                  child: SvgPicture.asset(
                    (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                    fit: BoxFit.fitHeight,
                  ),
                ),
                if (isBusiness())
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "${controller.homeServerName}",
                      style: companyStyle.copyWith(color: Theme.of(context).colorScheme.primary),
                    ),
                  )
              ],
            ),
            bottom: PageMeThemes.isColumnMode(context)
                ? PreferredSize(
                    preferredSize: const Size.fromHeight(2.0),
                    child: Container(
                        height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
                        color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black //Theme.of(context).colorScheme.primary,
                        ),
                  )
                : null),
        body: Stack(
          children: [
            if (PageMeThemes.isColumnMode(context))
              Image.asset(
                Theme.of(context).brightness == Brightness.light ? 'assets/background_light.png' : 'assets/background_dark.png',
                width: double.infinity,
                height: double.infinity,
                fit: BoxFit.cover,
              ),
            Padding(
              padding: PageMeThemes.isColumnMode(context) ? const EdgeInsets.all(8.0) : const EdgeInsets.all(0),
              child: Center(
                child: Material(
                  elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
                  borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                  child: Container(
                    width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(32) : null,
                      color: Theme.of(context).colorScheme.background,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                OnBoardingProfilePictureHeading(controller: controller),
                                Stack(
                                  clipBehavior: Clip.none,
                                  children: [
                                    GestureDetector(
                                      onTapDown:(_)=>  controller.setAvatarAction(),
                                      child: Container(
                                        width: 150,
                                        height: 150,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(20),
                                          shape: BoxShape.rectangle,
                                          border: Border.all(
                                            color: Theme.of(context).colorScheme.secondary,
                                            width: 2,
                                          ),
                                        ),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(18),
                                          child: ContentBanner(
                                            mxContent: controller.profile?.avatarUrl,
                                            defaultIcon: Icons.person_outline_outlined,
                                            height: 300,
                                            enableViewer: true,
                                            heroTag: 'account-header',
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: -10,
                                      right: -10,
                                      child: FloatingActionButton(
                                        mini: true,
                                        onPressed: controller.setAvatarAction,
                                        backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                                        foregroundColor: Theme.of(context).colorScheme.onPrimaryContainer,
                                        child: const Icon(Icons.camera_alt_outlined),
                                      ),
                                    )
                                  ],
                                ),
                                buildSVG('camera.svg', 90, Theme.of(context).colorScheme.secondaryContainer),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
                          child: SizedBox(
                            width: double.infinity,
                            height: 60,
                            child: ElevatedButton(
                                style: ButtonStyle(
                                  elevation: MaterialStateProperty.resolveWith(
                                    (states) {
                                      return null;
                                    },
                                  ),
                                  backgroundColor: MaterialStateProperty.resolveWith(
                                    (states) {
                                      return Theme.of(context).colorScheme.primaryContainer;
                                    },
                                  ),
                                ),
                                onPressed: () async {
                                  controller.nextPage();
                                },
                                child: Text(
                                  "Next",
                                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Theme.of(context).colorScheme.onPrimaryContainer),
                                )),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
