import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:image_picker/image_picker.dart';

import 'package:matrix/matrix.dart';


import 'package:pageMe/widgets/matrix.dart';

import '../../utils/famedlysdk_store.dart';
import '../../utils/platform_infos.dart';
import '../settings/settings.dart';
import 'onboarding_profile_picture_view.dart';

class OnBoardingProfilePicture extends StatefulWidget {
  const OnBoardingProfilePicture({Key? key}) : super(key: key);

  @override
  OnBoardingProfilePictureController createState() => OnBoardingProfilePictureController();
}

class OnBoardingProfilePictureController extends State<OnBoardingProfilePicture> {
  final TextEditingController nameController = TextEditingController();
  Future<dynamic>? profileFuture;
  Profile? profile;
  bool profileUpdated = false;
  String? homeServerName;
  int? pageNumber = 0;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const double assetWidth = 350;
  bool showBootStrap = false;

  Store? _store;
  Store get store => _store ??= Store();

  void updateProfile() => setState(() {
        profileUpdated = true;
        profile = profileFuture = null;
      });

  void setAvatarAction() async {
    Logs().v("[OnBoardingProfilePicture] - setAvatarAction");
    final actions = [
      if (PlatformInfos.isMobile)
        SheetAction(
          key: AvatarAction.camera,
          label: L10n.of(context)!.openCamera,
          isDefaultAction: true,
          icon: Icons.camera_alt_outlined,
        ),
      SheetAction(
        key: AvatarAction.file,
        label: L10n.of(context)!.openGallery,
        icon: Icons.photo_outlined,
      ),
      if (profile?.avatarUrl != null)
        SheetAction(
          key: AvatarAction.remove,
          label: L10n.of(context)!.removeYourAvatar,
          isDestructiveAction: true,
          icon: Icons.delete_outlined,
        ),
    ];
    final action = actions.length == 1
        ? actions.single.key
        : await showModalActionSheet<AvatarAction>(
            context: context,
            title: L10n.of(context)!.changeYourAvatar,
            actions: actions,
          );
    if (action == null) return;
    final matrix = Matrix.of(context);
    if (action == AvatarAction.remove) {
      final success = await showFutureLoadingDialog(
        context: context,
        title: "Removing your avatar...",
        future: () => matrix.client.setAvatar(null),
      );
      if (success.error == null) {
        updateProfile();
      }
      return;
    }
    MatrixFile file;

    final result = await ImagePicker().pickImage(
      source: action == AvatarAction.camera ? ImageSource.camera : ImageSource.gallery,
      imageQuality: 50,
    );
    if (result == null) return;
    final bytes = await showFutureLoadingDialog(
      context: context,
      future: () => result.readAsBytes(),
        title: "Loading image...",
      onError: (error) => 'Error reading file'
    );

    file = MatrixFile(
      bytes: bytes.result!,
      name: result.path,
    );

    final success = await showFutureLoadingDialog(
      context: context,
      future: () => matrix.client.setAvatar(file),
        title: "Uploading your avatar...",
        onError: (error) => 'Error setting avatar'
    );
    if (success.error == null) {
      updateProfile();
    }
  }

  void nextPage() {
    setState(() {
      context.go('/onboard/onboarding_profile_picture/onboarding_bootstrap');
    });
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    profileFuture ??= client
        .getProfileFromUserId(
      client.userID!,
      cache: !profileUpdated,
      getFromRooms: !profileUpdated,
    )
        .then((p) {
      if (mounted) setState(() => profile = p);
      return p;
    });
    homeServerName = Matrix.of(context).client.homeserver.toString().replaceFirst('https://matrix.', '');
    homeServerName = homeServerName?.replaceFirst('.pageme.co.za', '').toUpperCase();

    return OnBoardingProfilePictureView(this);
  }
}
