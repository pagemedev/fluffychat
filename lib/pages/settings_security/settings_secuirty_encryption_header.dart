import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/keys/pageme_client_appkey.dart';
import 'package:pageMe/pages/settings_security/settings_security.dart';
import 'package:pageMe/utils/beautify_string_extension.dart';
import 'package:pageme_client/pageme_client.dart';
import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../settings/settings_container.dart';
import '../settings/settings_section_title.dart';
import '../../utils/date_time_extension.dart';

class SettingsEncryptionHeaderSection extends StatelessWidget {
  final SettingsSecurityController controller;
  const SettingsEncryptionHeaderSection({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final listTileBackgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final Color backgroundColor = listTileBackgroundColor;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;

    final listTileTextStyle = TextStyle(
      color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface,
      fontSize: 16,
      //fontWeight: FontWeight.w500,
    );
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const SettingsSectionTitle(title: 'Encryption status and management'),
        FutureBuilder(
          future: () async {
            return (await controller.matrixState.client.encryption!.keyManager.isCached()) && (await controller.matrixState.client.encryption!.crossSigning.isCached());
          }(),
          builder: (context, snapshot) {
            Logs().v('Snapshot state: ${snapshot.connectionState}'); // debug
            Logs().v('Snapshot data: ${snapshot.data}'); // debug
            return (!controller.matrixState.client.encryption!.crossSigning.enabled ||
                    !controller.matrixState.client.encryption!.keyManager.enabled ||
                    controller.matrixState.client.isUnknownSession ||
                    snapshot.data == false)
                ? SettingsContainer(
                    context: context,
                    iconData: Icons.auto_fix_high_outlined,
                    title: 'Enable full encryption and verification',
                    backgroundColor: backgroundColor,
                    contentColor: Colors.orange,
                    onTap: () async => controller.showBootstrapDialog(context),
                  )
                : Container();
          },
        ),
        Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
          child: Material(
            borderRadius: BorderRadius.circular(12),
            color: listTileBackgroundColor,
            elevation: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: CupertinoListSection.insetGrouped(
                margin: EdgeInsets.zero,
                backgroundColor: listTileBackgroundColor,
                hasLeading: true,
                children: [
                  CupertinoListTile(
                    backgroundColor: listTileBackgroundColor,
                    title: Text(L10n.of(context)!.crossSigningEnabled, style: listTileTextStyle),
                    trailing: (controller.matrixState.client.encryption!.crossSigning.enabled)
                        ? const Icon(Icons.check_circle, color: Colors.green)
                        : const Icon(Icons.error, color: Colors.red),
                  ),
                  CupertinoListTile(
                    backgroundColor: listTileBackgroundColor,
                    title: Text(L10n.of(context)!.onlineKeyBackupEnabled, style: listTileTextStyle),
                    trailing: (controller.matrixState.client.encryption!.keyManager.enabled)
                        ? const Icon(Icons.check_circle, color: Colors.green)
                        : const Icon(Icons.error, color: Colors.red),
                  ),
                  CupertinoListTile(
                    backgroundColor: listTileBackgroundColor,
                    title: Text('Session verified', style: listTileTextStyle),
                    trailing:
                        (!controller.matrixState.client.isUnknownSession) ? const Icon(Icons.check_circle, color: Colors.green) : const Icon(Icons.error, color: Colors.red),
                  ),
                  FutureBuilder(
                    future: () async {
                      return (await controller.matrixState.client.encryption!.keyManager.isCached()) &&
                          (await controller.matrixState.client.encryption!.crossSigning.isCached());
                    }(),
                    builder: (context, snapshot) => CupertinoListTile(
                      backgroundColor: listTileBackgroundColor,
                      title: Text(
                        L10n.of(context)!.keysCached,
                        style: listTileTextStyle,
                      ),
                      trailing: (snapshot.data == true) ? const Icon(Icons.check_circle, color: Colors.green) : const Icon(Icons.error, color: Colors.red),
                    ),
                  ),
                  if (isBusiness())
                  FutureBuilder<EncryptionEntryResult>(
                    future: controller.matrixState.pagemeClient.user.isEncryptionKeyStored(userId: controller.matrixState.client.userID!, appKey: PageMeClientAppKey.appKey),
                    builder: (context, snapshot) => CupertinoListTile(
                      backgroundColor: listTileBackgroundColor,
                      title: Text(
                        "Recovery key is backed up",
                        style: listTileTextStyle,
                      ),
                      subtitle: snapshot.hasData
                          ? snapshot.data!.isStored
                              ? Text("Date stored: ${snapshot.data!.dateStored?.localizedTimeShort(context)}")
                              : null
                          : null,
                      trailing: snapshot.hasData
                          ? (snapshot.data!.isStored)
                              ? const Icon(Icons.check_circle, color: Colors.green)
                              : const Icon(Icons.error, color: Colors.red)
                          : const SizedBox.square(dimension: 20, child: CircularProgressIndicator()),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        SettingsContainer(
          context: context,
          iconData: Icons.vpn_key_outlined,
          title: 'View your public key',
          backgroundColor: backgroundColor,
          contentColor: contentColor,
          onTap: () async => showOkAlertDialog(
            useRootNavigator: false,
            context: context,
            title: L10n.of(context)!.yourPublicKey,
            message: controller.matrixState.client.fingerprintKey.beautified,
            okLabel: L10n.of(context)!.ok,
          ),
        ),
        if (isBusiness())
        SettingsContainer(
          context: context,
          iconData: Icons.cloud_upload_outlined,
          title: 'Manually save a recovery key',
          backgroundColor: backgroundColor,
          contentColor: contentColor,
          onTap: () async {
            final result = (await showTextInputDialog(
              textFields: [
                DialogTextField(
                  minLines: 1,
                  maxLines: 4,
                  hintText: "Enter recovery key",
                  validator: (value) {
                    if (value == null) {
                      return "Input was empty";
                    }
                    if (value.isEmpty) {
                      return "Input was empty";
                    }
                    if (value.length < 59) {
                      return "${value.length}/59 too short";
                    }
                    if (value.length > 59) {
                      return "${value.length}/59 too long";
                    }
                    return null;
                  },
                ),
              ],
              context: context,
              title: "Save your ${L10n.of(context)!.recoveryKey}",
              message: "Enter a valid recovery key to be securely saved",
              okLabel: L10n.of(context)!.ok,
            ))
                ?.single;
            Logs().i(result.toString());
            if (result == null) return;
            if (result.isEmpty || result.length != 59) return;
            final String recoveryKey = result;
            Logs().v("recoveryKey length: ${recoveryKey.length}");
            final bool isValid = await controller.checkRecoveryKeyValidity(recoveryKey: recoveryKey);
            if (isValid) {
              await controller.sendEncryptedRecoveryKey(recoveryKey);
              if (controller.isRecoveryKeyStoredOnServer) {
                return showAlertDialog(
                  context: context,
                  title: "Successfully stored recovery key",
                  actions: [
                    AlertDialogAction(label: L10n.of(context)!.close, key: 'Close', isDefaultAction: true),
                  ],
                );
              } else {
                return showAlertDialog(
                  context: context,
                  title: "Error storing recovery key",
                  actions: [
                    AlertDialogAction(label: L10n.of(context)!.close, key: 'Close', isDefaultAction: true),
                  ],
                );
              }
            } else {
              return showAlertDialog(
                context: context,
                title: "Recovery entered was not valid",
                actions: [
                  AlertDialogAction(label: L10n.of(context)!.close, key: 'Close', isDefaultAction: true),
                ],
              );
            }
          },
        ),
      ],
    );
  }
}
