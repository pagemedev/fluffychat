import 'dart:typed_data';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/encryption/ssss.dart';
import 'package:matrix/matrix.dart';

import 'package:pageMe/config/setting_keys.dart';
import 'package:pageMe/widgets/app_lock.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:pointycastle/asymmetric/api.dart';
import '../../keys/pageme_client_appkey.dart';
import '../bootstrap/bootstrap_dialog.dart';
import 'settings_security_view.dart';

class SettingsSecurity extends StatefulWidget {
  const SettingsSecurity({Key? key}) : super(key: key);

  @override
  SettingsSecurityController createState() => SettingsSecurityController();
}

class SettingsSecurityController extends State<SettingsSecurity> {
  late MatrixState matrixState;
  bool isRecoveryKeyStoredOnServer = false;

  void changePasswordAccountAction() async {
    final input = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.changePassword,
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      textFields: [
        DialogTextField(
          hintText: L10n.of(context)!.pleaseEnterYourPassword,
          obscureText: true,
          minLines: 1,
          maxLines: 1,
        ),
        DialogTextField(
          hintText: L10n.of(context)!.chooseAStrongPassword,
          obscureText: true,
          minLines: 1,
          maxLines: 1,
        ),
      ],
    );
    if (input == null) return;
    final success = await showFutureLoadingDialog(
      context: context,
      future: () => Matrix.of(context).client.changePassword(input.last, oldPassword: input.first),
    );
    if (success.error == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(L10n.of(context)!.passwordHasBeenChanged)));
    }
  }

  void setAppLockAction() async {
    if (AppLock.of(context).isActive) {
      AppLock.of(context).showLockScreen();
    }
    final newLock = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.pleaseChooseAPasscode,
      message: L10n.of(context)!.pleaseEnter4Digits,
      cancelLabel: L10n.of(context)!.cancel,
      textFields: [
        DialogTextField(
          validator: (text) {
            if (text!.isEmpty ||
                (text.length == 4 && int.tryParse(text)! >= 0)) {
              return null;
            }
            return L10n.of(context)!.pleaseEnter4Digits;
          },
          keyboardType: TextInputType.number,
          obscureText: true,
          maxLines: 1,
          minLines: 1,
          maxLength: 4,
        ),
      ],
    );
    if (newLock != null) {
      await AppLock.of(context).changePincode(newLock.single);
    }
  }

  Future<void> showBootstrapDialog(BuildContext context) async {
    await BootstrapDialog(
      client: Matrix.of(context).client,
    ).show(context);
  }

  void refresh() {
    setState(() {});
  }

  Future<bool> checkRecoveryKeyValidity({
    required String recoveryKey,
  }) async {
    final bool isValidKey;
    try {
      final Uint8List privateKey = SSSS.decodeRecoveryKey(recoveryKey);
      final OpenSSSS openSSSS = matrixState.client.encryption!.ssss.open(EventTypes.CrossSigningMasterKey);
      isValidKey = await matrixState.client.encryption!.ssss.checkKey(privateKey, openSSSS.keyData);
    } on InvalidPassphraseException catch (e) {

      Logs().e("checkRecoveryKeyValidity: ${e.cause}");
      return false;
    }
    Logs().i("checkRecoveryKeyValidity: $isValidKey");
    return isValidKey;
  }

  String encryptRecoveryKey(String recoveryKey) {
    final String publicKeyString = matrixState.homeServer!.publicKey;
    final parser = encrypt.RSAKeyParser();
    final RSAPublicKey publicKey = parser.parse(publicKeyString) as RSAPublicKey;

    final encrypt.Encrypter encrypter = encrypt.Encrypter(encrypt.RSA(publicKey: publicKey));

    final encrypt.Encrypted encryptedText = encrypter.encrypt(recoveryKey);
    return encryptedText.base64;
  }

  Future<void> sendEncryptedRecoveryKey(String key) async {
    final String encryptedRecoveryKey = encryptRecoveryKey(key);
    final result =
    await matrixState.pagemeClient.user.addUserEncryptionKey(userId: matrixState.client.userID!, encryptedEncrytionKey: encryptedRecoveryKey, appKey: PageMeClientAppKey.appKey);
    setState(() {
      isRecoveryKeyStoredOnServer = result;
    });
    Logs().i('Encryption key stored in serverpod: $isRecoveryKeyStoredOnServer for user: ${matrixState.client.userID}, encryptedRecoveryKey: $encryptedRecoveryKey');
  }

  @override
  Future<void> didChangeDependencies() async {
    matrixState = Matrix.of(context);

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {

    return SettingsSecurityView(this);
  }
}
