import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/pages/settings/settings_container.dart';
import 'package:pageMe/pages/settings_security/settings_secuirty_encryption_header.dart';

import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/widgets/matrix.dart';
import '../../config/themes.dart';
import '../settings/settings_section_title.dart';
import 'settings_security.dart';

class SettingsSecurityView extends StatelessWidget {
  final SettingsSecurityController controller;
  const SettingsSecurityView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;

    return Scaffold(
      backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
      appBar: AppBar(
        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
        title: Text(L10n.of(context)!.security, style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
        leading: const BackButton(),
        centerTitle: false,
        elevation: 0,
      ),
      body: ListTileTheme(
        iconColor: Theme.of(context).textTheme.bodyText1!.color,
        child: Container(
          constraints: const BoxConstraints(maxWidth: 1000),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SettingsSectionTitle(title: 'Password Management'),
                SettingsContainer(
                  context: context,
                  iconData: Icons.lock_reset_outlined,
                  title: L10n.of(context)!.changePassword,
                  backgroundColor: backgroundColor,
                  contentColor: contentColor,
                  onTap: controller.changePasswordAccountAction,
                ),
                SettingsContainer(
                  context: context,
                  iconData: Icons.mail_lock_outlined,
                  title: L10n.of(context)!.passwordRecovery,
                  backgroundColor: backgroundColor,
                  contentColor: contentColor,
                  onTap: () => context.go('/settings/security/3pid'),
                ),
                const SettingsSectionTitle(title: 'Privacy'),
                SettingsContainer(
                  context: context,
                  iconData: Icons.group_remove_outlined,
                  title: L10n.of(context)!.ignoredUsers,
                  backgroundColor: backgroundColor,
                  contentColor: contentColor,
                  onTap: () => context.go('/settings/security/ignorelist'),
                ),
                if (PlatformInfos.isMobile)
                  SettingsContainer(
                    context: context,
                    iconData: Icons.phonelink_lock_outlined,
                    title: L10n.of(context)!.appLock,
                    backgroundColor: backgroundColor,
                    contentColor: contentColor,
                    onTap: () => controller.setAppLockAction(),
                  ),
                if (Matrix.of(context).client.encryption != null) ...{
                  SettingsEncryptionHeaderSection(controller: controller),
                },
                const SizedBox(
                  height: 16,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
