import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/device_settings/device_list.dart';
import 'package:pageMe/pages/device_settings/devices_list_appbar.dart';
import 'package:pageMe/pages/device_settings/user_device_list_item.dart';
import '../../config/themes.dart';
import '../../widgets/layouts/max_width_body.dart';

class DevicesListView extends StatelessWidget {
  final DeviceListController controller;
  final DeviceState deviceState;
  DevicesListView({Key? key, required this.controller, required this.deviceState}) : super(key: key);

  final List<Device> devices = [];

  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;
    return Scaffold(
      backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
      appBar: DevicesListAppBar(
        deviceState: deviceState,
        controller: controller,
        context: context,
      ),
      body: Container(
        constraints: const BoxConstraints(
          maxWidth: 1000,
        ),
        child: FutureBuilder<bool>(
          future: controller.loadUserDevices(context),
          builder: (BuildContext context, snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Icon(Icons.error_outlined),
                    Text(snapshot.error.toString()),
                  ],
                ),
              );
            }
            if (!snapshot.hasData || controller.devices == null) {
              return const Center(child: CircularProgressIndicator.adaptive(strokeWidth: 2));
            }
            switch (deviceState) {
              case DeviceState.verified:
                return Scrollbar(
                  controller: scrollController,
                  interactive: true,
                  trackVisibility: true,
                  child: ListView.builder(
                    shrinkWrap: true,
                    controller: scrollController,
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemCount: controller.verifiedDevices.length,
                    itemExtent: 94,
                    itemBuilder: (context, i) => DeviceListItem(
                      deviceState: DeviceState.verified,
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      userDevice: controller.verifiedDevices[i],
                      controller: controller,
                      context: context,
                      isSelected: controller.isSelected(controller.verifiedDevices[i].deviceId),
                    ),
                  ),
                );
              case DeviceState.unverified:
                return Scrollbar(
                  controller: scrollController,
                  interactive: true,
                  trackVisibility: true,
                  child: ListView.builder(
                    key: ValueKey(controller.selectedDevices.toString()),
                    shrinkWrap: true,
                    controller: scrollController,
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemCount: controller.unverifiedDevices.length,
                    itemBuilder: (context, i) => DeviceListItem(
                      deviceState: DeviceState.unverified,
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      userDevice: controller.unverifiedDevices[i],
                      controller: controller,
                      context: context,
                      isSelected: controller.isSelected(controller.unverifiedDevices[i].deviceId),
                    ),
                  ),
                );
              case DeviceState.blocked:
                return Scrollbar(
                  controller: scrollController,
                  interactive: true,
                  trackVisibility: true,
                  child: ListView.builder(
                    key: ValueKey(controller.selectedDevices.toString()),
                    shrinkWrap: true,
                    controller: scrollController,
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemCount: controller.blockedDevices.length,
                    itemExtent: 94,
                    itemBuilder: (context, i) => DeviceListItem(
                      deviceState: DeviceState.blocked,
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      userDevice: controller.blockedDevices[i],
                      controller: controller,
                      context: context,
                      isSelected: controller.isSelected(controller.blockedDevices[i].deviceId),
                    ),
                  ),
                );
              default:
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(Icons.error_outlined),
                      Text('Invalid devices type passed to this page.'),
                    ],
                  ),
                );
            }
          },
        ),
      ),
    );
  }
}
