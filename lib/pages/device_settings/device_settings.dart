import 'dart:async';

import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/encryption/utils/key_verification.dart';
import 'package:matrix/matrix.dart';

import 'package:pageMe/pages/device_settings/device_settings_view.dart';
import 'package:pageMe/pages/key_verification/key_verification_dialog.dart';
import '../../utils/logger_functions.dart';
import '../../widgets/matrix.dart';
import '../chat_list/chat_list.dart';
import 'device_provider.dart';

class DevicesSettings extends StatefulWidget {
  const DevicesSettings({Key? key}) : super(key: key);

  @override
  DevicesSettingsController createState() => DevicesSettingsController();
}

class DevicesSettingsController extends State<DevicesSettings> {
  List<Device>? devices;
  final selectedDevices = <String>{};
  late final Client client;
  String? errorMessage; // Declare an error state variable
  final _devicesStreamController = StreamController<List<Device>?>.broadcast();

  Stream<List<Device>?> get devicesStream => _devicesStreamController.stream;

  Future<void> loadUserDevices(BuildContext context) async {
    try {
      devices = await Matrix.of(context).client.getDevices();
      _devicesStreamController.add(devices);
    } on Exception catch (e) {
      // If there's an error, set the error message
      errorMessage = "Error loading devices: $e";
    }
  }

  void reload() => setState(() => devices = null);

  bool loadingDeletingDevices = false;
  String? errorDeletingDevices;

  @override
  void initState() {
    loadUserDevices(context);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    client = Matrix.of(context).client;
    loadUserDevices(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _devicesStreamController.close();
    super.dispose();
  }

  void selectDeviceAction(String deviceId) {
    Logs().i('selectDeviceAction: $deviceId');
    setState(() => selectedDevices.contains(deviceId) ? selectedDevices.remove(deviceId) : selectedDevices.add(deviceId));
    Logs().i('selectedDevices: ${selectedDevices.toString()}');
  }

  bool isSelected(String deviceId) {
    return selectedDevices.contains(deviceId);
  }

  SelectMode get selectMode => selectedDevices.isEmpty ? SelectMode.normal : SelectMode.select;

  void removeDevicesAction(List<Device> devices) async {
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.areYouSure,
          message: 'Remove ${devices.length} devices',
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.cancel) return;
    final deviceIds = <String>[];
    for (final userDevice in devices) {
      deviceIds.add(userDevice.deviceId);
    }

    try {
      setState(() {
        loadingDeletingDevices = true;
        errorDeletingDevices = null;
      });
      await client.uiaRequestBackground(
        (auth) => client.deleteDevices(
          deviceIds,
          auth: auth,
        ),
      );
      reload();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Successfully removed ${devices.length} devices',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } catch (e, s) {
      Logs().e('Error while deleting devices', e, s);
      setState(() => errorDeletingDevices = e.toString());
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Error while deleting devices: $errorDeletingDevices',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } finally {
      setState(() {
        loadingDeletingDevices = false;
        selectedDevices.clear();
      });
    }
  }

  void renameDeviceAction(Device device) async {
    final displayName = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.changeDeviceName,
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      textFields: [
        DialogTextField(
          hintText: device.displayName,
        )
      ],
    );
    if (displayName == null) return;
    final success = await showFutureLoadingDialog(
      context: context,
      future: () => client.updateDevice(device.deviceId, displayName: displayName.single),
    );
    if (success.error == null) {
      reload();
    }
  }

  bool _isOwnDevice(Device userDevice) => userDevice.deviceId == Matrix.of(context).client.deviceID;

  bool _isVerified(Device userDevice) {
    return client.userDeviceKeys[client.userID]?.deviceKeys[userDevice.deviceId]?.verified ?? false;
  }

  bool _isUnverified(Device userDevice) {
    return !_isVerified(userDevice) && !_isBlocked(userDevice);
  }

  bool _isBlocked(Device userDevice) {
    return client.userDeviceKeys[client.userID]?.deviceKeys[userDevice.deviceId]?.blocked ?? false;
  }

  Device? get thisDevice => devices!.firstWhereOrNull(
        _isOwnDevice,
      );

  List<Device> get notThisDevice {
    return List<Device>.from(devices!)
      ..removeWhere(_isOwnDevice)
      ..sort((a, b) => (b.lastSeenTs ?? 0).compareTo(a.lastSeenTs ?? 0));
  }

  List<Device> get verifiedDevices {
    return notThisDevice..where(_isVerified)
      ..removeWhere(_isUnverified)
      ..removeWhere(_isBlocked)
      ..toList();
  }

  List<Device> get unverifiedDevices {
    return notThisDevice
      ..removeWhere(_isVerified)
      ..removeWhere(_isBlocked)
      ..toList();
  }

  List<Device> get blockedDevices {
    return notThisDevice.where(_isBlocked).toList()..removeWhere(_isUnverified);
  }

  @override
  Widget build(BuildContext context) {
    return DevicesSettingsView(this);
  }
}
