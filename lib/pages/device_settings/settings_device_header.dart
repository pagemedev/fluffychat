import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/date_time_extension.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/device_extension.dart';
import '../../config/themes.dart';

class SettingsDeviceHeaderSection extends StatelessWidget {
  const SettingsDeviceHeaderSection({
    Key? key,
    required this.userDevice,
  }) : super(key: key);

  final Device userDevice;

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
          boxShadow: const [
            BoxShadow(
              blurRadius: 3,
              color: Color(0x33000000),
              offset: Offset(0, 1),
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(16, 16, 16, 16),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                width: 90,
                height: 90,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  shape: BoxShape.rectangle,
                  border: Border.all(
                    color: Theme.of(context).colorScheme.secondary,
                    width: 2,
                  ),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(18),
                  child: Icon(
                    userDevice.icon,
                    color: contentColor,
                    size: 40,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(20, 0, 0, 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(userDevice.deviceName,
                          maxLines: 2,
                          softWrap: true,
                          style: TextStyle(
                            overflow: TextOverflow.ellipsis,
                            color:
                            PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          )),
                      if (userDevice.appDetails != null)
                        Text(userDevice.appDetails!,
                            maxLines: 2,
                            softWrap: true,
                            style: TextStyle(
                              overflow: TextOverflow.ellipsis,
                              color: PageMeThemes.isDarkMode(context)
                                  ? Theme.of(context).colorScheme.onTertiaryContainer
                                  : Theme.of(context).colorScheme.onSurface,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            )),
                      Padding(
                        padding: const EdgeInsetsDirectional.fromSTEB(2, 0, 0, 0),
                        child: Text(
                          L10n.of(context)!.lastActiveAgo(DateTime.fromMillisecondsSinceEpoch(userDevice.lastSeenTs ?? 0).localizedTimeShort(context)),
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onBackground,
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
/*                      if (userDevice.lastSeenIp != null) Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(2, 2, 0, 0),
                            child: Text(
                              userDevice.lastSeenIp!,
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.onBackground,
                                fontSize: 14,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          )*/
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),

    );
  }
}
