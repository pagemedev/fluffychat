import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../../config/themes.dart';
import '../../utils/platform_infos.dart';
import 'device_list.dart';

enum DeviceState {verified, unverified, blocked}
/// This class represents an AppBar for a devices list. It is a custom AppBar
/// that is intended to be used in a chat application.
///
/// The AppBar changes its behavior and display based on the select mode
/// (i.e., normal or select), which is passed as a parameter.
///
/// The class also requires a `DeviceListController` and a `BuildContext` upon instantiation.
class DevicesListAppBar extends StatelessWidget implements PreferredSizeWidget {
  /// Creates a `DevicesListAppBar`.
  ///
  /// The [selectMode], [controller], and [context] arguments must not be null.
  const DevicesListAppBar({Key? key, required this.controller, required this.context, required this.deviceState}) : super(key: key);

  final DeviceListController controller;
  final DeviceState deviceState;
  final BuildContext context;

  @override
  Size get preferredSize => const Size.fromHeight(58.0);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return AppBar(
        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
        automaticallyImplyLeading: false,
        elevation: 0,
        toolbarHeight: 56,
        leadingWidth: _calculateLeadingWidth(),
        leading: _buildLeading(),
        centerTitle: false,
        actions: _buildActions(constraints.maxWidth),
        title: _buildTitle(),
      );
    });
  }

  /// Calculates the width of the leading widget based on the select mode and context.
  double _calculateLeadingWidth() {
    return !controller.isSelectMode
        ? PageMeThemes.isColumnMode(context)
            ? 56
            : 56
        : 56;
  }

  /// Builds the actions for the AppBar in select mode on a mobile platform.
  List<Widget> _buildSelectMobileActions() {
    // Your logic when selectMode is Select and platform is mobile
    return [
      if (deviceState == DeviceState.verified || deviceState == DeviceState.unverified )IconButton(
        tooltip: L10n.of(context)!.blockDevice,
        icon: const Icon(Icons.remove_moderator_outlined),
        onPressed: () => controller.blockDevicesAction(controller.selectedDevices.toList()),
      ),
      if (deviceState == DeviceState.blocked) IconButton(
        tooltip: L10n.of(context)!.unblockDevice,
        icon: const Icon(Icons.add_moderator_outlined),
        onPressed: () => controller.unblockDevicesAction(controller.selectedDevices.toList()),
      ),
      IconButton(
        tooltip: L10n.of(context)!.removeDevice,
        icon: const Icon(Icons.delete_sweep),
        onPressed: () => controller.removeDevicesAction(controller.selectedDevices.toList()),
      ),
    ];
  }

  /// Builds the actions for the AppBar based on the select mode.
  List<Widget>? _buildActions(double maxWidth) {
    if (controller.isSelectMode && PlatformInfos.isMobile) {
      return _buildSelectMobileActions();
    } else {
      return [];
    }
  }

  /// Builds the leading widget for the AppBar.
  Widget _buildLeading() {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0),
      child: _buildIconButton(),
    );
  }

  /// Builds an icon button for the AppBar.
  Widget? _buildIconButton() {
    return !controller.isSelectMode
        ? const BackButton()
        : IconButton(
            tooltip: L10n.of(context)!.cancel,
            icon: const Icon(Icons.close_outlined),
            onPressed: () => controller.cancelAction(),
          );
  }

  /// Builds the title for the AppBar based on the select mode.
  Widget _buildTitle() {
    if (controller.isSelectMode) {
      return _buildSelectTitle();
    } else {
      return _buildNormalTitle();
    }
  }

  /// Builds the title for the AppBar in select mode.
  Widget _buildSelectTitle() {
    return Text(
      controller.selectedDevices.length.toString(),
      style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
    );
  }

  /// Builds the title for the appbar when nothing is selected
  Widget _buildNormalTitle() {
    switch (deviceState){
      case DeviceState.verified:
        return Text('Verified Devices', style: TextStyle(color: Theme.of(context).colorScheme.onBackground));
      case DeviceState.unverified:
        return Text('Unverified Devices', style: TextStyle(color: Theme.of(context).colorScheme.onBackground));
      case DeviceState.blocked:
        return Text('Blocked Devices', style: TextStyle(color: Theme.of(context).colorScheme.onBackground));
    }

  }
}
