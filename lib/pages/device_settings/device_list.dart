import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/encryption/utils/key_verification.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/device_settings/devices_list_view.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../../utils/logger_functions.dart';
import '../../widgets/matrix.dart';
import '../chat_list/chat_list.dart';
import '../key_verification/key_verification_dialog.dart';
import 'devices_list_appbar.dart';

class DeviceList extends StatefulWidget {
  final DeviceState deviceState;
  const DeviceList({Key? key, required this.deviceState}) : super(key: key);

  @override
  DeviceListController createState() => DeviceListController();
}

class DeviceListController extends State<DeviceList> {
  List<Device>? devices;
  final selectedDevices = <Device>{};
  late final Client client;


  Future<bool> loadUserDevices(BuildContext context) async {
    if (devices != null) return true;
    devices = await Matrix.of(context).client.getDevices();
    return true;
  }

  void reload() => setState(() => devices = null);

  bool loadingDeletingDevices = false;
  String? errorDeletingDevices;

  @override
  void didChangeDependencies() {
    client = Matrix.of(context).client;
    super.didChangeDependencies();
  }

  void selectDeviceAction(Device device) {
    Logs().i( 'selectDeviceAction: ${device.deviceId}');
    setState(() {
      final existingDevices = selectedDevices.where((d) => d.deviceId == device.deviceId);
      if (existingDevices.isNotEmpty) {
        selectedDevices.remove(existingDevices.first);
      } else {
        selectedDevices.add(device);
      }
    });
    Logs().i( 'selectedDevices: ${selectedDevices.map((d) => d.deviceId).toString()}');
  }

  bool isSelected(String deviceId) {
    return selectedDevices.where((element) => element.deviceId == deviceId).isNotEmpty;
  }

  void cancelAction() {
    setState(() {
      selectedDevices.clear();
    });
  }

  bool get isSelectMode => selectMode == SelectMode.select;

  SelectMode get selectMode => selectedDevices.isEmpty ? SelectMode.normal : SelectMode.select;

  void removeDevicesAction(List<Device> devices) async {
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.areYouSure,
          message: 'Remove ${devices.length} devices',
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.cancel) return;
    final deviceIds = <String>[];
    for (final userDevice in devices) {
      deviceIds.add(userDevice.deviceId);
    }

    try {
      setState(() {
        loadingDeletingDevices = true;
        errorDeletingDevices = null;
      });
      await client.uiaRequestBackground(
        (auth) => client.deleteDevices(
          deviceIds,
          auth: auth,
        ),
      );
      reload();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Successfully removed ${devices.length} devices',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } catch (e, s) {
      Logs().e('Error while deleting devices', e, s);
      setState(() => errorDeletingDevices = e.toString());
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Error while deleting devices: $errorDeletingDevices',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } finally {
      setState(() {
        loadingDeletingDevices = false;
        selectedDevices.clear();
      });
    }
  }

  void removeDeviceAction(Device device) async {
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.areYouSure,
          message: 'Remove ${device.displayName ?? device.deviceId}',
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.cancel) return;
    final deviceId = device.deviceId;

    try {
      setState(() {
        loadingDeletingDevices = true;
        errorDeletingDevices = null;
      });
      await client.uiaRequestBackground(
        (auth) => client.deleteDevice(
          deviceId,
          auth: auth,
        ),
      );
      setState(() {});
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Successfully removed: ${device.displayName ?? device.deviceId}',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } catch (e, s) {
      Logs().e('Error while deleting devices', e, s);
      setState(() => errorDeletingDevices = e.toString());
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Error while deleting device: $errorDeletingDevices',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } finally {
      setState(() => loadingDeletingDevices = false);
    }
  }

  void renameDeviceAction(Device device) async {
    final displayName = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.changeDeviceName,
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      textFields: [
        DialogTextField(
          hintText: device.displayName,
        )
      ],
    );
    if (displayName == null) return;
    final success = await showFutureLoadingDialog(
      context: context,
      future: () => client.updateDevice(device.deviceId, displayName: displayName.single),
    );
    if (success.error == null) {
      reload();
    }
  }

  void verifyDeviceAction(Device device) async {
    final req = client.userDeviceKeys[client.userID!]!.deviceKeys[device.deviceId]!.startVerification();
    req.onUpdate = () {
      if ({KeyVerificationState.error, KeyVerificationState.done}.contains(req.state)) {
        setState(() {});
      }
    };
    await KeyVerificationDialog(request: req).show(context);
  }

  void blockDeviceAction(Device device) async {
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.areYouSure,
          message: 'Block ${device.displayName ?? device.deviceId}',
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.cancel) return;
    final key = client.userDeviceKeys[client.userID!]!.deviceKeys[device.deviceId]!;
    if (key.directVerified) {
      await key.setVerified(false);
    }
    await key.setBlocked(true);

    setState(() {});
    if (_isBlocked(device)) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Successfully blocked: ${device.displayName ?? device.deviceId}',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Error while blocking device: ${device.displayName ?? device.deviceId}',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    }
  }

  void blockDevicesAction(List<Device> devices) async {
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.areYouSure,
          message: 'Block ${devices.length} devices',
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.cancel) return;
    for (final device in devices) {
      final key = client.userDeviceKeys[client.userID!]!.deviceKeys[device.deviceId]!;
      if (key.directVerified) {
        await key.setVerified(false);
      }
      await key.setBlocked(true);
    }
    bool allDevicesBlocked = true;
    for (final device in devices) {
      if (!_isBlocked(device)) {
        allDevicesBlocked = false;
      }
    }

    if (allDevicesBlocked) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Successfully blocked ${devices.length} devices',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Error while blocking devices',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    }
    cancelAction();
  }

  void unblockDevicesAction(List<Device> devices) async {
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.areYouSure,
          message: 'Unblock ${devices.length} devices',
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.cancel) return;
    for (final device in devices) {
      final key = client.userDeviceKeys[client.userID!]!.deviceKeys[device.deviceId]!;
      await key.setBlocked(false);
    }
    bool allDevicesUnblocked = true;
    for (final device in devices) {
      if (_isBlocked(device)) {
        allDevicesUnblocked = false;
      }
    }

    if (allDevicesUnblocked) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Successfully unblocked ${devices.length} devices',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Error while unblocking devices',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    }
    cancelAction();
  }

  void unblockDeviceAction(Device device) async {
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.areYouSure,
          message: 'Unblock ${device.displayName ?? device.deviceId}',
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.cancel) return;
    final key = client.userDeviceKeys[client.userID!]!.deviceKeys[device.deviceId]!;
    await key.setBlocked(false);
    setState(() {});
    if (!_isBlocked(device)) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Successfully unblocked: ${device.displayName ?? device.deviceId}',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Error while unblocking device: ${device.displayName ?? device.deviceId}',
            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
          ),
          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
      );
    }
  }

  bool _isOwnDevice(Device userDevice) => userDevice.deviceId == Matrix.of(context).client.deviceID;


  bool _isVerified(Device userDevice) {
    return client.userDeviceKeys[client.userID]?.deviceKeys[userDevice.deviceId]?.verified ?? false;
  }

  bool _isUnverified(Device userDevice) {
    return !_isVerified(userDevice) && !_isBlocked(userDevice);
  }

  bool _isBlocked(Device userDevice) {
    return client.userDeviceKeys[client.userID]?.deviceKeys[userDevice.deviceId]?.blocked ?? false;
  }


  Device? get thisDevice => devices!.firstWhereOrNull(
        _isOwnDevice,
      );

  List<Device> get notThisDevice {
    return List<Device>.from(devices!)
      ..removeWhere(_isOwnDevice)
      ..sort((a, b) => (b.lastSeenTs ?? 0).compareTo(a.lastSeenTs ?? 0));
  }

  List<Device> get verifiedDevices {
    return notThisDevice..where(_isVerified)
      ..removeWhere(_isUnverified)
      ..removeWhere(_isBlocked)
      ..toList();
  }

  List<Device> get unverifiedDevices {
    return notThisDevice..removeWhere(_isVerified)..removeWhere(_isBlocked)..toList();
  }

  List<Device> get blockedDevices {
    return notThisDevice.where(_isBlocked).toList()..removeWhere(_isUnverified);
  }

  @override
  Widget build(BuildContext context) => DevicesListView(
        controller: this,
        deviceState: widget.deviceState,
      );
}
