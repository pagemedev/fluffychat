import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/device_settings/device_settings.dart';
import 'package:pageMe/pages/device_settings/settings_device_header.dart';
import 'package:pageMe/pages/settings/settings_container.dart';
import 'package:pageMe/pages/settings/settings_profile_header.dart';
import 'package:pageMe/pages/settings/settings_section_title.dart';
import 'package:pageMe/widgets/layouts/max_width_body.dart';
import '../../config/themes.dart';
import 'device_list.dart';
import 'devices_list_appbar.dart';

class DevicesSettingsView extends StatefulWidget {
  final DevicesSettingsController controller;

  const DevicesSettingsView(this.controller, {Key? key}) : super(key: key);

  @override
  State<DevicesSettingsView> createState() => _DevicesSettingsViewState();
}

class _DevicesSettingsViewState extends State<DevicesSettingsView> {
  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;
    return Scaffold(
      backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
        leading: const BackButton(),
        centerTitle: false,
        title: Text(L10n.of(context)!.devices, style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
      ),
      body: StreamBuilder<List<Device>?>(
        stream: widget.controller.devicesStream,
        builder: (BuildContext context, snapshot) {
          if (widget.controller.errorMessage != null) {
            return Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Icon(Icons.error_outlined),
                  Text(snapshot.error.toString()),
                ],
              ),
            );
          }
          if (!snapshot.hasData || widget.controller.devices == null) {
            return const Center(child: CircularProgressIndicator.adaptive(strokeWidth: 2));
          }
          return Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Container(
              constraints: const BoxConstraints(
                maxWidth: 1000,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SettingsDeviceHeaderSection(userDevice: widget.controller.thisDevice!),
                    const SettingsSectionTitle(title: 'Manage this device'),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.edit,
                      title: 'Rename device',
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => widget.controller.renameDeviceAction(widget.controller.thisDevice!),
                    ),
                    SettingsSectionTitle(title: 'Manage your other devices (${widget.controller.notThisDevice.length})'),
                    if (widget.controller.verifiedDevices.isNotEmpty)
                      SettingsContainer(
                        context: context,
                        iconData: Icons.verified_user_outlined,
                        title: 'Verified Devices (${widget.controller.verifiedDevices.length})',
                        backgroundColor: backgroundColor,
                        contentColor: contentColor,
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const DeviceList(
                              deviceState: DeviceState.verified,
                            ),
                          ),
                        ).then((value) { widget.controller.loadUserDevices(context);}),
                      ),
                    if (widget.controller.unverifiedDevices.isNotEmpty)
                      SettingsContainer(
                        context: context,
                        iconData: Icons.safety_check_outlined,
                        title: 'Unverified Devices (${widget.controller.unverifiedDevices.length})',
                        backgroundColor: backgroundColor,
                        contentColor: contentColor,
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>  const DeviceList(
                              deviceState: DeviceState.unverified ,
                            ),
                          ),
                        ).then((value) { widget.controller.loadUserDevices(context);}),
                      ),
                    if (widget.controller.blockedDevices.isNotEmpty)
                      SettingsContainer(
                        context: context,
                        iconData: Icons.remove_moderator_outlined,
                        title: 'Blocked Devices (${widget.controller.blockedDevices.length})',
                        backgroundColor: backgroundColor,
                        contentColor: contentColor,
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const DeviceList(
                              deviceState: DeviceState.blocked,
                            ),
                          ),
                        ).then((value) { widget.controller.loadUserDevices(context);}),
                      ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.delete_outline,
                      title: widget.controller.errorDeletingDevices ?? L10n.of(context)!.removeAllOtherDevices,
                      backgroundColor: backgroundColor,
                      contentColor: Colors.red,
                      onTap: widget.controller.loadingDeletingDevices ? null : () => widget.controller.removeDevicesAction(widget.controller.notThisDevice),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
