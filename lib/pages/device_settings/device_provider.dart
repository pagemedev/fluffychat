import 'package:flutter/widgets.dart';

import 'device_settings.dart';

class DevicesSettingsControllerProvider extends InheritedWidget {
  final DevicesSettingsController devicesSettingsController;

  const DevicesSettingsControllerProvider({Key? key, required this.devicesSettingsController, required Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(DevicesSettingsControllerProvider oldWidget) {
    return devicesSettingsController != oldWidget.devicesSettingsController;
  }

  static DevicesSettingsController? of(BuildContext context) {
    final DevicesSettingsControllerProvider? result = context.dependOnInheritedWidgetOfExactType<DevicesSettingsControllerProvider>();
    return result?.devicesSettingsController;
  }
}