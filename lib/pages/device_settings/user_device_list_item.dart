
import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/conditonal_wrapper.dart';
import 'package:pageMe/utils/string_color.dart';

import '../../config/themes.dart';
import '../../utils/date_time_extension.dart';
import '../../utils/matrix_sdk_extensions.dart/device_extension.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/context_menu/custom_context_menu.dart';
import '../../widgets/matrix.dart';
import '../chat_list/chat_list.dart';
import 'device_list.dart';
import 'devices_list_appbar.dart';

enum UserDeviceListItemAction {
  rename,
  remove,
  verify,
  block,
  unblock,
}

class UserDeviceListItem extends StatelessWidget {
  final Device userDevice;
  final void Function(Device) remove;
  final void Function(Device) rename;
  final void Function(Device) verify;
  final void Function(Device) block;
  final void Function(Device) unblock;

  const UserDeviceListItem(
    this.userDevice, {
    required this.remove,
    required this.rename,
    required this.verify,
    required this.block,
    required this.unblock,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    final keys = client.userDeviceKeys[Matrix.of(context).client.userID]?.deviceKeys[userDevice.deviceId];
    final isOwnDevice = userDevice.deviceId == client.deviceID;

    return ListTile(
      onTap: () async {
        final action = await showModalActionSheet<UserDeviceListItemAction>(
          context: context,
          title: '${userDevice.displayName} (${userDevice.deviceId})',
          actions: [
            SheetAction(
              key: UserDeviceListItemAction.rename,
              label: L10n.of(context)!.changeDeviceName,
            ),
            if (!isOwnDevice && keys != null) ...{
              SheetAction(
                key: UserDeviceListItemAction.verify,
                label: L10n.of(context)!.verifyStart,
              ),
              if (!keys.blocked)
                SheetAction(
                  key: UserDeviceListItemAction.block,
                  label: L10n.of(context)!.blockDevice,
                  isDestructiveAction: true,
                ),
              if (keys.blocked)
                SheetAction(
                  key: UserDeviceListItemAction.unblock,
                  label: L10n.of(context)!.unblockDevice,
                  isDestructiveAction: true,
                ),
            },
            if (!isOwnDevice)
              SheetAction(
                key: UserDeviceListItemAction.remove,
                label: L10n.of(context)!.delete,
                isDestructiveAction: true,
              ),
          ],
        );
        if (action == null) return;
        switch (action) {
          case UserDeviceListItemAction.rename:
            rename(userDevice);
            break;
          case UserDeviceListItemAction.remove:
            remove(userDevice);
            break;
          case UserDeviceListItemAction.verify:
            verify(userDevice);
            break;
          case UserDeviceListItemAction.block:
            block(userDevice);
            break;
          case UserDeviceListItemAction.unblock:
            unblock(userDevice);
            break;
        }
      },
      leading: CircleAvatar(
        foregroundColor: Colors.white,
        backgroundColor: keys == null
            ? Colors.grey[700]
            : keys.blocked
                ? Colors.red
                : keys.verified
                    ? Colors.green
                    : Colors.orange,
        child: Icon(userDevice.icon),
      ),
      title: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              userDevice.displayname,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          if (keys != null)
            Text(
              keys.blocked
                  ? L10n.of(context)!.blocked
                  : keys.verified
                      ? L10n.of(context)!.verified
                      : L10n.of(context)!.unverified,
              style: TextStyle(
                color: keys.blocked
                    ? Colors.red
                    : keys.verified
                        ? Colors.green
                        : Colors.orange,
              ),
            ),
        ],
      ),
      subtitle: Text(
        L10n.of(context)!.lastActiveAgo(DateTime.fromMillisecondsSinceEpoch(userDevice.lastSeenTs ?? 0).localizedTimeShort(context)),
        style: const TextStyle(fontWeight: FontWeight.w300),
      ),
    );
  }
}

class DeviceListItem extends StatefulWidget {
  const DeviceListItem({
    Key? key,
    required this.context,
    required this.backgroundColor,
    required this.contentColor,
    this.trailingIcon,
    required this.userDevice,
    this.isSelected = false,
    required this.controller,
    required this.deviceState,
  }) : super(key: key);
  final DeviceState deviceState;
  final Device userDevice;
  final DeviceListController controller;
  final BuildContext context;
  final Color backgroundColor;
  final Color contentColor;
  final Icon? trailingIcon;
  final bool isSelected;

  @override
  State<DeviceListItem> createState() => _DeviceListItemState();
}

class _DeviceListItemState extends State<DeviceListItem> {
  bool isDeviceHovered = false;

  void toggleIsHovered(bool isHovered) {
    setState(() {
      isDeviceHovered = isHovered;
    });
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle contextMenuTextStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w500,
      color: Theme.of(context).colorScheme.onBackground,
    );
    final Color contextMenuIconColor = Theme.of(context).colorScheme.onBackground;
    const double contextMenuIconSize = 20;
    final client = Matrix.of(context).client;
    final keys = client.userDeviceKeys[Matrix.of(context).client.userID]?.deviceKeys[widget.userDevice.deviceId];
    final isOwnDevice = widget.userDevice.deviceId == client.deviceID;
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
      child: ConditionalWrapper(
        condition: !PlatformInfos.isMobile,
        wrapper: (context, child) {
          return CustomContextMenuArea(
            width: 250,
            verticalPadding: 0,
            builder: (c) {
              final List<Widget> contextMenuOptions = [];
              buildIndividualContextMenu(contextMenuOptions, context, contextMenuTextStyle, contextMenuIconSize, contextMenuIconColor, c, isOwnDevice, keys);
              buildMultiItemContextMenu(contextMenuOptions, context, contextMenuTextStyle, contextMenuIconSize, contextMenuIconColor, c);
              return [
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: contextMenuOptions,
                )
              ];
            },
            child: child,
          );
        },
        builder: (context) {
          return LayoutBuilder(
            builder: (context, constraints) {
              return Material(
                color: widget.isSelected ? Theme.of(context).colorScheme.primaryContainer : widget.backgroundColor, // Set the Material color
                borderRadius: BorderRadius.circular(12),
                elevation: 1, // Same as your Container
                child: InkWell(
                  onHover: (isHovered) => toggleIsHovered(isHovered),
                  onLongPress: () => widget.controller.selectDeviceAction(widget.userDevice),
                  onTap: () async {
                    if (widget.controller.selectMode == SelectMode.select) {
                      widget.controller.selectDeviceAction(widget.userDevice);
                    } else {
                      final action = await showModalActionSheet<UserDeviceListItemAction>(
                        context: context,
                        title: '${widget.userDevice.displayName} (${widget.userDevice.deviceId})',
                        actions: [
                          SheetAction(
                            key: UserDeviceListItemAction.rename,
                            label: L10n.of(context)!.changeDeviceName,
                            icon: Icons.edit,
                          ),
                          if (!isOwnDevice && keys != null) ...{
                            SheetAction(
                              key: UserDeviceListItemAction.verify,
                              label: L10n.of(context)!.verifyStart,
                              icon: Icons.verified_user_outlined,
                            ),
                            if (!keys.blocked)
                              SheetAction(
                                key: UserDeviceListItemAction.block,
                                label: L10n.of(context)!.blockDevice,
                                isDestructiveAction: true,
                                icon: Icons.remove_moderator_outlined,
                              ),
                            if (keys.blocked)
                              SheetAction(
                                key: UserDeviceListItemAction.unblock,
                                label: L10n.of(context)!.unblockDevice,
                                isDestructiveAction: true,
                                icon: Icons.add_moderator_outlined,
                              ),
                          },
                          if (!isOwnDevice)
                            SheetAction(
                              key: UserDeviceListItemAction.remove,
                              label: L10n.of(context)!.delete,
                              isDestructiveAction: true,
                              icon: Icons.delete_outline,
                            ),
                        ],
                      );
                      if (action == null) return;
                      switch (action) {
                        case UserDeviceListItemAction.rename:
                          widget.controller.renameDeviceAction(widget.userDevice);
                          break;
                        case UserDeviceListItemAction.remove:
                          widget.controller.removeDeviceAction(widget.userDevice);
                          break;
                        case UserDeviceListItemAction.verify:
                          widget.controller.verifyDeviceAction(widget.userDevice);
                          break;
                        case UserDeviceListItemAction.block:
                          widget.controller.blockDeviceAction(widget.userDevice);
                          break;
                        case UserDeviceListItemAction.unblock:
                          widget.controller.unblockDeviceAction(widget.userDevice);
                          break;
                      }
                    }
                  },
                  splashColor: widget.backgroundColor.withAlpha(50).lighten(),
                  highlightColor: widget.backgroundColor.withAlpha(30).lighten(),
                  borderRadius: BorderRadius.circular(12), // Same as your Container
                  child: SizedBox(
                    width: constraints.maxWidth,
                    child: Padding(
                      padding: const EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          if (PlatformInfos.isMobile)
                            Icon(
                              widget.userDevice.icon,
                              color: widget.isSelected ? Theme.of(context).colorScheme.onPrimaryContainer : widget.contentColor,
                              size: 24,
                            ),
                          if (!PlatformInfos.isMobile)
                            Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  widget.userDevice.icon,
                                  color: widget.isSelected ? Theme.of(context).colorScheme.onPrimaryContainer : widget.contentColor,
                                  size: 24,
                                ),
                                AnimatedSize(
                                  duration: const Duration(milliseconds: 300),
                                  child: SizedBox(
                                    height: (isDeviceHovered || widget.controller.selectMode == SelectMode.select) ? 15 : 0,
                                  ),
                                ),
                                AnimatedOpacity(
                                  duration: const Duration(milliseconds: 300),
                                  opacity: (isDeviceHovered || widget.controller.selectMode == SelectMode.select) ? 1 : 0,
                                  child: AnimatedSize(
                                    duration: const Duration(milliseconds: 300),
                                    child: SizedBox(
                                      height: (isDeviceHovered || widget.controller.selectMode == SelectMode.select) ? 15 : 0,
                                      width: (isDeviceHovered || widget.controller.selectMode == SelectMode.select) ? 15 : 0,
                                      child: Checkbox(
                                        value: widget.controller.selectedDevices.contains(widget.userDevice),
                                        onChanged: (_) => widget.controller.selectDeviceAction(widget.userDevice),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          Expanded(
                            child: Padding(
                                padding: const EdgeInsetsDirectional.fromSTEB(12, 0, 0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(widget.userDevice.deviceName,
                                        maxLines: 4,
                                        softWrap: true,
                                        style: TextStyle(
                                          overflow: TextOverflow.ellipsis,
                                          color: widget.isSelected
                                              ? Theme.of(context).colorScheme.onPrimaryContainer
                                              : PageMeThemes.isDarkMode(context)
                                                  ? Theme.of(context).colorScheme.onTertiaryContainer
                                                  : Theme.of(context).colorScheme.onSurface,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        )),
                                    if (widget.userDevice.appDetails != null)
                                      Text(widget.userDevice.appDetails!,
                                          maxLines: 4,
                                          softWrap: true,
                                          style: TextStyle(
                                            overflow: TextOverflow.ellipsis,
                                            color: widget.isSelected
                                                ? Theme.of(context).colorScheme.onPrimaryContainer
                                                : PageMeThemes.isDarkMode(context)
                                                    ? Theme.of(context).colorScheme.onTertiaryContainer
                                                    : Theme.of(context).colorScheme.onSurface,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                          )),
                                    if (widget.userDevice.lastSeenTs != null)
                                      Text(L10n.of(context)!.lastActiveAgo(DateTime.fromMillisecondsSinceEpoch(widget.userDevice.lastSeenTs ?? 0).localizedTimeShort(context)),
                                          maxLines: 2,
                                          softWrap: true,
                                          style: TextStyle(
                                            overflow: TextOverflow.ellipsis,
                                            color: widget.isSelected
                                                ? Theme.of(context).colorScheme.onPrimaryContainer
                                                : PageMeThemes.isDarkMode(context)
                                                    ? Theme.of(context).colorScheme.onTertiaryContainer
                                                    : Theme.of(context).colorScheme.onSurface,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                          ))
                                  ],
                                )),
                          ),
                          Align(
                            alignment: const AlignmentDirectional(0.9, 0),
                            child: (widget.trailingIcon != null)
                                ? widget.trailingIcon
                                : Icon(
                                    Icons.arrow_forward_ios,
                                    color: widget.isSelected ? Theme.of(context).colorScheme.onPrimaryContainer : widget.contentColor,
                                    size: 18,
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }
          );
        },
      ),
    );
  }

  void buildMultiItemContextMenu(
      List<Widget> contextMenuOptions, BuildContext context, TextStyle contextMenuTextStyle, double contextMenuIconSize, Color contextMenuIconColor, BuildContext c) {
    if (widget.controller.selectMode == SelectMode.select && widget.controller.selectedDevices.length >= 2) {
      if (widget.deviceState == DeviceState.verified || widget.deviceState == DeviceState.unverified) {
        contextMenuOptions.add(
          ListTile(
            dense: true,
            title: Text('Block devices', style: contextMenuTextStyle),
            trailing: Icon(
              Icons.remove_moderator_outlined,
              size: contextMenuIconSize,
              color: contextMenuIconColor,
            ),
            onTap: () async {
              Navigator.of(c).pop();
              widget.controller.blockDevicesAction(widget.controller.selectedDevices.toList());
            },
          ),
        );
      }
      if (widget.deviceState == DeviceState.blocked) {
        contextMenuOptions.add(
          ListTile(
            dense: true,
            title: Text("Unblock devices", style: contextMenuTextStyle),
            trailing: Icon(
              Icons.add_moderator_outlined,
              size: contextMenuIconSize,
              color: contextMenuIconColor,
            ),
            onTap: () async {
              Navigator.of(c).pop();
              widget.controller.unblockDevicesAction(widget.controller.selectedDevices.toList());
            },
          ),
        );
      }
      contextMenuOptions.add(
        ListTile(
          dense: true,
          title: Text("Remove devices", style: contextMenuTextStyle.copyWith(color: Colors.red, fontWeight: FontWeight.w600)),
          trailing: Icon(
            Icons.delete_sweep,
            size: contextMenuIconSize,
            color: Colors.red,
          ),
          onTap: () async {
            Navigator.of(c).pop();
            widget.controller.removeDevicesAction(widget.controller.selectedDevices.toList());
          },
        ),
      );
    }
  }

  void buildIndividualContextMenu(List<Widget> contextMenuOptions, BuildContext context, TextStyle contextMenuTextStyle, double contextMenuIconSize,
      Color contextMenuIconColor, BuildContext c, bool isOwnDevice, DeviceKeys? keys) {
    if (widget.controller.selectMode == SelectMode.normal || widget.controller.selectedDevices.length <= 1) {
      contextMenuOptions
        ..add(ListTile(
          dense: true,
          title: Text(L10n.of(context)!.changeDeviceName, style: contextMenuTextStyle.copyWith(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.w600)),
          trailing: Icon(
            Icons.edit,
            size: contextMenuIconSize,
            color: Theme.of(context).colorScheme.primary,
          ),
          onTap: () async {
            Navigator.of(c).pop();
            widget.controller.renameDeviceAction(widget.userDevice);
          },
        ))
        ..add(const Divider(
          height: 2,
          endIndent: 8,
          indent: 8,
        ));
      if (!isOwnDevice && keys != null) {
        contextMenuOptions.addAll([
          ListTile(
            dense: true,
            title: Text(
              L10n.of(context)!.verifyStart,
              style: contextMenuTextStyle,
            ),
            trailing: Icon(
              Icons.verified_user_outlined,
              size: contextMenuIconSize,
              color: contextMenuIconColor,
            ),
            onTap: () async {
              Navigator.of(c).pop();
              widget.controller.verifyDeviceAction(widget.userDevice);
            },
          ),
          if (!keys.blocked)
            ListTile(
              dense: true,
              title: Text(
                L10n.of(context)!.blockDevice,
                style: contextMenuTextStyle,
              ),
              trailing: Icon(
                Icons.remove_moderator_outlined,
                size: contextMenuIconSize,
                color: contextMenuIconColor,
              ),
              onTap: () async {
                Navigator.of(c).pop();
                widget.controller.blockDeviceAction(widget.userDevice);
              },
            ),
          if (keys.blocked)
            ListTile(
              dense: true,
              title: Text(L10n.of(context)!.unblockDevice, style: contextMenuTextStyle),
              trailing: Icon(
                Icons.add_moderator_outlined,
                size: contextMenuIconSize,
                color: contextMenuIconColor,
              ),
              onTap: () async {
                Navigator.of(c).pop();
                widget.controller.unblockDeviceAction(widget.userDevice);
              },
            )
        ]);
      }
      if (!isOwnDevice) {
        contextMenuOptions.add(
          ListTile(
            dense: true,
            title: Text(L10n.of(context)!.delete, style: contextMenuTextStyle.copyWith(color: Colors.red)),
            trailing: Icon(
              Icons.delete_outline,
              size: contextMenuIconSize,
              color: Colors.red,
            ),
            onTap: () async {
              Navigator.of(c).pop();
              widget.controller.removeDeviceAction(widget.userDevice);
            },
          ),
        );
      }
    }
  }
}
