import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:matrix/encryption/utils/bootstrap.dart';
import 'package:pageMe/pages/bootstrap/bootstrap_dialog.dart';
import 'package:pageMe/pages/onboarding_bootstrap/onboarding_bootstrap.dart';

import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../../widgets/matrix.dart';

class OnBoardingBootstrapHeading extends StatelessWidget {
  final OnBoardingBootstrapController controller;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 16.0);

  static const titleStyle = TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0);
  const OnBoardingBootstrapHeading({Key? key, required this.controller}) : super(key: key);

  Widget buildSVG(String assetName, [double width = 250, Color? color]) {
    return SafeArea(
      child: SvgPicture.asset('assets/$assetName', width: width, color: color),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: globalHeaderHeight, left: 12, right: 12),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                text: 'Backup',
                style: titleStyle.copyWith(color: Theme.of(context).colorScheme.primary),
                children: <TextSpan>[
                  TextSpan(text: ' and ', style: titleStyle.copyWith(color: PageMeThemes.whiteBlackColor(context))),
                  TextSpan(text: 'Encryption', style: titleStyle.copyWith(color: Theme.of(context).colorScheme.primary)),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5, left: 12, right: 12, bottom: globalHeaderHeight),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              "Last but not least is setting up our encryption and backup keys. This is the most important step and helps keep ${isBusiness() ? "you and your business's information safe!" : "your information safe!"}",
              textAlign: TextAlign.center,
              style: titleStyle.copyWith(fontSize: 18, fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ],
    );
  }
}

class OnBoardingBootstrapView extends StatelessWidget {
  final OnBoardingBootstrapController controller;
  static const double assetWidth = 350;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 19.0);
  static const companyStyle = TextStyle(fontSize: 22.0);
  static const titleStyle = TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0);

  const OnBoardingBootstrapView(this.controller, {Key? key}) : super(key: key);

  Widget buildImage(String assetName, [double? width = assetWidth, Color? color]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight),
        child: Image.asset('assets/$assetName', width: width, color: color),
      ),
    );
  }

  Widget buildSVG(String assetName, [double width = assetWidth, Color? color]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight),
        child: SvgPicture.asset('assets/$assetName', width: width, color: color),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.background,
          elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
          toolbarHeight: isBusiness() ? 90 : null,
          //automaticallyImplyLeading: !(Matrix.watch(context).client.encryption?.enabled ?? false),
          centerTitle: true,
          actions: [
            if (!isBusiness())
              TextButton(
                  onPressed: () async => controller.onFinishOnBoarding(context),
                  child: Text(
                    'Skip',
                    style: TextStyle(color: Theme.of(context).colorScheme.primary, fontSize: 18),
                  ))
          ],
          title: Column(
            children: [
              SizedBox(
                height: globalHeaderHeight,
                width: 150,
                child: SvgPicture.asset(
                  (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                  fit: BoxFit.fitHeight,
                ),
              ),
              if (isBusiness())
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "${controller.homeServerName}",
                    style: companyStyle.copyWith(color: Theme.of(context).colorScheme.primary),
                  ),
                )
            ],
          ),
        ),
        body: BootstrapDialog(
          client: Matrix.of(context).client,
          isOnBoardingScreen: true,
          wipe: false,
        ),
      ),
    );
  }
}
