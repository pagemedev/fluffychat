
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';

import 'package:pageMe/widgets/matrix.dart';
import '../../config/setting_keys.dart';
import '../../utils/famedlysdk_store.dart';
import '../bootstrap/bootstrap_dialog.dart';


import 'onboarding_bootstrap_view.dart';

class OnBoardingBootstrap extends StatefulWidget {
  const OnBoardingBootstrap({Key? key}) : super(key: key);

  @override
  OnBoardingBootstrapController createState() => OnBoardingBootstrapController();
}

class OnBoardingBootstrapController extends State<OnBoardingBootstrap> {
  final TextEditingController nameController = TextEditingController();
  late MatrixState matrix;
  bool _isInitialized = false;
  Future<dynamic>? profileFuture;
  Profile? profile;
  bool profileUpdated = false;
  String? homeServerName;
  int? pageNumber = 0;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const double assetWidth = 350;
  bool showBootStrap = false;
  bool completedBootstrap = false;
  Store? _store;
  Store get store => _store ??= Store();



  /// Sets the first time user key to false and navigates to the /rooms page (ChatList)
  Future<void> onFinishOnBoarding(context) async {
    final matrix = Matrix.of(context);
    await store.setItemBool(SettingKeys.firstTimeUser + matrix.client.userID!, false);
    context.go('/rooms');
  }
  
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() async {
      matrix = Matrix.of(context);
      if (!_isInitialized) {
        profileFuture ??= matrix.client
            .getProfileFromUserId(
          matrix.client.userID!,
          cache: !profileUpdated,
          getFromRooms: !profileUpdated,
        )
            .then((p) {
          if (mounted) setState(() => profile = p);
          return p;
        });
        homeServerName = matrix.client.homeserver.toString().replaceFirst('https://matrix.', '');
        homeServerName = homeServerName?.replaceFirst('.pageme.co.za', '').toUpperCase();
        _isInitialized = true;
      }
      super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    matrix.navigatorContext = context;
    return OnBoardingBootstrapView(this);
  }
}
