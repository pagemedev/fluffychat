import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/settings/settings_container.dart';
import 'package:pageMe/pages/settings/settings_section_title.dart';


import 'package:pageMe/config/setting_keys.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/widgets/settings_switch_list_tile.dart';
import '../../config/themes.dart';
import 'settings_chat.dart';

class SettingsChatView extends StatelessWidget {
  final SettingsChatController controller;
  const SettingsChatView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
        leading: const BackButton(),
        elevation: 0,
        title: Text(L10n.of(context)!.chat, style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
      ),
      backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
      body: SafeArea(
        child: Container(
          constraints: const BoxConstraints(maxWidth: 1000),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SettingsSectionTitle(title: 'Chat Display Options'),
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                  child: Material(
                    borderRadius: BorderRadius.circular(12),
                    color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                    elevation: 1,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: CupertinoListSection.insetGrouped(
                        margin: EdgeInsets.zero,
                        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                        children: [
                          SettingsSwitchListTile.adaptive(
                            title: 'Display rich content',
                            onChanged: (b) => FlavorConfig.renderHtml = b,
                            storeKey: SettingKeys.renderHtml,
                            defaultValue: FlavorConfig.renderHtml,
                          ),
                          SettingsSwitchListTile.adaptive(
                            title: 'Minimal chat list mode',
                            onChanged: (b) => FlavorConfig.minimalChatListMode = b,
                            storeKey: SettingKeys.minimalChatListMode,
                            defaultValue: FlavorConfig.minimalChatListMode,
                          ),
                          //additionalInfo: Text(profile?.displayName ?? Matrix.of(context).client.userID!.localpart!),
                          if (PlatformInfos.isMobile)
                            SettingsSwitchListTile.adaptive(
                              title: "Show animated stickers and emotes",
                              onChanged: (b) => FlavorConfig.autoplayImages = b,
                              storeKey: SettingKeys.autoplayImages,
                              defaultValue: FlavorConfig.autoplayImages,
                            ),
                          //additionalInfo: Text(Matrix.of(context).client.userID!),
                          SettingsSwitchListTile.adaptive(
                            title: 'Show direct chats in spaces',
                            onChanged: (b) => FlavorConfig.showDirectChatsInSpaces = b,
                            storeKey: SettingKeys.showDirectChatsInSpaces,
                            defaultValue: FlavorConfig.showDirectChatsInSpaces,
                          ),
/*                          SettingsSwitchListTile.adaptive(
                            title: 'Separate direct chats and groups',
                            onChanged: (b) => FlavorConfig.separateChatTypes = b,
                            storeKey: SettingKeys.separateChatTypes,
                            defaultValue: FlavorConfig.separateChatTypes,
                          ),*/
                          //additionalInfo: Text(profile?.displayName ?? Matrix.of(context).client.userID!.localpart!),
                        ],
                      ),
                    ),
                  ),
                ),
                const SettingsSectionTitle(title: 'Chat Interaction'),
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                  child: Material(
                    borderRadius: BorderRadius.circular(12),
                    color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                    elevation: 1,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: CupertinoListSection.insetGrouped(
                        margin: EdgeInsets.zero,
                        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                        children: [
                          SettingsSwitchListTile.adaptive(
                            title: L10n.of(context)!.sendOnEnter,
                            onChanged: (b) => FlavorConfig.sendOnEnter = b,
                            storeKey: SettingKeys.sendOnEnter,
                            defaultValue: FlavorConfig.sendOnEnter,
                          ),
                          //additionalInfo: Text(profile?.displayName ?? Matrix.of(context).client.userID!.localpart!),
                        ],
                      ),
                    ),
                  ),
                ),
                SettingsContainer(
                  context: context,
                  onTap: () => context.go('${GoRouterState.of(context).fullPath}/emotes'),
                  iconData: Icons.insert_emoticon_outlined,
                  title: L10n.of(context)!.emoteSettings,
                  backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                  contentColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface,
                ),
                const SettingsSectionTitle(title: 'Advanced Content Settings'),
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                  child: Material(
                    borderRadius: BorderRadius.circular(12),
                    color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                    elevation: 1,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: CupertinoListSection.insetGrouped(
                        margin: EdgeInsets.zero,
                        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                        children: [
                          SettingsSwitchListTile.adaptive(
                            title: L10n.of(context)!.hideRedactedEvents,
                            onChanged: (b) => FlavorConfig.hideRedactedEvents = b,
                            storeKey: SettingKeys.hideRedactedEvents,
                            defaultValue: FlavorConfig.hideRedactedEvents,
                          ),
                          //additionalInfo: Text(profile?.displayName ?? Matrix.of(context).client.userID!.localpart!),
                          SettingsSwitchListTile.adaptive(
                            title: L10n.of(context)!.hideUnknownEvents,
                            onChanged: (b) => FlavorConfig.hideUnknownEvents = b,
                            storeKey: SettingKeys.hideUnknownEvents,
                            defaultValue: FlavorConfig.hideUnknownEvents,
                          ),
                          //additionalInfo: Text(profile?.displayName ?? Matrix.of(context).client.userID!.localpart!),
                          SettingsSwitchListTile.adaptive(
                            title: L10n.of(context)!.hideUnimportantStateEvents,
                            onChanged: (b) => FlavorConfig.hideUnimportantStateEvents = b,
                            storeKey: SettingKeys.hideUnimportantStateEvents,
                            defaultValue: FlavorConfig.hideUnimportantStateEvents,
                          ),
                          //additionalInfo: Text(profile?.displayName ?? Matrix.of(context).client.userID!.localpart!),
                        ],
                      ),
                    ),
                  ),
                ),

/*              if (Matrix.of(context).webrtcIsSupported)
                    SettingsSwitchListTile.adaptive(
                      title: L10n.of(context)!.experimentalVideoCalls,
                      onChanged: (b) {
                        FlavorConfig.experimentalVoip = b;
                        Matrix.of(context).createVoipPlugin();
                        return;
                      },
                      storeKey: SettingKeys.experimentalVoip,
                      defaultValue: FlavorConfig.experimentalVoip,
                    ),
                  const Divider(height: 1),
                  if (Matrix.of(context).webrtcIsSupported && !kIsWeb)
                    ListTile(
                      title: Text(L10n.of(context)!.callingPermissions),
                      onTap: () =>
                          CallKeepManager().checkoutPhoneAccountSetting(context),
                      trailing: const Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Icon(Icons.call),
                      ),
                    ),*/
                const SettingsSectionTitle(title: 'Experimental Features'),
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                  child: Material(
                    borderRadius: BorderRadius.circular(12),
                    color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                    elevation: 1,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: CupertinoListSection.insetGrouped(
                        margin: EdgeInsets.zero,
                        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                        children: [
                          SettingsSwitchListTile.adaptive(
                            title: "Search in chat",
                            onChanged: (b) => FlavorConfig.searchInChat = b,
                            storeKey: SettingKeys.searchInChat,
                            defaultValue: FlavorConfig.searchInChat,
                          ),
                          //additionalInfo: Text(profile?.displayName ?? Matrix.of(context).client.userID!.localpart!),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 16,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
