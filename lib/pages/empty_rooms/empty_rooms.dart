import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/config/setting_keys.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/pages/chat_list/navi_rail_item.dart';
import 'package:pageMe/pages/new_chat/new_contact_tile.dart';
import 'package:pageMe/pages/new_chat/new_group_tile.dart';

import 'package:pageme_client/pageme_client.dart' as pageme;
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/chat_list/chat_list.dart';
import 'package:pageMe/pages/empty_rooms/empty_rooms_view.dart';
import 'package:pageMe/pages/user_directory/user_directory_view.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:serverpod_flutter/serverpod_flutter.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import 'package:pageMe/utils/famedlysdk_store.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:pageMe/widgets/public_room_bottom_sheet.dart';
import '../../config/flavor_config.dart';

import '../../keys/pageme_client_appkey.dart';
import '../../utils/global_key_provider.dart';
import '../../utils/pageme_share.dart';
import '../../utils/platform_infos.dart';
import '../../utils/url_launcher.dart';
import '../new_chat/new_chat.dart';
import '../new_chat/qr_scanner_modal.dart';
import 'empty_rooms_floating_action_button.dart';

class EmptyRooms extends StatefulWidget {
  final ChatListController chatListController;
  const EmptyRooms({Key? key, required this.chatListController}) : super(key: key);

  @override
  EmptyRoomsController createState() => EmptyRoomsController();
}

class EmptyRoomsController extends State<EmptyRooms> with TickerProviderStateMixin{
  final TextEditingController controller = TextEditingController();
  Future<QueryPublicRoomsResponse>? publicRoomsResponse;
  String? lastServer;
  Timer? _coolDown;
  String? genericSearchTerm;
  List<Profile>? userDirectory = [];
  bool isLoading = false;
  String? homeServerName;
  List<pageme.HomeServer>? homeServers;
  StreamSubscription? _onSync;
  bool? showTutorialBanner = false;
  bool showAfterTutorialBanner = false;
  late final AnimationController animationController;
  Future<dynamic>? profileFuture;
  Profile? profile;

  ///DidChangeDependencies Variables
  late NavigatorState navigator;
  late L10n? l10n;
  late ThemeData theme;
  late MatrixState matrixState;
  late ScaffoldMessengerState scaffoldMessenger;

  @override
  void dispose() {
    _onSync?.cancel();
    animationController.dispose();
    super.dispose();
  }

  DateTime _lastSetState = DateTime.now();

  Future<void> dismissShowTutorialBanner() async {
    await matrixState.store.setBool(SettingKeys.showTutorialBanner, false);
    setState(() {
      showTutorialBanner = false;
    });
    if (!showAfterTutorialBanner) {
      scaffoldMessenger.showSnackBar(const SnackBar(content: Text("To take the tutorial, you can select 'Start Tutorial' in 'Settings' -> 'Additional'")));
    }
  }

  Future<void> dismissAfterTutorialBanner() async {
    await matrixState.store.setBool(SettingKeys.showAfterTutorialBanner, false);
    setState(() {
      showAfterTutorialBanner = false;
    });
  }

  void _updateView() {
    _lastSetState = DateTime.now();
    setState(() {});
  }

  Future<void> getUserDirectory() async {
    Logs().i("Get user directory");
    await fetchUsers(context);
  }

  void joinGroupAction(PublicRoomsChunk room) {
    showModalBottomSheet(
      context: context,
      builder: (c) => PublicRoomBottomSheet(
        roomAlias: room.canonicalAlias ?? room.roomId,
        outerContext: context,
        chunk: room,
      ),
    );
  }

  String? server;

  static const String _serverStoreNamespace = 'com.pageme.pageme.search.server';

  void setServer() async {
    final newServer = await showTextInputDialog(
        useRootNavigator: false,
        title: l10n!.changeTheHomeserver,
        context: context,
        okLabel: l10n!.ok,
        cancelLabel: l10n!.cancel,
        textFields: [DialogTextField(prefixText: 'https://', hintText: matrixState.client.homeserver?.host, initialText: server, keyboardType: TextInputType.url, autocorrect: false)]);
    if (newServer == null) return;
    Store().setItem(_serverStoreNamespace, newServer.single);
    setState(() {
      server = newServer.single;
    });
  }

  String? currentSearchTerm;
  List<Profile> foundProfiles = [];
  static const searchUserDirectoryLimit = 50;

  Future<void> fetchUsers(BuildContext context) async {
    Logs().i('Fetching starting.');
    isLoading = true;
    const List<String> alphabet = [
      'a',
      'b',
      'c',
      'd',
      'e',
      'f',
      'g',
      'h',
      'i',
      'j',
      'k',
      'l',
      'm',
      'n',
      'o',
      'p',
      'q',
      'r',
      's',
      't',
      'u',
      'v',
      'w',
      'x',
      'y',
      'z',
    ];
    for (final letter in alphabet) {
      currentSearchTerm = letter;
      SearchUserDirectoryResponse? response;
      try {
        response = await matrixState.client.searchUserDirectory(
          letter,
          limit: searchUserDirectoryLimit,
        );
      } catch (e, s) {
        Logs().e("$e, $s");
        continue;
      } finally {
        isLoading = false;
      }
      foundProfiles = List<Profile>.from(response?.results ?? []);
      for (Profile profile in foundProfiles) {
        print('${profile.displayName}');
        if (!userDirectory!.any((i) => isProfileEqual(i, profile))) {
          userDirectory!.add(profile);
        }
      }
    }
    setState(() {
      userDirectory = userDirectory!.toSet().toList();
      isLoading = false;
    });
    Logs().i('Fetching done.');
  }

  bool isProfileEqual(Profile a, Profile b) => a.userId == b.userId;

  final FocusNode textFieldFocus = FocusNode();

  final TextEditingController inviteController = TextEditingController();

  final formKey = GlobalKey<FormState>();

  static const Set<String> supportedSigils = {'@', '!', '#'};
  static const String prefix = 'https://matrix.to/#/';

  void submitAction([_]) async {
    inviteController.text = inviteController.text.trim();
    if (!formKey.currentState!.validate()) return;
    UrlLauncher(context, '$prefix${inviteController.text}').openMatrixToUrl();
  }

  String? validateForm(String? value) {
    if (value!.isEmpty) {
      return l10n!.pleaseEnterAMatrixIdentifier;
    }
    if (!inviteController.text.isValidMatrixId || !supportedSigils.contains(inviteController.text.sigil)) {
      return l10n!.makeSureTheIdentifierIsValid;
    }
    if (inviteController.text == matrixState.client.userID) {
      return l10n!.youCannotInviteYourself;
    }
    return null;
  }

  void inviteAction() => PageMeShare.share(
        '${FlavorConfig.deepLinkPrefix}${matrixState.client.userID}',
        context,
      );

  void openScannerAction() async {
    if (PlatformInfos.isAndroid) {
      final info = await DeviceInfoPlugin().androidInfo;
      if ((info.version.sdkInt ?? 16) < 21) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              l10n!.unsupportedAndroidVersionLong,
            ),
          ),
        );
        return;
      }
    }
    await Permission.camera.request();
    if (!context.mounted) return;
    await showModalBottomSheet(
      context: context,
      useRootNavigator: false,
      //useSafeArea: false,
      builder: (_) => const QrScannerModal(),
    );
  }

  Future<void> displayQR(BuildContext context) async {
    await showModalBottomSheet(
        context: context,
        backgroundColor: theme.backgroundColor,
        useRootNavigator: false,
        builder: (_context) {
          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: const Icon(Icons.close_outlined),
                onPressed: Navigator.of(_context).pop,
                tooltip: l10n!.close,
              ),
              title: Text("${matrixState.client.userID}"),
            ),
            body: Center(
              child: Container(
                color: Colors.white,
                child: QrImageView(
                  data: 'https://matrix.to/#/${matrixState.client.userID}',
                  version: QrVersions.auto,
                  size: min(MediaQuery.sizeOf(context).width - 16, 200),
                ),
              ),
            ),
          );
        });
  }

  void displayRoomCreationOptions() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom, top: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListView(
                    shrinkWrap: true,
                    children: [
                      NewContactTile(onTap: () {
                        Navigator.of(context).pop();
                        inviteNewContact();
                      }),
                      const NewGroupTile(fromRooms: true,)
                    ],
                  ),
                ],
              ),
            ));
  }

  void inviteNewContact() async {
    await showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      if (PlatformInfos.isMobile)
                        ListTile(
                          leading: const Icon(Icons.share_outlined),
                          title: const Text('Send your contact details'),
                          onTap: () {
                            navigator.pop();
                            inviteAction();
                          },
                        ),
                      ListTile(
                        leading: const Icon(Icons.qr_code_outlined),
                        title: const Text('Display your QR code'),
                        onTap: () {
                          navigator.pop();
                          displayQR(context);
                        },
                      ),
                      if (PlatformInfos.isMobile)
                        ListTile(
                          leading: const Icon(Icons.camera_alt_outlined),
                          title: const Text('Scan new contact\'s QR code'),
                          onTap: () {
                            navigator.pop();
                            openScannerAction();
                          },
                        ),
                      ListTile(
                        title: const Text("Invite manually with user ID"),
                        leading: const Icon(Icons.person_add_alt_outlined),
                        onTap: () {
                          navigator.pop();
                          showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            useSafeArea: true,
                            builder: (context) => Padding(
                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  ListTile(
                                    title: const Text("Enter the user ID"),
                                    subtitle: Text('For example: @john:${matrixState.client.homeserver!.host.toLowerCase().substring(7)}'),
                                    isThreeLine: false,
                                  ),
                                  Form(
                                    key: formKey,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(12.0, 8, 12, 8),
                                      child: TextFormField(
                                        controller: inviteController,
                                        autocorrect: false,
                                        autofocus: true,
                                        textInputAction: TextInputAction.go,
                                        focusNode: textFieldFocus,
                                        onFieldSubmitted: (value) {
                                          submitAction();
                                          navigator.pop();
                                        },
                                        validator: validateForm,
                                        decoration: InputDecoration(
                                          hintText: '@username:${matrixState.client.homeserver!.host.toLowerCase().substring(7)}',
                                          suffixIcon: IconButton(
                                            icon: const Icon(Icons.send_outlined),
                                            onPressed: () {
                                              navigator.pop();
                                              submitAction();
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ));
  }

  int groupComparator(String group1, String group2) {
    //Logs().i( '$group1 and $group2');
    if (group1 == homeServerName && group2 != homeServerName) {
      return -1;
    } else if (group1 != homeServerName && group2 == homeServerName) {
      return 1;
    } else {
      return group1.compareTo(group2);
    }
  }

  int itemComparator(Profile item1, Profile item2) {
    if (item1.displayName != null && item2.displayName != null) {
      return item1.displayName!.toLowerCase().compareTo(item2.displayName!.toLowerCase());
    } else if (item1.displayName != null && item2.displayName == null) {
      return item1.displayName!.toLowerCase().compareTo(item2.userId.split('@').last.toLowerCase());
    } else if (item1.displayName == null && item2.displayName != null) {
      return item1.userId.split('@').last.toLowerCase().compareTo(item2.displayName!.toLowerCase());
    } else {
      return item1.userId.split('@').last.toLowerCase().compareTo(item2.userId.split('@').last.toLowerCase());
    }
  }

  String? extractServerFromUserId(String userId) {
    // User IDs are in the format "@username:server"
    final server = userId.split(':').last; //.split('.').first.toUpperCase();
    // You can further process the server name to format it as needed
    return getServerName(server);
  }

  String? getServerName(String serverAddress) {
    if (homeServers == null) {
      return null;
    }
    if (serverAddress == "matrix.pageme.co.za" || serverAddress == "pageme.co.za") {
      return "Public";
    }
    for (final homeServer in homeServers!) {
      if (homeServer.baseUrl.contains(serverAddress) || serverAddress.contains(homeServer.baseUrl)) {
        return homeServer.description;
      }
    }
    return null;
  }

  Future<List<pageme.HomeServer>> fetchAllHomeServers() async {
    List<pageme.HomeServer>? resultHomeservers;
    try {
      resultHomeservers = await matrixState.pagemeClient.homeserver.getAllHomeServers(
        appKey: PageMeClientAppKey.appKey,
      );
    } on Exception catch (e) {
      Logs().e("Error fetching list of homeserver from serverpod.");
    }
    if (resultHomeservers == null) {
      return [];
    } else {
      return resultHomeservers;
    }
  }

  @override
  void didChangeDependencies() async {
    matrixState = Matrix.of(context);
    theme = Theme.of(context);
    l10n = L10n.of(context);
    navigator = Navigator.of(context);
    scaffoldMessenger = ScaffoldMessenger.of(context);

    super.didChangeDependencies();
    if (isBusiness()) {
      widget.chatListController.toggleIsOnBoarding(true);
      if (userDirectory!.isEmpty) {
        getUserDirectory();
      }
    }
    fetchAllHomeServers().then((value) {
      setState(() {
        homeServers = value;
      });
    });
    getShowTutorialBanner();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      controller.text = PageMeApp.router.routeInformationProvider.value.uri.queryParameters['query'] ?? '';
      final server = await Store().getItem(_serverStoreNamespace);
      if (server?.isNotEmpty ?? false) {
        this.server = server;
      }
    });

    animationController = AnimationController(vsync: this);
  }

  void getShowTutorialBanner() async {
    /// Uncomment both line to test the tutorial banners
    //await matrixState.store.setBool(SettingKeys.showTutorialBanner, true);
    //await matrixState.store.setBool(SettingKeys.showAfterTutorialBanner, false);
    setState(() {
      showTutorialBanner = matrixState.store.getBool(SettingKeys.showTutorialBanner) ?? true;
    });
    setState(() {
      showAfterTutorialBanner= matrixState.store.getBool(SettingKeys.showAfterTutorialBanner) ?? false;
    });
    if (showAfterTutorialBanner){
      await dismissShowTutorialBanner();
    }
    Logs().v("showTutorialBanner - $showTutorialBanner");
  }

  @override
  Widget build(BuildContext context) {
    final client = matrixState.client;

    profileFuture ??= client
        .getProfileFromUserId(
      client.userID!,
      cache: true,
      getFromRooms: true,
    )
        .then((p) {
      if (mounted) setState(() => profile = p);
      return p;
    });
    homeServerName = getServerName(client.homeserver!.host);
    _onSync ??= client.onSync.stream.listen((_) {
      if (DateTime.now().millisecondsSinceEpoch - _lastSetState.millisecondsSinceEpoch < 1000) {
        _coolDown?.cancel();
        _coolDown = Timer(const Duration(milliseconds: 1000), _updateView);
      } else {
        _updateView();
      }
    });

    return EmptyRoomsView(controller: this);
  }
}
