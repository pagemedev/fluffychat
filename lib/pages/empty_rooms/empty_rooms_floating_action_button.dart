
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pages/empty_rooms/empty_rooms.dart';
import 'package:shimmer_animation/shimmer_animation.dart';

import '../../config/flavor_config.dart';
import '../chat_list/chat_list.dart';

class EmptyRoomsFloatingActionButton extends StatelessWidget {
  final EmptyRoomsController controller;
  const EmptyRoomsFloatingActionButton({
    Key? key, required this.controller
  }) : super(key: key);

  void _onPressed(BuildContext context) {
    if (isBusiness()){
      context.go('/rooms/newchat');
    } else{
      controller.displayRoomCreationOptions();
    }
  }


  Widget icon(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 2, left: 0),
      child: SvgPicture.asset(
        'assets/pageme_chat_bubble.svg',
        width: 20,
        height: 20,
        color: isBusiness()
            ? PageMeThemes.isDarkMode(context)
            ? Theme.of(context).colorScheme.onPrimaryContainer
            : Theme.of(context).colorScheme.onPrimary
            : PageMeThemes.isDarkMode(context)
            ? Theme.of(context).colorScheme.onPrimaryContainer
            : Theme.of(context).colorScheme.onPrimary,
      ),
    );
  }

  String getLabel(BuildContext context) {
    return L10n.of(context)!.newChat;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(16),
      elevation: 6,
      child: Shimmer(
        duration: const Duration(seconds: 2), //Default value
        interval: const Duration(seconds: 2), //Default value: Duration(seconds: 0)
        color: Colors.white, //Default value
        colorOpacity: 0.1, //Default value
        enabled: true, //Default value
        direction: const ShimmerDirection.fromLTRB(),
        child: AnimatedContainer(
            duration: const Duration(milliseconds: 200),
            curve: Curves.easeInOut,
            width: 144,
            child: FloatingActionButton.extended(
              onPressed: () => _onPressed(context),
              elevation: 4,
              icon: icon(context),
              heroTag: 'FAB',
              label: Text(
                getLabel(context),
                overflow: TextOverflow.fade,
              ),
            )),
      ),
    );
  }
}

