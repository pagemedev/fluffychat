import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config/flavor_config.dart';
import '../../config/setting_keys.dart';
import '../../widgets/fake_chat_list_item.dart';
import '../../widgets/tutorial_manager.dart';
import 'empty_rooms.dart';

class PostTutorialBanner extends StatelessWidget {
  const PostTutorialBanner({
    super.key,
    required this.controller,
    required this.context,
  });

  final EmptyRoomsController controller;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Padding(
      key: UniqueKey(),
      padding: const EdgeInsets.all(8.0),
      child: Card(
        color: Theme.of(context).colorScheme.background,
        elevation: 4,
        child: Stack(
          children: [
            Column(
              key: const ValueKey(null),
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                FakeChatListItem(
                  title: "Thank you ${controller.profile?.displayName ?? controller.profile?.userId ?? ""}",
                  subtitle: "To take the tutorial again, you can select 'Start Tutorial' in 'Settings' -> 'Additional'\n\nFeel free to close this banner.",
                  avatarAssetPath: isBusiness() ? "assets/pageme_business_logo_rounded.png" : 'assets/pageme_public_logo.png',
                ),
              ],
            ),
            Positioned(
              top: 0,
              right: 0,
              child: IconButton(
                onPressed: controller.dismissAfterTutorialBanner,
                icon: const Icon(Icons.close),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TutorialBanner extends StatelessWidget {
  const TutorialBanner({
    super.key,
    required this.controller,
    required this.context,
  });

  final EmptyRoomsController controller;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Padding(
      key: UniqueKey(),
      padding: const EdgeInsets.all(8.0),
      child: Card(
        color: Theme.of(context).colorScheme.background,
        elevation: 4,
        child: Stack(
          children: [
            Column(
              key: const ValueKey(null),
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                FakeChatListItem(
                    title: "Welcome ${controller.profile?.displayName ?? controller.profile?.userId ?? ""}",
                    subtitle: "Take a tutorial of the app with our guided walkthrough.",
                    avatarAssetPath: isBusiness() ? "assets/pageme_business_logo_rounded.png" : 'assets/pageme_public_logo.png'),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 0.0, left: 12.5, right: 10, bottom: 8),
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          backgroundColor: Theme.of(context).colorScheme.primary,
                          side: const BorderSide(color: Colors.transparent),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        onPressed: () => TutorialManager(
                            context: context,
                            onFinish: () async {
                              await SharedPreferences.getInstance()..setBool(SettingKeys.showAfterTutorialBanner, true);
                            }).showTutorial(),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "Start tour",
                              style: TextStyle(color: Theme.of(context).colorScheme.onPrimary, fontWeight: FontWeight.w600, fontSize: 18),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            const Icon(
                              Icons.play_arrow,
                              size: 18,
                              color: Colors.black,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Positioned(
              top: 0,
              right: 0,
              child: IconButton(
                onPressed: () async => await controller.dismissShowTutorialBanner(),
                icon: const Icon(Icons.close),
              ),
            )
          ],
        ),
      ),
    );
  }
}