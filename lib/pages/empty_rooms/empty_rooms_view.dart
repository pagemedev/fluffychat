import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:pageMe/pages/chat_list/chat_list_item.dart';
import 'package:pageMe/pages/empty_rooms/empty_rooms.dart';
import 'package:pageMe/pages/empty_rooms/empty_rooms_item.dart';
import 'package:pageMe/pages/chat_list/search_title.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/empty_rooms/tutorial_banners.dart';
import 'package:pageMe/pages/user_directory/server_title.dart';
import 'package:pageMe/utils/global_key_provider.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/widgets/fake_chat_list_item.dart';
import 'package:pageMe/widgets/tutorial_manager.dart';
import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/avatar.dart';
import '../../widgets/matrix.dart';
import 'package:grouped_list/grouped_list.dart';
import 'dart:math' as math;

import 'empty_rooms_floating_action_button.dart';
import '../chat_list/new_chat_floating_action_button.dart';
class EmptyRoomsView extends StatelessWidget {
  final EmptyRoomsController controller;
  const EmptyRoomsView({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isPageMeBusiness = isBusiness();
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        if (controller.showTutorialBanner ?? false) TutorialBanner(controller: controller, context: context),
        if (controller.showAfterTutorialBanner) PostTutorialBanner(controller: controller, context: context),
        if (!(controller.showTutorialBanner ?? false) && !controller.showAfterTutorialBanner)
        Flexible(
          flex: 1,
          fit: FlexFit.loose,
          child: Container(),
        ),
        if (!(controller.showTutorialBanner ?? false) && !controller.showAfterTutorialBanner)
          Column(
            children: [
              Padding(
                key: UniqueKey(),
                padding: const EdgeInsets.only(top: 18.0),
                child: Text(
                  "Looks empty...\nLet's fill this with chats!",
                  style: TextStyle(color: Theme.of(context).colorScheme.onBackground, fontWeight: FontWeight.w500, fontSize: 22),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),

        Flexible(
          fit: FlexFit.loose,
          flex: 20,
          child: LottieBuilder.asset(
            isPageMeBusiness ? 'assets/empty_chat_business.json' : 'assets/empty_chat_public.json',
            repeat: true,
            fit: BoxFit.fitHeight,
            controller: controller.animationController,
            frameRate: FrameRate(60),
            onLoaded: (composition) {
              controller.animationController
                ..duration = composition.duration
                ..repeat(period: const Duration(seconds: 5));
            },
          ),
        ),
        Flexible(
          flex: 1,
          fit: FlexFit.loose,
          child: Container(),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 45.0),
          child: Text(
            isPageMeBusiness
                ? "Start by inviting your colleagues or exploring those already on your server"
                : "Start by inviting your friends and family to chat with you securely.",
            style: TextStyle(
              color: Theme.of(context).colorScheme.onBackground,
              fontWeight: FontWeight.w400,
              fontSize: 16,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Flexible(
          flex: 5,
          child: Padding(
            padding: const EdgeInsets.only(top: 12, bottom: 12),
            child: Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationX(math.pi),
              child: SvgPicture.asset(
                'assets/arrow.svg',
                color: Theme.of(context).colorScheme.primaryContainer,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: EmptyRoomsFloatingActionButton(
            key: GlobalKeyProvider.fabButton,
            controller: controller,
            //key: GlobalKeyProvider.fabButton,
          ),
        )
      ],
    );
  }
}





