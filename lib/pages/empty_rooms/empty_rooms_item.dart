import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/chat_list/chat_list.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/client_presence_extension.dart';
import 'package:pageMe/utils/string_color.dart';
import '../../config/themes.dart';
import '../../utils/logger_functions.dart';
import '../../widgets/avatar.dart';
import '../../widgets/matrix.dart';

class EmptyRoomsItem extends StatefulWidget {
  final Profile profile;
  final ChatListController chatListController;
  const EmptyRoomsItem({Key? key, required this.profile, required this.chatListController}) : super(key: key);

  @override
  State<EmptyRoomsItem> createState() => _EmptyRoomsItemState();
}

class _EmptyRoomsItemState extends State<EmptyRoomsItem> {
  bool _isCreating = false;

  bool get isCreated {
    final client = Matrix.of(context).client;
    return client.contactList.where((p) => p.userid.toLowerCase().contains(widget.profile.userId)).toList().isNotEmpty;
  }

  Future<void> createRoom(String userId) async {
    setState(() {
      _isCreating = true;
    });
    final client = Matrix.of(context).client;
    try {
      final roomId = await client.startDirectChat(userId);
    } on Exception catch (e) {
      loggerError(logMessage: e.toString());
    } finally {
      setState(() {
        _isCreating = false;
      });
     widget.chatListController.updateState();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.background : Theme.of(context).colorScheme.secondaryContainer,
      width: MediaQuery.sizeOf(context).width,
      child: ListTile(
        leading: Avatar(
          mxContent: widget.profile.avatarUrl,
          name: widget.profile.displayName ?? widget.profile.userId,
          //size: 24,
        ),
        trailing: IconButton(
          onPressed: () async {
            if (isCreated) {
              return;
            } else {
              await createRoom(widget.profile.userId);
            }
          },
          color: Theme.of(context).colorScheme.primary,
          icon: _isCreating
              ? const CircularProgressIndicator()
              : isCreated
                  ? const Icon(Icons.done, color: Colors.green,)
                  :  Icon(Icons.person_add,),
          iconSize: 25,
        ),
        title: Text(
          widget.profile.displayName ?? widget.profile.userId.localpart!,
          style: const TextStyle(),
          maxLines: 1,
        ),
        subtitle: Text(
          widget.profile.userId,
          maxLines: 1,
          style: const TextStyle(
            fontSize: 12,
          ),
        ),
      ),
    );
  }
}
