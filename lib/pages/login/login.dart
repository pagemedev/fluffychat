import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:collection/collection.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/keys/pageme_client_appkey.dart';
import 'package:pageMe/utils/localized_exception_extension.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:pageme_client/pageme_client.dart';

import '../../config/flavor_config.dart';
import '../../utils/platform_infos.dart';
import 'login_view.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  LoginController createState() => LoginController();
}

class LoginController extends State<Login> with AutomaticKeepAliveClientMixin {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  String? usernameError;
  String? passwordError;
  String? homeServerError;
  bool loading = false;
  bool showPassword = false;
  bool isKeyBoardOpen = false;

  void toggleShowPassword() => setState(() => showPassword = !showPassword);

  @override
  void didChangeDependencies() async {
    if (isPublic()) {
      await checkPublicServerAction();
    }
    super.didChangeDependencies();
  }

  void onChanged() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
  }

  void setKeyboard() {
    setState(() {
      isKeyBoardOpen = true;
    });
  }

  Future<void> checkPublicServerAction() async {
    setState(() {
      loading = true;
    });
    final matrix = Matrix.of(context);
    try {
      final List<HomeServer> homeServers = await matrix.pagemeClient.homeserver.getAllHomeServers(appKey: PageMeClientAppKey.appKey);
      final HomeServer? homeServer = homeServers.singleWhereOrNull((server) => server.baseUrl == FlavorConfig.publicServers[0].tryGet('address')!); //FlavorConfig.publicServers[0].tryGet('address')!;
      if (homeServer != null) {
        matrix.homeServer = homeServer;
      }
      final String homeServerAddress = homeServer?.baseUrl ?? FlavorConfig.publicServers[0].tryGet('address')!;
      var homeserver = Uri.parse(homeServerAddress);
      if (homeserver.scheme.isEmpty) {
        homeserver = Uri.https(homeServerAddress, '');
      }

      matrix.loginHomeServerSummary = await matrix.getLoginClient().checkHomeserver(homeserver);
      final ssoSupported = matrix.loginHomeServerSummary!.loginFlows.any((flow) => flow.type == 'm.login.sso');

      try {
        await Matrix.of(context).getLoginClient().register();
        matrix.loginRegistrationSupported = true;
      } on MatrixException catch (e) {
        matrix.loginRegistrationSupported = e.requireAdditionalAuthentication;
      }

      if (!ssoSupported && matrix.loginRegistrationSupported == false) {
        // Server does not support SSO or registration. We can skip to login page:
      }
    } catch (e) {
      setState(() => homeServerError = (e).toLocalizedString(context));
      return;
    } finally {
      if (mounted) {
        setState(() => loading = false);
      }
    }
  }

  void previousPage() {
    if (mounted) {
      setState(() {
        context.pop();
/*        widget.pageViewController.previousPage(
          duration: const Duration(milliseconds: 500),
          curve: Curves.fastLinearToSlowEaseIn,
        )*/
      });
    }
  }

  void createAccount() {
    if (mounted) {
      setState(() {
        if (PlatformInfos.canSubscribe) {
          context.go('/login/subscriptions');
        } else {
          Matrix.of(context).navigatorContext = context;
          context.go('/login/signup');
        }
/*        widget.pageViewController.previousPage(
          duration: const Duration(milliseconds: 500),
          curve: Curves.fastLinearToSlowEaseIn,
        )*/
      });
    }
  }

  void login([_]) async {
    final matrix = Matrix.of(context);
    if (usernameController.text.isEmpty) {
      setState(() => usernameError = L10n.of(context)!.pleaseEnterYourUsername);
    } else {
      setState(() => usernameError = null);
    }
    if (passwordController.text.isEmpty) {
      setState(() => passwordError = L10n.of(context)!.pleaseEnterYourPassword);
    } else {
      setState(() => passwordError = null);
    }

    if (usernameController.text.isEmpty || passwordController.text.isEmpty) {
      return;
    }

    setState(() => loading = true);
    LoginResponse loginResponse;
    try {
      final username = usernameController.text;
      AuthenticationIdentifier identifier;
      if (username.isEmail) {
        identifier = AuthenticationThirdPartyIdentifier(
          medium: 'email',
          address: username,
        );
      } else if (username.isPhoneNumber) {
        identifier = AuthenticationThirdPartyIdentifier(
          medium: 'msisdn',
          address: username,
        );
      } else {
        identifier = AuthenticationUserIdentifier(user: username);
      }
      loginResponse = await matrix.getLoginClient().login(LoginType.mLoginPassword,
          identifier: identifier,
          // To stay compatible with older server versions
          // ignore: deprecated_member_use
          user: identifier.type == AuthenticationIdentifierTypes.userId ? username : null,
          password: passwordController.text,
          initialDeviceDisplayName: await PlatformInfos.clientName);
    } on MatrixException catch (exception) {
      setState(() => passwordError = exception.errorMessage);
      return setState(() => loading = false);
    } catch (exception) {
      setState(() => passwordError = exception.toString());
      return setState(() => loading = false);
    }

    if (mounted) setState(() => loading = false);
  }

  Timer? _coolDown;

  void checkWellKnownWithCoolDown(String userId) async {
    _coolDown?.cancel();
    _coolDown = Timer(
      const Duration(seconds: 1),
      () => _checkWellKnown(userId),
    );
  }

  void _checkWellKnown(String userId) async {
    setState(() => usernameError = null);
    if (!userId.isValidMatrixId) return;
    try {
      final oldHomeserver = Matrix.of(context).getLoginClient().homeserver;
      var newDomain = Uri.https(userId.domain!, '');
      Matrix.of(context).getLoginClient().homeserver = newDomain;
      DiscoveryInformation? wellKnownInformation;
      try {
        wellKnownInformation = await Matrix.of(context).getLoginClient().getWellknown();
        if (wellKnownInformation.mHomeserver.baseUrl.toString().isNotEmpty) {
          newDomain = wellKnownInformation.mHomeserver.baseUrl;
        }
      } catch (_) {
        // do nothing, newDomain is already set to a reasonable fallback
      }
      if (newDomain != oldHomeserver) {
        await showFutureLoadingDialog(
          context: context,
          // do nothing if we error, we'll handle it below
          future: () => Matrix.of(context).getLoginClient().checkHomeserver(newDomain).catchError((e) {}),
        );
        if (Matrix.of(context).getLoginClient().homeserver == null) {
          Matrix.of(context).getLoginClient().homeserver = oldHomeserver;
          // okay, the server we checked does not appear to be a matrix server
          Logs().v('$newDomain is not running a homeserver, asking to use $oldHomeserver');
          final dialogResult = await showOkCancelAlertDialog(
            context: context,
            useRootNavigator: false,
            message: L10n.of(context)!.noMatrixServer(newDomain, oldHomeserver!),
            okLabel: L10n.of(context)!.ok,
            cancelLabel: L10n.of(context)!.cancel,
          );
          if (dialogResult == OkCancelResult.ok) {
            setState(() => usernameError = null);
          } else {
            Navigator.of(context, rootNavigator: false).pop();
            return;
          }
        }
        setState(() => usernameError = null);
      } else {
        setState(() => Matrix.of(context).getLoginClient().homeserver = oldHomeserver);
      }
    } catch (e) {
      setState(() => usernameError = e.toString());
    }
  }

  void passwordForgotten() async {
    final input = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.passwordForgotten,
      message: 'Enter your registered email address',
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      fullyCapitalizedForMaterial: false,
      textFields: [
        DialogTextField(
          initialText: usernameController.text.isEmail ? usernameController.text : '',
          hintText: L10n.of(context)!.enterAnEmailAddress,
          keyboardType: TextInputType.emailAddress,
        ),
      ],
    );
    if (input == null) return;
    final clientSecret = DateTime.now().millisecondsSinceEpoch.toString();
    final response = await showFutureLoadingDialog(
      context: context,
      future: () => Matrix.of(context).getLoginClient().requestTokenToResetPasswordEmail(
            clientSecret,
            input.single,
            sendAttempt++,
          ),
    );
    if (response.error != null) return;
    final password = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.passwordForgotten,
      message: L10n.of(context)!.chooseAStrongPassword,
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      fullyCapitalizedForMaterial: false,
      textFields: [
        const DialogTextField(
          hintText: '******',
          obscureText: true,
          minLines: 1,
          maxLines: 1,
        ),
      ],
    );
    if (password == null) return;
    final ok = await showOkAlertDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.weSentYouAnEmail,
      message: L10n.of(context)!.pleaseClickOnLink,
      okLabel: L10n.of(context)!.iHaveClickedOnLink,
      fullyCapitalizedForMaterial: false,
    );
    if (ok != OkCancelResult.ok) return;
    final data = <String, dynamic>{
      'new_password': password.single,
      'logout_devices': false,
      "auth": AuthenticationThreePidCreds(
        type: AuthenticationTypes.emailIdentity,
        threepidCreds: ThreepidCreds(
          sid: response.result!.sid,
          clientSecret: clientSecret,
        ),
      ).toJson(),
    };
    final success = await showFutureLoadingDialog(
      context: context,
      future: () => Matrix.of(context).getLoginClient().request(
            RequestType.POST,
            '/client/r0/account/password',
            data: data,
          ),
    );
    if (success.error == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(L10n.of(context)!.passwordHasBeenChanged)));
      usernameController.text = input.single;
      passwordController.text = password.single;
      login();
    }
  }

  static int sendAttempt = 0;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return WillPopScope(
      onWillPop: () async {
        previousPage();
        return false;
      },
      child: LoginView(this),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

extension on String {
  static final RegExp _phoneRegex = RegExp(r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$');
  bool get isEmail => EmailValidator.validate(this);
  bool get isPhoneNumber => _phoneRegex.hasMatch(this);
}
