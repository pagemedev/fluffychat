import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_svg/svg.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/pdf_viewer.dart';
import 'package:pageMe/utils/url_launcher.dart';
import 'package:pageMe/widgets/keyboard_aware_container.dart';
import 'package:pageme_client/pageme_client.dart';

import '../../utils/conditonal_wrapper.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/matrix.dart';
import 'login.dart';

class LoginView extends StatelessWidget {
  final LoginController controller;
  static const double assetWidth = 350;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 19.0);
  static const titleStyle = TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 0);
  static const double maxHeightFourWords = 155;
  const LoginView(this.controller, {Key? key}) : super(key: key);

  Widget buildTitleSVGWidget(String title, String asset, [double width = 250, Color? color]) {
    return Column(
      children: [
        buildSVG(asset, width, color),
        const SizedBox(
          height: 20,
        ),
        Text(
          title,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }

  Widget buildTitleImageWidget(String title, String asset, [double? width = 250, Color? color]) {
    return Column(
      children: [
        buildImage(asset, width, color),
        const SizedBox(
          height: 20,
        ),
        Text(
          title,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }

  Widget buildImage(String assetName, double? width, Color? color) {
    return SizedBox(
      width: width,
      height: width! * (712 / 279),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: globalHeaderHeight),
          child: Image.asset('assets/$assetName', width: width, color: color),
        ),
      ),
    );
  }

  Widget buildSVG(String assetName, [double width = assetWidth, Color? color]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight),
        child: SvgPicture.asset('assets/$assetName', width: width, color: color),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: KeyboardVisibilityProvider(
        child: Scaffold(
          backgroundColor: PageMeThemes.isDarkMode(context) ? null : Theme.of(context).colorScheme.background,
          appBar: !PageMeThemes.isColumnMode(context)
              ? AppBar(
                  backgroundColor: Theme.of(context).colorScheme.background,
                  elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
                  toolbarHeight: 100,
                  centerTitle: true,
                  title: Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 40),
                        child: SizedBox(
                          //height: globalHeaderHeight,
                          width: MediaQuery.sizeOf(context).width / 3,
                          child: SvgPicture.asset(
                            (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                    ],
                  ),
                  bottom: PageMeThemes.isColumnMode(context)
                      ? PreferredSize(
                          preferredSize: const Size.fromHeight(2.0),
                          child: Container(
                              height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
                              color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black //Theme.of(context).colorScheme.primary,
                              ),
                        )
                      : null,
                )
              : PreferredSize(preferredSize: Size.zero, child: Container()),
          body: SafeArea(
            bottom: true,
            child: LayoutBuilder(builder: (context, constraints) {
              return Stack(
                children: [
                  if (PageMeThemes.isColumnMode(context))
                    Image.asset(
                      Theme.of(context).brightness == Brightness.light ? 'assets/light_backgorund_alt.png' : 'assets/dark_background_alt.png',
                      width: double.infinity,
                      height: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  Padding(
                    padding: PageMeThemes.isColumnMode(context) ? const EdgeInsets.all(8.0) : const EdgeInsets.all(0),
                    child: Center(
                      child: Material(
                        color: Colors.transparent,
                        elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
                        borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                        child: Container(
                          width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                            color: PageMeThemes.isColumnMode(context) ? Theme.of(context).colorScheme.background.withOpacity(0.85) : Theme.of(context).colorScheme.background,
                          ),
                          child: Padding(
                            padding: PageMeThemes.isColumnMode(context) ? const EdgeInsets.symmetric(horizontal: 24.0) : const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                if (PageMeThemes.isColumnMode(context))
                                  Padding(
                                    padding: const EdgeInsets.only(top: 0.0),
                                    child: SizedBox(
                                      width: PageMeThemes.columnWidth * 1.5 / 3,
                                      height: 50,
                                      child: SvgPicture.asset(
                                        (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                                        fit: BoxFit.fitWidth,
                                      ),
                                    ),
                                  ),
                                Container(
                                  padding: bodyPadding,
                                  child: const Text(
                                    'Login to your account.',
                                    style: bodyStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                KeyboardAwareContainer(maxHeight: 32, minHeight: PageMeThemes.isColumnMode(context) ? 32 : 0),
                                KeyboardAwareContainer(
                                  maxHeight: PageMeThemes.isColumnMode(context) ? 255 : (constraints.maxWidth - 16) * (279 / 712),
                                  minHeight: PageMeThemes.isColumnMode(context) ? 32 : 0,
                                  child: LayoutBuilder(builder: (context, constraints) {
                                    return IntrinsicWidth(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          //buildImage('page_me_four_words.png', 87.5, PageMeThemes.whiteBlackColor(context)),
                                          Image.asset(
                                            'assets/page_me_four_words.png',
                                            //height: PageMeThemes.isColumnMode(context) ? null : max(constraints.maxHeight - 16, 0),
                                            color: PageMeThemes.whiteBlackColor(context),
                                            width: constraints.maxWidth - 16,
                                            fit: BoxFit.fitWidth,
                                          ),
                                          SizedBox(
                                            height: constraints.maxHeight * 5 / maxHeightFourWords,
                                          ),
                                          Divider(
                                            color: Theme.of(context).colorScheme.primaryContainer,
                                            indent: constraints.maxHeight * 5 / maxHeightFourWords,
                                            endIndent: constraints.maxHeight * 5 / maxHeightFourWords,
                                            thickness: constraints.maxHeight * 5 / maxHeightFourWords,
                                            height: constraints.maxHeight / maxHeightFourWords,
                                          ),
                                        ],
                                      ),
                                    );
                                  }),
                                ),
                                Flexible(
                                  flex: 1,
                                  child: FocusScope(
                                    autofocus: true,
                                    child: AutofillGroup(
                                      child: ListView(
                                        shrinkWrap: true,
                                        children: <Widget>[
                                          KeyboardAwareContainer(maxHeight: 0, minHeight: PageMeThemes.isColumnMode(context) ? 50 : 0),
                                          Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: TextField(
                                              readOnly: controller.loading,
                                              autocorrect: false,
                                              autofocus: false,
                                              onChanged: controller.checkWellKnownWithCoolDown,
                                              controller: controller.usernameController,
                                              enableSuggestions: true,
                                              textInputAction: TextInputAction.next,
                                              keyboardType: TextInputType.emailAddress,
                                              style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
                                              autofillHints: controller.loading ? null : const [AutofillHints.username],
                                              decoration: PageMeThemes.loginTextFieldDecoration(
                                                prefixIcon: Icon(
                                                  Icons.account_box_outlined,
                                                  color: Theme.of(context).colorScheme.onBackground,
                                                ),
                                                errorText: controller.usernameError,
                                                hintText: "Username",
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: ConditionalKeyBoardShortcuts(
                                              enabled: !PlatformInfos.isMobile,
                                              keysToPress: {LogicalKeyboardKey.enter},
                                              onKeysPressed: controller.loading ? null : () => controller.login(),
                                              helpLabel: L10n.of(context)!.submit,
                                              child: TextField(
                                                readOnly: controller.loading,
                                                autocorrect: false,
                                                enableSuggestions: true,
                                                autofillHints: controller.loading ? null : [AutofillHints.password],
                                                controller: controller.passwordController,
                                                textInputAction: TextInputAction.next,
                                                obscureText: !controller.showPassword,
                                                onChanged: (_) => controller.onChanged(),
                                                style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
                                                decoration: PageMeThemes.loginTextFieldDecoration(
                                                  prefixIcon: Icon(
                                                    Icons.lock_outlined,
                                                    color: Theme.of(context).colorScheme.onBackground,
                                                  ),
                                                  errorText: controller.passwordError,
                                                  suffixIcon: FocusTraversalGroup(
                                                    descendantsAreFocusable: false,
                                                    descendantsAreTraversable: false,
                                                    child: IconButton(
                                                      autofocus: false,
                                                      tooltip: L10n.of(context)!.showPassword,
                                                      icon: Icon(
                                                        controller.showPassword ? Icons.visibility_off_outlined : Icons.visibility_outlined,
                                                        color: Theme.of(context).colorScheme.onBackground,
                                                      ),
                                                      onPressed: controller.toggleShowPassword,
                                                    ),
                                                  ),
                                                  hintText: L10n.of(context)!.password,
                                                ),
                                              ),
                                            ),
                                          ),
                                          if (!isBusiness())
                                            Column(
                                              mainAxisSize: MainAxisSize.min,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    const Text("Don't have an account?"),
                                                    TextButton(
                                                      onPressed: () {
                                                        controller.createAccount();
                                                        FocusManager.instance.primaryFocus?.unfocus();
                                                      },
                                                      child: const Text(
                                                        'Create an account',
                                                        style: TextStyle(
                                                          fontWeight: FontWeight.bold,
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                const Text(
                                                  "···",
                                                  style: TextStyle(fontSize: 30, height: 0.2),
                                                ),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    const Text("Forgotten password?"),
                                                    TextButton(
                                                      onPressed: () {
                                                        controller.passwordForgotten();
                                                        FocusManager.instance.primaryFocus?.unfocus();
                                                      },
                                                      child: const Text(
                                                        'Reset password',
                                                        style: TextStyle(
                                                          fontWeight: FontWeight.bold,
                                                          color: Colors.redAccent,
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                controller.homeServerError != null
                                    ? Text(
                                        controller.homeServerError!,
                                      )
                                    : const SizedBox(),
                                Padding(
                                  padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
                                  child: SizedBox(
                                    width: double.infinity,
                                    height: 60,
                                    child: ElevatedButton(
                                        onPressed: controller.loading ? null : () => controller.login(),
                                        style: ButtonStyle(
                                          shadowColor: MaterialStateProperty.resolveWith(
                                            (states) {
                                              return Colors.black;
                                            },
                                          ),
                                          elevation: MaterialStateProperty.resolveWith(
                                            (states) {
                                              if (controller.usernameController.text.isEmpty || controller.passwordController.text.isEmpty) {
                                                return 0;
                                              }
                                              return 2;
                                            },
                                          ),
                                          backgroundColor: MaterialStateProperty.resolveWith(
                                            (states) {
                                              if (controller.usernameController.text.isEmpty || controller.passwordController.text.isEmpty) {
                                                return Theme.of(context).colorScheme.secondaryContainer;
                                              }
                                              return Theme.of(context).colorScheme.primaryContainer;
                                            },
                                          ),
                                        ),
                                        child: controller.loading
                                            ? const LinearProgressIndicator(
                                                color: Colors.white,
                                                backgroundColor: Colors.black54,
                                              )
                                            : Text(L10n.of(context)!.login,
                                                style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: (controller.usernameController.text.isEmpty || controller.passwordController.text.isEmpty
                                                      ? Theme.of(context).colorScheme.onSecondaryContainer
                                                      : Theme.of(context).colorScheme.onPrimaryContainer),
                                                ))),
                                  ),
                                ),
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Padding(
                                      padding: EdgeInsets.only(left: 12.0, right: 12, bottom: 0),
                                      child: Text(
                                        "By registering or logging in you agree to the",
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          SizedBox(
                                            height: 20,
                                            child: TextButton(
                                              onPressed: () async {
                                                await openEULA(context);
                                              },
                                              style: TextButton.styleFrom(
                                                padding: EdgeInsets.zero,
                                                minimumSize: Size.zero,
                                              ),
                                              child: const Text(
                                                "EULA",
                                              ),
                                            ),
                                          ),
                                          const Text(
                                            "and ",
                                          ),
                                          SizedBox(
                                            height: 20,
                                            child: TextButton(
                                              onPressed: () async {
                                                await openPrivacyPolicy(context);
                                              },
                                              style: TextButton.styleFrom(
                                                padding: EdgeInsets.zero,
                                                minimumSize: Size.zero,
                                              ),
                                              child: const Text(
                                                "Privacy Policy",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }),
          ),
        ),
      ),
    );
  }

  Future<void> openPrivacyPolicy(BuildContext context) async {
    final String? privacyPolicy;
    final navigator = Navigator.of(context);
    final matrixState = Matrix.of(context);
    final HomeServer? homeServer = matrixState.homeServer; // ?? await matrixState.pagemeClient.homeserver.getHomeServer(4, appKey: PageMeClientAppKey.appKey);
    if (homeServer == null) {
      Logs().v("Home server is not set in MatrixState");
      if (!context.mounted) {
        Logs().v("context is not mounted");
        return;
      }
      await UrlLauncher(context, FlavorConfig.privacyUrl).launchUrl();
      return;
    }
    privacyPolicy = homeServer.privacyPolicy;
    if (privacyPolicy == null) {
      Logs().v("Privacy policy not set for home server.");
      if (!context.mounted) {
        Logs().v("context is not mounted");
        return;
      }
      await UrlLauncher(context, FlavorConfig.privacyUrl).launchUrl();
      return;
    }
    await navigator.push(
      MaterialPageRoute(
        builder: (_) {
          return PdfView(pdfLink: privacyPolicy!, pdfName: 'Privacy Policy');
        },
      ),
    );
    return;
  }

  Future<void> openEULA(BuildContext context) async {
    final String? eula;
    final navigator = Navigator.of(context);
    final matrixState = Matrix.of(context);
    final HomeServer? homeServer = matrixState.homeServer;
    if (homeServer == null) {
      if (!context.mounted) return;
      await UrlLauncher(context, FlavorConfig.eula).launchUrl();
      return;
    }
    eula = homeServer.jurisdiction;
    if (eula == null) {
      if (!context.mounted) return;
      await UrlLauncher(context, FlavorConfig.eula).launchUrl();
      return;
    }
    await navigator.push(
      MaterialPageRoute(
        builder: (_) {
          return PdfView(pdfLink: eula!, pdfName: 'End User License Agreement');
        },
      ),
    );
    return;
  }
}
