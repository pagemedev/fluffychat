import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import 'package:matrix/matrix.dart';

import 'package:pageMe/pages/image_viewer/image_viewer_view.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/widgets/matrix.dart';
import '../../cubits/file_handler/file_handler_bloc.dart';
import '../../utils/matrix_sdk_extensions.dart/event_extension.dart';

class ImageViewer extends StatefulWidget {
  final Event event;

  const ImageViewer(this.event, {Key? key}) : super(key: key);

  @override
  ImageViewerController createState() => ImageViewerController();
}

class ImageViewerController extends State<ImageViewer> {
  /// Forward this image to another room.
  void forwardAction() {
    final Map<String, dynamic> shareEvent;
    shareEvent = widget.event.content;
    BlocProvider.of<FileHandlerBloc>(context).add(ShareEventAdditionRequested(shareEvents: [shareEvent]));
    context.go('/rooms');
  }

  /// Save this file with a system call.
  Future<void> saveFileAction(BuildContext context) async => widget.event.saveFile(context);

  /// Save this file with a system call.
  void shareFileAction(BuildContext context) => widget.event.shareFile(context);

  static const maxScaleFactor = 1.5;

  /// Go back if user swiped it away
  void onInteractionEnds(ScaleEndDetails endDetails) {
    if (PlatformInfos.usesTouchscreen == false) {
      if (endDetails.velocity.pixelsPerSecond.dy > MediaQuery.sizeOf(context).height * maxScaleFactor) {
        Navigator.of(context, rootNavigator: false).pop();
      }
    }
  }

  @override
  Widget build(BuildContext context) => ImageViewerView(this);
}
