import 'dart:typed_data';

import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'package:pageMe/pages/chat/events/image_bubble.dart';
import 'package:pageMe/utils/platform_infos.dart';
import '../../widgets/mxc_image.dart';
import 'image_viewer.dart';

class ImageViewerBytesView extends StatelessWidget {
  final Uint8List image;
  static const maxScaleFactor = 1.5;
  const ImageViewerBytesView( {Key? key, required this.image}) : super(key: key);

  void onInteractionEnds(ScaleEndDetails endDetails, BuildContext context) {
    if (PlatformInfos.usesTouchscreen == false) {
      if (endDetails.velocity.pixelsPerSecond.dy >
          MediaQuery.sizeOf(context).height * maxScaleFactor) {
        Navigator.of(context, rootNavigator: false).pop();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: Navigator.of(context).pop,
          color: Colors.white,
          tooltip: L10n.of(context)!.close,
        ),
        backgroundColor: const Color(0x44000000),
      ),
      body: InteractiveViewer(
        minScale: 1.0,
        maxScale: 10.0,
        onInteractionEnd:(details) => onInteractionEnds(details, context),
        child: Center(
          child: Hero(
            tag: image.toString(),
            child: Image.memory(
              image,
              fit: BoxFit.fill,
              isAntiAlias: true,
              filterQuality: FilterQuality.none,
            ),
          ),
        ),
      ),
    );
  }
}



