import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/localized_exception_extension.dart';
import 'package:pageMe/utils/size_string.dart';

import '../../utils/resize_image.dart';
import '../../utils/send_service.dart';

class SendFileDialog extends StatefulWidget {
  final Set<String>? recipients;
  final Room room;
  final List<MatrixFile> files;

  const SendFileDialog({
    this.recipients,
    required this.room,
    required this.files,
    Key? key,
  }) : super(key: key);

  @override
  SendFileDialogState createState() => SendFileDialogState();
}

class SendFileDialogState extends State<SendFileDialog> {
  bool origImage = false;
  bool isSending = false;
  ChatService chatService = ChatService();
  /// Images smaller than 20kb don't need compression.
  static const int minSizeToCompress = 20 * 1024;

  Widget imageFilesWidget(String sizeString) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Flexible(
        child: Image.memory(
          widget.files.first.bytes,
          fit: BoxFit.contain,
          key: Key(widget.files.first.hashCode.toString()),
        ),
      ),
      Row(
        children: <Widget>[
          Checkbox(
            value: origImage,
            onChanged: (v) => setState(() => origImage = v ?? false),
          ),
          InkWell(
            onTap: () => setState(() => origImage = !origImage),
            child: Text('${L10n.of(context)!.sendOriginal} ($sizeString)'),
          ),
        ],
      )
    ]);
  }

  Widget videoFilesWidget({required String fileName, required String sizeString}) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Flexible(
        child: FutureBuilder<MatrixImageFile?>(
          future: widget.files.first.getVideoThumbnail(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data == null) {
                return Text('$fileName ($sizeString)');
              }
              return Image.memory(snapshot.data!.bytes, fit: BoxFit.contain, key: Key(snapshot.data.hashCode.toString()));
            } else {
              return const CircularProgressIndicator();
            }
          },
        ),
      ),
      Row(
        children: <Widget>[
          Checkbox(
            value: origImage,
            onChanged: (v) => setState(() => origImage = v ?? false),
          ),
          InkWell(
            onTap: () => setState(() => origImage = !origImage),
            child: Text('${L10n.of(context)!.sendOriginal} ($sizeString)'),
          ),
        ],
      )
    ]);
  }

  Widget contentWidget({required bool allFilesAreImages, required bool allFilesAreVideo}) {
    final sizeString = widget.files.fold<double>(0, (p, file) => p + file.bytes.length).sizeString;
    final fileName = widget.files.length == 1 ? widget.files.single.name : L10n.of(context)!.countFiles(widget.files.length.toString());

    if (allFilesAreImages) {
      return imageFilesWidget(sizeString);
    } else if (allFilesAreVideo) {
      return videoFilesWidget(sizeString: sizeString, fileName: fileName);
    } else {
      return Text('$fileName ($sizeString)');
    }
  }

  @override
  Widget build(BuildContext context) {
    var sendStr = L10n.of(context)!.sendFile;
    final bool allFilesAreImages = widget.files.every((file) => file is MatrixImageFile);
    final bool allFilesAreAudio = widget.files.every((file) => file is MatrixAudioFile);
    final bool allFilesAreVideo = widget.files.every((file) => file is MatrixVideoFile);

    if (allFilesAreImages) {
      sendStr = L10n.of(context)!.sendImage;
    } else if (allFilesAreAudio) {
      sendStr = L10n.of(context)!.sendAudio;
    } else if (allFilesAreVideo) {
      sendStr = L10n.of(context)!.sendVideo;
    }

    return AlertDialog(
      title: Text(sendStr),
      content: contentWidget(allFilesAreImages: allFilesAreImages, allFilesAreVideo: allFilesAreVideo),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            // just close the dialog
            Navigator.of(context, rootNavigator: false).pop();
          },
          child: Text(L10n.of(context)!.cancel),
        ),
        TextButton(
          onPressed: () => chatService.send(
              files: widget.files,
              roomIds: {widget.room.id},
              context: context,
              isSharing: true,
              onStart: () {
                Navigator.of(context, rootNavigator: false).pop();
              }),
          child: Text(L10n.of(context)!.send),
        ),
      ],
    );
  }
}
