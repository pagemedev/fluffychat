import 'dart:async';

import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/logger_functions.dart';


import '../../widgets/matrix.dart';

class EncryptionButton extends StatefulWidget {
  final Room room;
  const EncryptionButton(this.room, {Key? key}) : super(key: key);

  @override
  _EncryptionButtonState createState() => _EncryptionButtonState();
}

class _EncryptionButtonState extends State<EncryptionButton> {
  StreamSubscription? _onSyncSub;
  Color? colorLeft;
  Color? colorRight;
  String? toolTip;
  bool? userVerified;
  bool? participantsVerified;
  final List<User> unverifiedParticipants = [];

  void _enableEncryptionAction() async {
    if (widget.room.encrypted) {
      context.go('/rooms/${widget.room.id}/encryption');
      return;
    }
    if (widget.room.joinRules == JoinRules.public) {
      await showOkAlertDialog(
        useRootNavigator: false,
        context: context,
        okLabel: L10n.of(context)!.ok,
        message: L10n.of(context)!.noEncryptionForPublicRooms,
      );
      return;
    }
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.enableEncryption,
          message: widget.room.client.encryptionEnabled ? L10n.of(context)!.enableEncryptionWarning : L10n.of(context)!.needPantalaimonWarning,
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.ok) {
      await showFutureLoadingDialog(
        context: context,
        future: () => widget.room.enableEncryption(),
      );
      // we want to enable the lock icon
      setState(() {});
    }
  }

  @override
  void dispose() {
    _onSyncSub?.cancel();
    super.dispose();
  }

  void checkUsers(List<User> users) {
    final client = Matrix.of(context).client;
    users.removeWhere((u) => !{Membership.invite, Membership.join}.contains(u.membership) || !widget.room.client.userDeviceKeys.containsKey(u.id));
    for (final u in users) {
      //Logs().i( u.id);
      final status = client.userDeviceKeys[u.id]!.verified;
      if (u.id == client.userID) {
        if (status == UserVerifiedStatus.verified) {
          userVerified = true;
        } else if (status == UserVerifiedStatus.unknownDevice) {
          userVerified = false;
        } else {
          userVerified = false;
        }
      } else {
        if (status == UserVerifiedStatus.verified) {
          participantsVerified = true;
        } else if (status == UserVerifiedStatus.unknownDevice) {
          unverifiedParticipants.add(u);
        } else {
          unverifiedParticipants.add(u);
        }
      }
    }
    if (unverifiedParticipants.isEmpty) {
      participantsVerified = true;
    } else {
      participantsVerified = false;
    }

    colorLeft = participantsVerified! ? Theme.of(context).colorScheme.primary : Colors.orangeAccent;
    colorRight = userVerified! ? Theme.of(context).colorScheme.primary : Colors.orangeAccent;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.room.encrypted) {
      _onSyncSub ??= Matrix.of(context).client.onSync.stream.where((s) => s.deviceLists != null).listen((s) => setState(() {}));
    }
    return FutureBuilder<List<User>>(
        future: widget.room.requestParticipants(),
        builder: (BuildContext context, snapshot) {
          setToolTips(snapshot);
          return Stack(
            children: [
              IconButton(
                tooltip: toolTip,
                icon: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    SvgPicture.asset(
                      'assets/pageme_app_logo_ transparent_right.svg',
                      height: 20,
                      color: colorRight,
                    ),
                    SvgPicture.asset(
                      'assets/pageme_app_logo_ transparent_left.svg',
                      height: 20,
                      color: colorLeft,
                    ),
                  ],
                ),
                onPressed: _enableEncryptionAction,
              ),
                 Positioned(
                  right: 2,
                  bottom: 10,
                  child: Icon(
                    (widget.room.encrypted) ? Icons.lock: Icons.lock_open,
                    color: (widget.room.encrypted) ? Colors.green :Colors.orange,
                    size: 10,
                  ),
                ),
            ],
          );
        });
  }

  void setToolTips(AsyncSnapshot<List<User>> snapshot) {
    if (widget.room.encrypted && snapshot.hasData) {
      final users = snapshot.data!;

      checkUsers(users);



      if (userVerified! && participantsVerified!) {
        toolTip = 'Encrypted, sessions and devices verified';
      } else if (!userVerified! && participantsVerified!) {
        toolTip = 'Encrypted, your device is unverified';
      } else if (userVerified! && !participantsVerified!) {
        toolTip = 'Encrypted, participant device is unverified';
      } else {
        toolTip = 'Encrypted with unverified sessions and devices';
      }
    } else if (!widget.room.encrypted && widget.room.joinRules != JoinRules.public && snapshot.hasData) {
      //Logs().i( "Not encrypted, private and has data");
      final users = snapshot.data!;
      checkUsers(users);


      if (userVerified! && participantsVerified!) {
        toolTip = 'Encryption not enabled, sessions and devices verified';
      } else if (!userVerified! && participantsVerified!) {
        toolTip = 'Encryption not enabled, your device is unverified';
      } else if (userVerified! && !participantsVerified!) {
        toolTip = 'Encryption not enabled, participant device is unverified';
      } else {
        toolTip = 'Encryption not enabled with unverified sessions and devices';
      }
    } else if (!widget.room.encrypted && widget.room.joinRules == JoinRules.public && snapshot.hasData) {
      //Logs().i( "Not encrypted, public and has data");
      final users = snapshot.data!;

      checkUsers(users);


      if (userVerified! && participantsVerified!) {
        toolTip = 'Encryption not available, sessions and devices verified';
      } else if (!userVerified! && participantsVerified!) {
        toolTip = 'Encryption not available, your device is unverified';
      } else if (userVerified! && !participantsVerified!) {
        toolTip = 'Encryption not available, participant device is unverified';
      } else {
        toolTip = 'Encryption not available, with unverified sessions and devices';
      }
    } else {
      //Logs().i( "Encrypted: ${widget.room.encrypted}, Public: ${widget.room.joinRules == JoinRules.public}, Data: ${snapshot.hasData}");
    }
  }
}
