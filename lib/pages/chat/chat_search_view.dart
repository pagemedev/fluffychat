import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/pages/chat/seen_by_row.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/filtered_timeline_extension.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../../cubits/search_handler/search_handler_cubit.dart';
import '../../utils/custom_scroll_behaviour.dart';
import '../../utils/platform_infos.dart';
import '../user_bottom_sheet/user_bottom_sheet.dart';
import 'message/message.dart';

class ChatSearchView extends StatefulWidget {
  final ChatController controller;
  const ChatSearchView({super.key, required this.controller});

  @override
  State<ChatSearchView> createState() => _ChatSearchViewState();
}

class _ChatSearchViewState extends State<ChatSearchView> {
  late final ChatController controller;
  bool _isInitialized = false;

  final AutoScrollController scrollController = AutoScrollController();

/*  void _updateScrollController() {
    if (!mounted) {
      return;
    }
    if (!scrollController.hasClients) return;
    if ((scrollController.position.pixels == scrollController.position.maxScrollExtent) &&
        controller.timeline!.events.isNotEmpty &&
        controller.timeline!.events[controller.timeline!.events.length - 1].type != EventTypes.RoomCreate) {
      BlocProvider.of<SearchHandlerCubit>(context).encryptedSearch(searchTerm: 'password', timeline: controller.timeline!, lastEventId: BlocProvider.of<SearchHandlerCubit>(context).state);
    }
  }*/

  @override
  void initState() {

    controller = widget.controller;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_isInitialized){
      //BlocProvider.of<SearchHandlerCubit>(context).initializeNewSearch();
      //scrollController.addListener(_updateScrollController);
      _isInitialized = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchHandlerCubit, SearchHandlerState>(
      builder: (context, state) {
        if (state is SearchHandlerInitial) {
          return const Text("Enter a search term");
        } else if (state is SearchHandlerLoading) {
          return const Center(child: CircularProgressIndicator());
        } else if (state is SearchHandlerResultSuccess) {
          final thisEventsKeyMap = <String, int>{};
          for (var i = 0; i < state.events.length; i++) {
            thisEventsKeyMap[state.events[i].eventId] = i;
          }
          return ListView.builder(
            controller: scrollController,
            reverse: true,
            itemCount: state.events.length,

            keyboardDismissBehavior: PlatformInfos.isIOS ? ScrollViewKeyboardDismissBehavior.onDrag : ScrollViewKeyboardDismissBehavior.manual,
            itemBuilder: (context, i) {
              final event = state.events[i];
              return event.isVisibleInGui
                  ? Message(
                      event,
                      onInfoTab: controller.showEventInfo,
                      onAvatarTab: (Event event) => showModalBottomSheet(
                        context: context,
                        builder: (c) => UserBottomSheet(
                          user: event.senderFromMemoryOrFallback,
                          outerContext: context,
                          onMention: () => controller.sendController.text += '${event.senderFromMemoryOrFallback.mention} ',
                        ),
                      ),
                      //unfold: controller.unfold,
                      timeStampSizeLarge: controller.timeStampSizeLarge!,
                      timeStampSizeSmall: controller.timeStampSizeSmall!,
                      onSelect: controller.onSelectMessage,
                      scrollToEventId: (String eventId) => controller.scrollToEventId(eventId),
                      longPressSelect: controller.selectedEvents.isEmpty,
                      selected: controller.selectedEvents.any((e) => e.eventId == event.eventId),
                      timeline: controller.timeline!,
                      nextEvent: i < controller.timeline!.events.length ? controller.timeline!.events[i] : null,
                      swipeRight: (swipeEvent) => controller.replyAction(replyTo: swipeEvent),
                    )
                  : const SizedBox.shrink();
            },
          );
        } else if (state is SearchHandlerError) {
          return Text("Error: ${state.message}");
        } else {
          return const Center(child: Text("Enter a search term"));
        }
      },
    );
  }
}
