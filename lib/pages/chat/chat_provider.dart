import 'package:flutter/widgets.dart';

import 'chat.dart';

class ChatControllerProvider extends InheritedWidget {
  final ChatController chatController;

  const ChatControllerProvider({Key? key, required this.chatController, required Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(ChatControllerProvider oldWidget) {
    return chatController != oldWidget.chatController;
  }

  static ChatController? of(BuildContext context) {
    final ChatControllerProvider? result = context.dependOnInheritedWidgetOfExactType<ChatControllerProvider>();
    return result?.chatController;
  }
}