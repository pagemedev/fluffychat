import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';

import '../../../cubits/sending_handler/sending_handler_bloc.dart';
import '../chat.dart';
import 'chat_send_action_button.dart';

class ChatActionButtons extends StatefulWidget {
  final ChatController controller;

  const ChatActionButtons({super.key, required this.controller});

  @override
  ChatActionButtonsState createState() => ChatActionButtonsState();
}

class ChatActionButtonsState extends State<ChatActionButtons> with TickerProviderStateMixin {
  late AnimationController _containerController;
  late AnimationController _columnController;
  late Animation<Offset> _containerAnimation;
  late Animation<double> _columnAnimation;
  bool isFullResolution = false;

  void toggleFullResolution(bool isEnabled) async {
    BlocProvider.of<SendingHandlerBloc>(context).add(FileCompressionChangeRequested(isEnabled: isEnabled, roomIds: [widget.controller.roomId!]));
  }

  @override
  void initState() {
    super.initState();
    _containerController = AnimationController(
      duration: const Duration(milliseconds: 150),
      vsync: this,
    );

    _columnController = AnimationController(
      duration: const Duration(milliseconds: 350),
      reverseDuration: const Duration(milliseconds: 150),
      vsync: this,
    );

    _containerAnimation = Tween<Offset>(
      begin: const Offset(1.1, 0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _containerController,
      curve: Curves.easeOutExpo,
    ));

    //TweenSequence([TweenSequenceItem])

    _columnAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(CurvedAnimation(
      parent: _columnController,
      curve: Curves.easeOutExpo,
    ));
  }

  @override
  void dispose() {
    _containerController.dispose();
    _columnController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileHandlerBloc, FileHandlerState>(
      buildWhen: (previous, next){
        return previous != next;
      },
      builder: (context, fileState) {
        final bool hasFiles = fileState.filesState.isNotEmpty;
        final bool containsImageState = fileState.filesState.any((element) => element is ImageState);
        if (hasFiles) {
          unawaited(_columnController.forward().whenComplete(() => _containerController.forward()).whenComplete(() => setState(() {})));
        } else {
          unawaited(_containerController.reverse().whenComplete(() => _columnController.reverse()).whenComplete(() => setState(() {})));
        }
        return BlocBuilder<SendingHandlerBloc, SendingHandlerState>(
          builder: (context, sendingState) {
            if (sendingState.isSending) {
              unawaited(_columnController.reverse().whenComplete(() => setState(() {})));
            }
            return AnimatedBuilder(
                animation: _columnController,
                builder: (context, _) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    verticalDirection: VerticalDirection.up,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ChatSendActionButton(
                        inputRowBorderRadius: _getBorderRadius(),
                        controller: widget.controller,
                      ),
                      AnimatedSwitcher(
                        duration: const Duration(milliseconds: 150),
                        reverseDuration: const Duration(milliseconds: 150),
                        switchInCurve: Curves.easeOutExpo,
                        switchOutCurve: Curves.easeInExpo,
                        transitionBuilder: (Widget child, Animation<double> animation) {
                          Animation<Offset> offsetAnimation;
                          if (!_containerAnimation.isDismissed) {
                            offsetAnimation = Tween<Offset>(begin: const Offset(1.1, 0), end: Offset.zero).animate(animation);
                          } else {
                            offsetAnimation = Tween<Offset>(begin: const Offset(1.1, 0), end: Offset.zero).animate(animation);
                          }
                          return SlideTransition(
                            position: offsetAnimation,
                            child: child,
                          );
                        },
                        child: _columnController.isCompleted
                            ? Padding(
                                padding: EdgeInsets.only(bottom: containsImageState ? 6 : 50),
                                child: Container(
                                  height: containsImageState ? 94 : 50,
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).colorScheme.tertiaryContainer,
                                    borderRadius: BorderRadius.circular(14),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      SizedBox(
                                        height: 40,
                                        child: IconButton(
                                          onPressed: () async {
                                            _columnController.reverse();
                                            await Future.delayed(const Duration(milliseconds: 150))
                                                .then((value) => context.read<FileHandlerBloc>().add(const FileCompletionRequested(isCancelled: true)));
                                          },
                                          icon: const Icon(
                                            Icons.close,
                                            color: Colors.redAccent,
                                          ),
                                        ),
                                      ),
                                      if (containsImageState)
                                        SizedBox(
                                          height: 40,
                                          child: IconButton(
                                            onPressed: () => toggleFullResolution(!sendingState.isCompressionEnabled),
                                            icon: Icon(
                                              sendingState.isCompressionEnabled ? Icons.hd_outlined : Icons.hd,
                                              color: sendingState.isCompressionEnabled ?  Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.primary,
                                            ),
                                          ),
                                        )
                                    ],
                                  ),
                                ),
                              )
                            : SizeTransition(
                                sizeFactor: _columnAnimation,
                                axis: Axis.vertical,
                                axisAlignment: 1,
                                child: SizedBox(
                                  height: 100 * _columnAnimation.value,
                                ),
                              ),
                      )
                    ],
                  );
                });
          },
        );
      },
    );
  }

  BorderRadius _getBorderRadius() {
    return BorderRadius.circular(14);
  }
}
