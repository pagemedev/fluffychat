import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';
import '../../../config/flavor_config.dart';
import '../../../config/themes.dart';
import '../../../cubits/sending_handler/sending_handler_bloc.dart';
import '../../../utils/platform_infos.dart';
import '../chat.dart';

class ChatSendActionButton extends StatelessWidget {
  const ChatSendActionButton({
    Key? key,
    required this.inputRowBorderRadius,
    required this.controller,
  }) : super(key: key);

  final BorderRadius inputRowBorderRadius;
  final ChatController controller;

  bool isReadyToSend(bool isReadyWithFiles) {
    return controller.isReadyWithText || isReadyWithFiles;
  }

  void requestSendEvent(FileHandlerState state, BuildContext context) {
    if (!isReadyToSend(state.isReadyWithFiles)) {
      // Send voice message as there is no text or files available;
      controller.voiceMessageAction();
      return;
    }
    if (!state.isReadyWithFiles && controller.isReadyWithText) {
      // Send text message as there are no files and there is text.
      BlocProvider.of<SendingHandlerBloc>(context).add(
        SendTextMessageRequested(
          currentText: controller.getCurrentText(),
          roomIds: [controller.roomId],
          editedEventId: controller.editEvent?.eventId,
          replyEvent: controller.replyEvent,
        ),
      );
      controller.handleTextCompletion();
      controller.handleQuillCompletion();
      controller.resetTextValues();
      return;
    }
    if (state.isReadyWithFiles) {
      BlocProvider.of<SendingHandlerBloc>(context).add(
        SendFilesRequested(
          files: state.filesState,
          roomIds: [controller.roomId],
          replyEvent: controller.replyEvent,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileHandlerBloc, FileHandlerState>(
      builder: (context, state) {
        return BlocBuilder<SendingHandlerBloc, SendingHandlerState>(
          builder: (context, sendState) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4.0),
              child: Material(
                elevation: 2,
                shape: RoundedRectangleBorder(
                  side: isBusiness() ? BorderSide.none : const BorderSide(color: Colors.black, width: 0.3),
                  borderRadius: inputRowBorderRadius,
                ),
                shadowColor: Colors.black,
                clipBehavior: Clip.hardEdge,
                child: AnimatedContainer(
                  height: 48,
                  width: PlatformInfos.isDesktop
                      ? isReadyToSend(state.isReadyWithFiles)
                          ? 48
                          : 0
                      : 48,
                  alignment: Alignment.center,
                  curve: Curves.fastLinearToSlowEaseIn,
                  decoration: BoxDecoration(
                    borderRadius: inputRowBorderRadius,
                    color: getButtonBackgroundColor(sendState, state, context),
                  ),
                  duration: const Duration(milliseconds: 300),
                  child: buildButtonByPlatform(sendState, context, state),
                ),
              ),
            );
          },
        );
      },
    );
  }

  Color getButtonBackgroundColor(SendingHandlerState sendState, FileHandlerState state, BuildContext context) {
    if (PlatformInfos.isAndroid) {
      return PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.tertiary;
    } else {
      return ((sendState.isSending && !sendState.isError) || (state.isCompressing || state.isProcessing))
          ? Theme.of(context).colorScheme.tertiaryContainer
          : PageMeThemes.isDarkMode(context)
              ? Theme.of(context).colorScheme.primary
              : Theme.of(context).colorScheme.tertiary;
    }
  }

  Widget buildButtonByPlatform(SendingHandlerState sendState, BuildContext context, FileHandlerState state) {
    if (PlatformInfos.isDesktop) {
      if (state.isProcessing || state.isCompressing) {
        return const CancelProcessingButton();
      }
      if (sendState.isSending && !sendState.isError) {
        return const CancelSendButton();
      } else {
        return IconButton(
          tooltip: L10n.of(context)!.send,
          icon: const Icon(Icons.send),
          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onTertiary,
          onPressed: () => requestSendEvent(state, context),
        );
      }
    } else if (PlatformInfos.isAndroid) {
      if (state.isProcessing || state.isCompressing) {
        return const CancelProcessingButton();
      }
      return IconButton(
        tooltip: isReadyToSend(state.isReadyWithFiles) ? L10n.of(context)!.send : L10n.of(context)!.voiceMessage,
        icon: isReadyToSend(state.isReadyWithFiles) ? const Icon(Icons.send) : const Icon(Icons.mic),
        color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onTertiary,
        onPressed: () => requestSendEvent(state, context),
      );
    } else {
      if (state.isProcessing || state.isCompressing) {
        return const CancelProcessingButton();
      }
      return IconButton(
        tooltip: (sendState.isSending && !sendState.isError)
            ? L10n.of(context)!.cancel
            : isReadyToSend(state.isReadyWithFiles)
                ? L10n.of(context)!.send
                : L10n.of(context)!.voiceMessage,
        icon: (sendState.isSending && !sendState.isError)
            ? const Icon(
                Icons.close,
                color: Colors.redAccent,
              )
            : isReadyToSend(state.isReadyWithFiles)
                ? const Icon(Icons.send)
                : const Icon(Icons.mic),
        color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onTertiary,
        onPressed: () =>
            (sendState.isSending && !sendState.isError) ? BlocProvider.of<SendingHandlerBloc>(context).add(const CancelSendEvent()) : requestSendEvent(state, context),
      );
    }
  }
}

class CancelSendButton extends StatelessWidget {
  const CancelSendButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      tooltip: L10n.of(context)!.cancel,
      icon: const Icon(
        Icons.close,
        color: Colors.redAccent,
      ),
      color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onTertiary,
      onPressed: () => BlocProvider.of<SendingHandlerBloc>(context).add(const CancelSendEvent()),
    );
  }
}

class CancelProcessingButton extends StatelessWidget {
  const CancelProcessingButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      tooltip: L10n.of(context)!.cancel,
      icon: const Icon(
        Icons.close,
        color: Colors.redAccent,
      ),
      onPressed: () => BlocProvider.of<FileHandlerBloc>(context).add(const FileCompletionRequested(isCancelled: true)),
    );
  }
}
