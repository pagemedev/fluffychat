// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/cubits/search_handler/search_handler_cubit.dart';
import 'package:pageMe/pages/chat/chat_search_view.dart';
import 'package:pageMe/pages/chat/seen_by_row.dart';
import 'package:pageMe/pages/chat/typing_indicators.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/filtered_timeline_extension.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:swipe_to/swipe_to.dart';

import '../../config/themes.dart';
import '../../utils/custom_scroll_behaviour.dart';
import '../../utils/platform_infos.dart';
import '../user_bottom_sheet/user_bottom_sheet.dart';
import 'chat.dart';
import 'message/message.dart';

class ChatEventList extends StatelessWidget {
  final ChatController controller;
  const ChatEventList({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final horizontalPadding = PageMeThemes.isColumnMode(context) ? 8.0 : 0.0;
    // create a map of eventId --> index to greatly improve performance of
    // ListView's findChildIndexCallback
    final thisEventsKeyMap = <String, int>{};
    for (var i = 0; i < controller.timeline!.events.length; i++) {
      thisEventsKeyMap[controller.timeline!.events[i].eventId] = i;
    }
    return Padding(
      padding: EdgeInsets.only(
        bottom: 4,
        left: horizontalPadding,
        right: horizontalPadding,
      ),
      child: BlocListener<SearchHandlerCubit, SearchHandlerState>(
        listenWhen: (_, __) => true,
        listener: (context, state) {
          if (controller.isSearchMode) {
            if (state is SearchHandlerInitial) {
              return controller.setDisplaySearchMode(false);
            } else {
              return controller.setDisplaySearchMode(true);
            }
          } else {
            return controller.setDisplaySearchMode(false);
          }
        },
        child: GestureDetector(
          onTap: () {
            if (PlatformInfos.isDesktop) {
              controller.removeReactionsOverlay();
            }
          },
          child: controller.displaySearchMode
              ? ChatSearchView(controller: controller)
              : CustomScrollView(
                  reverse: true,
                  controller: controller.scrollController,
                  scrollBehavior: CustomScrollBehavior(),
                  keyboardDismissBehavior: PlatformInfos.isIOS ? ScrollViewKeyboardDismissBehavior.onDrag : ScrollViewKeyboardDismissBehavior.manual,
                  slivers: [
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                        childCount: controller.timeline!.events.length + 2,
                        findChildIndexCallback: (key) => controller.findChildIndexCallback(
                          key,
                          thisEventsKeyMap,
                        ),
                        (BuildContext context, int i) {
                          // Footer to display typing indicator and read receipts:
                          if (i == 0) {
                            return Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                (controller.room.isDirectChat) ? const SizedBox.shrink() : SeenByRow(controller),
                                //TypingIndicators(controller),
                              ],
                            );
                          }
                          // Request history button or progress indicator:
                          if (i == controller.timeline!.events.length + 1) {
                            if (controller.timeline!.isRequestingHistory) {
                              return const Center(
                                child: CircularProgressIndicator.adaptive(strokeWidth: 2),
                              );
                            }
                            if (controller.canLoadMore) {
                              Center(
                                child: OutlinedButton(
                                  style: OutlinedButton.styleFrom(
                                    backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                                  ),
                                  onPressed: controller.requestHistory,
                                  child: Text(L10n.of(context)!.loadMore),
                                ),
                              );
                            }
                            return Container();
                          }
                          // The message at this index:
                          final event = controller.timeline!.events[i - 1];
                          return AutoScrollTag(
                            key: ValueKey(event.eventId),
                            index: i - 1,
                            controller: controller.scrollController,
                            child: event.isVisibleInGui
                                ? Message(
                                    event,
                                    key: ValueKey('message_item_${event.eventId}'),
                                    onInfoTab: controller.showEventInfo,
                                    onAvatarTab: (Event event) => showModalBottomSheet(
                                      context: context,
                                      builder: (c) => UserBottomSheet(
                                        user: event.senderFromMemoryOrFallback,
                                        outerContext: context,
                                        onMention: () => controller.sendController.text += '${event.senderFromMemoryOrFallback.mention} ',
                                      ),
                                    ),
                                    //unfold: controller.unfold,
                                    timeStampSizeLarge: controller.timeStampSizeLarge!,
                                    timeStampSizeSmall: controller.timeStampSizeSmall!,
                                    onSelect: controller.onSelectMessage,
                                    scrollToEventId: (String eventId) => controller.scrollToEventId(eventId),
                                    longPressSelect: controller.selectedEvents.isEmpty,
                                    selected: controller.selectedEvents.any((e) => e.eventId == event.eventId),
                                    timeline: controller.timeline!,
                                    nextEvent: i < controller.timeline!.events.length ? controller.timeline!.events[i] : null,
                                    swipeRight: (swipeEvent) => controller.replyAction(replyTo: swipeEvent),
                                  )
                                : const SizedBox.shrink(),
                          );
                        },
                      ),
                    )
                  ],
                ),
        ),
      ),
    );
  }
}
