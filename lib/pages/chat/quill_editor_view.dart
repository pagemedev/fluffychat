import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:keyboard_shortcuts/keyboard_shortcuts.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';
import 'package:pageMe/cubits/image_handler/image_handler_bloc.dart';
import 'package:pageMe/pages/chat/chat.dart';
import '../../config/themes.dart';
import '../../utils/logger_functions.dart';
import '../../utils/platform_infos.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class QuillEditorView extends StatefulWidget {
  final quill.QuillController controller;
  const QuillEditorView({Key? key, required this.controller}) : super(key: key);

  @override
  State<QuillEditorView> createState() => _QuillEditorViewState();
}

class _QuillEditorViewState extends State<QuillEditorView> {
  @override
  Widget build(BuildContext context) {
    final ChatController? controller = Chat.of(context);
    return BlocSelector<FileHandlerBloc, FileHandlerState, bool>(
        selector: (state) => state.filesState.isNotEmpty,
        builder: (context, hasFiles) {
          if (hasFiles) {
            return Container();
          }
          return quill.QuillProvider(
            configurations: quill.QuillConfigurations(
              controller: widget.controller,
            ),
            child: Column(
              children: [
                if (!PlatformInfos.isMobile)
                  Row(
                    children: [
                      SizedBox(
                        height: 45,
                        child: KeyBoardShortcuts(
                          keysToPress: {LogicalKeyboardKey.altLeft, LogicalKeyboardKey.keyE},
                          onKeysPressed: controller!.emojiPickerAction,
                          helpLabel: L10n.of(context)!.emojis,
                          child: IconButton(
                            tooltip: 'Open the text editor',
                            icon: PageTransitionSwitcher(
                              transitionBuilder: (
                                Widget child,
                                Animation<double> primaryAnimation,
                                Animation<double> secondaryAnimation,
                              ) {
                                return SharedAxisTransition(
                                  animation: primaryAnimation,
                                  secondaryAnimation: secondaryAnimation,
                                  transitionType: SharedAxisTransitionType.scaled,
                                  fillColor: Colors.transparent,
                                  child: child,
                                );
                              },
                              child: Icon(controller.showTextEditor ? Icons.keyboard_hide : Icons.edit_note,
                                  key: ValueKey(
                                    controller.showTextEditor,
                                  ),
                                  size: 30,
                                  color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSurface),
                            ),
                            onPressed: controller.textEditorAction,
                          ),
                        ),
                      ),
                      Expanded(
                        child: quill.QuillToolbar(
                          configurations: quill.QuillToolbarConfigurations(
                            color: Theme.of(context).colorScheme.surface,
                            multiRowsDisplay: false,
                            showSubscript: false,
                            showSuperscript: false,
                            showAlignmentButtons: true,
                            customButtons: [
                              quill.QuillToolbarCustomButtonOptions(
                                  controller: controller.textEditorController,
                                  icon: Icon(controller.showAttachmentPicker ? Icons.keyboard : Icons.attachment_outlined),
                                  onPressed: controller.attachmentPickerAction,
                                  tooltip: 'Send an attachment'),
                              quill.QuillToolbarCustomButtonOptions(
                                  controller: controller.textEditorController,
                                  icon: Icon(controller.showEmojiPicker ? Icons.keyboard : Icons.emoji_emotions_outlined),
                                  onPressed: controller.emojiPickerAction,
                                  tooltip: L10n.of(context)!.emojis),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                if (!PlatformInfos.isMobile)
                  const Divider(
                    height: 2,
                  ),
                const SizedBox(
                  height: 12,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  child: SizedBox(
                    height: 200,
                    child: Stack(
                      children: [
                        quill.QuillEditor(
                          scrollController: ScrollController(),
                          focusNode: controller!.quillFocus,
                          //controller: widget.controller,
                          configurations: quill.QuillEditorConfigurations(
                              enableInteractiveSelection: true,
                              enableSelectionToolbar: true,
                              showCursor: true,
                              placeholder: 'Write a composed message...',
                              scrollable: true,
                              autoFocus: true,
                              expands: false,
                              padding: EdgeInsets.zero,isOnTapOutsideEnabled: true,
                              readOnly: false,
                              customStyles: quill.DefaultStyles.getInstance(context).merge(
                                quill.DefaultStyles(
                                  placeHolder: quill.DefaultTextBlockStyle(
                                    TextStyle(fontSize: 16, color: Theme.of(context).colorScheme.onTertiaryContainer), // Set the text color here
                                    const quill.VerticalSpacing(0, 0),
                                    const quill.VerticalSpacing(0, 0),
                                    null,
                                  ),
                                ),
                              )),
                          // true for view only mode
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 10.0),
                            child: TextButton(
                              style: TextButton.styleFrom(
                                  backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.tertiary,
                                  elevation: 4,
                                  shadowColor: Colors.black,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(7.5),
                                  )),
                              onPressed: controller.send,
                              child: Text(
                                'Send',
                                style: TextStyle(
                                  color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onTertiary,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
    return Container();
  }
}

class SendButtonBlockEmbed extends quill.CustomBlockEmbed {
  const SendButtonBlockEmbed() : super(sendButtonType, '');

  static const String sendButtonType = 'sendButton';

  static SendButtonBlockEmbed fromDocument(quill.Document document) => const SendButtonBlockEmbed();
}

class SendButtonEmbedBuilder extends quill.EmbedBuilder {
  SendButtonEmbedBuilder({required this.onSend});

  final VoidCallback onSend;

  @override
  String get key => 'sendButton';

  @override
  Widget build(
    BuildContext context,
    quill.QuillController controller,
    quill.Embed node,
    bool readOnly,
    bool inline,
    TextStyle textStyle,
  ) {
    return SizedBox.expand(
      child: TextButton(
        onPressed: onSend,
        child: const Text('Send'),
      ),
    );
  }
}
