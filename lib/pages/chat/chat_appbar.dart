import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:pageMe/cubits/search_handler/search_handler_cubit.dart';
import 'package:pageMe/utils/string_color.dart';
import 'package:provider/provider.dart';
import 'package:searchbar_animation/searchbar_animation.dart';

import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/chat_settings_popup_menu.dart';
import '../../widgets/m2_popup_menu_button.dart';
import '../../widgets/unread_rooms_badge.dart';
import 'chat.dart';
import 'chat_app_bar_title.dart';

enum _EventContextAction { info, report }

/// This class represents an AppBar for a chat list. It is a custom AppBar
/// that is intended to be used in a chat application.
///
/// The AppBar changes its behavior and display based on the select mode
/// (i.e., normal, select, or share), which is passed as a parameter.
///
/// The class also requires a `ChatListController` and a `BuildContext` upon instantiation.
class ChatAppBar extends StatefulWidget implements PreferredSizeWidget {
  /// Creates a `ChatListAppBar`.
  ///
  /// The [selectMode], [controller], and [context] arguments must not be null.
  const ChatAppBar({Key? key, required this.controller, required this.context}) : super(key: key);

  final ChatController controller;
  final BuildContext context;

  @override
  State<ChatAppBar> createState() => _ChatAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(58.0);
}

class _ChatAppBarState extends State<ChatAppBar> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        toolbarHeight: 56,
        actionsIconTheme: IconThemeData(color: Theme.of(context).colorScheme.onBackground),
        leading: _buildLeading(),
        centerTitle: false,
        actions: _buildActions(constraints.maxWidth),
        bottom: ChatAppBarBottom(context: context),
        title: _animateTitle(),
        systemOverlayStyle: !PageMeThemes.isDarkMode(context) ? SystemUiOverlayStyle.dark : SystemUiOverlayStyle.light,
      );
    });
  }

  double _calculateSearchBarSize(double maxWidth) {
    if (PageMeThemes.isColumnMode(widget.context)) {
      return widget.controller.isSearchMode ? maxWidth / 3 : 50;
    }
    return widget.controller.isSearchMode ? maxWidth - 108 : 50;
  }

  /// Builds the actions for the AppBar based on the select mode.
  List<Widget>? _buildActions(double maxWidth) {
    return _appBarActions(widget.context, maxWidth);
  }

  /// Builds the leading widget for the AppBar.
  Widget _buildLeading() {
    return widget.controller.selectMode
        ? IconButton(
            icon: const Icon(
              Icons.close,
            ),
            onPressed: widget.controller.clearSelectedEvents,
            color: PageMeThemes.isDarkMode(widget.context) ? Colors.white : Theme.of(widget.context).colorScheme.onBackground,
            tooltip: L10n.of(widget.context)!.close,
          )
        : !PageMeThemes.isColumnMode(widget.context)
            ? UnreadRoomsBadge(
                filter: (r) => r.id != widget.controller.roomId,
                badgePosition: BadgePosition.topEnd(end: 8, top: 4),
                child: const Center(
                  child: BackButton(),
                ),
              )
            : const BackButton();
  }

  Widget _animateTitle() {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      height: getTitleHeight(),
      width: getTitleHeight(),
      child: ChatAppBarTitle(widget.controller),
    );
  }

  double? getTitleHeight() {
    if (PageMeThemes.isColumnMode(context)) {
      return null;
    } else {
      if (widget.controller.isSearchMode) {
        return 0;
      } else {
        return null;
      }
    }
  }

  List<Widget> _appBarActions(BuildContext context, maxWidth) {
    if (widget.controller.selectMode && PlatformInfos.isMobile) {
      return [
        if (widget.controller.canEditSelectedEvents)
          IconButton(
            icon: const Icon(
              Icons.edit_outlined,
            ),
            color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
            tooltip: L10n.of(context)!.edit,
            onPressed: widget.controller.editSelectedEventAction,
          ),
        IconButton(
          icon: const Icon(
            Icons.copy_outlined,
          ),
          color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
          tooltip: L10n.of(context)!.copy,
          onPressed: widget.controller.copyEventsAction,
        ),
        if (widget.controller.canSaveSelectedEvent)
          // Use builder context to correctly position the share dialog on iPad
          Builder(
              builder: (context) => IconButton(
                    icon: Icon(Icons.adaptive.share),
                    color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
                    tooltip: L10n.of(context)!.share,
                    onPressed: () => widget.controller.shareSelectedEvent(context),
                  )),
        IconButton(
          icon: const Icon(
            Icons.push_pin_outlined,
          ),
          color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
          onPressed: widget.controller.pinEvent,
          tooltip: L10n.of(context)!.pinMessage,
        ),
        if (widget.controller.canRedactSelectedEvents)
          IconButton(
            icon: const Icon(
              Icons.delete_outlined,
            ),
            color: Colors.red,
            tooltip: L10n.of(context)!.redactMessage,
            onPressed: widget.controller.redactEventsAction,
          ),
        if (widget.controller.selectedEvents.length == 1)
          M2PopupMenuButton<_EventContextAction>(
            //color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
            onSelected: (action) {
              switch (action) {
                case _EventContextAction.info:
                  widget.controller.showEventInfo();
                  widget.controller.clearSelectedEvents();
                  break;
                case _EventContextAction.report:
                  widget.controller.reportEventAction();
                  break;
              }
            },
            itemBuilder: (context) => [
              PopupMenuItem(
                value: _EventContextAction.info,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(Icons.info_outlined),
                    const SizedBox(width: 12),
                    Text(L10n.of(context)!.messageInfo),
                  ],
                ),
              ),
              PopupMenuItem(
                value: _EventContextAction.report,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(
                      Icons.shield_outlined,
                      color: Colors.red,
                    ),
                    const SizedBox(width: 12),
                    Text(L10n.of(context)!.reportMessage),
                  ],
                ),
              ),
            ],
          ),
      ];
    } else {
      return [
/*        if (Matrix.of(context).voipPlugin != null && controller.room!.isDirectChat)
          IconButton(
            onPressed: controller.onPhoneButtonTap,
            icon: const Icon(
              Icons.call,
              color: Colors.white,
            ),
            tooltip: L10n.of(context)!.placeCall,
          ),*/
        //if (PageMeThemes.isColumnMode(context))
        if (FlavorConfig.searchInChat)
        SizedBox(
          height: 50,
          width: _calculateSearchBarSize(maxWidth),
          child: SearchBarAnimation(
            durationInMilliSeconds: 300,
            searchBoxWidth: _calculateSearchBarSize(maxWidth),
            onPressButton: (isOpen) async {
              if (isOpen) {
                BlocProvider.of<SearchHandlerCubit>(context).initializeNewSearch();
              }
              widget.controller.toggleSearchMode(isOpen);
            },
            hintText: "Search this room",
            textEditingController: widget.controller.searchController,
            isOriginalAnimation: false,
            trailingWidget: Icon(
              Icons.search,
              color: Theme.of(context).colorScheme.onBackground,
              size: 22,
            ),
            enableBoxShadow: false,
            enableButtonShadow: false,
            buttonShadowColour: Colors.transparent,
            buttonElevation: 0,
            searchBoxBorderColour: Colors.transparent,
            secondaryButtonWidget: Icon(Icons.close, color: Theme.of(context).colorScheme.onBackground),
            buttonWidget: Icon(Icons.search, color: Theme.of(context).colorScheme.onBackground),
            buttonColour: Colors.transparent,
            searchBoxColour: Colors.transparent,
            buttonBorderColour: Colors.transparent,
            cursorColour: Theme.of(context).colorScheme.onBackground,
            enteredTextStyle: TextStyle(color: Theme.of(context).colorScheme.onBackground),
            onEditingComplete: () => BlocProvider.of<SearchHandlerCubit>(context).encryptedSearch(searchTerm: widget.controller.searchController.value.text, timeline: widget.controller.timeline!),
            isSearchBoxOnRightSide: true,
            enableKeyboardFocus: true,
            onCollapseComplete: () async {
              widget.controller.searchController.clear();
              widget.controller.toggleSearchMode(false);
              widget.controller.setDisplaySearchMode(false);
              await BlocProvider.of<SearchHandlerCubit>(context).cancelSearch();
            },
          ),
        ),
        ChatSettingsPopupMenu(widget.controller.room, true),
        if (PageMeThemes.isColumnMode(context))
          IconButton(
              onPressed: () {
                Provider.of<PageMeThemes>(context, listen: false).toggleThreeColumnMode(context);
                widget.controller.updateView();
              },
              icon: Icon(
                Provider.of<PageMeThemes>(context).isThreeColumnMode ? Symbols.dock_to_left_rounded : Symbols.dock_to_left_rounded,
                weight: 300,
                fill: Provider.of<PageMeThemes>(context).isThreeColumnMode ? 1 : 0,
                grade: 200,
                opticalSize: 48,
                color: Theme.of(context).colorScheme.onBackground,
              ))
      ];
    }
  }
}

class ChatAppBarBottom extends StatelessWidget implements PreferredSizeWidget {
  const ChatAppBarBottom({
    super.key,
    required this.context,
  });

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
        duration: const Duration(milliseconds: 300),
        child: BlocBuilder<SearchHandlerCubit, SearchHandlerState>(
          builder: (context, state) {
            if (state is SearchHandlerInitial || state is SearchHandlerComplete || state is SearchHandlerError) {
              return Container(
                height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
                color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black,
              );
            } else {
              return LinearProgressIndicator(
                color: Theme.of(context).colorScheme.primaryContainer.lighten(20),
                minHeight: 2,
                backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black,
              );
            }
          },
        ));
  }

  @override
  Size get preferredSize => const Size.fromHeight(2.0);
}
