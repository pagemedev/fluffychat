import 'package:animations/animations.dart';
import 'package:badges/badges.dart';
import 'package:desktop_drop/desktop_drop.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_persistent_keyboard_height/flutter_persistent_keyboard_height.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/cubits/search_handler/search_handler_cubit.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/pages/chat/chat_app_bar_title.dart';
import 'package:pageMe/pages/chat/chat_appbar.dart';
import 'package:pageMe/pages/chat/chat_event_list.dart';
import 'package:pageMe/pages/chat/pinned_events.dart';
import 'package:pageMe/pages/chat/tombstone_display.dart';
import 'package:pageMe/widgets/chat_settings_popup_menu.dart';
import 'package:pageMe/widgets/connection_status_header.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:provider/provider.dart';

import '../../utils/platform_infos.dart';
import '../../utils/stream_extension.dart';
import '../../widgets/m2_popup_menu_button.dart';
import '../../widgets/unread_rooms_badge.dart';
import 'chat_attachment_picker.dart';
import 'chat_emoji_picker.dart';
import 'chat_input_row.dart';

enum _EventContextAction { info, report }

class ChatView extends StatelessWidget {
  final ChatController controller;

  const ChatView(this.controller, {Key? key}) : super(key: key);

  List<Widget> _appBarActions(BuildContext context) {
    if (controller.selectMode && PlatformInfos.isMobile) {
      return [
        if (controller.canEditSelectedEvents)
          IconButton(
            icon: const Icon(
              Icons.edit_outlined,
            ),
            color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
            tooltip: L10n.of(context)!.edit,
            onPressed: controller.editSelectedEventAction,
          ),
        IconButton(
          icon: const Icon(
            Icons.copy_outlined,
          ),
          color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
          tooltip: L10n.of(context)!.copy,
          onPressed: controller.copyEventsAction,
        ),
        if (controller.canSaveSelectedEvent)
          // Use builder context to correctly position the share dialog on iPad
          Builder(
              builder: (context) => IconButton(
                    icon: Icon(Icons.adaptive.share),
                    color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
                    tooltip: L10n.of(context)!.share,
                    onPressed: () => controller.shareSelectedEvent(context),
                  )),
        IconButton(
          icon: const Icon(
            Icons.push_pin_outlined,
          ),
          color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
          onPressed: controller.pinEvent,
          tooltip: L10n.of(context)!.pinMessage,
        ),
        if (controller.canRedactSelectedEvents)
          IconButton(
            icon: const Icon(
              Icons.delete_outlined,
            ),
            color: Colors.red,
            tooltip: L10n.of(context)!.redactMessage,
            onPressed: controller.redactEventsAction,
          ),
        if (controller.selectedEvents.length == 1)
          M2PopupMenuButton<_EventContextAction>(
            color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
            onSelected: (action) {
              switch (action) {
                case _EventContextAction.info:
                  controller.showEventInfo();
                  controller.clearSelectedEvents();
                  break;
                case _EventContextAction.report:
                  controller.reportEventAction();
                  break;
              }
            },
            itemBuilder: (context) => [
              PopupMenuItem(
                value: _EventContextAction.info,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(Icons.info_outlined),
                    const SizedBox(width: 12),
                    Text(L10n.of(context)!.messageInfo),
                  ],
                ),
              ),
              PopupMenuItem(
                value: _EventContextAction.report,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(
                      Icons.shield_outlined,
                      color: Colors.red,
                    ),
                    const SizedBox(width: 12),
                    Text(L10n.of(context)!.reportMessage),
                  ],
                ),
              ),
            ],
          ),
      ];
    } else {
      return [
/*        if (Matrix.of(context).voipPlugin != null && controller.room!.isDirectChat)
          IconButton(
            onPressed: controller.onPhoneButtonTap,
            icon: const Icon(
              Icons.call,
              color: Colors.white,
            ),
            tooltip: L10n.of(context)!.placeCall,
          ),*/
        //if (PageMeThemes.isColumnMode(context))
        ChatSettingsPopupMenu(controller.room, true),
        if (PageMeThemes.isColumnMode(context))
          IconButton(
              onPressed: () {
                Provider.of<PageMeThemes>(context, listen: false).toggleThreeColumnMode(context);
                controller.updateView();
              },
              icon: Icon(
                Provider.of<PageMeThemes>(context).isThreeColumnMode ? Symbols.dock_to_left_rounded : Symbols.dock_to_left_rounded,
                weight: 300,
                fill: Provider.of<PageMeThemes>(context).isThreeColumnMode ? 1 : 0,
                grade: 200,
                opticalSize: 48,
                color: Theme.of(context).colorScheme.onBackground,
              ))
      ];
    }
  }


  @override
  Widget build(BuildContext context) {
    if (controller.selectedEvents.length > 1) {
      controller.removeReactionsOverlay();
    }
    if (controller.room.membership == Membership.invite) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        await showFutureLoadingDialog(context: context, future: () => controller.room.join());
      });
    }

    return WillPopScope(
      onWillPop: () async {
        if (controller.selectedEvents.isNotEmpty) {
          controller.removeReactionsOverlay();
          controller.clearSelectedEvents();
          return false;
        } else if (controller.showEmojiPicker) {
          controller.emojiPickerAction();
          return false;
        } else if (controller.showAttachmentPicker) {
          controller.attachmentPickerAction();
          return false;
        } else if (controller.reactionsOverlay?.mounted == true){
          controller.removeReactionsOverlay();
        }
        return true;
      },
      child: GestureDetector(
        onTapDown: controller.setReadMarker,
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        behavior: HitTestBehavior.translucent,
        child: StreamBuilder(
          stream: controller.room.onUpdate.stream.rateLimit(const Duration(seconds: 1)),
          builder: (context, snapshot) => FutureBuilder<bool>(
            future: controller.getTimeline(),
            builder: (BuildContext context, snapshot) {
              return PersistentKeyboardHeightProvider(
                child: Scaffold(
                  resizeToAvoidBottomInset: false,
                  appBar: ChatAppBar(controller: controller, context: context,)/*AppBar(
                    actionsIconTheme: const IconThemeData(color: Colors.black),
                    bottom: PreferredSize(
                      preferredSize: const Size.fromHeight(2.0),
                      child: Container(
                          height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
                          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black //Theme.of(context).colorScheme.primary,
                          //indent: 12,
                          //endIndent: 12,
                          ),
                    ),
                    automaticallyImplyLeading: false,
                    leading: controller.selectMode
                        ? IconButton(
                            icon: const Icon(
                              Icons.close,
                            ),
                            onPressed: controller.clearSelectedEvents,
                            color: PageMeThemes.isDarkMode(context) ? Colors.white : Theme.of(context).colorScheme.onBackground,
                            tooltip: L10n.of(context)!.close,
                          )
                        : !PageMeThemes.isColumnMode(context)
                            ? UnreadRoomsBadge(
                                filter: (r) => r.id != controller.roomId,
                                badgePosition: BadgePosition.topEnd(end: 8, top: 4),
                                child: const Center(
                                  child: BackButton(),
                                ),
                              )
                            : const BackButton(),
                    titleSpacing: 0,
                    title: ChatAppBarTitle(controller),
                    actions: _appBarActions(context),
                  )*/,
                  floatingActionButton: controller.showScrollDownButton && controller.selectedEvents.isEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(bottom: 56.0),
                          child: FloatingActionButton(
                            onPressed: controller.scrollDown,
                            foregroundColor: Theme.of(context).colorScheme.onTertiary,
                            backgroundColor: Theme.of(context).colorScheme.tertiary,
                            shape: RoundedRectangleBorder(side: isBusiness() ? BorderSide.none : const BorderSide(color: Colors.black, width: 0.3), borderRadius: BorderRadius.circular(12)),
                            mini: true,
                            child: Icon(
                              Icons.arrow_downward_outlined,
                              color: Theme.of(context).colorScheme.onTertiary,
                            ),
                          ),
                        )
                      : null,
                  backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.background : Theme.of(context).colorScheme.background,
                  body: DropTarget(
                    onDragDone: controller.onDragDone,
                    onDragEntered: controller.onDragEntered,
                    onDragExited: controller.onDragExited,
                    child: Stack(
                      children: <Widget>[
                        (Matrix.of(context).wallpaper != null)
                            ? Image.file(
                                Matrix.of(context).wallpaper!,
                                width: double.infinity,
                                height: double.infinity,
                                fit: BoxFit.cover,
                              )
                            : Container(),
                        SafeArea(
                          child: Column(
                            children: <Widget>[
                              const ConnectionStatusHeader(),
                              TombstoneDisplay(controller),
                              PinnedEvents(controller),
                              Expanded(
                                child: GestureDetector(
                                    onTap: controller.clearSingleSelectedEvent,
                                    child: Builder(
                                      builder: (context) {
                                        if (controller.timeline == null) {
                                          return const Center(
                                            child: CircularProgressIndicator.adaptive(strokeWidth: 2),
                                          );
                                        }
                                        return ChatEventList(controller: controller);
                                      },
                                    )),
                              ),
                              if (controller.room.canSendDefaultMessages && controller.room.membership == Membership.join)
                                Builder(builder: (context) {
                                  final keyboardHeight = PersistentKeyboardHeight.of(context).keyboardHeight;
                                  return Padding(
                                    padding: controller.inputFocus.hasFocus
                                        ? EdgeInsets.only(bottom: keyboardHeight)
                                        : MediaQuery.of(context).viewInsets.bottom != 0
                                            ? EdgeInsets.only(bottom: keyboardHeight)
                                            : const EdgeInsets.all(0),
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          //ReactionsPicker(controller),
                                          ChatInputRow(controller),
                                          if (PlatformInfos.isMobile && controller.showTextEditor)
                                            Row(
                                              children: [
                                                SizedBox(
                                                  height: 45,
                                                  child: IconButton(
                                                    tooltip: 'Open the text editor',
                                                    icon: PageTransitionSwitcher(
                                                      transitionBuilder: (
                                                        Widget child,
                                                        Animation<double> primaryAnimation,
                                                        Animation<double> secondaryAnimation,
                                                      ) {
                                                        return SharedAxisTransition(
                                                          animation: primaryAnimation,
                                                          secondaryAnimation: secondaryAnimation,
                                                          transitionType: SharedAxisTransitionType.scaled,
                                                          fillColor: Colors.transparent,
                                                          child: child,
                                                        );
                                                      },
                                                      child: Icon(controller.showTextEditor ? Icons.keyboard_hide : Icons.edit_note,
                                                          key: ValueKey(
                                                            controller.showTextEditor,
                                                          ),
                                                          size: 30,
                                                          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSurface),
                                                    ),
                                                    onPressed: controller.textEditorAction,
                                                  ),
                                                ),
                                                Expanded(
                                                  child: quill.QuillProvider(
                                                    configurations: quill.QuillConfigurations(controller: controller.textEditorController),
                                                    child: quill.QuillToolbar(
                                                      configurations: quill.QuillToolbarConfigurations(
                                                        color: Theme.of(context).colorScheme.surface,
                                                        multiRowsDisplay: false,
                                                        customButtons: [
                                                          quill.QuillToolbarCustomButtonOptions(
                                                              controller: controller.textEditorController,
                                                              icon: Icon(controller.showAttachmentPicker ? Icons.keyboard : Icons.attachment_outlined),
                                                              onPressed: controller.attachmentPickerAction,
                                                              tooltip: 'Send an attachment'),
                                                          quill.QuillToolbarCustomButtonOptions(
                                                              controller: controller.textEditorController,
                                                              icon: Icon(controller.showEmojiPicker ? Icons.keyboard : Icons.emoji_emotions_outlined),
                                                              onPressed: controller.emojiPickerAction,
                                                              tooltip: L10n.of(context)!.emojis),
                                                        ],
                                                        showSubscript: false,
                                                        showSuperscript: false,
                                                        showAlignmentButtons: true,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            )
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                              Builder(builder: (context) {
                                return controller.showEmojiPicker
                                    ? MediaQuery.of(context).viewInsets.bottom == 0
                                        ? Center(
                                            child: Material(
                                              elevation: 1,
                                              shadowColor: Theme.of(context).secondaryHeaderColor.withAlpha(100),
                                              clipBehavior: Clip.hardEdge,
                                              color: (Theme.of(context).brightness == Brightness.light) ? Theme.of(context).colorScheme.surface : Theme.of(context).appBarTheme.backgroundColor,
                                              child: ChatEmojiPicker(controller),
                                            ),
                                          )
                                        : const SizedBox.shrink()
                                    : const SizedBox.shrink();
                              }),
                              Builder(builder: (context) {
                                return controller.showAttachmentPicker
                                    ? MediaQuery.of(context).viewInsets.bottom == 0
                                        ? Center(
                                            child: Material(
                                              elevation: 1,
                                              shadowColor: Theme.of(context).secondaryHeaderColor.withAlpha(100),
                                              clipBehavior: Clip.hardEdge,
                                              color: (Theme.of(context).brightness == Brightness.light) ? Theme.of(context).colorScheme.surface : Theme.of(context).appBarTheme.backgroundColor,
                                              child: ChatAttachmentPicker(controller),
                                            ),
                                          )
                                        : const SizedBox.shrink()
                                    : const SizedBox.shrink();
                              })
                            ],
                          ),
                        ),
                        if (controller.dragging)
                          Container(
                            color: Theme.of(context).scaffoldBackgroundColor.withOpacity(0.9),
                            alignment: Alignment.center,
                            child: const Icon(
                              Icons.upload_outlined,
                              size: 100,
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
