import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:desktop_drop/desktop_drop.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
//import 'package:image_editor_plus/data/image_item.dart';
//import 'package:image_editor_plus/image_editor_plus.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/setting_keys.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';
import 'package:pageMe/pages/chat/chat_view.dart';
import 'package:pageMe/pages/chat/event_info_dialog.dart';
import 'package:pageMe/pages/chat/reactions_picker.dart';
import 'package:pageMe/pages/chat/recording_dialog.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/filtered_timeline_extension.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/ios_badge_client_extension.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/send_service.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:provider/provider.dart';
import 'package:record/record.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vsc_quill_delta_to_html/vsc_quill_delta_to_html.dart';

import '../../pageme_app.dart';
import '../../utils/account_bundles.dart';
import '../../utils/famedlysdk_store.dart';
import '../../utils/localized_exception_extension.dart';
import '../chat_details/chat_details.dart';
import 'chat_provider.dart';
import 'send_location_dialog.dart';
import 'sticker_picker_dialog.dart';

typedef OnPickImageCallback = void Function(double? maxWidth, double? maxHeight, int? quality);

extension CustomMessageTypes on MessageTypes {
  static const String quillText = 'm.quill_text';
}

class ChatPage extends StatelessWidget {
  final String roomId;

  const ChatPage({
    super.key,
    required this.roomId,
  });

  @override
  Widget build(BuildContext context) {
    final room = Matrix.of(context).client.getRoomById(roomId);
    if (room == null) {
      return Scaffold(
        appBar: AppBar(title: Text(L10n.of(context)!.oopsSomethingWentWrong)),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Text(L10n.of(context)!.youAreNoLongerParticipatingInThisChat),
          ),
        ),
      );
    }
    final isEmbedded = GoRouterState.of(context).fullPath == '/rooms/:roomid';
    return Row(
      children: [
        Expanded(
          child: ClipRRect(
            borderRadius: const BorderRadius.only(
              topRight: Radius.circular(FlavorConfig.borderRadius),
              bottomRight: Radius.circular(FlavorConfig.borderRadius),
            ),
            child: Chat(
              key: Key('chat_page_$roomId'),
              room: room,
            ),
          ),
        ),
        if (Provider.of<PageMeThemes>(context).isThreeColumnMode && room.membership == Membership.join)
          Padding(
            padding: isEmbedded ? const EdgeInsets.only(left: 8.0) : EdgeInsets.zero,
            child: Material(
              borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
              child: SizedBox(
                width: PageMeThemes.columnWidth,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(FlavorConfig.borderRadius),
                    bottomLeft: Radius.circular(FlavorConfig.borderRadius),
                  ),
                  child: ChatDetails(roomId: roomId),
                ),
              ),
            ),
          ),
      ],
    );
  }
}

class Chat extends StatefulWidget {
  final Room room;
  const Chat({Key? key, required this.room}) : super(key: key);

  @override
  ChatController createState() => ChatController();

  static ChatController? of(BuildContext context) {
    final ChatControllerProvider? result = context.dependOnInheritedWidgetOfExactType<ChatControllerProvider>();
    if (result == null) {
      loggerWarning(logMessage: 'ChatControllerProvider not found in the widget tree');
      return null;
      //throw FlutterError('ChatControllerProvider not found in the widget tree');
    }
    return result.chatController;
  }
}

class ChatController extends State<Chat> {
  static const double reactionIconSize = 56;
  static const double reactionIconCount = 6;
  static const totalReactionsWidgetSize = (reactionIconSize * reactionIconCount);
  static const double leftSideOfScreenColumnMode = 580;
  bool _isIntialized = false;
  String get roomId => widget.room.id;
  Room get room => sendingClient.getRoomById(roomId) ?? widget.room;
  OverlayEntry? reactionsOverlay;
  ChatService chatService = ChatService();

  late Client sendingClient;

  Timeline? timeline;

  Size? timeStampSizeLarge;

  Size? timeStampSizeSmall;

  late MatrixState matrix;
  late Size mediaQuerySizeOf;
  late OverlayState overlayState;
  late L10n l10n;
  late ThemeData theme;
  late ScaffoldMessengerState scaffoldMessenger;
  late FocusScopeNode focusScope;
  FocusNode overlayFocusNode = FocusNode();

  Event? reactionEvent;

  final AutoScrollController scrollController = AutoScrollController();

  bool get isReadyWithText => getCurrentText().isNotEmpty;

  FocusNode inputFocus = FocusNode();
  FocusNode quillFocus = FocusNode();

  Timer? typingCoolDown;
  Timer? typingTimeout;
  bool currentlyTyping = false;
  bool dragging = false;
  //bool isFileHandlerInitiated = false;
  late FileHandlerBloc fileHandlerBloc;

  ChatControllerProvider provide(BuildContext context, Widget child) {
    return ChatControllerProvider(chatController: this, child: child);
  }

  double checkOverlayYPosition(double overlayPosition) {
    const double minDistanceFromTop = 100;
    final double maxDistanceFromTop = mediaQuerySizeOf.height;
    Logs().v("Overlay y position: $overlayPosition, minDistanceFromTop: $minDistanceFromTop, maxDistanceFromTop: $maxDistanceFromTop");
    if (overlayPosition < minDistanceFromTop) {
      overlayPosition = minDistanceFromTop;
    } else if (overlayPosition > maxDistanceFromTop) {
      overlayPosition = maxDistanceFromTop - 100;
    }
    return overlayPosition;
  }

  double checkOverlayXPosition(double overlayPosition, bool isOwnEvent) {
    final double minDistanceToRight = mediaQuerySizeOf.width - totalReactionsWidgetSize - leftSideOfScreenColumnMode;
    const double minDistanceToLeft = 10;
    Logs().v("Overlay x position: $overlayPosition, minDistanceToRight: $minDistanceToRight, minDistanceToLeft: $minDistanceToLeft,");
    if (overlayPosition > minDistanceToRight && isOwnEvent) {
      overlayPosition = minDistanceToRight;
    } else if (overlayPosition < minDistanceToLeft && !isOwnEvent) {
      overlayPosition = minDistanceToLeft;
    }

    return overlayPosition;
  }

  double platformHandlerLeft({required Offset touchPosition, Event? event}) {
    if (PlatformInfos.isMobile) {
      return mediaQuerySizeOf.width - totalReactionsWidgetSize - 14;
    } else {
      final isOwnEvent = event!.senderId == matrix.client.userID;
      if (isOwnEvent) {
        return checkOverlayXPosition(touchPosition.dx - leftSideOfScreenColumnMode, isOwnEvent);
      } else {
        return checkOverlayXPosition(touchPosition.dx - totalReactionsWidgetSize - leftSideOfScreenColumnMode, isOwnEvent);
      }
    }
  }

  double platformHandlerTop({required Offset touchPosition, Event? event}) {
    if (PlatformInfos.isMobile) {
      return checkOverlayYPosition(-touchPosition.dy - 130);
    } else {
      return touchPosition.dy - 70;
    }
  }

  void showReactionsPicker({required BuildContext context, required Offset touchPosition, Event? event}) {
    Logs().v("Entering showReactionsPicker method.");

    if (reactionsOverlay?.mounted == true) {
      Logs().e("Reactions overlay was already mounted, removing overlay.");
      reactionsOverlay?.remove();
      Logs().v("Removed existing reactionsOverlay.");
    } else {
      Logs().v("Reactions overlay was not previously mounted.");
    }
    reactionsOverlay = OverlayEntry(
      builder: (context) {
        return Positioned(
          left: platformHandlerLeft(touchPosition: touchPosition, event: event),
          top: platformHandlerTop(touchPosition: touchPosition, event: event),
          child: SizedBox(
            width: totalReactionsWidgetSize,
            height: 56,
            child: FocusableActionDetector(
              onFocusChange: _onOverlayFocusChange,
              autofocus: false,
              focusNode: overlayFocusNode,
              child: ReactionsPicker(
                this,
                event: event,
              ),
            ),
          ),
        );
      },
    );
    // Before inserting
    overlayState.insert(reactionsOverlay!);
    overlayFocusNode.requestFocus();
  }

  void _onOverlayFocusChange(bool hasFocus) {
    Logs().v("Focus Changed, has focus: $hasFocus");
    if (!hasFocus) {
      removeReactionsOverlay();
    }
  }

  void onDragEntered(_) => setState(() => dragging = true);

  void onDragExited(_) => setState(() => dragging = false);

  void onDragDone(DropDoneDetails details) async {
    setState(() => dragging = false);
    fileHandlerBloc.add(XFileAdditionRequested(details.files));
  }

  bool get canSaveSelectedEvent =>
      selectedEvents.length == 1 &&
      {
        MessageTypes.Video,
        MessageTypes.Image,
        MessageTypes.Sticker,
        MessageTypes.Audio,
        MessageTypes.File,
      }.contains(selectedEvents.single.messageType);

  void shareSelectedEvent(context) => selectedEvents.single.shareFile(context);

  List<Event> selectedEvents = [];

  final Set<String> unfolded = {};

  Event? replyEvent;

  Event? editEvent;

  bool showScrollDownButton = false;

  bool get selectMode => selectedEvents.isNotEmpty;

  final int _loadHistoryCount = 50;

  String inputText = '';

  String pendingText = '';

  bool get canLoadMore => timeline!.events.isEmpty || timeline!.events.last.type != EventTypes.RoomCreate;

  void toggleEmojiPicker() {
    if (showEmojiPicker) {
      setState(() {
        showEmojiPicker = !showEmojiPicker;
      });
    } else {
      setState(() {
        showAttachmentPicker = false;
        showEmojiPicker = !showEmojiPicker;
      });
    }
  }

  void toggleTextEditor() {
    if (showTextEditor) {
      setState(() {
        showTextEditor = !showTextEditor;
      });
      inputFocus.requestFocus();
    } else {
      setState(() {
        showAttachmentPicker = false;
        showEmojiPicker = false;
        showTextEditor = !showTextEditor;
      });
      quillFocus.requestFocus();
    }
  }

  void toggleAttachmentPicker() {
    if (showAttachmentPicker) {
      setState(() {
        showAttachmentPicker = !showAttachmentPicker;
      });
    } else {
      setState(() {
        showEmojiPicker = false;
        showAttachmentPicker = !showAttachmentPicker;
      });
    }
  }

  bool showTextEditor = false;
  bool showEmojiPicker = false;
  bool showAttachmentPicker = false;

  EmojiPickerType emojiPickerType = EmojiPickerType.keyboard;
  int maxRequestCount = 5;
  int currentRequestHistoryCount = 0;

  void requestHistory() async {
    if (canLoadMore && timeline != null) {
      try {
        currentRequestHistoryCount++;
        await timeline!.requestHistory(historyCount: _loadHistoryCount);
        if (timeline!.events.every((element) => !element.isVisibleInGui) && (currentRequestHistoryCount <= maxRequestCount)) {
          Logs().i("No events were visible in the GUI, requesting more");
          await timeline!.requestHistory(historyCount: _loadHistoryCount * 2);
        }
      } catch (err) {
        /*scaffoldMessenger.showSnackBar(
          SnackBar(
            content: Text(
              (mounted) ? (err).toLocalizedString(context) : "Error requesting timeline",
              style: TextStyle(color: theme.colorScheme.onTertiaryContainer),
            ),
            backgroundColor: theme.colorScheme.tertiaryContainer,
            closeIconColor: theme.colorScheme.onTertiaryContainer,
          ),
        );*/
        Logs().e((mounted) ? (err).toLocalizedString(context) : "Error requesting timeline");
        rethrow;
      }
    }
  }

  void _updateScrollController() {
    if (!mounted) {
      return;
    }
    removeReactionsOverlay();
    setReadMarker();
    if (!scrollController.hasClients) return;
    if ((scrollController.position.pixels == scrollController.position.maxScrollExtent) &&
        timeline!.events.isNotEmpty &&
        timeline!.events[timeline!.events.length - 1].type != EventTypes.RoomCreate) {
      requestHistory();
    }
    if (scrollController.position.pixels > 20 && showScrollDownButton == false) {
      setState(() => showScrollDownButton = true);
    } else if (scrollController.position.pixels == 0 && showScrollDownButton == true) {
      setState(() => showScrollDownButton = false);
    }
  }

  void _loadDraft() async {
    final prefs = await SharedPreferences.getInstance();
    final draft = prefs.getString('draft_$roomId');
    if (draft != null && draft.isNotEmpty) {
      sendController.text = draft;
      setState(() => inputText = draft);
    }
  }

  bool _isSearchMode = false;
  bool get isSearchMode => _isSearchMode;
  void toggleSearchMode(bool searchBarOpen) {
    setState(() {
      _isSearchMode = searchBarOpen;
    });
  }

  bool _displaySearchMode = false;
  bool get displaySearchMode => _displaySearchMode;
  void setDisplaySearchMode(bool shouldDisplay) {
    setState(() {
      _displaySearchMode = shouldDisplay;
    });
  }

  @override
  void initState() {
    scrollController.addListener(_updateScrollController);
    inputFocus.addListener(_inputFocusListener);
    _loadDraft();

    super.initState();
  }

  @override
  void didUpdateWidget(Chat oldWidget) {
    super.didUpdateWidget(oldWidget);
    Logs().v("Parent widget updated");
  }

  @override
  Future<void> didChangeDependencies() async {
    initContextDependencies();
    super.didChangeDependencies();
    if (!_isIntialized) {
      if (fileHandlerBloc.state.isInitiated) {
        Logs().v("onPop - BlocProvider.of<FileHandlerBloc>(context).state.isInitiated");
        try {
          fileHandlerBloc.add(const FileCompletionRequested(isCancelled: true));
        } on Exception {
          Logs().e("ChatView, onPop: FileHandlerBloc isInitiated, but cannot send cancel to bloc.");
        }
      }
      _isIntialized = true;
    }
    await getTimeStampSize();
  }

  void initContextDependencies() {
    matrix = Matrix.of(context);
    sendingClient = matrix.client;
    mediaQuerySizeOf = MediaQuery.sizeOf(context);
    overlayState = Overlay.of(context);
    l10n = L10n.of(context)!;
    theme = Theme.of(context);
    focusScope = FocusScope.of(context);
    scaffoldMessenger = ScaffoldMessenger.of(context);
    fileHandlerBloc = BlocProvider.of<FileHandlerBloc>(context);
  }

  void updateView() {
    if (!mounted) return;
    setState(() {});
  }

  Future<void> getTimeStampSize() async {
    timeStampSizeLarge = Size.fromWidth(double.parse(await Store().getItem(SettingKeys.timeStampSizeLarge) ?? '0'));
    timeStampSizeSmall = Size.fromWidth(double.parse(await Store().getItem(SettingKeys.timeStampSizeSmall) ?? '0'));
  }

  Future<bool> getTimeline({
    String? eventContextId,
  }) async {
    if (timeline == null) {
      if (!context.mounted) {
        Logs().e("Chat Controller(getTimeline): Context not mounted");
        return false;
      }
      final client = matrix.client;
      await client.roomsLoading;
      await client.accountDataLoading;
      if (eventContextId != null && (!eventContextId.isValidMatrixId || eventContextId.sigil != '\$')) {
        eventContextId = null;
      }
      timeline = await room.getTimeline(
        onUpdate: updateView,
      );
      if (timeline!.events.isNotEmpty) {
        if (room.markedUnread) await room.markUnread(false);
        setReadMarker();
      }

      // when the scroll controller is attached we want to scroll to an event id, if specified
      // and update the scroll controller...which will trigger a request history, if the
      // "load more" button is visible on the screen
      SchedulerBinding.instance.addPostFrameCallback((_) async {
        if (mounted) {
          final event = PageMeApp.router.routeInformationProvider.value.uri.queryParameters['event'];
          if (event != null) {
            scrollToEventId(event);
          }
          _updateScrollController();
        }
      });
    }
    timeline!.requestKeys(onlineKeyBackupOnly: false);
    return true;
  }

  Future<void>? _setReadMarkerFuture;

  void setReadMarker([_]) {
    if (_setReadMarkerFuture == null && (room.hasNewMessages || room.notificationCount > 0) && timeline != null && timeline!.events.isNotEmpty && matrix.webHasFocus) {
      Logs().v('Set read marker...');
      // ignore: unawaited_futures
      _setReadMarkerFuture = timeline!.setReadMarker().then((_) {
        _setReadMarkerFuture = null;
      });
      if (PlatformInfos.isIOS) {
        room.client.updateIosBadge();
      }
      if (PlatformInfos.isMacOS) {
        room.client.updateMacOSBadge(room: room);
      }
    }
  }

  void removeReactionsOverlay() {
    if (reactionsOverlay?.mounted == true) {
      reactionsOverlay?.remove();
      Logs().v("Reactions overlay removed");
      reactionsOverlay = null;
    }
  }

  @override
  void dispose() {
    timeline?.cancelSubscriptions();
    timeline = null;
    inputFocus.removeListener(_inputFocusListener);
    overlayFocusNode.unfocus();
    overlayFocusNode.dispose();
    _isSearchMode = false;
    super.dispose();
  }

  TextEditingController searchController = TextEditingController();
  TextEditingController sendController = TextEditingController();
  quill.QuillController textEditorController = quill.QuillController.basic();

  ///TODO understand this
  void setSendingClient(Client c) {
    // first cancel typing with the old sending client
    if (currentlyTyping) {
      // no need to have the setting typing to false be blocking
      typingCoolDown?.cancel();
      typingCoolDown = null;
      room.setTyping(false);
      currentlyTyping = false;
    }
    // then set the new sending client
    setState(() => sendingClient = c);
  }

  void setActiveClient(Client c) => setState(() {
        matrix.setActiveClient(c);
      });

  Future<void> send() async {
    _storeInputTimeoutTimer?.cancel();
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('draft_$roomId');
    final client = matrix.client;
    final String currentText = getCurrentText().trim();
    if (currentText.isEmpty) return;
    var parseCommands = true;

    final commandMatch = RegExp(r'^\/(\w+)').firstMatch(currentText);
    if (commandMatch != null && !room.client.commands.keys.contains(commandMatch[1]!.toLowerCase())) {
      final dialogResult = await showOkCancelAlertDialog(
        context: context,
        useRootNavigator: false,
        title: l10n.commandInvalid,
        message: l10n.commandMissing(commandMatch[0]!),
        okLabel: l10n.sendAsText,
        cancelLabel: l10n.cancel,
      );
      if (dialogResult == OkCancelResult.cancel) return;
      parseCommands = false;
    }

    // ignore: unawaited_futures
    if (showTextEditor) {
      unawaited(room.sendEvent(
        {
          'serialized_delta': currentText,
          'msgtype': CustomMessageTypes.quillText,
          'body':
              '${await client.getProfileFromUserId(client.userID!).then((value) => value.displayName ?? value.userId)} sent you a composed message.\n\nYou currently unable to read this as your app version does not support this feature.\nUpdate to see these types of messages.\n\nhttps://play.google.com/store/apps/details?id=com.pageme.business&pli=1',
          // Add a custom message type
        },
        type: EventTypes.Message,
        inReplyTo: replyEvent,
        editEventId: editEvent?.eventId,
      ));
    } else {
      unawaited(room.sendTextEvent(currentText, inReplyTo: replyEvent, editEventId: editEvent?.eventId, parseMarkdown: true, parseCommands: parseCommands));
    }

    if (showTextEditor) {
      textEditorController.document = quill.Document();
    } else {
      sendController.value = TextEditingValue(
        text: pendingText,
        selection: const TextSelection.collapsed(offset: 0),
      );
    }

    setState(() {
      inputText = pendingText;
      replyEvent = null;
      editEvent = null;
      pendingText = '';
    });
  }

  Future<void> resetTextValues() async {
    await removeDraftMessage(room);
    setState(() {
      inputText = pendingText;
      replyEvent = null;
      editEvent = null;
      pendingText = '';
    });
  }

  void handleQuillCompletion() {
    if (!textEditorController.document.isEmpty()) {
      textEditorController.document = quill.Document();
    }
  }

  void handleTextCompletion() {
    sendController.value = TextEditingValue(
      text: pendingText,
      selection: const TextSelection.collapsed(offset: 0),
    );
  }

  void sendStickerAction() async {
    final sticker = await showModalBottomSheet<ImagePackImageContent>(
      context: context,
      useRootNavigator: false,
      builder: (c) => StickerPickerDialog(room: room),
    );
    if (sticker == null) return;
    final eventContent = <String, dynamic>{
      'body': sticker.body,
      if (sticker.info != null) 'info': sticker.info,
      'url': sticker.url.toString(),
    };
    // send the sticker
    await room.sendEvent(
      eventContent,
      type: EventTypes.Sticker,
    );
  }

  void voiceMessageAction() async {
    if (PlatformInfos.isAndroid) {
      final info = await DeviceInfoPlugin().androidInfo;
      if (info.version.sdkInt < 19) {
        showOkAlertDialog(
          context: context,
          title: l10n.unsupportedAndroidVersion,
          message: l10n.unsupportedAndroidVersionLong,
          okLabel: l10n.close,
        );
        return;
      }
    }

    if (await Record().hasPermission() == false) return;
    final result = await showDialog<RecordingResult>(
      context: context,
      useRootNavigator: false,
      builder: (c) => const RecordingDialog(),
    );
    if (result == null) return;
    final audioFile = File(result.path);
    final file = MatrixAudioFile(
      bytes: audioFile.readAsBytesSync(),
      name: audioFile.path,
    );
    await room.sendFileEvent(
      file,
      inReplyTo: replyEvent,
      extraContent: {
        'info': {
          ...file.info,
          'duration': result.duration,
        },
        'org.matrix.msc3245.voice': {},
        'org.matrix.msc1767.audio': {
          'duration': result.duration,
          'waveform': result.waveform,
        },
      },
    );
    setState(() {
      replyEvent = null;
    });
  }

  void attachmentPickerAction() {
    if (showAttachmentPicker) {
      inputFocus.requestFocus();
    } else {
      //inputFocus.unfocus();
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    }
    emojiPickerType = EmojiPickerType.keyboard;
    toggleAttachmentPicker();
  }

  void textEditorAction() {
    if (showTextEditor) {
      if (textEditorController.document.toPlainText().isNotEmpty) {
        sendController.text = textEditorController.document.toPlainText().trimRight();
      }
    } else {
      if (sendController.text.isNotEmpty) {
        final plainTextDelta = quill.Delta()..insert(sendController.text);
        textEditorController.document.replace(0, textEditorController.document.length - 1, plainTextDelta.toList().first.value);
      }
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    }
    toggleTextEditor();
  }

  String convertQuillDocumentToHtml(quill.Delta quillDelta) {
    final converter = QuillDeltaToHtmlConverter(
      quillDelta.toJson().cast<Map<String, dynamic>>(),
      // Optionally, you can provide a configuration for the converter.
      // For example, to adjust image or video handling, or to use custom CSS classes.
      ConverterOptions.forEmail(),
    );
    return converter.convert();
  }

  String getCurrentText() {
    if (showTextEditor) {
      final delta = textEditorController.document.toDelta();
      final serializedDelta = jsonEncode(delta.toJson());
      return serializedDelta;
    } else {
      return sendController.text;
    }
  }

  void emojiPickerAction() {
    if (showEmojiPicker) {
      inputFocus.requestFocus();
    } else {
      //inputFocus.unfocus();
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    }
    emojiPickerType = EmojiPickerType.keyboard;
    toggleEmojiPicker();
  }

  void _inputFocusListener() {
    if (showEmojiPicker && inputFocus.hasFocus) {
      emojiPickerType = EmojiPickerType.keyboard;
      setState(() => showEmojiPicker = false);
    }
    if (showAttachmentPicker && inputFocus.hasFocus) {
      emojiPickerType = EmojiPickerType.keyboard;
      setState(() => showAttachmentPicker = false);
    }
  }

  void sendLocationAction() async {
    await showDialog(
      context: context,
      useRootNavigator: false,
      builder: (c) => SendLocationDialog(room: room),
    );
  }

  String _getSelectedEventString() {
    var copyString = '';
    if (selectedEvents.length == 1) {
      if (selectedEvents.first.messageType == CustomMessageTypes.quillText) {
        return selectedEvents.first.getDisplayEvent(timeline!).calcQuillTextToPlainText(locals: MatrixLocals(l10n));
      } else {
        return selectedEvents.first.getDisplayEvent(timeline!).calcLocalizedBodyFallback(MatrixLocals(l10n));
      }
    }
    for (final event in selectedEvents) {
      if (copyString.isNotEmpty) copyString += '\n\n';
      if (event.messageType == CustomMessageTypes.quillText) {
        copyString += event.getDisplayEvent(timeline!).calcQuillTextToPlainText(locals: MatrixLocals(l10n));
      } else {
        copyString += event.getDisplayEvent(timeline!).calcLocalizedBodyFallback(MatrixLocals(l10n), withSenderNamePrefix: true);
      }
    }
    return copyString;
  }

  List<Map<String, dynamic>> _getSelectedForwardEvents() {
    final List<Map<String, dynamic>> shareEvents = [];
    if (selectedEvents.length == 1) {
      final Map<String, dynamic> shareEvent;
      if (selectedEvents.first.messageType == CustomMessageTypes.quillText) {
        if (selectedEvents.first.hasAttachment) {
          shareEvent = selectedEvents.first.toJson();
        } else {
          shareEvent = {
            'msgtype': 'm.text',
            'body': selectedEvents.first.getDisplayEvent(timeline!).calcQuillTextToPlainText(locals: MatrixLocals(l10n)),
          };
        }
        shareEvents.add(shareEvent);
      } else {
        final Map<String, dynamic> shareEvent;
        if (selectedEvents.first.hasAttachment) {
          shareEvent = selectedEvents.first.toJson();
        } else {
          shareEvent = {
            'msgtype': 'm.text',
            'body': selectedEvents.first.getDisplayEvent(timeline!).calcLocalizedBodyFallback(MatrixLocals(l10n)),
          };
        }
        shareEvents.add(shareEvent);
      }
    }
    for (final event in selectedEvents) {
      final Map<String, dynamic> shareEvent;
      if (event.messageType == CustomMessageTypes.quillText) {
        if (event.hasAttachment) {
          shareEvent = event.content;
        } else {
          shareEvent = {
            'msgtype': 'm.text',
            'body': event.getDisplayEvent(timeline!).calcQuillTextToPlainText(locals: MatrixLocals(l10n)),
          };
        }
      } else {
        if (event.hasAttachment) {
          shareEvent = event.content;
        } else {
          shareEvent = {
            'msgtype': 'm.text',
            'body': event.getDisplayEvent(timeline!).calcLocalizedBodyFallback(MatrixLocals(l10n), withSenderNamePrefix: true),
          };
        }
      }
      shareEvents.add(shareEvent);
    }
    return shareEvents;
  }

  void copyEventsAction({Event? event}) {
    if (event != null) {
      onSelectMessage(event);
    }
    Clipboard.setData(ClipboardData(text: _getSelectedEventString()));

    setState(() {
      showAttachmentPicker = false;
      showEmojiPicker = false;
      selectedEvents.clear();
    });
  }

  void reportEventAction() async {
    final event = selectedEvents.single;
    final score = await showConfirmationDialog<int>(
        context: context,
        title: l10n.reportMessage,
        message: l10n.howOffensiveIsThisContent,
        cancelLabel: l10n.cancel,
        okLabel: l10n.ok,
        actions: [
          AlertDialogAction(
            key: -100,
            label: l10n.extremeOffensive,
          ),
          AlertDialogAction(
            key: -50,
            label: l10n.offensive,
          ),
          AlertDialogAction(
            key: 0,
            label: l10n.inoffensive,
          ),
        ]);
    if (score == null) return;
    final reason = await showTextInputDialog(
        useRootNavigator: false,
        context: context,
        title: l10n.whyDoYouWantToReportThis,
        okLabel: l10n.ok,
        cancelLabel: l10n.cancel,
        textFields: [DialogTextField(hintText: l10n.reason)]);
    if (reason == null || reason.single.isEmpty) return;
    final result = await showFutureLoadingDialog(
      context: context,
      future: () => matrix.client.reportContent(
        event.roomId!,
        event.eventId,
        reason: reason.single,
        score: score,
      ),
    );
    if (result.error != null) return;
    setState(() {
      showAttachmentPicker = false;
      showEmojiPicker = false;
      selectedEvents.clear();
    });
    scaffoldMessenger.showSnackBar(SnackBar(
      content: Text(
        l10n.contentHasBeenReported,
        style: TextStyle(color: theme.colorScheme.onTertiaryContainer),
      ),
      backgroundColor: theme.colorScheme.tertiaryContainer,
      closeIconColor: theme.colorScheme.onTertiaryContainer,
    ));
  }

  void redactEventsAction({Event? event}) async {
    removeReactionsOverlay();
    if (event != null) {
      onSelectMessage(event);
    }
    final confirmed = await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: l10n.messageWillBeRemovedWarning,
          okLabel: l10n.remove,
          cancelLabel: l10n.cancel,
        ) ==
        OkCancelResult.ok;
    if (!confirmed) return;

    if (!mounted) return;
    final List<String> eventsToUnpin = [];
    await showFutureLoadingDialog(
        context: context,
        onError: (error) {
          Logs().e('Error: ${error.toString()}');
          // return a string representation of the error
          return error.toString();
        },
        backLabel: 'Back',
        title: 'Removing events',
        barrierDismissible: true,
        future: () async {
          for (final event in selectedEvents) {
            if (room.pinnedEventIds.contains(event.eventId)) {
              eventsToUnpin.add(event.eventId);
            }
            //for rate limits.
            Logs().v("Paused before requests: ${DateTime.now()}");
            await Future.delayed(const Duration(milliseconds: 200));
            Logs().v("Paused after requests: ${DateTime.now()}");
            try {
              if (event.status.isSent) {
                if (event.canRedact) {
                  await event.redactEvent();
                } else {
                  final client = currentRoomBundle.firstWhere((cl) => selectedEvents.first.senderId == cl!.userID, orElse: () => null);
                  if (client == null) {
                    return;
                  }
                  final room = client.getRoomById(roomId)!;
                  await Event.fromJson(event.toJson(), room).redactEvent();
                }
              } else {
                await event.remove();
              }
            } catch (e) {
              Logs().e('Error: ${e.toString()}');
              rethrow;
            }
          }
          final tempPinnedEvents = room.pinnedEventIds;
          tempPinnedEvents.removeWhere((element) => eventsToUnpin.contains(element));
          await room.setPinnedEvents(tempPinnedEvents);
        });
    setState(() {
      showAttachmentPicker = false;
      showEmojiPicker = false;
      selectedEvents.clear();
    });
  }

  List<Client?> get currentRoomBundle {
    final clients = matrix.currentBundle!;
    clients.removeWhere((c) => c!.getRoomById(roomId) == null);
    return clients;
  }

  bool get canRedactSelectedEvents {
    final clients = matrix.currentBundle;
    for (final event in selectedEvents) {
      if (event.canRedact == false && !(clients!.any((cl) => event.senderId == cl!.userID))) return false;
    }
    return true;
  }

  bool canRedactEvent(Event event) {
    final clients = matrix.currentBundle;
    if (event.canRedact == false && !(clients!.any((cl) => event.senderId == cl!.userID))) return false;
    return true;
  }

  bool get canEditSelectedEvents {
    if (selectedEvents.length != 1 || !selectedEvents.first.status.isSent) {
      return false;
    }
    return currentRoomBundle.any((cl) => selectedEvents.first.senderId == cl!.userID);
  }

  bool canEditEvent(Event event) {
    return currentRoomBundle.any((cl) => event.senderId == cl!.userID);
  }

  void forwardEventsAction({Event? event}) async {
    removeReactionsOverlay();
    if (event != null) {
      onSelectMessage(event);
    }
    final Map<String, dynamic> shareEvent;
    if (selectedEvents.length == 1) {
      shareEvent = selectedEvents.first.getDisplayEvent(timeline!).content;
      //matrix.shareContent = [shareEvent];
      fileHandlerBloc.add(ShareEventAdditionRequested(shareEvents: [shareEvent]));
    } else {
      _getSelectedForwardEvents();
      //matrix.shareContent = _getSelectedForwardEvents();
      fileHandlerBloc.add(ShareEventAdditionRequested(shareEvents: _getSelectedForwardEvents()));
    }
    setState(() => selectedEvents.clear());
    context.go('/rooms');
  }

  Future<void> sendAgainAction() async {
    final event = selectedEvents.first;
    if (event.status.isError) {
      unawaited(event.sendAgain());
    }
    final allEditEvents = event.aggregatedEvents(timeline!, RelationshipTypes.edit).where((e) => e.status.isError);
    for (final e in allEditEvents) {
      unawaited(e.sendAgain());
    }
    setState(() => selectedEvents.clear());
  }

  void replyAction({Event? replyTo}) {
    removeReactionsOverlay();

    setState(() {
      replyEvent = replyTo ?? selectedEvents.first;
      selectedEvents.clear();
    });
    if (showTextEditor) {
      quillFocus.requestFocus();
    } else {
      inputFocus.requestFocus();
    }
  }

  void scrollToEventId(String eventId) async {
    var eventIndex = timeline!.events.indexWhere((e) => e.eventId == eventId);
    if (eventIndex == -1) {
      // event id not found...maybe we can fetch it?
      // the try...finally is here to start and close the loading dialog reliably
      await showFutureLoadingDialog(
          context: context,
          future: () async {
            // okay, we first have to fetch if the event is in the room
            try {
              final event = await timeline!.getEventById(eventId);
              if (event == null) {
                // event is null...meaning something is off
                return;
              }
            } catch (err) {
              if (err is MatrixException && err.errcode == 'M_NOT_FOUND') {
                // event wasn't found, as the server gave a 404 or something
                return;
              }
              rethrow;
            }
            // okay, we know that the event *is* in the room
            while (eventIndex == -1) {
              if (!canLoadMore) {
                // we can't load any more events but still haven't found ours yet...better stop here
                return;
              }
              try {
                await timeline!.requestHistory(historyCount: _loadHistoryCount);
              } catch (err) {
                if (err is TimeoutException) {
                  // loading the history timed out...so let's do nothing
                  return;
                }
                rethrow;
              }
              eventIndex = timeline!.events.indexWhere((e) => e.eventId == eventId);
            }
          });
    }
    if (!mounted) {
      return;
    }
    await scrollController.scrollToIndex(
      eventIndex,
      preferPosition: AutoScrollPosition.middle,
    );
    _updateScrollController();
  }

  void scrollDown() => scrollController.jumpTo(0);

  void onEmojiSelected(_, Emoji? emoji, Event? event) {
    switch (emojiPickerType) {
      case EmojiPickerType.reaction:
        sendEmojiReaction(emoji, event);
        break;
      case EmojiPickerType.keyboard:
        typeEmoji(emoji);
        onInputBarChanged(sendController.text);
        break;
    }
  }

  void sendEmojiReaction(Emoji? emoji, Event? event) {
    removeReactionsOverlay();
    setState(() => showEmojiPicker = false);
    if (emoji == null) return;
    // make sure we don't send the same emoji twice
    if (_allReactionEvents.any((e) {
      final relatesTo = e.content['m.relates_to'];
      return relatesTo is Map && relatesTo['key'] == emoji.emoji;
    })) return;
    return sendEmojiAction(emoji.emoji, event);
  }

  void typeEmoji(Emoji? emoji) {
    if (emoji == null) return;
    final text = sendController.text;
    final selection = sendController.selection;
    final newText = sendController.text.isEmpty ? emoji.emoji : text.replaceRange(selection.start, selection.end, emoji.emoji);
    sendController.value = TextEditingValue(
      text: newText,
      selection: TextSelection.collapsed(
        // don't forget an UTF-8 combined emoji might have a length > 1
        offset: selection.baseOffset + emoji.emoji.length,
      ),
    );
  }

  late Iterable<Event> _allReactionEvents;

  void emojiPickerBackspace() {
    switch (emojiPickerType) {
      case EmojiPickerType.reaction:
        setState(() => showEmojiPicker = false);
        break;
      case EmojiPickerType.keyboard:
        sendController
          ..text = sendController.text.characters.skipLast(1).toString()
          ..selection = TextSelection.fromPosition(TextPosition(offset: sendController.text.length));
        break;
    }
  }

  void pickEmojiReactionAction(Iterable<Event> allReactionEvents, Event? event) async {
    _allReactionEvents = allReactionEvents;
    emojiPickerType = EmojiPickerType.reaction;
    reactionEvent = event;
    setState(() => showEmojiPicker = true);
  }

  void sendEmojiAction(String? emoji, Event? event) async {
    removeReactionsOverlay();
    if (event != null) {
      await room.sendReaction(
        event.eventId,
        emoji!,
      );
    } else {
      final events = List<Event>.from(selectedEvents);
      setState(() => selectedEvents.clear());
      for (final event in events) {
        await room.sendReaction(
          event.eventId,
          emoji!,
        );
      }
    }
  }

  void clearSelectedEvents() => setState(() {
        selectedEvents.clear();
        showAttachmentPicker = false;
        showEmojiPicker = false;
        showTextEditor = false;
      });

  void clearSingleSelectedEvent() {
    if (selectedEvents.length <= 1) {
      removeReactionsOverlay();
      clearSelectedEvents();
    }
  }

  void editSelectedEventAction({Event? event}) {
    removeReactionsOverlay();

    if (event != null) {
      onSelectMessage(event);
    }
    final client = currentRoomBundle.firstWhere((cl) => selectedEvents.first.senderId == cl!.userID, orElse: () => null);
    if (client == null) {
      return;
    }
    setSendingClient(client);
    if (selectedEvents.first.messageType == CustomMessageTypes.quillText) {
      editEvent = selectedEvents.first.getDisplayEvent(timeline!);
      Logs().v("editSelectedEventAction - ${editEvent!.toJson().toString()}");
      final serializedDelta = editEvent?.content['serialized_delta'];
      final delta = quill.Delta.fromJson(jsonDecode(serializedDelta as String));
      setState(() {
        selectedEvents.clear();
        textEditorController.document = quill.Document.fromDelta(delta);
      });
      textEditorAction();
    } else {
      setState(() {
        pendingText = sendController.text;
        editEvent = selectedEvents.first;
        selectedEvents.clear();
        inputText = sendController.text = editEvent!.getDisplayEvent(timeline!).calcLocalizedBodyFallback(MatrixLocals(l10n), withSenderNamePrefix: false, hideReply: true);
      });
    }
    if (showTextEditor) {
      quillFocus.requestFocus();
    } else {
      inputFocus.requestFocus();
    }
  }

  void goToNewRoomAction() async {
    if (OkCancelResult.ok !=
        await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: l10n.goToTheNewRoom,
          message: room.getState(EventTypes.RoomTombstone)!.parsedTombstoneContent.body,
          okLabel: l10n.ok,
          cancelLabel: l10n.cancel,
        )) {
      return;
    }
    final result = await showFutureLoadingDialog(
      context: context,
      future: () => room.client.joinRoom(room.getState(EventTypes.RoomTombstone)!.parsedTombstoneContent.replacementRoom),
    );
    await showFutureLoadingDialog(
      context: context,
      future: room.leave,
    );
    if (result.error == null) {
      context.go("/rooms/${result.result!}");
    }
  }

  void onSelectMessage(Event event) {
    if (!event.redacted) {
      if (selectedEvents.contains(event)) {
        setState(
          () => selectedEvents.remove(event),
        );
      } else {
        setState(
          () => selectedEvents.add(event),
        );
      }
      if (selectedEvents.isEmpty){
        removeReactionsOverlay();
      }
      selectedEvents.sort(
        (a, b) => a.originServerTs.compareTo(b.originServerTs),
      );
    }
  }

  int? findChildIndexCallback(Key key, Map<String, int> thisEventsKeyMap) {
    // this method is called very often. As such, it has to be optimized for speed.
    if (key is! ValueKey) {
      return null;
    }
    final eventId = key.value;
    if (eventId is! String) {
      return null;
    }
    // first fetch the last index the event was at
    final index = thisEventsKeyMap[eventId];
    if (index == null) {
      return null;
    }
    // we need to +1 as 0 is the typing thing at the bottom
    return index + 1;
  }

  Future<void> onInputBarSubmitted(_) async {
    await send();
    focusScope.requestFocus(inputFocus);
  }

  void onAddPopupMenuButtonSelected(String choice) {
    if (choice == 'sticker') {
      sendStickerAction();
    }
    if (choice == 'location') {
      sendLocationAction();
    }
  }

  Future<void> unpinEvent(String eventId) async {
    final response = await showOkCancelAlertDialog(
      context: context,
      title: l10n.unpin,
      message: l10n.confirmEventUnpin,
      okLabel: l10n.unpin,
      cancelLabel: l10n.cancel,
    );
    if (response == OkCancelResult.ok) {
      final events = room.pinnedEventIds..removeWhere((oldEvent) => oldEvent == eventId);
      await showFutureLoadingDialog(
        context: context,
        future: () => room.setPinnedEvents(events),
      );
    }
  }

  void pinEvent({Event? event}) {
    if (event != null) {
      if (event.redacted) return;
      onSelectMessage(event);
    }
    final room = this.room;
    if (room == null) return;
    final pinnedEventIds = room.pinnedEventIds;
    final selectedEventIds = selectedEvents.map((e) => e.eventId).toSet();
    final unpin = selectedEventIds.length == 1 && pinnedEventIds.contains(selectedEventIds.single);
    if (unpin) {
      pinnedEventIds.removeWhere(selectedEventIds.contains);
    } else {
      pinnedEventIds.addAll(selectedEventIds);
    }
    showFutureLoadingDialog(
      context: context,
      future: () async => room.setPinnedEvents(pinnedEventIds),
    );
  }

  Timer? _storeInputTimeoutTimer;
  static const Duration _storeInputTimeout = Duration(milliseconds: 500);

  void setDraftMessage(String text) {
    _storeInputTimeoutTimer?.cancel();
    _storeInputTimeoutTimer = Timer(_storeInputTimeout, () async {
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString('draft_$roomId', text);
    });
  }

  Future<void> removeDraftMessage(Room room) async {
    _storeInputTimeoutTimer?.cancel();
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('draft_${room.id}');
  }

  void onInputBarChanged(String text) {
    setDraftMessage(text);
    setReadMarker();
    if (text.endsWith(' ') && matrix.hasComplexBundles) {
      final clients = currentRoomBundle;
      for (final client in clients) {
        final prefix = client!.sendPrefix;
        if ((prefix.isNotEmpty) && text.toLowerCase() == '${prefix.toLowerCase()} ') {
          setSendingClient(client);
          setState(() {
            inputText = '';
            sendController.text = '';
          });
          return;
        }
      }
    }
    typingCoolDown?.cancel();
    typingCoolDown = Timer(const Duration(seconds: 2), () {
      typingCoolDown = null;
      currentlyTyping = false;
      room.setTyping(false);
    });
    typingTimeout ??= Timer(const Duration(seconds: 30), () {
      typingTimeout = null;
      currentlyTyping = false;
    });
    if (!currentlyTyping) {
      currentlyTyping = true;
      room.setTyping(true, timeout: const Duration(seconds: 30).inMilliseconds);
    }
    setState(() => inputText = text);
  }

  void showEventInfo([Event? event]) => (event ?? selectedEvents.single).showInfoDialog(context);

  void onPhoneButtonTap() async {
    // VoIP required Android SDK 21
    if (PlatformInfos.isAndroid) {
      DeviceInfoPlugin().androidInfo.then((value) {
        if (value.version.sdkInt < 21) {
          Navigator.pop(context);
          showOkAlertDialog(
            context: context,
            title: l10n.unsupportedAndroidVersion,
            message: l10n.unsupportedAndroidVersionLong,
            okLabel: l10n.close,
          );
        }
      });
    }
    final callType = await showModalActionSheet<CallType>(
      style: AdaptiveStyle.material,
      context: context,
      title: l10n.warning,
      message: l10n.videoCallsBetaWarning,
      cancelLabel: l10n.cancel,
      actions: [
        SheetAction(
          label: l10n.voiceCall,
          icon: Icons.phone_outlined,
          key: CallType.kVoice,
        ),
        SheetAction(
          label: l10n.videoCall,
          icon: Icons.video_call_outlined,
          key: CallType.kVideo,
        ),
      ],
    );
    if (callType == null) return;

    final success = await showFutureLoadingDialog(context: context, future: () => matrix.voipPlugin!.voip.requestTurnServerCredentials());
    if (success.result != null) {
      final voipPlugin = matrix.voipPlugin;
      await voipPlugin!.voip.inviteToCall(room.id, callType).catchError((e) {
        scaffoldMessenger.showSnackBar(
          SnackBar(content: Text((e as Object).toLocalizedString(context))),
        );
      });
    } else {
      await showOkAlertDialog(
        context: context,
        title: l10n.unavailable,
        okLabel: l10n.next,
        useRootNavigator: false,
      );
    }
  }

  void cancelReplyEventAction() => setState(() {
        if (editEvent != null) {
          inputText = sendController.text = pendingText;
          pendingText = '';
        }
        replyEvent = null;
        editEvent = null;
      });

  @override
  Widget build(BuildContext context) => ChatControllerProvider(chatController: this, child: ChatView(this));
}

enum EmojiPickerType { reaction, keyboard }
