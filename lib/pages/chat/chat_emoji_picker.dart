import 'package:flutter/material.dart';

import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_persistent_keyboard_height/flutter_persistent_keyboard_height.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/cubits/image_handler/image_handler_bloc.dart';

import '../../utils/platform_infos.dart';
import 'chat.dart';
import 'package:matrix/matrix.dart';

class ChatEmojiPicker extends StatelessWidget {
  final ChatController controller;
  const ChatEmojiPicker(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = PlatformInfos.isDesktop || PlatformInfos.isWeb;
    final double screenHeight = MediaQuery.sizeOf(context).height;
    final double screenWidth = MediaQuery.sizeOf(context).width;
    Logs().i(screenWidth.toString());
    Logs().i((screenWidth / 70).ceil().toString());
    final double keyboardHeight = isDesktop
        ? screenHeight / 3
        : (PersistentKeyboardHeight.of(context).keyboardHeight == 0)
            ? 250
            : PersistentKeyboardHeight.of(context).keyboardHeight;
    return AnimatedContainer(
      duration: const Duration(milliseconds: 0),
      height: keyboardHeight,
      child: controller.showEmojiPicker
          ? EmojiPicker(
              config: Config(
                enableSkinTones: true,
                bgColor: Theme.of(context).scaffoldBackgroundColor,
                columns: PageMeThemes.isColumnMode(context) ? (screenWidth / 70).ceil().toInt() : 8,
                emojiSizeMax: PageMeThemes.isColumnMode(context) ? screenWidth / 70 : 32,
                checkPlatformCompatibility: true,
                emojiTextStyle: PlatformInfos.isWeb ? GoogleFonts.notoColorEmoji() : null,
              ),
              onEmojiSelected: (_,emoji) => controller.onEmojiSelected(_,emoji, controller.reactionEvent),
              onBackspacePressed: controller.emojiPickerBackspace,
            )
          : null,
    );
  }
}
