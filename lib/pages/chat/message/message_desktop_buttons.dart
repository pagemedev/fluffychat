import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';
import 'dart:math' as math;
import '../../../config/flavor_config.dart';
import '../../../config/themes.dart';
import '../../../utils/platform_infos.dart';
import '../../../widgets/context_menu/context_menu_area.dart';
import '../../../widgets/matrix.dart';
import '../chat.dart';

enum MessageDesktopButtonsType { reply, download, menu }

class MessageDesktopButtons extends StatefulWidget {
  final Event event;
  final Event displayEvent;
  final GlobalKey<State<StatefulWidget>> menuButtonKey;
  final ValueNotifier<bool> showDesktopButtons;
  const MessageDesktopButtons({super.key, required this.event, required this.menuButtonKey, required this.displayEvent, required this.showDesktopButtons});

  @override
  State<MessageDesktopButtons> createState() => _MessageDesktopButtonsState();
}

class _MessageDesktopButtonsState extends State<MessageDesktopButtons> with TickerProviderStateMixin {
  late AnimationController _controller;
  late ChatController chatController;
  static const double desktopButtonIconSize = 15;
  static const double desktopButtonTotalSize = 35;
  late AnimationController _animationController;
  List<MessageDesktopButtonsType> isPlaying = [];

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);

    _animationController = AnimationController(vsync: this, duration: const Duration(seconds: 5));
  }

  @override
  void dispose() {
    _controller.dispose();
    _animationController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    chatController = Chat.of(context)!;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: widget.showDesktopButtons,
      builder: (context, bool showDesktopButtons, _) {
        final ownMessage = widget.event.senderId == Matrix.of(context).client.userID;
        final hasAttachment = widget.displayEvent.hasAttachment;
        final canEditEvent = chatController.canEditEvent(widget.event);
        final canRedactEvent = chatController.canRedactEvent(widget.event);
        final selectMode = chatController.selectMode;
        if (!showDesktopButtons) {
          return const SizedBox.shrink();
        }

        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: [
            SizedBox(
              height: desktopButtonTotalSize,
              child: IconButton(
                onPressed: () {
                  chatController.replyAction(replyTo: widget.displayEvent);
                },
                style: IconButton.styleFrom(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(FlavorConfig.borderRadius / 2),
                  ),
                ),
                tooltip: 'Reply',
                color: Theme.of(context).colorScheme.onTertiaryContainer,
                icon: Material(
                  elevation: 4,
                  borderRadius: BorderRadius.circular(FlavorConfig.borderRadius / 2),
                  color: Theme.of(context).colorScheme.tertiaryContainer,
                  child: const Padding(
                    padding: EdgeInsets.all(6.0),
                    child: Icon(
                      Icons.reply,
                      size: desktopButtonIconSize,
                    ),
                  ),
                ),
              ),
            ),
            if (hasAttachment)
              IconButton(
                onPressed: () async {
                  _animationController.reset();
                  unawaited(_animationController.animateTo(0.8, curve: Curves.ease));
                  await widget.displayEvent.saveFile(context).whenComplete(() async {
                    await _animationController.animateTo(1, curve: Curves.ease);
                    await Future.delayed(const Duration(seconds: 2));
                    _animationController.reset();
                  });
                },
                padding: EdgeInsets.zero,
                color: Theme.of(context).colorScheme.onSurface,
                style: IconButton.styleFrom(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(FlavorConfig.borderRadius / 2),
                  ),
                ),
                icon: Material(
                  borderRadius: BorderRadius.circular(FlavorConfig.borderRadius / 2),
                  color: Theme.of(context).colorScheme.inverseSurface,
                  child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: SizedBox.square(
                      dimension: desktopButtonIconSize,
                      child: Lottie.asset(
                        'assets/download_button.json',
                        controller: _animationController,
                        alignment: Alignment.center,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),
            SizedBox(
              height: desktopButtonTotalSize,
              child: IconButton(
                key: widget.menuButtonKey,
                onPressed: () async {
                  try {
                    const double verticalPadding = 0;
                    const double width = 200;
                    final RenderBox box = widget.menuButtonKey.currentContext?.findRenderObject() as RenderBox;

                    final double totalHeight = calculateMenuHeight(selectMode, canEditEvent, canRedactEvent);

                    final Offset position = calculateContextMenuPosition(ownMessage, hasAttachment, box, width, totalHeight);

                    Logs().v(_buildContextMenu(chatController, context, context, position).length.toString());
                    await showContextMenu(
                      position,
                      context,
                      (c) => _buildContextMenu(chatController, context, c, position),
                      verticalPadding,
                      width,
                    );
                  } catch (e){
                    Logs().e("MessageDesktopButtons - $e");
                  }
                },
                tooltip: 'Menu',
                style: IconButton.styleFrom(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(FlavorConfig.borderRadius / 2),
                  ),
                ),
                color: Theme.of(context).colorScheme.onSecondaryContainer,
                icon: Material(
                  elevation: 4,
                  borderRadius: BorderRadius.circular(FlavorConfig.borderRadius / 2),
                  color: Theme.of(context).colorScheme.secondaryContainer,
                  child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Transform(
                      alignment: Alignment.center,
                      transform: Matrix4.rotationY(math.pi),
                      child: const Icon(
                        Icons.more_horiz,
                        size: desktopButtonIconSize,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Offset calculateContextMenuPosition(bool ownMessage, bool hasAttachment, RenderBox box, double width, double totalHeight) {
    Offset offset;
    offset = box.localToGlobal(
      Offset(
        _calculateXPosRelative(ownMessage, width, hasAttachment),
        _calculateYPosRelative(),
      ),
    );
    return offset;
  }

  double _calculateXPosRelative(bool ownMessage, double width, bool hasAttachment) {
    double xPosRelative;
    if (ownMessage) {
      xPosRelative = desktopButtonTotalSize - width;
      //offset = box.localToGlobal(Offset(desktopButtonTotalSize - width, 0));
    } else {
      if (hasAttachment) {
        xPosRelative = -2 * desktopButtonTotalSize;
        //offset = box.localToGlobal(const Offset(-2 * desktopButtonTotalSize, 0));
      } else {
        xPosRelative = -desktopButtonTotalSize;
        //offset = box.localToGlobal(const Offset(-desktopButtonTotalSize, 0));
      }
    }
    return xPosRelative;
  }

  double _calculateYPosRelative() {
    double yPosRelative = 0;
    if (PlatformInfos.isWindows) {
      yPosRelative = -desktopButtonTotalSize * 0.75;
    }
    return yPosRelative;
  }

  double calculateMenuHeight(bool selectMode, bool canEdit, bool canRedact) {
    int itemCount = 0; // Initial count
    int dividerCount = 0;
    int extraSize = (PlatformInfos.isWindows || PlatformInfos.isLinux) ? 36 : 0;
    const int verticalOuterPadding = 0;
    const double itemHeight = 48.0; // Height for each item
    const double dividerHeight = 2.0; // Height for the divider
    // Depending on the condition, increment the count
    if (!selectMode) itemCount++; // Reply
    if (!selectMode) dividerCount++; // Reply Divider
    itemCount++; // React
    itemCount++; // Copy Text
    itemCount++; // Forward Message
    if (canEdit && !selectMode) itemCount++; // Edit
    itemCount++; // Pin or Unpin
    itemCount++; // Clear Selection
    if (canRedact) dividerCount++;
    if (canRedact) itemCount++; // Delete
    // Add divider heights
    final double totalDividerHeight = dividerHeight * dividerCount; // Two dividers are present
    // Calculate the total height
    final double totalHeight = (itemCount * itemHeight) + totalDividerHeight + verticalOuterPadding + extraSize;
    Logs().v('itemCount: $itemCount, totalHeight: $totalHeight, itemHeight: $itemHeight');
    return totalHeight;
  }

  List<Widget> _buildContextMenu(ChatController? controller, BuildContext context, BuildContext c, Offset menuButtonPosition) {
    final TextStyle contextMenuTextStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w500,
      color: Theme.of(context).colorScheme.onBackground,
    );
    const double contextMenuIconSize = 20; // Suggestion 3: Use constant for repeated values
    const EdgeInsets listItemPadding = EdgeInsets.fromLTRB(7, 7, 17, 7); // Suggestion 3
    const double columnWidthFactor = 1.5;
    final Color contextMenuIconColor = Theme.of(context).colorScheme.onBackground;

    if (controller == null) {
      return [
        ListTile(
          title: Text(
            'No Actions',
            style: contextMenuTextStyle,
          ),
          dense: true,
        )
      ];
    }
    final pinnedEventIds = controller.room?.pinnedEventIds;
    final pinned = pinnedEventIds!.contains(widget.event.eventId);
    final reactKey = GlobalKey(debugLabel: "reactKey");

    return [
      if (!controller.selectMode)
        ListTile(
          key: UniqueKey(),
          dense: true,
          hoverColor: listTileHoverSplashColor(context),
          splashColor: listTileHoverSplashColor(context),
          title: Text('Reply', style: contextMenuTextStyle.copyWith(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.w600)),
          trailing: Icon(
            Icons.reply_outlined,
            size: contextMenuIconSize,
            color: Theme.of(context).colorScheme.primary,
          ),
          onTap: () async => _replyAction(c, controller),
        ),
      if (!controller.selectMode)
        Divider(
          key: UniqueKey(),
          height: 2,
          endIndent: 8,
          indent: 8,
        ),
      SizedBox(
        height: 48,
        child: ListTile(
            key: reactKey,
            dense: true,
            hoverColor: listTileHoverSplashColor(context),
            splashColor: listTileHoverSplashColor(context),
            title: Text('React', style: contextMenuTextStyle.copyWith(fontWeight: FontWeight.w600)),
            trailing: Icon(
              Icons.add_reaction_outlined,
              size: contextMenuIconSize,
              color: contextMenuIconColor,
            ),
            onTap: () {
              //if (controller.reactionsOverlay != null && controller.reactionsOverlay!.mounted) {
              //  return;
              // }
              Navigator.of(c).pop();
              controller.showReactionsPicker(context: context, touchPosition: menuButtonPosition, event: widget.event);
            }),
      ),
      ListTile(
        key: UniqueKey(),
        dense: true,
        hoverColor: listTileHoverSplashColor(context),
        splashColor: listTileHoverSplashColor(context),
        title: Text(controller.selectMode ? 'Copy All Text' : 'Copy Text', style: contextMenuTextStyle),
        trailing: Transform(
          alignment: Alignment.center,
          transform: Matrix4.rotationY(math.pi),
          child: Icon(
            controller.selectMode ? Icons.copy_all_outlined : Icons.copy_outlined,
            size: contextMenuIconSize,
            color: contextMenuIconColor,
          ),
        ),
        onTap: () async => _copyEventsAction(c, controller),
      ),
      ListTile(
        key: UniqueKey(),
        dense: true,
        hoverColor: listTileHoverSplashColor(context),
        splashColor: listTileHoverSplashColor(context),
        title: Text(controller.selectMode ? 'Forward Messages' : 'Forward Message', style: contextMenuTextStyle),
        trailing: Transform(
          alignment: Alignment.center,
          transform: Matrix4.rotationY(math.pi),
          child: Icon(
            controller.selectMode ? Icons.reply_all_outlined : Icons.reply_outlined,
            size: contextMenuIconSize,
            color: contextMenuIconColor,
          ),
        ),
        onTap: () async => _forwardEventsAction(c, controller),
      ),
      if (controller.canEditEvent(widget.event) && !controller.selectMode)
        ListTile(
          key: UniqueKey(),
          dense: true,
          hoverColor: listTileHoverSplashColor(context),
          splashColor: listTileHoverSplashColor(context),
          title: Text('Edit Message', style: contextMenuTextStyle),
          trailing: Icon(
            Icons.edit_outlined,
            size: contextMenuIconSize,
            color: contextMenuIconColor,
          ),
          onTap: () async => _editEventsAction(c, controller),
        ),
      ListTile(
        key: UniqueKey(),
        dense: true,
        hoverColor: listTileHoverSplashColor(context),
        splashColor: listTileHoverSplashColor(context),
        title: Text(
            controller.selectMode
                ? pinned
                    ? 'Unpin Messages'
                    : 'Pin Messages'
                : pinned
                    ? 'Unpin Message'
                    : 'Pin Message',
            style: contextMenuTextStyle),
        trailing: Transform(
          alignment: Alignment.center,
          transform: Matrix4.rotationY(math.pi),
          child: Icon(
            pinned ? Icons.push_pin : Icons.push_pin_outlined,
            size: contextMenuIconSize,
            color: contextMenuIconColor,
          ),
        ),
        onTap: () async => _pinEventsAction(c, controller),
      ),
      ListTile(
        key: UniqueKey(),
        dense: true,
        hoverColor: listTileHoverSplashColor(context),
        splashColor: listTileHoverSplashColor(context),
        trailing: Icon(
          (controller.selectMode) ? Icons.deselect : Icons.select_all,
          size: contextMenuIconSize,
          color: contextMenuIconColor,
        ),
        title: Text((controller.selectMode) ? 'Clear Selection' : 'Select multiple', style: contextMenuTextStyle),
        onTap: () async => _clearSelectionAction(c, controller),
      ),
      if (controller.canRedactEvent(widget.event))
        Divider(
          key: UniqueKey(),
          height: 2,
          endIndent: 8,
          indent: 8,
        ),
      if (controller.canRedactEvent(widget.event))
        ListTile(
          key: UniqueKey(),
          dense: true,
          hoverColor: listTileHoverSplashColor(context),
          splashColor: listTileHoverSplashColor(context),
          title: Text(controller.selectMode ? 'Delete Messages' : 'Delete Message', style: contextMenuTextStyle.copyWith(color: Colors.redAccent, fontWeight: FontWeight.w600)),
          trailing: Icon(
            controller.selectMode ? Icons.delete_sweep_outlined : Icons.delete_outline,
            size: contextMenuIconSize,
            color: Colors.redAccent,
          ),
          onTap: () async => _redactEventAction(c, controller),
        )
    ];
  }

  Color listTileHoverSplashColor(BuildContext context) =>
      PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiary.withOpacity(0.2) : Theme.of(context).colorScheme.primaryContainer.withOpacity(0.2);

  /// This method executes the reply action for a chat message.
  /// It takes a context and a ChatController as arguments.
  /// It does not return anything.
  void _replyAction(BuildContext c, ChatController controller) async {
    Navigator.of(c).pop();
    controller.replyAction(replyTo: widget.event.getDisplayEvent(controller.timeline!));
  }

  /// This method executes the copy events action for a chat message.
  /// It takes a context and a ChatController as arguments.
  /// It does not return anything.
  void _copyEventsAction(BuildContext c, ChatController controller) async {
    Navigator.of(c).pop();
    controller.selectMode ? controller.copyEventsAction : controller.copyEventsAction(event: widget.event);
  }

  /// This method executes the forward events action for a chat message.
  /// It takes a context and a ChatController as arguments.
  /// It does not return anything.
  void _forwardEventsAction(BuildContext c, ChatController controller) async {
    Navigator.of(c).pop();
    controller.selectMode ? controller.forwardEventsAction : controller.forwardEventsAction(event: widget.event);
  }

  /// This method executes the pin events action for a chat message.
  /// It takes a context and a ChatController as arguments.
  /// It does not return anything.
  void _pinEventsAction(BuildContext c, ChatController controller) async {
    Navigator.of(c).pop();
    controller.selectMode ? controller.pinEvent() : controller.pinEvent(event: widget.event);
    controller.clearSelectedEvents();
  }

  /// This method executes the edit events action for a chat message.
  /// It takes a context and a ChatController as arguments.
  /// It does not return anything.
  void _editEventsAction(BuildContext c, ChatController controller) async {
    Navigator.of(c).pop();
    controller.editSelectedEventAction(event: widget.event.getDisplayEvent(controller.timeline!));
  }

  /// This method executes the clear selection action for a chat message.
  /// It takes a context and a ChatController as arguments.
  /// It does not return anything.
  void _clearSelectionAction(BuildContext c, ChatController controller) async {
    Navigator.of(c).pop();
    (controller.selectMode) ? controller.clearSelectedEvents() : controller.onSelectMessage(widget.event);
  }

  /// This method executes the redact event action for a chat message.
  /// It takes a context and a ChatController as arguments.
  /// It does not return anything.
  void _redactEventAction(BuildContext c, ChatController controller) async {
    Navigator.of(c).pop();
    controller.selectMode ? controller.redactEventsAction() : controller.redactEventsAction(event: widget.event);
  }
}
