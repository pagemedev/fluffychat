import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/chat/events/quill_content.dart';

import 'package:pageMe/utils/date_time_extension.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:pageMe/widgets/matrix.dart';
import '../../../utils/adaptive_bottom_sheet.dart';
import '../../../utils/platform_infos.dart';
import '../../../utils/text_extension.dart';
import '../../../utils/url_launcher.dart';
import '../../../widgets/avatar.dart';
import '../../../widgets/timestamp_image.dart';
import '../../bootstrap/bootstrap_dialog.dart';
import '../chat.dart';
import '../events/audio_player.dart';
import '../events/html_message.dart';
import '../events/map_bubble.dart';
import '../events/message_download_content.dart';
import '../events/sticker.dart';

class MessageContent extends StatelessWidget {
  final Event event;
  final Color textColor;
  final void Function(Event)? onInfoTab;
  final VoidCallback? onTap;
  final bool longPressSelect;
  final Size? timeStampSizeLarge;
  final Size? timeStampSizeSmall;

  const MessageContent(
    this.event, {
    this.onInfoTab,
    Key? key,
    required this.textColor,
    required this.timeStampSizeLarge,
    required this.timeStampSizeSmall,
    required this.longPressSelect,
    this.onTap,
  }) : super(key: key);

  void _verifyOrRequestKey(BuildContext context) async {
    final l10n = L10n.of(context)!;
    if (event.content['can_request_session'] != true) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
          event.type == EventTypes.Encrypted
              ? l10n.needPantalaimonWarning
              : event.calcLocalizedBodyFallback(
                  MatrixLocals(l10n),
                ),
          style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
        ),
        backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
        closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
      ));
      return;
    }
    final client = Matrix.of(context).client;
    if (client.isUnknownSession && client.encryption!.crossSigning.enabled) {
      final success = await BootstrapDialog(
        client: Matrix.of(context).client,
      ).show(context);
      if (success != true) return;
    }
    event.requestKey();
    final sender = event.senderFromMemoryOrFallback;
    await showAdaptiveBottomSheet(
      context: context,
      builder: (context) => Scaffold(
        appBar: AppBar(
          leading: CloseButton(onPressed: Navigator.of(context).pop),
          title: Text(
            l10n.whyIsThisMessageEncrypted,
            style: const TextStyle(fontSize: 16),
          ),
        ),
        body: SafeArea(
          child: ListView(
            padding: const EdgeInsets.all(16),
            children: [
              ListTile(
                contentPadding: EdgeInsets.zero,
                leading: Avatar(
                  mxContent: sender.avatarUrl,
                  name: sender.calcDisplayname(),
                ),
                title: Text(sender.calcDisplayname()),
                subtitle: Text(event.originServerTs.localizedTime(context)),
                trailing: const Icon(Icons.lock_outlined),
              ),
              const Divider(),
              Text(
                event.calcLocalizedBodyFallback(
                  MatrixLocals(l10n),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildEncrypted(BuildContext context) {
    final client = Matrix.of(context).client;
    final ownMessage = event.senderId == client.userID;
    final buttonTextColor = event.senderId == Matrix.of(context).client.userID ? textColor : null;
    final timeStampSizePadding = (ownMessage) ? timeStampSizeLarge : timeStampSizeSmall;
    return _ButtonContent(
      textColor: buttonTextColor,
      onPressed: () => _verifyOrRequestKey(context),
      icon: const Icon(Icons.lock_outline),
      label: L10n.of(context)!.encrypted,
      timeStampSize: timeStampSizePadding!,
    );
  }

  Widget _buildLocation(BuildContext context) {
    final geoUri = Uri.tryParse(event.content.tryGet<String>('geo_uri') ?? '');
    if (geoUri != null && geoUri.scheme == 'geo') {
      final latlong = geoUri.path.split(';').first.split(',').map((s) => double.tryParse(s)).toList();
      if (latlong.length == 2 && latlong.first != null && latlong.last != null) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            MapBubble(
              latitude: latlong.first!,
              longitude: latlong.last!,
            ),
            const SizedBox(height: 6),
            OutlinedButton.icon(
              icon: Icon(Icons.location_on_outlined, color: textColor),
              onPressed: UrlLauncher(context, geoUri.toString(), "Open location in default Maps app").launchUrl,
              label: Text(
                L10n.of(context)!.openInMaps,
                style: TextStyle(color: textColor),
              ),
            ),
          ],
        );
      } else {
        return _buildText(context);
      }
    } else {
      return _buildText(context);
    }
  }

  Widget _buildFile(BuildContext context) {
    return MessageDownloadContent(event, textColor);
  }

  Widget _buildVideo(BuildContext context) {
    if (PlatformInfos.isMobile || PlatformInfos.isMacOS || PlatformInfos.isWeb) {
      final client = Matrix.of(context).client;
      final ownMessage = event.senderId == client.userID;
      final timeStampSizePadding = (ownMessage) ? timeStampSizeLarge : timeStampSizeSmall;
      return TimestampedVideo(event: event, longPressSelect: longPressSelect, onTap: onTap, timeStampPadding: timeStampSizePadding!);
    }
    return MessageDownloadContent(event, textColor);
  }

  Widget _buildAudio(BuildContext context) {
    if (PlatformInfos.isMobile || PlatformInfos.isMacOS) {
      return AudioPlayerWidget(
        event,
        color: textColor,
      );
    }
    return MessageDownloadContent(event, textColor);
  }

  Widget _buildSticker(BuildContext context) {
    if (event.redacted) return _buildText(context);
    //Logs().i("_buildSticker called ");
    return Sticker(event);
  }

  Widget _buildImage(BuildContext context) {
    //Logs().i("_buildImage called ");
    final client = Matrix.of(context).client;
    final ownMessage = event.senderId == client.userID;
    final timeStampSizePadding = (ownMessage) ? timeStampSizeLarge : timeStampSizeSmall;
    return TimestampedImage(
      event: event,
      longPressSelect: longPressSelect,
      onTap: onTap,
      timeStampPadding: timeStampSizePadding!,
    );
  }

  Widget _buildQuillText(BuildContext context) {
    final serializedDelta = event.content['serialized_delta']; // Replace with the appropriate getter for the serialized delta
    if (serializedDelta != null) {
      return QuillContent(
        textColor: textColor,
        event: event,
      );
    } else {
      return _buildText(context);
    }
  }

  Widget _buildText(BuildContext context) {
    final client = Matrix.of(context).client;
    final ownMessage = event.senderId == client.userID;
    final fontSize = FlavorConfig.messageFontSize * FlavorConfig.fontSizeFactor;
    final buttonTextColor = event.senderId == Matrix.of(context).client.userID ? textColor : null;
    final timeStampSizePadding = (ownMessage) ? timeStampSizeLarge : timeStampSizeSmall;
    if (event.redacted) {
      return _ButtonContent(
        label: L10n.of(context)!.redactedAnEvent(event.senderFromMemoryOrFallback.calcDisplayname()),
        icon: Icon(
          Icons.delete_outlined,
          color: buttonTextColor,
        ),
        textColor: buttonTextColor,
        onPressed: () => onInfoTab!(event),
        timeStampSize: timeStampSizePadding!,
      );
    }
    final bigEmotes = event.onlyEmotes && event.numberEmotes > 0 && event.numberEmotes <= 2;
    final String text = event.calcLocalizedBodyFallback(MatrixLocals(L10n.of(context)!), hideReply: true);
    return TimeStampLinkText(
      timeStampSize: timeStampSizePadding!,
      text: text,
      textStyle: TextStyle(
        color: textColor,
        fontSize: bigEmotes ? fontSize * 3 : fontSize,
        decoration: event.redacted ? TextDecoration.lineThrough : null,
      ),
      linkStyle: TextStyle(
        color: textColor.withAlpha(150),
        fontSize: bigEmotes ? fontSize * 3 : fontSize,
        decoration: TextDecoration.underline,
      ),
      onLinkTap: (url) => UrlLauncher(context, url.toString()).launchUrl(),
    );
  }

  Widget _buildCallInvite(BuildContext context) {
    final buttonTextColor = event.senderId == Matrix.of(context).client.userID ? textColor : null;
    return _ButtonContent(
      label: L10n.of(context)!.startedACall(event.senderFromMemoryOrFallback.calcDisplayname()),
      icon: const Icon(
        Icons.phone_outlined,
        color: Colors.green,
      ),
      textColor: buttonTextColor,
      onPressed: () => onInfoTab!(event),
      timeStampSize: timeStampSizeSmall!,
    );
  }

  Widget _buildDefault(BuildContext context) {
    final client = Matrix.of(context).client;
    final ownMessage = event.senderId == client.userID;
    final buttonTextColor = event.senderId == Matrix.of(context).client.userID ? textColor : null;
    final timeStampSizePadding = (ownMessage) ? timeStampSizeLarge : timeStampSizeSmall;
    return _ButtonContent(
      label: L10n.of(context)!.userSentUnknownEvent(event.senderFromMemoryOrFallback.calcDisplayname(), event.type),
      icon: const Icon(Icons.info_outlined),
      textColor: buttonTextColor,
      onPressed: () => onInfoTab!(event),
      timeStampSize: timeStampSizePadding!,
    );
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    final ownMessage = event.senderId == client.userID;
    final messageTypeToWidget = {
      CustomMessageTypes.quillText: _buildQuillText,
      MessageTypes.Image: _buildImage,
      MessageTypes.Sticker: _buildSticker,
      MessageTypes.Audio: _buildAudio,
      MessageTypes.Video: _buildVideo,
      MessageTypes.File: _buildFile,
      MessageTypes.Location: _buildLocation,
      MessageTypes.Text: _buildText,
      MessageTypes.Notice: _buildText,
      MessageTypes.Emote: _buildText,
      MessageTypes.BadEncrypted: _buildEncrypted,
      MessageTypes.None: _buildText,
    };

    final Function? buildFunction;
    if (FlavorConfig.renderHtml && !event.redacted && event.isRichMessage && event.type != EventTypes.Sticker) {
      var html = event.formattedText;
      if (event.messageType == MessageTypes.Emote) {
        html = '* $html';
      }
      //Logs().i("[MessageContent] build: ${event.messageType}, ${event.type}");
      return HtmlMessage(
        html: html,
        textColor: textColor,
        room: event.room,
        timeStampPadding: (ownMessage) ? timeStampSizeLarge : timeStampSizeSmall,
      );
    }
    if (event.type != EventTypes.CallInvite) {
      buildFunction = messageTypeToWidget[event.messageType];
    } else {
      buildFunction = _buildCallInvite;
    }
    return buildFunction != null ? buildFunction(context) : _buildDefault(context);
  }
}

class _ButtonContent extends StatelessWidget {
  final void Function() onPressed;
  final String label;
  final Icon icon;
  final Color? textColor;
  final Size timeStampSize;

  const _ButtonContent({
    required this.label,
    required this.icon,
    required this.textColor,
    required this.onPressed,
    required this.timeStampSize,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          icon,
          const SizedBox(
            width: 10,
          ),
          TimeStampText(
            textSpan: TextSpan(text: label),
            //text: label,
            overflow: TextOverflow.ellipsis,
            textStyle: TextStyle(color: textColor),
            timeStampSize: timeStampSize,
          ),
        ],
      ),
    );
  }
}
