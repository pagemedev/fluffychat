import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pages/chat/events/bubble_container.dart';
import 'package:pageMe/pages/chat/events/timestamp.dart';
import 'package:pageMe/pages/chat/message/message_desktop_buttons.dart';
import 'package:pageMe/utils/conditonal_wrapper.dart';
import 'package:pageMe/utils/date_time_extension.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/widgets/matrix.dart';
import '../chat.dart';
import 'message_content.dart';
import '../events/message_reactions.dart';
import '../events/reply_content.dart';
import '../events/state_message.dart';
import '../events/verification_request_content.dart';

/// The [Message] class represents a chat message in the application.
///
/// It is a [StatefulWidget] that takes an [Event] object as a parameter,
/// which represents the message event. It also takes a number of optional
/// parameters for handling various user interactions with the message.
///
/// The [Message] class also includes a number of helper methods for
/// determining the display characteristics of the message, such as whether
/// the message is from the current user, whether the message should be
/// displayed with a time stamp, and what color the message bubble should be.
///
/// Example usage:
///
/// ```dart
/// Message(
///   event,
///   nextEvent: nextEvent,
///   longPressSelect: true,
///   onSelect: (event) => handleSelect(event),
///   onInfoTab: (event) => handleInfoTab(event),
///   onAvatarTab: (event) => handleAvatarTab(event),
///   scrollToEventId: (eventId) => handleScrollTo(eventId),
///   selected: isSelected,
///   timeline: timeline,
///   timeStampSizeLarge: Size(10, 10),
///   timeStampSizeSmall: Size(5, 5),
///   swipeRight: (event) => handleSwipeRight(event),
/// )
/// ```
///
/// See also:
///
///  * [Event], which represents a chat event in the Matrix protocol.
class Message extends StatefulWidget {
  final Event event;
  final Event? nextEvent;
  final void Function(Event)? onSelect;
  final void Function(Event)? onAvatarTab;
  final void Function(Event)? onInfoTab;
  final void Function(String)? scrollToEventId;
  final void Function(Event)? swipeRight;
  final bool longPressSelect;
  final bool selected;
  final Timeline timeline;
  final Size timeStampSizeLarge;
  final Size timeStampSizeSmall;

  const Message(
    this.event, {
    this.nextEvent,
    this.longPressSelect = false,
    this.onSelect,
    this.onInfoTab,
    this.onAvatarTab,
    this.scrollToEventId,
    this.selected = false,
    required this.timeline,
    required this.timeStampSizeLarge,
    required this.timeStampSizeSmall,
    Key? key,
    this.swipeRight,
  }) : super(key: key);

  @override
  State<Message> createState() => MessageState();
}

class MessageState extends State<Message> {
  static const double bubbleSizeFactor = 8.0;
  static const double columnWidthFactor = 1.5;
  static const double paddingFactor = 6.0;
  final FocusNode selectableFocusNode = FocusNode();
  //bool showDesktopButtons = false;
  final showDesktopButtons = ValueNotifier<bool>(false);



/*  void toggleSelectionInProgress(bool newValue) {
    setState(() {
      if (newValue) {
        selectableFocusNode.requestFocus();
      } else {
        selectableFocusNode.unfocus();
      }
    });
  }*/

  /// Checks if the event type is one of the common types that we support.
  bool isSupportedEventType(String eventType) {
    return {EventTypes.Message, EventTypes.Sticker, EventTypes.Encrypted, EventTypes.CallInvite}.contains(eventType);
  }

  /// Builds a widget for unsupported event types.
  Widget buildUnsupportedEventTypeWidget(Event event) {
    if (event.type.startsWith('m.call.')) {
      return Container(height: 0);
    }
    return StateMessage(event);
  }

  /// Checks if the event is a verification request.
  bool isVerificationRequest(Event event) {
    return event.type == EventTypes.Message && event.messageType == EventTypes.KeyVerificationRequest;
  }

  /// Determines the color of own message.
  Color determineColor(BuildContext context, bool ownMessage) {
    return ownMessage ? Theme.of(context).colorScheme.primaryContainer : Theme.of(context).colorScheme.tertiaryContainer;
  }

  /// Checks if the time should be displayed for the event.
  bool shouldDisplayTime(Event event, Event? nextEvent) {
    return event.type == EventTypes.RoomCreate || nextEvent == null || !event.originServerTs.sameDate(nextEvent.originServerTs);
  }

  /// Checks if the sender of the current event is the same as the sender of the next event.
  bool isSameSender(Event event, Event? nextEvent, bool displayTime) {
    return nextEvent != null &&
            [
              EventTypes.Message,
              EventTypes.Sticker,
              EventTypes.Encrypted,
            ].contains(nextEvent.type)
        ? nextEvent.senderFromMemoryOrFallback.id == event.senderFromMemoryOrFallback.id && !displayTime
        : false;
  }

  /// Determines the text color based on whether it's an own message.
  Color determineTextColor(BuildContext context, bool ownMessage) {
    return ownMessage ? Theme.of(context).colorScheme.onPrimaryContainer : Theme.of(context).colorScheme.onTertiaryContainer;
  }

  /// Determines the main axis alignment of the row based on whether it's an own message.
  MainAxisAlignment determineRowMainAxisAlignment(bool ownMessage) {
    return ownMessage ? MainAxisAlignment.end : MainAxisAlignment.start;
  }

  /// Checks if the event is a media message.
  bool isMediaMessage(Event event) {
    return {
      MessageTypes.Video,
      MessageTypes.Image,
      MessageTypes.Sticker,
    }.contains(event.messageType);
  }

  /// Determines the color of own message.
  Color determineOwnMessageColor(Event displayEvent, BuildContext context) {
    return displayEvent.status.isError ? Colors.redAccent : Theme.of(context).colorScheme.primaryContainer;
  }

  Widget buildBubbleContainer(BuildContext context, bool sameSender, Color color, bool ownMessage, Event displayEvent, Color textColor, bool noBubble) {
    return BubbleContainer(
      isText: !noBubble,
      color: color,
      tail: !sameSender,
      isSender: ownMessage,
      event: widget.event,
      timeStamp: TimeStamp(
        event: widget.event,
        isSender: ownMessage,
        timeline: widget.timeline,
      ),
      longPressSelect: widget.longPressSelect,
      selected: widget.selected,
      onSelect: (event) => widget.onSelect!(event),
      swipeRight: (event) => widget.swipeRight!(event),
      child: Container(
        constraints: const BoxConstraints(maxWidth: PageMeThemes.columnWidth * columnWidthFactor),
        padding: noBubble ? EdgeInsets.zero : EdgeInsets.symmetric(horizontal: 8 * FlavorConfig.bubbleSizeFactor),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (widget.event.relationshipType == RelationshipTypes.reply)
              FutureBuilder<Event?>(
                future: widget.event.getReplyEvent(widget.timeline),
                builder: (BuildContext context, snapshot) {
                  final replyEvent = snapshot.hasData
                      ? snapshot.data!
                      : Event(
                          eventId: widget.event.relationshipEventId!,
                          content: {'msgtype': 'm.text', 'body': '...'},
                          senderId: widget.event.senderId,
                          type: 'm.room.message',
                          room: widget.event.room,
                          status: EventStatus.sent,
                          originServerTs: DateTime.now(),
                        );
                  return InkWell(
                    onTap: () {
                      if (widget.scrollToEventId != null) {
                        widget.scrollToEventId!(replyEvent.eventId);
                      }
                    },
                    child: AbsorbPointer(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: paddingFactor),
                        child: Padding(
                          padding: const EdgeInsets.only(top: paddingFactor),
                          child: ReplyContent(
                            replyEvent,
                            lightText: ownMessage,
                            timeline: widget.timeline,
                            timeStampLarge: widget.timeStampSizeLarge,
                            timeStampSmall: widget.timeStampSizeSmall,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ConditionalWrapper(
              condition: ((PlatformInfos.isDesktop || PlatformInfos.isWeb) && widget.event.messageType != CustomMessageTypes.quillText) ||
                  (widget.selected && widget.event.messageType != CustomMessageTypes.quillText),
              wrapper: (context, child) {
                return SelectionArea(
                    focusNode: selectableFocusNode,
                    //onSelectionChanged: (content) => toggleSelectionInProgress(content != null),
                    contextMenuBuilder: (context, selectableRegionState) {
                      return AdaptiveTextSelectionToolbar.selectableRegion(
                        selectableRegionState: selectableRegionState,
                      );
                    },
                    child: child);
              },
              builder: (context) {
                return MessageContent(
                  displayEvent.getDisplayEvent(widget.timeline),
                  textColor: textColor,
                  onInfoTab: widget.onInfoTab,
                  onTap: () => widget.onSelect!(widget.event),
                  longPressSelect: widget.longPressSelect,
                  timeStampSizeLarge: widget.timeStampSizeLarge,
                  timeStampSizeSmall: widget.timeStampSizeSmall,
                );
              },
            ),
            if (widget.event.hasAggregatedEvents(widget.timeline, RelationshipTypes.edit))
              Padding(
                padding: EdgeInsets.only(top: 4.0 * FlavorConfig.bubbleSizeFactor),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.edit_outlined,
                      color: textColor.withAlpha(164),
                      size: 14,
                    ),
                    Text(
                      ' - ${displayEvent.originServerTs.localizedTimeShort(context)}',
                      style: TextStyle(
                        color: textColor.withAlpha(164),
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget buildMessageColumn(BuildContext context, bool sameSender, Color color, bool ownMessage, Event displayEvent, Color textColor, bool noBubble) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        if (!sameSender) const SizedBox(height: 6),
        buildBubbleContainer(context, sameSender, color, ownMessage, displayEvent, textColor, noBubble),
      ],
    );
  }

  List<Widget> buildRowChildren(BuildContext context, bool sameSender, Color color, bool ownMessage, Event displayEvent, Color textColor, bool noBubble) {
    final menuButtonKey = GlobalKey(debugLabel: displayEvent.eventId.toString());
    return [
      if (Chat.of(context)!.selectMode && (PlatformInfos.isWeb || PlatformInfos.isDesktop))
        ConditionalWrapper(
            condition: ownMessage,
            wrapper: (context, child) {
              return Expanded(child: child);
            },
            builder: (context) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(
                    height: 30,
                    child: Checkbox(
                        value: widget.selected,
                        onChanged: (_) {
                          widget.onSelect!(displayEvent);
                        }),
                  ),
                ],
              );
            }),
      if (ownMessage && (PlatformInfos.isDesktop || PlatformInfos.isWeb))
        MessageDesktopButtons(
          displayEvent: displayEvent,
          menuButtonKey: menuButtonKey,
          event: widget.event,
          showDesktopButtons: showDesktopButtons,
        ),
      buildMessageColumn(context, sameSender, color, ownMessage, displayEvent, textColor, noBubble),
      if (!ownMessage && (PlatformInfos.isDesktop || PlatformInfos.isWeb))
        MessageDesktopButtons(
          displayEvent: displayEvent,
          menuButtonKey: menuButtonKey,
          event: widget.event,
          showDesktopButtons: showDesktopButtons,
        ),
      if (Chat.of(context)!.selectMode && !(PlatformInfos.isWeb || PlatformInfos.isDesktop))
        ConditionalWrapper(
            condition: !ownMessage,
            wrapper: (context, child) {
              return Expanded(child: child);
            },
            builder: (context) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: SizedBox(
                      height: 30,
                      width: 30,
                      child: Checkbox(
                        value: widget.selected,
                        onChanged: (_) {
                          widget.onSelect!(displayEvent);
                        },
                      ),
                    ),
                  ),
                ],
              );
            }),
    ];
  }


  Widget buildRow(MainAxisAlignment rowMainAxisAlignment, List<Widget> rowChildren) {
    return Container(
      decoration: BoxDecoration(
        color: widget.selected
            ? PageMeThemes.isDarkMode(context)
                ? Theme.of(context).colorScheme.tertiary.withOpacity(0.4)
                : Theme.of(context).colorScheme.primary.withAlpha(100)
            : Colors.transparent,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: rowMainAxisAlignment,
        mainAxisSize: MainAxisSize.max,
        children: rowChildren,
      ),
    );
  }

  /// Builds the localized date stamp in the chat view
  Widget buildDisplayTime(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: bubbleSizeFactor * FlavorConfig.bubbleSizeFactor),
      child: Center(
        child: Material(
          color: Theme.of(context).colorScheme.tertiaryContainer,
          borderRadius: BorderRadius.circular(FlavorConfig.borderRadius / 2),
          clipBehavior: Clip.antiAlias,
          child: Padding(
            padding: const EdgeInsets.all(paddingFactor),
            child: Text(
              widget.event.originServerTs.localizedDate(context),
              style: TextStyle(fontSize: 14 * FlavorConfig.fontSizeFactor, color: Theme.of(context).colorScheme.onTertiaryContainer),
            ),
          ),
        ),
      ),
    );
  }

  /// Builds the message reaction row for when a users what to send a reaction.
  Widget buildMessageReactions(BuildContext context, bool ownMessage) {
    return Padding(
      padding: EdgeInsets.only(
        top: 4.0 * FlavorConfig.bubbleSizeFactor,
        left: (ownMessage ? 0 : 20),
        right: 12.0,
      ),
      child: MessageReactions(widget.event, widget.timeline),
    );
  }

  Widget buildMessage(BuildContext context, bool displayTime, Widget row, bool ownMessage) {
    Widget container;
    if (widget.event.hasAggregatedEvents(widget.timeline, RelationshipTypes.reaction) || displayTime) {
      container = Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: ownMessage ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          if (displayTime) buildDisplayTime(context),
          row,
          if (widget.event.hasAggregatedEvents(widget.timeline, RelationshipTypes.reaction)) buildMessageReactions(context, ownMessage),
        ],
      );
    } else {
      container = row;
    }

    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 0.0,
          vertical: 0.5 * FlavorConfig.bubbleSizeFactor,
        ),
        child: container,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    if (!isSupportedEventType(widget.event.type)) {
      return buildUnsupportedEventTypeWidget(widget.event);
    }

    if (isVerificationRequest(widget.event)) {
      return VerificationRequestContent(event: widget.event, timeline: widget.timeline);
    }

    final ownMessage = widget.event.senderId == client.userID;
    var color = determineColor(context, ownMessage);
    final displayTime = shouldDisplayTime(widget.event, widget.nextEvent);
    final sameSender = isSameSender(widget.event, widget.nextEvent, displayTime);
    final textColor = determineTextColor(context, ownMessage);
    final rowMainAxisAlignment = determineRowMainAxisAlignment(ownMessage);
    final displayEvent = widget.event.getDisplayEvent(widget.timeline);
    final noBubble = isMediaMessage(widget.event);
    if (displayEvent.messageType == CustomMessageTypes.quillText){
    }
    if (ownMessage) {
      color = determineOwnMessageColor(displayEvent, context);
    }

    final rowChildren = buildRowChildren(context, sameSender, color, ownMessage, displayEvent, textColor, noBubble);
    final row = buildRow(rowMainAxisAlignment, rowChildren);

    return ConditionalWrapper(
        condition: PlatformInfos.isDesktop || PlatformInfos.isWeb,
        wrapper: (context, child) {
          return MouseRegion(
            opaque: false,
            hitTestBehavior: HitTestBehavior.translucent,
            onHover: (_) => showDesktopButtons.value = true,
            onExit: (_) => showDesktopButtons.value = false,
            onEnter: (_) => showDesktopButtons.value = true,
            child: child,
          );
        },
        builder: (context) {
          return buildMessage(context, displayTime, row, ownMessage);
        });
  }
}
