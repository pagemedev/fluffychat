import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:animations/animations.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:keyboard_shortcuts/keyboard_shortcuts.dart';
import 'package:matrix/matrix.dart';

import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/cubits/image_handler/image_handler_bloc.dart';
import 'package:pageMe/cubits/sending_handler/sending_handler_bloc.dart';
import 'package:pageMe/pages/chat/quill_editor_view.dart';
import 'package:pageMe/pages/chat/reply_display.dart';
import 'package:pageMe/utils/conditonal_wrapper.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/widgets/avatar.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:path/path.dart';
import '../../config/themes.dart';
import '../../cubits/file_handler/file_handler_bloc.dart';
import '../../widgets/m2_popup_menu_button.dart';
import '../file_sharing/files_display.dart';
import 'chat.dart';
import 'chat_action_buttons/chat_action_buttons.dart';
import 'chat_action_buttons/chat_send_action_button.dart';
import 'input_bar.dart';
import 'dart:math' as math;

class ChatInputRow extends StatefulWidget {
  final ChatController controller;
  const ChatInputRow(this.controller, {super.key});

  @override
  State<ChatInputRow> createState() => _ChatInputRowState();
}

class _ChatInputRowState extends State<ChatInputRow> {
  bool previousAnimationComplete = false;

  @override
  Widget build(BuildContext context) {
    final borderRadius = _getBorderRadius();
    /*   if (widget.controller.showEmojiPicker && widget.controller.emojiPickerType == EmojiPickerType.reaction) {
      return Container();
    }*/
    return BlocConsumer<SendingHandlerBloc, SendingHandlerState>(
      listener: (context, sendingState) async {
        final fileState = BlocProvider.of<FileHandlerBloc>(context).state;
        if (sendingState.isSent) {
          handleSendingImages(fileState: fileState, context: context);
          //widget.controller.handleTextCompletion();
          //widget.controller.handleQuillCompletion();
          //widget.controller.resetTextValues();
        } else if (sendingState.isCancelled) {
          handleSendingImages(fileState: fileState, context: context, isCancelled: true);
          //widget.controller.handleTextCompletion();
          //widget.controller.handleQuillCompletion();
          //await widget.controller.resetTextValues();
        }
      },
      builder: (context, sendingState) {
        return BlocBuilder<FileHandlerBloc, FileHandlerState>(
          buildWhen: (previous, next) {
            //Logs().v("previous.processingStatus: ${previous.processingStatus}, next.processingStatus: ${next.processingStatus}");
            return true;
          },
          builder: (context, fileState) {
            //Logs().v("ChatInputRow - fileState.isInitiated: ${fileState.isInitiated}");
            return Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4.0),
                      child: AnimatedSwitcher(
                        duration: const Duration(milliseconds: 500),
                        reverseDuration: const Duration(milliseconds: 500),
                        switchInCurve: Curves.easeOutExpo,
                        switchOutCurve: Curves.easeInExpo,
                        transitionBuilder: (Widget child, Animation<double> animation) {
                          final Animation<Offset> offsetAnimation = Tween<Offset>(begin: const Offset(0.0, 2), end: Offset.zero).animate(animation);
                          return SlideTransition(
                            position: offsetAnimation,
                            child: child,
                          );
                        },
                        child: (fileState.isInitiated)
                            ? (sendingState.isSending && PlatformInfos.isAndroid)
                                ? InputContainer(widget.controller, borderRadius)
                                : const FilesContainer()
                            : InputContainer(widget.controller, borderRadius),
                      ),
                    ),
                  ),
                  (!widget.controller.showTextEditor)
                      ? ChatActionButtons(
                          controller: widget.controller,
                        )
                      : const SizedBox.shrink(),
                ],
              ),
            );
          },
        );
      },
    );
  }

  void handleSendingImages({required FileHandlerState fileState, required BuildContext context, bool? isCancelled}) {
    if (fileState.isReadyWithFiles) {
      BlocProvider.of<FileHandlerBloc>(context).add(FileCompletionRequested(isCancelled: isCancelled ?? false));
    }
  }

  BorderRadius _getBorderRadius() {
    return const BorderRadius.only(
      bottomRight: Radius.circular(14),
      bottomLeft: Radius.circular(14),
      topRight: Radius.circular(14),
      topLeft: Radius.circular(14),
    );
  }
}

class FilesContainer extends StatelessWidget {
  const FilesContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 2,
      borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
      color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.surfaceVariant : Theme.of(context).colorScheme.surface,
      shadowColor: Colors.black,
      clipBehavior: Clip.hardEdge,
      child: const FilesDisplay(),
    );
  }
}

class InputContainer extends StatelessWidget {
  final ChatController controller;
  final BorderRadius borderRadius;
  const InputContainer(this.controller, this.borderRadius, {super.key});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 2,
      borderRadius: borderRadius,
      color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.surfaceVariant : Theme.of(context).colorScheme.surface,
      shadowColor: Colors.black,
      clipBehavior: Clip.hardEdge,
      child: _buildContent(context),
    );
  }

  Widget _buildContent(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ReplyDisplay(
          controller,
        ),
        controller.showTextEditor
            ? QuillEditorView(controller: controller.textEditorController)
            : Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: controller.selectMode ? _buildSelectModeWidgets(context) : _buildNonSelectModeWidgets(context),
              ),
      ],
    );
  }

  List<Widget> _buildSelectModeWidgets(BuildContext context) {
    return <Widget>[
      _buildReplyResendButton(context),
      if (controller.showEmojiPicker || controller.showAttachmentPicker) const Spacer(),
      _buildIconButtonWithText(
          L10n.of(context)!.forward,
          Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: Icon(
              Icons.reply,
              color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiary : Theme.of(context).colorScheme.primary,
            ),
          ),
          controller.forwardEventsAction,
          context),
      if (controller.showEmojiPicker) EmojiPickerButton(controller: controller, context: context),
      if (controller.showAttachmentPicker) AttachmentButton(controller: controller, context: context),
    ];
  }

  Widget _buildIconButtonWithText(String text, Widget icon, VoidCallback onPressed, BuildContext context) {
    final iconColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSurface;

    return SizedBox(
      height: 45,
      child: TextButton(
        onPressed: onPressed,
        child: Row(
          children: <Widget>[
            icon,
            const SizedBox(
              width: 2,
            ),
            Text(
              text,
              style: TextStyle(color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSurface),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildReplyResendButton(BuildContext context) {
    if (controller.selectedEvents.length == 1) {
      final isSent = controller.selectedEvents.first.getDisplayEvent(controller.timeline!).status.isSent;
      return isSent
          ? _buildIconButtonWithText(
              L10n.of(context)!.reply,
              Icon(
                Icons.reply,
                color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiary : Theme.of(context).colorScheme.primary,
              ),
              controller.replyAction,
              context)
          : _buildIconButtonWithText(
              L10n.of(context)!.tryToSendAgain,
              Icon(
                Icons.send_outlined,
                color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiary : Theme.of(context).colorScheme.primary,
              ),
              controller.sendAgainAction,
              context,
            );
    }
    return Container();
  }

  List<Widget> _buildNonSelectModeWidgets(BuildContext context) {
    if (context.watch<FileHandlerBloc>().state.filesState.isNotEmpty && !PlatformInfos.isAndroid) {
      return [];
    }
    return <Widget>[
      QuillTextEditorIconButton(controller: controller, context: context),
      _buildChatAccountPicker(context),
      TextInputBar(controller: controller, context: context),
      EmojiPickerButton(controller: controller, context: context),
      AttachmentButton(controller: controller, context: context),
    ];
  }

  Widget _buildChatAccountPicker(BuildContext context) {
    if (controller.matrix.isMultiAccount && controller.matrix.hasComplexBundles && controller.matrix.currentBundle!.length > 1) {
      return Container(
        height: 45,
        alignment: Alignment.center,
        child: _ChatAccountPicker(controller),
      );
    }

    return Container(); // or some other default value
  }
}

class QuillTextEditorIconButton extends StatelessWidget {
  const QuillTextEditorIconButton({
    super.key,
    required this.controller,
    required this.context,
  });

  final ChatController controller;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return BlocSelector<FileHandlerBloc, FileHandlerState, bool>(
      selector: (state) => state.filesState.isNotEmpty,
      builder: (context, hasFiles) {
        if (hasFiles) {
          return Container(
            width: 12,
          );
        }
        return SizedBox(
          height: 45,
          child: ConditionalKeyBoardShortcuts(
            enabled: !PlatformInfos.isMobile,
            keysToPress: {LogicalKeyboardKey.altLeft, LogicalKeyboardKey.keyE},
            onKeysPressed: controller.emojiPickerAction,
            helpLabel: L10n.of(context)!.emojis,
            child: IconButton(
              tooltip: 'Open the text editor',
              icon: PageTransitionSwitcher(
                transitionBuilder: (
                  Widget child,
                  Animation<double> primaryAnimation,
                  Animation<double> secondaryAnimation,
                ) {
                  return SharedAxisTransition(
                    animation: primaryAnimation,
                    secondaryAnimation: secondaryAnimation,
                    transitionType: SharedAxisTransitionType.scaled,
                    fillColor: Colors.transparent,
                    child: child,
                  );
                },
                child: Icon(controller.showTextEditor ? Icons.keyboard : Icons.edit_note,
                    key: ValueKey(
                      controller.showTextEditor,
                    ),
                    size: 30,
                    color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSurface),
              ),
              onPressed: controller.textEditorAction,
            ),
          ),
        );
      },
    );
  }
}

class TextInputBar extends StatelessWidget {
  const TextInputBar({
    super.key,
    required this.controller,
    required this.context,
  });

  final ChatController controller;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: controller.showTextEditor
          ? const SizedBox.shrink()
          : InputBar(
              room: controller.room,
              minLines: 1,
              maxLines: 8,
              autofocus: !PlatformInfos.isMobile,
              keyboardType: TextInputType.multiline,
              textInputAction: FlavorConfig.sendOnEnter ? TextInputAction.send : null,
              onSubmitted: controller.onInputBarSubmitted,
              focusNode: controller.inputFocus,
              controller: controller.sendController,
              decoration: InputDecoration(
                labelStyle: TextStyle(fontSize: FlavorConfig.messageFontSize * FlavorConfig.fontSizeFactor),
                hintText: L10n.of(context)!.writeAMessage,
                hintStyle: TextStyle(
                    fontSize: FlavorConfig.messageFontSize * FlavorConfig.fontSizeFactor,
                    color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSurface),
                hintMaxLines: 1,
                border: InputBorder.none,
                enabledBorder: InputBorder.none,
                filled: false,
              ),
              onChanged: controller.onInputBarChanged,
            ),
    );
  }
}

class EmojiPickerButton extends StatelessWidget {
  const EmojiPickerButton({
    super.key,
    required this.controller,
    required this.context,
  });

  final ChatController controller;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      alignment: Alignment.center,
      child: ConditionalKeyBoardShortcuts(
        enabled: !PlatformInfos.isMobile,
        keysToPress: {LogicalKeyboardKey.altLeft, LogicalKeyboardKey.keyE},
        onKeysPressed: controller.emojiPickerAction,
        helpLabel: L10n.of(context)!.emojis,
        child: IconButton(
          tooltip: L10n.of(context)!.emojis,
          icon: PageTransitionSwitcher(
            transitionBuilder: (
              Widget child,
              Animation<double> primaryAnimation,
              Animation<double> secondaryAnimation,
            ) {
              return SharedAxisTransition(
                animation: primaryAnimation,
                secondaryAnimation: secondaryAnimation,
                transitionType: SharedAxisTransitionType.scaled,
                fillColor: Colors.transparent,
                child: child,
              );
            },
            child: Icon(
              controller.showEmojiPicker ? Icons.keyboard : Icons.emoji_emotions_outlined,
              color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSurface,
              key: ValueKey(
                controller.showEmojiPicker,
              ),
            ),
          ),
          onPressed: controller.emojiPickerAction,
        ),
      ),
    );
  }
}

class AttachmentButton extends StatelessWidget {
  const AttachmentButton({
    super.key,
    required this.controller,
    required this.context,
  });

  final ChatController controller;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    if (controller.getCurrentText().isEmpty) {
      return SizedBox(
        height: 45,
        child: ConditionalKeyBoardShortcuts(
          enabled: !PlatformInfos.isMobile,
          keysToPress: {LogicalKeyboardKey.altLeft, LogicalKeyboardKey.keyE},
          onKeysPressed: controller.emojiPickerAction,
          helpLabel: L10n.of(context)!.emojis,
          child: IconButton(
            tooltip: 'Send an attachment',
            icon: PageTransitionSwitcher(
              transitionBuilder: (
                Widget child,
                Animation<double> primaryAnimation,
                Animation<double> secondaryAnimation,
              ) {
                return SharedAxisTransition(
                  animation: primaryAnimation,
                  secondaryAnimation: secondaryAnimation,
                  transitionType: SharedAxisTransitionType.scaled,
                  fillColor: Colors.transparent,
                  child: child,
                );
              },
              child: RotationTransition(
                  turns: controller.showAttachmentPicker ? const AlwaysStoppedAnimation(1) : const AlwaysStoppedAnimation(225 / 360),
                  child: Icon(controller.showAttachmentPicker ? Icons.keyboard : Icons.attachment_outlined,
                      key: ValueKey(
                        controller.showAttachmentPicker,
                      ),
                      color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSurfaceVariant : Theme.of(context).colorScheme.onSurface)),
            ),
            onPressed: controller.attachmentPickerAction,
          ),
        ),
      ); // or some other default value
    } else {
      return Container();
    }
  }
}

class _ChatAccountPicker extends StatelessWidget {
  final ChatController controller;

  const _ChatAccountPicker(this.controller, {Key? key}) : super(key: key);

  void _popupMenuButtonSelected(String mxid) {
    final client = controller.matrix.currentBundle!.firstWhere((cl) => cl!.userID == mxid, orElse: () => null);
    if (client == null) {
      Logs().w('Attempted to switch to a non-existing client $mxid');
      return;
    }
    controller.setSendingClient(client);
  }

  @override
  Widget build(BuildContext context) {
    controller.matrix ??= Matrix.of(context);
    final clients = controller.currentRoomBundle;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: FutureBuilder<Profile>(
        future: controller.sendingClient!.fetchOwnProfile(),
        builder: (context, snapshot) => M2PopupMenuButton<String>(
          onSelected: _popupMenuButtonSelected,
          itemBuilder: (BuildContext context) => clients
              .map((client) => PopupMenuItem<String>(
                    value: client!.userID,
                    child: FutureBuilder<Profile>(
                      future: client.fetchOwnProfile(),
                      builder: (context, snapshot) => ListTile(
                        leading: Avatar(
                          mxContent: snapshot.data?.avatarUrl,
                          name: snapshot.data?.displayName ?? client.userID!.localpart,
                          size: 20,
                        ),
                        title: Text(snapshot.data?.displayName ?? client.userID!),
                        contentPadding: const EdgeInsets.all(0),
                      ),
                    ),
                  ))
              .toList(),
          child: Avatar(
            mxContent: snapshot.data?.avatarUrl,
            name: snapshot.data?.displayName ?? controller.matrix.client.userID!.localpart,
            size: 20,
          ),
        ),
      ),
    );
  }
}
