import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';

import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:pageMe/utils/string_color.dart';
import 'package:pageMe/widgets/mxc_image.dart';
import 'package:thumbnailer/thumbnailer.dart';
import '../../../widgets/matrix.dart';
import 'html_message.dart';

class ReplyContent extends StatelessWidget {
  final Event replyEvent;
  final bool lightText;
  final Timeline? timeline;
  final Size? timeStampSmall;
  final Size? timeStampLarge;
  final bool hideUnderline;

  const ReplyContent(
    this.replyEvent, {
    this.lightText = false,
    Key? key,
    this.timeline,
    this.timeStampSmall,
    this.timeStampLarge,
    this.hideUnderline = false,
  }) : super(key: key);

  double getTextHeight() {
    final TextSpan span = TextSpan(text: 'I', style: TextStyle(fontSize: FlavorConfig.messageFontSize * FlavorConfig.fontSizeFactor));
    final TextPainter tp = TextPainter(text: span, textAlign: TextAlign.left, textDirection: TextDirection.ltr);
    tp.layout();

    return tp.height;
  }

  @override
  Widget build(BuildContext context) {
    Widget replyBody;
    final client = Matrix.of(context).client;
    final ownMessage = replyEvent.senderId == client.userID;
    final bigEmotes = replyEvent.onlyEmotes && replyEvent.numberEmotes > 0 && replyEvent.numberEmotes <= 2;
    final timeline = this.timeline;
    final displayEvent = timeline != null ? replyEvent.getDisplayEvent(timeline) : replyEvent;
    final fontSize = FlavorConfig.messageFontSize * FlavorConfig.fontSizeFactor;
    final isMultiMedia = replyEvent.messageType == MessageTypes.Image || replyEvent.messageType == MessageTypes.Video;
    final isFile = replyEvent.messageType == MessageTypes.File;
    if (FlavorConfig.renderHtml &&
        [EventTypes.Message, EventTypes.Encrypted].contains(displayEvent.type) &&
        [MessageTypes.Text, MessageTypes.Notice, MessageTypes.Emote].contains(displayEvent.messageType) &&
        !displayEvent.redacted &&
        displayEvent.content['format'] == 'org.matrix.custom.html' &&
        displayEvent.content['formatted_body'] is String) {
      String? html = displayEvent.content['formatted_body'] as String?;
      if (displayEvent.messageType == MessageTypes.Emote) {
        html = '* $html';
      }
      replyBody = HtmlMessage(
        timeStampPadding: ownMessage ? timeStampLarge : timeStampSmall,
        maxLines: 1,
        html: html!,
        textColor: PageMeThemes.isDarkMode(context)
            ? lightText
                ? Theme.of(context).colorScheme.onPrimaryContainer
                : Theme.of(context).colorScheme.primary
            : lightText
                ? Theme.of(context).colorScheme.onPrimary
                : Theme.of(context).colorScheme.primary,
        room: displayEvent.room,
      );
    } else if (replyEvent.messageType == CustomMessageTypes.quillText) {
      replyBody = Text(
        displayEvent.calcQuillTextToPlainText(locals: MatrixLocals(L10n.of(context)!), withSenderNamePrefix: false),
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
        style: TextStyle(
          color: PageMeThemes.isDarkMode(context)
              ? lightText
                  ? Theme.of(context).colorScheme.onPrimaryContainer
                  : Theme.of(context).colorScheme.primary
              : lightText
                  ? Theme.of(context).colorScheme.onPrimary
                  : Theme.of(context).colorScheme.primary,
          fontSize: fontSize,
        ),
      );
    } else if (isMultiMedia) {
      replyBody = ClipRRect(
        borderRadius: BorderRadius.circular(FlavorConfig.borderRadius / 4),
        child: MxcImage(
          event: replyEvent,
          thumbnailMethod: ThumbnailMethod.scale,
          height: 64,
          isThumbnail: true,
          cacheKey: replyEvent.thumbnailMxcUrl.toString(),
          fit: BoxFit.fitHeight,
        ),
      );
    } else if (isFile) {
      final filename = replyEvent.content.tryGet<String>('filename') ?? replyEvent.body;
      replyBody = Container(
        decoration: hideUnderline ? null : BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: Colors.black38,
        ),
        child: ListTile(
            contentPadding: hideUnderline ? EdgeInsets.zero : const EdgeInsets.symmetric(horizontal: 8),
            onTap: () async => replyEvent.openFile(context),
            titleAlignment: ListTileTitleAlignment.center,
            dense: true,
            visualDensity: const VisualDensity(horizontal: -4),
            leading: SizedBox(
              width: 32,
              child: Center(
                child: Icon(
                  Thumbnailer.getIconDataForMimeType(replyEvent.attachmentMimetype) ?? Icons.file_copy,
                  size: 30,
                  color: replyEvent.attachmentMimetype.lightColor,
                ),
              ),
            ),
            title: Text(
              filename,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                color: PageMeThemes.isDarkMode(context)
                    ? lightText
                        ? Theme.of(context).colorScheme.onPrimaryContainer
                        : Theme.of(context).colorScheme.primary
                    : lightText
                        ? Theme.of(context).colorScheme.onPrimary
                        : Theme.of(context).colorScheme.primary,
                fontSize: fontSize,
              ),
            )),
      );
    } else {
      replyBody = Text(
        displayEvent.calcLocalizedBodyFallback(
          MatrixLocals(L10n.of(context)!),
          withSenderNamePrefix: false,
          hideReply: true,
        ),
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
        style: TextStyle(
          color: PageMeThemes.isDarkMode(context)
              ? lightText
                  ? Theme.of(context).colorScheme.onPrimaryContainer
                  : Theme.of(context).colorScheme.primary
              : lightText
                  ? Theme.of(context).colorScheme.onPrimary
                  : Theme.of(context).colorScheme.primary,
          fontSize: fontSize,
        ),
      );
    }

    final double height = getTextHeight() * 2 + 2;
    RenderObject.debugCheckingIntrinsics = true;
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        if (!hideUnderline)
          Container(
            width: 4,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(FlavorConfig.borderRadius/8),
              color: PageMeThemes.isDarkMode(context)
                ? Theme.of(context).colorScheme.primary
                : ownMessage
                ? Theme.of(context).colorScheme.tertiary
                : Theme.of(context).colorScheme.tertiary,
            ),
            height: _calcLeftReplyLineHeight(isMultiMedia, height),
          ),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 6.0),
                child: Text(
                  displayEvent.senderFromMemoryOrFallback.calcDisplayname(),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: PageMeThemes.isDarkMode(context)
                        ? Theme.of(context).colorScheme.primary
                        : lightText
                            ? Theme.of(context).colorScheme.onPrimary
                            : Theme.of(context).colorScheme.primary,
                    fontSize: fontSize,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 6.0),
                child: replyBody,
              ),
            ],
          ),
        ),
      ],
    );
  }

  double _calcLeftReplyLineHeight(bool isMultiMedia, double height) {
    if (replyEvent.messageType == MessageTypes.Image || replyEvent.messageType == MessageTypes.Video) {
      return 86;
    }
    if (replyEvent.messageType == MessageTypes.File) {
      return 66;
    }
    return height;
  }
}
