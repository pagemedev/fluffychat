import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:chewie/chewie.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:path_provider/path_provider.dart';
import 'package:universal_html/html.dart' as html;
import 'package:video_player/video_player.dart';

import 'package:pageMe/pages/chat/events/image_bubble.dart';
import 'package:pageMe/utils/localized_exception_extension.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';

import '../../../config/themes.dart';
import '../../../utils/platform_infos.dart';

class EventVideoPlayer extends StatefulWidget {
  final Event event;
  final double? width;
  final double? height;
  const EventVideoPlayer(
    this.event, {
    Key? key,
    this.width,
    this.height,
  }) : super(key: key);

  @override
  EventVideoPlayerState createState() => EventVideoPlayerState();
}

class EventVideoPlayerState extends State<EventVideoPlayer> {
  late ThemeData theme;
  late ScaffoldMessengerState scaffoldMessenger;
  late L10n? l10n;

  ChewieController? _chewieManager;
  bool _isDownloading = false;
  String? _networkUri;
  File? _tmpFile;
  double? containedHeight;
  double? containedWidth;
  bool maxSize = false;
  double? width;
  double? height;


  @override
  void initState() {
    width = widget.width;
    height = widget.height;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    theme = Theme.of(context);
    scaffoldMessenger =ScaffoldMessenger.of(context);
    l10n = L10n.of(context);
    super.didChangeDependencies();
  }

  void _downloadAction() async {
    setState(() => _isDownloading = true);
    try {
      final videoFile = await widget.event.downloadAndDecryptAttachment();
      if (kIsWeb) {
        final blob = html.Blob([videoFile.bytes]);
        _networkUri = html.Url.createObjectUrlFromBlob(blob);
      } else {
        final tempDir = await getTemporaryDirectory();
        final fileName = Uri.encodeComponent(widget.event.attachmentOrThumbnailMxcUrl()!.pathSegments.last);
        final file = File('${tempDir.path}/${fileName}_${videoFile.name}');
        if (await file.exists() == false) {
          await file.writeAsBytes(videoFile.bytes);
        }
        _tmpFile = file;
      }
      final tmpFile = _tmpFile;
      final networkUri = _networkUri;
      if (kIsWeb && networkUri != null && _chewieManager == null) {
        _chewieManager ??= ChewieController(
          videoPlayerController: VideoPlayerController.networkUrl(Uri.parse(networkUri)),
          autoPlay: true,
          autoInitialize: true,
          fullScreenByDefault: false,
          allowFullScreen: true,
          deviceOrientationsAfterFullScreen: [
            DeviceOrientation.landscapeRight,
            DeviceOrientation.landscapeLeft,
            DeviceOrientation.portraitUp,
          ],
          draggableProgressBar: false,
          hideControlsTimer: const Duration(seconds: 1),
          aspectRatio: (widget.width != null || widget.height != null) ? widget.width! / widget.height! : null,
        );
      } else if (!kIsWeb && tmpFile != null && _chewieManager == null) {
        _chewieManager ??= ChewieController(
          videoPlayerController: VideoPlayerController.file(tmpFile),
          autoPlay: true,
          autoInitialize: true,
          fullScreenByDefault: false,
          allowFullScreen: true,
          deviceOrientationsOnEnterFullScreen: [DeviceOrientation.portraitUp],
          deviceOrientationsAfterFullScreen: [
            DeviceOrientation.landscapeRight,
            DeviceOrientation.landscapeLeft,
            DeviceOrientation.portraitUp,
          ],
          draggableProgressBar: false,
          hideControlsTimer: const Duration(seconds: 1),
          showControlsOnInitialize: false,
          aspectRatio: (widget.width != null || widget.height != null) ? widget.width! / widget.height! : null,
        );
      }
    } on MatrixConnectionException catch (e) {
      scaffoldMessenger.showSnackBar(
        SnackBar(
          content: Text(
            e.toLocalizedString(context),
            style: TextStyle(color: theme.colorScheme.onTertiaryContainer),
          ),
          backgroundColor: theme.colorScheme.tertiaryContainer,
          closeIconColor: theme.colorScheme.onTertiaryContainer,
        ),
      );
    } catch (e, s) {
      scaffoldMessenger.showSnackBar(SnackBar(
        content: Text(e.toLocalizedString(context)),
      ));
      Logs().w('Error while playing video', e, s);
    } finally {
      // Workaround for Chewie needs time to get the aspectRatio
      await Future.delayed(const Duration(milliseconds: 100));
      setState(() => _isDownloading = false);
    }
  }

  Size _calculateImageSizing(BuildContext context, BoxConstraints constraints) {
    final double maxHeight = (PlatformInfos.isWeb || PlatformInfos.isDesktop) ? MediaQuery.sizeOf(context).height * 0.35 : MediaQuery.sizeOf(context).height * 0.48;
    final double maxWidth = (PlatformInfos.isWeb || PlatformInfos.isDesktop) ? constraints.maxWidth * 0.75 : constraints.maxWidth * 0.8;
    //Logs().v('_calculateImageSizing - maxHeight: $maxHeight, maxWidth: $maxWidth');
    final double aspectRatio;
    if (width != null || height != null) {
      aspectRatio = width! / height!;
    } else {
      maxSize = true;
      containedHeight = maxHeight * 0.8;
      containedWidth = maxWidth * 0.8;
      return Size(containedWidth!, containedHeight!);
    }
    // aspect ratio of the image

    if (width! > maxWidth) {
      maxSize = true;
      containedWidth = maxWidth;
      containedHeight = (containedWidth! / aspectRatio);

      if (containedHeight! > maxHeight) {
        containedHeight = maxHeight;
        containedWidth = (containedHeight! * aspectRatio);
        //Logs().v('_calculateImageSizing - containedWidth: $containedWidth, containedHeight: $containedHeight');
        return Size(containedWidth!, containedHeight!);
      }
    } else if (height! > maxHeight) {
      maxSize = true;
      containedHeight = maxHeight;
      containedWidth = (containedHeight! * aspectRatio);

      if (containedWidth! > maxWidth) {
        containedWidth = maxWidth;
        containedHeight = (containedWidth! / aspectRatio);
        //Logs().v('_calculateImageSizing - containedWidth: $containedWidth, containedHeight: $containedHeight');
        return Size(containedWidth!, containedHeight!);
      }
    } else {
      //Logs().v('_calculateImageSizing - width: ${width}, height: ${height}');
      return Size(width!, height!);
    }
    //Logs().v('_calculateImageSizing - containedWidth: $containedWidth, containedHeight: $containedHeight');
    return Size(containedWidth!, containedHeight!);
  }

  @override
  void dispose() {
    super.dispose();
    _chewieManager?.dispose();
  }

  static const String fallbackBlurHash = 'L5H2EC=PM+yV0g-mq.wG9c010J}I';

  @override
  Widget build(BuildContext context) {
    final hasThumbnail = widget.event.hasThumbnail;
    final blurHash = (widget.event.infoMap as Map<String, dynamic>).tryGet<String>('xyz.amorgan.blurhash') ?? fallbackBlurHash;

    final chewieManager = _chewieManager;
    return LayoutBuilder(builder: (context, constraints) {
      final double maxHeight = MediaQuery.sizeOf(context).height * 0.75;
      final double maxWidth = (PageMeThemes.isColumnMode(context)) ? PageMeThemes.columnWidth * 1.5 : constraints.maxWidth;
      _calculateImageSizing(context, constraints);
      return Material(
        clipBehavior: Clip.hardEdge,
        color: Colors.black,
        borderRadius: BorderRadius.circular(6),
        child: Container(
          constraints: maxSize
              ? BoxConstraints(
                  maxWidth: containedWidth ?? maxWidth,
                  maxHeight: containedHeight ?? maxHeight,
                )
              : null,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
          child: chewieManager != null
              ? Center(
                  child: ClipRRect(
                    clipBehavior: Clip.hardEdge,
                    borderRadius: BorderRadius.circular(6),
                    child: Chewie(controller: chewieManager),
                  ),
                )
              : Stack(
                  children: [
                    if (hasThumbnail)
                      Center(
                        child: ImageBubble(
                          widget.event,
                          width: containedWidth ?? maxWidth,
                          height: containedHeight ?? maxHeight,
                          longPressSelect: false,
                          tapToView: false,
                          isVideoThumbnail: true,
                          fit: BoxFit.fill,
                        ),
                      )
                    else
                      BlurHash(hash: blurHash),
                    Center(
                      child: OutlinedButton.icon(
                        style: OutlinedButton.styleFrom(
                          backgroundColor: theme.colorScheme.surface.withOpacity(0.75),
                        ),
                        icon: _isDownloading
                            ? const SizedBox(
                                width: 24,
                                height: 24,
                                child: CircularProgressIndicator.adaptive(strokeWidth: 2),
                              )
                            : const Icon(Icons.download_outlined),
                        label: Text(
                          _isDownloading ? l10n!.loadingPleaseWait : l10n!.videoWithSize(widget.event.sizeString ?? '?MB'),
                        ),
                        onPressed: _isDownloading ? null : _downloadAction,
                      ),
                    ),
                  ],
                ),
        ),
      );
    });
  }
}
