import 'package:flutter/material.dart';

import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/platform_infos.dart';

import '../../../widgets/matrix.dart';
import '../../../widgets/mxc_image.dart';
import '../../image_viewer/image_viewer.dart';

class ImageBubble extends StatefulWidget {
  final Event event;
  final bool tapToView;
  final BoxFit fit;
  final Color? backgroundColor;
  final bool thumbnailOnly;
  final bool animated;
  final bool isVideoThumbnail;
  final double? width;
  final double? height;
  final Size? timeStampPadding;
  final bool longPressSelect;
  final void Function()? onTap;

  const ImageBubble(
    this.event, {
    this.tapToView = true,
    this.backgroundColor,
    this.fit = BoxFit.cover,
    this.thumbnailOnly = true,
    this.width = 400,
    this.height = 300,
    this.animated = false,
    this.onTap,
    Key? key,
    this.timeStampPadding,
    required this.longPressSelect,
    this.isVideoThumbnail = false,
  }) : super(key: key);

  @override
  State<ImageBubble> createState() => _ImageBubbleState();
}

class _ImageBubbleState extends State<ImageBubble> {
  double? containedHeight;

  double? containedWidth;

  bool maxSize = false;

  Size _calculateImageSizing(BuildContext context, BoxConstraints constraints) {
    final double maxHeight = widget.isVideoThumbnail
        ? constraints.maxHeight
        : (PlatformInfos.isWeb || PlatformInfos.isDesktop)
            ? MediaQuery.sizeOf(context).height * 0.35
            : MediaQuery.sizeOf(context).height * 0.48;
    final double maxWidth = widget.isVideoThumbnail
        ? constraints.maxWidth
        : (PlatformInfos.isWeb || PlatformInfos.isDesktop)
            ? constraints.maxWidth * 0.75
            : constraints.maxWidth * 0.8;
    //Logs().v('_calculateImageSizing - maxHeight: $maxHeight, maxWidth: $maxWidth');

    widget.width ?? maxWidth / 2;
    widget.height ?? maxHeight / 2;

    final double aspectRatio = widget.width! / widget.height!; // aspect ratio of the image

    if (widget.width! > maxWidth) {
      maxSize = true;
      containedWidth = maxWidth;
      containedHeight = (containedWidth! / aspectRatio);

      if (containedHeight! > maxHeight) {
        containedHeight = maxHeight;
        containedWidth = (containedHeight! * aspectRatio);
        //Logs().v('_calculateImageSizing - containedWidth: $containedWidth, containedHeight: $containedHeight');
        return Size(containedWidth!, containedHeight!);
      }
    } else if (widget.height! > maxHeight) {
      maxSize = true;
      containedHeight = maxHeight;
      containedWidth = (containedHeight! * aspectRatio);

      if (containedWidth! > maxWidth) {
        containedWidth = maxWidth;
        containedHeight = (containedWidth! / aspectRatio);
        //Logs().v('_calculateImageSizing - containedWidth: $containedWidth, containedHeight: $containedHeight');
        return Size(containedWidth!, containedHeight!);
      }
    } else {
      //Logs().v('_calculateImageSizing - widget.width: ${widget.width}, widget.height: ${widget.height}');
      return Size(widget.width!, widget.height!);
    }
    //Logs().v('_calculateImageSizing - containedWidth: $containedWidth, containedHeight: $containedHeight');
    return Size(containedWidth!, containedHeight!);
  }

  Widget _buildPlaceholder(BuildContext context) {
    if (widget.event.messageType == MessageTypes.Sticker) {
      return const Center(
        child: CircularProgressIndicator.adaptive(),
      );
    }
    final String blurHashString = widget.event.infoMap['xyz.amorgan.blurhash'] is String ? widget.event.infoMap['xyz.amorgan.blurhash'] : 'LEHV6nWB2yk8pyo0adR*.7kCMdnj';
    final ratio = widget.event.infoMap['w'] is int && widget.event.infoMap['h'] is int ? widget.event.infoMap['w'] / widget.event.infoMap['h'] : 1.0;
    var width = 32;
    var height = 32;
    if (ratio > 1.0) {
      height = (width / ratio).round();
    } else {
      width = (height * ratio).round();
    }
    return SizedBox(
      width: widget.width,
      height: widget.height,
      child: BlurHash(
        hash: blurHashString,
        decodingWidth: width,
        decodingHeight: height,
        imageFit: widget.fit,
      ),
    );
  }

  void _onTap(BuildContext context) {
    if (widget.onTap != null) {
      if (!widget.longPressSelect) {
        widget.onTap!();
        return;
      }
    }
    if (!widget.tapToView) return;
    showDialog(
      context: Matrix.of(context).navigatorContext,
      useRootNavigator: false,
      builder: (_) => ImageViewer(widget.event),
    );
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    final ownMessage = widget.event.senderId == client.userID;
    //Logs().i("${widget.width.toString()} and ${widget.height.toString()}");
    return LayoutBuilder(builder: (context, constraints) {
      final double maxHeight = MediaQuery.sizeOf(context).height * 0.75;
      final double maxWidth = (PageMeThemes.isColumnMode(context)) ? PageMeThemes.columnWidth * 1.5 : constraints.maxWidth;
      _calculateImageSizing(context, constraints);
      return ClipRRect(
        borderRadius: BorderRadius.circular(6),
        child: InkWell(
          onTap: () => _onTap(context),
          child: Hero(
            tag: widget.event.eventId,
            child: AnimatedSwitcher(
              duration: const Duration(milliseconds: 1000),
              child: Container(
                color: ownMessage ? Theme.of(context).colorScheme.primaryContainer : Theme.of(context).colorScheme.tertiaryContainer,
                constraints: maxSize
                    ? BoxConstraints(
                        maxWidth: containedWidth ?? maxWidth,
                        maxHeight: containedHeight ?? maxHeight,
                      )
                    : null,
                child: Stack(
                  clipBehavior: Clip.hardEdge,
                  children: [
                    MxcImage(
                      key: widget.event.status.isSending ? ValueKey(widget.event.eventId) : ValueKey(widget.event.attachmentMxcUrl),
                      cacheKey: widget.event.status.isSending ? widget.event.eventId : widget.event.attachmentMxcUrl.toString(),
                      event: widget.event,
                      width: widget.width,
                      height: widget.height,
                      fit: widget.fit,
                      animated: widget.animated,
                      isThumbnail: widget.thumbnailOnly,
                      placeholder: _buildPlaceholder,
                    ),
                    if (widget.event.status.isSending)
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          height: 35,
                          width: 35,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(17.5),
                            color: Colors.black54,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: CircularProgressIndicator(
                              color: Theme.of(context).colorScheme.primary,
                              strokeWidth: 2,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}
