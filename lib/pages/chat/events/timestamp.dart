import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/date_time_extension.dart';
import 'package:pageMe/utils/room_status_extension.dart';

import '../../../widgets/matrix.dart';

class TimeStamp extends StatelessWidget {
  final Event event;
  final bool isSender;
  final Timeline timeline;

  const TimeStamp({Key? sentKey, required this.event, required this.isSender, required this.timeline}) : super(key: sentKey);

  @override
  Widget build(BuildContext context) {
    return isSender
        ? Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                width: 3,
              ),
              Text(
                event.originServerTs.localizedTimeOfDay(context),
                style: TextStyle(color: Theme.of(context).colorScheme.onPrimaryContainer, fontSize: 11),
              ),
              const SizedBox(
                width: 3,
              ),
              ReadReceipt(event: event, timeline: timeline)
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                width: 3,
              ),
              Text(
                event.originServerTs.localizedTimeOfDay(context),
                style: TextStyle(
                    color:
                        event.messageType == MessageTypes.Image || event.messageType == MessageTypes.Video ? Theme.of(context).colorScheme.onPrimaryContainer : Theme.of(context).colorScheme.onSurface,
                    fontSize: 11),
              ),
              const SizedBox(
                width: 3,
              ),
            ],
          );
  }
}

class ImageFormatStamp extends StatelessWidget {
  final Event event;

  const ImageFormatStamp({Key? sentKey, required this.event}) : super(key: sentKey);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(
          width: 3,
        ),
        Text(
          event.attachmentMimetype,
          style: TextStyle(color: Theme.of(context).colorScheme.onPrimaryContainer, fontSize: 11),
        ),
        const SizedBox(
          width: 3,
        ),
      ],
    );
  }
}

class ReadReceipt extends StatelessWidget {
  final Event event;
  final Timeline timeline;
  const ReadReceipt({Key? key, required this.event, required this.timeline}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    const double tickSize = 15;
    Widget? stateWidget;
    final seenByUsers = event.room.getSeenByUsers(timeline, eventId: event.eventId);
    final bool sending = event.status == EventStatus.sending;
    final bool sent = event.status == EventStatus.sent;
    final bool delivered = event.status == EventStatus.synced;
    bool seen = (seenByUsers.isNotEmpty &&
            seenByUsers.length ==
                event.room.getParticipants().where((element) => element.membership == Membership.join).length -
                    (event.room.getParticipants().where((element) => element.id.contains('@whatsappbot')).length == 1 ? 2 : 1) ||
        (event.room.isDirectChat && timeline.events.first.senderId != client.userID));
    switch (event.type) {
      case EventTypes.Message:
      case EventTypes.Encrypted:
      case EventTypes.Sticker:
        if (sending && !sent && !delivered) {
          stateWidget = Icon(
            Icons.access_time,
            size: tickSize,
            color: Theme.of(context).colorScheme.onPrimaryContainer,
          );
        }
        if (sent && !delivered) {
          stateWidget = Icon(
            Icons.done,
            size: tickSize,
            color: Theme.of(context).colorScheme.onPrimaryContainer,
          );
        }
        if (delivered && !seen) {
          stateWidget = Icon(
            Icons.done_all,
            size: tickSize,
            color: Theme.of(context).colorScheme.onPrimaryContainer,
          );
        }
        if (delivered && seen) {
          stateWidget = const Icon(
            Icons.done_all,
            size: tickSize,
            color: Colors.lightGreen,
          );
        }
        break;
      case EventTypes.CallInvite:
        stateWidget = null;
        break;
      default:
        break;
    }

    return stateWidget ?? const SizedBox.shrink();
  }
}
