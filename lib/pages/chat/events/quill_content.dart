import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:matrix/matrix.dart' as matrix;
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';

import '../../../config/flavor_config.dart';
import '../../../config/themes.dart';
import '../../../utils/platform_infos.dart';
import '../../../utils/url_launcher.dart';
import '../../../widgets/matrix.dart';
import '../chat.dart';

class QuillContent extends StatefulWidget {
  final matrix.Event event;
  final Color textColor;
  const QuillContent({Key? key, required this.event, required this.textColor}) : super(key: key);

  @override
  State<QuillContent> createState() => _QuillContentState();
}

class _QuillContentState extends State<QuillContent> {
  late final ScrollController scrollController;
  late final FocusNode focusNode;

  @override
  void initState() {
    scrollController = ScrollController(debugLabel: "QuillContent ScrollController");
    focusNode = FocusNode(debugLabel: "QuillContent FocusNode", skipTraversal: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    final ownMessage = widget.event.senderId == client.userID;
    final String serializedDelta = widget.event.content['serialized_delta'] as String;
    final delta = quill.Delta.fromJson(jsonDecode(serializedDelta));
    final document = quill.Document.fromDelta(delta);
    final fontSize = FlavorConfig.messageFontSize * FlavorConfig.fontSizeFactor;
    final quill.QuillController controller = quill.QuillController(
      document: document,
      selection: const TextSelection.collapsed(offset: 0),
    );
    final customStyles = quill.DefaultStyles.getInstance(context).merge(
      quill.DefaultStyles(
        paragraph: quill.DefaultTextBlockStyle(
          TextStyle(color: widget.textColor, fontSize: fontSize), // Set the text color here
          const quill.VerticalSpacing(0, 0),
          const quill.VerticalSpacing(0, 0),
          null,
        ),
      ),
    );
    final enableInteractiveSelection = (PlatformInfos.isDesktop || PlatformInfos.isWeb) || (Chat.of(context)!.selectedEvents.contains(widget.event) && PlatformInfos.isMobile);
    return quill.QuillProvider(
      configurations: quill.QuillConfigurations(
          controller: controller
      ),
      child: quill.QuillEditor(
        scrollController: scrollController,
        focusNode: focusNode,
         configurations: quill.QuillEditorConfigurations(
           enableInteractiveSelection: true,
           enableSelectionToolbar: true,
           scrollPhysics: const NeverScrollableScrollPhysics(),
           scrollable: false,
           //enableUnfocusOnTapOutside: true,
           showCursor: false,
           onLaunchUrl: (url) => UrlLauncher(context, url.toString()).launchUrl(),
           autoFocus: false,
           expands: false, detectWordBoundary: true,
           padding: EdgeInsets.zero,
           //controller: controller,
           readOnly: true,
           keyboardAppearance: PageMeThemes.isDarkMode(context) ? Brightness.dark : Brightness.light,
           customStyles: customStyles,
      ),
        // Add any other desired cmization
      ),
    );
  }
}
