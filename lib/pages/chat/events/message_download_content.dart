import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';

import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/string_color.dart';
import 'package:thumbnailer/thumbnailer.dart';

class MessageDownloadContent extends StatelessWidget {
  final Event event;
  final Color textColor;

  const MessageDownloadContent(this.event, this.textColor, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final filename = event.content.tryGet<String>('filename') ?? event.body;
    final filetype = (filename.contains('.')
        ? filename.split('.').last.toUpperCase()
        : event.content.tryGetMap<String, dynamic>('info')?.tryGet<String>('mimetype')?.toUpperCase() ?? 'UNKNOWN');
    final sizeString = event.sizeString;

    return LayoutBuilder(builder: (context, constraints) {
      final double maxWidth = (PlatformInfos.isWeb || PlatformInfos.isDesktop) ? constraints.maxWidth * 0.65 : constraints.maxWidth;
      return Container(
        constraints: BoxConstraints(maxWidth: maxWidth),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Colors.black38,
              ),
              child: ListTile(
                onTap: () async => event.openFile(context),
                titleAlignment: ListTileTitleAlignment.center,
                dense: true,
                leading: SizedBox(
                  width: 20,
                  child: Center(
                    child: Icon(
                      Thumbnailer.getIconDataForMimeType(event.attachmentMimetype) ?? Icons.file_copy,
                      size: 30,
                      color: event.attachmentMimetype.lightColor,
                    ),
                  ),
                ),
                title: Text(
                  filename,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: textColor, fontWeight: FontWeight.w600, fontSize: 14),
                ),
                subtitle: Row(
                  children: [
                    if (sizeString != null)
                      Text(
                        "$sizeString · ",
                        style: TextStyle(color: textColor.withAlpha(150), fontSize: 12),
                      ),
                    Text(
                      filetype,
                      style: TextStyle(color: textColor.withAlpha(150), fontSize: 12),
                    ),
                  ],
                ),
                trailing: PlatformInfos.isMobile
                    ? IconButton(
                      onPressed: () async => await event.saveFile(context),
                      tooltip: L10n.of(context)!.saveFile,
                      icon: Icon(Icons.save_as_outlined, color: Theme.of(context).colorScheme.onPrimaryContainer),
                      style: const ButtonStyle(
                          padding: MaterialStatePropertyAll(EdgeInsets.zero)
                      ),
                    )
                    : null,
              ),
            ),
            const SizedBox(
              height: 14,
            )
          ],
        ),
      );
    });
  }
}
