import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_persistent_keyboard_height/flutter_persistent_keyboard_height.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';

import '../../utils/platform_infos.dart';
import 'chat.dart';
import 'package:matrix/matrix.dart';

class ChatAttachmentPicker extends StatelessWidget {
  final ChatController controller;
  const ChatAttachmentPicker(this.controller, {Key? key}) : super(key: key);

  List<Widget> attachmentOptions(ChatController controller, BuildContext context) {
    final fileHandler = BlocProvider.of<FileHandlerBloc>(context);
    return [
      GestureDetector(
        onTap: (){
          fileHandler.add(const FileSelectionRequested(FileType.any));
          controller.toggleAttachmentPicker();
        },
        child: const Column(
          children: [
            CircleAvatar(
              backgroundColor: Colors.green,
              foregroundColor: Colors.white,
              child: Icon(Icons.file_upload),
            ),
            SizedBox(
              height: 8,
            ),
            Text('File')
          ],
        ),
      ),
      GestureDetector(
        onTap: () {
          fileHandler.add(const FileSelectionRequested(FileType.image));
          controller.toggleAttachmentPicker();
        },
        child: const Column(
          children: [
            CircleAvatar(
              backgroundColor: Colors.blue,
              foregroundColor: Colors.white,
              child: Icon(Icons.image_outlined),
            ),
            SizedBox(
              height: 8,
            ),
            Text('Image')
          ],
        ),
      ),
      GestureDetector(
        onTap: () {
          fileHandler.add(const FileSelectionRequested(FileType.video));
          controller.toggleAttachmentPicker();
        },
        child: const Column(
          children: [
            CircleAvatar(
              backgroundColor: Colors.orange,
              foregroundColor: Colors.white,
              child: Icon(Icons.video_file),
            ),
            SizedBox(
              height: 8,
            ),
            Text('Video')
          ],
        ),
      ),
      if (PlatformInfos.isMobile)
        GestureDetector(
          onTap: () async {
            FocusScope.of(context).requestFocus(FocusNode());
            final file = await ImagePicker().pickImage(source: ImageSource.camera);
            if (file == null) return;
            fileHandler.add(XFileAdditionRequested([file]));
            controller.toggleAttachmentPicker();
          },
          child: const Column(
            children: [
              CircleAvatar(
                backgroundColor: Colors.purple,
                foregroundColor: Colors.white,
                child: Icon(Icons.camera_alt_outlined),
              ),
              SizedBox(
                height: 8,
              ),
              Text('Camera')
            ],
          ),
        ),
      if (PlatformInfos.isMobile)
        GestureDetector(
          onTap: () async {
            FocusScope.of(context).requestFocus(FocusNode());
            final file = await ImagePicker().pickVideo(source: ImageSource.camera);
            if (file == null) return;
            fileHandler.add(XFileAdditionRequested([file]));
            controller.toggleAttachmentPicker();
          },
          child: const Column(
            children: [
              CircleAvatar(
                backgroundColor: Colors.red,
                foregroundColor: Colors.white,
                child: Icon(Icons.videocam_outlined),
              ),
              SizedBox(
                height: 8,
              ),
              Text('Record')
            ],
          ),
        ),
      if (controller.room.getImagePacks(ImagePackUsage.sticker).isNotEmpty)
        GestureDetector(
          onTap: () => controller.onAddPopupMenuButtonSelected('sticker'),
          child: const Column(
            children: [
              CircleAvatar(
                backgroundColor: Colors.orange,
                foregroundColor: Colors.white,
                child: Icon(Icons.emoji_emotions_outlined),
              ),
              SizedBox(
                height: 8,
              ),
              Text('Sticker')
            ],
          ),
        ),
      if (!PlatformInfos.isLinux)
        GestureDetector(
          onTap: () => controller.onAddPopupMenuButtonSelected('location'),
          child: const Column(
            children: [
              CircleAvatar(
                backgroundColor: Colors.brown,
                foregroundColor: Colors.white,
                child: Icon(Icons.gps_fixed_outlined),
              ),
              SizedBox(
                height: 8,
              ),
              Text('Location')
            ],
          ),
        ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = PlatformInfos.isDesktop || PlatformInfos.isWeb;
    Logs().d('${PersistentKeyboardHeight.of(context).keyboardHeight}');
    final double keyboardHeight = isDesktop
        ? 100.00
        : (PersistentKeyboardHeight.of(context).keyboardHeight == 0)
            ? 250
            : PersistentKeyboardHeight.of(context).keyboardHeight;
    return AnimatedContainer(
      color: Theme.of(context).colorScheme.background,
      duration: const Duration(milliseconds: 0),
      height: keyboardHeight,
      child: controller.showAttachmentPicker
          ? (!isDesktop)
              ? GridView.count(padding: const EdgeInsets.all(16), crossAxisCount: 4, children: attachmentOptions(controller, context))
              : Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: attachmentOptions(controller, context)
                      .map((e) => Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                            child: e,
                          ))
                      .toList(),
                )
          : null,
    );
  }
}
