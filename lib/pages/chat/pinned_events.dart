import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:matrix_link_text/link_text.dart';

import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/url_launcher.dart';

class PinnedEvents extends StatelessWidget {
  final ChatController controller;

  const PinnedEvents(this.controller, {Key? key}) : super(key: key);

  Future<void> showPinnedEventsDialog(BuildContext context, List<Event?> events) async {
    await showModalBottomSheet(
      context: context,
      isDismissible: true,
      elevation: 5,
      builder: (BuildContext context) {
        return PinnedEventsDialog(events: events, controller: controller);
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    final pinnedEventIds = controller.room.pinnedEventIds;

    if (pinnedEventIds.isEmpty) {
      return Container();
    }
    final completers = pinnedEventIds.map<Completer<Event?>>((e) {
      final completer = Completer<Event?>();
      controller.room.getEventById(e).then((value) => completer.complete(value));
      return completer;
    });
    return FutureBuilder<List<Event?>>(
        future: Future.wait(completers.map((e) => e.future).toList()),
        builder: (context, snapshot) {
          final pinnedEvents = snapshot.data;
          final event = (pinnedEvents != null && pinnedEvents.isNotEmpty) ? snapshot.data?.last : null;

          if (event == null || pinnedEvents == null) {
            return Container();
          }

          final fontSize = FlavorConfig.messageFontSize * FlavorConfig.fontSizeFactor;
          return Material(
            color: Theme.of(context).appBarTheme.backgroundColor,
            child: InkWell(
              onTap: () async => showPinnedEventsDialog(
                context,
                pinnedEvents,
              ),
              child: Row(
                children: [
                  IconButton(
                    splashRadius: 20,
                    iconSize: 20,
                    color: Theme.of(context).colorScheme.onSurfaceVariant,
                    icon: const Icon(Icons.push_pin),
                    tooltip: L10n.of(context)!.unpin,
                    onPressed: controller.room.canSendEvent(EventTypes.RoomPinnedEvents) ?? false ? () => controller.unpinEvent(event.eventId) : null,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4.0),
                      child: (event.messageType == CustomMessageTypes.quillText)
                          ? LinkText(
                              text: event.calcQuillTextToPlainText(
                                locals: MatrixLocals(L10n.of(context)!),
                              ),
                              maxLines: 2,
                              textStyle: TextStyle(
                                color: Theme.of(context).colorScheme.onSurfaceVariant,
                                overflow: TextOverflow.ellipsis,
                                fontSize: fontSize,
                                decoration: event.redacted ? TextDecoration.lineThrough : null,
                              ),
                              linkStyle: TextStyle(
                                color: Theme.of(context).textTheme.bodyText1?.color?.withAlpha(150),
                                fontSize: fontSize,
                                decoration: TextDecoration.underline,
                              ),
                              onLinkTap: (url) => UrlLauncher(context, url.toString()).launchUrl(),
                            )
                          : FutureBuilder<String>(
                              future: event.calcLocalizedBody(
                                MatrixLocals(L10n.of(context)!),
                                withSenderNamePrefix: true,
                                hideReply: true,
                              ),
                              builder: (context, snapshot) {
                                return LinkText(
                                  text: snapshot.data ??
                                      event.calcLocalizedBodyFallback(
                                        MatrixLocals(L10n.of(context)!),
                                        withSenderNamePrefix: true,
                                        hideReply: true,
                                      ),
                                  maxLines: 2,
                                  textStyle: TextStyle(
                                    color: Theme.of(context).colorScheme.onSurfaceVariant,
                                    overflow: TextOverflow.ellipsis,
                                    fontSize: fontSize,
                                    decoration: event.redacted ? TextDecoration.lineThrough : null,
                                  ),
                                  linkStyle: TextStyle(
                                    color: Theme.of(context).textTheme.bodyText1?.color?.withAlpha(150),
                                    fontSize: fontSize,
                                    decoration: TextDecoration.underline,
                                  ),
                                  onLinkTap: (url) => UrlLauncher(context, url.toString()).launchUrl(),
                                );
                              }),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}

class PinnedEventsDialog extends StatefulWidget {
  final List<Event?> events;
  final ChatController controller;

  const PinnedEventsDialog({Key? key, required this.events, required this.controller}) : super(key: key);

  @override
  PinnedEventsDialogState createState() => PinnedEventsDialogState();
}

class PinnedEventsDialogState extends State<PinnedEventsDialog> {
  @override
  Widget build(BuildContext context) {
    return /*AlertDialog(
      title: Text(L10n.of(context)!.pinMessage),
      contentPadding: const EdgeInsets.all(8),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text("Close"))
      ],
      content: */
        SingleChildScrollView(
            child: ListBody(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 24.0, top: 16, right: 32, bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Pinned messages", style: TextStyle(fontSize: 18)),
                ],
              ),
              Text("${widget.events.length}", style: const TextStyle(fontSize: 18)),
            ],
          ),
        ),
        ...widget.events.map((event) {
          return ListTile(
            style: ListTileStyle.list,
            title: Text(_getEventDisplayText(event)),
            leading: IconButton(
              style: IconButton.styleFrom(padding: EdgeInsets.zero, visualDensity: VisualDensity(horizontal: -4)),
              icon: const Icon(Icons.push_pin, size: 18),
              onPressed: () => _unpinEvent(event),
            ),
            trailing: IconButton(
              icon: Icon(PlatformInfos.isIOS ? CupertinoIcons.chevron_right : Icons.chevron_right),
              onPressed: () async => _navigateToEvent(event),
            ),
            onTap: () async => _navigateToEvent(event),
          );
        }).toList()
      ],
    )); /*,
      ),
    );*/
  }

  String _getEventDisplayText(Event? event) {
    if (event?.messageType == CustomMessageTypes.quillText) {
      return event?.calcQuillTextToPlainText(
            locals: MatrixLocals(L10n.of(context)!),
          ) ??
          'A composed message';
    } else {
      return event?.calcLocalizedBodyFallback(
            MatrixLocals(L10n.of(context)!),
            withSenderNamePrefix: true,
            hideReply: true,
          ) ??
          'UNKNOWN';
    }
  }

  void _unpinEvent(Event? event) {
    if (event != null) {
      setState(() async {
        await widget.controller.unpinEvent(event.eventId);
        await Navigator.of(context).maybePop();
      }); // Close the dialog after unpinning
    }
  }

  Future<void> _navigateToEvent(Event? event) async {
    if (event != null) {
      widget.controller.scrollToEventId(event.eventId);
    }
  }
}
