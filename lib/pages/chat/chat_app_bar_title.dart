import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/utils/room_status_extension.dart';

import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/pages/user_bottom_sheet/user_bottom_sheet.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:pageMe/widgets/avatar.dart';

import '../../widgets/future_loading_dialog.dart';
import '../../widgets/matrix.dart';

class ChatAppBarTitle extends StatefulWidget {
  final ChatController controller;

  const ChatAppBarTitle(this.controller, {Key? key}) : super(key: key);

  @override
  ChatAppBarTitleState createState() => ChatAppBarTitleState();
}

class ChatAppBarTitleState extends State<ChatAppBarTitle> with SingleTickerProviderStateMixin {
  late final Client client;
  late AnimationController _controller;
  late Animation<double> _verticalAnimation;
  late Animation<double> _horizontalAnimation;
  bool _isInitialized = false;
  GetPresenceResponse? presenceResponse;
  List<User>? participantsList;
  String? participants;
  Profile? user;
  StreamSubscription? _onSyncSub;
  Color? colorLeft;
  Color? colorRight;
  String? toolTip;
  bool? userVerified;
  bool? participantsVerified;
  final List<User> unverifiedParticipants = [];

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1200),
    );

    _verticalAnimation = TweenSequence([
      TweenSequenceItem<double>(
        tween: Tween<double>(begin: 0, end: 16).chain(CurveTween(curve: Curves.easeOut)),
        weight: 0.5,
      ),
      TweenSequenceItem<double>(
        tween: Tween<double>(begin: 18, end: 16).chain(CurveTween(curve: Curves.easeOut)),
        weight: 0.5,
      ),
    ]).animate(_controller);

    super.initState();
  }

  void _initHorizontalAnimation(double maxWidth) {
    _horizontalAnimation = TweenSequence([
      TweenSequenceItem<double>(
        tween: Tween<double>(begin: 0, end: 0),
        weight: 0.5,
      ),
      TweenSequenceItem<double>(
        tween: Tween<double>(begin: 0, end: maxWidth).chain(CurveTween(curve: Curves.easeOut)),
        weight: 0.5,
      ),
    ]).animate(_controller);
  }

  void checkUsers(List<User> users) {
    final client = Matrix.of(context).client;
    final room = widget.controller.room;
    users.removeWhere((u) => !{Membership.invite, Membership.join}.contains(u.membership) || !room!.client.userDeviceKeys.containsKey(u.id));
    for (final u in users) {
      //Logs().i( u.id);
      final status = client.userDeviceKeys[u.id]!.verified;
      if (u.id == client.userID) {
        if (status == UserVerifiedStatus.verified) {
          userVerified = true;
        } else if (status == UserVerifiedStatus.unknownDevice) {
          userVerified = false;
        } else {
          userVerified = false;
        }
      } else {
        if (status == UserVerifiedStatus.verified) {
          participantsVerified = true;
        } else if (status == UserVerifiedStatus.unknownDevice) {
          unverifiedParticipants.add(u);
        } else {
          unverifiedParticipants.add(u);
        }
      }
    }
    if (unverifiedParticipants.isEmpty) {
      participantsVerified = true;
    } else {
      participantsVerified = false;
    }
    colorLeft = participantsVerified! ? Colors.transparent : Colors.orangeAccent;
    colorRight = userVerified! ? Colors.transparent : Colors.orangeAccent;
  }

  void _enableEncryptionAction() async {
    final room = widget.controller.room;
    if (room.encrypted) {
      context.go('/rooms/${room.id}/encryption');
      return;
    }
    if (room.joinRules == JoinRules.public) {
      await showOkAlertDialog(
        useRootNavigator: false,
        context: context,
        okLabel: L10n.of(context)!.ok,
        message: L10n.of(context)!.noEncryptionForPublicRooms,
      );
      return;
    }
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.enableEncryption,
          message: room.client.encryptionEnabled ? L10n.of(context)!.enableEncryptionWarning : L10n.of(context)!.needPantalaimonWarning,
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.ok) {
      await showFutureLoadingDialog(
        context: context,
        future: () => room.enableEncryption(),
      );
      // we want to enable the lock icon
      setState(() {});
    }
  }

  @override
  void didChangeDependencies() async {
    client = Matrix.of(context).client;
    user = await client.getProfileFromUserId(client.userID!, cache: true, getFromRooms: true);
    final room = widget.controller.room;
    if (room != null) {
      if (room.isDirectChat) {
        participantsList = room.getParticipants([Membership.join]);
        if (participantsList != null) {
          final numberOfJoinedUsers = participantsList!.length;
          if (numberOfJoinedUsers == 2) {
            checkUsers(participantsList!);
            setToolTips();
            presenceResponse = await client.getPresence(room.directChatMatrixID!);
          }
        }
      } else {
        participantsList = room.getParticipants([Membership.join]);
        if (participantsList != null) {
          checkUsers(participantsList!);
          setToolTips();
          participants = participantsList!.map((user) {
            if (user.id == client.userID!) {
              return 'You';
            } else {
              return user.calcDisplayname();
            }
          }).join(", ");
        } else {
          participants = '';
        }
      }
      if (room.encrypted) {
        _onSyncSub ??= client.onSync.stream.where((s) => s.deviceLists != null).listen((s) => setState(() {}));
      }
    }

    super.didChangeDependencies();
  }

  @override
  Future<void> dispose() async {
    _controller.dispose();
    unawaited(_onSyncSub?.cancel());
    super.dispose();
  }

  void setToolTips() {
    final room = widget.controller.room;
    if (room != null) {
      if (room.encrypted) {
        if (userVerified! && participantsVerified!) {
          toolTip = 'Encrypted, sessions and devices verified';
        } else if (!userVerified! && participantsVerified!) {
          toolTip = 'Encrypted, your device is unverified';
        } else if (userVerified! && !participantsVerified!) {
          toolTip = 'Encrypted, participant device is unverified';
        } else {
          toolTip = 'Encrypted with unverified sessions and devices';
        }
      } else if (!room.encrypted && room.joinRules != JoinRules.public) {
        if (userVerified! && participantsVerified!) {
          toolTip = 'Encryption not enabled, sessions and devices verified';
        } else if (!userVerified! && participantsVerified!) {
          toolTip = 'Encryption not enabled, your device is unverified';
        } else if (userVerified! && !participantsVerified!) {
          toolTip = 'Encryption not enabled, participant device is unverified';
        } else {
          toolTip = 'Encryption not enabled with unverified sessions and devices';
        }
      } else if (!room.encrypted && room.joinRules == JoinRules.public) {
        if (userVerified! && participantsVerified!) {
          toolTip = 'Encryption not available, sessions and devices verified';
        } else if (!userVerified! && participantsVerified!) {
          toolTip = 'Encryption not available, your device is unverified';
        } else if (userVerified! && !participantsVerified!) {
          toolTip = 'Encryption not available, participant device is unverified';
        } else {
          toolTip = 'Encryption not available, with unverified sessions and devices';
        }
      } else {}
    }
  }

  @override
  Widget build(BuildContext context) {
    final room = widget.controller.room;

    if (room == null) {
      return Container();
    }
    if (widget.controller.selectedEvents.isNotEmpty) {
      return Text(
        widget.controller.selectedEvents.length.toString(),
        style: TextStyle(
          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryContainer,
        ),
      );
    }
    final typingText = room.getLocalizedTypingText(context);
    if (typingText.isNotEmpty) {
      _controller.forward();
    } else if (presenceResponse != null) {
      if (presenceResponse!.presence.isOnline) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    } else if (participantsList != null && !room.isDirectChat) {
      if (participantsList!.isNotEmpty) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    } else {
      _controller.reverse();
    }

    return Padding(
      padding: PageMeThemes.isColumnMode(context) ? const EdgeInsets.only(left: 16.0) : EdgeInsets.zero,
      child: InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
        onTap: () => context.go('/rooms/${room.id}/details'),
        child: LayoutBuilder(builder: (context, constraints) {
          final bool isLargeGroupChat;
          if (participantsList != null) {
            isLargeGroupChat = !room.isDirectChat && participantsList!.length >= 4;
          } else {
            isLargeGroupChat = false;
          }
          return SizedBox(
            width: constraints.maxWidth,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                IconButton(
                  padding: EdgeInsets.zero,
                  tooltip: toolTip,
                  icon: SizedBox(
                    width: !(userVerified ?? true) ? 70 : 50,
                    child: Stack(
                      clipBehavior: Clip.none,
                      alignment: Alignment.centerLeft,
                      children: [
                        if (!(userVerified ?? true))
                        Positioned(
                          left: 20,
                          child: Material(
                            shape: userVerified ?? true
                                ? const CircleBorder()
                                : CircleBorder(
                                    side: BorderSide(color: colorRight ?? Colors.transparent, width: 1.25),
                                  ),
                            elevation: 6,
                            child: Avatar(
                              mxContent: user?.avatarUrl,
                              name: user?.displayName ?? user?.userId.localpart ?? 'Unknown',
                              size: 34,
                            ),
                          ),
                        ),
                        Stack(
                          alignment: Alignment.centerRight,
                          clipBehavior: Clip.none,
                          children: [
                            if (isLargeGroupChat)
                              Positioned(
                                left: -7,
                                child: Material(
                                    shape: const CircleBorder(),
                                    elevation: 0,
                                    color: Theme.of(context).colorScheme.secondary.withOpacity(0.4),
                                    child: const SizedBox(
                                      height: 34,
                                      width: 34,
                                    )),
                              ),
                            if (!room.isDirectChat)
                              Positioned(
                                left: -3.5,
                                child: Material(
                                    shape: const CircleBorder(),
                                    elevation: 4,
                                    color: Theme.of(context).colorScheme.secondary.withOpacity(0.8),
                                    child: const SizedBox(
                                      height: 34,
                                      width: 34,
                                    )),
                              ),
                            Material(
                              shape: participantsVerified ?? true
                                  ? const CircleBorder()
                                  : CircleBorder(
                                      side: BorderSide(color: colorLeft ?? Colors.transparent, width: 1.25),
                                    ),
                              elevation: 8,
                              child: Avatar(
                                mxContent: room.avatar,
                                name: room.getLocalizedDisplayname(),
                                size: 35,
                              ),
                            ),
                          ],
                        ),
                        Positioned(
                          right: 10,
                          bottom: 0,
                          child: Icon(
                            (room.encrypted) ? Icons.lock : Icons.lock_open,
                            color: (room.encrypted) ? Colors.green : Colors.orange,
                            size: 10,
                          ),
                        )
                      ],
                    ),
                  ),
                  onPressed: () {
                    _enableEncryptionAction();
                  },
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        room.getLocalizedDisplayname(
                          MatrixLocals(L10n.of(context)!),
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 16,
                            overflow: TextOverflow.ellipsis,
                            color: kIsWeb
                                ? PageMeThemes.isDarkMode(context)
                                    ? Colors.white
                                    : Colors.black
                                : null),
                      ),
                      LayoutBuilder(builder: (context, constraints) {
                        if (!_isInitialized) {
                          _initHorizontalAnimation(constraints.maxWidth);
                          _isInitialized = true;
                        } // 44 can be replaced with the sum width of other elements in the row
                        return AnimatedBuilder(
                          animation: _controller,
                          builder: (BuildContext context, Widget? child) {
                            return Container(
                              width: _horizontalAnimation.value,
                              height: _verticalAnimation.value,
                              clipBehavior: Clip.hardEdge,
                              decoration: const BoxDecoration(), alignment: Alignment.bottomLeft,
                              //duration: const Duration(milliseconds: 1200),
                              //curve: Curves.bounceInOut,
                              padding: const EdgeInsets.only(right: 4),
                              child: (typingText.isNotEmpty)
                                  ? Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          typingText,
                                          style: TextStyle(color: Theme.of(context).colorScheme.secondary, fontSize: 12),
                                          softWrap: false,
                                          overflow: TextOverflow.fade,
                                        )
                                      ],
                                    )
                                  : (presenceResponse != null && room.isDirectChat)
                                      ? (presenceResponse!.presence.isOnline)
                                          ? Text(
                                              presenceResponse!.presence.name,
                                              style: TextStyle(
                                                color: Theme.of(context).colorScheme.secondary,
                                                fontSize: 12,
                                              ),
                                              softWrap: false,
                                              overflow: TextOverflow.fade,
                                            )
                                          : const SizedBox.shrink()
                                      : (participants != null)
                                          ? Text(
                                              participants!,
                                              style: TextStyle(
                                                color: Theme.of(context).colorScheme.secondary,
                                                fontSize: 12,
                                              ),
                                              softWrap: false,
                                              overflow: TextOverflow.ellipsis,
                                            )
                                          : const SizedBox.shrink(),
                            );
                          },
                        );
                      }),
                    ],
                  ),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
