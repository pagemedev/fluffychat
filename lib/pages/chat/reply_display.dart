import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:hive/hive.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';

import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:pageMe/utils/string_color.dart';
import '../../config/flavor_config.dart';
import 'chat.dart';
import 'events/reply_content.dart';

class ReplyDisplay extends StatelessWidget {
  final ChatController controller;
  const ReplyDisplay(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeIn,
      height: _calcReplyContentHeight(),
      clipBehavior: Clip.hardEdge,
      decoration: const BoxDecoration(),
      child: Padding(
        padding: const EdgeInsets.only(top: 6.0, left: 6, right: 6),
        child: Column(
          children: [
            Expanded(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      clipBehavior: Clip.hardEdge,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(topLeft: Radius.circular(11), topRight: Radius.circular(11)),
                          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.surfaceVariant.darken(10) : Theme.of(context).colorScheme.surface.darken(5)),
                      child: controller.replyEvent != null
                          ? Stack(
                              children: [
                                Positioned(
                                  top: -10,
                                  right: -5,
                                  child: IconButton(
                                    padding: EdgeInsets.zero,
                                    tooltip: L10n.of(context)!.close,
                                    icon: const Icon(
                                      Icons.close,
                                      size: 16,
                                    ),
                                    onPressed: controller.cancelReplyEventAction,
                                  ),
                                ),
                                ReplyContent(
                                  controller.replyEvent!,
                                  timeline: controller.timeline!,
                                  hideUnderline: true,
                                ),
                              ],
                            )
                          : Stack(
                              children: [
                                Positioned(
                                  top: -10,
                                  right: -5,
                                  child: IconButton(
                                    padding: EdgeInsets.zero,
                                    tooltip: L10n.of(context)!.close,
                                    icon: const Icon(
                                      Icons.close,
                                      size: 16,
                                    ),
                                    onPressed: controller.cancelReplyEventAction,
                                  ),
                                ),
                                _EditContent(controller.editEvent?.getDisplayEvent(controller.timeline!)),
                              ],
                            ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Theme.of(context).colorScheme.primary,
              height: 2,
            )
          ],
        ),
      ),
    );
  }

  double _calcReplyContentHeight() {
    if (controller.editEvent != null) {
      return 56;
    }
    if (controller.replyEvent != null) {
      if (controller.replyEvent!.messageType == MessageTypes.Image || controller.replyEvent!.messageType == MessageTypes.Video) {
        return 100;
      }
      if (controller.replyEvent!.messageType == MessageTypes.File){
        return 80;
      }
      return 56;
    }
    return 0;
  }
}

class _EditContent extends StatelessWidget {
  final Event? event;

  const _EditContent(this.event);

  @override
  Widget build(BuildContext context) {
    final fontSize = FlavorConfig.messageFontSize * FlavorConfig.fontSizeFactor;
    final event = this.event;
    if (event == null) {
      return Container();
    }
    if (event.messageType == CustomMessageTypes.quillText) {
      return Row(
        children: <Widget>[
          const SizedBox(width: 6),
          Flexible(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Editing',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).colorScheme.primary,
                  fontSize: fontSize,
                ),
              ),
              Text(
                event.calcQuillTextToPlainText(locals: MatrixLocals(L10n.of(context)!), withSenderNamePrefix: false) ?? 'Sent a composed message',
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText2!.color,
                ),
              )
            ],
          )),
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          const SizedBox(width: 6),
          FutureBuilder<String>(
              future: event.calcLocalizedBody(
                MatrixLocals(L10n.of(context)!),
                withSenderNamePrefix: false,
                hideReply: true,
              ),
              builder: (context, snapshot) {
                return Flexible(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Editing',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.primary,
                        fontSize: fontSize,
                      ),
                    ),
                    Text(
                      snapshot.data ??
                          event.calcLocalizedBodyFallback(
                            MatrixLocals(L10n.of(context)!),
                            withSenderNamePrefix: false,
                            hideReply: true,
                          ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(
                        color: Theme.of(context).textTheme.bodyText2!.color,
                      ),
                    )
                  ],
                ));
              }),
        ],
      );
    }
    //event.calcQuillTextToPlainText(locals: MatrixLocals(L10n.of(context)!)) ?? 'Sent a composed message'
  }
}
