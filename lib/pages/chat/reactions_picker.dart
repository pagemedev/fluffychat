import 'package:emoji_proposal/emoji_proposal.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import 'package:matrix/matrix.dart';

import 'package:pageMe/config/app_emojis.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/utils/logger_functions.dart';

import '../../utils/platform_infos.dart';

class ReactionsPicker extends StatelessWidget {
  final Event? event;
  final ChatController controller;
  const ReactionsPicker(this.controller, {Key? key, this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (controller.showEmojiPicker) return const SizedBox.shrink();
    final display = controller.editEvent == null && controller.replyEvent == null && controller.room!.canSendDefaultMessages && (PlatformInfos.isMobile ?  controller.selectedEvents.isNotEmpty : true);
    Logs().v('Display ReactionsPicker: $display');
    return AnimatedContainer(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
        color: Theme.of(context).colorScheme.tertiaryContainer,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.3),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      duration: const Duration(milliseconds: 800),
      height: (display) ? 56 : 0,
      width: (display) ? null : 0,
      child: Material(
        color: Colors.transparent,
        child: Builder(builder: (context) {
          if (!display) {
            return Container();
          }
          final proposals = proposeEmojis(event != null ? event!.plaintextBody : controller.selectedEvents.first.plaintextBody, number: 25, languageCodes: EmojiProposalLanguageCodes.values.toSet());
          final emojis = proposals.isNotEmpty ? proposals.map((e) => e.char).toList() : List<String>.from(AppEmojis.emojis);
          final allReactionEvents = (event != null ? event! : controller.selectedEvents.first)
              .aggregatedEvents(controller.timeline!, RelationshipTypes.reaction)
              .where((event) => event.senderId == event.room.client.userID && event.type == 'm.reaction');

          for (final event in allReactionEvents) {
            try {
              emojis.remove((event.content['m.relates_to'] as Map)['key']);
            } catch (_) {}
          }
          return Row(
            children: [
              Expanded(
                  child: Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(FlavorConfig.borderRadius), topLeft: Radius.circular(FlavorConfig.borderRadius))),
                      padding: const EdgeInsets.only(right: 1),
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: emojis.length,
                        itemBuilder: (c, i) => InkWell(
                          borderRadius: BorderRadius.circular(8),
                          onTap: () => controller.sendEmojiAction(emojis[i], event),
                          child: Container(
                            width: 56,
                            height: 56,
                            alignment: Alignment.center,
                            child: Text(
                              emojis[i],
                              style: const TextStyle(fontSize: 30),
                            ),
                          ),
                        ),
                      ))),
              IconButton(
                onPressed: () {
                  controller.removeReactionsOverlay();
                  controller.pickEmojiReactionAction(allReactionEvents, event);
                },
                icon: const Icon(Icons.add_outlined),
                color: Theme.of(context).colorScheme.onPrimary,
                style: IconButton.styleFrom(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(FlavorConfig.borderRadius - 4)), backgroundColor: Theme.of(context).colorScheme.primary),
              ),
              const SizedBox(
                width: 4,
              )
            ],
          );
        }),
      ),
    );
  }
}
