import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/widgets/avatar.dart';
import 'package:pageMe/widgets/matrix.dart';

class TypingIndicators extends StatelessWidget {
  final ChatController controller;
  const TypingIndicators(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final typingUsers = controller.room!.typingUsers..removeWhere((u) => u.stateKey == Matrix.of(context).client.userID);
    const topPadding = 15.0;
    const bottomPadding = 4.0;

    return Stack(
      children: [
        Container(
          width: double.infinity,
          alignment: Alignment.center,
          child: AnimatedContainer(
            //constraints: const BoxConstraints(maxWidth: PageMeThemes.columnWidth * 2.5),
            height: typingUsers.isEmpty ? 0 : Avatar.defaultSize + bottomPadding,
            duration: const Duration(milliseconds: 300),
            curve: Curves.bounceInOut,
            alignment: controller.timeline!.events.isNotEmpty &&
                controller.timeline!.events.first.senderId ==
                    Matrix.of(context).client.userID
                ? Alignment.topRight
                : Alignment.topLeft,
            clipBehavior: Clip.hardEdge,
            decoration: const BoxDecoration(),
            padding: const EdgeInsets.only(
              left: 8.0,
              bottom: bottomPadding,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(
                  height: Avatar.defaultSize,
                  width: typingUsers.length < 2 ? Avatar.defaultSize : Avatar.defaultSize + 16,
                  child: Stack(
                    children: [
                      if (typingUsers.isNotEmpty)
                        Avatar(
                          mxContent: typingUsers.first.avatarUrl,
                          name: typingUsers.first.calcDisplayname(),
                        ),
                      if (typingUsers.length == 2)
                        Padding(
                          padding: const EdgeInsets.only(left: 16),
                          child: Avatar(
                            mxContent: typingUsers.length == 2 ? typingUsers.last.avatarUrl : null,
                            name: typingUsers.length == 2 ? typingUsers.last.calcDisplayname() : '+${typingUsers.length - 1}',
                          ),
                        ),
                    ],
                  ),
                ),
                const SizedBox(width: 8),
              ],
            ),
          ),
        ),
        Positioned(
          left: 45,
          child: Align(
            alignment: Alignment.bottomLeft,
            child: Builder(builder: (context) {
              if (typingUsers.isEmpty) return Container();
              return Lottie.asset(
                  'assets/typing.json',
                  repeat: true,
                  height: 75,
              );
            }),
          ),
        )

      ],
    );
  }
}
