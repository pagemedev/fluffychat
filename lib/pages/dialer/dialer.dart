import 'dart:async';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';

import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:just_audio/just_audio.dart';
import 'package:lottie/lottie.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';
import 'package:vibration/vibration.dart';
import 'package:wakelock/wakelock.dart';

import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/widgets/avatar.dart';
import 'pip/pip_view.dart';

class _StreamView extends StatefulWidget {
  const _StreamView(this.wrappedStream, {Key? key, this.mainView = false, required this.matrixClient, required this.call}) : super(key: key);

  final WrappedMediaStream wrappedStream;
  final Client matrixClient;
  final CallSession call;
  final bool mainView;

  @override
  State<_StreamView> createState() => _StreamViewState();
}

class _StreamViewState extends State<_StreamView> {
  Uri? get avatarUrl => widget.call.room.avatar;

  String? get displayName => widget.wrappedStream.displayName;

  String get callerName => widget.call.displayName ?? '';

  String get avatarName => widget.wrappedStream.avatarName;

  bool get isLocal => widget.wrappedStream.isLocal();

  bool get mirrored => widget.wrappedStream.isLocal() && widget.wrappedStream.purpose == SDPStreamMetadataPurpose.Usermedia;

  bool get audioMuted => widget.wrappedStream.audioMuted;

  bool get videoMuted => widget.wrappedStream.videoMuted;

  bool get isScreenSharing => widget.wrappedStream.purpose == SDPStreamMetadataPurpose.Screenshare;

  final StopWatchTimer _stopWatchTimer = StopWatchTimer(mode: StopWatchMode.countUp);

  @override
  void dispose() async {
    super.dispose();
    await _stopWatchTimer.dispose(); // Need to call dispose function.
  }

  @override
  Widget build(BuildContext context) {
    String? callStatus;

    switch (widget.call.state) {
      case CallState.kRinging:
        callStatus = "Ringing";
        break;
      case CallState.kFledgling:
        callStatus = "Initializing";
        break;
      case CallState.kInviteSent:
        callStatus = "Calling";
        break;
      case CallState.kWaitLocalMedia:
        callStatus = "Fetching Stream";
        break;
      case CallState.kCreateOffer:
        callStatus = "Created Offer";
        break;
      case CallState.kCreateAnswer:
        callStatus = "Created Answer";
        break;
      case CallState.kConnecting:
        callStatus = "Connecting";
        break;
      case CallState.kConnected:
        callStatus = "Connected";
        // Start timer.
        _stopWatchTimer.onStartTimer();
        break;
      case CallState.kEnded:
        callStatus = "Ended";
        // Reset timer
        _stopWatchTimer.onResetTimer();
        break;
    }

    Logs().i( 'User Display Name: $displayName');
    return Container(
        decoration: const BoxDecoration(
          color: Colors.transparent,
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            if (videoMuted)
              Container(
                color: Colors.transparent,
              ),
            if (!videoMuted)
              RTCVideoView(
                // yes, it must explicitly be casted even though I do not feel
                // comfortable with it...
                widget.wrappedStream.renderer as RTCVideoRenderer,
                mirror: mirrored,
                objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitContain,
              ),
            if (videoMuted)
              Positioned(
                  top: 125,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Avatar(
                        mxContent: avatarUrl,
                        name: callerName,
                        size: widget.mainView ? 96 : 48,
                        client: widget.matrixClient,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          callerName ?? '',
                          style: TextStyle(color: PageMeThemes.blackWhiteColor(context), fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                      ),
                       Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          '${FlavorConfig.applicationName} voice call',
                          style: TextStyle(color: PageMeThemes.blackWhiteColor(context), fontSize: 20),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          callStatus ?? '',
                          style: TextStyle(color: PageMeThemes.blackWhiteColor(context), fontSize: 18),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      if (widget.call.state == CallState.kConnected)
                        StreamBuilder<int>(
                          stream: _stopWatchTimer.rawTime,
                          initialData: 0,
                          builder: (context, snap) {
                            final value = snap.data;
                            final displayTime = StopWatchTimer.getDisplayTime(
                              value!,
                              second: true,
                              hours: true,
                              minute: true,
                              milliSecond: false,
                            );
                            return Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    displayTime,
                                    style: TextStyle(color: PageMeThemes.blackWhiteColor(context), fontSize: 18),
                                  ),
                                ),
                              ],
                            );
                          },
                        ),
                    ],
                  )),
/*
            if (!isScreenSharing)
              Positioned(
                left: 4.0,
                bottom: 4.0,
                child: Icon(audioMuted ? Icons.mic_off : Icons.mic, color: Colors.white, size: 18.0),
              )
*/
          ],
        ));
  }
}

class Calling extends StatefulWidget {
  final Brightness brightness;
  final VoidCallback? onClear;
  final BuildContext context;
  final String callId;
  final CallSession call;
  final Client client;

  const Calling({required this.context, required this.call, required this.client, required this.callId, this.onClear, Key? key, required this.brightness})
      : super(key: key);

  @override
  MyCallingPage createState() => MyCallingPage();
}

class MyCallingPage extends State<Calling> {
  Room? get room => call.room;

  String get displayName => call.displayName ?? '';

  String get callId => widget.callId;

  CallSession get call => widget.call;

  Brightness get brightness => widget.brightness;

  MediaStream? get localStream {
    if (call.localUserMediaStream != null) {
      return call.localUserMediaStream!.stream!;
    }
    return null;
  }

  MediaStream? get remoteStream {
    if (call.getRemoteStreams.isNotEmpty) {
      return call.getRemoteStreams[0].stream!;
    }
    return null;
  }

  bool get speakerOn => call.speakerOn;

  bool get isMicrophoneMuted => call.isMicrophoneMuted;

  bool get isLocalVideoMuted => call.isLocalVideoMuted;

  bool get isScreensharingEnabled => call.screensharingEnabled;

  bool get isRemoteOnHold => call.remoteOnHold;

  bool get voiceonly => call.type == CallType.kVoice;

  bool get connecting => call.state == CallState.kConnecting;

  bool get connected => call.state == CallState.kConnected;

  bool get mirrored => call.facingMode == 'user';

  List<WrappedMediaStream> get streams => call.streams;
  double? _localVideoHeight;
  double? _localVideoWidth;
  EdgeInsetsGeometry? _localVideoMargin;
  CallState? _state;

  void _playCallSound() async {
    const path = 'assets/sounds/call.ogg';
    if (kIsWeb || PlatformInfos.isMobile || PlatformInfos.isMacOS) {
      final player = AudioPlayer();
      await player.setAsset(path);
      player.play();
    } else {
      Logs().w('Playing sound not implemented for this platform!');
    }
  }

  @override
  void initState() {
    super.initState();
    initialize();
    _playCallSound();
  }

  void initialize() async {
    final call = this.call;
    call.onCallStateChanged.stream.listen(_handleCallState);
    call.onCallEventChanged.stream.listen((event) {
      if (event == CallEvent.kFeedsChanged) {
        setState(() {
          call.tryRemoveStopedStreams();
        });
      } else if (event == CallEvent.kLocalHoldUnhold || event == CallEvent.kRemoteHoldUnhold) {
        setState(() {});
        Logs().i('Call hold event: local ${call.localHold}, remote ${call.remoteOnHold}');
      }
    });
    _state = call.state;

    if (call.type == CallType.kVideo) {
      try {
        // Enable wakelock (keep screen on)
        unawaited(Wakelock.enable());
      } catch (_) {}
    }
  }

  void cleanUp() {
    Timer(
      const Duration(seconds: 2),
      () => widget.onClear?.call(),
    );
    if (call.type == CallType.kVideo) {
      try {
        unawaited(Wakelock.disable());
      } catch (_) {}
    }
  }

  @override
  void dispose() {
    super.dispose();
    call.cleanUp.call();
  }

  void _resizeLocalVideo(Orientation orientation) {
    final shortSide = min(MediaQuery.sizeOf(context).width, MediaQuery.sizeOf(context).height);
    _localVideoMargin = remoteStream != null ? const EdgeInsets.only(top: 20.0, right: 20.0) : EdgeInsets.zero;
    _localVideoWidth = remoteStream != null ? shortSide / 3 : MediaQuery.sizeOf(context).width;
    _localVideoHeight = remoteStream != null ? shortSide / 4 : MediaQuery.sizeOf(context).height;
  }

  void _handleCallState(CallState state) {
    Logs().v('CallingPage::handleCallState: ${state.toString()}');
    if ({CallState.kConnected, CallState.kEnded}.contains(state)) {
      try {
        Vibration.vibrate(duration: 200);
      } catch (e) {
        Logs().e('[Dialer] could not vibrate for call updates');
      }
    }

    if (mounted) {
      setState(() {
        _state = state;
        if (_state == CallState.kEnded) cleanUp();
      });
    }
  }

  void _answerCall() {
    Logs().i( "Test");
    setState(() {
      call.answer();
    });
  }

  void _hangUp() {
    setState(() {
      if (call.isRinging) {
        call.reject();
      } else {
        call.hangup();
      }
    });
  }

  void _muteMic() {
    setState(() {
      call.setMicrophoneMuted(!call.isMicrophoneMuted);
    });
  }

  void _screenSharing() async {
    if (PlatformInfos.isAndroid) {
      if (!call.screensharingEnabled) {
        FlutterForegroundTask.init(
          androidNotificationOptions: AndroidNotificationOptions(
            channelId: 'notification_channel_id',
            channelName: 'Foreground Notification',
            channelDescription: 'This notification appears when the foreground service is running.',
          ),
          iosNotificationOptions: const IOSNotificationOptions(),
          foregroundTaskOptions: const ForegroundTaskOptions(),
        );
        FlutterForegroundTask.startService(
            notificationTitle: 'screen sharing', notificationText: 'You are sharing your screen in ${FlavorConfig.applicationName}');
      } else {
        FlutterForegroundTask.stopService();
      }
    }

    setState(() {
      call.setScreensharingEnabled(!call.screensharingEnabled);
    });
  }

  void _remoteOnHold() {
    setState(() {
      call.setRemoteOnHold(!call.remoteOnHold);
    });
  }

  void _muteCamera() {
    setState(() {
      call.setLocalVideoMuted(!call.isLocalVideoMuted);
    });
  }

  void _switchCamera() async {
    if (call.localUserMediaStream != null) {
      await Helper.switchCamera(call.localUserMediaStream!.stream!.getVideoTracks()[0]);
      if (PlatformInfos.isMobile) {
        call.facingMode == 'user' ? call.facingMode = 'environment' : call.facingMode = 'user';
      }
    }
    setState(() {});
  }

  /*
  void _switchSpeaker() {
    setState(() {
      session.setSpeakerOn();
    });
  }
  */

  List<Widget> _buildActionButtons(bool isFloating) {
    if (isFloating) {
      return [];
    }

    final switchCameraButton = FloatingActionButton(
      heroTag: 'switchCamera',
      onPressed: _switchCamera,
      backgroundColor: Colors.black45,
      child: const Icon(Icons.switch_camera),
    );

    final hangupButton = FloatingActionButton(
      heroTag: 'hangup',
      onPressed: _hangUp,
      tooltip: 'Hangup',
      backgroundColor: _state == CallState.kEnded ? Colors.black45 : Colors.red,
      child: const Icon(Icons.call_end),
    );

    final answerButton = Hero(
      tag: 'answer',
      child: GestureDetector(
        onTap: _answerCall,
        child: Lottie.asset(
          'assets/green_phone_call.json',
          repeat: true,
          height: 80,
        ),
      ),
    );

    final connectingAnimation = Lottie.asset(
      'assets/connecting_dots.json',
      repeat: true,
      height: 100,
    );

    final muteMicButton = FloatingActionButton(
      heroTag: 'muteMic',
      onPressed: _muteMic,
      foregroundColor: isMicrophoneMuted ? Colors.black26 : Colors.white,
      backgroundColor: isMicrophoneMuted ? Colors.white : Colors.black45,
      child: Icon(isMicrophoneMuted ? Icons.mic_off : Icons.mic),
    );

    final screenSharingButton = FloatingActionButton(
      heroTag: 'screenSharing',
      onPressed: _screenSharing,
      foregroundColor: isScreensharingEnabled ? Colors.black26 : Colors.white,
      backgroundColor: isScreensharingEnabled ? Colors.white : Colors.black45,
      child: const Icon(Icons.desktop_mac),
    );

    final holdButton = FloatingActionButton(
      heroTag: 'hold',
      onPressed: _remoteOnHold,
      foregroundColor: isRemoteOnHold ? Colors.black26 : Colors.white,
      backgroundColor: isRemoteOnHold ? Colors.white : Colors.black45,
      child: const Icon(Icons.pause),
    );

    final muteCameraButton = FloatingActionButton(
      heroTag: 'muteCam',
      onPressed: _muteCamera,
      foregroundColor: isLocalVideoMuted ? Colors.black26 : Colors.white,
      backgroundColor: isLocalVideoMuted ? Colors.white : Colors.black45,
      child: Icon(isLocalVideoMuted ? Icons.videocam_off : Icons.videocam),
    );

    switch (_state) {
      case CallState.kRinging:
        return call.isOutgoing ? <Widget>[hangupButton] : <Widget>[answerButton, hangupButton];
      case CallState.kInviteSent:
      case CallState.kCreateAnswer:
      case CallState.kConnecting:
        return call.isOutgoing ? <Widget>[hangupButton] : <Widget>[connectingAnimation];
      case CallState.kConnected:
        return <Widget>[
          muteMicButton,
          //switchSpeakerButton,
          if (!voiceonly && !kIsWeb) switchCameraButton,
          if (!voiceonly) muteCameraButton,
          if (PlatformInfos.isMobile || PlatformInfos.isWeb) screenSharingButton,
          holdButton,
          hangupButton,
        ];
      case CallState.kEnded:
        return <Widget>[
          hangupButton,
        ];
      case CallState.kFledgling:
        // TODO: Handle this case.
        break;
      case CallState.kWaitLocalMedia:
        // TODO: Handle this case.
        break;
      case CallState.kCreateOffer:
        // TODO: Handle this case.
        break;
      case null:
        // TODO: Handle this case.
        break;
    }
    return <Widget>[];
  }

  List<Widget> _buildContent(Orientation orientation, bool isFloating) {
    final stackWidgets = <Widget>[];

    final call = this.call;
    if (call.callHasEnded) {
      return stackWidgets;
    }

    if (call.localHold || call.remoteOnHold) {
      var title = '';
      if (call.localHold) {
        title = '${call.displayName} held the call.';
      } else if (call.remoteOnHold) {
        title = 'You held the call.';
      }
      stackWidgets.add(Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          const Icon(
            Icons.pause,
            size: 48.0,
            color: Colors.white,
          ),
          Text(
            title,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 24.0,
            ),
          )
        ]),
      ));
      return stackWidgets;
    }

    var primaryStream = call.remoteScreenSharingStream ?? call.localScreenSharingStream ?? call.remoteUserMediaStream ?? call.localUserMediaStream;

    if (!connected) {
      primaryStream = call.localUserMediaStream;
    }

    if (primaryStream != null) {
      stackWidgets.add(Center(
        child: _StreamView(
          primaryStream,
          mainView: true,
          matrixClient: widget.client,
          call: call,
        ),
      ));
    }

    if (isFloating || !connected) {
      return stackWidgets;
    }

    _resizeLocalVideo(orientation);

    if (call.getRemoteStreams.isEmpty) {
      return stackWidgets;
    }

    final secondaryStreamViews = <Widget>[];

    if (call.remoteScreenSharingStream != null) {
      final remoteUserMediaStream = call.remoteUserMediaStream;
      secondaryStreamViews.add(SizedBox(
        width: _localVideoWidth,
        height: _localVideoHeight,
        child: _StreamView(
          remoteUserMediaStream!,
          matrixClient: widget.client,
          call: call,
        ),
      ));
      secondaryStreamViews.add(const SizedBox(height: 10));
    }

    final localStream = call.localUserMediaStream ?? call.localScreenSharingStream;
    if (localStream != null && !isFloating) {
      secondaryStreamViews.add(SizedBox(
        width: _localVideoWidth,
        height: _localVideoHeight,
        child: _StreamView(
          localStream,
          matrixClient: widget.client,
          call: call,
        ),
      ));
      secondaryStreamViews.add(const SizedBox(height: 10));
    }

    if (call.localScreenSharingStream != null && !isFloating) {
      secondaryStreamViews.add(SizedBox(
        width: _localVideoWidth,
        height: _localVideoHeight,
        child: _StreamView(
          call.remoteUserMediaStream!,
          matrixClient: widget.client,
          call: call,
        ),
      ));
      secondaryStreamViews.add(const SizedBox(height: 10));
    }

    if (secondaryStreamViews.isNotEmpty) {
      stackWidgets.add(Container(
        padding: const EdgeInsets.fromLTRB(0, 20, 0, 120),
        alignment: Alignment.bottomRight,
        child: Container(
          width: _localVideoWidth,
          margin: _localVideoMargin,
          child: Column(
            children: secondaryStreamViews,
          ),
        ),
      ));
    }

    return stackWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return PIPView(builder: (context, isFloating) {
      return Scaffold(
          resizeToAvoidBottomInset: !isFloating,
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          floatingActionButton: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 400, minWidth: 340, maxHeight: 150, minHeight: 150),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: _buildActionButtons(isFloating),
            ),
          ),
          body: OrientationBuilder(builder: (BuildContext context, Orientation orientation) {
            return Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    brightness == Brightness.light ? 'assets/background_light.png' : 'assets/background_dark.png',
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: Stack(
                children: [
                  ..._buildContent(orientation, isFloating),
                  if (!isFloating)
                    Positioned(
                        top: 24.0,
                        left: 24.0,
                        child: IconButton(
                          color: Colors.black45,
                          icon: const Icon(Icons.arrow_back),
                          onPressed: () {
                            PIPView.of(context)?.setFloating(true);
                          },
                        )),
                ],
              ),
            );
          }));
    });
  }
}
