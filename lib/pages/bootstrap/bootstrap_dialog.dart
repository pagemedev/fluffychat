import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:lottie/lottie.dart';
import 'package:matrix/encryption.dart';
import 'package:matrix/encryption/utils/bootstrap.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/keys/pageme_client_appkey.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:pointycastle/asymmetric/api.dart';
import 'package:share_plus/share_plus.dart';

import '../../config/setting_keys.dart';
import '../../config/themes.dart';
import '../../utils/famedlysdk_store.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/adaptive_flat_button.dart';
import '../key_verification/key_verification_dialog.dart';

class BootstrapDialog extends StatefulWidget {
  final bool wipe;
  final bool isOnBoardingScreen;
  final Client client;
  const BootstrapDialog({
    Key? key,
    this.wipe = false,
    this.isOnBoardingScreen = false,
    required this.client,
  }) : super(key: key);

  Future<bool?> show(BuildContext context) => PlatformInfos.isCupertinoStyle
      ? showCupertinoDialog(
          context: context,
          builder: (context) => this,
          barrierDismissible: true,
          useRootNavigator: false,
        )
      : showDialog(
          context: context,
          builder: (context) => this,
          barrierDismissible: true,
          useRootNavigator: false,
        );

  @override
  BootstrapDialogState createState() => BootstrapDialogState();
}

class BootstrapDialogState extends State<BootstrapDialog> with SingleTickerProviderStateMixin {
  final TextEditingController _recoveryKeyTextEditingController = TextEditingController();
  late MatrixState matrix;
  late NavigatorState navigator;
  late GoRouteInformationProvider go;
  late ThemeData theme;
  late final AnimationController animationController;
  bool shouldShowRecoveryKeyMessage = false;
  L10n? l10n;
  Bootstrap? bootstrap;
  bool _isInitialized = false;
  bool _shouldSendKey = false;
  String? _recoveryKeyInputError;
  bool _isRecoveryKeyStoredOnServer = false;
  bool _recoveryKeyInputLoading = false;
  Store? _store;
  Store get store => _store ??= Store();
  String? titleText;

  bool _recoveryKeyStored = false;
  bool _recoveryKeyCopied = false;
  bool _recoveryKeyShared = false;

  bool? _storeInSecureStorage = false;

  bool? _wipe;

  String get _secureStorageKey => 'ssss_recovery_key_${bootstrap!.client.userID}';

  bool get _supportsSecureStorage => PlatformInfos.isMobile || PlatformInfos.isDesktop;

  String _getSecureStorageLocalizedName() {
    if (PlatformInfos.isAndroid) {
      return l10n!.storeInAndroidKeystore;
    }
    if (PlatformInfos.isIOS || PlatformInfos.isMacOS) {
      return l10n!.storeInAppleKeyChain;
    }
    return l10n!.storeSecurlyOnThisDevice;
  }

  @override
  void initState() {
    animationController = AnimationController(vsync: this);
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    l10n = L10n.of(context);
    navigator = Navigator.of(context);
    go = PageMeApp.router.routeInformationProvider;
    matrix = Matrix.of(context);
    theme = Theme.of(context);
    if (!_isInitialized) {
      if (widget.isOnBoardingScreen) {
        await _waitForFirstSync();
      }
      await _createBootstrap(widget.wipe);
      _isInitialized = true;
    }

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _recoveryKeyTextEditingController.dispose();
    super.dispose();
  }

  String encryptRecoveryKey(String recoveryKey) {
    final String publicKeyString = matrix.homeServer!.publicKey;
    final parser = encrypt.RSAKeyParser();
    final RSAPublicKey publicKey = parser.parse(publicKeyString) as RSAPublicKey;

    final encrypt.Encrypter encrypter = encrypt.Encrypter(encrypt.RSA(publicKey: publicKey));

    final encrypt.Encrypted encryptedText = encrypter.encrypt(recoveryKey);
    return encryptedText.base64;
  }

  Future<void> _createBootstrap(bool wipe) async {
    _wipe = wipe;
    titleText = null;
    _recoveryKeyStored = false;
    bootstrap = widget.client.encryption!.bootstrap(onUpdate: (bootstrap) async {
      setState(() {});
    });
    final key = await const FlutterSecureStorage().read(key: _secureStorageKey);
    if (key == null) return;
    _recoveryKeyTextEditingController.text = key;
  }

  Future<void> _waitForFirstSync() async {
    final client = matrix.client;
    await client.roomsLoading;
    await client.accountDataLoading;
    if (client.prevBatch == null) {
      await client.onSync.stream.first;
    }
  }

  Future<void> sendEncryptedRecoveryKey(String key) async {
    setState(() {
      shouldShowRecoveryKeyMessage = true;
    });
    final String encryptedRecoveryKey = encryptRecoveryKey(key);
    final result =
        await matrix.pagemeClient.user.addUserEncryptionKey(userId: matrix.client.userID!, encryptedEncrytionKey: encryptedRecoveryKey, appKey: PageMeClientAppKey.appKey);
    _shouldSendKey = false;
    setState(() {
      _isRecoveryKeyStoredOnServer = result;
    });
    Logs().i('Encryption key stored in serverpod: $_isRecoveryKeyStoredOnServer for user: ${matrix.client.userID}, encryptedRecoveryKey: $encryptedRecoveryKey');
  }

  @override
  Widget build(BuildContext context) {
    _wipe ??= widget.wipe;
    final buttons = <AdaptiveFlatButton>[];
    Widget body = Center(child: PlatformInfos.isCupertinoStyle ? const CupertinoActivityIndicator() : const CircularProgressIndicator());
    titleText = l10n!.loadingPleaseWait;

    final columnModeBotNavPadding = (MediaQuery.sizeOf(context).width - PageMeThemes.columnWidth * 1.5) / 2;
    if (bootstrap == null) {
      return _buildWaitingForBootstrap(context, columnModeBotNavPadding);
    }
    final shouldCreateRecoveryKey = bootstrap!.newSsssKey?.recoveryKey != null && _recoveryKeyStored == false;
    if (shouldCreateRecoveryKey) {
      _shouldSendKey = true;
      final key = bootstrap!.newSsssKey!.recoveryKey;
      titleText = l10n!.recoveryKey;
      return _buildCreateNewRecoveryKey(context, columnModeBotNavPadding, key);
    } else {
      switch (bootstrap!.state) {
        case BootstrapState.loading:
          break;
        case BootstrapState.askWipeSsss:
          WidgetsBinding.instance.addPostFrameCallback(
            (_) => bootstrap!.wipeSsss(_wipe!),
          );
          break;
        case BootstrapState.askBadSsss:
          WidgetsBinding.instance.addPostFrameCallback(
            (_) => bootstrap!.ignoreBadSecrets(true),
          );
          break;
        case BootstrapState.askUseExistingSsss:
          WidgetsBinding.instance.addPostFrameCallback(
            (_) => bootstrap!.useExistingSsss(!_wipe!),
          );
          break;
        case BootstrapState.askUnlockSsss:
          WidgetsBinding.instance.addPostFrameCallback(
            (_) => bootstrap!.unlockedSsss(),
          );
          break;
        case BootstrapState.askNewSsss:
          WidgetsBinding.instance.addPostFrameCallback(
            (_) async => bootstrap!.newSsss(),
          );
          break;
        case BootstrapState.openExistingSsss:
          _recoveryKeyStored = true;
          _shouldSendKey = false;
          return _buildOpenExistingSsss(context);
        case BootstrapState.askWipeCrossSigning:
          WidgetsBinding.instance.addPostFrameCallback(
            (_) async => bootstrap!.wipeCrossSigning(_wipe!),
          );
          break;
        case BootstrapState.askSetupCrossSigning:
          WidgetsBinding.instance.addPostFrameCallback(
            (_) async => bootstrap!.askSetupCrossSigning(
              setupMasterKey: true,
              setupSelfSigningKey: true,
              setupUserSigningKey: true,
            ),
          );
          break;
        case BootstrapState.askWipeOnlineKeyBackup:
          WidgetsBinding.instance.addPostFrameCallback(
            (_) => bootstrap!.wipeOnlineKeyBackup(_wipe!),
          );

          break;
        case BootstrapState.askSetupOnlineKeyBackup:
          WidgetsBinding.instance.addPostFrameCallback(
            (_) async => bootstrap!.askSetupOnlineKeyBackup(true),
          );
          break;
        case BootstrapState.error:
          _shouldSendKey = false;
          titleText = l10n!.oopsSomethingWentWrong;
          body = const Icon(Icons.error_outline, color: Colors.red, size: 40);
          buttons.add(AdaptiveFlatButton(
            label: l10n!.close,
            onPressed: () async {
              if (widget.isOnBoardingScreen) {
                await store.setItemBool(SettingKeys.firstTimeUser + matrix.client.userID!, false);
                context.go("/rooms");
              } else {
                navigator.pop<bool>(false);
              }
            },
          ));
          break;
        case BootstrapState.done:
          if (isBusiness() && bootstrap!.newSsssKey?.recoveryKey != null && _shouldSendKey) {
            unawaited(sendEncryptedRecoveryKey(bootstrap!.newSsssKey!.recoveryKey!));
          }
          titleText = l10n!.everythingReady;
          body = Material(
            color: Colors.transparent,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 25.0),
                      child: SizedBox(
                        height: 150,
                        child: LottieBuilder.asset(
                          'assets/unlocked-animation-dark.json',
                          repeat: false,
                          addRepaintBoundary: true,
                          fit: BoxFit.fitHeight,
                          controller: animationController,
                          frameRate: FrameRate(60),
                          onLoaded: (composition) {
                            animationController
                              ..duration = composition.duration
                              ..forward();
                          },
                        ),
                      ),
                    ),
                  ),
                  CheckboxListTile(
                    title: Text(shouldShowRecoveryKeyMessage ? "Key Enabled" : "Messages Restored"),
                    value: true,
                    visualDensity: const VisualDensity(horizontal: -4, vertical: -4),
                    onChanged: (_) {},
                  ),
                  if (isBusiness() && shouldShowRecoveryKeyMessage)
                    CheckboxListTile(
                      title: const Text("Cloud backup"),
                      value: _isRecoveryKeyStoredOnServer,
                      visualDensity: const VisualDensity(horizontal: -4, vertical: -4),
                      onChanged: (_) {},
                    ),
                ],
              ),
            ),
          );
          buttons.add(AdaptiveFlatButton(
            label: l10n!.close,
            onPressed: () async {
              if (widget.isOnBoardingScreen) {
                await store.setItemBool(SettingKeys.firstTimeUser + matrix.client.userID!, false);
                context.go("/rooms");
              } else {
                navigator.pop<bool>(false);
              }
            },
          ));
          break;
      }
    }
    final title = Text(
      titleText!,
      style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
    );
/*    if (PlatformInfos.isCupertinoStyle) {
      return CupertinoAlertDialog(
        title: title,
        content: body,
        actions: buttons,
      );
    }*/
    return _buildDoneScreen(context, title, body, buttons, columnModeBotNavPadding);
    /*AlertDialog(
      title: title,
      content: body,
      actions: buttons,
    );*/
  }

  Widget _buildWaitingForBootstrap(BuildContext context, double columnModeBotNavPadding) {
    return ClipRRect(
      borderRadius: PageMeThemes.isColumnMode(context) ?  BorderRadius.circular(FlavorConfig.borderRadius) : BorderRadius.zero,
      child: Scaffold(
        backgroundColor: widget.isOnBoardingScreen ? PageMeThemes.isColumnMode(context) ? theme.colorScheme.background.withOpacity(0.85) : theme.colorScheme.background : null,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: widget.isOnBoardingScreen ? PageMeThemes.isColumnMode(context) ? Colors.transparent : theme.colorScheme.background : null,
          automaticallyImplyLeading: !widget.isOnBoardingScreen,
          leading: (!widget.isOnBoardingScreen)
              ? IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: navigator.pop,
                )
              : null,
          title: Text("Enable ${l10n!.recoveryKey}"),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
          child: SizedBox(
            width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
            height: 60,
            child: ElevatedButton.icon(
              icon: const Icon(Icons.check_outlined),
              label: Text(l10n!.next),
              onPressed: null,
            ),
          ),
        ),
        body: const Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget _buildDoneScreen(
    BuildContext context,
    Text title,
    Widget body,
    List<AdaptiveFlatButton> buttons,
    double columnModeBotNavPadding,
  ) {
    return ClipRRect(
      borderRadius: PageMeThemes.isColumnMode(context) ?  BorderRadius.circular(FlavorConfig.borderRadius) : BorderRadius.zero,
      child: Scaffold(
        backgroundColor: widget.isOnBoardingScreen ? PageMeThemes.isColumnMode(context) ? theme.colorScheme.background.withOpacity(0.85) : theme.colorScheme.background : null,
        appBar: AppBar(
          centerTitle: true,
          elevation: widget.isOnBoardingScreen ? 0 : null,
          scrolledUnderElevation: widget.isOnBoardingScreen ? 0 : null,
          backgroundColor: widget.isOnBoardingScreen ? PageMeThemes.isColumnMode(context) ? Colors.transparent : theme.colorScheme.background : null,
          automaticallyImplyLeading: !widget.isOnBoardingScreen,
          leading: (!widget.isOnBoardingScreen)
              ? IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () async {
                    navigator.pop<bool>(false);
                  },
                )
              : null,
          title: title,
        ),
        body: body,
        bottomNavigationBar: Padding(
          padding:const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
          child: SizedBox(
            width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
            height: 60,
            child: ElevatedButton.icon(
              icon: const Icon(Icons.check_outlined),
              label: Text(l10n!.close),
              onPressed: () async {
                if (widget.isOnBoardingScreen) {
                  await store.setItemBool(SettingKeys.firstTimeUser + matrix.client.userID!, false);
                  context.go("/rooms");
                } else {
                  navigator.pop<bool>(false);
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildCreateNewRecoveryKey(BuildContext context, double columnModeBotNavPadding, String? key) {
    return ClipRRect(
      borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : BorderRadius.zero,
      child: Scaffold(
        backgroundColor: widget.isOnBoardingScreen ? PageMeThemes.isColumnMode(context) ? theme.colorScheme.background.withOpacity(0.85) : theme.colorScheme.background : null,
        appBar: AppBar(
          centerTitle: true,
          elevation: widget.isOnBoardingScreen ? 0 : null,
          scrolledUnderElevation: widget.isOnBoardingScreen ? 0 : null,
          backgroundColor: widget.isOnBoardingScreen ? PageMeThemes.isColumnMode(context) ? Colors.transparent : theme.colorScheme.background : null,
          automaticallyImplyLeading: !widget.isOnBoardingScreen,
          leading: (!widget.isOnBoardingScreen)
              ? IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: navigator.pop,
                )
              : null,
          title: Text(
            "Enable ${l10n!.recoveryKey}",
            style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
          child: SizedBox(
            width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
            height: 60,
            child: ElevatedButton.icon(
              icon: const Icon(Icons.check_outlined),
              label: Text(l10n!.next),
              onPressed: (_recoveryKeyCopied || _storeInSecureStorage == true)
                  ? () async {
                      if (_storeInSecureStorage == true && !PlatformInfos.isWindows) {
                        await const FlutterSecureStorage().write(
                          key: _secureStorageKey,
                          value: key,
                        );
                      }
                      setState(() => _recoveryKeyStored = true);
                    }
                  : null,
            ),
          ),
        ),
        body: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: PageMeThemes.columnWidth * 1.5),
            child: ListView(
              padding: const EdgeInsets.only(bottom: 16.0, left: 16.0, right: 16.0),
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextField(
                    minLines: 2,
                    maxLines: 4,
                    readOnly: true,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                    ),
                    controller: TextEditingController(text: key),
                  ),
                ),
                const Divider(
                  height: 16,
                  thickness: 1,
                ),
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                  trailing: Icon(
                    Icons.info_outlined,
                    color: theme.colorScheme.primary,
                  ),
                  subtitle: Text(l10n!.chatBackupDescription),
                ),
                const Divider(
                  height: 16,
                  thickness: 1,
                ),
                if (isBusiness())
                  const ListTile(
                    contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                    subtitle: Text(
                        "As a business user, this recovery key will be securely stored to your company server after you have a saved a copy for yourself and enabled it."),
                  ),
                if (isBusiness())
                  const Divider(
                    height: 16,
                    thickness: 1,
                  ),
                if (_supportsSecureStorage)
                  CheckboxListTile(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                    value: _storeInSecureStorage,
                    activeColor: theme.colorScheme.primary,
                    onChanged: (b) {
                      setState(() {
                        _storeInSecureStorage = b;
                      });
                    },
                    title: Text(_getSecureStorageLocalizedName()),
                    subtitle: Text(l10n!.storeInSecureStorageDescription),
                  ),
                const SizedBox(height: 16),
                CheckboxListTile(
                  contentPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                  value: _recoveryKeyCopied,
                  activeColor: Theme.of(context).colorScheme.primary,
                  onChanged: (b) async {
                    await Clipboard.setData(ClipboardData(text: key!)).then((_) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(
                            'Copied to your clipboard !',
                            style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
                          ),
                          behavior: SnackBarBehavior.floating,
                          backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
                          closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
                        ),
                      );
                    });
                    setState(() => _recoveryKeyCopied = true);
                  },
                  title: Text(L10n.of(context)!.copyToClipboard),
                  subtitle: Text(L10n.of(context)!.saveKeyManuallyDescription),
                ),
                const SizedBox(height: 16),
                if (PlatformInfos.isMobile || PlatformInfos.isMacOS || PlatformInfos.isWeb)
                  CheckboxListTile(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                    value: _recoveryKeyShared,
                    activeColor: Theme.of(context).colorScheme.primary,
                    onChanged: (b) async {
                      final box = context.findRenderObject() as RenderBox;
                      await Share.share(
                        key!,
                        subject: "Save your encryption key to",
                        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
                      );
                      setState(() => _recoveryKeyShared = true);
                    },
                    title: const Text("Share to a secure app"),
                    subtitle: const Text("Share your key to a secure storage app such as a locked note or password manager"),
                  ),
                const SizedBox(height: 16),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildOpenExistingSsss(BuildContext context) {
    return ClipRRect(
      borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : BorderRadius.zero,
      child: Scaffold(
        backgroundColor: widget.isOnBoardingScreen ? PageMeThemes.isColumnMode(context) ? theme.colorScheme.background.withOpacity(0.85) : theme.colorScheme.background : null,
        appBar: AppBar(
          centerTitle: true,
          elevation: widget.isOnBoardingScreen ? 0 : null,
          backgroundColor: widget.isOnBoardingScreen ? PageMeThemes.isColumnMode(context) ? Colors.transparent : theme.colorScheme.background : null,
          automaticallyImplyLeading: !widget.isOnBoardingScreen,
          leading: (!widget.isOnBoardingScreen)
              ? IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: navigator.pop,
                )
              : null,
          title: Text(
            l10n!.unlockOldMessages,
            style: TextStyle(color: theme.colorScheme.onBackground),
          ),
        ),
        body: GestureDetector(
          onTap: FocusScope.of(context).unfocus,
          child: Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: PageMeThemes.columnWidth * 1.5),
              child: ListView(
                padding: const EdgeInsets.only(bottom: 16.0, left: 16.0, right: 16.0),
                children: [
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                    subtitle: Text(l10n!.pleaseEnterRecoveryKeyDescription),
                  ),
                  const Divider(height: 8),
                  TextField(
                    minLines: 1,
                    maxLines: 4,
                    autocorrect: false,
                    readOnly: _recoveryKeyInputLoading,
                    autofillHints: _recoveryKeyInputLoading ? null : [AutofillHints.password],
                    controller: _recoveryKeyTextEditingController,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(16),
                      hintStyle: TextStyle(fontFamily: theme.textTheme.bodyLarge?.fontFamily),
                      hintText: l10n!.recoveryKey,
                      border: InputBorder.none,
                      errorText: _recoveryKeyInputError,
                    ),
                  ),
                  const Divider(height: 8),
                  const SizedBox(height: 16),
                  ElevatedButton.icon(
                    icon: _recoveryKeyInputLoading ? const CircularProgressIndicator.adaptive() : const Icon(Icons.lock_open_outlined),
                    label: Text(l10n!.unlockOldMessages, style: const TextStyle(color: Colors.white)),
                    onPressed: _recoveryKeyInputLoading
                        ? null
                        : () async {
                            setState(() {
                              _recoveryKeyInputError = null;
                              _recoveryKeyInputLoading = true;
                            });
                            try {
                              final key = _recoveryKeyTextEditingController.text.trim();
                              await bootstrap!.newSsssKey!.unlock(
                                keyOrPassphrase: key,
                              );
                              await bootstrap!.openExistingSsss();
                              Logs().d('SSSS unlocked');
                              if (bootstrap!.encryption.crossSigning.enabled) {
                                Logs().v(
                                  'Cross signing is already enabled. Try to self-sign',
                                );
                                try {
                                  await bootstrap!.client.encryption!.crossSigning.selfSign(recoveryKey: key);
                                  Logs().d('Successful selfsigned');
                                } catch (e, s) {
                                  Logs().e(
                                    'Unable to self sign with recovery key after successfully open existing SSSS',
                                    e,
                                    s,
                                  );
                                }
                              }
                            } catch (e, s) {
                              Logs().w('Unable to unlock SSSS', e, s);
                              setState(
                                () => _recoveryKeyInputError = l10n!.oopsSomethingWentWrong,
                              );
                            } finally {
                              setState(
                                () => _recoveryKeyInputLoading = false,
                              );
                            }
                          },
                  ),
                  const SizedBox(height: 16),
                  Row(children: [
                    const Expanded(child: Divider()),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Text(l10n!.or),
                    ),
                    const Expanded(child: Divider()),
                  ]),
                  const SizedBox(height: 16),
                  FutureBuilder<dynamic>(
                    future: widget.client.userDeviceKeysLoading, // Replace with the actual method that fetches userDeviceKeys
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return ElevatedButton.icon(icon: const CircularProgressIndicator(), label: const Text("Fetching your other device keys"), onPressed: () {});
                      } else if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      } else {
                        return ElevatedButton.icon(
                          icon: const Icon(Icons.cloud_download_outlined),
                          label: Text(l10n!.transferFromAnotherDevice),
                          onPressed: _recoveryKeyInputLoading
                              ? null
                              : () async {
                                  final req = await showFutureLoadingDialog(
                                    context: context,
                                    future: () => widget.client.userDeviceKeys[widget.client.userID!]!.startVerification(),
                                  );
                                  if (req.error != null) return;
                                  await KeyVerificationDialog(request: req.result!).show(context);
                                  if (widget.isOnBoardingScreen) {
                                    await store.setItemBool(SettingKeys.firstTimeUser + matrix.client.userID!, false);
                                    context.go("/rooms");
                                  } else {
                                    navigator.pop();
                                  }
                                },
                        );
                      }
                    },
                  ),
                  const SizedBox(height: 16),
                  if (isBusiness()) const Divider(height: 8),
                  if (isBusiness())
                    const ListTile(
                      contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                      subtitle: Text(
                          'If you have lost your recovery key please contact your assigned Administrator.\n\nDo not press the "Recovery key Lost" button unless it is at the request of your administrator.'),
                    ),
                  ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                    ),
                    icon: const Icon(Icons.delete_outlined),
                    label: Text(l10n!.recoveryKeyLost),
                    onPressed: _recoveryKeyInputLoading
                        ? null
                        : () async {
                            if (OkCancelResult.ok ==
                                await showOkCancelAlertDialog(
                                  useRootNavigator: false,
                                  context: context,
                                  title: l10n!.recoveryKeyLost,
                                  message: l10n!.wipeChatBackup,
                                  okLabel: l10n!.ok,
                                  cancelLabel: l10n!.cancel,
                                  isDestructiveAction: true,
                                )) {
                              await _createBootstrap(true).whenComplete(() => setState(() {}));
                            }
                          },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
