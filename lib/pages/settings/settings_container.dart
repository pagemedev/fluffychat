import 'package:flutter/material.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/string_color.dart';

class SettingsContainer extends StatelessWidget {
  const SettingsContainer({
    Key? key,
    required this.context,
    this.iconData,
    required this.title,
    required this.backgroundColor,
    required this.contentColor,
    this.onTap,
    this.trailingIcon,
    this.subTitle,
    this.enabled = true,
  }) : super(key: key);

  final BuildContext context;
  final IconData? iconData;
  final String title;
  final String? subTitle;
  final Color backgroundColor;
  final Color contentColor;
  final VoidCallback? onTap;
  final Icon? trailingIcon;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    final borderRadius = BorderRadius.circular(6);
    final splashColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiary.withOpacity(0.4) : PageMeThemes.dropScreenColor.withOpacity(0.4);
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(16, 10, 16, 0),
      child: Opacity(
        opacity: enabled ? 1 : 0.6,
        child: Material(
          color: backgroundColor, // Set the Material color
          borderRadius: borderRadius,
          elevation: enabled ? 2 : 0, // Same as your Container
          child: InkWell(
            onTap: () async {
              if (onTap == null || !enabled) return;
              onTap!();
            },
            splashColor: enabled ? splashColor : backgroundColor,
            hoverColor: enabled ? splashColor : backgroundColor,
            highlightColor: enabled ? backgroundColor.withAlpha(30).lighten() : backgroundColor,
            borderRadius: borderRadius, // Same as your Container
            child: SizedBox(
              width: double.infinity,
              height: 60,
              child: Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Icon(
                        iconData,
                        color: contentColor,
                        size: 24,
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsetsDirectional.fromSTEB(12, 0, 0, 0),
                        child: (subTitle == null)
                            ? Text(
                                title,
                                style: TextStyle(
                                  color: contentColor,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal,
                                ),
                              )
                            : Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    title,
                                    style: TextStyle(
                                      color: contentColor,
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  Text(
                                    subTitle!,
                                    style: TextStyle(
                                      color: contentColor.withOpacity(0.8),
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal,
                                    ),
                                    maxLines: 2,
                                  )
                                ],
                              ),
                      ),
                    ),
                    if (enabled)
                    Align(
                      alignment: const AlignmentDirectional(0.9, 0),
                      child: (trailingIcon != null)
                          ? trailingIcon
                          : Icon(
                              Icons.arrow_forward_ios,
                              color: contentColor,
                              size: 18,
                            ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
