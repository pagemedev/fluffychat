import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pages/settings/settings.dart';
import 'package:pageMe/pages/settings/settings_container.dart';
import 'package:pageMe/pages/settings/settings_profile_header.dart';
import 'package:pageMe/pages/settings/settings_section_title.dart';
import 'package:pageMe/utils/global_key_provider.dart';
import 'package:pageMe/widgets/tutorial_manager.dart';

import '../../config/flavor_config.dart';
import '../../utils/platform_infos.dart';
import '../../utils/url_launcher.dart';

class SettingsViewNew extends StatelessWidget {
  const SettingsViewNew({Key? key, required this.controller}) : super(key: key);
  final SettingsController controller;

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;
    return WillPopScope(
      onWillPop: () async {
        context.go('/rooms');
        return false;
      },
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
          body: SafeArea(
            top: true,
            child: NestedScrollView(
              headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) => <Widget>[
                SliverAppBar(
                  leading: BackButton(
                    onPressed: () => context.go("/rooms"),
                  ),
                  centerTitle: false,
                  title: Text(
                    L10n.of(context)!.settings,
                    style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
                  ),
                  actions: [
                    TextButton(
                      key: GlobalKeyProvider.settingsLogOutButton,
                      onPressed: controller.logoutAction,
                      child: Row(
                        children: [
                          Text(
                            L10n.of(context)!.logout,
                            style: TextStyle(
                                color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryContainer,
                                fontSize: 16,
                                fontWeight: FontWeight.w500),
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          Icon(
                            Icons.exit_to_app_outlined,
                            color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryContainer,
                            size: 20,
                          ),
                        ],
                      ),
/*                    context: context,
                      iconData: Icons.exit_to_app_outlined,
                      title: L10n.of(context)!.logout,
                      backgroundColor: backgroundColor,
                      contentColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryContainer,
                      onTap: controller.logoutAction,*/
                    )
                  ],
                  backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
                )
              ],
              body: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SettingsProfileHeaderSection(profile: controller.profile, onEditImage: controller.setAvatarAction),
                    AnimatedContainer(
                      height: controller.showChatBackupBanner ? null : 0,
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.bounceInOut,
                      child: controller.showChatBackupBanner
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SettingsSectionTitle(
                                  title: 'Important Actions',
                                ),
                                SettingsContainer(
                                  context: context,
                                  iconData: Icons.backup_outlined,
                                  title: L10n.of(context)!.enableAutoBackups,
                                  backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                                  contentColor: Theme.of(context).colorScheme.onPrimaryContainer,
                                  onTap: controller.firstRunBootstrapAction,
                                ),
                              ],
                            )
                          : Container(),
                    ),
                    const SettingsSectionTitle(
                      title: 'Preferences',
                    ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.account_box_outlined,
                      title: 'Account',
                      subTitle: 'Manage your account and profile',
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => context.go('/settings/account'),
                    ),
                    SettingsContainer(
                      key: GlobalKeyProvider.settingsAppearanceButton,
                      context: context,
                      iconData: Icons.format_paint_outlined,
                      title: 'Appearance',
                      subTitle: 'Change your theme, wallpaper and font',
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => context.go('/settings/style'),
                    ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.chat_outlined,
                      title: 'Chat',
                      subTitle: 'Change your chat preferences',
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => context.go('/settings/chat'),
                    ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.notifications_none,
                      title: 'Notifications',
                      subTitle: 'Manage which notifications you receive',
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => context.go('/settings/notifications'),
                    ),
                    const SettingsSectionTitle(
                      title: 'Management',
                    ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.devices_outlined,
                      title: 'Devices',
                      subTitle: 'Manage the devices you are signed in on',
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => context.go('/settings/devices'),
                    ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.security,
                      title: 'Security',
                      subTitle: "Manage your password and encryption",
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => context.go('/settings/security'),
                    ),
                    if (!isBusiness() && PlatformInfos.canSubscribe)
                      SettingsContainer(
                        context: context,
                        iconData: Icons.payment,
                        title: 'Subscriptions',
                        subTitle: "Manage your subscription",
                        backgroundColor: backgroundColor,
                        contentColor: contentColor,
                        onTap: () => context.go('/settings/subscription_information'),
                      ),
                    const SettingsSectionTitle(
                      title: 'Additional',
                    ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.play_circle_outline,
                      title: 'Start Tutorial',
                      subTitle: 'Follow our guided walkthrough of the app.',
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () {
                        context.go("/rooms");
                        TutorialManager(context: context).showTutorial();
                      },
                    ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.help_outline_rounded,
                      title: 'Support',
                      subTitle: 'Visit our Gitlab issues page.',
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => UrlLauncher(context, FlavorConfig.supportUrl).launchUrl(),
                    ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.privacy_tip_outlined,
                      title: 'Terms of Service',
                      subTitle: "Visit our terms of service page",
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => UrlLauncher(context, FlavorConfig.privacyUrl).launchUrl(),
                    ),
                    SettingsContainer(
                      context: context,
                      iconData: Icons.info_outlined,
                      title: 'About',
                      subTitle: "View version, logs and licensing",
                      backgroundColor: backgroundColor,
                      contentColor: contentColor,
                      onTap: () => PlatformInfos.showDialog(context),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
