
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import '../../config/themes.dart';
import '../../widgets/content_banner.dart';
import '../../widgets/matrix.dart';


class SettingsProfileHeaderSection extends StatelessWidget {
  const SettingsProfileHeaderSection({
    Key? key,
    this.profile,
    required this.onEditImage,
  }) : super(key: key);

  final VoidCallback onEditImage;
  final Profile? profile;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
          boxShadow: const [
            BoxShadow(
              blurRadius: 3,
              color: Color(0x33000000),
              offset: Offset(0, 1),
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(16, 16, 16, 16),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    width: 90,
                    height: 90,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      shape: BoxShape.rectangle,
                      border: Border.all(
                        color: Theme.of(context).colorScheme.secondary,
                        width: 2,
                      ),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(18),
                      child: ContentBanner(
                        mxContent: profile?.avatarUrl,
                        defaultIcon: Icons.person_outline_outlined,
                        height: 300,
                        enableViewer: true,
                        heroTag: 'profile-header',
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: -10,
                    right: -10,
                    child: FloatingActionButton(
                      mini: true,
                      onPressed: onEditImage,
                      backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                      foregroundColor: Theme.of(context).colorScheme.onPrimaryContainer,
                      child: const Icon(Icons.camera_alt_outlined),
                    ),
                  )
                ],
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(20, 0, 0, 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(profile?.displayName ?? Matrix.of(context).client.userID!.localpart!,
                          maxLines: 2,
                          softWrap: true,
                          style: TextStyle(
                            overflow: TextOverflow.ellipsis,
                            color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          )),
                      Padding(
                        padding: const EdgeInsetsDirectional.fromSTEB(2, 0, 0, 0),
                        child: Text(
                          '@${Matrix.of(context).client.userID!.localpart!}',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onBackground,
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsetsDirectional.fromSTEB(2, 2, 0, 0),
                        child: Text(
                          Matrix.of(context).client.userID!.domain!,
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.onBackground,
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

