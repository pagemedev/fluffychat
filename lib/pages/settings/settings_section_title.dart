import 'package:flutter/material.dart';

import '../../config/themes.dart';

class SettingsSectionTitle extends StatelessWidget {
  final String title;
  final EdgeInsetsGeometry padding;
  const SettingsSectionTitle({
    Key? key,
    required this.title,
    this.padding = const EdgeInsetsDirectional.fromSTEB(16, 16, 0, 0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Text(
        title,
        style: TextStyle(
          color: Theme.of(context).colorScheme.onBackground,
          fontSize: 16,
          fontWeight: FontWeight.normal,
        ),
      ),
    );
  }
}

class ChatDetailsSectionTitle extends StatelessWidget {
  final String title;
  final EdgeInsetsGeometry padding;
  const ChatDetailsSectionTitle({
    Key? key,
    required this.title,
    this.padding = const EdgeInsetsDirectional.fromSTEB(16, 16, 0, 0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Text(
        title,
        style: TextStyle(
          color: PageMeThemes.isDarkMode(context) ? Colors.white.withOpacity(0.9) : Theme.of(context).colorScheme.onBackground,
          fontSize: 16,
          fontWeight: FontWeight.normal,
        ),
      ),
    );
  }
}
