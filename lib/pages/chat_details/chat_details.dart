import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:go_router/go_router.dart';

import 'package:pageMe/widgets/matrix.dart';

import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:matrix/matrix.dart' as matrix;
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:image_picker/image_picker.dart';
import 'package:matrix/matrix.dart';


import '../../pageme_app.dart';
import '../settings/settings.dart';
import 'chat_details_view.dart';

enum AliasActions { copy, delete, setCanonical }

class ChatDetails extends StatefulWidget {
  final String roomId;
  const ChatDetails({Key? key, required this.roomId}) : super(key: key);

  @override
  ChatDetailsController createState() => ChatDetailsController();
}

class ChatDetailsController extends State<ChatDetails> {
  List<User>? members;
  bool displaySettings = false;

  void toggleDisplaySettings() => setState(() => displaySettings = !displaySettings);

  String? get roomId => widget.roomId;

  void setDisplaynameAction() async {
    final room = Matrix.of(context).client.getRoomById(roomId!)!;
    final input = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.changeTheNameOfTheGroup,
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      textFields: [
        DialogTextField(
          initialText: room.getLocalizedDisplayname(
            MatrixLocals(
              L10n.of(context)!,
            ),
          ),
        )
      ],
    );
    if (input == null) return;
    final success = await showFutureLoadingDialog(
      context: context,
      future: () => room.setName(input.single),
    );
    if (success.error == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
          L10n.of(context)!.displaynameHasBeenChanged,
          style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
        ),
        backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
        closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
      ));
    }
  }

  void setHistoryVisibility() async {
    final room = Matrix.of(context).client.getRoomById(roomId!)!;
    final currentHistoryVisibility = room.historyVisibility;
    final newHistoryVisibility =
    await showConfirmationDialog<HistoryVisibility>(
      context: context,
      title: L10n.of(context)!.visibilityOfTheChatHistory,
      actions: HistoryVisibility.values
          .map(
            (visibility) => AlertDialogAction(
          key: visibility,
          label: visibility
              .getLocalizedString(MatrixLocals(L10n.of(context)!)),
          isDefaultAction: visibility == currentHistoryVisibility,
        ),
      )
          .toList(),
    );
    if (newHistoryVisibility == null ||
        newHistoryVisibility == currentHistoryVisibility) return;
    await showFutureLoadingDialog(
      context: context,
      future: () => room.setHistoryVisibility(newHistoryVisibility),
    );
  }

  void setJoinRules() async {
    final room = Matrix.of(context).client.getRoomById(roomId!)!;
    final currentJoinRule = room.joinRules;
    final newJoinRule = await showConfirmationDialog<JoinRules>(
      context: context,
      title: L10n.of(context)!.whoIsAllowedToJoinThisGroup,
      actions: JoinRules.values
          .map(
            (joinRule) => AlertDialogAction(
          key: joinRule,
          label:
          joinRule.getLocalizedString(MatrixLocals(L10n.of(context)!)),
          isDefaultAction: joinRule == currentJoinRule,
        ),
      )
          .toList(),
    );
    if (newJoinRule == null || newJoinRule == currentJoinRule) return;
    await showFutureLoadingDialog(
      context: context,
      future: () async {
        await room.setJoinRules(newJoinRule);
        room.client.setRoomVisibilityOnDirectory(
          roomId!,
          visibility: {
            JoinRules.public,
            JoinRules.knock,
          }.contains(newJoinRule)
              ? matrix.Visibility.public
              : matrix.Visibility.private,
        );
      },
    );
  }


  void setGuestAccess() async {
    final room = Matrix.of(context).client.getRoomById(roomId!)!;
    final currentGuestAccess = room.guestAccess;
    final newGuestAccess = await showConfirmationDialog<GuestAccess>(
      context: context,
      title: L10n.of(context)!.areGuestsAllowedToJoin,
      actions: GuestAccess.values
          .map(
            (guestAccess) => AlertDialogAction(
          key: guestAccess,
          label: guestAccess
              .getLocalizedString(MatrixLocals(L10n.of(context)!)),
          isDefaultAction: guestAccess == currentGuestAccess,
        ),
      )
          .toList(),
    );
    if (newGuestAccess == null || newGuestAccess == currentGuestAccess) return;
    await showFutureLoadingDialog(
      context: context,
      future: () => room.setGuestAccess(newGuestAccess),
    );
  }


  void editAliases() async {
    final room = Matrix.of(context).client.getRoomById(roomId!);

    // The current endpoint doesnt seem to be implemented in Synapse. This may
    // change in the future and then we just need to switch to this api call:
    //
    // final aliases = await showFutureLoadingDialog(
    //   context: context,
    //   future: () => room.client.requestRoomAliases(room.id),
    // );
    //
    // While this is not working we use the unstable api:
    final aliases = await showFutureLoadingDialog(
      context: context,
      future: () => room!.client
          .request(
            RequestType.GET,
            '/client/unstable/org.matrix.msc2432/rooms/${Uri.encodeComponent(room.id)}/aliases',
          )
          .then((response) => response['aliases'] is List ? List<String>.from(response['aliases'] as List<dynamic>) : <String>[])
    );
    // Switch to the stable api once it is implemented.

    if (aliases.error != null) return;
    final adminMode = room!.canSendEvent('m.room.canonical_alias');
    if (aliases.result!.isEmpty && (room.canonicalAlias.isNotEmpty)) {
      aliases.result!.add(room.canonicalAlias);
    }
    if (aliases.result!.isEmpty && adminMode) {
      return setAliasAction();
    }
    final select = await showConfirmationDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.editRoomAliases,
      actions: [
        if (adminMode) AlertDialogAction(label: L10n.of(context)!.create, key: 'new'),
        ...aliases.result!.map((alias) => AlertDialogAction(key: alias, label: alias)).toList(),
      ],
    );
    if (select == null) return;
    if (select == 'new') {
      return setAliasAction();
    }
    final option = await showConfirmationDialog<AliasActions>(
      context: context,
      title: select,
      actions: [
        AlertDialogAction(
          label: L10n.of(context)!.copyToClipboard,
          key: AliasActions.copy,
          isDefaultAction: true,
        ),
        if (adminMode) ...{
          AlertDialogAction(
            label: L10n.of(context)!.setAsCanonicalAlias,
            key: AliasActions.setCanonical,
            isDestructiveAction: true,
          ),
          AlertDialogAction(
            label: L10n.of(context)!.delete,
            key: AliasActions.delete,
            isDestructiveAction: true,
          ),
        },
      ],
    );
    if (option == null) return;
    switch (option) {
      case AliasActions.copy:
        await Clipboard.setData(ClipboardData(text: select));
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(L10n.of(context)!.copiedToClipboard)),
        );
        break;
      case AliasActions.delete:
        await showFutureLoadingDialog(
          context: context,
          future: () => room.client.deleteRoomAlias(select),
        );
        break;
      case AliasActions.setCanonical:
        await showFutureLoadingDialog(
          context: context,
          future: () => room.client.setRoomStateWithKey(
            room.id,
            EventTypes.RoomCanonicalAlias,
            '',
            {
              'alias': select,
            },
          ),
        );
        break;
    }
  }

  void setAliasAction() async {
    final room = Matrix.of(context).client.getRoomById(roomId!)!;
    final domain = room.client.userID!.domain;

    final input = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.setInvitationLink,
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      textFields: [
        DialogTextField(
          prefixText: '#',
          suffixText: domain,
          hintText: L10n.of(context)!.alias,
          initialText: room.canonicalAlias.localpart,
        )
      ],
    );
    if (input == null) return;
    await showFutureLoadingDialog(
      context: context,
      future: () => room.client.setRoomAlias('#${input.single}:${domain!}', room.id),
    );
  }

  void setTopicAction() async {
    final room = Matrix.of(context).client.getRoomById(roomId!)!;
    final input = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.setGroupDescription,
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      textFields: [
        DialogTextField(
          hintText: L10n.of(context)!.setGroupDescription,
          initialText: room.topic,
          minLines: 1,
          maxLines: 4,
        )
      ],
    );
    if (input == null) return;
    final success = await showFutureLoadingDialog(
      context: context,
      future: () => room.setDescription(input.single),
    );
    if (success.error == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(L10n.of(context)!.groupDescriptionHasBeenChanged)));
    }
  }

  void setGuestAccessAction(GuestAccess guestAccess) => showFutureLoadingDialog(
        context: context,
        future: () => Matrix.of(context).client.getRoomById(roomId!)!.setGuestAccess(guestAccess),
      );

  void setHistoryVisibilityAction(HistoryVisibility historyVisibility) => showFutureLoadingDialog(
        context: context,
        future: () => Matrix.of(context).client.getRoomById(roomId!)!.setHistoryVisibility(historyVisibility),
      );

  void setJoinRulesAction(JoinRules joinRule) => showFutureLoadingDialog(
        context: context,
        future: () => Matrix.of(context).client.getRoomById(roomId!)!.setJoinRules(joinRule),
      );

  void goToEmoteSettings() async {
    final room = Matrix.of(context).client.getRoomById(roomId!)!;
    // okay, we need to test if there are any emote state events other than the default one
    // if so, we need to be directed to a selection screen for which pack we want to look at
    // otherwise, we just open the normal one.
    if ((room.states['im.ponies.room_emotes'] ?? <String, Event>{})
        .keys
        .any((String s) => s.isNotEmpty)) {
      await context.push('/rooms/${room.id}/details/multiple_emotes');
    } else {
      await context.push('/rooms/${room.id}/details/emotes');
    }
  }

  void setAvatarAction() async {
    final room = Matrix.of(context).client.getRoomById(roomId!);
    final actions = [
      if (PlatformInfos.isMobile)
        SheetAction(
          key: AvatarAction.camera,
          label: L10n.of(context)!.openCamera,
          isDefaultAction: true,
          icon: Icons.camera_alt_outlined,
        ),
      SheetAction(
        key: AvatarAction.file,
        label: L10n.of(context)!.openGallery,
        icon: Icons.photo_outlined,
      ),
      if (room?.avatar != null)
        SheetAction(
          key: AvatarAction.remove,
          label: L10n.of(context)!.delete,
          isDestructiveAction: true,
          icon: Icons.delete_outlined,
        ),
    ];
    final action = actions.length == 1
        ? actions.single.key
        : await showModalActionSheet<AvatarAction>(
            style: AdaptiveStyle.material,
            context: context,
            title: L10n.of(context)!.editRoomAvatar,
            actions: actions,
          );
    if (action == null) return;
    if (action == AvatarAction.remove) {
      await showFutureLoadingDialog(
        context: context,
        future: () => room!.setAvatar(null),
      );
      return;
    }
    MatrixFile file;

      final result = await ImagePicker().pickImage(
        source: action == AvatarAction.camera ? ImageSource.camera : ImageSource.gallery,
        imageQuality: 50,
      );
      if (result == null) return;
      file = MatrixFile(
        bytes: await result.readAsBytes(),
        name: result.path,
      );
     /*else {
      final result = await FilePicker.platform.pickFiles(type: FileType.image, allowMultiple: false);
      if (result == null || result.files.isEmpty) return;
      file = MatrixFile(
        bytes: result.files.first.bytes!,
        name: result.files.first.name,
      );
    }*/
    await showFutureLoadingDialog(
      context: context,
      future: () => room!.setAvatar(file),
    );
  }

  void requestMoreMembersAction() async {
    final room = Matrix.of(context).client.getRoomById(roomId!);
    final participants = await showFutureLoadingDialog(context: context, future: () => room!.requestParticipants());
    if (participants.error == null) {
      setState(() => members = participants.result);
    }
  }

  static const fixedWidth = 360.0;

  @override
  Widget build(BuildContext context) {
    members ??= Matrix.of(context).client.getRoomById(roomId!)!.getParticipants();
    return SizedBox(
      width: fixedWidth,
      child: ChatDetailsView(this),
    );
  }
}
