import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';

import '../../config/themes.dart';
import '../../utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import '../../utils/pageme_share.dart';
import '../../widgets/content_banner.dart';
import '../../widgets/matrix.dart';
import 'chat_details.dart';

class ChatDetailsHeaderSection extends StatelessWidget {
  const ChatDetailsHeaderSection({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final ChatDetailsController controller;

  @override
  Widget build(BuildContext context) {
    final room = Matrix.of(context).client.getRoomById(controller.roomId!);
    if (room == null) {
      return Scaffold(
        appBar: AppBar(
          title: Text(L10n.of(context)!.oopsSomethingWentWrong),
        ),
        body: Center(
          child: Text(L10n.of(context)!.youAreNoLongerParticipatingInThisChat),
        ),
      );
    }

    final isEmbedded = GoRouterState.of(context).fullPath == '/rooms/:roomid';

    return StreamBuilder(
        stream: room.onUpdate.stream,
        builder: (context, snapshot) {
          var members = room.getParticipants().toList()..sort((b, a) => a.powerLevel.compareTo(b.powerLevel));
          members = members.take(10).toList();
          final actualMembersCount = (room.summary.mInvitedMemberCount ?? 0) + (room.summary.mJoinedMemberCount ?? 0);
          final canRequestMoreMembers = members.length < actualMembersCount;
          final iconColor = Theme.of(context).colorScheme.onPrimaryContainer;
          final iconBackgroundColor = Theme.of(context).colorScheme.primaryContainer;
          final displayname = room.getLocalizedDisplayname(
            MatrixLocals(L10n.of(context)!),
          );
          return Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                boxShadow: const [
                  BoxShadow(
                    blurRadius: 3,
                    color: Color(0x33000000),
                    offset: Offset(0, 1),
                  )
                ],
              ),
              child: Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(16, 16, 16, 16),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Container(
                          width: 90,
                          height: 90,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            shape: BoxShape.rectangle,
                            border: Border.all(
                              color: Theme.of(context).colorScheme.secondary,
                              width: 2,
                            ),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(18),
                            child: ContentBanner(
                              mxContent: room.avatar,
                              defaultIcon: Icons.person_outline_outlined,
                              height: 300,
                              enableViewer: true,
                              heroTag: 'room-header',
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: -10,
                          right: -10,
                          child: FloatingActionButton(
                            mini: true,
                            onPressed: controller.setAvatarAction,
                            backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                            foregroundColor: Theme.of(context).colorScheme.onPrimaryContainer,
                            child: const Icon(Icons.camera_alt_outlined),
                          ),
                        )
                      ],
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextButton.icon(
                              onPressed: () async => room.isDirectChat
                                  ? null
                                  : room.canChangeStateEvent(
                                      EventTypes.RoomName,
                                    )
                                      ? controller.setDisplaynameAction()
                                      : PageMeShare.share(
                                          displayname,
                                          context,
                                          //copyOnly: true,
                                        ),
                              icon: Icon(
                                room.isDirectChat
                                    ? Icons.chat_bubble_outline
                                    : room.canChangeStateEvent(
                                        EventTypes.RoomName,
                                      )
                                        ? Icons.edit_outlined
                                        : Icons.copy_outlined,
                                size: 16,
                              ),
                              style: TextButton.styleFrom(
                                foregroundColor: PageMeThemes.isDarkMode(context) ? Colors.white.withOpacity(0.9) : Theme.of(context).colorScheme.onBackground,
                              ),
                              label: Text(
                                room.isDirectChat ? "Direct Chats" : displayname,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(fontSize: 16),
                              ),
                            ),
                            TextButton.icon(
                              onPressed: () async => room.isDirectChat
                                  ? null
                                  : context.push(
                                      '/rooms/${controller.roomId}/details/members',
                                    ),
                              icon: const Icon(
                                Icons.group_outlined,
                                size: 14,
                              ),
                              style: ButtonStyle(
                                foregroundColor: MaterialStateProperty.all(Theme.of(context).colorScheme.secondary),
                                overlayColor: room.isDirectChat ? MaterialStateProperty.all(Colors.transparent) : null,
                                surfaceTintColor: room.isDirectChat ? MaterialStateProperty.all(Colors.transparent) : null,
                                splashFactory: room.isDirectChat ? NoSplash.splashFactory : null,
                              ),
                              label: Text(
                                L10n.of(context)!.countParticipants(
                                  actualMembersCount,
                                ),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                //    style: const TextStyle(fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
