import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/chat_details/chat_details_header.dart';
import 'package:pageMe/pages/chat_details/participant_list_item.dart';
import 'package:pageMe/pages/settings/settings_container.dart';
import 'package:pageMe/pages/settings/settings_section_title.dart';

import '../../config/themes.dart';
import '../../utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import '../../utils/pageme_share.dart';
import '../../utils/url_launcher.dart';
import '../../widgets/avatar.dart';
import '../../widgets/chat_settings_popup_menu.dart';
import '../../widgets/layouts/max_width_body.dart';
import '../../widgets/matrix.dart';
import 'chat_details.dart';

class ChatDetailsView extends StatelessWidget {
  final ChatDetailsController controller;

  const ChatDetailsView(this.controller, {super.key});

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;
    final room = Matrix.of(context).client.getRoomById(controller.roomId!);
    if (room == null) {
      return Scaffold(
        appBar: AppBar(
          title: Text(L10n.of(context)!.oopsSomethingWentWrong),
        ),
        body: Center(
          child: Text(L10n.of(context)!.youAreNoLongerParticipatingInThisChat),
        ),
      );
    }

    final isEmbedded = GoRouterState.of(context).fullPath == '/rooms/:roomid';

    return StreamBuilder(
      stream: room.onUpdate.stream,
      builder: (context, snapshot) {
        var members = room.getParticipants().toList()..sort((b, a) => a.powerLevel.compareTo(b.powerLevel));
        members = members.take(10).toList();
        final actualMembersCount = (room.summary.mInvitedMemberCount ?? 0) + (room.summary.mJoinedMemberCount ?? 0);
        final canRequestMoreMembers = members.length < actualMembersCount;
        final iconColor = Theme.of(context).colorScheme.onPrimaryContainer;
        final iconBackgroundColor = Theme.of(context).colorScheme.primaryContainer;
        final splashColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiary.withOpacity(0.4) : PageMeThemes.dropScreenColor.withOpacity(0.4);
        final borderRadius = BorderRadius.circular(6);
        final displayname = room.getLocalizedDisplayname(
          MatrixLocals(L10n.of(context)!),
        );
        final scaffoldAppbarBackgroundColor = Theme.of(context).colorScheme.background;
        return Scaffold(
          backgroundColor: scaffoldAppbarBackgroundColor,
          appBar: isEmbedded
              ? null
              : AppBar(
                  leading: const Center(child: BackButton()),
                  elevation: 0,
                  backgroundColor: scaffoldAppbarBackgroundColor,
                  actions: <Widget>[
                    if (room.canonicalAlias.isNotEmpty)
                      IconButton(
                        tooltip: L10n.of(context)!.share,
                        icon: Icon(Icons.adaptive.share_outlined),
                        onPressed: () async => PageMeShare.share(
                          FlavorConfig.inviteLinkPrefix + room.canonicalAlias,
                          context,
                        ),
                      ),
                    ChatSettingsPopupMenu(room, false),
                  ],
                  title: Text(room.isSpace ? "Space Options" : "Chat Options"),
                ),
          body: MaxWidthBody(
            withScrolling: true,
            child: ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: members.length + 1 + (canRequestMoreMembers ? 1 : 0),
              itemBuilder: (BuildContext context, int i) {
                return i == 0
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          ChatDetailsHeaderSection(controller: controller),
                          if (!room.isDirectChat && room.canInvite)
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: FilledButton.icon(
                                style: FilledButton.styleFrom(
                                    backgroundColor: Theme.of(context).colorScheme.primaryContainer, shape: RoundedRectangleBorder(borderRadius: borderRadius)),
                                onPressed: () => context.go('/rooms/${room.id}/invite'),
                                label: Text(
                                  L10n.of(context)!.inviteContact,
                                  style: TextStyle(color: Theme.of(context).colorScheme.onPrimaryContainer, fontSize: 16),
                                ),
                                icon: Icon(
                                  Symbols.add,
                                  size: 20,
                                  weight: 600,
                                  color: Theme.of(context).colorScheme.onPrimaryContainer,
                                ),
                              ),
                            ),
                          ListTile(
                            contentPadding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
                            visualDensity: const VisualDensity(vertical: -2, horizontal: -4),
                            trailing: (room.canChangeStateEvent(EventTypes.RoomTopic))
                                ? IconButton(
                                    icon: Icon(
                                      Icons.edit_outlined,
                                      size: 18,
                                      color: contentColor,
                                    ),
                                    onPressed: controller.setTopicAction,
                                  )
                                : null,
                            //visualDensity: VisualDensity(vertical: -4),
                            title: Text(
                              "Description",
                              style: TextStyle(
                                color: PageMeThemes.isDarkMode(context) ? Colors.white.withOpacity(0.9) : Theme.of(context).colorScheme.onBackground,
                                //fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
                            child: SelectableLinkify(
                              text: room.topic.isEmpty ? "No ${room.isDirectChat ? "chat" : "space"} description created yet." : room.topic,
                              options: const LinkifyOptions(humanize: false),
                              linkStyle: const TextStyle(color: Colors.blueAccent),
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: room.topic.isEmpty ? FontStyle.italic : FontStyle.normal,
                                color: Theme.of(context).textTheme.bodyMedium!.color,
                                decorationColor: Theme.of(context).textTheme.bodyMedium!.color,
                              ),
                              onOpen: (url) async => UrlLauncher(context, url.url).launchUrl(),
                            ),
                          ),
                          const SizedBox(height: 16),
                          const ChatDetailsSectionTitle(title: "Settings"),
                          if (room.joinRules == JoinRules.public)
                            SettingsContainer(
                              context: context,
                              title: L10n.of(context)!.editRoomAliases,
                              backgroundColor: backgroundColor,
                              contentColor: contentColor,
                              iconData: Icons.link_outlined,
                              subTitle: (room.canonicalAlias.isNotEmpty) ? room.canonicalAlias : L10n.of(context)!.none,
                              trailingIcon: const Icon(Icons.chevron_right_outlined),
                              onTap: controller.editAliases,
                            ),
                          SettingsContainer(
                              context: context,
                              title: L10n.of(context)!.emoteSettings,
                              backgroundColor: backgroundColor,
                              contentColor: contentColor,
                              iconData: Icons.insert_emoticon_outlined,
                              subTitle: L10n.of(context)!.setCustomEmotes,
                              onTap: controller.goToEmoteSettings),
                          if (!room.isDirectChat)
                            SettingsContainer(
                              context: context,
                              enabled: room.canChangeJoinRules,
                              title: L10n.of(context)!.whoIsAllowedToJoinThisGroup,
                              backgroundColor: backgroundColor,
                              contentColor: contentColor,
                              iconData: Icons.shield_outlined,
                              subTitle: room.joinRules?.getLocalizedString(
                                    MatrixLocals(L10n.of(context)!),
                                  ) ??
                                  L10n.of(context)!.none,
                              onTap: controller.setJoinRules,
                              trailingIcon: const Icon(Icons.chevron_right_outlined),
                            ),
                          if (!room.isDirectChat)
                            SettingsContainer(
                              context: context,
                              enabled: room.canChangeHistoryVisibility,
                              title: L10n.of(context)!.visibilityOfTheChatHistory,
                              backgroundColor: backgroundColor,
                              contentColor: contentColor,
                              trailingIcon: const Icon(Icons.chevron_right_outlined),
                              onTap: controller.setHistoryVisibility,
                              subTitle: room.historyVisibility?.getLocalizedString(
                                    MatrixLocals(L10n.of(context)!),
                                  ) ??
                                  L10n.of(context)!.none,
                              iconData: Icons.visibility_outlined,
                            ),
                          if (room.joinRules == JoinRules.public)
                            SettingsContainer(
                              context: context,
                              enabled: room.canChangeGuestAccess,
                              title: L10n.of(context)!.areGuestsAllowedToJoin,
                              backgroundColor: backgroundColor,
                              contentColor: contentColor,
                              iconData: Icons.person_add_alt_1_outlined,
                              subTitle: room.guestAccess.getLocalizedString(
                                MatrixLocals(L10n.of(context)!),
                              ),
                              trailingIcon: const Icon(Icons.chevron_right_outlined),
                              onTap: controller.setGuestAccess,
                            ),
                          if (!room.isDirectChat)
                            SettingsContainer(
                              context: context,
                              title: "Chat permissions",
                              enabled: room.canChangePowerLevel,
                              backgroundColor: backgroundColor,
                              contentColor: contentColor,
                              trailingIcon: const Icon(Icons.chevron_right_outlined),
                              subTitle: L10n.of(context)!.whoCanPerformWhichAction,
                              iconData: Icons.edit_attributes_outlined,
                              onTap: () async => context.push('/rooms/${room.id}/details/permissions'),
                            ),
                          const SizedBox(
                            height: 8,
                          ),
                          ListTile(
                            title: Text(
                              L10n.of(context)!.countParticipants(
                                actualMembersCount.toString(),
                              ),
                              style: TextStyle(
                                color: PageMeThemes.isDarkMode(context) ? Colors.white.withOpacity(0.9) : Theme.of(context).colorScheme.onBackground,
                                //fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      )
                    : i < members.length + 1
                        ? ParticipantListItem(members[i - 1])
                        : ListTile(
                            title: Text(
                              L10n.of(context)!.loadCountMoreParticipants(
                                (actualMembersCount - members.length).toString(),
                              ),
                            ),
                            textColor: PageMeThemes.isDarkMode(context) ? Colors.white.withOpacity(0.9) : Theme.of(context).colorScheme.onBackground,
                            leading: CircleAvatar(
                              backgroundColor: iconBackgroundColor,
                              child: const Icon(
                                Icons.group_outlined,
                                color: Colors.grey,
                              ),
                            ),
                            onTap: () async => context.push(
                              '/rooms/${controller.roomId!}/details/members',
                            ),
                            trailing: const Icon(Icons.chevron_right_outlined),
                          );
              },
            ),
          ),
        );
      },
    );
  }
}
