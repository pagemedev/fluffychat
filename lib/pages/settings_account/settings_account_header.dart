import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';

import '../../config/themes.dart';
import '../../widgets/content_banner.dart';
import '../../widgets/matrix.dart';
import '../settings/settings_section_title.dart';

class SettingsAccountHeaderSection extends StatelessWidget {
  const SettingsAccountHeaderSection({
    Key? key,
    required this.profile,
    required this.onEditImage,
  }) : super(key: key);

  final VoidCallback onEditImage;
  final Profile? profile;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    width: 90,
                    height: 90,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      shape: BoxShape.rectangle,
                      border: Border.all(
                        color: Theme.of(context).colorScheme.secondary,
                        width: 2,
                      ),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(18),
                      child: ContentBanner(
                        mxContent: profile?.avatarUrl,
                        defaultIcon: Icons.person_outline_outlined,
                        height: 300,
                        enableViewer: true,
                        heroTag: 'account-header',
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: -10,
                    right: -10,
                    child: FloatingActionButton(
                      mini: true,
                      onPressed: onEditImage,
                      backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                      foregroundColor: Theme.of(context).colorScheme.onPrimaryContainer,
                      child: const Icon(Icons.camera_alt_outlined),
                    ),
                  )
                ],
              ),
            ],
          ),
          const SettingsSectionTitle(title: 'General Information'),
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
            child: Material(
              borderRadius: BorderRadius.circular(12),
              color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
              elevation: 1,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: CupertinoListSection.insetGrouped(
                  margin: EdgeInsets.zero,
                  backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                  hasLeading: true,
                  children: [
                    CupertinoListTile(
                      backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                      title: Text('Display Name',
                          style: TextStyle(
                            color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface,
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          )),
                      additionalInfo: Text(profile?.displayName ?? Matrix.of(context).client.userID!.localpart!),
                    ),
                    CupertinoListTile(
                      backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                      title: Text('User ID',
                          style: TextStyle(
                            color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface,
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          )),
                      additionalInfo: Text(Matrix.of(context).client.userID!),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
