import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:pageMe/pages/settings_account/settings_account_header.dart';

import 'package:pageMe/utils/pageme_share.dart';
import 'package:pageMe/widgets/matrix.dart';

import '../../config/themes.dart';
import '../settings/settings_container.dart';
import '../settings/settings_section_title.dart';
import 'settings_account.dart';

class SettingsAccountView extends StatelessWidget {
  final SettingsAccountController controller;
  const SettingsAccountView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;
    return WillPopScope(
      onWillPop: () async {
        context.pop();
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          elevation: 0,
          backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
          leading: const BackButton(),
          title: Text(L10n.of(context)!.account, style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
        ),
        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
        body:  Container(
            constraints: const BoxConstraints(
              maxWidth: 1000,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SettingsAccountHeaderSection(profile: controller.profile, onEditImage: controller.setAvatarAction),
                  const SettingsSectionTitle(
                    title: 'Actions',
                  ),
                  SettingsContainer(
                    context: context,
                    iconData: Icons.person_3_outlined,
                    title: 'Copy and share your user ID',
                    backgroundColor: backgroundColor,
                    contentColor: contentColor,
                    trailingIcon: const Icon(Icons.copy_outlined),
                    onTap: () => PageMeShare.share(
                      Matrix.of(context).client.userID!,
                      context,
                    ),
                  ),
                  /*ListTile(
                    trailing: const Icon(Icons.person_add_outlined),
                    title: Text(L10n.of(context)!.addAccount),
                    subtitle: Text(L10n.of(context)!.enableMultiAccounts),
                    onTap: controller.addAccountAction,
                  ),*/
                  SettingsContainer(
                    context: context,
                    iconData: Icons.edit_outlined,
                    title: L10n.of(context)!.editDisplayname,
                    backgroundColor: backgroundColor,
                    contentColor: contentColor,
                    onTap: controller.setDisplaynameAction,
                  ),
                  const SettingsSectionTitle(
                    title: 'Manage Account',
                  ),
                  SettingsContainer(
                    context: context,
                    iconData: Icons.exit_to_app_outlined,
                    title: L10n.of(context)!.logout,
                    backgroundColor: backgroundColor,
                    contentColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryContainer,
                    onTap: controller.logoutAction,
                  ),
                  SettingsContainer(
                    context: context,
                    iconData: Icons.delete_outlined,
                    title: L10n.of(context)!.deleteAccount,
                    backgroundColor: backgroundColor,
                    contentColor: Colors.red,
                    onTap: controller.deleteAccountAction,
                  ),
                  const SizedBox(
                    height: 16,
                  )
                ],
              ),
            ),
          ),

      ),
    );
  }
}
