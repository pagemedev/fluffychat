import 'dart:async';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/new_chat/new_chat.dart';
import 'package:pageMe/pages/user_directory/user_directory_view.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:pageme_client/pageme_client.dart' as pageme;

class UserDirectory extends StatefulWidget {
  final VoidCallback toggleFiltering;
  final List<pageme.HomeServer> homeServers;
  final List<HomeServerSearchResult> homeServerSearchResults;
  final List<int> selectedHomeServers;
  final String searchText;

  const UserDirectory({
    Key? key,
    required this.toggleFiltering,
    required this.homeServers,
    required this.homeServerSearchResults,
    required this.selectedHomeServers,
    required this.searchText,
  }) : super(key: key);

  @override
  UserDirectoryController createState() => UserDirectoryController();
}

class UserDirectoryController extends State<UserDirectory> {
  Future<QueryPublicRoomsResponse>? publicRoomsResponse;
  String? lastServer;
  bool isFilterMenuOpen = false;
  String? genericSearchTerm;
  bool isLoading = false;
  String? homeServerName;
  String? currentSearchTerm;
  List<Profile> foundProfiles = [];
  static const searchUserDirectoryLimit = 100;
  late MatrixState matrixState;

  bool get isFiltering => widget.selectedHomeServers.isNotEmpty;
  bool get isSearching => widget.searchText.isNotEmpty;

  List<Profile> get userDirectoryFilter {
    final List<Profile> filteredProfiles = [];
    if (isFiltering) {
      if (isSearching) {
        for (final index in widget.selectedHomeServers) {
          filteredProfiles.addAll(
            widget.homeServerSearchResults[index].homeServerUsers.where(
              (element) {
                final bool matchOnID = element.userId.contains(widget.searchText.toLowerCase());

                if (element.displayName != null) {
                  final bool matchOnDisplayName = element.displayName!.toLowerCase().contains(widget.searchText.toLowerCase());
                  return matchOnID || matchOnDisplayName;
                }
                return matchOnID;
              },
            ),
          );
        }
      } else {
        for (final index in widget.selectedHomeServers) {
          filteredProfiles.addAll(widget.homeServerSearchResults[index].homeServerUsers);
        }
      }
    } else {
      if (isSearching) {
        for (final result in widget.homeServerSearchResults) {
          filteredProfiles.addAll(
            result.homeServerUsers.where(
              (element) {
                final bool matchOnID = element.userId.toLowerCase().contains(widget.searchText.toLowerCase());

                if (element.displayName != null) {
                  final bool matchOnDisplayName = element.displayName!.toLowerCase().contains(widget.searchText.toLowerCase());
                  return matchOnID || matchOnDisplayName;
                }
                return matchOnID;
              },
            ),
          );
        }
      } else {
        for (final result in widget.homeServerSearchResults) {
          filteredProfiles.addAll(result.homeServerUsers);
        }
      }
    }
    Logs().v("Count of profile: ${filteredProfiles.length}");
    return filteredProfiles.toSet().toList();
  }

  void toggleFiltering() {
    widget.toggleFiltering();
    setState(() {
      isFilterMenuOpen = !isFilterMenuOpen;
    });
  }

  int groupComparator(String group1, String group2) {
    group1 = group1.toLowerCase();
    group2 = group2.toLowerCase();
    if (group1.toLowerCase() == homeServerName && group2 != homeServerName) {
      return -1;
    } else if (group1 != homeServerName && group2 == homeServerName) {
      return 1;
    } else {
      return group1.compareTo(group2);
    }
  }

  int itemComparator(Profile item1, Profile item2) {
    if (item1.displayName != null && item2.displayName != null) {
      return item1.displayName!.toLowerCase().compareTo(item2.displayName!.toLowerCase());
    } else if (item1.displayName != null && item2.displayName == null) {
      return item1.displayName!.toLowerCase().compareTo(item2.userId.split('@').last.toLowerCase());
    } else if (item1.displayName == null && item2.displayName != null) {
      return item1.userId.split('@').last.toLowerCase().compareTo(item2.displayName!.toLowerCase());
    } else {
      return item1.userId.split('@').last.toLowerCase().compareTo(item2.userId.split('@').last.toLowerCase());
    }
  }

  String? extractServerFromUserId(String userId) {
    // User IDs are in the format "@username:server"
    final server = userId.split(':').last; //.split('.').first.toUpperCase();
    // You can further process the server name to format it as needed
    return getServerName(server);
  }

  String? getServerName(String serverAddress) {
    if (serverAddress == "matrix.pageme.co.za" || serverAddress == "pageme.co.za") {
      return "Public";
    }
    for (final homeServer in widget.homeServers) {
      if (homeServer.baseUrl.contains(serverAddress) || serverAddress.contains(homeServer.baseUrl)) {
        return homeServer.description;
      }
    }
    return null;
  }

  @override
  Future<void> didChangeDependencies() async {
    matrixState = Matrix.of(context);
    homeServerName = extractServerFromUserId(matrixState.client.userID!)?.toLowerCase();
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => UserDirectoryView(this);
}
