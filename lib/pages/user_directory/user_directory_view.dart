import 'package:flutter/material.dart';

import 'package:grouped_list/grouped_list.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/user_directory/user_directory.dart';
import 'package:pageMe/pages/user_directory/user_directory_item.dart';
import 'package:pageMe/utils/string_color.dart';
import '../../config/flavor_config.dart';
import '../../config/themes.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/fake_chat_list_item.dart';
import 'server_title.dart';

class UserDirectoryView extends StatelessWidget {
  final UserDirectoryController controller;

  const UserDirectoryView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const dummyChatCount = 5;
    final titleColor = Theme.of(context).textTheme.bodyLarge!.color!.withAlpha(100);
    final subtitleColor = Theme.of(context).textTheme.bodyLarge!.color!.withAlpha(50);
    return Padding(
      padding: const EdgeInsets.only(top: 0.0, left: 0, right: 0, bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  "Contact Directory",
                  style: TextStyle(color: Theme.of(context).colorScheme.onSecondaryContainer, fontWeight: FontWeight.bold, fontSize: 20),
                ),
                if (controller.widget.homeServers.isNotEmpty) const Spacer(),
                if (controller.widget.homeServers.isNotEmpty)
                  TextButton.icon(
                    onPressed: () => controller.toggleFiltering(),
                    icon: Icon(
                      controller.isFilterMenuOpen ? Icons.filter_alt : Icons.filter_alt_outlined,
                      size: 18,
                    ),
                    label: const Text("Filter", style: TextStyle(fontSize: 16)),
                  ),
                const SizedBox(width: 15,)
              ],
            ),
          ),
          (controller.widget.homeServerSearchResults.isNotEmpty && controller.widget.homeServers.isNotEmpty)
              ? Padding(
                  padding: const EdgeInsets.only(
                    top: 6.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
                        child: GroupedListView<Profile, String>(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          padding: EdgeInsets.zero,
                          groupSeparatorBuilder: (String groupByValue) => ServerTitle(
                            title: groupByValue,
                            color: PageMeThemes.isDarkMode(context) ? Colors.black26  : Theme.of(context).colorScheme.secondaryContainer,
                          ),
                          keyboardDismissBehavior: PlatformInfos.isIOS ? ScrollViewKeyboardDismissBehavior.onDrag : ScrollViewKeyboardDismissBehavior.manual,
                          order: GroupedListOrder.ASC,
                          itemComparator: controller.itemComparator,
                          groupComparator: controller.groupComparator,
                          elements:  controller.userDirectoryFilter,
                          groupBy: (profile) {
                            final server = controller.extractServerFromUserId(profile.userId)!;
                            return server;
                            //return serverName == controller.homeServerName ? controller.homeServerName! : serverName ?? 'Unknown';
                          },
                          sort: true,
                          itemBuilder: (BuildContext context, profile) {
                            // final foundProfile = controller.userDirectory![i];
                            return UserDirectoryItem(
                              profile: profile,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                )
              : ListView.builder(
                  key: const Key('dummychats'),
                  shrinkWrap: true,
                  itemCount: dummyChatCount,
                  itemBuilder: (context, i) => Opacity(
                    opacity: (dummyChatCount - i) / dummyChatCount,
                    child: FakeChatListItemLoading(titleColor: titleColor, context: context, subtitleColor: subtitleColor),
                  ),
                ),
        ],
      ),
    );
  }
}
