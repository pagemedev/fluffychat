import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/client_presence_extension.dart';

import '../../config/themes.dart';
import '../../widgets/avatar.dart';
import '../../widgets/matrix.dart';
import '../../widgets/profile_bottom_sheet.dart';

class UserDirectoryItem extends StatefulWidget {
  final Profile profile;
  const UserDirectoryItem({
    Key? key,
    required this.profile,
  }) : super(key: key);

  @override
  State<UserDirectoryItem> createState() => _UserDirectoryItemState();
}

class _UserDirectoryItemState extends State<UserDirectoryItem> {
  bool _isCreating = false;
  bool _isRoomCreated = false; // New variable to explicitly track room creation

  bool get isCreated {
    final client = Matrix.of(context).client;
    return _isRoomCreated || client.contactList.where((p) => p.userid.toLowerCase().contains(widget.profile.userId)).toList().isNotEmpty;
  }

  Future<void> createRoom(String userId) async {
    setState(() {
      _isCreating = true;
    });
    final client = Matrix.of(context).client;
    try {
      final roomId = await client.startDirectChat(userId);
      if (roomId.isNotEmpty) {
        setState(() {
          _isRoomCreated = true; // Update the state to indicate the room is created
        });
      }
    } on Exception catch (e) {
      Logs().e(e.toString());
    } finally {
      setState(() {
        _isCreating = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.background : Theme.of(context).colorScheme.background,
      width: MediaQuery.sizeOf(context).width,
      child: ListTile(
        onTap: () async => showModalBottomSheet(
          context: context,
          showDragHandle: true,
          builder: (c) => ProfileBottomSheet(
            userId: widget.profile.userId,
            outerContext: context,
          ),
        ),
        leading: Avatar(
          mxContent: widget.profile.avatarUrl,
          name: widget.profile.displayName ?? widget.profile.userId,
          //size: 24,
        ),
        trailing: IconButton(
          onPressed: () async {
            if (isCreated) {
              return;
            } else {
              await createRoom(widget.profile.userId);
            }
          },
          color: Theme.of(context).colorScheme.primary,
          icon: _isCreating
              ? const CircularProgressIndicator()
              : isCreated
              ? const Icon(
            Icons.done,
            color: Colors.green,
          )
              : const Icon(Icons.person_add),
          iconSize: 25,
        ),
        title: Text(
          widget.profile.displayName ?? widget.profile.userId.localpart!,
          style: const TextStyle(),
          maxLines: 1,
        ),
        subtitle: Text(
          widget.profile.userId,
          maxLines: 1,
          style: const TextStyle(
            fontSize: 12,
          ),
        ),
      ),
    );
  }
}