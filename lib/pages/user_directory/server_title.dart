import 'package:flutter/material.dart';
import 'package:pageMe/config/flavor_config.dart';

import '../../config/themes.dart';

class ServerTitle extends StatelessWidget {
  final String title;
  final Widget? trailing;
  final void Function()? onTap;
  final Color? color;

  const ServerTitle({
    required this.title,
    this.trailing,
    this.onTap,
    this.color,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Material(
        color: color ?? Theme.of(context).colorScheme.surface,borderOnForeground: true,
        child: InkWell(
          onTap: onTap,
          splashColor: Theme.of(context).colorScheme.surface,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 8,
              ),
              child: IconTheme(
                data: Theme.of(context).iconTheme.copyWith(size: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onSecondaryContainer : Theme.of(context).colorScheme.onSecondaryContainer,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    if (trailing != null)
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: trailing!,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
}
