import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_svg/svg.dart';

import 'package:pageMe/widgets/layouts/max_width_body.dart';
import 'new_space.dart';

class NewSpaceView extends StatelessWidget {
  final NewSpaceController controller;

  const NewSpaceView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.of(context)!.createNewSpace, style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
      ),
      body: MaxWidthBody(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 25.0),
                child: Center(
                  child: GestureDetector(
                    onTap: () async => await controller.selectAvatarAction(),
                    child: Column(
                      children: [
                        Material(
                          borderRadius: BorderRadius.circular(44),
                          elevation: 4,
                          child: CircleAvatar(
                            radius: 44,
                            backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                            backgroundImage: (controller.file != null) ? MemoryImage(controller.file!.bytes) : null,
                            child: (controller.file != null)
                                ? null
                                : Icon(
                                    Icons.add_a_photo_outlined,
                                    size: 30,
                                    color: Theme.of(context).colorScheme.onPrimaryContainer,
                                  ),
                          ),
                        ),
                        (controller.file != null) ? const Text("Remove") : Container()
                      ],
                    ),
                  ),
                ),
              ),
              const ListTile(
                title: Text("Space Name",),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: TextField(
                  controller: controller.nameController,
                  autofocus: true,
                  autocorrect: false,
                  textInputAction: TextInputAction.next,
                  onSubmitted: controller.submitAction,
                  decoration: InputDecoration(
                      //labelText: "Name",
                      //prefixIcon: const Icon(Icons.people_outlined),
                      hintText: L10n.of(context)!.enterASpacepName),
                ),
              ),
              const ListTile(
                title: Text("Space Topic (Optional)"),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: TextField(
                  controller: controller.topicController,
                  autofocus: true,
                  autocorrect: false,
                  textInputAction: TextInputAction.go,
                  onSubmitted: controller.submitAction,
                  decoration: const InputDecoration(
                      //labelText: "Topic",
                      //prefixIcon: Icon(Icons.people_outlined),
                      hintText: "Enter a topic"),
                ),
              ),
              const ListTile(
                title: Text("Space Access"),
              ),
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: 2,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                        selectedTileColor: Theme.of(context).colorScheme.primaryContainer.withOpacity(0.7),
                        //tileColor: index == controller.selectedIndex ? Colors.red : Colors.yellow,
                        trailing: Checkbox(
                          activeColor: Theme.of(context).colorScheme.primary,
                          value: index == controller.selectedIndex,
                          onChanged: (_) => controller.setGroupAccess(index),
                        ),
                        title: Text(controller.listGroupAccessTitle[index]),
                        subtitle: Text(controller.listGroupAccessSubtitle[index]),
                        selected: index == controller.selectedIndex,
                        selectedColor: Theme.of(context).colorScheme.onPrimaryContainer,
                        onTap: () => controller.setGroupAccess(index));
                  }),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: controller.submitAction,
        child: const Icon(Icons.arrow_forward_outlined),
      ),
    );
  }
}
