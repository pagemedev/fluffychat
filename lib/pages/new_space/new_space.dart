import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:image_picker/image_picker.dart';
import 'package:matrix/matrix.dart' as sdk;
import 'package:matrix/matrix.dart';


import 'package:pageMe/pages/new_space/new_space_view.dart';
import 'package:pageMe/widgets/matrix.dart';

import '../../utils/logger_functions.dart';
import '../../utils/platform_infos.dart';
import '../settings/settings.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class NewSpace extends StatefulWidget {
  const NewSpace({Key? key}) : super(key: key);

  @override
  NewSpaceController createState() => NewSpaceController();
}

class NewSpaceController extends State<NewSpace> {
  TextEditingController controller = TextEditingController();
  bool publicGroup = false;
  TextEditingController nameController = TextEditingController();
  TextEditingController topicController = TextEditingController();
  int selectedIndex = 0;
  MatrixFile? file;
  List<String> listGroupAccessSubtitle = ['Invite only, best for yourself or teams', 'Open to anyone, best for communities'];
  List<String> listGroupAccessTitle = ['Private', 'Public'];
  bool displaySettings = false;

  void toggleDisplaySettings() =>
      setState(() => displaySettings = !displaySettings);

  void setPublicGroup(bool b) => setState(() => publicGroup = b);

  void setGroupAccess(int index) {
    Logs().i( "selected: $index");
    setState(() {
      selectedIndex = index;
    });
    if (selectedIndex == 0) {
      publicGroup = false;
    } else if (selectedIndex == 1) {
      publicGroup = true;
    }
  }


  void submitAction([_]) async {
    final matrix = Matrix.of(context);
    final roomID = await showFutureLoadingDialog(
      context: context,
      future: () async {
        final roomId = await matrix.client.createRoom(
        topic: topicController.text,
        preset: publicGroup
            ? sdk.CreateRoomPreset.publicChat
            : sdk.CreateRoomPreset.privateChat,
        creationContent: {'type': RoomCreationTypes.mSpace},
        visibility: publicGroup ? sdk.Visibility.public : null,
        roomAliasName: publicGroup && controller.text.isNotEmpty
            ? controller.text.trim().toLowerCase().replaceAll(' ', '_')
            : null,
        name: nameController.text.isNotEmpty ? nameController.text : null,
      );
      final room = Matrix.of(context).client.getRoomById(roomId);
      if (room == null) {
        throw Exception('Could not get room from roomId');
      }
      if (!room.isSpace){
        throw Exception('Room is not a space');
      }
      room.setAvatar(file);
      return roomId;
      }

    );
    if (roomID.error == null) {
      context.go('/rooms/${roomID.result!}/invite');
    }
  }

  Future<void> selectAvatarAction() async {
    final actions = [
      if (PlatformInfos.isMobile)
        SheetAction(
          key: AvatarAction.camera,
          label: L10n.of(context)!.openCamera,
          isDefaultAction: true,
          icon: Icons.camera_alt_outlined,
        ),
      SheetAction(
        key: AvatarAction.file,
        label: L10n.of(context)!.openGallery,
        icon: Icons.photo_outlined,
      ),
      if (file != null)
        SheetAction(
          key: AvatarAction.remove,
          label: L10n.of(context)!.delete,
          isDestructiveAction: true,
          icon: Icons.delete_outlined,
        ),
    ];
    final action = actions.length == 1
        ? actions.single
        : await showModalActionSheet<AvatarAction>(
      context: context,
      title: "Edit space avatar",
      actions: actions,
    );
    if (action == null) return;
    if (action == AvatarAction.remove) {
      setState(() {
        file = null;
      });
      return;
    }

    final result = await ImagePicker().pickImage(
      source: action == AvatarAction.camera ? ImageSource.camera : ImageSource.gallery,
      imageQuality: 50,
    );
    if (result == null) return;
    file = MatrixFile(
      bytes: await result.readAsBytes(),
      name: result.path,
    );
    /*else {
      final result = await FilePicker.platform.pickFiles(type: FileType.image, allowMultiple: false);
      if (result == null || result.files.isEmpty) return;
      file = MatrixFile(
        bytes: result.files.first.bytes!,
        name: result.files.first.name,
      );
    }*/
    setState(() {});
  }


  @override
  Widget build(BuildContext context) => NewSpaceView(this);
}
