import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../config/themes.dart';
import '../../widgets/avatar.dart';

class NewGroupTile extends StatelessWidget {
  final bool fromRooms;
  const NewGroupTile({Key? key, this.fromRooms = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: SizedBox(
        width: Avatar.defaultSize,
        height: Avatar.defaultSize,
        child: Stack(
          children: [
            Center(
              child: CircleAvatar(
                radius: 44,
                backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                child: const Icon(
                  Icons.group_add,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
      title: const Text("New group"),
      onTap: () {
        if (fromRooms) {
          context.go("/rooms/newgroup");
        }else{
          context.go("/rooms/newchat/newgroup");
        }
      },
    );
  }
}
