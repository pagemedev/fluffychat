import 'package:flutter/material.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pages/new_chat/new_chat.dart';


import '../../widgets/avatar.dart';

class NewContactTile extends StatelessWidget {
  final Function onTap;
  const NewContactTile({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: ListTile(
        leading:  SizedBox(
          width: Avatar.defaultSize,
          height: Avatar.defaultSize,
          child: Stack(
            children: [
              Center(
                child: CircleAvatar(
                  radius: 44,
                  backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                  child: const Icon(Icons.person_add, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
        title: const Text('New contact'),
        onTap: () {
          onTap();
          //context.go("/newprivatechat");
        },
      ),
    );
  }
}
