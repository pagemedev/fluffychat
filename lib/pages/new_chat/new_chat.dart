import 'dart:async';
import 'dart:math';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:device_info_plus/device_info_plus.dart';

import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';

import 'package:pageMe/utils/platform_infos.dart';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../keys/pageme_client_appkey.dart';
import '../../utils/pageme_share.dart';
import '../../utils/url_launcher.dart';
import '../../widgets/matrix.dart';
import 'qr_scanner_modal.dart';
import '../settings/settings.dart';
import 'new_chat_view.dart';
import 'package:pageme_client/pageme_client.dart' as pageme;

class NewChat extends StatefulWidget {
  const NewChat({Key? key}) : super(key: key);

  @override
  NewChatController createState() => NewChatController();
}

class NewChatController extends State<NewChat> with TickerProviderStateMixin {
  late MatrixState matrixState;
  static const searchUserDirectoryLimit = 200;
  final ScrollController scrollController = ScrollController();
  final TextEditingController searchController = TextEditingController();
  final TextEditingController inviteController = TextEditingController();
  List<pageme.HomeServer> homeServers = [];
  //List<Profile> userDirectory = [];
  List<HomeServerSearchResult> homeServerSearchResults = [];
  List<int> selectedHomeServers = [];
  Future<QueryPublicRoomsResponse>? publicRoomsResponse;
  bool scrolledToTop = true;
  Timer? _coolDown;
  String? genericSearchTerm;
  String? currentSearchTerm;
  List<Profile> foundProfiles = [];
  bool showSearchBar = false;
  final formKey = GlobalKey<FormState>();

  bool isFiltering = false;

  bool get isSearching => searchController.text.isNotEmpty;

  void toggleFiltering() {
    setState(() {
      isFiltering = !isFiltering;
      if (!isFiltering){
        clearSelectedHomeServers();
      }
    });
  }

  void clearSelectedHomeServers() {
    selectedHomeServers.clear();
  }

  void toggleSelection(int homeServerIndex) {
    setState(
      () => selectedHomeServers.contains(homeServerIndex) ? selectedHomeServers.remove(homeServerIndex) : selectedHomeServers.add(homeServerIndex),
    );
  }

  void _onScroll() {
    final newScrolledToTop = scrollController.position.pixels <= 0;
    if (newScrolledToTop != scrolledToTop) {
      setState(() {
        scrolledToTop = newScrolledToTop;
      });
    }
  }

  void setSearchBar() {
    setState(() => showSearchBar = !showSearchBar);
  }

  bool _hideFab = false;
  final FocusNode textFieldFocus = FocusNode();
  bool get hideFab => _hideFab;

  static const Set<String> supportedSigils = {'@', '!', '#'};

  static const String prefix = 'https://matrix.to/#/';

  void setHideFab() {
    if (textFieldFocus.hasFocus != _hideFab) {
      setState(() => _hideFab = textFieldFocus.hasFocus);
    }
  }

  @override
  void initState() {
    scrollController.addListener(_onScroll);
    textFieldFocus.addListener(setHideFab);
    super.initState();
  }

  void search(String query) async {
    setState(() {});
    _coolDown?.cancel();
    _coolDown = Timer(
      const Duration(milliseconds: 500),
      () => setState(() {
        genericSearchTerm = query;
        publicRoomsResponse = null;
        searchUser(context, searchController.text);
      }),
    );
  }

  Future<void> submitAction([_]) async {
    inviteController.text = inviteController.text.trim();
    if (!formKey.currentState!.validate()) return;
    UrlLauncher(context, '$prefix${inviteController.text}').openMatrixToUrl();
  }

  String? validateForm(String? value) {
    if (value!.isEmpty) {
      return L10n.of(context)!.pleaseEnterAMatrixIdentifier;
    }
    if (!inviteController.text.isValidMatrixId || !supportedSigils.contains(inviteController.text.sigil)) {
      return L10n.of(context)!.makeSureTheIdentifierIsValid;
    }
    if (inviteController.text == matrixState.client.userID) {
      return L10n.of(context)!.youCannotInviteYourself;
    }
    return null;
  }

  void inviteAction() => PageMeShare.share(
        '${FlavorConfig.deepLinkPrefix}${matrixState.client.userID}',
        context,
      );

  Future<void> displayQR(BuildContext context) async {
    await showModalBottomSheet(
        context: context,
        backgroundColor: Theme.of(context).backgroundColor,
        useRootNavigator: false,
        builder: (_context) {
          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: const Icon(Icons.close_outlined),
                onPressed: Navigator.of(_context).pop,
                tooltip: L10n.of(context)!.close,
              ),
              title: Text("${matrixState.client.userID}"),
            ),
            body: Center(
              child: Container(
                color: Colors.white,
                child: QrImageView(
                  data: 'https://matrix.to/#/${matrixState.client.userID}',
                  version: QrVersions.auto,
                  size: min(MediaQuery.sizeOf(context).width - 16, 200),
                ),
              ),
            ),
          );
        });
  }

  void inviteNewContact() async {
    await showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      if (PlatformInfos.isMobile)
                        ListTile(
                          leading: const Icon(Icons.share_outlined),
                          title: const Text('Send your contact details'),
                          onTap: () {
                            Navigator.of(context).pop();
                            inviteAction();
                          },
                        ),
                      ListTile(
                        leading: const Icon(Icons.qr_code_outlined),
                        title: const Text('Display your QR code'),
                        onTap: () {
                          Navigator.of(context).pop();
                          displayQR(context);
                        },
                      ),
                      if (PlatformInfos.isMobile)
                        ListTile(
                          leading: const Icon(Icons.camera_alt_outlined),
                          title: const Text('Scan new contact\'s QR code'),
                          onTap: () {
                            Navigator.of(context).pop();
                            openScannerAction();
                          },
                        ),
                      ListTile(
                        title: const Text("Invite manually with user ID"),
                        leading: const Icon(Icons.person_add_alt_outlined),
                        onTap: () {
                          Navigator.of(context).pop();
                          showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            useSafeArea: true,
                            builder: (context) => Padding(
                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  ListTile(
                                    title: const Text("Enter the user ID"),
                                    subtitle: Text('For example: @john:${matrixState.client.homeserver!.host.toLowerCase().substring(7)}'),
                                    isThreeLine: false,
                                  ),
                                  Form(
                                    key: formKey,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(12.0, 8, 12, 8),
                                      child: TextFormField(
                                        controller: inviteController,
                                        autocorrect: false,
                                        autofocus: true,
                                        textInputAction: TextInputAction.go,
                                        focusNode: textFieldFocus,
                                        onFieldSubmitted: (value) async {
                                          Navigator.of(context).pop();
                                          await submitAction();
                                        },
                                        validator: validateForm,
                                        decoration: InputDecoration(
                                          hintText: '@username:${matrixState.client.homeserver!.host.toLowerCase().substring(7)}',
                                          suffixIcon: IconButton(
                                            icon: const Icon(Icons.send_outlined),
                                            onPressed: () async {
                                              Navigator.of(context).pop();
                                              await submitAction();
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ));
  }

  void searchUser(BuildContext context, String text) async {
    if (text.isEmpty) {
      setState(() {
        foundProfiles = [];
      });
    }
    currentSearchTerm = text;
    if (currentSearchTerm?.isEmpty ?? true) return;
    final matrix = matrixState;
    SearchUserDirectoryResponse? response;
    try {
      response = await matrix.client.searchUserDirectory(
        text,
        limit: searchUserDirectoryLimit,
      );
    } catch (_) {}
    foundProfiles = List<Profile>.from(response?.results ?? []);
    if (foundProfiles.isEmpty && text.isValidMatrixId && text.sigil == '@') {
      foundProfiles.add(Profile.fromJson({
        'displayname': text.localpart,
        'user_id': text,
      }));
    }
    setState(() {});
  }

  void openScannerAction() async {
    if (PlatformInfos.isAndroid) {
      final info = await DeviceInfoPlugin().androidInfo;
      if ((info.version.sdkInt ?? 16) < 21) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              L10n.of(context)!.unsupportedAndroidVersionLong,
            ),
          ),
        );
        return;
      }
    }
    await Permission.camera.request();
    await showModalBottomSheet(
      context: context,
      useRootNavigator: false,
      //useSafeArea: false,
      builder: (_) => const QrScannerModal(),
    );
  }

  Future<List<pageme.HomeServer>> fetchAllHomeServers() async {
    List<pageme.HomeServer>? resultHomeservers;
    try {
      resultHomeservers = await matrixState.pagemeClient.homeserver.getAllHomeServers(
        appKey: PageMeClientAppKey.appKey,
      );
    } on Exception catch (e) {
      Logs().e("Error fetching list of homeserver from serverpod: $e");
    }
    if (resultHomeservers == null) {
      return [];
    } else {
      return resultHomeservers;
    }
  }

  Future<void> getUserDirectory() async {
    Logs().i("Get user directory");
    await fetchUsers(context);
    setState(() {});
  }

  bool isProfileEqual(Profile a, Profile b) => a.userId == b.userId;

  Future<void> fetchUsers(BuildContext context) async {
    Logs().i('Fetching starting.');
    if (homeServers.isEmpty) return;
    for (final homeServer in homeServers) {
      HomeServerSearchResult homeServerSearchResult;
      if (homeServer.baseUrl == "matrix.pageme.co.za") {
        currentSearchTerm = "pageme";
      } else {
        currentSearchTerm = homeServer.baseUrl.toLowerCase().split(".pageme.co.za").first;
      }
      if (currentSearchTerm == null) {
        continue;
      }
      SearchUserDirectoryResponse? response;
      try {
        response = await matrixState.client.searchUserDirectory(
          currentSearchTerm!,
          limit: searchUserDirectoryLimit,
        );
      } catch (e, s) {
        Logs().e("$e, $s");
        continue;
      }
      foundProfiles = List<Profile>.from(response.results);
      Logs().i("Is result limited, ${homeServer.description}: ${response.limited}");
      if (foundProfiles.isNotEmpty) {
        homeServerSearchResult = HomeServerSearchResult(homeServer: homeServer, homeServerUsers: foundProfiles);
        homeServerSearchResults.add(homeServerSearchResult);
      }

/*      for (final Profile profile in foundProfiles) {
        if (!userDirectory.any((i) => isProfileEqual(i, profile))) {
          userDirectory.add(profile);
        }
      }*/
    }
    setState(() {
      homeServerSearchResults = homeServerSearchResults.toSet().toList();
      //userDirectory = userDirectory.toSet().toList();
    });
    Logs().i('Fetching done.');
  }

  @override
  Future<void> didChangeDependencies() async {
    matrixState = Matrix.of(context);
    if (isBusiness()) {
      unawaited(fetchAllHomeServers().then((value) {
        setState(() {
          homeServers.addAll(value);
        });
      }).whenComplete(() {
        if (homeServerSearchResults.isEmpty && homeServers.isNotEmpty) {
          getUserDirectory();
        }
      }));
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    matrixState.navigatorContext = context;
    return NewChatView(this);
  }
}

enum ContactAction { share, scan, displayQR }

class HomeServerSearchResult {
  final pageme.HomeServer homeServer;
  final List<Profile> homeServerUsers;

  HomeServerSearchResult({required this.homeServer, required this.homeServerUsers});
}
