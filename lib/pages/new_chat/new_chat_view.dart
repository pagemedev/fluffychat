import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pages/user_directory/user_directory.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/widgets/profile_list.dart';
import '../../widgets/contacts_list.dart';
import '../../widgets/default_app_bar_search_field.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'new_chat.dart';
import 'new_contact_tile.dart';
import 'new_group_tile.dart';

class NewChatView extends StatelessWidget {
  final NewChatController controller;
  const NewChatView(this.controller, {Key? key}) : super(key: key);

  List<Widget> choiceChips(BuildContext context) {
    final List<Widget> chips = [];
    for (int i = 0; i < controller.homeServerSearchResults.length; i++) {
      final Widget item = Padding(
        padding: const EdgeInsets.only(left: 10, right: 5),
        child: FilterChip(
          label: Text(controller.homeServerSearchResults[i].homeServer.description!),
          labelStyle: TextStyle(color: controller.selectedHomeServers.contains(i) ? PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onPrimaryContainer : Theme.of(context).colorScheme.onPrimary : Theme.of(context).colorScheme.onBackground),
          side: BorderSide(color: Theme.of(context).colorScheme.primary),
          selected: controller.selectedHomeServers.contains(i),
          selectedColor: Theme.of(context).colorScheme.primaryContainer,
          onSelected: (_) {
            controller.toggleSelection(i);
          },
        ),
      );
      chips.add(item);
    }
    return chips;
  }

  @override
  Widget build(BuildContext context) {
    Logs().i(controller.foundProfiles.isNotEmpty ? 'In profileList' : 'In contactList');
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 112,
              floating: false,
              pinned: true,
              leading: const BackButton(),
              title: Text("Select contact", style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
              actions: [
                IconButton(
                  onPressed: () async => controller.displayQR(context),
                  icon: const Icon(Icons.qr_code_outlined),
                ),
              ],
              flexibleSpace: SafeArea(
                child: FlexibleSpaceBar(
                  background: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(
                        height: 56,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: DefaultAppBarSearchField(
                          padding: const EdgeInsets.only(left: 2),
                          autofocus: false,
                          autocorrect: true,
                          hintText: L10n.of(context)!.search,
                          searchController: controller.searchController,
                          suffix: const Icon(Icons.search_outlined),
                          onChanged: controller.search,
                          unfocusOnClear: true,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ];
        },
        body: ListView(
          padding: const EdgeInsets.only(top: 8),
          shrinkWrap: true,
          children: [
            AnimatedSwitcher(
              duration: const Duration(seconds: 1),
              child: !controller.isFiltering
                  ? !controller.isSearching
                      ? Column(
                          children: [
                            NewContactTile(onTap: () {
                              controller.inviteNewContact();
                            }),
                            const NewGroupTile(),

                          ],
                        )
                      : Container()
                  : LayoutBuilder(builder: (context, constraints) {
                      return SizedBox(
                        width: constraints.maxWidth,
                        child: Wrap(
                          alignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.start,
                          runAlignment: WrapAlignment.start,
                          direction: Axis.horizontal,
                          children: choiceChips(context),
                        ),
                      );
                    }),
            ),
            const Divider(
              endIndent: 12,
              indent: 12,
              height: 22,
            ),
            if (!isBusiness())
              const ListTile(
                title: Text('Contacts on PageMe'),
              ),
            isBusiness()
                ? UserDirectory(
                    toggleFiltering: () => controller.toggleFiltering(),
                    homeServers: controller.homeServers,
                    homeServerSearchResults: controller.homeServerSearchResults,
                    selectedHomeServers: controller.selectedHomeServers,
                    searchText: controller.searchController.text,
                  )
                : controller.foundProfiles.isNotEmpty
                    ? ProfileList(
                        controller: controller,
                        neverScrollPhysics: true,
                      )
                    : ContactsList(
                        searchController: controller.searchController,
                        neverScrollPhysics: false,
                      ),
            const SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: PlatformInfos.isMobile && !controller.hideFab
          ? FloatingActionButton(
              onPressed: controller.openScannerAction,
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Icon(
                  Icons.qr_code_scanner_outlined,
                ),
              ),
            )
          : null,
    );
  }
}
