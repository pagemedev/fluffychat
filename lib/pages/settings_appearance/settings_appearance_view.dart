import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/settings/settings_section_title.dart';
import '../../config/themes.dart';
import '../../widgets/matrix.dart';
import '../chat/events/bubble_container.dart';
import '../settings/settings_container.dart';
import 'dummy_bubble_container.dart';
import 'settings_appearance.dart';

class SettingsAppearanceView extends StatelessWidget {
  final SettingsAppearanceController controller;

  const SettingsAppearanceView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.currentTheme ??= AdaptiveTheme.of(context).mode;
    const colorPickerSize = 32.0;
    final wallpaper = Matrix.of(context).wallpaper;
    final listBackgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface;
    final backgroundColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor;
    final Color contentColor = PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.onTertiaryContainer : Theme.of(context).colorScheme.onSurface;

    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: PageMeThemes.isDarkMode(context) ? Theme.of(context).appBarTheme.backgroundColor : Theme.of(context).canvasColor,
        leading: const BackButton(),
        elevation: 0,
        title: Text('Appearance', style: TextStyle(color: Theme.of(context).colorScheme.onBackground)),
      ),
      body: Container(
        constraints: const BoxConstraints(
          maxWidth: 1000,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SettingsSectionTitle(title: 'Theme Management'),
              Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                child: Material(
                  borderRadius: BorderRadius.circular(12),
                  color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                  elevation: 1,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: CupertinoListSection.insetGrouped(
                      margin: EdgeInsets.zero,
                      backgroundColor: listBackgroundColor,
                      hasLeading: true,
                      children: [
                        CupertinoListTile(
                          backgroundColor: listBackgroundColor,
                          title: Text(L10n.of(context)!.systemTheme),
                          trailing: Radio<AdaptiveThemeMode>(
                            groupValue: controller.currentTheme,
                            value: AdaptiveThemeMode.system,
                            onChanged: controller.switchTheme,
                          ),
                        ),
                        CupertinoListTile(
                          backgroundColor: listBackgroundColor,
                          title: Text(L10n.of(context)!.lightTheme),
                          trailing: Radio<AdaptiveThemeMode>(
                            groupValue: controller.currentTheme,
                            value: AdaptiveThemeMode.light,
                            onChanged: controller.switchTheme,
                          ),
                        ),
                        CupertinoListTile(
                          backgroundColor: listBackgroundColor,
                          title: Text(L10n.of(context)!.darkTheme),
                          trailing: Radio<AdaptiveThemeMode>(
                            groupValue: controller.currentTheme,
                            value: AdaptiveThemeMode.dark,
                            onChanged: controller.switchTheme,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SettingsSectionTitle(title: L10n.of(context)!.wallpaper),
              SettingsContainer(
                context: context,
                iconData: Icons.photo_outlined,
                title: L10n.of(context)!.changeWallpaper,
                backgroundColor: listBackgroundColor,
                contentColor: contentColor,
                onTap: controller.setWallpaperAction,
              ),
              if (wallpaper != null)
                SettingsContainer(
                  context: context,
                  iconData: Icons.delete_outlined,
                  title: 'Remove wallpaper',
                  backgroundColor: listBackgroundColor,
                  contentColor: Colors.red,
                  onTap: controller.deleteWallpaperAction,
                ),
              const SettingsSectionTitle(title: 'Message format'),
              Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                child: Material(
                  borderRadius: BorderRadius.circular(12),
                  color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.tertiaryContainer : Theme.of(context).colorScheme.surface,
                  elevation: 1,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: CupertinoListSection.insetGrouped(
                      margin: EdgeInsets.zero,
                      backgroundColor: listBackgroundColor,
                      hasLeading: true,
                      children: [
                        CupertinoListTile(
                            backgroundColor: listBackgroundColor,
                            leading: Text(L10n.of(context)!.fontSize),
                            leadingSize: 60,
                            leadingToTitle: 0,
                            trailing: Text(FlavorConfig.fontSizeFactor.toString()),
                            title: Slider.adaptive(
                              min: 0.5,
                              max: 2.5,
                              label: FlavorConfig.fontSizeFactor.toString(),
                              divisions: 20,
                              //onChangeEnd: controller.changeFontSizeFactor,
                              value: FlavorConfig.fontSizeFactor,
                              semanticFormatterCallback: (d) => d.toString(),
                              onChanged: controller.changeFontSizeFactor,
                            )),
                      ],
                    ),
                  ),
                ),
              ),
              const SettingsSectionTitle(title: 'Preview message'),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: LayoutBuilder(builder: (context, constraints) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      height: wallpaper != null
                          ? (FlavorConfig.fontSizeFactor + (FlavorConfig.fontSizeFactor / 2.4)) * 150
                          : (FlavorConfig.fontSizeFactor + (FlavorConfig.fontSizeFactor / 2.4)) * 120,
                      child: Stack(
                        children: [
                          if (wallpaper != null)
                            Material(
                              elevation: 1,
                              borderRadius: BorderRadius.circular(12),
                              child: Container(
                                decoration: BoxDecoration(borderRadius: BorderRadius.circular(12), color: Theme.of(context).scaffoldBackgroundColor),
                                width: constraints.maxWidth,
                                height: (FlavorConfig.fontSizeFactor + (FlavorConfig.fontSizeFactor / 2.4)) * 150,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(12),
                                  child: Image.file(
                                    wallpaper,
                                    fit: BoxFit.cover,
                                    isAntiAlias: true,
                                  ),
                                ),
                              ),
                            ),
                          Positioned(
                            top: 20,
                            right: 0,
                            child: DummyBubbleContainer(
                              isText: true,
                              color: Theme.of(context).colorScheme.primaryContainer,
                              tail: true,
                              isSender: true,
                              longPressSelect: false,
                              selected: false,
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 8 * FlavorConfig.bubbleSizeFactor),
                                constraints: const BoxConstraints(maxWidth: PageMeThemes.columnWidth * 1.5),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor',
                                      style: TextStyle(
                                        color: Theme.of(context).colorScheme.onPrimaryContainer,
                                        fontSize: FlavorConfig.messageFontSize * FlavorConfig.fontSizeFactor,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              ),

              /*ListTile(
                  title: Text(L10n.of(context)!.bubbleSize),
                  trailing: Text('× ${FlavorConfig.bubbleSizeFactor}'),
                ),
                Slider.adaptive(
                  min: 0.5,
                  max: 1.5,
                  divisions: 4,
                  value: FlavorConfig.bubbleSizeFactor,
                  semanticFormatterCallback: (d) => d.toString(),
                  onChanged: controller.changeBubbleSizeFactor,
                ),*/
              const SizedBox(
                height: 16,
              )
            ],
          ),
        ),
      ),
    );
  }
}
