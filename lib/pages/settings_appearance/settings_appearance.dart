import 'dart:io';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/setting_keys.dart';
import 'package:pageMe/config/themes.dart';

import '../../widgets/matrix.dart';
import 'settings_appearance_view.dart';

class SettingsAppearance extends StatefulWidget {
  const SettingsAppearance({Key? key}) : super(key: key);

  @override
  SettingsAppearanceController createState() => SettingsAppearanceController();
}

class SettingsAppearanceController extends State<SettingsAppearance> {
  late MatrixState matrix;

  void setWallpaperAction() async {
    FilePickerResult? wallpaper;
    try {
      wallpaper = await FilePicker.platform.pickFiles(
        type: FileType.image,
        allowMultiple: false,
      );
    } catch (e) {
      wallpaper = null;
      Logs().e(e.toString());
      showSnackBar(e.toString());
    }
    if (wallpaper != null) {
      final path = wallpaper.files.first.path;
      if (path == null) return;

      await matrix.store.setString(SettingKeys.wallpaper, path);
      setState(() {
        matrix.wallpaper = File(path);
      });
    }
  }

  void showSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
      ),
      backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
      closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
      showCloseIcon: true,
    ));
  }

  void deleteWallpaperAction() async {
    if (await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: 'Are you sure?',
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.cancel) {
      return;
    }
    matrix.wallpaper = null;
    await matrix.store.remove(SettingKeys.wallpaper);
    setState(() {});
  }

  void setChatColor(Color color) async {
    await matrix.store.setString(
      SettingKeys.chatColor,
      color.value.toString(),
    );
    FlavorConfig.chatColor = color;
    AdaptiveTheme.of(context).setTheme(
      light: PageMeThemes.lightMaterial,
      dark: PageMeThemes.darkMaterial,
    );
  }

  AdaptiveThemeMode? currentTheme;

  static final List<Color> customColors = [
    FlavorConfig.primaryColor,
    Colors.blue.shade800,
    Colors.green.shade800,
    Colors.orange.shade900,
    Colors.pink.shade700,
    Colors.blueGrey.shade600,
  ];

  void switchTheme(AdaptiveThemeMode? newTheme) {
    if (newTheme == null) return;
    switch (newTheme) {
      case AdaptiveThemeMode.light:
        AdaptiveTheme.of(context).setLight();
        break;
      case AdaptiveThemeMode.dark:
        AdaptiveTheme.of(context).setDark();
        break;
      case AdaptiveThemeMode.system:
        AdaptiveTheme.of(context).setSystem();
        break;
    }
    setState(() => currentTheme = newTheme);
  }

  Future<void> changeFontSizeFactor(double d) async {
    try {
      setState(() => FlavorConfig.fontSizeFactor = d);
      await matrix.store.setString(
        SettingKeys.fontSizeFactor,
        FlavorConfig.fontSizeFactor.toString(),
      );
    } on Exception catch (e) {
      Logs().e("[SettingsAppearanceController] changeFontSizeFactor: $e");
    }
  }

  void changeBubbleSizeFactor(double d) {
    setState(() => FlavorConfig.bubbleSizeFactor = d);
    matrix.store.setString(
      SettingKeys.bubbleSizeFactor,
      FlavorConfig.bubbleSizeFactor.toString(),
    );
  }

  @override
  void didChangeDependencies() {
    matrix = Matrix.of(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    //Logs().i("Building SettingsAppearanceView");
    return SettingsAppearanceView(this);
  }
}
