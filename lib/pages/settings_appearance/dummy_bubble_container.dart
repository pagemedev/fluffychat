import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/string_color.dart';

import '../../../config/flavor_config.dart';
import '../../../utils/platform_infos.dart';

/// The [DummyBubbleContainer] class represents a chat bubble container widget.
///
/// It is a [StatefulWidget] that displays a chat message bubble with various
/// customizable options such as sender information, text content, timestamp,
/// color, elevation, and more. It also supports user interactions like selecting,
/// swiping, and long-pressing the bubble.
///
/// Example usage:
///
/// ```dart
/// BubbleContainer(
///   event: event,
///   isSender: isSender,
///   isText: isText,
///   child: Text('Hello, world!'),
///   timeStamp: Text('9:00 AM'),
///   color: Colors.white,
///   elevation: 2,
///   textStyle: TextStyle(
///     color: Colors.black,
///     fontSize: 16,
///   ),
///   onSelect: (event) => handleSelect(event),
///   swipeRight: (event) => handleSwipeRight(event),
///   longPressSelect: true,
///   selected: isSelected,
/// )
/// ```

class DummyBubbleContainer extends StatefulWidget {
  final Event? event;
  final bool isSender;
  final bool isText;
  final Widget child;
  final bool tail;
  final Color color;
  final double elevation;
  final TextStyle textStyle;
  final Widget? timeStamp;
  static bool useMouse = false;
  final void Function(Event)? onSelect;
  final void Function(Event)? swipeRight;
  final bool longPressSelect;
  final bool selected;

  const DummyBubbleContainer({
    Key? key,
    this.event,
    required this.child,
    this.timeStamp,
    this.isSender = true,
    this.isText = true,
    this.color = Colors.white70,
    this.tail = true,
    this.elevation = 2,
    this.textStyle = const TextStyle(
      color: Colors.black87,
      fontSize: 16,
    ),
    this.onSelect,
    this.swipeRight,
    required this.longPressSelect,
    required this.selected,
  }) : super(key: key);

  @override
  State<DummyBubbleContainer> createState() => _DummyBubbleContainerState();
}

class _DummyBubbleContainerState extends State<DummyBubbleContainer> {
  static const double contextMenuIconSize = 20;
  static const EdgeInsets listItemPadding = EdgeInsets.fromLTRB(7, 5, 17, 7);
  static const double columnWidthFactor = 1.5;
  final GlobalKey rawMaterialButtonKey = GlobalKey(debugLabel: 'rawMaterialButtonKey');

  /// This method builds a RawMaterialButton, which forms the chat bubble in the chat interface.
  /// It takes a context, alignment for the button, and a boolean flag stateTick as arguments.
  /// It returns a RawMaterialButton that is styled and positioned according to the provided arguments.
  Widget _buildRawMaterialButton(BuildContext context, Alignment alignment) {
    bool stateTick = false;
    stateTick = widget.event?.type == EventTypes.Message || widget.event?.type == EventTypes.Encrypted || widget.event?.type == EventTypes.Sticker;

    return GestureDetector(
      key: rawMaterialButtonKey,
      onLongPress: (PlatformInfos.isDesktop || PlatformInfos.isWeb)
          ? () {}
          : !widget.longPressSelect
          ? null
          : () => widget.onSelect!(widget.event!),
      onLongPressStart: (details) {
        if (PlatformInfos.isDesktop || PlatformInfos.isWeb) {
          return;
        }
      },
      child: RawMaterialButton(
        onPressed: !DummyBubbleContainer.useMouse && widget.longPressSelect
            ? () {}
            : () {
          widget.onSelect!(widget.event!);
        },
        shape: CustomBorder(
          tail: widget.tail,
          alignment: alignment,
          side: BorderSide.none,
        ),
        constraints: const BoxConstraints(minWidth: 0, minHeight: 0),
        visualDensity: const VisualDensity(horizontal: -4, vertical: -4),
        highlightColor: highlightColor(context),
        elevation: 2,
        splashColor: splashColor(context),
        fillColor: widget.color,
        child: Container(
          constraints: BoxConstraints(
              maxWidth: PlatformInfos.isMobile ? (MediaQuery.sizeOf(context).width * ( 0.85)) : PageMeThemes.columnWidth * columnWidthFactor),
          margin: widget.isSender
              ? stateTick
              ? const EdgeInsets.fromLTRB(7, 7, 16, 7)
              : listItemPadding
              : const EdgeInsets.fromLTRB(17, 7, 7, 7),
          child: Stack(
            clipBehavior: Clip.none,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (widget.event != null)
                    if (!widget.event!.room.isDirectChat && !widget.isSender)
                      if (widget.tail)
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, bottom: 4),
                          child: Text(
                            widget.event!.senderFromMemoryOrFallback.calcDisplayname(),
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              color: PageMeThemes.isDarkMode(context)
                                  ? widget.event!.senderFromMemoryOrFallback.calcDisplayname().generateTextColorDark(widget.color)
                                  : widget.event!.senderFromMemoryOrFallback.calcDisplayname().generateTextColorLight(widget.color),
                            ),
                          ),
                        ),
                  widget.child,
                ],
              ),
              if (widget.timeStamp != null)
                Positioned(
                  bottom: (widget.event!.messageType == MessageTypes.Image || widget.event!.messageType == MessageTypes.Video) ? 0 : -4,
                  right: (widget.event!.messageType == MessageTypes.Image || widget.event!.messageType == MessageTypes.Video) ? 4 : 0,
                  child: widget.timeStamp!,
                ),

              // Here you can add other children to the stack...
            ],
          ),
        ),
      ),
    );
  }

  Color highlightColor(BuildContext context) => PageMeThemes.isDarkMode(context) ? widget.color.withOpacity(0.3) : widget.color.darken(40);

  Color splashColor(BuildContext context) => PageMeThemes.isDarkMode(context) ? widget.color.darken(50) : widget.color.withOpacity(0.8);

  /// This is the main method that builds the chat bubble container widget.
  /// It takes a context as an argument.
  /// It returns a widget that represents the chat bubble in the chat interface.
  @override
  Widget build(BuildContext context) {

    Alignment alignment;

    alignment = (widget.event?.type == EventTypes.Message || widget.event?.type == EventTypes.Encrypted || widget.event?.type == EventTypes.Sticker || widget.event == null) && widget.isSender
        ? Alignment.topRight
        : Alignment.topLeft;

    return Align(
      alignment: alignment,
      child: PlatformInfos.isMobile
          ? SwipeableHorizontal(
          actionIconSize: 30,
          actionIcon: Icons.reply,
          enabled: !widget.selected,
          onSwipeEnd: () => widget.swipeRight!(widget.event!),
          threshold: math.min(80, MediaQuery.sizeOf(context).width / 8),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
            child: _buildRawMaterialButton(context, alignment),
          ))
          : Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
        child: _buildCustomContainer(context, alignment),
      ),
    );
  }

  CustomContainer _buildCustomContainer(BuildContext context, Alignment alignment) {
    bool stateTick = false;
    stateTick = widget.event?.type == EventTypes.Message || widget.event?.type == EventTypes.Encrypted || widget.event?.type == EventTypes.Sticker;
    return CustomContainer(
        alignment: alignment,
        tail: widget.tail,
        color: widget.color,
        child: Container(
          constraints:
          BoxConstraints(maxWidth: PlatformInfos.isMobile ? (MediaQuery.sizeOf(context).width * (0.85)) : PageMeThemes.columnWidth * columnWidthFactor),
          margin: widget.isSender
              ? stateTick
              ? const EdgeInsets.fromLTRB(7, 7, 16, 7)
              : listItemPadding
              : const EdgeInsets.fromLTRB(17, 7, 7, 7),
          child: Stack(
            clipBehavior: Clip.none,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (widget.event != null)
                    if (!widget.event!.room.isDirectChat && !widget.isSender)
                      if (widget.tail)
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, bottom: 4),
                          child: Text(
                            widget.event!.senderFromMemoryOrFallback.calcDisplayname(),
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              color: PageMeThemes.isDarkMode(context)
                                  ? widget.event!.senderFromMemoryOrFallback.calcDisplayname().generateTextColorDark(widget.color)
                                  : widget.event!.senderFromMemoryOrFallback.calcDisplayname().generateTextColorLight(widget.color),
                            ),
                          ),
                        ),
                  widget.child,
                ],
              ),
              if (widget.timeStamp != null)
                Positioned(
                  bottom: (widget.event!.messageType == MessageTypes.Image || widget.event!.messageType == MessageTypes.Video) ? 0 : -4,
                  right: (widget.event!.messageType == MessageTypes.Image || widget.event!.messageType == MessageTypes.Video) ? 4 : 0,
                  child: widget.timeStamp!,
                ),
              //TODO ImageFormatStamp
              /*Positioned(
                top: (widget.event!.messageType == MessageTypes.Image || widget.event!.messageType == MessageTypes.Video) ? 0 : -4,
                left: (widget.event!.messageType == MessageTypes.Image || widget.event!.messageType == MessageTypes.Video) ? 4 : 0,
                child: ImageFormatStamp(
                  event: widget.event!,
                ),)*/

              // Here you can add other children to the stack...
            ],
          ),
        ));
  }
}

class CustomBorder extends OutlinedBorder {
  final Alignment alignment;
  final bool tail;

  const CustomBorder({required this.alignment, required this.tail, BorderSide side = BorderSide.none}) : super(side: side);

  final double _radius = 10.0;
  final double _x = 10.0;

  Path customBorderPath(Rect rect) {
    if (alignment == Alignment.topRight) {
      if (tail) {
        final pathBubble = Path();
        pathBubble.addRRect(RRect.fromLTRBAndCorners(
          0,
          0,
          rect.width - _x,
          rect.height,
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius),
          topLeft: Radius.circular(_radius),
        ));
        final pathTail = Path();
        pathTail.moveTo(rect.width - _x, 0);
        pathTail.lineTo(rect.width - _x, 10);
        pathTail.lineTo(rect.width - 3, 3);
        pathTail.relativeArcToPoint(const Offset(-3, -3), radius: const Radius.circular(3), clockwise: false, rotation: 360);
        return Path.combine(PathOperation.union, pathBubble, pathTail);
      } else {
        final pathBubble = Path();
        pathBubble.addRRect(RRect.fromLTRBAndCorners(
          0,
          0,
          rect.width - _x,
          rect.height,
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius),
          topLeft: Radius.circular(_radius),
          topRight: Radius.circular(_radius),
        ));
        return pathBubble;
      }
    } else {
      if (tail) {
        final pathBubble = Path();
        pathBubble.addRRect(RRect.fromLTRBAndCorners(
          _x,
          0,
          rect.width,
          rect.height,
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius),
          topRight: Radius.circular(_radius),
        ));
        final pathTail = Path();
        pathTail.moveTo(_x, 0);
        pathTail.lineTo(_x, 10);
        pathTail.lineTo(_x - 7, 3);
        pathTail.relativeArcToPoint(const Offset(3, -3), radius: const Radius.circular(3), rotation: 360);
        pathTail.lineTo(0, 0);
        return Path.combine(PathOperation.union, pathBubble, pathTail);
      } else {
        final pathBubble = Path();
        pathBubble.addRRect(RRect.fromLTRBAndCorners(
          _x,
          0,
          rect.width,
          rect.height,
          bottomRight: Radius.circular(_radius),
          topRight: Radius.circular(_radius),
          bottomLeft: Radius.circular(_radius),
          topLeft: Radius.circular(_radius),
        ));
        return pathBubble;
      }
    }
  }

  @override
  OutlinedBorder copyWith({BorderSide? side}) {
    return CustomBorder(side: side ?? this.side, alignment: alignment, tail: tail);
  }

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.all(side.width);

  @override
  Path getInnerPath(Rect rect, {TextDirection? textDirection}) {
    return customBorderPath(rect);
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection? textDirection}) {
    return customBorderPath(rect);
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection? textDirection}) {
    if (isBusiness()) {
      return;
    } else {
      canvas.drawPath(
          customBorderPath(rect),
          Paint()
            ..style = PaintingStyle.stroke
            ..color = Colors.black
            ..strokeWidth = 0.3);
    }
  }

  @override
  ShapeBorder scale(double t) => CustomBorder(side: side.scale(t), alignment: alignment, tail: tail);
}

class CustomContainer extends StatelessWidget {
  final Alignment alignment;
  final bool tail;
  final Color color;
  final Widget child;

  CustomContainer({
    required this.alignment,
    required this.tail,
    required this.color,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: CustomContainerPainter(alignment: alignment, tail: tail, color: color),
      child: child,
    );
  }
}

class CustomContainerPainter extends CustomPainter {
  final Alignment alignment;
  final bool tail;
  final Color color;

  CustomContainerPainter({
    required this.alignment,
    required this.tail,
    required this.color,
  });

  final double _radius = 10.0;
  final double _x = 10.0;

  @override
  void paint(Canvas canvas, Size size) {
    Path path;
    if (alignment == Alignment.topRight) {
      if (tail) {
        final pathBubble = Path();
        pathBubble.addRRect(RRect.fromLTRBAndCorners(
          0,
          0,
          size.width - _x,
          size.height,
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius),
          topLeft: Radius.circular(_radius),
        ));
        final pathTail = Path();
        pathTail.moveTo(size.width - _x, 0);
        pathTail.lineTo(size.width - _x, 10);
        pathTail.lineTo(size.width - 3, 3);
        pathTail.relativeArcToPoint(const Offset(-3, -3), radius: const Radius.circular(3), clockwise: false, rotation: 360);
        path = Path.combine(PathOperation.union, pathBubble, pathTail);
      } else {
        final pathBubble = Path();
        pathBubble.addRRect(RRect.fromLTRBAndCorners(
          0,
          0,
          size.width - _x,
          size.height,
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius),
          topLeft: Radius.circular(_radius),
          topRight: Radius.circular(_radius),
        ));
        path = pathBubble;
      }
    } else {
      if (tail) {
        final pathBubble = Path();
        pathBubble.addRRect(RRect.fromLTRBAndCorners(
          _x,
          0,
          size.width,
          size.height,
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius),
          topRight: Radius.circular(_radius),
        ));
        final pathTail = Path();
        pathTail.moveTo(_x, 0);
        pathTail.lineTo(_x, 10);
        pathTail.lineTo(_x - 7, 3);
        pathTail.relativeArcToPoint(const Offset(3, -3), radius: const Radius.circular(3), rotation: 360);
        pathTail.lineTo(0, 0);
        path = Path.combine(PathOperation.union, pathBubble, pathTail);
      } else {
        final pathBubble = Path();
        pathBubble.addRRect(RRect.fromLTRBAndCorners(
          _x,
          0,
          size.width,
          size.height,
          bottomRight: Radius.circular(_radius),
          topRight: Radius.circular(_radius),
          bottomLeft: Radius.circular(_radius),
          topLeft: Radius.circular(_radius),
        ));
        path = pathBubble;
      }
    }
    // Paint the shadow first
    final shadowPath = path.shift(const Offset(2.0, 2.0)); // change the offset to change the shadow direction
    final shadowPaint = Paint()
      ..color = Colors.black.withOpacity(0.5) // Change color and opacity to your liking
      ..maskFilter = const ui.MaskFilter.blur(ui.BlurStyle.normal, 4.0); // Change blur style and sigma for different shadow effects
    canvas.drawPath(shadowPath, shadowPaint);

    // Then paint the main path over the shadow
    canvas.drawPath(path, Paint()..color = color);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

class SwipeableHorizontal extends StatefulWidget {
  final Widget child;
  final VoidCallback? onSwipeStart;
  final VoidCallback? onSwipeLeft;
  final VoidCallback? onSwipeRight;
  final VoidCallback? onSwipeCancel;
  final VoidCallback? onSwipeEnd;
  final double threshold;
  final IconData actionIcon;
  final double actionIconSize;
  final bool enabled;

  const SwipeableHorizontal({
    super.key,
    required this.child,
    this.onSwipeStart,
    this.onSwipeLeft,
    this.onSwipeRight,
    this.onSwipeCancel,
    this.onSwipeEnd,
    this.threshold = 64.0,
    this.enabled = true,
    required this.actionIcon,
    this.actionIconSize = 30,
  });

  @override
  State<StatefulWidget> createState() {
    return _SwipeableHorizontalState();
  }
}

class _SwipeableHorizontalState extends State<SwipeableHorizontal> with TickerProviderStateMixin {
  ValueNotifier<double> _dragExtent = ValueNotifier<double>(0.0);
  late AnimationController _moveController;
  late Animation<Offset> _moveAnimation;
  bool _pastLeftThreshold = false;
  bool _pastRightThreshold = false;
  bool _releasePastThreshold = false; // Added this new variable

  @override
  void initState() {
    super.initState();
    _moveController = AnimationController(duration: Duration(milliseconds: 200), vsync: this);
    _moveAnimation = Tween<Offset>(begin: Offset.zero, end: Offset(1.0, 0.0)).animate(_moveController);
    _moveController.animateTo(0.0);
  }

  @override
  void dispose() {
    _moveController.dispose();
    _dragExtent.dispose();
    super.dispose();
  }

  void _handleDragStart(DragStartDetails details) {
    if (widget.onSwipeStart != null) {
      widget.onSwipeStart!();
    }
  }

  void _handleDragUpdate(DragUpdateDetails details) {
    var delta = details.primaryDelta;
    var oldDragExtent = _dragExtent.value;
    _dragExtent.value += delta ?? 0;

    if (oldDragExtent.sign != _dragExtent.value.sign) {
      setState(() {
        _updateMoveAnimation();
      });
    }

    var movePastThresholdPixels = widget.threshold;
    var newPos = _dragExtent.value.abs() / context.size!.width;

    if (_dragExtent.value.abs() > movePastThresholdPixels) {
      _releasePastThreshold = true; // Set to true if widget is past threshold

      // how many "thresholds" past the threshold we are. 1 = the threshold 2
      // = two thresholds.
      var n = _dragExtent.value.abs() / movePastThresholdPixels;

      // Take the number of thresholds past the threshold, and reduce this
      // number
      var reducedThreshold = math.pow(n, 0.3);

      var adjustedPixelPos = movePastThresholdPixels * reducedThreshold;
      newPos = adjustedPixelPos / context.size!.width;

      if (_dragExtent.value > 0 && !_pastLeftThreshold) {
        _pastLeftThreshold = true;

        if (widget.onSwipeRight != null) {
          widget.onSwipeRight!();
        }
      }
      if (_dragExtent.value < 0 && !_pastRightThreshold) {
        _pastRightThreshold = true;

        if (widget.onSwipeLeft != null) {
          widget.onSwipeLeft!();
        }
      }
    } else {
      _releasePastThreshold = false; // Set to false if widget is before threshold

      // Send a cancel event if the user has swiped back underneath the
      // threshold
      if (_pastLeftThreshold || _pastRightThreshold) {
        if (widget.onSwipeCancel != null) {
          widget.onSwipeCancel!();
        }
      }
      _pastLeftThreshold = false;
      _pastRightThreshold = false;
    }

    _moveController.value = newPos;
  }

  void _handleDragEnd(DragEndDetails details) {
    _moveController.animateTo(0.0, duration: Duration(milliseconds: 200));
    _dragExtent.value = 0.0;

    if (widget.onSwipeEnd != null && _releasePastThreshold) {
      // Changed this line
      widget.onSwipeEnd!();
    }

    _releasePastThreshold = false; // Reset to false after each swipe
  }

  void _updateMoveAnimation() {
    var end = _dragExtent.value.sign;
    _moveAnimation = Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(end, 0.0)).animate(_moveController);
  }

  double checkScale(double scale) {
    if (scale < 0) {
      return 0.0;
    } else if (scale >= 0 && scale <= 1) {
      return scale;
    } else {
      return 1.0;
    }
  }

  Widget _buildSwipeableBackground(BuildContext context, AnimationController moveController, ValueNotifier<double> dragExtent, double threshold) {
    return AnimatedBuilder(
      animation: Listenable.merge([moveController, dragExtent]),
      builder: (context, child) {
        double scale = checkScale(dragExtent.value / threshold);
        Color color = dragExtent.value.abs() > widget.threshold
            ? PageMeThemes.isDarkMode(context)
            ? Theme.of(context).colorScheme.primary
            : Theme.of(context).colorScheme.tertiary
            : Theme.of(context).colorScheme.onBackground;
        return Transform.scale(
          scale: scale,
          child: ColorFiltered(
            colorFilter: ColorFilter.mode(color, BlendMode.modulate),
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
              ),
              child: Flex(
                direction: Axis.horizontal,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    widget.actionIcon,
                    size: widget.actionIconSize,
                    color: color,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.enabled) {
      return widget.child;
    }
    var children = <Widget>[
      Positioned(
        top: 0,
        bottom: 0,
        left: 0,
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return _buildSwipeableBackground(context, _moveController, _dragExtent, widget.threshold);
          },
        ),
      ),
      SlideTransition(
        position: _moveAnimation,
        child: widget.child,
      ),
    ];

    return GestureDetector(
      onHorizontalDragStart: _handleDragStart,
      onHorizontalDragUpdate: _handleDragUpdate,
      onHorizontalDragEnd: _handleDragEnd,
      behavior: HitTestBehavior.opaque,
      child: Stack(
        children: children,
      ),
    );
  }
}
