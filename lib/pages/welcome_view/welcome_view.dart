import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/utils/localized_exception_extension.dart';

import '../../config/themes.dart';
import '../../widgets/matrix.dart';

class WelcomeView extends StatefulWidget {
  final String title;
  final String body;
  final bool isTopSafeArea;
  final bool isBottomSafeArea;

  const WelcomeView({
    Key? key,
    required this.isTopSafeArea,
    required this.isBottomSafeArea,
    required this.title,
    required this.body,
  }) : super(key: key);

  @override
  WelcomeViewState createState() => WelcomeViewState();
}

class WelcomeViewState extends State<WelcomeView> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  static const double assetWidth = 350;
  static const double globalHeaderHeight = 35;
  static const double globalHeaderPadding = 16;
  static const bodyStyle = TextStyle(fontSize: 19.0);
  static const titleStyle = TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700);
  static const bodyPadding = EdgeInsets.fromLTRB(32.0, 0.0, 32.0, 16.0);
  String? error;
  bool? isLoading = false;

  Future<void> checkPublicServerAction() async {
    setState(() {
      isLoading = true;
    });
    try {
      final String homeServerAddress = FlavorConfig.publicServers[0].tryGet('address')!;
      var homeserver = Uri.parse(homeServerAddress);
      if (homeserver.scheme.isEmpty) {
        homeserver = Uri.https(homeServerAddress, '');
      }
      final matrix = Matrix.of(context);
      matrix.loginHomeServerSummary = await matrix.getLoginClient().checkHomeserver(homeserver);
      final ssoSupported = matrix.loginHomeServerSummary!.loginFlows.any((flow) => flow.type == 'm.login.sso');

      try {
        await Matrix.of(context).getLoginClient().register();
        matrix.loginRegistrationSupported = true;
      } on MatrixException catch (e) {
        matrix.loginRegistrationSupported = e.requireAdditionalAuthentication;
      }

      if (!ssoSupported && matrix.loginRegistrationSupported == false) {
        // Server does not support SSO or registration. We can skip to login page:
      }
      nextPage();
    } catch (e) {
      setState(() => error = (e).toLocalizedString(context));
      return;
    } finally {
      if (mounted) {
        setState(() => isLoading = false);
      }
    }
  }

  void nextPage() {
    setState(() {
      if (isBusiness()) {
        context.go('/server_select');
      } else {
        context.go('/login');
      }
    });
  }

  Widget buildSVG(String assetName, [double width = assetWidth]) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: globalHeaderHeight + (4 * globalHeaderPadding)),
        child: SvgPicture.asset('assets/$assetName', width: width),
      ),
    );
  }

  Widget _buildFlex(context) {
    return Stack(
      children: [
        if (PageMeThemes.isColumnMode(context))
          Image.asset(
            Theme.of(context).brightness == Brightness.light ? 'assets/light_background_alt.png' : 'assets/dark_background_alt.png',
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
        Padding(
          padding: PageMeThemes.isColumnMode(context) ? const EdgeInsets.all(8.0) : const EdgeInsets.all(0),
          child: Center(
            child: Material(
              elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
              borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
              child: Container(
                width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.5 : double.infinity,
                decoration: BoxDecoration(
                  borderRadius: PageMeThemes.isColumnMode(context) ? BorderRadius.circular(FlavorConfig.borderRadius) : null,
                  color: Theme.of(context).colorScheme.background,
                ),
                child: Column(
                  //direction: Axis.vertical,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    if (PageMeThemes.isColumnMode(context))
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: SizedBox(
                          width: PageMeThemes.columnWidth * 1.5 / 3,
                          //height: 150,
                          child: SvgPicture.asset(
                            (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.center,
                        child: buildSVG("welcome.svg"),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 60,
                          ),
                          Text(widget.title, style: titleStyle, textAlign: TextAlign.center),
                          const SizedBox(
                            height: 30,
                          ),
                          Container(
                            width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 1.2 : null,
                            padding: bodyPadding,
                            child: Text(
                              widget.body,
                              style: bodyStyle,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0, right: 12, bottom: 8),
                      child: SizedBox(
                        width: PageMeThemes.isColumnMode(context) ? PageMeThemes.columnWidth * 2 : double.infinity,
                        height: error != null ? 80 : 60,
                        child: Column(
                          children: [
                            error != null
                                ? Text(
                                    error!,
                                  )
                                : const SizedBox(),
                            isLoading!
                                ? const LinearProgressIndicator(
                                    color: Colors.white,
                                  )
                                : SizedBox(
                                    height: 60,
                                    child: ElevatedButton(
                                        style: ButtonStyle(
                                          shadowColor: MaterialStateProperty.resolveWith(
                                            (states) {
                                              return Colors.black;
                                            },
                                          ),
                                          elevation: MaterialStateProperty.resolveWith((states) {
                                            return 2;
                                          }),
                                          backgroundColor: MaterialStateProperty.resolveWith((states) {
                                            return Theme.of(context).colorScheme.primaryContainer;
                                          }),
                                        ),
                                        onPressed: () {
                                          nextPage();
                                        },
                                        child: Text(
                                          "Let's go",
                                          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Theme.of(context).colorScheme.onPrimaryContainer),
                                        )),
                                  ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    FlutterNativeSplash.remove();
    Matrix.of(context).navigatorContext = context;
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: !PageMeThemes.isColumnMode(context)
          ? AppBar(
              backgroundColor: Theme.of(context).colorScheme.background,
              elevation: PageMeThemes.isColumnMode(context) ? 3 : 0,
              toolbarHeight: 100,
              centerTitle: true,
              title: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 40),
                    child: SizedBox(
                      //height: globalHeaderHeight,
                      width: MediaQuery.sizeOf(context).width / 3,
                      child: SvgPicture.asset(
                        (PageMeThemes.isDarkMode(context)) ? 'assets/pageme_app_name_white.svg' : 'assets/pageme_app_name_black.svg',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
              bottom: PageMeThemes.isColumnMode(context)
                  ? PreferredSize(
                      preferredSize: const Size.fromHeight(2.0),
                      child: Container(
                          height: PageMeThemes.isDarkMode(context) ? 2 : 1.25,
                          color: PageMeThemes.isDarkMode(context) ? Theme.of(context).colorScheme.primaryContainer : Colors.black //Theme.of(context).colorScheme.primary,
                          ),
                    )
                  : null,
            )
          : PreferredSize(preferredSize: Size.zero, child: Container()),
      body: _buildFlex(context),
    );
  }
}
