import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/client_manager.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/push_factorys/push_ultity.dart';
import 'package:pageMe/utils/sentry_controller.dart';
import 'package:pageMe/utils/window_manager.dart';
import 'package:pageMe/widgets/error_widget.dart';
import 'package:pageMe/widgets/lock_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:universal_html/html.dart' as html;

import 'config/flavor_config.dart';
import 'config/setting_keys.dart';
import 'firebase_options.dart';
import 'pageme_app.dart';

void main() async {
  await runZonedGuarded(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      FlavorConfig.initialize(flavor: Flavor.business);

      FlutterError.onError = (FlutterErrorDetails details) => Zone.current.handleUncaughtError(
            details.exception,
            details.stack ?? StackTrace.current,
          );

      final store = await SharedPreferences.getInstance();
      final clients = await ClientManager.getClients(store: store);
      Logs().level = kReleaseMode ? Level.verbose : Level.verbose;

/*      if (PlatformInfos.isMobile || PlatformInfos.isMacOS) {
        Logs().i(clients.first.clientName);
        PushSetup().pushSetupClientOnly(clients.first);
      }*/

/*      final queryParameters = <String, String>{};
      if (kIsWeb) {
        queryParameters.addAll(Uri.parse(html.window.location.href).queryParameters);
      }*/


      if (PlatformInfos.isAndroid && AppLifecycleState.detached == WidgetsBinding.instance.lifecycleState) {
        // Do not send online presences when app is in background fetch mode.
        for (final client in clients) {
          client.syncPresence = PresenceType.offline;
        }

        // In the background fetch mode we do not want to waste resources with
        // starting the Flutter engine but process incoming push notifications.
        PushSetup().pushSetupClientOnly(clients.first);
        // To start the flutter engine afterwards we add an custom observer.
        WidgetsBinding.instance.addObserver(AppStarter(clients, store));
        Logs().i(
          '${FlavorConfig.applicationName} started in background-fetch mode. No GUI will be created unless the app is no longer detached.',
        );
        return;
      }

      // Started in foreground mode.
      Logs().i(
        '${FlavorConfig.applicationName} started in foreground mode. Rendering GUI...',
      );
      await startGui(clients, store);
    },
    SentryController.captureException,
  );
}

/// Fetch the pincode for the applock and start the flutter engine.
Future<void> startGui(List<Client> clients, SharedPreferences store) async {
  // Fetch the pin for the applock if existing for mobile applications.
  String? pin;
  if (PlatformInfos.isMobile) {
    try {
      pin = await const FlutterSecureStorage().read(key: SettingKeys.appLockKey);
    } catch (e, s) {
      Logs().d('Unable to read PIN from Secure storage', e, s);
    }
  }

  if (PlatformInfos.isDesktop) {
    await WindowManagerWidget.initWindow();
  }

  // Preload first client
  final firstClient = clients.firstOrNull;
  await firstClient?.roomsLoading;
  await firstClient?.accountDataLoading;

  ErrorWidget.builder = (details) => PageMeErrorWidget(details);
  runApp(PageMeApp(clients: clients, pincode: pin, store: store));
}

/// Watches the lifecycle changes to start the application when it
/// is no longer detached.
class AppStarter with WidgetsBindingObserver {
  final List<Client> clients;
  final SharedPreferences store;
  bool guiStarted = false;

  AppStarter(this.clients, this.store);

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    if (guiStarted) return;
    if (state == AppLifecycleState.detached) return;

    Logs().i(
      '${FlavorConfig.applicationName} switches from the detached background-fetch mode to ${state.name} mode. Rendering GUI...',
    );
    // Switching to foreground mode needs to reenable send online sync presence.
    for (final client in clients) {
      client.syncPresence = PresenceType.online;
    }
    await startGui(clients, store);
    // We must make sure that the GUI is only started once.
    guiStarted = true;
  }
}
