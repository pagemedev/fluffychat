import 'dart:ui';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/cubits/search_handler/search_handler_cubit.dart';
import 'package:pageMe/pages/subscriptions/subscriptions_cubit.dart';
import 'package:pageMe/utils/conditonal_wrapper.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/window_manager.dart';
import 'package:pageMe/widgets/app_lock.dart';
import 'package:provider/provider.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'config/go_routes.dart';
import 'config/themes.dart';
import 'cubits/file_handler/file_handler_bloc.dart';
import 'cubits/sending_handler/sending_handler_bloc.dart';
import 'utils/custom_scroll_behaviour.dart';
import 'utils/localized_exception_extension.dart';
import 'widgets/matrix.dart';

class PageMeApp extends StatefulWidget {
  final Widget? testWidget;
  final List<Client> clients;
  final String? pincode;
  final Map<String, String>? queryParameters;
  final SharedPreferences store;
  //static GlobalKey<VRouterState> routerKey = GlobalKey<VRouterState>();

  const PageMeApp({
    Key? key,
    this.testWidget,
    required this.clients,
    this.queryParameters,
    required this.store,
    this.pincode,
  }) : super(key: key);

  /// getInitialLink may rereturn the value multiple times if this view is
  /// opened multiple times for example if the user logs out after they logged
  /// in with qr code or magic link.
  static bool gotInitialLink = false;

  static final GoRouter router = GoRouter(routes: AppRoutes.routes);

  @override
  PageMeAppState createState() => PageMeAppState();
}

class PageMeAppState extends State<PageMeApp> {
  bool? columnMode;
  String? _initialUrl;

  final _appLifecycleListener = AppLifecycleListener(
    onExitRequested: () async {
      return AppExitResponse.exit;
    },
  );

  @override
  void dispose() {
    _appLifecycleListener.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() async {
    if (isBusiness()) {
      _initialUrl = widget.clients.any((client) => client.isLogged()) ? '/rooms' : '/home';
    } else {
      bool? isSubscribed;
      final SubscriptionsCubit subscriptionCubit = BlocProvider.of<SubscriptionsCubit>(context);
      if (widget.clients.any((client) => client.isLogged())) {
        if (!await Purchases.isConfigured) return;
        await subscriptionCubit.login(appUserId: widget.clients.first.userID!).whenComplete(() => isSubscribed = BlocProvider.of<SubscriptionsCubit>(context).state.isSubscribed);
      } else {
        isSubscribed = BlocProvider.of<SubscriptionsCubit>(context).state.isSubscribed;
      }
      Logs().i('isSubscribed: $isSubscribed');
      _initialUrl = widget.clients.any((client) => client.isLogged())
          ? isSubscribed!
              ? '/rooms'
              : 'subscriptions'
          : '/home';
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
      light: isBusiness() ? PageMeThemes.lightMaterial : PageMeThemes.lightMaterialPublic,
      dark: isBusiness() ? PageMeThemes.darkMaterial : PageMeThemes.darkMaterialPublic,
      initial: AdaptiveThemeMode.system,
      builder: (theme, darkTheme) => LayoutBuilder(
        builder: (context, constraints) {
          /*  Logs().v('Device max width = ${constraints.maxWidth}');
          const maxColumns = 3;
          var newColumns = (constraints.maxWidth / (PlatformInfos.isIOS ? PageMeThemes.columnWidth * 1.5 : PageMeThemes.columnWidth)).floor();
          if (newColumns > maxColumns) newColumns = maxColumns;
          columnMode ??= newColumns > 1;
          if (columnMode != newColumns > 1) {
            Logs().v('Set Column Mode = $columnMode');
            WidgetsBinding.instance.addPostFrameCallback((_) {
              setState(() {
                //_initialUrl = PageMeApp.routerKey.currentState?.url;
                columnMode = newColumns > 1;
                //PageMeApp.routerKey = GlobalKey<VRouterState>();
              });
            });
          }*/
          return MaterialApp.router(
            title: FlavorConfig.applicationName,
            themeMode: ThemeMode.system,
            theme: theme,
            darkTheme: darkTheme,
            scrollBehavior: CustomScrollBehavior(),
            localizationsDelegates: L10n.localizationsDelegates,
            supportedLocales: L10n.supportedLocales,
            routerConfig: PageMeApp.router,
            builder: (context, child) => AppLockWidget(
              pincode: widget.pincode,
              clients: widget.clients,
              child: Navigator(
                //key: GlobalKeyProvider.navigatorKey,
                onGenerateRoute: (_) => MaterialPageRoute(
                  builder: (_) {
                    LoadingDialog.defaultTitle = L10n.of(context)!.loadingPleaseWait;
                    LoadingDialog.defaultBackLabel = L10n.of(context)!.close;
                    LoadingDialog.defaultOnError = (e) => (e as Object?)!.toLocalizedString(context);
                    WidgetsBinding.instance.addPostFrameCallback((_) async {
                      await SystemChrome.setPreferredOrientations([
                        DeviceOrientation.portraitUp,
                        if (PlatformInfos.isDesktop || PlatformInfos.isWeb) DeviceOrientation.portraitDown,
                        if (PlatformInfos.isDesktop || PlatformInfos.isWeb) DeviceOrientation.landscapeLeft,
                        if (PlatformInfos.isDesktop || PlatformInfos.isWeb) DeviceOrientation.landscapeRight,
                      ]);
                      SystemChrome.setSystemUIOverlayStyle(
                        SystemUiOverlayStyle(
                          statusBarColor: Colors.transparent,
                          systemNavigationBarDividerColor: Theme.of(context).scaffoldBackgroundColor,
                          systemNavigationBarColor: Theme.of(context).scaffoldBackgroundColor,
                          systemNavigationBarIconBrightness: Theme.of(context).brightness == Brightness.light ? Brightness.dark : Brightness.light,
                        ),
                      );
                    });
                    return ConditionalWrapper(
                      condition: PlatformInfos.isDesktop,
                      wrapper: (context, child) {
                        return WindowManagerWidget(child: child);
                      },
                      builder: (context) {
                        return ChangeNotifierProvider(
                          create: (context) => PageMeThemes(),
                          child: Matrix(
                            clients: widget.clients,
                            store: widget.store,
                            child: MultiBlocProvider(providers: [
                              BlocProvider(create: (context) => FileHandlerBloc(context)),
                              BlocProvider(create: (context) => SearchHandlerCubit(Matrix.of(context).client)),
                              BlocProvider(create: (context) => SendingHandlerBloc(client: Matrix.of(context).client, context: context)),
                            ], child: child!),
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
            ),
          );

          /*VRouter(
            key: PageMeApp.routerKey,
            title: FlavorConfig.applicationName,
            theme: theme,
            debugShowCheckedModeBanner: false,
            scrollBehavior: CustomScrollBehavior(),
            logs: kReleaseMode ? VLogs.none : VLogs.info,
            darkTheme: darkTheme,
            localizationsDelegates: const [
              ...L10n.localizationsDelegates,
            ],
            supportedLocales: L10n.supportedLocales,
            initialUrl: _initialUrl ?? '/',
            routes: AppRoutes(columnMode ?? false).routes,
            builder: (context, child) {
              LoadingDialog.defaultTitle = L10n.of(context)!.loadingPleaseWait;
              LoadingDialog.defaultBackLabel = L10n.of(context)!.close;
              LoadingDialog.defaultOnError = (e) => (e as Object?)!.toLocalizedString(context);
              WidgetsBinding.instance.addPostFrameCallback((_) {
                SystemChrome.setPreferredOrientations([
                  DeviceOrientation.portraitUp,
                  if (PlatformInfos.isDesktop || PlatformInfos.isWeb) DeviceOrientation.portraitDown,
                  if (PlatformInfos.isDesktop || PlatformInfos.isWeb) DeviceOrientation.landscapeLeft,
                  if (PlatformInfos.isDesktop || PlatformInfos.isWeb) DeviceOrientation.landscapeRight,
                ]);
                SystemChrome.setSystemUIOverlayStyle(
                  SystemUiOverlayStyle(
                    statusBarColor: Colors.transparent,
                    systemNavigationBarDividerColor: Theme.of(context).scaffoldBackgroundColor,
                    systemNavigationBarColor: Theme.of(context).scaffoldBackgroundColor,
                    systemNavigationBarIconBrightness: Theme.of(context).brightness == Brightness.light ? Brightness.dark : Brightness.light,
                  ),
                );
              });
              return ConditionalWrapper(
                condition: PlatformInfos.isDesktop,
                wrapper: (context, child) {
                  return WindowManagerWidget(child: child);
                },
                builder: (context) {
                  return Matrix(
                    context: context,
                    router: PageMeApp.routerKey,
                    clients: widget.clients,
                    child: MultiBlocProvider(providers: [
                      BlocProvider(create: (context) => FileHandlerBloc(context)),
                      BlocProvider(create: (context) => SendingHandlerBloc(client: Matrix.of(context).client, context: context)),
                    ], child: child),
                  );
                },
              );
            },
          );*/
        },
      ),
    );
  }
}
