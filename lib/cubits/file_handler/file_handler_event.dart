part of 'file_handler_bloc.dart';


sealed class FileHandlerEvent extends Equatable {
  const FileHandlerEvent();

  @override
  List<Object> get props => [];
}

final class FileSelectionRequested extends FileHandlerEvent {
  final FileType fileType;
  const FileSelectionRequested(this.fileType);

  @override
  List<Object> get props => [fileType];
}

final class CameraSelectionRequested extends FileHandlerEvent {
  final FileType fileType;
  const CameraSelectionRequested(this.fileType);

  @override
  List<Object> get props => [fileType];
}

class ShareEventAdditionRequested extends FileHandlerEvent{
  final List<Map<String, dynamic>> shareEvents;

  const ShareEventAdditionRequested({this.shareEvents = const []});
  @override
  List<Object> get props => [shareEvents];
}

class FileAdditionRequested<T> extends FileHandlerEvent {
  final List<T> files;

  const FileAdditionRequested({required this.files});

  @override
  List<Object> get props => [files];
}

class XFileAdditionRequested extends FileAdditionRequested<XFile> {
  const XFileAdditionRequested(List<XFile> files) : super(files: files);
}

class PlatformFileAdditionRequested extends FileAdditionRequested<PlatformFile> {
  const PlatformFileAdditionRequested(List<PlatformFile> files) : super(files: files);
}

/*class ShareFileAdditionRequested extends FileAdditionRequested<SharedMediaFile> {
  const ShareFileAdditionRequested(List<SharedMediaFile> files) : super(files: files);
}*/

class MatrixFileAdditionRequested extends FileAdditionRequested<MatrixFile> {
  const MatrixFileAdditionRequested(List<MatrixFile> files) : super(files: files);
}





final class FileCompletionRequested extends FileHandlerEvent {
  final bool isCancelled;

  const FileCompletionRequested({this.isCancelled = false});

  @override
  List<Object> get props => [];
}


final class FileRemoveRequested extends FileHandlerEvent {
  final int removeAtIndex;

  const FileRemoveRequested({required this.removeAtIndex});

  @override
  List<Object> get props => [];
}

final class FileReorderRequested extends FileHandlerEvent {
  final int oldIndex;
  final int newIndex;

  const FileReorderRequested({
    required this.oldIndex,
    required this.newIndex,
  });

  @override
  List<Object> get props => [oldIndex, newIndex];
}

final class FileFocusRequested extends FileHandlerEvent {
  final int focusOnIndex;

  const FileFocusRequested({required this.focusOnIndex});

  @override
  List<Object> get props => [];
}

final class FileFocusRemoveRequested extends FileHandlerEvent {
  final int removeFocusOnIndex;

  const FileFocusRemoveRequested({required this.removeFocusOnIndex});

  @override
  List<Object> get props => [];
}
