import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:blurhash_dart/blurhash_dart.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:ffmpeg_kit_flutter_min_gpl/ffmpeg_kit.dart';
import 'package:ffmpeg_kit_flutter_min_gpl/ffmpeg_kit_config.dart';
import 'package:ffmpeg_kit_flutter_min_gpl/ffprobe_kit.dart';
import 'package:ffmpeg_kit_flutter_min_gpl/media_information.dart';
import 'package:ffmpeg_kit_flutter_min_gpl/return_code.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image/image.dart';
import 'package:image_picker/image_picker.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/resize_image.dart';
import 'package:path_provider/path_provider.dart';

import '../../utils/platform_infos.dart';
import '../video_handler/util.dart';

//For PlatformFile
//For XFile

part 'file_handler_event.dart';
part 'file_handler_state.dart';
part 'file_state.dart';

class FileHandlerBloc extends Bloc<FileHandlerEvent, FileHandlerState> {
  final BuildContext context;

  final maximumFileNumber = 20;
  final maximumVideoFileNumber = 10;
  final maxIndividualFileSize = 100 * 1024 * 1024;
  final maxIndividualVideoFileSize = 60 * 1024 * 1024;

  FileHandlerBloc(this.context) : super(const FileHandlerState.initial()) {
    on<ShareEventAdditionRequested>(_onShareEventAdditionRequested);
    // File addition requests
    on<FileAdditionRequested>(_onFileAdditionRequested);
    // File Selection Requests
    on<FileSelectionRequested>(_onFileSelectionRequested);
    //File Completion Requests
    on<FileCompletionRequested>(_onFileCompletionRequested);
    //File Removal Requests
    on<FileRemoveRequested>(_onFileRemovalRequested);
    //File Focus Requests
    on<FileFocusRequested>(_onFileFocusRequested);
    //File Focus Removal Requests
    on<FileFocusRemoveRequested>(_onFileFocusRemoveRequested);
    //File Reorder Requests
    on<FileReorderRequested>(_onFileReorderRequested);
  }

  void _onShareEventAdditionRequested(ShareEventAdditionRequested event, Emitter<FileHandlerState> emit) async {
    Logs().v('Received a ShareEventAdditionRequested event');
    try {
      final updatedEvents = List<Map<String, dynamic>>.from(state.shareEventState)..addAll(event.shareEvents);
      emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: const [], shareEventState: updatedEvents));

      Logs().v('ShareEventAddition successfully completed.');
    } catch (e) {
      Logs().e('Error occurred during ShareEventAddition: $e');
      emit(FileHandlerState(processingStatus: ProcessingStatus.failure, filesState: state.filesState, errorMessage: 'ShareEventAddition error: $e'));
    }
  }

  void _onFileReorderRequested(FileReorderRequested event, Emitter<FileHandlerState> emit) {
    if (state.isOnlyShareEventState) {
      final List<Map<String, dynamic>> updatedShareEventState = List.from(state.shareEventState);
      // Save the item to move
      final Map<String, dynamic> itemToMove = updatedShareEventState[event.oldIndex];
      if (event.newIndex == state.filesState.length) {
        updatedShareEventState.removeAt(event.oldIndex);
        updatedShareEventState.add(itemToMove);
      } else if (event.oldIndex == 0) {
        updatedShareEventState.removeAt(event.oldIndex);
        updatedShareEventState.insert(event.newIndex - 1, itemToMove);
      } else {
        // Remove the item from its old position
        updatedShareEventState.removeAt(event.oldIndex);

        // Insert the item at its new position
        updatedShareEventState.insert(event.newIndex, itemToMove);
      }
      // Emit the new state
      emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: state.filesState, shareEventState: updatedShareEventState));
    } else {
      final List<FileState> updatedFileState = List.from(state.filesState);

      // Save the item to move
      final FileState itemToMove = updatedFileState[event.oldIndex];
      Logs().v('state.imagesState.length: ${state.filesState.length}, event.newIndex: ${event.newIndex}, event.oldIndex: ${event.oldIndex}');
      if (event.newIndex == state.filesState.length) {
        updatedFileState.removeAt(event.oldIndex);
        updatedFileState.add(itemToMove);
      } else if (event.oldIndex == 0) {
        updatedFileState.removeAt(event.oldIndex);
        updatedFileState.insert(event.newIndex - 1, itemToMove);
      } else {
        // Remove the item from its old position
        updatedFileState.removeAt(event.oldIndex);

        // Insert the item at its new position
        updatedFileState.insert(event.newIndex, itemToMove);
      }
      // Emit the new state
      emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: updatedFileState));
    }
  }

  void _onFileFocusRequested(FileFocusRequested event, Emitter<FileHandlerState> emit) {
    final List<FileState> updatedFileState = List.from(state.filesState);
    updatedFileState[event.focusOnIndex] = updatedFileState[event.focusOnIndex].copyWith(focusState: FocusState.focused);

    emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: updatedFileState));
  }

  void _onFileFocusRemoveRequested(FileFocusRemoveRequested event, Emitter<FileHandlerState> emit) {
    final List<FileState> updatedFileState = List.from(state.filesState);
    updatedFileState[event.removeFocusOnIndex] = updatedFileState[event.removeFocusOnIndex].copyWith(focusState: FocusState.unfocused);

    emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: updatedFileState));
  }

  void _onFileRemovalRequested(FileRemoveRequested event, Emitter<FileHandlerState> emit) {
    if (state.isOnlyShareEventState) {
      Logs().v("_onFileRemovalRequested: Share Event");
      emit(FileHandlerState(processingStatus: ProcessingStatus.removing, filesState: state.filesState, shareEventState: state.shareEventState));
      try {
        final tempShareEvents = List<Map<String, dynamic>>.from(state.shareEventState);
        Logs().v("_onFileRemovalRequested: Before removal file count: ${tempShareEvents.length.toString()}");
        tempShareEvents.removeAt(event.removeAtIndex);
        Logs().v("_onFileRemovalRequested: Temp after removal file count ${tempShareEvents.length.toString()}");
        if (tempShareEvents.isEmpty) {
          Logs().v('_onFileRemovalRequested - FileState was empty so we can just emit an empty FileState. Cancelling selection...');
          emit(FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: state.filesState, shareEventState: tempShareEvents));
          return;
        }

        emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: state.filesState, shareEventState: tempShareEvents));
      } catch (e) {
        Logs().e('Error occurred during share event removal: $e');
        emit(FileHandlerState(processingStatus: ProcessingStatus.failure, filesState: const [], shareEventState: const [], errorMessage: 'Removal error: $e'));
      }
    } else {
      Logs().v("_onFileRemovalRequested: File State");
      emit(FileHandlerState(processingStatus: ProcessingStatus.removing, filesState: state.filesState));
      try {
        Logs().v("_onFileRemovalRequested: Before removal file count: ${state.filesState.length.toString()}");
        final tempFiles = List<FileState>.from(state.filesState);
        tempFiles.removeAt(event.removeAtIndex);
        Logs().v("_onFileRemovalRequested: Temp after removal file count ${tempFiles.length.toString()}");

        if (tempFiles.isEmpty) {
          Logs().v('_onFileRemovalRequested - FileState was empty so we can just emit an empty FileState. Cancelling selection...');
          emit(FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: tempFiles));
          return;
        }

        emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: tempFiles));
      } catch (e) {
        Logs().e('Error occurred during file removal: $e');
        emit(FileHandlerState(processingStatus: ProcessingStatus.failure, filesState: const [], errorMessage: 'Removal error: $e'));
      }
    }
  }

  void _onFileCompletionRequested(FileCompletionRequested event, Emitter<FileHandlerState> emit) {
    if (event.isCancelled) {
      emit(const FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: []));
    } else {
      emit(const FileHandlerState(processingStatus: ProcessingStatus.idle, filesState: []));
    }
  }

  void _onFileSelectionRequested(FileSelectionRequested event, Emitter<FileHandlerState> emit) async {
    try {
      Logs().v('File selection requested...');
      emit(FileHandlerState(processingStatus: ProcessingStatus.selecting, filesState: state.filesState));
      final selectedFiles = await _selectFilesBasedOnPlatform(event.fileType);
      Logs().v('Number of selected files: ${selectedFiles.length}');
      fileSelectionLimitationHandler(event, selectedFiles);
      if (selectedFiles.isEmpty) {
        Logs().v('No files were selected. Cancelling selection...');
        if (state.filesState.isEmpty) {
          emit(const FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: []));
          Logs().v('FileState was empty so we can just emit an empty FileState. Cancelling selection...');
        } else {
          Logs().v('FileState was not empty so we just emit the current FileState. Cancelling selection...');
          emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: state.filesState));
        }
        return;
      }

      emit(FileHandlerState(processingStatus: ProcessingStatus.processing, filesState: state.filesState));

      final List<Future<FileState>> newFileStateFutures = platformFileToState(selectedFiles);

      //final List<Future<FileState>> fileFutures = selectedFile.map((file) => FileState.create(file, context)).toList();
      Logs().v('Number of file futures created: ${newFileStateFutures.length}');
      final List<FileState> newFiles = await Future.wait(newFileStateFutures);
      Logs().v('Number of new files processed: ${newFiles.length}');

      // Add new images to the existing ones
      final List<FileState> allFiles = List<FileState>.from(state.filesState)..addAll(newFiles);
      Logs().v('Total number of files after adding new ones: ${allFiles.length}');
      if (state.processingStatus == ProcessingStatus.cancelled) {
        return;
      }
      emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: allFiles));
      Logs().v('File selection successfully completed.');
    } catch (e) {
      Logs().e('Error occurred during file selection: $e');
      emit(FileHandlerState(processingStatus: ProcessingStatus.failure, filesState: state.filesState, errorMessage: 'Selection error: $e'));
    }
  }

  void fileSelectionLimitationHandler(FileSelectionRequested event, List<PlatformFile> selectedFiles) {
    if (event.fileType == FileType.video && selectedFiles.length > maximumVideoFileNumber) {
      selectedFiles.removeRange(maximumVideoFileNumber, selectedFiles.length - 1);
      Logs().v("Maximum number of videos selected, taking the first $maximumVideoFileNumber");
      showSnackBar("Maximum number of videos selected, taking the first $maximumVideoFileNumber");
    }
    if (selectedFiles.length >= maximumFileNumber) {
      selectedFiles.removeRange(maximumFileNumber, selectedFiles.length);
      Logs().v("Maximum number of ${event.fileType == FileType.image ? "images" : "files"} selected, taking the first $maximumFileNumber");
      showSnackBar("Maximum number of ${event.fileType == FileType.image ? "images" : "files"} selected, taking the first $maximumFileNumber");
    }
    if (event.fileType == FileType.video && selectedFiles.any((element) => element.size > maxIndividualVideoFileSize)) {
      selectedFiles.removeWhere((element) => element.size > maxIndividualVideoFileSize);
      Logs().v("Maximum individual video file size of 60Mb exceeded");
      showSnackBar("Maximum individual video file size of 60Mb exceeded");
    }
    if (selectedFiles.any((element) {
      return element.size > maxIndividualFileSize;
    })) {
      selectedFiles.removeWhere((element) => element.size > maxIndividualFileSize);
      Logs().v("Maximum individual file size of 100Mb exceeded");
      showSnackBar("Maximum individual file size of 100Mb exceeded");
    }
  }

  void showSnackBar(String message) {
    ScaffoldMessenger.of(context).showMaterialBanner(
      MaterialBanner(
        content: Text(
          message,
          style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
        ),
        backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
        actions: [
          IconButton(
            onPressed: () => ScaffoldMessenger.of(context).clearMaterialBanners(),
            icon: const Icon(Icons.close),
          )
        ],
      ),
    );
  }

  List<Future<FileState>> platformFileToState(List<PlatformFile> selectedFiles) {
    final List<Future<FileState>> newFileStateFutures = selectedFiles.map((file) async {
      Logs().i("File type: ${identifyPlatformFileType(file)} - ${file.extension}");
      if (identifyPlatformFileType(file) == FileType.image) {
        return await ImageState.createFromPlatformFile(file, context);
      } else if (identifyPlatformFileType(file) == FileType.video) {
        Logs().i("platformFileToState");
        return await VideoState.platformFileToState(file, context);
      } else {
        return await FileState.createFromPlatformFile(file, context);
      }
    }).toList();
    Logs().i("platformFileToState - ${newFileStateFutures.length}");
    return newFileStateFutures;
  }

  bool _numberOfFilesCheck(FileAdditionRequested event, Emitter<FileHandlerState> emit) {
    if (event is PlatformFileAdditionRequested) {
      if (event.files.length + state.filesState.length > maximumVideoFileNumber && event.files.every((file) => (identifyPlatformFileType(file) == FileType.video))) {
        final numberOfFilesToAdd = maximumVideoFileNumber - state.filesState.length;
        if (numberOfFilesToAdd <= 0) {
          emit(FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: state.filesState));
          showSnackBar("Maximum number of files already selected");
          return false;
        } else {
          event.files.removeRange(numberOfFilesToAdd - 1, event.files.length - 1);
          showSnackBar("Maximum number of videos selected, taking the first $maximumVideoFileNumber");
        }
      } else if (event.files.length + state.filesState.length > maximumFileNumber) {
        final numberOfFilesToAdd = maximumFileNumber - state.filesState.length;
        if (numberOfFilesToAdd <= 0) {
          emit(FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: state.filesState));
          showSnackBar("Maximum number of files already selected");
          return false;
        } else {
          event.files.removeRange(numberOfFilesToAdd - 1, event.files.length - 1);
          showSnackBar("Maximum number of files selected, taking the first $maximumFileNumber");
        }
      }
    }
    if (event is MatrixFileAdditionRequested) {
      if (event.files.length + state.filesState.length > maximumVideoFileNumber && event.files.every((file) => file is MatrixVideoFile)) {
        final numberOfFilesToAdd = maximumVideoFileNumber - state.filesState.length;
        if (numberOfFilesToAdd <= 0) {
          emit(FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: state.filesState));
          showSnackBar("Maximum number of files already selected");
          return false;
        } else {
          event.files.removeRange(numberOfFilesToAdd - 1, event.files.length - 1);
          showSnackBar("Maximum number of videos selected, taking the first $maximumVideoFileNumber");
        }
      } else if (event.files.length + state.filesState.length > maximumFileNumber) {
        final numberOfFilesToAdd = maximumFileNumber - state.filesState.length;
        if (numberOfFilesToAdd <= 0) {
          emit(FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: state.filesState));
          showSnackBar("Maximum number of files already selected");
          return false;
        } else {
          event.files.removeRange(numberOfFilesToAdd - 1, event.files.length - 1);
          showSnackBar("Maximum number of files selected, taking the first $maximumFileNumber");
        }
      }
    }
    if (event is XFileAdditionRequested) {
      if (event.files.length + state.filesState.length > maximumVideoFileNumber && event.files.every((element) => identifyXFileType(element) == FileType.video)) {
        final numberOfFilesToAdd = maximumVideoFileNumber - state.filesState.length;
        if (numberOfFilesToAdd <= 0) {
          emit(FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: state.filesState));
          showSnackBar("Maximum number of files already selected");
          return false;
        } else {
          event.files.removeRange(numberOfFilesToAdd - 1, event.files.length - 1);
          showSnackBar("Maximum number of videos selected, taking the first $maximumVideoFileNumber");
        }
      } else if (event.files.length + state.filesState.length > maximumFileNumber) {
        final numberOfFilesToAdd = maximumFileNumber - state.filesState.length;
        if (numberOfFilesToAdd <= 0) {
          emit(FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: state.filesState));
          showSnackBar("Maximum number of files already selected");
          return false;
        } else {
          event.files.removeRange(numberOfFilesToAdd - 1, event.files.length - 1);
          showSnackBar("Maximum number of files selected, taking the first $maximumFileNumber");
        }
      }
    }
    return true; // If all checks pass
  }

  Future<bool> _fileSizeChecks(FileAdditionRequested event, Emitter<FileHandlerState> emit) async {
    if (event is PlatformFileAdditionRequested) {
      if (event.files.every((file) => (identifyPlatformFileType(file) == FileType.video)) && event.files.any((element) => element.size > maxIndividualVideoFileSize)) {
        event.files.removeWhere((element) => element.size > maxIndividualVideoFileSize);
        if (event.files.isEmpty) {
          Logs().v("Maximum individual video file size of 60Mb exceeded");
          showSnackBar("Maximum individual video file size of 60Mb exceeded");
          return false;
        } else {
          Logs().v("Maximum individual video file size of 60Mb exceeded");
          showSnackBar("Maximum individual video file size of 60Mb exceeded");
        }
      }
      if (event.files.any((element) => element.size > maxIndividualFileSize)) {
        event.files.removeWhere((element) => element.size > maxIndividualFileSize);
        if (event.files.isEmpty) {
          Logs().v("Maximum individual file size of 100Mb exceeded");
          showSnackBar("Maximum individual file size of 100Mb exceeded");
          return false;
        } else {
          Logs().v("Maximum individual file size of 100Mb exceeded");
          showSnackBar("Maximum individual file size of 100Mb exceeded");
        }
      }
    } else if (event is MatrixFileAdditionRequested) {
      if (event.files.every((file) => file is MatrixVideoFile) && event.files.any((element) => element.size > maxIndividualVideoFileSize)) {
        event.files.removeWhere((element) => element.size > maxIndividualVideoFileSize);
        if (event.files.isEmpty) {
          Logs().v("Maximum individual video file size of 60Mb exceeded");
          showSnackBar("Maximum individual video file size of 60Mb exceeded");
          return false;
        } else {
          Logs().v("Maximum individual video file size of 60Mb exceeded");
          showSnackBar("Maximum individual video file size of 60Mb exceeded");
        }
      }
      if (event.files.any((element) => element.size > maxIndividualFileSize)) {
        event.files.removeWhere((element) => element.size > maxIndividualFileSize);
        if (event.files.isEmpty) {
          Logs().v("Maximum individual file size of 100Mb exceeded");
          showSnackBar("Maximum individual file size of 100Mb exceeded");
          return false;
        } else {
          Logs().v("Maximum individual file size of 100Mb exceeded");
          showSnackBar("Maximum individual file size of 100Mb exceeded");
        }
      }
    } else if (event is XFileAdditionRequested) {
      bool shouldShowSnackBar = false;
      Logs().v("XFileAdditionRequested: ${event.files.first.path}");
      if (event.files.every((element) => identifyXFileType(element) == FileType.video)) {
        for (int i = event.files.length - 1; i >= 0; i--) {
          //Since we're iterating backward, removing items won't affect the indices of the remaining items to be checked.
          final XFile file = event.files[i];
          final int fileSize = await file.length();
          if (fileSize > maxIndividualVideoFileSize) {
            event.files.removeAt(i);
            shouldShowSnackBar = true;
          }
        }
        if (event.files.isEmpty && shouldShowSnackBar) {
          Logs().v("Maximum individual video file size of 60Mb exceeded");
          showSnackBar("Maximum individual video file size of 60Mb exceeded");
          return false;
        } else if (shouldShowSnackBar) {
          Logs().v("Maximum individual video file size of 60Mb exceeded");
          showSnackBar("Maximum individual video file size of 60Mb exceeded");
        }
      } else {
        for (int i = event.files.length - 1; i >= 0; i--) {
          //Since we're iterating backward, removing items won't affect the indices of the remaining items to be checked.
          final XFile file = event.files[i];
          final int fileSize = await file.length();
          if (fileSize > maxIndividualFileSize) {
            event.files.removeAt(i);
            shouldShowSnackBar = true;
          }
        }
        if (event.files.isEmpty && shouldShowSnackBar) {
          Logs().v("Maximum individual file size of 100Mb exceeded");
          showSnackBar("Maximum individual file size of 100Mb exceeded");
          return false;
        } else if (shouldShowSnackBar) {
          Logs().v("Maximum individual file size of 100Mb exceeded");
          showSnackBar("Maximum individual file size of 100Mb exceeded");
        }
      }
    }
    return true; // If all checks pass
  }

  void _onFileAdditionRequested(FileAdditionRequested event, Emitter<FileHandlerState> emit) async {
    try {
      Logs().v('Received a FileAdditionRequested event');

      emit(FileHandlerState(processingStatus: ProcessingStatus.processing, filesState: state.filesState));
      final List<Future<FileState>> newFileStateFutures;

      if (!_numberOfFilesCheck(event, emit) || !await _fileSizeChecks(event, emit)) {
        emit(const FileHandlerState(processingStatus: ProcessingStatus.cancelled, filesState: []));
        return;
      }

      if (event is PlatformFileAdditionRequested) {
        newFileStateFutures = platformFileToState(event.files);
      } else if (event is XFileAdditionRequested) {
        final platformFiles = await _convertXFilesToPlatformFiles(event.files);
        newFileStateFutures = platformFileToState(platformFiles);
      } else {
        newFileStateFutures = event.files.map((file) => matrixFileToState(file, context)).toList();
      }

      final List<FileState> newFileState = await Future.wait(newFileStateFutures);

      Logs().v('Number of new files added: ${newFileState.length}');

      // Add new files to the existing ones
      final updatedFileState = List<FileState>.from(state.filesState)..addAll(newFileState);
      Logs().v('Total number of files after adding new ones: ${updatedFileState.length}');
      if (state.processingStatus == ProcessingStatus.cancelled) {
        return;
      }
      emit(FileHandlerState(processingStatus: ProcessingStatus.ready, filesState: updatedFileState));

      Logs().v('File addition successfully completed.');
    } catch (e) {
      Logs().e('Error occurred during file addition: $e');
      emit(FileHandlerState(processingStatus: ProcessingStatus.failure, filesState: state.filesState, errorMessage: 'File addition error: $e'));
    }
  }

  FileType identifyPlatformFileType(PlatformFile file) {
    // Mapping of file extensions to file types.
    final Map<String, FileType> extensionToFileType = {
      // Images
      'jpg': FileType.image,
      'jpeg': FileType.image,
      'png': FileType.image,
      'gif': FileType.image,
      'bmp': FileType.image,
      'webp': FileType.image,
      'tiff': FileType.image,
      // Videos
      'mp4': FileType.video,
      'avi': FileType.video,
      'mov': FileType.video,
      'flv': FileType.video,
      'wmv': FileType.video,
      // Audio
      'mp3': FileType.audio,
      'wav': FileType.audio,
      'ogg': FileType.audio,
      'flac': FileType.audio,
    };

    // Retrieve the file type using the extension.
    final FileType? fileType = extensionToFileType[file.extension?.toLowerCase()];

    // If the extension is not in the map, default to FileType.custom.
    return fileType ?? FileType.custom;
  }

  FileType identifyXFileType(XFile file) {
    // Mapping of file extensions to file types.
    final Map<String, FileType> extensionToFileType = {
      // Images
      'jpg': FileType.image,
      'jpeg': FileType.image,
      'png': FileType.image,
      'gif': FileType.image,
      'bmp': FileType.image,
      'webp': FileType.image,
      'tiff': FileType.image,
      // Videos
      'mp4': FileType.video,
      'avi': FileType.video,
      'mov': FileType.video,
      'flv': FileType.video,
      'wmv': FileType.video,
      // Audio
      'mp3': FileType.audio,
      'wav': FileType.audio,
      'ogg': FileType.audio,
      'flac': FileType.audio,
    };

    // Extract the file extension from the path
    final String extension = file.path.split('.').last.toLowerCase();

    // Retrieve the file type using the extension.
    final FileType? fileType = extensionToFileType[extension];

    // If the extension is not in the map, default to FileType.custom.
    return fileType ?? FileType.custom;
  }

  Future<FileState> matrixFileToState(MatrixFile file, BuildContext context) async {
    if (file is MatrixImageFile) {
      Logs().v("matrixFileToState - file is MatrixImageFile: ImageState.createFromMatrixImageFile");
      return await ImageState.createFromMatrixImageFile(file, context);
    } else if (file is MatrixVideoFile) {
      Logs().v("matrixFileToState - file is MatrixVideoFile: VideoState");
      return VideoState(videoFile: file);
    } else {
      Logs().v("matrixFileToState - file is MatrixFile: FileState");
      return FileState(file: file);
    }
  }

  Future<List<PlatformFile>> _selectFilesBasedOnPlatform(FileType fileType) async {
    if (PlatformInfos.isAndroid && fileType == FileType.image) {
      final androidVersion = await _checkAndroidVersion();
      if (androidVersion != null && androidVersion >= 13) {
        return await _selectImagesAndroid13();
      }
    }
    return await _selectFilesGeneric(fileType);
  }

  Future<int?> _checkAndroidVersion() async {
    if (Platform.isAndroid) {
      final androidInfo = await DeviceInfoPlugin().androidInfo;
      return androidInfo.version.sdkInt;
    } else {
      return null;
    }
  }

  Future<List<PlatformFile>> _selectImagesAndroid13() async {
    try {
      final picker = ImagePicker();
      final xFiles = await picker.pickMultiImage();
      if (xFiles.isEmpty) return [];
      return await _convertXFilesToPlatformFiles(xFiles);
    } catch (e) {
      Logs().e('Failed to select images: $e');
      return [];
    }
  }

  Future<List<PlatformFile>> _selectFilesGeneric(FileType fileType) async {
    try {
      final result = await FilePicker.platform.pickFiles(
        type: fileType,
        withData: isWithData(),
        allowCompression: true,
        allowMultiple: true,
        withReadStream: false,
      );
      return result?.files ?? [];
    } catch (e) {
      Logs().e('Failed to select $fileType: $e');
      return [];
    }
  }

  bool isWithData() {
    if (kIsWeb) {
      return true;
    } else if (PlatformInfos.isWindows || PlatformInfos.isIOS) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<PlatformFile>> _convertXFilesToPlatformFiles(List<XFile> xFiles) async {
    return await Future.wait(xFiles.map(_convertSingleXFileToPlatformFile));
  }

  Future<PlatformFile> _convertSingleXFileToPlatformFile(XFile xFile) async {
    final fileSize = await xFile.length();
    final fileBytes = await xFile.readAsBytes();
    return PlatformFile(
      name: xFile.name,
      path: xFile.path,
      size: fileSize,
      bytes: fileBytes,
    );
  }
}
