part of 'file_handler_bloc.dart';

enum FocusState { focused, unfocused }

class FileState extends Equatable {
  final MatrixFile file;
  final MatrixImageFile? thumbnailFile;
  final FocusState focusState;

  const FileState({
    required this.file,
    this.focusState = FocusState.unfocused,
    this.thumbnailFile,
  });

  @override
  List<Object?> get props => [file, focusState];

  @override
  bool? get stringify => true;

  bool get isFocused => focusState == FocusState.focused;

  MatrixImageFile? get getImageFile => null;

  MatrixVideoFile? get getVideoFile => null;

  FileState copyWith({
    MatrixFile? file,
    MatrixImageFile? thumbnailFile,
    FocusState? focusState,
  }) {
    return FileState(file: file ?? this.file, focusState: focusState ?? this.focusState, thumbnailFile: thumbnailFile ?? this.thumbnailFile);
  }

  static Future<FileState> createFromPlatformFile(PlatformFile platformFile, BuildContext context) async {
    Logs().v('Creating FileState...');
    final MatrixFile file;
    if (platformFile.bytes != null){
       file = MatrixFile(bytes: platformFile.bytes!, name: platformFile.name);
    }else{
      final File tempFile = File(platformFile.path!);
      file = MatrixFile(bytes: tempFile.readAsBytesSync(), name: platformFile.name);
    }
    return FileState(
      file: file,
    );
  }
}

class ImageState extends FileState {
  final MatrixImageFile imageFile;
  final MatrixImageFile compressedImageFile;

  const ImageState({
    super.focusState,
    required this.imageFile,
    super.thumbnailFile,
    required this.compressedImageFile,
  }) : super(file: imageFile);

  @override
  MatrixImageFile get file => imageFile;

  @override
  MatrixImageFile get getImageFile => super.file as MatrixImageFile;

  @override
  List<Object> get props => [imageFile, super.focusState];

  @override
  bool get isFocused => super.focusState == FocusState.focused;

  @override
  ImageState copyWith({
    MatrixFile? file,
    MatrixImageFile? thumbnailFile,
    MatrixFile? compressedImageFile,
    FocusState? focusState,
  }) {
    if (file != null && file is! MatrixImageFile) {
      throw ArgumentError('file must be of type MatrixImageFile');
    }
    return ImageState(
        imageFile: file as MatrixImageFile? ?? this.file,
        focusState: focusState ?? this.focusState,
        thumbnailFile: thumbnailFile ?? this.thumbnailFile,
        compressedImageFile: compressedImageFile as MatrixImageFile? ?? this.compressedImageFile);
  }

  static Future<BlurHash> createBlurHash(Uint8List file) async {
    final image = decodeImage(file)!;
    return BlurHash.encode(image, numCompX: 4, numCompY: 3);
  }

  static Future<ImageState> createFromMatrixImageFile(MatrixImageFile imageFile, BuildContext context) async {
    final ({ui.Image image, ui.Image thumbnail}) imageInfo = await getMetaData(imageFile.bytes, context);
    final ui.Image imageMetaData = imageInfo.image;
    final ui.Image thumbnailMetaData = imageInfo.thumbnail;

    final updatedImageFile = MatrixImageFile(
      bytes: imageFile.bytes,
      name: imageFile.name,
      width: imageMetaData.width,
      height: imageMetaData.height,
      mimeType: "image/${imageFile.mimeType}",
    );

    final MatrixImageFile compressedImageFile;

    if (imageMetaData.width > 1600 || imageMetaData.height > 1600) {
      compressedImageFile = MatrixImageFile(
        bytes: (await thumbnailMetaData.toByteData(format: ui.ImageByteFormat.png))!.buffer.asUint8List(),
        name: imageFile.name,
        mimeType: "image/png",
        width: thumbnailMetaData.width,
        height: thumbnailMetaData.height,
      );
    } else {
      compressedImageFile = updatedImageFile;
    }

    final MatrixImageFile thumbnailFile = MatrixImageFile(
      bytes: (await thumbnailMetaData.toByteData(format: ui.ImageByteFormat.png))!.buffer.asUint8List(),
      name: imageFile.name,
      mimeType: "image/png",
      width: thumbnailMetaData.width,
      height: thumbnailMetaData.height,
      //blurhash: blurHash.hash,
    );

    return ImageState(
      imageFile: updatedImageFile,
      compressedImageFile: compressedImageFile,
      thumbnailFile: thumbnailFile,
    );
  }

  static Future<ImageState> createFromPlatformFile(PlatformFile file, BuildContext context) async {
    final DateTime startImageState = DateTime.now();
    Logs().v('Creating ImageState...');

    if (file.bytes == null) {
      const String error = 'Failed to create ImageState - The file contains no bytes';
      Logs().e(error);
      throw Exception(error);
    }

    Logs().v('Calculating image metadata...');
    final DateTime startMetaData = DateTime.now();

    final ({ui.Image image, ui.Image thumbnail}) imageInfo = await getMetaData(file.bytes!, context);
    final ui.Image imageMetaData = imageInfo.image;
    final ui.Image thumbnailMetaData = imageInfo.thumbnail;
    final DateTime endMetaData = DateTime.now();
    final Duration timeTakenMetaData = endMetaData.difference(startMetaData);

    Logs().v('Time taken to calculate metadata: ${timeTakenMetaData.inMilliseconds} ms');
    Logs().v('Calculating image metadata complete');

    Logs().v('Creating blurHash...');
    final DateTime startBlurHash = DateTime.now();
    //final BlurHash blurHash = await createBlurHash(file.bytes!);
    final DateTime endBlurHash = DateTime.now();
    final Duration timeTakenBlurHash = endBlurHash.difference(startBlurHash);

    Logs().v('Time taken to calculate blurHash: ${timeTakenBlurHash.inMilliseconds} ms');
    Logs().v('Creating blurHash complete');

    Logs().v('Creating matrix image file...');

    final MatrixImageFile imageFile = MatrixImageFile(
      bytes: file.bytes!,
      name: file.name,
      mimeType: "image/${file.extension}",
      width: imageMetaData.width,
      height: imageMetaData.height,
      //blurhash: blurHash.hash,
    );

    final MatrixImageFile compressedImageFile = MatrixImageFile(
      bytes: (await thumbnailMetaData.toByteData(format: ui.ImageByteFormat.png))!.buffer.asUint8List(),
      name: file.name,
      mimeType: 'image/png',
      width: thumbnailMetaData.width,
      height: thumbnailMetaData.height,
      //blurhash: blurHash.hash,
    );

    final MatrixImageFile thumbnailFile = MatrixImageFile(
      bytes: (await thumbnailMetaData.toByteData(format: ui.ImageByteFormat.png))!.buffer.asUint8List(),
      name: file.name,
      mimeType: 'image/png',
      width: thumbnailMetaData.width,
      height: thumbnailMetaData.height,
      //blurhash: blurHash.hash,
    );
    Logs().v('Creating matrix image file complete');
    final DateTime endImageState = DateTime.now();
    final Duration timeTakenImageState = endImageState.difference(startImageState);
    Logs().v('Time taken to create ImageState: ${timeTakenImageState.inMilliseconds} ms');
    Logs().v('Returning ImageState...');
    return ImageState(
      imageFile: imageFile,
      compressedImageFile: compressedImageFile,
      thumbnailFile: thumbnailFile,
    );
  }

  static Future<({ui.Image image, ui.Image thumbnail})> getMetaData(Uint8List bytes, BuildContext context) async {
    const double thumbnailHeight = 800;
    final Uint8List imageData = bytes;
    Logs().v('Obtained image data from file...');

    final ui.Codec imageCodec = await ui.instantiateImageCodec(imageData);
    Logs().v('Instantiated image codec...');

    final ui.FrameInfo frameInfo = await imageCodec.getNextFrame();
    Logs().v('Retrieved the next frame...');

    final ui.Image image = frameInfo.image;
    final double aspectRatio = image.width / image.height;
    final int newWidth = (thumbnailHeight * aspectRatio).round();

    Logs().v('Retrieved the image from frame info...');
    final ui.Codec thumbnailCodec = await ui.instantiateImageCodec(imageData, targetHeight: thumbnailHeight.toInt(), targetWidth: newWidth);
    final ui.FrameInfo thumbnailFrameInfo = await thumbnailCodec.getNextFrame();
    final ui.Image thumbnail = thumbnailFrameInfo.image;

    return (image: image, thumbnail: thumbnail);
  }
}

class VideoState extends FileState {
  final MatrixVideoFile videoFile;

  const VideoState({
    super.focusState,
    required this.videoFile,
    super.thumbnailFile,
  }) : super(file: videoFile);

  @override
  List<Object> get props => [videoFile, super.focusState];

  @override
  bool get isFocused => super.focusState == FocusState.focused;

  @override
  MatrixVideoFile get file => videoFile;

  @override
  MatrixVideoFile get getVideoFile => super.file as MatrixVideoFile;

  @override
  VideoState copyWith({
    MatrixFile? file,
    MatrixImageFile? thumbnailFile,
    FocusState? focusState,
  }) {
    if (file != null && file is! MatrixVideoFile) {
      throw ArgumentError('file must be of type MatrixVideoFile');
    }
    return VideoState(
      videoFile: file as MatrixVideoFile? ?? this.file,
      focusState: focusState ?? this.focusState,
      thumbnailFile: thumbnailFile ?? this.thumbnailFile,
    );
  }

  static Future<VideoState> platformFileToState(PlatformFile file, BuildContext context) async {
    Logs().v('Creating VideoState for ${file.name}');
    final CompressedVideoResult? compressedVideoResult;
    final MatrixVideoFile videoFile;
    final MatrixImageFile? thumbnailFile;
    if (PlatformInfos.isMobile || PlatformInfos.isMacOS ){
       compressedVideoResult = await compressVideoFile(file);
       videoFile = compressedVideoResult!.compressedVideoFile!;
       thumbnailFile = compressedVideoResult.compressedThumbnailFile!;
    }else if (PlatformInfos.isWeb){
       videoFile = MatrixVideoFile(bytes: file.bytes!, name: file.name);
       thumbnailFile = await videoFile.getVideoThumbnail();
    }else if (PlatformInfos.isWindows || PlatformInfos.isLinux ){
       videoFile = MatrixVideoFile(bytes: file.bytes!, name: file.name);
       thumbnailFile = await videoFile.getVideoThumbnail();
    }else{
      videoFile = MatrixVideoFile(bytes: file.bytes!, name: file.name);
      thumbnailFile = await videoFile.getVideoThumbnail();
    }
    return VideoState(videoFile: videoFile, thumbnailFile: thumbnailFile);
  }

  static Future<CompressedVideoResult?> compressVideoFile(PlatformFile file) async {
    final Uint8List? compressedVideoBytes;
    final Uint8List? compressedThumbnailBytes;
    final VideoMetadata? videoMetadata;
    final ImageMetadata? imageMetadata;

    final directory = await getApplicationCacheDirectory();
    final compressedVideoTempFilePath = "${directory.absolute.absolute.path}/output-${DateTime.now().millisecondsSinceEpoch}.${file.extension}";
    final thumbnailTempFilePath = "${directory.absolute.path}/thumbnail-${DateTime.now().millisecondsSinceEpoch}.png";
    //final compressedVideoTempFileUri = await FileUriProvider().getUriForFile(compressedVideoTempFilePath, '${FlavorConfig.appId}.fileprovider');
    //final thumbnailTempFileUri = await FileUriProvider().getUriForFile(thumbnailTempFilePath, '${FlavorConfig.appId}.fileprovider');
    Logs().i("We here 1");
    final List<Uint8List?> results = await Future.wait([
      executeVideoCompression(file, compressedVideoTempFilePath),
      executeThumbnailGeneration(file, thumbnailTempFilePath),
    ]);
    Logs().i("We here 2");
    if (results.isEmpty) {
      Logs().e("Results were empty");
    }

    compressedVideoBytes = results[0];
    compressedThumbnailBytes = results[1];

    Logs().i("We here 3");
    final List<dynamic> metaDataResults = await Future.wait([
      extractVideoMetadata(compressedVideoTempFilePath),
      extractImageMetadata(thumbnailTempFilePath),
    ]);
    Logs().i("We here 4");
    videoMetadata = metaDataResults[0];
    imageMetadata = metaDataResults[1];

    final MatrixImageFile imageFile = MatrixImageFile(
      bytes: compressedThumbnailBytes!,
      name: file.name,
      width: imageMetadata!.width.toInt(),
      height: imageMetadata.height.toInt(),
      mimeType: "image/${imageMetadata.codecName}",
    );

    final MatrixVideoFile videoFile = MatrixVideoFile(
      bytes: compressedVideoBytes!,
      name: file.name,
      width: videoMetadata!.width.toInt(),
      height: videoMetadata.height.toInt(),
      mimeType: "video/${videoMetadata.codecName}",
      duration: videoMetadata.duration.toInt(),
    );

    Logs().i("Did we get here before we are done");

    return CompressedVideoResult(videoFile, imageFile);
  }

  static Future<ImageMetadata> extractImageMetadata(String thumbnailTempFilePath) async {
    final Completer<String?> metaDataCompleter = Completer<String?>();
    MediaInformation? information;

    String? metadataOutput;

    await FFprobeKit.getMediaInformationAsync(thumbnailTempFilePath, (session) async {
      information = (session).getMediaInformation();
      final state = FFmpegKitConfig.sessionStateToString(await session.getState());
      final returnCode = await session.getReturnCode();
      final failStackTrace = await session.getFailStackTrace();
      if (ReturnCode.isSuccess(returnCode)) {
        metadataOutput = await session.getOutput();
        metaDataCompleter.complete(metadataOutput);
      } else if (ReturnCode.isCancel(returnCode)) {
        metaDataCompleter.completeError(Exception(returnCode.toString()));
      } else {
        metaDataCompleter.completeError(Exception(returnCode.toString()));
        ffprint("extractImageMetadata failed with state $state and rc $returnCode.${notNull(failStackTrace, "\\n")}");
      }
      final File thumbnailTempFile = File(thumbnailTempFilePath);
      if (thumbnailTempFile.existsSync()) {
        await thumbnailTempFile.delete();
      }
    });

    double? height;
    double? width;
    String? codecName;

    await metaDataCompleter.future.then((value) {
      codecName =information!.getStreams().first.getCodec();
      width = information!.getStreams().first.getWidth()?.toDouble();
      height = information!.getStreams().first.getHeight()?.toDouble();
    });

    // Return only width and height as duration does not apply to images
    return ImageMetadata(width: width!, height: height!, codecName: codecName!);
  }

  static Future<VideoMetadata> extractVideoMetadata(String compressedVideoTempFilePath) async {
    final Completer<String?> metaDataCompleter = Completer<String?>();
    MediaInformation? information;
    String? metadataOutput;

    await FFprobeKit.getMediaInformationAsync(compressedVideoTempFilePath, (session) async {
      information = (session).getMediaInformation();
      final state = FFmpegKitConfig.sessionStateToString(await session.getState());
      final returnCode = await session.getReturnCode();
      final failStackTrace = await session.getFailStackTrace();
      if (ReturnCode.isSuccess(returnCode)) {
        metadataOutput = await session.getOutput();
        metaDataCompleter.complete(metadataOutput);
      } else if (ReturnCode.isCancel(returnCode)) {
        metaDataCompleter.completeError(Exception(returnCode.toString()));
      } else {
        metaDataCompleter.completeError(Exception(returnCode.toString()));
        ffprint("extractVideoMetadata failed with state $state and rc $returnCode.${notNull(failStackTrace, "\\n")}");
      }

      final File compressedVideoTempFile = File(compressedVideoTempFilePath);

      if (!compressedVideoTempFile.existsSync()) return;
      await compressedVideoTempFile.delete();
    });

    int? height;
    int? width;
    double? duration;
    String? codecName;

    await metaDataCompleter.future.then((value) {
      codecName =information!.getStreams().first.getCodec();
      duration = double.tryParse(information!.getDuration() ?? '0') ?? 0;
      width = information!.getStreams().first.getWidth();
      height = information!.getStreams().first.getHeight();
    });
    // Parsing logic here. This is an example and should be adapted based on actual output format
    return VideoMetadata(width: width!, height: height!, duration: duration!, codecName: codecName!);
  }

  static Future<Uint8List?> executeThumbnailGeneration(PlatformFile file, String thumbnailTempFilePath) async {
    final Completer<Uint8List?> thumbnailBytesCompleter = Completer<Uint8List>();
    File? thumbnailTempFile;

    final thumbnailFfmpegCommand = "-hwaccel auto -i '${file.path}' -ss 00:00:01 -frames:v 1 -vf 'scale=-1:800' '$thumbnailTempFilePath'";

    await FFmpegKit.executeAsync(thumbnailFfmpegCommand, (session) async {
      final state = FFmpegKitConfig.sessionStateToString(await session.getState());

      final returnCode = await session.getReturnCode();
      final failStackTrace = await session.getFailStackTrace();

      if (ReturnCode.isSuccess(returnCode)) {
        thumbnailTempFile = File(thumbnailTempFilePath);
        if (thumbnailTempFile == null) {
          Logs().e("executeThumbnailGeneration: $thumbnailTempFilePath does not exist");
          thumbnailBytesCompleter.completeError(Exception("executeThumbnailGeneration: $thumbnailTempFilePath does not exist"));
        }
        final Uint8List compressedVideoBytes = await thumbnailTempFile!.readAsBytes();
        thumbnailBytesCompleter.complete(compressedVideoBytes);
      } else if (ReturnCode.isCancel(returnCode)) {
        thumbnailBytesCompleter.completeError(Exception(returnCode.toString()));
      } else {
        thumbnailBytesCompleter.completeError(Exception(returnCode.toString()));
        ffprint("Encode failed with state $state and rc $returnCode.${notNull(failStackTrace, "\\n")}");
      }
    }, (log) => ffprint(log.getMessage()), (statistics) {})
        .then((session) => ffprint("Async FFmpeg process started with sessionId ${session.getSessionId()}."));

    return await thumbnailBytesCompleter.future.onError((error, stackTrace) {
      Logs().e("Error: ${error.toString()}\nStackTrace: ${stackTrace.toString()}");
      return null;
    });
  }

  static Future<Uint8List?> executeVideoCompression(PlatformFile file, String compressedVideoTempFilePath) async {
    final Completer<Uint8List?> compressedVideoBytesCompleter = Completer<Uint8List>();
    File? compressedVideoTempFile;

    //final ffmpegCommand = "-i ${file.path} -c:v libx264 -crf 35 -preset ultrafast ${compressedVideoTempFilePath}";
    //final ffmpegCommand = VideoUtil.generateCompressVideoScript(file.path!, compressedVideoTempFilePath, "mpeg4");
    final ffmpegCommand =
       "-hwaccel auto -i '${file.path}' -c:v libx264 -preset ultrafast -crf 34 -vf 'scale=800:-2' -r 24 -c:a aac -b:a 96k -g 30 '$compressedVideoTempFilePath'";

    final DateTime startTime = DateTime.now();
    DateTime? endTime;
    await FFmpegKit.executeAsync(
      ffmpegCommand,
      (session) async {
        final state = FFmpegKitConfig.sessionStateToString(await session.getState());

        final returnCode = await session.getReturnCode();
        final failStackTrace = await session.getFailStackTrace();

        if (ReturnCode.isSuccess(returnCode)) {
          compressedVideoTempFile = File(compressedVideoTempFilePath);
          if (compressedVideoTempFile == null) {
            compressedVideoBytesCompleter.completeError(Exception(returnCode.toString()));
          }
          final Uint8List compressedVideoBytes = await compressedVideoTempFile!.readAsBytes();
          compressedVideoBytesCompleter.complete(compressedVideoBytes);
        } else if (ReturnCode.isCancel(returnCode)) {
          compressedVideoBytesCompleter.completeError(Exception(returnCode.toString()));
        } else {
          compressedVideoBytesCompleter.completeError(Exception(returnCode.toString()));
          ffprint("Encode failed with state $state and rc $returnCode.${notNull(failStackTrace, "\\n")}");
        }
        endTime = DateTime.now();
      },
      (log) => ffprint(log.getMessage()),
      (statistics) {},
    ).then((session) => ffprint("Async FFmpeg process started with sessionId ${session.getSessionId()}."));

    final result = await compressedVideoBytesCompleter.future.onError((error, stackTrace) {
      Logs().e("Error: ${error.toString()}\nStackTrace: ${stackTrace.toString()}");
      return null;
    });
    Logs().i("Total Compression time: ${endTime?.difference(startTime).inMilliseconds}");
    return result;
  }
}

class CompressedVideoResult {
  final MatrixVideoFile? compressedVideoFile;
  final MatrixImageFile? compressedThumbnailFile;

  CompressedVideoResult(this.compressedVideoFile, this.compressedThumbnailFile);
}

class VideoMetadata {
  final int width;
  final int height;
  final double duration;
  final String codecName;

  VideoMetadata({required this.width, required this.height, required this.duration, required this.codecName, });
}

class ImageMetadata {
  final double width;
  final double height;
  final String codecName;

  ImageMetadata({required this.width, required this.height, required this.codecName});
}
