part of 'file_handler_bloc.dart';

enum ProcessingStatus { initial, cancelled, selecting, processing, compressing, ready, removing, failure, idle }

enum SendingStatus { idle, uploading, sent, failure }

class FileHandlerState extends Equatable {
  final List<Map<String, dynamic>> shareEventState;
  final List<FileState> filesState;
  final ProcessingStatus processingStatus;
  final SendingStatus fileSendingStatus;
  final String? errorMessage;

  const FileHandlerState( {
    required this.filesState,
    this.shareEventState = const [],
    this.errorMessage,
    this.processingStatus = ProcessingStatus.idle,
    this.fileSendingStatus = SendingStatus.idle,
  });

  // Define an initial state constructor
  const FileHandlerState.initial()
      : filesState = const [],
        shareEventState = const [],
        processingStatus = ProcessingStatus.idle,
        fileSendingStatus = SendingStatus.idle,
        errorMessage = null;

  bool get isOnlyImageState => filesState.every((fileState) => filesState is ImageState);
  bool get isOnlyVideoState => filesState.every((fileState) => filesState is VideoState);
  bool get isOnlyShareEventState => shareEventState.isNotEmpty && filesState.isEmpty;
  bool get isProcessing => processingStatus == ProcessingStatus.processing;
  bool get isCompressing => processingStatus == ProcessingStatus.compressing;

  @override
  List<Object> get props => [filesState, processingStatus, fileSendingStatus, isReadyWithFiles, isInitiated, isOnlyImageState, isOnlyVideoState, shareEventState];

  @override
  bool? get stringify => true;

  FileHandlerState copyWith({
    List<Map<String, dynamic>>? shareEventState,
    List<FileState>? filesState,
    ProcessingStatus? processingStatus,
    SendingStatus? fileSendingStatus,
    String? errorMessage,
    bool? isCompressionEnabled,
  }) {
    return FileHandlerState(
    shareEventState: shareEventState ?? this.shareEventState,
    filesState: filesState ?? this.filesState,
    processingStatus: processingStatus ?? this.processingStatus,
    fileSendingStatus: fileSendingStatus ?? this.fileSendingStatus,
    errorMessage: errorMessage ?? this.errorMessage,
    );
  }

  //bool get isInitiated => processingStatus
  bool get isInitiated {
    if (filesState.isNotEmpty || shareEventState.isNotEmpty) {
      return true;
    } else {
      switch (processingStatus) {
        case ProcessingStatus.initial:
          return true;
        case ProcessingStatus.selecting:
          return true;
        case ProcessingStatus.processing:
          return true;
        case ProcessingStatus.compressing:
          return true;
        case ProcessingStatus.ready:
          return true;
        case ProcessingStatus.removing:
          return true;
        case ProcessingStatus.failure:
          return true;
        case ProcessingStatus.cancelled:
          return false;
        case ProcessingStatus.idle:
          return false;
        default:
          return false;
      }
    }
  }

  bool get isReadyWithFiles {
    if (filesState.isEmpty && shareEventState.isEmpty) {
      return false;
    }
    switch (processingStatus) {
      case ProcessingStatus.initial:
        return false;
      case ProcessingStatus.selecting:
        return false;
      case ProcessingStatus.processing:
        return false;
      case ProcessingStatus.compressing:
        return false;
      case ProcessingStatus.ready:
        return true;
      case ProcessingStatus.removing:
        return true;
      case ProcessingStatus.failure:
        return false;
      case ProcessingStatus.cancelled:
        return false;
      case ProcessingStatus.idle:
        return false;
      default:
        return false;
    }
  }
}

class ImageHandlerState extends FileHandlerState {
  final List<ImageState> imagesState;

  const ImageHandlerState({
    required this.imagesState,
    super.processingStatus,
    super.fileSendingStatus,
    super.errorMessage,
  }) : super(filesState: imagesState);

  @override
  List<Object> get props => [
        imagesState,
        super.props,
        super.processingStatus,
        super.isReadyWithFiles,
        super.fileSendingStatus,
      ];

  @override
  bool? get stringify => true;
}

class VideoHandlerState extends FileHandlerState {
  final List<VideoState> videosState;

  const VideoHandlerState({
    required this.videosState,
    super.processingStatus,
    super.fileSendingStatus,
    super.errorMessage,
  }) : super(filesState: videosState);

  @override
  List<Object> get props => [
        videosState,
        super.props,
        super.processingStatus,
        super.isReadyWithFiles,
        super.fileSendingStatus,
      ];

  @override
  bool? get stringify => true;
}

/*class AudioHandlerState extends FileHandlerState {
  final List<AudioState> audiosState;

  const AudioHandlerState({
    required this.audiosState,
    super.processingStatus,
    super.fileSendingStatus,
    super.errorMessage,
  }) : super(filesState: audiosState);

  @override
  List<Object> get props => [audiosState, super.props];

  @override
  bool? get stringify => true;
}*/
