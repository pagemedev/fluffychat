/*
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_compression_flutter/flutter_image_compress.dart';
import 'package:image_compression_flutter/image_compression_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:matrix/matrix.dart';
import 'dart:ui' as ui;
import '../../config/themes.dart';
import '../../utils/platform_infos.dart';

part 'image_handler_event.dart';
part 'image_handler_state.dart';

class ImageHandlerBloc extends Bloc<ImageHandlerEvent, ImageHandlerState> {
  final BuildContext context;
  ImageHandlerBloc(this.context) : super(const InitialState()) {
    on<ImagesSelectionRequested>(_onImagesSelectionRequested);
    on<ImagesCompletionRequested>(_onImagesCompletionRequested);
    on<ImagesRemoveRequested>(_onImagesRemovalRequested);
    on<ImagesFocusRequested>(_onImagesFocusRequested);
    on<ImagesFocusRemoveRequested>(_onImagesFocusRemoveRequested);
    on<ImagesReorderRequested>(_onImagesReorderRequested);
  }

  void _onImagesReorderRequested(ImagesReorderRequested event, Emitter<ImageHandlerState> emit) {
    final List<ImageState> updatedImagesState = List.from(state.imagesState);

    // Save the item to move
    final ImageState itemToMove = updatedImagesState[event.oldIndex];
    Logs().v('state.imagesState.length: ${state.imagesState.length}, event.newIndex: ${event.newIndex}, event.oldIndex: ${event.oldIndex}');
    if (event.newIndex == state.imagesState.length) {
      updatedImagesState.removeAt(event.oldIndex);
      updatedImagesState.add(itemToMove);
    } else if (event.oldIndex == 0) {
      updatedImagesState.removeAt(event.oldIndex);
      updatedImagesState.insert(event.newIndex - 1, itemToMove);
    } else {
      // Remove the item from its old position
      updatedImagesState.removeAt(event.oldIndex);

      // Insert the item at its new position
      updatedImagesState.insert(event.newIndex, itemToMove);
    }

    // Emit the new state
    emit(SelectionState(status: ProcessingStatus.ready, imagesState: updatedImagesState));
  }

  void _onImagesFocusRequested(ImagesFocusRequested event, Emitter<ImageHandlerState> emit) {
    final List<ImageState> updatedImagesState = List.from(state.imagesState);
    updatedImagesState[event.focusOnIndex] = updatedImagesState[event.focusOnIndex].copyWith(imageStatus: FocusState.focused);

    emit(SelectionState(status: ProcessingStatus.ready, imagesState: updatedImagesState));
  }

  void _onImagesFocusRemoveRequested(ImagesFocusRemoveRequested event, Emitter<ImageHandlerState> emit) {
    final List<ImageState> updatedImagesState = List.from(state.imagesState);
    updatedImagesState[event.removeFocusOnIndex] = updatedImagesState[event.removeFocusOnIndex].copyWith(imageStatus: FocusState.unfocused);

    emit(SelectionState(status: ProcessingStatus.ready, imagesState: updatedImagesState));
  }

  void _onImagesRemovalRequested(ImagesRemoveRequested event, Emitter<ImageHandlerState> emit) {
    try {
      Logs().v(state.imagesState.length.toString());
      final tempFiles = List<ImageState>.from(state.imagesState);
      tempFiles.removeAt(event.removeAtIndex);
      Logs().v(tempFiles.length.toString());

      if (tempFiles.isEmpty) {
        emit(const SelectionState(status: ProcessingStatus.cancelled, imagesState: []));
      }

      emit(RemovalState(status: RemovalStatus.success, imagesState: tempFiles));
    } catch (e) {
      Logs().e('Error occurred during image removal: $e');
      emit(RemovalState(status: RemovalStatus.failure, imagesState: const [], errorMessage: 'Removal error: $e'));
    }
  }

  void _onImagesCompletionRequested(ImagesCompletionRequested event, Emitter<ImageHandlerState> emit) {
    if (event.isCancelled){
      emit(const SelectionState(status: ProcessingStatus.cancelled, imagesState: []));
    }else{
      emit(const SelectionState(status: ProcessingStatus.idle, imagesState: []));
    }
  }

  void _onImagesSelectionRequested(ImagesSelectionRequested event, Emitter<ImageHandlerState> emit) async {
    Logs().v('Images selection requested...');
    emit(SelectionState(status: ProcessingStatus.inProgress, imagesState: state.imagesState));
    try {
      final selectedImages = await _selectImagesBasedOnPlatform();
      Logs().v('Number of selected images: ${selectedImages.length}');

      if (selectedImages.isEmpty) {
        Logs().v('No images were selected. Cancelling selection...');
        emit(const SelectionState(status: ProcessingStatus.cancelled, imagesState: []));
        return;
      }

      emit(SelectionState(status: ProcessingStatus.processing, imagesState: state.imagesState));
      final List<Future<ImageState>> imageFutures = selectedImages.map((file) => ImageState.create(file, context)).toList();
      Logs().v('Number of image futures created: ${imageFutures.length}');
      emit(SelectionState(status: ProcessingStatus.compressing, imagesState: state.imagesState));
      final List<ImageState> newImages = await Future.wait(imageFutures);
      Logs().v('Number of new images processed: ${newImages.length}');

      // Add new images to the existing ones
      final List<ImageState> allImages = List<ImageState>.from(state.imagesState)..addAll(newImages);
      Logs().v('Total number of images after adding new ones: ${allImages.length}');

      emit(SelectionState(status: ProcessingStatus.ready, imagesState: allImages));
      Logs().v('Image selection successfully completed.');
    } catch (e) {
      Logs().e('Error occurred during image selection: $e');
      emit(SelectionState(status: ProcessingStatus.failure, imagesState: state.imagesState, errorMessage: 'Selection error: $e'));
    }
  }

  Future<List<PlatformFile>> _selectImagesBasedOnPlatform() async {
    if (PlatformInfos.isAndroid) {
      final androidVersion = await _checkAndroidVersion();
      if (androidVersion != null && androidVersion >= 13) {
        return await _selectImagesAndroid13();
      }
    }
    return await _selectImagesGeneric();
  }

  Future<int?> _checkAndroidVersion() async {
    if (Platform.isAndroid) {
      final androidInfo = await DeviceInfoPlugin().androidInfo;
      return androidInfo.version.sdkInt;
    } else {
      return null;
    }
  }

  Future<List<PlatformFile>> _selectImagesAndroid13() async {
    try {
      final picker = ImagePicker();
      final xFiles = await picker.pickMultiImage();
      if (xFiles.isEmpty) return [];
      return await _convertXFilesToPlatformFiles(xFiles);
    } catch (e) {
      Logs().e('Failed to select images: $e');
      return [];
    }
  }

  Future<List<PlatformFile>> _selectImagesGeneric() async {
    try {
      final result = await FilePicker.platform.pickFiles(
        type: FileType.image,
        withData: true,
        allowMultiple: true,
      );
      return result?.files ?? [];
    } catch (e) {
      Logs().e('Failed to select images: $e');
      return [];
    }
  }

  Future<List<PlatformFile>> _convertXFilesToPlatformFiles(List<XFile> xFiles) async {
    return await Future.wait(xFiles.map(_convertSingleXFileToPlatformFile));
  }

  Future<PlatformFile> _convertSingleXFileToPlatformFile(XFile xFile) async {
    final fileSize = await xFile.length();
    final fileBytes = await xFile.readAsBytes();
    if (fileSize == null || fileBytes == null) {
      throw Exception('Failed to read file: ${xFile.path}');
    }
    return PlatformFile(
      name: xFile.name,
      path: xFile.path,
      size: fileSize,
      bytes: fileBytes,
    );
  }

  List<Uint8List> _convertPlatformFilesToBytes(List<PlatformFile> platformFiles) {
    return platformFiles.map((file) => file.bytes!).toList();
  }
}
*/
