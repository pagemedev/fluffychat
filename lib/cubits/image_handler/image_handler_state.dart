/*
part of 'image_handler_bloc.dart';

enum ProcessingStatus { initial, cancelled, inProgress, processing, compressing, ready, failure, idle }

enum RemovalStatus { success, failure, idle }

enum FocusState { focused, unfocused }

abstract class ImageHandlerState extends Equatable {
  final List<ImageState> imagesState;
  final String? errorMessage;

  const ImageHandlerState({required this.imagesState, this.errorMessage});

  @override
  List<Object> get props => [imagesState];

  @override
  bool? get stringify => true;

  bool isInitiated(){
    if (this is SelectionState && (this as SelectionState).isInitiated()) {
      return true;
    } else if (this is RemovalState && (this as RemovalState).hasImages()) {
      return true;
    }else{
      return false;
    }
  }

  bool isReadyWithImages(){
    if (this is SelectionState && (this as SelectionState).isReadyWithImages()) {
      return true;
    } else if (this is RemovalState && (this as RemovalState).isReadyWithImages()) {
      return true;
    }else{
      return false;
    }
  }
}

class ImageState extends Equatable {
  final Uint8List imageBytes;
  final FocusState imageStatus;
  final Uint8List? thumbnailBytes;
  final int width;
  final int height;
  final String name;
  final String? mimeType;

  const ImageState({
    required this.imageBytes,
    this.thumbnailBytes,
    required this.imageStatus,
    required this.width,
    required this.height,
    required this.name,
    this.mimeType,
  });

  @override
  List<Object> get props => [imageBytes, imageStatus, width, height, thumbnailBytes ?? ''];

  bool get isFocused => imageStatus == FocusState.focused;

  ImageState copyWith({FocusState? imageStatus, Uint8List? imageBytes, Uint8List? thumbnailBytes, int? width, int? height}) {
    return ImageState(
      imageStatus: imageStatus ?? this.imageStatus,
      imageBytes: imageBytes ?? this.imageBytes,
      width: width ?? this.width,
      height: height ?? this.height,
      thumbnailBytes: thumbnailBytes ?? this.thumbnailBytes,
      name: name,
      mimeType: mimeType,
    );
  }

  static Future<Uint8List?> createThumbnailMobile({required PlatformFile file, required int width, required int height}) async {
    Uint8List? thumbnailBytes;
    try {
      thumbnailBytes = await FlutterImageCompress.compressWithList(
        file.bytes!,
        minWidth: width,
        minHeight: height,
        quality: 100, // compression quality, 25 is usually low enough
        format: CompressFormat.webp, // you can choose the format
      );

      Logs().v('Thumbnail image successfully compressed.');
    } catch (e) {
      Logs().e('Error while compressing the thumbnail image: $e');
    }
    return thumbnailBytes;
  }

  static Future<Uint8List?> createThumbnailDesktop({required PlatformFile file, required int width, required int height}) async {
    Uint8List? thumbnailBytes;
    ImageFile? imageInput;
    const Configuration config = Configuration(
      outputType: ImageOutputType.png,
      useJpgPngNativeCompressor: true,
      quality: 20,
    );

    try {
      imageInput = ImageFile(filePath: file.identifier ?? '', rawBytes: file.bytes!, width: width, height: height);
      final param = ImageFileConfiguration(input: imageInput, config: config);
      final output = await compressor.compress(param);

      thumbnailBytes = output.rawBytes;
      Logs().v('Thumbnail image successfully compressed. Before: ${output.sizeInBytes}, After: ${file.size}');
    } catch (e) {
      Logs().e('Error while compressing the thumbnail image: $e');
    }
    return thumbnailBytes;
  }

  static Future<ImageState> create(PlatformFile file, BuildContext context) async {
    Logs().v('Creating ImageState...');
    final double thumbnailHeight = PageMeThemes.thumbnailImageHeight(context);
    Logs().v('Calculated max height: $thumbnailHeight');

    if (file.bytes == null) {
      String error = 'Failed to create ImageState - The file contains no bytes';
      Logs().e(error);
      throw Exception(error);
    }

    final Uint8List imageData = file.bytes!;
    Logs().v('Obtained image data from file...');

    final ui.Codec codec = await ui.instantiateImageCodec(imageData);
    Logs().v('Instantiated image codec...');

    final ui.FrameInfo frameInfo = await codec.getNextFrame();
    Logs().v('Retrieved the next frame...');

    final ui.Image image = frameInfo.image;
    Logs().v('Retrieved the image from frame info...');

    // Calculate the aspect ratio of the original image.
    final double aspectRatio = image.width / image.height;
    // Calculate the new width based on the aspect ratio and the fixed height of 100.
    final int newWidth = (thumbnailHeight * aspectRatio).round();

    Uint8List? thumbnailBytes;
    if (PlatformInfos.isDesktop) {
      //thumbnailBytes = await createThumbnailDesktop(file: file, height: maxHeight.toInt(), width: newWidth);
    } else if (PlatformInfos.isWeb) {
      thumbnailBytes = await createThumbnailDesktop(
        file: file,
        height: thumbnailHeight.toInt(),
        width: newWidth,
      );
    } else {
      thumbnailBytes = await createThumbnailMobile(
        file: file,
        height: thumbnailHeight.toInt(),
        width: newWidth,
      );
    }

    Logs().d('Image dimensions - Width: ${image.width}, Height: ${image.height}');

    Logs().v('Creating and returning ImageState...');
    return ImageState(imageBytes: imageData, imageStatus: FocusState.unfocused, width: image.width, height: image.height, thumbnailBytes: thumbnailBytes, name: file.name, mimeType: file.extension);
  }
}

class InitialState extends ImageHandlerState {
  const InitialState() : super(imagesState: const []);

  @override
  List<Object> get props => [imagesState];

  @override
  bool? get stringify => true;
}

class SelectionState extends ImageHandlerState {
  final ProcessingStatus status;

  const SelectionState({required this.status, required super.imagesState, super.errorMessage});

  SelectionState copyWith({ProcessingStatus? status, List<ImageState>? imagesState, String? errorMessage}) {
    return SelectionState(status: status ?? this.status, imagesState: imagesState ?? this.imagesState, errorMessage: errorMessage ?? this.errorMessage);
  }

  @override
  bool? get stringify => true;

  @override
  List<Object> get props => [imagesState, status];

  bool isInitiated() {
    if (imagesState.isNotEmpty) {
      return true;
    } else {
      switch (status) {
        case ProcessingStatus.initial:
          return true;
        case ProcessingStatus.inProgress:
          return true;
        case ProcessingStatus.processing:
          return true;
        case ProcessingStatus.compressing:
          return true;
        case ProcessingStatus.ready:
          return true;
        case ProcessingStatus.failure:
          return true;
        case ProcessingStatus.cancelled:
          return false;
        case ProcessingStatus.idle:
          return false;
        default:
          return false;
      }
    }
  }


  @override
  bool isReadyWithImages() {
    if (imagesState.isEmpty) {
      return false;
    }
    switch (status) {
      case ProcessingStatus.initial:
        return false;
      case ProcessingStatus.inProgress:
        return false;
      case ProcessingStatus.processing:
        return false;
      case ProcessingStatus.compressing:
        return false;
      case ProcessingStatus.ready:
        return true;
      case ProcessingStatus.failure:
        return false;
      case ProcessingStatus.cancelled:
        return false;
      case ProcessingStatus.idle:
        return false;
      default:
        return false;
    }
  }
}

class RemovalState extends ImageHandlerState {
  final RemovalStatus status;

  const RemovalState({required this.status, required super.imagesState, super.errorMessage});

  RemovalState copyWith({RemovalStatus? status, List<ImageState>? imagesState, String? errorMessage}) {
    return RemovalState(
      status: status ?? this.status,
      imagesState: imagesState ?? this.imagesState,
      errorMessage: errorMessage ?? this.errorMessage,
    );
  }

  @override
  bool? get stringify => true;

  @override
  List<Object> get props => [imagesState, status];

  bool hasImages() {
    if (imagesState.isEmpty) {
      return false;
    }
    return true;
  }

  @override
  bool isReadyWithImages() {
    switch (status) {
      case RemovalStatus.success:
        if (imagesState.isEmpty) {
          return false;
        }
        return true;
      case RemovalStatus.failure:
        return false;
      default:
        return false;
    }
  }
}

*/
/*enum ImageHandlerStatus { initial, selectionCancelled, selectionInProgress, selectionSuccess, selectionFailure, removalSuccess, removalFailure }





class ImageHandlerState extends Equatable {
  const ImageHandlerState({
    this.status = ImageHandlerStatus.initial,
    this.files = const [],
  });

  final ImageHandlerStatus status;
  final List<Uint8List> files; // assuming the converted images are Uint8List

  ImageHandlerState copyWith({
    ImageHandlerStatus? status,
    List<Uint8List>? files,
  }) {
    return ImageHandlerState(
      status: status ?? this.status,
      files: files ?? this.files,
    );
  }

  @override
  List<Object> get props => [status, files];
}*//*
*/
/*

*//*

*/
/*
sealed class ImageHandlerState extends Equatable {
  const ImageHandlerState({
    this.files = const [],
  });

  final List<Uint8List> files;

  @override
  List<Object> get props => [];

}

class InitialState extends ImageHandlerState {
  const InitialState({required List<Uint8List> files}): super(files: files);

  InitialState copyWith({List<Uint8List>? files}) {
    return InitialState(files: files ?? this.files);
  }
}

class SelectionCancelledState extends ImageHandlerState {
  const SelectionCancelledState({required List<Uint8List> files}) : super(files: files);

  SelectionCancelledState copyWith({List<Uint8List>? files}) {
    return SelectionCancelledState(files: files ?? this.files);
  }
}

class SelectionInProgressState extends ImageHandlerState {
  const SelectionInProgressState({required List<Uint8List> files}) : super(files: files);

  SelectionInProgressState copyWith({List<Uint8List>? files}) {
    return SelectionInProgressState(files: files ?? this.files);
  }

}

class SelectionSuccessState extends ImageHandlerState {
  const SelectionSuccessState({required List<Uint8List> files}) : super(files: files);

  SelectionSuccessState copyWith({List<Uint8List>? files}) {
    return SelectionSuccessState(files: files ?? this.files);
  }
}

class SelectionFailureState extends ImageHandlerState {
  const SelectionFailureState({required List<Uint8List> files}) : super(files: files);

  SelectionFailureState copyWith({List<Uint8List>? files}) {
    return SelectionFailureState(files: files ?? this.files);
  }
}

class RemovalSuccessState extends ImageHandlerState {
  const RemovalSuccessState({required List<Uint8List> files}) : super(files: files);

  RemovalSuccessState copyWith({List<Uint8List>? files}) {
    return RemovalSuccessState(files: files ?? this.files);
  }
}

class RemovalFailureState extends ImageHandlerState {
  const RemovalFailureState({required List<Uint8List> files}) : super(files: files);

  RemovalFailureState copyWith({List<Uint8List>? files}) {
    return RemovalFailureState(files: files ?? this.files);
  }
}*//*
*/
/*

*/
