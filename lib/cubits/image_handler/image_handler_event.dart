/*
part of 'image_handler_bloc.dart';

sealed class ImageHandlerEvent extends Equatable {
  const ImageHandlerEvent();

  @override
  List<Object> get props => [];
}

final class ImagesSelectionRequested extends ImageHandlerEvent {
  const ImagesSelectionRequested();

  @override
  List<Object> get props => [];
}


final class ImagesCompletionRequested extends ImageHandlerEvent {
  final bool isCancelled;

  const ImagesCompletionRequested({this.isCancelled = false});

  @override
  List<Object> get props => [];
}

final class ImagesEditingRequested extends ImageHandlerEvent {

  @override
  List<Object> get props => [];
}

final class ImagesRemoveRequested extends ImageHandlerEvent {
  final int removeAtIndex;

  const ImagesRemoveRequested({required this.removeAtIndex});

  @override
  List<Object> get props => [];
}

final class ImagesReorderRequested extends ImageHandlerEvent {
  final int oldIndex;
  final int newIndex;

  const ImagesReorderRequested({
    required this.oldIndex,
    required this.newIndex,
  });

  @override
  List<Object> get props => [oldIndex, newIndex];
}

final class ImagesFocusRequested extends ImageHandlerEvent {
  final int focusOnIndex;

  const ImagesFocusRequested({required this.focusOnIndex});

  @override
  List<Object> get props => [];
}

final class ImagesFocusRemoveRequested extends ImageHandlerEvent {
  final int removeFocusOnIndex;

  const ImagesFocusRemoveRequested({required this.removeFocusOnIndex});

  @override
  List<Object> get props => [];
}*/
