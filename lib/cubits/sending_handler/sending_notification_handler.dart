import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/cubits/sending_handler/sending_handler_bloc.dart';
import 'package:pageMe/utils/platform_infos.dart';

import '../../config/flavor_config.dart';

class SendingProgressNotificationHandler extends StatefulWidget {
  const SendingProgressNotificationHandler({
    super.key,
    required this.child,
  });

  final Widget? child;

  @override
  State<SendingProgressNotificationHandler> createState() => _SendingProgressNotificationHandlerState();
}

class _SendingProgressNotificationHandlerState extends State<SendingProgressNotificationHandler> {
  bool _isInitialized = false;
  late final List<DarwinNotificationCategory> appleNotificationCategories;
  late final InitializationSettings initializationSettings;
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  final List<AndroidNotificationAction> androidActions = [
    const AndroidNotificationAction('CANCEL_SEND', 'Cancel', showsUserInterface: true),
  ];

  Future<void> _showSendingProgressNotification(SendingFilesInProgress state) async {
    final int progressId = state.sendingId;
    final int itemsSent = (state.sentRooms) * state.totalItemsInCurrentRoom + (state.sentItemsInCurrentRoom + 1);
    final int maxProgress = state.totalRooms * state.totalItemsInCurrentRoom;

    final AndroidNotificationDetails androidNotificationDetails = maxProgress > 1
        ? AndroidNotificationDetails(
            "${FlavorConfig.pushNotificationsChannelId} Sending Progress",
            "${FlavorConfig.pushNotificationsChannelName} Sending Progress",
            channelDescription: "${FlavorConfig.pushNotificationsChannelDescription}  Sending Progress",
            channelShowBadge: false,
            importance: Importance.max,
            autoCancel: true,
            priority: Priority.high,
            onlyAlertOnce: true,
            showProgress: true,
            maxProgress: maxProgress,
            progress: itemsSent,
            actions: androidActions,
          )
        : AndroidNotificationDetails(
            "${FlavorConfig.pushNotificationsChannelId} Sending Progress",
            "${FlavorConfig.pushNotificationsChannelName} Sending Progress",
            channelDescription: "${FlavorConfig.pushNotificationsChannelDescription}  Sending Progress",
            channelShowBadge: false,
            importance: Importance.max,
            priority: Priority.high,
            onlyAlertOnce: true,
            showProgress: true,
            indeterminate: true,
            actions: androidActions,
          );
    final NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails);
    await flutterLocalNotificationsPlugin.show(progressId, 'Sending files', '$itemsSent out of $maxProgress', notificationDetails);
  }

  Future<void> _showSendingCompleteNotification(SendingFileHandlerComplete state) async {
    final int progressId = state.sendingId;

    final AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails(
      "${FlavorConfig.pushNotificationsChannelId} Sending Progress",
      "${FlavorConfig.pushNotificationsChannelName} Sending Progress",
      channelDescription: "${FlavorConfig.pushNotificationsChannelDescription}  Sending Progress",
      channelShowBadge: false,
      importance: Importance.max,
      autoCancel: true,
      timeoutAfter: 5000,
      priority: Priority.high,
      onlyAlertOnce: true,
      showProgress: true,
    );
    final NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails);
    await flutterLocalNotificationsPlugin.show(progressId, 'Sending files complete', null, notificationDetails);
  }

  Future<void> _showSendingCancelledNotification(SendingFileHandlerCancelled state) async {
    final int progressId = state.sendingId;

    final AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails(
      "${FlavorConfig.pushNotificationsChannelId} Sending Progress",
      "${FlavorConfig.pushNotificationsChannelName} Sending Progress",
      channelDescription: "${FlavorConfig.pushNotificationsChannelDescription}  Sending Progress",
      channelShowBadge: false,
      importance: Importance.max,
      autoCancel: true,
      timeoutAfter: 2000,
      priority: Priority.high,
      onlyAlertOnce: true,
    );
    final NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails);
    await flutterLocalNotificationsPlugin.show(progressId, 'Sending files cancelled', null, notificationDetails);
  }

  Future<void> _showSendingErrorNotification(SendingFileHandlerError state) async {
    final int progressId = state.sendingId;

    final AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails(
      "${FlavorConfig.pushNotificationsChannelId} Sending Progress",
      "${FlavorConfig.pushNotificationsChannelName} Sending Progress",
      channelDescription: "${FlavorConfig.pushNotificationsChannelDescription}  Sending Progress",
      channelShowBadge: false,
      importance: Importance.max,
      autoCancel: true,
      timeoutAfter: 6000,
      priority: Priority.high,
      onlyAlertOnce: true,
    );
    final NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails);
    await flutterLocalNotificationsPlugin.show(progressId, 'Sending files had an error', state.message, notificationDetails);
  }

  void _handleForegroundNotification(NotificationResponse response) async {
    print('handleForegroundNotification: ${response.payload}, ${response.id}, ${response.actionId}, ${response.input}, ${response.notificationResponseType}');
    if (response.actionId == 'CANCEL_SEND') {
      BlocProvider.of<SendingHandlerBloc>(context).add(const CancelSendEvent());
    }
  }

  final List<DarwinNotificationAction> appleActions = [
    DarwinNotificationAction.plain('CANCEL_SEND', 'Cancel', options: {DarwinNotificationActionOption.foreground}),
  ];

  @override
  void didChangeDependencies() async {
    if (_isInitialized) {
      await _initialize();
      _isInitialized = true;
    }

    super.didChangeDependencies();
  }

  Future<void> _initialize() async {
    appleNotificationCategories = [
      DarwinNotificationCategory(
        'SEND_HANDLER',
        actions: appleActions,
        options: <DarwinNotificationCategoryOption>{
          DarwinNotificationCategoryOption.hiddenPreviewShowTitle,
        },
      )
    ];

    initializationSettings = InitializationSettings(
      android: const AndroidInitializationSettings('@drawable/ic_notifications_logo'),
      iOS: DarwinInitializationSettings(notificationCategories: appleNotificationCategories),
      macOS: DarwinInitializationSettings(
        notificationCategories: appleNotificationCategories,
      ),
    );

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: _handleForegroundNotification,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SendingHandlerBloc, SendingHandlerState>(
      listenWhen: (previous, next) {
        if (!PlatformInfos.isAndroid) {
          return false;
        }
        if (next is SendingFilesInProgress && previous is SendingFilesInProgress) {
          Logs().v("previous.sentItemsInCurrentRoom: ${previous.sentItemsInCurrentRoom}, next.sentItemsInCurrentRoom: ${next.sentItemsInCurrentRoom}");
          if (previous.sentItemsInCurrentRoom != next.sentItemsInCurrentRoom) {
            return true;
          }
        }
        if ((previous is SendingHandlerCancelled || previous is SendingHandlerError) && next is SendingHandlerComplete) {
          return false;
        } else {
          return previous != next;
        }
      },
      listener: (context, state) async {
        if (PlatformInfos.isAndroid) {
          if (state is SendingFilesInProgress) {
            await _showSendingProgressNotification(state);
          } else if (state is SendingFileHandlerComplete) {
            await _showSendingCompleteNotification(state);
          } else if (state is SendingFileHandlerCancelled) {
            await _showSendingCancelledNotification(state);
          } else if (state is SendingFileHandlerError) {
            await _showSendingErrorNotification(state);
          }
        }
      },
      child: widget.child,
    );
  }
}
