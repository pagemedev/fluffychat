part of 'sending_handler_bloc.dart';

abstract class SendingHandlerEvent extends Equatable {
  const SendingHandlerEvent({required this.roomIds});

  final List<String> roomIds;

  @override
  List<Object> get props => [];
}

class CancelSendEvent extends SendingHandlerEvent {
  const CancelSendEvent() : super(roomIds: const []);

  @override
  List<Object> get props => [];
}

class SendTextMessageRequested extends SendingHandlerEvent {
  final String currentText;

  final Event? replyEvent;
  final String? editedEventId;

  const SendTextMessageRequested({
    required super.roomIds,
    required this.currentText,
    this.replyEvent,
    this.editedEventId,
  });

  @override
  List<Object> get props => [currentText, roomIds];
}

class SendFilesRequested extends SendingHandlerEvent {
  final List<FileState> files;
  final Event? replyEvent;

  const SendFilesRequested({
    required super.roomIds,
    required this.files,
    this.replyEvent,
  });

  @override
  List<Object> get props => [files, roomIds];
}

class SendImagesRequested extends SendingHandlerEvent {
  final List<ImageState> images;
  final Event? replyEvent;

  const SendImagesRequested({
    required super.roomIds,
    required this.images,
    this.replyEvent,
  });

  @override
  List<Object> get props => [images, roomIds];
}

class SendQuillMessageRequested extends SendingHandlerEvent {
  final String currentText;
  final Event? replyEvent;
  final String? editedEventId;
  const SendQuillMessageRequested({
    required super.roomIds,
    required this.currentText,
    this.replyEvent,
    this.editedEventId,
  });
}

class SendShareEventRequested extends SendingHandlerEvent {
  final List<Map<String, dynamic>> shareEventState;

  const SendShareEventRequested({
    required super.roomIds,
    required this.shareEventState,
  });
}

class SendAudioMessageRequested extends SendingHandlerEvent {
  const SendAudioMessageRequested({required super.roomIds});
}

final class FileCompressionChangeRequested extends SendingHandlerEvent {
  final bool isEnabled;

  const FileCompressionChangeRequested({required this.isEnabled, required super.roomIds});

  @override
  List<Object> get props => [isEnabled];
}
