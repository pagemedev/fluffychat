import 'dart:async';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:async/async.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/widgets/local_notifications_extension.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config/flavor_config.dart';
import '../../pages/chat/chat.dart';
import '../../utils/platform_infos.dart';
import '../../widgets/matrix.dart';
import '../file_handler/file_handler_bloc.dart';

part 'sending_handler_event.dart';
part 'sending_handler_state.dart';

class CancellationToken {
  bool isCancellationRequested = false;
}

class SendingHandlerBloc extends Bloc<SendingHandlerEvent, SendingHandlerState> {
  int sendingId = 0;
  final Client client;
  final BuildContext context;
  CancellationToken _cancellationToken = CancellationToken();
  CancelableOperation? _currentOperation;

  Timer? _storeInputTimeoutTimer;
  static const Duration _storeInputTimeout = Duration(milliseconds: 500);

  SendingHandlerBloc({required this.client, required this.context}) : super(const SendingHandlerInitial()) {
    on<SendTextMessageRequested>(_onSendTextMessageRequested);
    on<SendImagesRequested>(_onSendImagesRequested);
    on<SendFilesRequested>(_onSendFilesRequested);
    on<SendQuillMessageRequested>(_onSendQuillMessageRequested);
    on<SendShareEventRequested>(_onSendShareEventRequested);
    on<CancelSendEvent>(_onCancelSendEvent);
    on<FileCompressionChangeRequested>(_onFileCompressionChangeRequested);
  }

  void _onFileCompressionChangeRequested(FileCompressionChangeRequested event, Emitter<SendingHandlerState> emit) async {
    Logs().v('Received a FileFullResolutionRequested event');
    try {
      emit(SendingHandlerInitial(isCompressionEnabled: event.isEnabled));
      Logs().v('FileFullResolutionRequested successfully completed.');
    } catch (e) {
      Logs().e('Error occurred during FileFullResolutionRequested: $e');
      emit(SendingHandlerError(e.toString(), sendingId)); // use e.toString() instead of message
    }
  }

  Future<void> _onCancelSendEvent(CancelSendEvent event, Emitter<SendingHandlerState> emit) async {
    emit(SendingHandlerCancelled(event.roomIds.length, sendingId));
    // Cancels the operation
    return _currentOperation?.cancel() ?? Future.value();
  }

  Future<void> _onSendImagesRequested(
    SendImagesRequested event,
    Emitter<SendingHandlerState> emit,
  ) async {
    final sendingId = this.sendingId++;
    Logs().v("_onSendImagesRequested - state.isCompressionEnabled: ${state.isCompressionEnabled}");
    if (event.images.isEmpty) {
      emit(SendingHandlerError('Current images was empty', sendingId));
      return;
    }
    if (event.roomIds.length == 1) {
      context.go('/rooms/${event.roomIds.first}');
    }

    int roomIndex = 0;
    for (final String roomId in event.roomIds) {
      int imageIndex = 1;

      final room = client.getRoomById(roomId);
      if (room == null) {
        Logs().e('SendingHandlerBloc(_onSendTextMessageRequested): Could not get room from roomId');
        break;
      }
      emit(
        SendingImagesInProgress(
            sendingId: sendingId,
            totalRooms: event.roomIds.length,
            sentRooms: roomIndex,
            totalItemsInCurrentRoom: event.images.length,
            sentItemsInCurrentRoom: imageIndex,
            isChatListView: PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'),
      );

      for (final ImageState image in event.images) {
        await room.sendFileEvent(
          state.isCompressionEnabled ? image.compressedImageFile : image.imageFile,
          thumbnail: image.thumbnailFile,
        );
        imageIndex++;
        emit(SendingImagesInProgress(
            sendingId: sendingId,
            totalRooms: event.roomIds.length,
            sentRooms: roomIndex,
            totalItemsInCurrentRoom: event.images.length,
            sentItemsInCurrentRoom: imageIndex,
            isChatListView: PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'));
      }
      roomIndex++;
    }
    emit(SendingFileHandlerComplete(event.roomIds.length, sendingId));
  }

  Future<void> _onSendFilesRequested(
    SendFilesRequested event,
    Emitter<SendingHandlerState> emit,
  ) async {
    final sendingId = this.sendingId++;
    Logs().v("_onSendFilesRequested - state.isCompressionEnabled: ${state.isCompressionEnabled}");
    // Check if there's an ongoing operation
    if (_currentOperation != null && !_currentOperation!.isCompleted) {
      Logs().v('An operation is already in progress. Please wait or cancel it before starting a new one.');
      emit(SendingHandlerError('An operation is already in progress. Please wait or cancel it before starting a new one.', sendingId));
      return;
    }

    // Reset CancellationToken
    _cancellationToken.isCancellationRequested = false;

    if (event.files.isEmpty) {
      emit(SendingHandlerError('Current files was empty', sendingId));
      return;
    }
    // Wrap the operation with CancelableOperation
    _currentOperation = CancelableOperation.fromFuture(_performSendFiles(event, emit), onCancel: () async {
      Logs().v('_onSendFilesRequested - onCancel called');
      _cancellationToken.isCancellationRequested = true;
    });

    try {
      await _currentOperation!.value;
    } catch (error) {
      Logs().v('_onSendFilesRequested - error $error');
      _currentOperation?.cancel();
    }
  }

  Future<void> _performSendFiles(SendFilesRequested event, Emitter<SendingHandlerState> emit) async {
    int roomIndex = 0;
    for (final String roomId in event.roomIds) {
      if (_cancellationToken.isCancellationRequested) {
        _currentOperation = null;
        Logs().v('[SendingHandlerBloc] _performSendFiles - roomId level onCancel called');
        break;
      }
      int fileIndex = 0;
      final room = client.getRoomById(roomId);
      if (room == null) {
        Logs().e('[SendingHandlerBloc] _onSendTextMessageRequested: Could not get room from roomId');
        break;
      }
      emit(SendingFilesInProgress(
          sendingId: sendingId,
          totalRooms: event.roomIds.length,
          sentRooms: roomIndex,
          totalItemsInCurrentRoom: event.files.length,
          sentItemsInCurrentRoom: fileIndex,
          isCompressionEnabled: state.isCompressionEnabled,
          isChatListView: PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'));

      for (final FileState file in event.files) {
        if (_cancellationToken.isCancellationRequested) {
          Logs().v('[SendingHandlerBloc] _performSendFiles - file level onCancel called');
          _currentOperation = null;
          //emit(SendingHandlerComplete(event.roomIds.length));
          break;
        }

        // Update the progress
        emit(SendingFilesInProgress(
            sendingId: sendingId,
            totalRooms: event.roomIds.length,
            sentRooms: roomIndex,
            totalItemsInCurrentRoom: event.files.length,
            sentItemsInCurrentRoom: fileIndex,
            isCompressionEnabled: state.isCompressionEnabled,
            isChatListView: PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'));

        Logs().v("[SendingHandlerBloc] _performSendFiles - state.isCompressionEnabled: ${state.isCompressionEnabled}");
        if (state.isCompressionEnabled) {
          Logs().v("[SendingHandlerBloc] Sending compressed image");
          await room.sendFileEvent(
            (file is ImageState) ? file.compressedImageFile : file.file,
            thumbnail: file.thumbnailFile,
          );
        } else {
          Logs().v("[SendingHandlerBloc] Sending full image");
          await room.sendFileEvent(
            (file is ImageState) ? file.imageFile : file.file,
            thumbnail: file.thumbnailFile,
          );
        }

        fileIndex++;
      }
      roomIndex++;
    }
    emit(SendingFileHandlerComplete(event.roomIds.length, sendingId));
    _currentOperation = null;
  }

  Future<void> removeDraftMessage(Room room) async {
    _storeInputTimeoutTimer?.cancel();
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('draft_${room.id}');
  }

  Future<void> _onSendTextMessageRequested(
    SendTextMessageRequested event,
    Emitter<SendingHandlerState> emit,
  ) async {
    final isChatListView = PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms';
    final sendingId = this.sendingId++;
    if (event.currentText.isEmpty) {
      emit(SendingHandlerError('Current text was empty', sendingId));
      return;
    }
    int roomIndex = 0;
    emit(
      SendingTextInProgress(
          sendingId: sendingId,
          totalRooms: event.roomIds.length,
          sentRooms: roomIndex,
          totalItemsInCurrentRoom: 1,
          sentItemsInCurrentRoom: 1,
          isChatListView: PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'),
    );

    for (final String roomId in event.roomIds) {
      final Room? room = client.getRoomById(roomId);
      try {
        if (room == null) {
          Logs().e('SendingHandlerBloc(_onSendTextMessageRequested): Could not get room from roomId');
          break;
        }
        await removeDraftMessage(room);
        await room.sendTextEvent(
          event.currentText,
          inReplyTo: event.replyEvent,
          editEventId: event.editedEventId,
          parseMarkdown: true,
          parseCommands: await parseCommands(
            context: context,
            room: room,
            currentText: event.currentText,
          ),
        );
        roomIndex++;

        emit(
          SendingTextInProgress(
            sendingId: sendingId,
            totalRooms: event.roomIds.length,
            sentRooms: roomIndex,
            totalItemsInCurrentRoom: 1,
            sentItemsInCurrentRoom: 1,
            isChatListView: isChatListView,
          ),
        );
      } catch (e) {
        Logs().e("[SendingHandlerBloc] _onSendTextMessageRequested Error: $e");
      }
    }
    emit(SendingHandlerComplete(event.roomIds.length, sendingId));
  }

  Future<void> _onSendShareEventRequested(
    SendShareEventRequested event,
    Emitter<SendingHandlerState> emit,
  ) async {
    final sendingId = this.sendingId++;
    if (event.roomIds.isEmpty) {
      emit(SendingHandlerError('RoomIds was empty', sendingId));
      return;
    }

    if (event.shareEventState.isEmpty) {
      emit(SendingHandlerError('Share events was empty', sendingId));
      return;
    }

    int roomIndex = 0;
    int shareEventIndex = 0;
    emit(SendingTextInProgress(
        sendingId: sendingId,
        totalRooms: event.roomIds.length,
        sentRooms: roomIndex,
        totalItemsInCurrentRoom: event.shareEventState.length,
        sentItemsInCurrentRoom: shareEventIndex,
        isChatListView: PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'));

    for (final String roomId in event.roomIds) {
      final room = client.getRoomById(roomId);
      if (room == null) {
        Logs().e('SendingHandlerBloc(_onSendTextMessageRequested): Could not get room from roomId');
        break;
      }
      for (final event in event.shareEventState) {
        await room.sendEvent(
          event,
        );
        shareEventIndex++;
      }
      roomIndex++;
      emit(SendingTextInProgress(
          sendingId: sendingId,
          totalRooms: event.roomIds.length,
          sentRooms: roomIndex,
          totalItemsInCurrentRoom: 1,
          sentItemsInCurrentRoom: 1,
          isChatListView: PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'));
    }
    emit(SendingHandlerComplete(event.roomIds.length, sendingId));
  }

  Future<void> _onSendQuillMessageRequested(
    SendQuillMessageRequested event,
    Emitter<SendingHandlerState> emit,
  ) async {
    if (event.currentText.isEmpty) {
      emit(SendingHandlerError('Current text was empty', sendingId));
      return;
    }
    int roomIndex = 0;
    emit(SendingTextInProgress(
        sendingId: sendingId,
        totalRooms: event.roomIds.length,
        sentRooms: roomIndex,
        totalItemsInCurrentRoom: 1,
        sentItemsInCurrentRoom: 1,
        isChatListView: PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'));
    for (final String roomId in event.roomIds) {
      final room = client.getRoomById(roomId);
      if (room == null) {
        Logs().e('SendingHandlerBloc(_onSendQuillMessageRequested): Could not get room from roomId');
        break;
      }
      room.sendEvent(
        {
          'serialized_delta': event.currentText,
          'msgtype': CustomMessageTypes.quillText,
          'body':
              '${await client.getProfileFromUserId(client.userID!).then((value) => value.displayName ?? value.userId)} sent you a composed message.\n\nYou currently unable to read this as your app version does not support this feature.\nUpdate to see these types of messages.\n\nhttps://play.google.com/store/apps/details?id=com.pageme.business&pli=1',
        },
        inReplyTo: event.replyEvent,
        editEventId: event.editedEventId,
      );
      roomIndex++;
      emit(SendingTextInProgress(
          sendingId: sendingId,
          totalRooms: event.roomIds.length,
          sentRooms: roomIndex,
          totalItemsInCurrentRoom: 1,
          sentItemsInCurrentRoom: 1,
          isChatListView: PageMeApp.router.routeInformationProvider.value.uri.path == '/rooms'));
    }
    emit(SendingHandlerComplete(event.roomIds.length, sendingId));
  }

  Future<bool> parseCommands({
    required BuildContext context,
    required Room room,
    required String currentText,
  }) async {
    var parseCommands = true;
    final commandMatch = RegExp(r'^\/(\w+)').firstMatch(currentText);
    if (commandMatch != null && !room.client.commands.keys.contains(commandMatch[1]!.toLowerCase())) {
      final l10n = L10n.of(context)!;
      final dialogResult = await showOkCancelAlertDialog(
        context: context,
        useRootNavigator: false,
        title: l10n.commandInvalid,
        message: l10n.commandMissing(commandMatch[0]!),
        okLabel: l10n.sendAsText,
        cancelLabel: l10n.cancel,
      );
      if (dialogResult == OkCancelResult.cancel) return parseCommands;
      parseCommands = false;
    }
    return parseCommands;
  }
}
