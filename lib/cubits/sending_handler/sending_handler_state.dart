part of 'sending_handler_bloc.dart';

enum SendingHandlerStatus { initial, sending, sent, failure }

extension SendingHandlerStatusX on SendingHandlerState {
  bool get isSending => this is SendingHandlerInProgress;
  bool get isSent => this is SendingHandlerComplete;
  bool get isError => this is SendingHandlerError;
  bool get isCancelled => this is SendingHandlerCancelled;
}

abstract class SendingHandlerState extends Equatable {
  const SendingHandlerState({
    this.isCompressionEnabled = true,
  });
  final bool isCompressionEnabled;

  @override
  List<Object?> get props => [isCompressionEnabled];

  SendingHandlerState copyWith({bool? isCompressionEnabled});
}

class SendingHandlerInitial extends SendingHandlerState {
  final int totalRooms;

  const SendingHandlerInitial({this.totalRooms = 0, super.isCompressionEnabled = true});

  @override
  List<Object> get props => [totalRooms, super.isCompressionEnabled];

  @override
  SendingHandlerInitial copyWith({
    int? totalRooms,
    bool? isCompressionEnabled,
  }) {
    return SendingHandlerInitial(totalRooms: totalRooms ?? this.totalRooms, isCompressionEnabled: isCompressionEnabled ?? this.isCompressionEnabled);
  }
}

abstract class SendingHandlerInProgress extends SendingHandlerState {
  final int sendingId;
  final int totalRooms;
  final int sentRooms;
  final int totalItemsInCurrentRoom;
  final int sentItemsInCurrentRoom;
  final String statusMessage;
  final bool isChatListView;

  const SendingHandlerInProgress(
      {required this.sendingId,
      required this.totalRooms,
      required this.sentRooms,
      required this.totalItemsInCurrentRoom,
      required this.sentItemsInCurrentRoom,
      required this.statusMessage,
      required this.isChatListView,
      super.isCompressionEnabled});

  @override
  List<Object?> get props =>
      [sendingId, totalRooms, sentRooms, totalItemsInCurrentRoom, sentItemsInCurrentRoom, statusMessage, isCompressionEnabled, isChatListView, super.isCompressionEnabled];

  @override
  SendingHandlerInProgress copyWith({
    bool? isCompressionEnabled,
    int? totalRooms,
    int? sentRooms,
    int? totalItemsInCurrentRoom,
    int? sentItemsInCurrentRoom,
    bool? isChatListView,
  });
}

class SendingFilesInProgress extends SendingHandlerInProgress {
  SendingFilesInProgress(
      {required super.totalRooms,
      required super.sendingId,
      required super.sentRooms,
      required super.totalItemsInCurrentRoom,
      required super.sentItemsInCurrentRoom,
      required super.isChatListView,
      super.isCompressionEnabled})
      : super(
          statusMessage: _buildStatusMessage(
            totalRooms,
            sentRooms,
            totalItemsInCurrentRoom,
            sentItemsInCurrentRoom,
            isChatListView,
          ),
        );

  static String _buildStatusMessage(
    int totalRooms,
    int sentRooms,
    int totalItemsInCurrentRoom,
    int sentItemsInCurrentRoom,
    bool isChatListView,
  ) {
    String statusMessage = '';

    if (totalItemsInCurrentRoom > 1 || totalRooms == 1) {
      statusMessage += 'Sending file ${sentItemsInCurrentRoom + 1}/$totalItemsInCurrentRoom';
    } else if (totalItemsInCurrentRoom == 1) {
      statusMessage += 'Sending file';
    }

    if (totalRooms > 1 && isChatListView) {
      statusMessage += ', to room ${sentRooms + 1}/$totalRooms. ';
    }

    return statusMessage;
  }

  @override
  SendingFilesInProgress copyWith(
      {bool? isCompressionEnabled, int? totalRooms, int? sentRooms, int? totalItemsInCurrentRoom, int? sentItemsInCurrentRoom, bool? isChatListView}) {
    return SendingFilesInProgress(
      totalRooms: totalRooms ?? this.totalRooms,
      sentRooms: sentRooms ?? this.sentRooms,
      totalItemsInCurrentRoom: totalItemsInCurrentRoom ?? this.totalItemsInCurrentRoom,
      sentItemsInCurrentRoom: sentItemsInCurrentRoom ?? this.sentItemsInCurrentRoom,
      isChatListView: isChatListView ?? this.isChatListView,
      isCompressionEnabled: isCompressionEnabled ?? this.isCompressionEnabled,
      sendingId: sendingId,
    );
  }
}

class SendingImagesInProgress extends SendingHandlerInProgress {
  SendingImagesInProgress(
      {required super.totalRooms,
      required super.sendingId,
      required super.sentRooms,
      required super.totalItemsInCurrentRoom,
      required super.sentItemsInCurrentRoom,
      required super.isChatListView,
      super.isCompressionEnabled})
      : super(
          statusMessage: _buildStatusMessage(
            totalRooms,
            sentRooms,
            totalItemsInCurrentRoom,
            sentItemsInCurrentRoom,
            isChatListView,
          ),
        );

  static String _buildStatusMessage(
    int totalRooms,
    int sentRooms,
    int totalItemsInCurrentRoom,
    int sentItemsInCurrentRoom,
    bool isChatListView,
  ) {
    String statusMessage = '';

    if (totalRooms > 1 && isChatListView) {
      statusMessage += 'Sending to room ${sentRooms + 1} out of $totalRooms. ';
    }

    if (totalItemsInCurrentRoom > 1 || totalRooms == 1) {
      statusMessage += 'Sending image ${sentItemsInCurrentRoom + 1} out of $totalItemsInCurrentRoom';
    } else if (totalItemsInCurrentRoom == 1) {
      statusMessage += 'Sending image.';
    }

    return statusMessage;
  }

  @override
  SendingImagesInProgress copyWith({
    bool? isCompressionEnabled,
    int? totalRooms,
    int? sentRooms,
    int? totalItemsInCurrentRoom,
    int? sentItemsInCurrentRoom,
    bool? isChatListView,
  }) {
    return SendingImagesInProgress(
        totalRooms: totalRooms ?? this.totalRooms,
        sentRooms: sentRooms ?? this.sentRooms,
        totalItemsInCurrentRoom: totalItemsInCurrentRoom ?? this.totalItemsInCurrentRoom,
        sentItemsInCurrentRoom: sentItemsInCurrentRoom ?? this.sentItemsInCurrentRoom,
        isChatListView: isChatListView ?? this.isChatListView,
        isCompressionEnabled: isCompressionEnabled ?? this.isCompressionEnabled,
        sendingId: sendingId);
  }
}

class SendingTextInProgress extends SendingHandlerInProgress {
  SendingTextInProgress(
      {required super.totalRooms,
      required super.sendingId,
      required super.sentRooms,
      required super.totalItemsInCurrentRoom,
      required super.sentItemsInCurrentRoom,
      required super.isChatListView,
      super.isCompressionEnabled})
      : super(
          statusMessage: _buildStatusMessage(
            totalRooms,
            sentRooms,
            totalItemsInCurrentRoom,
            sentItemsInCurrentRoom,
            isChatListView,
          ),
        );

  static String _buildStatusMessage(
    int totalRooms,
    int sentRooms,
    int totalItemsInCurrentRoom,
    int sentItemsInCurrentRoom,
    bool isChatListView,
  ) {
    String statusMessage = '';

    if (totalRooms > 1 && isChatListView) {
      statusMessage += 'Sending to room ${sentRooms + 1} out of $totalRooms...';
    }

    if (totalItemsInCurrentRoom > 1 || totalRooms == 1) {
      statusMessage += 'Sending message ${sentItemsInCurrentRoom + 1} out of $totalItemsInCurrentRoom...';
    } else if (totalItemsInCurrentRoom == 1) {
      statusMessage += 'Sending message...';
    }

    return statusMessage;
  }

  @override
  SendingTextInProgress copyWith({
    bool? isCompressionEnabled,
    int? totalRooms,
    int? sentRooms,
    int? totalItemsInCurrentRoom,
    int? sentItemsInCurrentRoom,
    bool? isChatListView,
  }) {
    return SendingTextInProgress(
        totalRooms: totalRooms ?? this.totalRooms,
        sentRooms: sentRooms ?? this.sentRooms,
        totalItemsInCurrentRoom: totalItemsInCurrentRoom ?? this.totalItemsInCurrentRoom,
        sentItemsInCurrentRoom: sentItemsInCurrentRoom ?? this.sentItemsInCurrentRoom,
        isChatListView: isChatListView ?? this.isChatListView,
        isCompressionEnabled: isCompressionEnabled ?? this.isCompressionEnabled,
        sendingId: sendingId);
  }
}

class SendingHandlerComplete extends SendingHandlerState {
  final int totalRooms;
  final int sendingId;

  const SendingHandlerComplete(this.totalRooms, this.sendingId, {super.isCompressionEnabled = true});

  @override
  List<Object?> get props => [totalRooms, super.isCompressionEnabled, sendingId];

  @override
  SendingHandlerComplete copyWith({bool? isCompressionEnabled, int? totalRooms}) {
    return SendingHandlerComplete(
      totalRooms ?? this.totalRooms,
      sendingId,
      isCompressionEnabled: isCompressionEnabled ?? this.isCompressionEnabled,
    );
  }
}

class SendingFileHandlerComplete extends SendingHandlerComplete {
  const SendingFileHandlerComplete(super.totalRooms, super.sendingId);


  @override
  List<Object?> get props => [totalRooms, super.isCompressionEnabled, sendingId];

  @override
  SendingFileHandlerComplete copyWith({bool? isCompressionEnabled, int? totalRooms}) {
    return SendingFileHandlerComplete(
      totalRooms ?? this.totalRooms,
      sendingId,
    );
  }
}

class SendingHandlerCancelled extends SendingHandlerState {
  final int totalRooms;
  final int sendingId;
  final String statusMessage = "Cancelling the remaining files";

  const SendingHandlerCancelled(this.totalRooms, this.sendingId, {super.isCompressionEnabled = true});

  @override
  List<Object?> get props => [totalRooms, sendingId,  super.isCompressionEnabled];

  @override
  SendingHandlerCancelled copyWith({bool? isCompressionEnabled, int? totalRooms}) {
    return SendingHandlerCancelled(
      totalRooms ?? this.totalRooms,
      sendingId,
      isCompressionEnabled: isCompressionEnabled ?? this.isCompressionEnabled,
    );
  }
}

class SendingFileHandlerCancelled extends SendingHandlerCancelled{
  const SendingFileHandlerCancelled(super.totalRooms, super.sendingId);

  @override
  List<Object?> get props => [totalRooms, sendingId,  super.isCompressionEnabled];
}

class SendingHandlerError extends SendingHandlerState {
  final String message;
  final int sendingId;
  const SendingHandlerError(this.message, this.sendingId, {super.isCompressionEnabled = true});

  @override
  List<Object?> get props => [message, super.isCompressionEnabled, sendingId];

  @override
  SendingHandlerError copyWith({bool? isCompressionEnabled, String? message}) {
    return SendingHandlerError(
      message ?? this.message,
      sendingId,
      isCompressionEnabled: isCompressionEnabled ?? this.isCompressionEnabled,
    );
  }
}

class SendingFileHandlerError extends SendingHandlerError{
  const SendingFileHandlerError(super.message, super.sendingId);

  @override
  List<Object?> get props => [message, super.isCompressionEnabled, sendingId];
}
