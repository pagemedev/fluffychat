import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:matrix/matrix.dart';
part 'search_handler_state.dart';

class SearchHandlerCubit extends Cubit<SearchHandlerState> {
  final Client client;
  bool _isSearching = false;
  bool get isSearching => _isSearching;
  StreamSubscription<List<Event>>? _searchSubscription;

  SearchHandlerCubit(this.client) : super(SearchHandlerInitial());

  @override
  Future<void> close() async {
    await completeSearch();
    await super.close();
  }

  @override
  void onChange(Change<SearchHandlerState> change) {
    if (change is SearchHandlerResultSuccess) {
      Logs().i(
          "[SearchHandlerCubit] onChange - currentState: ${(change.currentState as SearchHandlerResultSuccess).events.length}, nextState: ${(change.nextState as SearchHandlerResultSuccess).events.length}");
    } else {
      Logs().i("[SearchHandlerCubit] onChange - currentState: ${change.currentState.runtimeType}, nextState: ${change.nextState.runtimeType}");
    }

    super.onChange(change);
  }

  void initializeNewSearch() {
    emit(SearchHandlerInitial());
  }

  void search({required String searchTerm, required Room room, String? lastEventId, Timeline? timeline}) {
    emit(SearchHandlerLoading());
    if (room.encrypted && timeline != null) {
      encryptedSearch(searchTerm: searchTerm, timeline: timeline);
    } else {
      unencryptedSearch(searchTerm: searchTerm, room: room);
    }
  }

  void encryptedSearch({required String searchTerm, required Timeline timeline, String? lastEventId}) async {
    if (_isSearching) {
      // Handle the case when a search is already in progress.
      // For example, you can return or cancel the ongoing search:
      // cancelSearch();
      return;
    }
    _isSearching = true;
    Logs().v("[SearchHandlerCubit] Starting search in room: ${timeline.room.name}");
    emit(SearchHandlerLoading());
    // Subscribe to the searchEvent stream
    final searchStream = timeline.searchEvent(
        searchTerm: searchTerm,
        requestHistoryCount: 100, // Adjust as needed
        maxHistoryRequests: 10,
        sinceEventId: lastEventId // Adjust as needed
        );

    _searchSubscription = searchStream.listen(
      (newEvents) {
        if (newEvents.isNotEmpty) {
          Logs().v("[SearchHandlerCubit] New events found: ${newEvents.length}");
          emit(SearchHandlerResultSuccess(events: List.from(newEvents)));
        }
      },
      onDone: () {

        completeSearch();
      },
      onError: (error) {
        Logs().e("[SearchHandlerCubit] Error - $error");
        emit(SearchHandlerError(error.toString()));
        cancelSearch();
      },
    );
  }

  void unencryptedSearch({required String searchTerm, required Room room}) async {
    emit(SearchHandlerLoading());
    try {
      Logs().v("[SearchHandlerCubit] Searching for $searchTerm in ${room.name}");
      final searchResult = await client.search(
        Categories(
          roomEvents: RoomEventsCriteria(
            searchTerm: searchTerm,
            filter: SearchFilter(
              rooms: [room.id],
              notSenders: ["@whatsappbot:finmon.pageme.co.za"],
            ),
          ),
        ),
      );
      Logs().v("[SearchHandlerCubit] Compiling results into events");
      // Fetching each Event object based on the eventId
      Logs().v("[SearchHandlerCubit] Search Results as json: ${searchResult.toJson()}");
      final List<Event> events = [];
      for (final Result result in searchResult.searchCategories.roomEvents?.results ?? []) {
        Logs().v("[SearchHandlerCubit] ${result.result!.content}");

        Logs().v("[SearchHandlerCubit] [Room ${room.name}] ${result.result!.content}");
        final Event? event = await room.getEventById(result.result!.eventId);
        if (event != null) {
          events.add(event);
        } else {
          Logs().v("[SearchHandlerCubit] [Room ${room.name}] event was null for eventId ${result.result!.eventId}");
        }
      }
      Logs().v("[SearchHandlerCubit] Emitting SearchHandlerResultSuccess, number of events: ${events.length}");
      emit(SearchHandlerResultSuccess(
        events: events,
      ));
    } catch (e) {
      Logs().e("[SearchHandlerCubit] Error - $e");
      emit(SearchHandlerError(e.toString()));
    }
  }

  Future<void> cancelSearch() async {
    Logs().v("[SearchHandlerCubit] Cancel search called.");
    if (_searchSubscription != null && _isSearching) {
      _isSearching = false;
      await _searchSubscription!.cancel();
      _searchSubscription = null;
      if (state is! SearchHandlerResultSuccess){
        emit(const SearchHandlerComplete(events: []));
      }else{
        emit(SearchHandlerComplete(events: (state as SearchHandlerResultSuccess).events));
      }
      Logs().v("[SearchHandlerCubit] Search canceled.");
    }
  }

  Future<void> completeSearch() async {
    Logs().v("[SearchHandlerCubit] Complete search called.");
    if (_searchSubscription != null && _isSearching) {
      _isSearching = false;
      await _searchSubscription!.cancel();
      _searchSubscription = null;
      if (state is! SearchHandlerResultSuccess){
        emit(const SearchHandlerComplete(events: []));
      }else{
        emit(SearchHandlerComplete(events: (state as SearchHandlerResultSuccess).events));
      }
      Logs().v("[SearchHandlerCubit] Search completed.");
    }
  }
}
