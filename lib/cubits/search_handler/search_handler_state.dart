part of 'search_handler_cubit.dart';

abstract class SearchHandlerState extends Equatable {
  const SearchHandlerState();
}

class SearchHandlerInitial extends SearchHandlerState {
  @override
  List<Object> get props => [];
}

class SearchHandlerLoading extends SearchHandlerState {
  @override
  List<Object> get props => [];
}

class SearchHandlerResultSuccess extends SearchHandlerState {
  final List<Event> events;
  const SearchHandlerResultSuccess({required this.events,});

  @override
  List<Object?> get props => [events, ];

  @override
  bool operator ==(Object other) {
    return other is SearchHandlerResultSuccess &&
        listEquals(other.events, events) && other.events.length == events.length;
  }

  @override
  int get hashCode => events.hashCode;
}

class SearchHandlerComplete extends SearchHandlerResultSuccess{
  const SearchHandlerComplete({required super.events});

  @override
  bool operator ==(Object other) {
    return other is SearchHandlerComplete &&
        listEquals(other.events, events) && other.events.length == events.length;
  }

  @override
  List<Object?> get props => [events, ];

  @override
  int get hashCode => events.hashCode;
}

/*
class SearchHandlerComplete extends SearchHandlerState {
  final List<Event> events;
  const SearchHandlerComplete({required this.events,});

  @override
  List<Object?> get props => [events, ];

  @override
  bool operator ==(Object other) {
    return other is SearchHandlerComplete &&
        listEquals(other.events, events) && other.events.length == events.length;
  }

  @override
  int get hashCode => events.hashCode;
}
*/

class SearchHandlerError extends SearchHandlerState {
  final String message;
  const SearchHandlerError(this.message);

  @override
  List<Object?> get props => [message];
}