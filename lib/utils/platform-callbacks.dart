import 'package:flutter/services.dart';

const _channel = MethodChannel('callbacks');
typedef MultiUseCallback = void Function(dynamic msg);
typedef CancelListening = void Function();
int _nextCallbackId = 0;
Map<int, MultiUseCallback> _callbacksById = {};

Future<void> _methodCallHandler(MethodCall call) async {
  switch (call.method) {
    case 'callListener':
      _callbacksById[call.arguments["id"]]!(call.arguments["args"]);
      break;
    default:
      print(
          'TestFairy: Ignoring invoke from native. This normally shouldn\'t happen.');
  }
}
Future<CancelListening> startListening(MultiUseCallback callback) async {
  _channel.setMethodCallHandler(_methodCallHandler);
  final int currentListenerId = _nextCallbackId++;
  _callbacksById[currentListenerId] = callback;
  await _channel.invokeMethod("startListening", currentListenerId);
  return () {
    _channel.invokeMethod("cancelListening", currentListenerId);
    _callbacksById.remove(currentListenerId);
  };
}
//Use the code below before registering callbacks to be able to listen calls from the native side
//_channel.setMethodCallHandler(_methodCallHandler);

