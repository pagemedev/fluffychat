import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/basic.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/subscriptions/subscriptions_cubit.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:pageMe/widgets/matrix.dart';

extension UiaRequestManager on MatrixState {
  Future uiaRequestHandler(UiaRequest uiaRequest) async {
    final L10n l10n;

    if (PlatformInfos.canSubscribe) {
      if (navigatorContext.mounted) {
        Logs().i('The context is mounted');

        l10n = L10n.of(navigatorContext)!;
        subscriptionsCubit ??= BlocProvider.of<SubscriptionsCubit>(navigatorContext);
      } else {
        Logs().i('The context is NOT mounted');
        l10n = L10n.of(context)!;
        subscriptionsCubit ??= BlocProvider.of<SubscriptionsCubit>(context);
      }
    } else {
      if (navigatorContext.mounted) {
        Logs().i('The context is mounted');
        l10n = L10n.of(navigatorContext)!;
      } else {
        Logs().i('The context is NOT mounted');
        l10n = L10n.of(context)!;
      }
    }
    try {
      if (uiaRequest.state != UiaRequestState.waitForUser || uiaRequest.nextStages.isEmpty) {
        Logs().d('Uia Request Stage: ${uiaRequest.state}');
        return;
      }
      final stage = uiaRequest.nextStages.first;
      Logs().d('Uia Request Stage: $stage');
      switch (stage) {
        case AuthenticationTypes.password:
          final input = cachedPassword ??
              (await showTextInputDialog(
                context: context,
                title: "Please enter your ${FlavorConfig.applicationName} password",
                okLabel: l10n.ok,
                cancelLabel: l10n.cancel,
                textFields: [
                  const DialogTextField(
                    minLines: 1,
                    maxLines: 1,
                    obscureText: true,
                    hintText: '******',
                  )
                ],
              ))
                  ?.single;
          if (input == null || input.isEmpty) {
            return uiaRequest.cancel();
          }
          return uiaRequest.completeStage(
            AuthenticationPassword(
              session: uiaRequest.session,
              password: input,
              identifier: AuthenticationUserIdentifier(user: client.userID!),
            ),
          );
        case AuthenticationTypes.emailIdentity:
          final currentThreepidCreds = this.currentThreepidCreds;
          if (currentThreepidCreds == null) {
            return uiaRequest.cancel(
              UiaException(L10n.of(context)!.serverRequiresEmail),
            );
          }
          final auth = AuthenticationThreePidCreds(
            session: uiaRequest.session,
            type: AuthenticationTypes.emailIdentity,
            threepidCreds: ThreepidCreds(
              sid: currentThreepidCreds.sid,
              clientSecret: currentClientSecret,
            ),
          );
          if (OkCancelResult.ok ==
              await showOkCancelAlertDialog(
                useRootNavigator: false,
                context: navigatorContext,
                title: l10n.weSentYouAnEmail,
                message: l10n.pleaseClickOnLink,
                okLabel: l10n.iHaveClickedOnLink,
                cancelLabel: l10n.cancel,
              )) {
            return uiaRequest.completeStage(auth);
          }
          return uiaRequest.cancel();
        case AuthenticationTypes.dummy:
          return uiaRequest.completeStage(
            AuthenticationData(
              type: AuthenticationTypes.dummy,
              session: uiaRequest.session,
            ),
          );
        default:
          final actions = [
            const SheetAction(
              key: UIAAction.proceed,
              label: "Proceed",
              isDefaultAction: true,
              icon: Icons.arrow_forward,
            ),
            const SheetAction(
              key: UIAAction.cancel,
              label: "Cancel",
              icon: Icons.cancel,
            ),
          ];
          final url = Uri.parse('${client.homeserver}/_matrix/client/r0/auth/$stage/fallback/web?session=${uiaRequest.session}');

          launch(
            url.toString(),
            forceSafariVC: true,
            forceWebView: false,
          );
          if (UIAAction.proceed ==
              await showModalActionSheet<UIAAction>(
                  isDismissible: false,
                  onWillPop: () async {
                    return await Future.value(false);
                  },
                  useRootNavigator: false,
                  context: navigatorContext.mounted ? navigatorContext : context,
                  title: "Lets check if you're a human",
                  message: "Follow the instruction on the webpage, then press 'Proceed'",
                  actions: actions,
                  builder: (context, widget) {
                    return ClipRRect(
                      borderRadius: const BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                      child: Container(
                        decoration: const BoxDecoration(borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10))),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            //webView(),
                            widget,
                          ],
                        ),
                      ),
                    );
                  })) {
            if (PlatformInfos.canSubscribe) {
              await subscriptionsCubit!.purchaseProduct();
            }
            return uiaRequest.completeStage(
              AuthenticationData(session: uiaRequest.session),
            );
          } else {
            return uiaRequest.cancel();
          }
      }
    } catch (e, s) {
      Logs().e('Error while background UIA', e, s);
      return uiaRequest.cancel(e is Exception ? e : Exception(e));
    }
  }
}

class UiaException implements Exception {
  final String reason;

  UiaException(this.reason);

  @override
  String toString() => reason;
}

enum UIAAction { proceed, cancel }
