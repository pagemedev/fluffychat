import 'package:flutter/material.dart';

import '../widgets/context_menu/context_menu.dart';

abstract class GlobalKeyProvider{
  static  GlobalKey menuButton = GlobalKey(debugLabel: 'menuButton');
  static GlobalKey searchButton = GlobalKey(debugLabel: "searchButton");
  static GlobalKey fabButton = GlobalKey(debugLabel: "fabButton" );
  static GlobalKey settingsButton = GlobalKey(debugLabel: "settingsButton");
  static GlobalKey settingsIcon = GlobalKey(debugLabel: "settingsIcon");
  static GlobalKey settingsAppearanceButton = GlobalKey(debugLabel: "settingsAppearanceButton");
  static GlobalKey settingsLogOutButton = GlobalKey(debugLabel: "settingsLogOutButton");
  static GlobalKey newContactButton = GlobalKey(debugLabel: "newContactButton");
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>(debugLabel: "navigatorKey");
}