import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pdfx/pdfx.dart';

class PdfView extends StatefulWidget {
  final String pdfLink;
  final String pdfName;
  const PdfView({super.key, required this.pdfLink, required this.pdfName});

  @override
  State<PdfView> createState() => _PdfViewState();
}

class _PdfViewState extends State<PdfView> {
  static const int _initialPage = 1;
  late PdfControllerPinch _pdfControllerPinch;

  @override
  void initState() {
    _pdfControllerPinch = PdfControllerPinch(
      document: PdfDocument.openData(
        _downloadFile(widget.pdfLink, widget.pdfName),
      ),
      initialPage: _initialPage,
    );
    super.initState();
  }

  Future<Uint8List> _downloadFile(String url, String filename) async {
    Logs().v("File: $filename - $url");
    final httpClient = HttpClient();
    final request = await httpClient.getUrl(Uri.parse(url));
    final response = await request.close();
    final bytes = await consolidateHttpClientResponseBytes(response);
    return bytes;
  }

  @override
  void dispose() {
    _pdfControllerPinch.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: Text(widget.pdfName),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.navigate_before),
            onPressed: () async {
              await _pdfControllerPinch.previousPage(
                curve: Curves.ease,
                duration: const Duration(milliseconds: 100),
              );
            },
          ),
          PdfPageNumber(
            controller: _pdfControllerPinch,
            builder: (_, loadingState, page, pagesCount) => Container(
              alignment: Alignment.center,
              child: Text(
                '$page/${pagesCount ?? 0}',
                style: const TextStyle(fontSize: 22),
              ),
            ),
          ),
          IconButton(
            icon: const Icon(Icons.navigate_next),
            onPressed: () async {
              await _pdfControllerPinch.nextPage(
                curve: Curves.ease,
                duration: const Duration(milliseconds: 100),
              );
            },
          ),
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () async {
              await _pdfControllerPinch.loadDocument(PdfDocument.openData(
                _downloadFile(widget.pdfLink, widget.pdfName),
              ));
            },
          )
        ],
      ),
      body: PdfViewPinch(
        builders: PdfViewPinchBuilders<DefaultBuilderOptions>(
          options: const DefaultBuilderOptions(),
          documentLoaderBuilder: (_) => const Center(child: CircularProgressIndicator()),
          pageLoaderBuilder: (_) => const Center(child: CircularProgressIndicator()),
// ...

          errorBuilder: (_, error) {
            final String errorMessage = 'Something went wrong while loading the PDF.\nPlease try again.\n\n${widget.pdfLink}';

            return Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Spacer(),
                    Text(
                      errorMessage,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18),
                    ),
                    const SizedBox(height: 20),
                    Spacer(),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Theme.of(context).colorScheme.primaryContainer),
                      onPressed: () async {
                        await _pdfControllerPinch.loadDocument(PdfDocument.openData(
                          _downloadFile(widget.pdfLink, widget.pdfName),
                        ));
                      },
                      child: Text(
                        'Retry',
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.onPrimaryContainer,
                        ),
                      ),
                    ),
                    // Optionally, add a button to report the error
                    // ElevatedButton(
                    //   child: Text('Report this issue'),
                    //   onPressed: () {
                    //     // Code to report the error (e.g., sending logs to your server)
                    //   },
                    // ),
                  ],
                ),
              ),
            );
          },

// ...
        ),
        controller: _pdfControllerPinch,
      ),
    );
  }
}
