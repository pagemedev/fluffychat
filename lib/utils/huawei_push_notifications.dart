import 'dart:async';
import 'dart:convert';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:go_router/go_router.dart';
import 'package:huawei_push/huawei_push.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/push_helper.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:pageMe/utils/push_helper_new.dart';
import '../config/flavor_config.dart';
import '../config/setting_keys.dart';
import 'famedlysdk_store.dart';
import 'logger_functions.dart';
import 'matrix_sdk_extensions.dart/client_stories_extension.dart';

class HuaweiPushNotifications {
  static HuaweiPushNotifications? _instance;
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  Client client;
  BuildContext? context;
  String? _hcmToken;
  void Function(String errorMsg, {Uri? link})? onHcmError;
  L10n? l10n;
  Store? _store;
  final pendingTests = <String, Completer<void>>{};
  DateTime? lastReceivedPush;

  Store get store => _store ??= Store();
  Future<void> loadLocale() async {
    // inspired by _lookupL10n in .dart_tool/flutter_gen/gen_l10n/l10n.dart
    l10n ??= (context != null ? L10n.of(context!) : null) ?? (await L10n.delegate.load(ui.window.locale));
  }

  factory HuaweiPushNotifications.clientOnly(Client client) {
    _instance ??= HuaweiPushNotifications._init(client: client);
    return _instance!;
  }

  factory HuaweiPushNotifications(
    Client _client,
    BuildContext _context, {
    final void Function(String errorMsg, {Uri? link})? onHcmError,
  }) {
    final instance = HuaweiPushNotifications.clientOnly(_client);
    instance.context = _context;
    instance.onHcmError = onHcmError;
    instance.fullInit();
    return instance;
  }

  HuaweiPushNotifications._init({required this.client}) {
    onLogin ??= client.onLoginStateChanged.stream.listen(handleLoginStateChanged);
    onRoomSync ??= client.onSync.stream.where((s) => s.hasRoomUpdate).listen((s) => _onClearingPush(getFromServer: false));
    initPlatformState();
  }

  Future<void> fullInit() => setupPush();

  void handleLoginStateChanged(_) => setupPush();

  StreamSubscription<LoginState>? onLogin;
  StreamSubscription<SyncUpdate>? onRoomSync;

  void _newHcmToken(String token) {
    _hcmToken = token;
  }

  void showResult(
    String name, [
    String? msg = 'Button pressed.',
  ]) {
    msg ??= '';
    debugPrint('[' + name + ']' + ': ' + msg);
    Push.showToast('[' + name + ']: ' + msg);
  }

  void _onTokenEvent(String event) {
    if (_hcmToken != event) {
      _hcmToken = event;
      setupPush();
    }
    showResult('TokenEvent', _hcmToken);
  }

  void _onTokenError(Object error) {
    PlatformException e = error as PlatformException;
    showResult('TokenErrorEvent', e.message!);
  }

  void _onMessageReceived(RemoteMessage remoteMessage) async {
    Logs().i("Message received");
    final data = jsonDecode(remoteMessage.data ?? "");

    showResult('onMessageReceived', 'Data: ' + data.toString());

    PushNotification? notification;
    notification = PushNotification(
      devices: [],
      eventId: data['event_id'],
      roomId: data['room_id'],
    );
    showResult('onMessageReceived', 'Data: ' + data.toString());

    try {
      l10n ??= (context != null ? L10n.of(context!) : null) ?? (await L10n.delegate.load(ui.window.locale));
      await pushHelper(
        notification,
        client: client,
        l10n: l10n,
        activeRoomId: GoRouterState.of(context!).pathParameters['roomid'],
        onSelectNotification: goToRoom,
      );
    } on Exception catch (e) {
      loggerError(logMessage: '$e');
    }
  }

  void _onMessageReceiveError(Object error) {
    showResult('onMessageReceiveError', error.toString());
  }

  void _onRemoteMessageSendStatus(String event) {
    showResult('RemoteMessageSendStatus', 'Status: ' + event.toString());
  }

  void _onRemoteMessageSendError(Object error) {
    PlatformException e = error as PlatformException;
    showResult('RemoteMessageSendError', 'Error: ' + e.toString());
  }

  void _onNewIntent(String? intentString) {
    // For navigating to the custom intent page (deep link) the custom
    // intent that sent from the push kit console is:
    // app://app2
    intentString = intentString ?? '';
    if (intentString != '') {
      showResult('CustomIntentEvent: ', intentString);
      List<String> parsedString = intentString.split('://');
    }
  }

  void _onIntentError(Object err) {
    PlatformException e = err as PlatformException;
    debugPrint('Error on intent stream: ' + e.toString());
  }

  void _onNotificationOpenedApp(dynamic initialNotification) {
    if (initialNotification != null) {
      showResult('onNotificationOpenedApp', initialNotification.toString());
    }
  }

  static void backgroundMessageCallback(RemoteMessage remoteMessage) async {
    //TODO clean up print statments
    Logs().i("Message received");
    final Map<String, dynamic> data = remoteMessage.dataOfMap as Map<String, dynamic>;
    Logs().i(data.toString());

    loggerDebug(logMessage: 'Background message is received, sending local notification.');
    PushNotification? notification;
    notification = PushNotification(
      devices: [],
      eventId: data['event_id'],
      roomId: data['room_id'],
    );

    try {
      pushHelper(
        notification,
      );
    } on Exception catch (e) {
      loggerError(logMessage: '$e');
    }
  }

  Future<void> initPlatformState() async {
    Logs().i("initPlatformState - Started");
    // If you want auto init enabled, after getting user agreement call this method.
    await Push.setAutoInitEnabled(true);
    Push.getTokenStream.listen(_onTokenEvent, onError: _onTokenError);
    Push.getIntentStream.listen(_onNewIntent, onError: _onIntentError);
    Push.onNotificationOpenedApp.listen(_onNotificationOpenedApp);
    final dynamic initialNotification = await Push.getInitialNotification();
    _onNotificationOpenedApp(initialNotification);
    final String? intent = await Push.getInitialIntent();
    _onNewIntent(intent);
    Push.onMessageReceivedStream.listen(
      _onMessageReceived,
      onError: _onMessageReceiveError,
    );
    Push.getRemoteMsgSendStatusStream.listen(
      _onRemoteMessageSendStatus,
      onError: _onRemoteMessageSendError,
    );
    final bool backgroundMessageHandler = await Push.registerBackgroundMessageHandler(
      backgroundMessageCallback,
    );
    debugPrint(
      'backgroundMessageHandler registered: $backgroundMessageHandler',
    );
  }

  void removeBackgroundMessageHandler() async {
    await Push.removeBackgroundMessageHandler();
  }

  ///////////////////////////////////////////////////
  String pushNotificationsGatewayHandler() {
    if (FlavorConfig.isPushNoticationDebugging) {
      const String pushNotificationsGatewayUrl = "https://192.168.0.37" + FlavorConfig.pushNotificationsEndpoint;
      return pushNotificationsGatewayUrl;
    } else {
      Logs().i("Homeserver: ${client.homeserver!.host}");
      final String pushNotificationsGatewayUrl = client.homeserver.toString().replaceFirst('matrix', 'sygnal') + FlavorConfig.pushNotificationsEndpoint;
      Logs().i("Push Notification Gateway: $pushNotificationsGatewayUrl");
      return pushNotificationsGatewayUrl;
    }
  }

  Future<void> setupPusher({
    String? gatewayUrl,
    String? token,
    Set<String?>? oldTokens,
    bool useDeviceSpecificAppId = false,
  }) async {
    final clientName = await PlatformInfos.clientName;
    Logs().i("Client Name: $clientName");
    oldTokens ??= <String>{};
    final pushers = await (client.getPushers().catchError((e) {
          Logs().w('[Push] Unable to request pushers', e);
          return <Pusher>[];
        })) ??
        [];
    var setNewPusher = false;
    // Just the plain app id, we add the .data_message suffix later
    var appId = FlavorConfig.pushNotificationsAppId;
    // we need the deviceAppId to remove potential legacy UP pusher
    var deviceAppId = '$appId.${client.deviceID}';
    // appId may only be up to 64 chars as per spec
    if (deviceAppId.length > 64) {
      deviceAppId = deviceAppId.substring(0, 64);
    }
    if (!useDeviceSpecificAppId) {
      appId += '.huawei_data_message';
    }
    final thisAppId = useDeviceSpecificAppId ? deviceAppId : appId;
    if (gatewayUrl != null && token != null) {
      final currentPushers = pushers.where((pusher) => pusher.pushkey == token);
      //Logs().i( 'Add Props Server: ${currentPushers.first.data.additionalProperties}');
      if (currentPushers.length == 1 &&
          currentPushers.first.kind == 'http' &&
          currentPushers.first.appId == thisAppId &&
          currentPushers.first.appDisplayName == clientName &&
          currentPushers.first.deviceDisplayName == client.deviceName &&
          currentPushers.first.lang == 'en' &&
          currentPushers.first.data.url.toString() == gatewayUrl &&
          currentPushers.first.data.format == FlavorConfig.pushNotificationsPusherFormat &&
          currentPushers.first.data.additionalProperties == FlavorConfig.pushNotificationsPusherIOSAddProps) {
        Logs().i('[Push] Pusher already set');
      } else {
        Logs().i('Need to set new pusher');
        oldTokens.add(token);
        if (client.isLogged()) {
          setNewPusher = true;
        }
      }
    } else {
      Logs().w('[Push] Missing required push credentials');
    }
    for (final pusher in pushers) {
      if ((token != null && pusher.pushkey != token && deviceAppId == pusher.appId) || oldTokens.contains(pusher.pushkey)) {
        try {
          await client.deletePusher(pusher);
          Logs().i('[Push] Removed legacy pusher for this device');
        } catch (err) {
          Logs().w('[Push] Failed to remove old pusher', err);
        }
      }
    }
    if (setNewPusher) {
      Logs().i('App detail: $thisAppId $clientName ${client.deviceName} ');
      try {
        await client.postPusher(
          Pusher(
            pushkey: token!,
            appId: thisAppId,
            appDisplayName: clientName,
            deviceDisplayName: client.deviceName!,
            lang: 'en',
            data: PusherData(
              url: Uri.parse(gatewayUrl!),
              format: FlavorConfig.pushNotificationsPusherFormat,
            ),
            kind: 'http',
          ),
          append: false,
        );
      } catch (e, s) {
        Logs().e('[Push] Unable to set pushers', e, s);
      }
    }
  }

  bool _wentToRoomOnStartup = false;

  Future<void> setupHCM() async {
    Logs().i('Setup HCM');
    if (_hcmToken?.isEmpty ?? true) {
      Push.getToken("HCM");
      initPlatformState();
    }
/*      try {
        Push.getToken("HCM");
        Logs().w("HCM Token: ${_hcmToken.toString()}");
        if (_hcmToken == null) throw ('PushToken is null');
      } catch (e, s) {
        Logs().w('[Push] cannot get token', e, e is String ? null : s);
        await _noHcmWarning();
        return;
      }
    }*/
    Logs().v(_hcmToken.toString());
    await setupPusher(
      gatewayUrl: pushNotificationsGatewayHandler(), //AppConfig.pushNotificationsGatewayUrl,
      token: _hcmToken,
    );
  }

  Future<void> setupPush() async {
    Logs().d("SetupPush");
    if (client.onLoginStateChanged.value != LoginState.loggedIn || !PlatformInfos.isMobile || context == null) {
      return;
    }
    unawaited(setupHCM());
    // ignore: unawaited_futures
    _flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails().then((details) {
      if (details == null || !details.didNotificationLaunchApp || _wentToRoomOnStartup) {
        return;
      }
      _wentToRoomOnStartup = true;
      goToRoom(details.notificationResponse);
    });
  }

  Future<void> goToRoom(NotificationResponse? response) async {
    try {
      final roomId = response?.payload;
      Logs().v('[Push] Attempting to go to room $roomId...');
      if (roomId == null) {
        return;
      }
      await client.roomsLoading;
      await client.accountDataLoading;
      final isStory = client.getRoomById(roomId)?.getState(EventTypes.RoomCreate)?.content.tryGet<String>('type') == ClientStoriesExtension.storiesRoomType;
      PageMeApp.router.go("${isStory ? 'stories' : 'rooms'}/$roomId");
    } catch (e, s) {
      Logs().e('[Push] Failed to open room', e, s);
    }
  }

  Future<void> _noHcmWarning() async {
    if (context == null) {
      return;
    }
    if (await store.getItemBool(SettingKeys.showNoHcm, true)) {
      await loadLocale();
      onHcmError?.call(l10n!.oopsPushError);

      if (null == await store.getItem(SettingKeys.showNoHcm)) {
        await store.setItemBool(SettingKeys.showNoHcm, false);
      }
    }
  }

////////////////////////////////////////////////////
  late Map<String, int> idMap;
  Future<void> _loadIdMap() async {
    idMap = Map<String, int>.from(json.decode((await store.getItem(SettingKeys.notificationCurrentIds)) ?? '{}'));
  }

  Future<int> mapRoomIdToInt(String roomId) async {
    await _loadIdMap();
    int? currentInt;
    try {
      currentInt = idMap[roomId];
    } catch (_) {
      currentInt = null;
    }
    if (currentInt != null) {
      return currentInt;
    }
    var nCurrentInt = 0;
    while (idMap.values.contains(currentInt)) {
      nCurrentInt++;
    }
    idMap[roomId] = nCurrentInt;
    await store.setItem(SettingKeys.notificationCurrentIds, json.encode(idMap));
    return nCurrentInt;
  }

  bool _clearingPushLock = false;
  Future<void> _onClearingPush({bool getFromServer = true}) async {
    if (_clearingPushLock) {
      return;
    }
    try {
      _clearingPushLock = true;
      late Iterable<String> emptyRooms;
      if (getFromServer) {
        Logs().v('[Push] Got new clearing push');
        var syncErrored = false;
        if (client.syncPending) {
          Logs().v('[Push] waiting for existing sync');
          // we need to catchError here as the Future might be in a different execution zone
          await client.oneShotSync().catchError((e) {
            syncErrored = true;
            Logs().v('[Push] Error one-shot syncing', e);
          });
        }
        if (!syncErrored) {
          Logs().v('[Push] single oneShotSync');
          // we need to catchError here as the Future might be in a different execution zone
          await client.oneShotSync().catchError((e) {
            syncErrored = true;
            Logs().v('[Push] Error one-shot syncing', e);
          });
          if (!syncErrored) {
            emptyRooms = client.rooms.where((r) => r.notificationCount == 0).map((r) => r.id);
          }
        }
        if (syncErrored) {
          try {
            Logs().v('[Push] failed to sync for fallback push, fetching notifications endpoint...');
            final notifications = await client.getNotifications(limit: 20);
            final notificationRooms = notifications.notifications.map((n) => n.roomId).toSet();
            emptyRooms = client.rooms.where((r) => !notificationRooms.contains(r.id)).map((r) => r.id);
          } catch (e) {
            Logs().v('[Push] failed to fetch pending notifications for clearing push, falling back...', e);
            emptyRooms = client.rooms.where((r) => r.notificationCount == 0).map((r) => r.id);
          }
        }
      } else {
        emptyRooms = client.rooms.where((r) => r.notificationCount == 0).map((r) => r.id);
      }
      await _loadIdMap();
      var changed = false;
      for (final roomId in emptyRooms) {
        final id = idMap[roomId];
        if (id != null) {
          idMap.remove(roomId);
          changed = true;
          await _flutterLocalNotificationsPlugin.cancel(id);
        }
      }
      if (changed) {
        await store.setItem(SettingKeys.notificationCurrentIds, json.encode(idMap));
      }
    } finally {
      _clearingPushLock = false;
    }
  }
}
