import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_shortcuts/keyboard_shortcuts.dart';
import 'package:pageMe/utils/platform_infos.dart';

class ConditionalWrapper extends StatelessWidget {
  final bool condition;
  final Widget Function(BuildContext) builder;
  final Widget Function(BuildContext, Widget)? wrapper;

  const ConditionalWrapper({Key? key,
    required this.condition,
    required this.builder,
    this.wrapper,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Widget child = builder(context);

    if (condition && wrapper != null) {
      return wrapper!(context, child);
    } else {
      return child;
    }
  }
}


class ConditionalKeyBoardShortcuts extends StatelessWidget {
  final Widget child;
  final Set<LogicalKeyboardKey>? keysToPress;
  final VoidCallback? onKeysPressed;
  final String helpLabel;
  final bool enabled;

  const ConditionalKeyBoardShortcuts({Key? key,
    required this.child,
    required this.keysToPress,
    required this.onKeysPressed,
    required this.helpLabel, required this.enabled,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!enabled) {
      return child;
    } else {
      return KeyBoardShortcuts(
        keysToPress: keysToPress,
        onKeysPressed: onKeysPressed,
        helpLabel: helpLabel,
        child: child,
      );
    }
  }
}
