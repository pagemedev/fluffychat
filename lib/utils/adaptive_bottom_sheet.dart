import 'package:flutter/material.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/utils/platform_infos.dart';



Future<T?> showAdaptiveBottomSheet<T>({
  required BuildContext context,
  required Widget Function(BuildContext) builder,
  bool isDismissible = true,
  bool isScrollControlled = true,
}) =>
    showModalBottomSheet(
      context: context,
      builder: builder,
      useRootNavigator: !PlatformInfos.isMobile,
      isDismissible: isDismissible,
      isScrollControlled: isScrollControlled,
      constraints: BoxConstraints(
        maxHeight: MediaQuery.sizeOf(context).height - 128,
        maxWidth: PageMeThemes.columnWidth * 1.5,
      ),
      clipBehavior: Clip.hardEdge,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(FlavorConfig.borderRadius),
          topRight: Radius.circular(FlavorConfig.borderRadius),
        ),
      ),
    );
