import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:matrix_link_text/link_text.dart';

class TimeStampLinkText extends StatelessWidget {
  final String? text;
  final Size timeStampSize;
  final TextStyle? textStyle;
  final TextStyle? linkStyle;
  final TextAlign? textAlign;
  final LinkTapHandler? onLinkTap;
  final int? maxLines;

  const TimeStampLinkText({
    Key? key,
    required this.text,
    required this.timeStampSize,
    this.textStyle,
    this.linkStyle,
    this.textAlign,
    this.onLinkTap,
    this.maxLines,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CleanRichText(
      TextSpan(children: [
        LinkTextSpans(
          text: text!,
          textStyle: textStyle,
          linkStyle: linkStyle,
          onLinkTap: onLinkTap,
          themeData: Theme.of(context),
        ),
        WidgetSpan(
            child: SizedBox(
          width: timeStampSize.width,
        )),
        LinkTextSpans(
          text: ' ',
          textStyle: textStyle,
          linkStyle: linkStyle,
          onLinkTap: onLinkTap,
          themeData: Theme.of(context),
        )
      ]),
      textAlign: textAlign,
      maxLines: maxLines,
    );
  }
}

class TimeStampText extends StatelessWidget {
  final Size timeStampSize;
  final Key? key;
  final InlineSpan? textSpan;
  final String? text;
  final Locale? locale;
  final TextOverflow? overflow;
  final Color? selectionColor;
  final String? semanticsLabel;
  final bool? softWrap;
  final StrutStyle? strutStyle;
  final TextStyle? textStyle;
  final TextAlign? textAlign;
  final TextHeightBehavior? textHeightBehavior;
  final TextDirection? textDirection;
  final double? textScaleFactor;
  final TextWidthBasis? textWidthBasis;
  final int? maxLines;

  const TimeStampText({
    this.key,
    this.textSpan,
    this.text,
    this.locale,
    this.overflow,
    this.selectionColor,
    this.semanticsLabel,
    this.softWrap,
    this.strutStyle,
    this.textStyle,
    this.textAlign,
    this.textHeightBehavior,
    this.textDirection,
    this.textScaleFactor,
    this.textWidthBasis,
    this.maxLines,
    required this.timeStampSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final DefaultTextStyle defaultTextStyle = DefaultTextStyle.of(context);
    TextStyle? effectiveTextStyle = textStyle;
    if (textStyle == null || textStyle!.inherit) {
      effectiveTextStyle = defaultTextStyle.style.merge(textStyle);
    }
    if (MediaQuery.boldTextOverride(context)) {
      effectiveTextStyle = effectiveTextStyle!.merge(const TextStyle(fontWeight: FontWeight.bold));
    }
    final SelectionRegistrar? registrar = SelectionContainer.maybeOf(context);

    Widget result = RichText(
      textAlign: textAlign ?? defaultTextStyle.textAlign ?? TextAlign.start,
      textDirection: textDirection, // RichText uses Directionality.of to obtain a default if this is null.
      locale: locale, // RichText uses Localizations.localeOf to obtain a default if this is null
      softWrap: softWrap ?? defaultTextStyle.softWrap,
      overflow: overflow ?? effectiveTextStyle?.overflow ?? defaultTextStyle.overflow,
      textScaleFactor: textScaleFactor ?? MediaQuery.textScaleFactorOf(context),
      maxLines: maxLines ?? defaultTextStyle.maxLines,
      strutStyle: strutStyle,
      textWidthBasis: textWidthBasis ?? defaultTextStyle.textWidthBasis,
      //textHeightBehavior: textHeightBehavior ?? defaultTextStyle.textHeightBehavior ?? DefaultTextHeightBehavior.of(context),
      selectionRegistrar: registrar,
      selectionColor: selectionColor ?? DefaultSelectionStyle.of(context).selectionColor,
      text: TextSpan(
        style: effectiveTextStyle,
        text: text,
        children: textSpan != null
            ? [
                textSpan!,
                WidgetSpan(
                    child:   SizedBox(
                  width: timeStampSize.width,
                )),
                const TextSpan(
                  text: ' ',
                )
              ]
            : null,
      ),
    );
    if (registrar != null) {
      result = MouseRegion(
        cursor: SystemMouseCursors.text,
        child: result,
      );
    }
    if (semanticsLabel != null) {
      result = Semantics(
        textDirection: textDirection,
        label: semanticsLabel,
        child: ExcludeSemantics(
          child: result,
        ),
      );
    }
    return result;
  }
}
