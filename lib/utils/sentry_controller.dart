import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:sentry/sentry.dart';

import '../config/setting_keys.dart';
import 'famedlysdk_store.dart';

abstract class SentryController {
  static Future<void> toggleSentryAction(
      BuildContext context, bool enableSentry) async {
    if (!FlavorConfig.enableSentry) return;
    final storage = Store();
    await storage.setItemBool(SettingKeys.sentry, enableSentry);
    return;
  }

  static Future<bool> getSentryStatus() async {
    if (!FlavorConfig.enableSentry) return false;
    final storage = Store();
    return await storage.getItemBool(SettingKeys.sentry);
  }

  static final sentry = SentryClient(SentryOptions(dsn: FlavorConfig.sentryDns));

  static void captureException(error, stackTrace) async {
    Logs().e('Capture exception', error, stackTrace);
    if (!kDebugMode && await getSentryStatus()) {
      await sentry.captureException(
        error,
        stackTrace: stackTrace,
      );
    }
  }
}
