import 'dart:developer';
import 'dart:io';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/utils/localized_exception_extension.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:pageMe/utils/string_extension.dart';
import 'package:url_launcher/url_launcher_string.dart';


abstract class PlatformInfos {
  static bool get isWeb => kIsWeb;
  static bool get isLinux => !kIsWeb && Platform.isLinux;
  static bool get isWindows => !kIsWeb && Platform.isWindows;
  static bool get isMacOS => !kIsWeb && Platform.isMacOS;
  static bool get isIOS => !kIsWeb && Platform.isIOS;
  static bool get isAndroid => !kIsWeb && Platform.isAndroid;

  static bool get isCupertinoStyle => isIOS || isMacOS;

  static bool get isMobile => isAndroid || isIOS;

  /// For desktops which don't support ChachedNetworkImage yet
  static bool get isBetaDesktop => isWindows || isLinux;

  static bool get isDesktop => isLinux || isWindows || isMacOS;

  static bool get usesTouchscreen => !isMobile;

  static bool get platformCanRecord => (isMobile || isMacOS);

  static bool get canSubscribe => isMobile && isPublic();

  static Future<bool> isIpad() async {
    final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    final IosDeviceInfo info = await deviceInfo.iosInfo;
    if (info.model.toLowerCase().contains("ipad")) {
      return true;
    }
    return false;
  }

  static Future<String> get clientName async {
    if (isWeb) {
      log('Web: ${await getDeviceName()}: ${FlavorConfig.applicationName} ${'web'} ${kReleaseMode ? '' : 'Debug'}');
      return '${await getDeviceName()}: ${FlavorConfig.applicationName} ${'web'} ${kReleaseMode ? '' : 'Debug'}';
    } else if (isWindows) {
      log('Windows: ${await getDeviceName()}_${FlavorConfig.applicationName}_${Platform.operatingSystem.capitalize}_${kReleaseMode ? '' : 'Debug'}');
      return '${await getDeviceName()}_${FlavorConfig.applicationName}_${Platform.operatingSystem.capitalize}_${kReleaseMode ? '' : 'Debug'}'
          .replaceAll(":", "_");
    }
    log('Other: ${await getDeviceName()}: ${FlavorConfig.applicationName} ${Platform.operatingSystem.capitalize} ${kReleaseMode ? '' : 'Debug'}');
    return '${await getDeviceName()}: ${FlavorConfig.applicationName} ${Platform.operatingSystem.capitalize} ${kReleaseMode ? '' : 'Debug'}';
  }

  static Future<String> getVersion() async {
    var version = kIsWeb ? 'Web' : 'Unknown';
    try {
      version = (await PackageInfo.fromPlatform()).version;
    } catch (_) {}
    return version;
  }

  static Future<String?> getDeviceName() async {
    String parseBrowserNameToReadable(BrowserName browserName) {
      if (browserName == null) {
        return 'Unknown';
      } else if (browserName == BrowserName.firefox) {
        return 'FireFox';
        // "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0"
      } else if (browserName == BrowserName.samsungInternet) {
        return 'Samsung Internet';
        // "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G955F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36
      } else if (browserName == BrowserName.opera) {
        return 'Opera';
        // "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106"
      } else if (browserName == BrowserName.msie) {
        return 'MSIE';
        // "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; Zoom 3.6.0; wbx 1.0.0; rv:11.0) like Gecko"
      } else if (browserName == BrowserName.edge) {
        return 'Microsoft Edge';
        // https://docs.microsoft.com/en-us/microsoft-edge/web-platform/user-agent-string
        // "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.74 Safari/537.36 Edg/79.0.309.43"
        // "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299"
      } else if (browserName == BrowserName.chrome) {
        return 'Google Chrome';
        // "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36"
      } else if (browserName == BrowserName.safari) {
        return 'Safari';
        // "Mozilla/5.0 (iPhone; CPU iPhone OS 11_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1 980x1306"
      } else {
        return 'Unknown';
      }
    }

    final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (PlatformInfos.isAndroid) {
      final AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.manufacturer.toLowerCase() == androidInfo.brand.toLowerCase()) {
        return '${androidInfo.brand} (${androidInfo.model})';
      } else {
        return '${androidInfo.manufacturer} (${androidInfo.brand} - ${androidInfo.model})';
      }
    } else if (PlatformInfos.isIOS) {
      final IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      return '${iosInfo.name?.capitalize} (${iosInfo.model?.capitalize})';
    } else if (PlatformInfos.isWindows) {
      final WindowsDeviceInfo windowsInfo = await deviceInfo.windowsInfo;
      return '${windowsInfo.computerName.capitalize} (${windowsInfo.productName.capitalize})';
    } else if (PlatformInfos.isLinux) {
      final LinuxDeviceInfo linuxInfo = await deviceInfo.linuxInfo;
      return '${linuxInfo.name.capitalize} (Version: ${linuxInfo.version ?? 'Unknown'})';
    } else if (PlatformInfos.isMacOS) {
      final MacOsDeviceInfo macInfo = await deviceInfo.macOsInfo;
      return '${macInfo.computerName.capitalize} (MacOS ${macInfo.majorVersion}.${macInfo.minorVersion}.${macInfo.patchVersion})';
    } else if (PlatformInfos.isWeb) {
      final WebBrowserInfo webInfo = await deviceInfo.webBrowserInfo;
      return parseBrowserNameToReadable(webInfo.browserName);
    } else {
      return 'Unknown Device';
    }
  }

  static void showDialog(BuildContext context) async {
    final version = await PlatformInfos.getVersion();
    showAboutDialog(
      context: context,
      useRootNavigator: false,
      children: [
        Text('Version: $version'),
        OutlinedButton(
          onPressed: () => launchUrlString(FlavorConfig.sourceCodeUrl),
          child: Text(L10n.of(context)!.sourceCode),
        ),
        OutlinedButton(
          onPressed: () => launchUrlString(FlavorConfig.emojiFontUrl),
          child: const Text(FlavorConfig.emojiFontName),
        ),
        OutlinedButton(
          onPressed: () => PageMeApp.router.go('/settings/logs'),
          child: const Text('Logs'),
        ),
      ],
      applicationIcon: isBusiness()
          ? Image.asset(
              'assets/pageme_business_logo.png',
              width: 64,
              height: 64,
            )
          : Image.asset('assets/pageme_public_logo.png', width: 64, height: 64),
      applicationName: FlavorConfig.applicationName,
    );
  }
}
