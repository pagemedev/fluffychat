import 'dart:async';
import 'dart:convert';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/utils/push_helper.dart';

import '../../config/flavor_config.dart';
import '../../config/setting_keys.dart';
import '../famedlysdk_store.dart';

class BasePushFactory implements PushFactory {
  @override
  bool clearingPushLock = false;

  @override
  bool wentToRoomOnStartup = false;

  @override
  late Client client;

  @override
  BuildContext? context;

  @override
  late Map<String, int> idMap;

  @override
  L10n? l10n;

  @override
  StreamSubscription<LoginState>? onLogin;

  @override
  void Function(String errorMsg, {Uri? link})? onPushError;

  @override
  StreamSubscription<SyncUpdate>? onRoomSync;

  @override
  String? pushToken;

  @override
  FlutterLocalNotificationsPlugin get flutterLocalNotificationsPlugin => FlutterLocalNotificationsPlugin();

  @override
  Future<void> goToRoom(NotificationResponse response) async {
    try {
      await client.roomsLoading;
      await client.accountDataLoading;
      if (response.payload == null) {
        Logs().i('[Push] response.payload is null, going to /rooms');
        PageMeApp.router.go("/rooms");
        return;
      }
      if (response.payload.toString().split(' ').length < 2) {
        Logs().i('[Push] response.payload has less than 2 elements, going to /rooms');
        PageMeApp.router.go("/rooms");
        return;
      }
      Logs().i(response.payload.toString());
      //final String eventId = response.payload.toString().split(' ')[0];
      final String roomId = response.payload.toString().split(' ')[1];
      final room = client.getRoomById(roomId);
      final bool isValidRoom = room is Room;
      if (!isValidRoom) {
        Logs().i('[Push] RoomId is null, going to /rooms');
        PageMeApp.router.go("/rooms");
        return;
      }
      Logs().i('[Push] Attempting to go to room $roomId...');
      if (room.isSpace){
        Logs().i('[Push] RoomId is a space, going to /rooms');
        PageMeApp.router.go("/rooms");
        return;
      }
      PageMeApp.router.go("/rooms/$roomId");
    } catch (e, s) {
      Logs().e('[Push] Failed to open room', e, s);
    }
  }

  @override
  void handleLoginStateChanged(_) => setupPush();

  @override
  Future<void> loadIdMap() async {
    idMap = Map<String, int>.from(json.decode((await store.getItem(SettingKeys.notificationCurrentIds)) ?? '{}'));
  }

  @override
  Future<void> loadLocale() async {
    l10n ??= (context != null ? L10n.of(context!) : null) ?? (await L10n.delegate.load(ui.window.locale));
  }

  @override
  Future<int> mapRoomIdToInt(String roomId) async {
    await loadIdMap();
    int? currentInt;
    try {
      currentInt = idMap[roomId];
    } catch (_) {
      currentInt = null;
    }
    if (currentInt != null) {
      return currentInt;
    }
    var nCurrentInt = 0;
    while (idMap.values.contains(currentInt)) {
      nCurrentInt++;
    }
    idMap[roomId] = nCurrentInt;
    await store.setItem(SettingKeys.notificationCurrentIds, json.encode(idMap));
    return nCurrentInt;
  }

  @override
  void onClearingPush({bool getFromServer = true}) async {
    if (clearingPushLock) {
      return;
    }
    try {
      clearingPushLock = true;
      late Iterable<String> emptyRooms;
      if (getFromServer) {
        Logs().v('[Push] Got new clearing push');
        var syncErrored = false;
        if (client.syncPending) {
          Logs().v('[Push] waiting for existing sync');
          // we need to catchError here as the Future might be in a different execution zone
          await client.oneShotSync().catchError((e) {
            syncErrored = true;
            Logs().v('[Push] Error one-shot syncing', e);
          });
        }
        if (!syncErrored) {
          Logs().v('[Push] single oneShotSync');
          // we need to catchError here as the Future might be in a different execution zone
          await client.oneShotSync().catchError((e) {
            syncErrored = true;
            Logs().v('[Push] Error one-shot syncing', e);
          });
          if (!syncErrored) {
            emptyRooms = client.rooms.where((r) => r.notificationCount == 0).map((r) => r.id);
          }
        }
        if (syncErrored) {
          try {
            Logs().v('[Push] failed to sync for fallback push, fetching notifications endpoint...');
            final notifications = await client.getNotifications(limit: 20);
            final notificationRooms = notifications.notifications.map((n) => n.roomId).toSet();
            emptyRooms = client.rooms.where((r) => !notificationRooms.contains(r.id)).map((r) => r.id);
          } catch (e) {
            Logs().v('[Push] failed to fetch pending notifications for clearing push, falling back...', e);
            emptyRooms = client.rooms.where((r) => r.notificationCount == 0).map((r) => r.id);
          }
        }
      } else {
        emptyRooms = client.rooms.where((r) => r.notificationCount == 0).map((r) => r.id);
      }
      await loadIdMap();
      var changed = false;
      for (final roomId in emptyRooms) {
        final id = idMap[roomId];
        if (id != null) {
          idMap.remove(roomId);
          changed = true;
          await flutterLocalNotificationsPlugin.cancel(id);
          //await clearNotificationGroupForRoom(roomId);
        }
      }
      if (changed) {
        await store.setItem(SettingKeys.notificationCurrentIds, json.encode(idMap));
      }
    } finally {
      clearingPushLock = false;
    }
  }

  @override
  String? pushNotificationsGatewayHandler() {
    if (FlavorConfig.isPushNoticationDebugging) {
      const String pushNotificationsGatewayUrl = "https://192.168.0.37:5000${FlavorConfig.pushNotificationsEndpoint}";
      return pushNotificationsGatewayUrl;
    } else {
      Logs().i("Homeserver: ${client.homeserver!.host}");
      final String pushNotificationsGatewayUrl = client.homeserver.toString().replaceFirst('matrix', 'sygnal') + FlavorConfig.pushNotificationsEndpoint;
      Logs().i("Push Notification Gateway: $pushNotificationsGatewayUrl");
      return pushNotificationsGatewayUrl;
    }
  }

  @override
  void onNewPushToken(String pushToken) {
    throw UnimplementedError();
  }

  @override
  Future<void> noPushWarning() {
    throw UnimplementedError();
  }

  @override
  Future<void> setupPush() {
    throw UnimplementedError();
  }

  @override
  Future<void> setupPusher({String? gatewayUrl, Set<String?>? oldTokens}) {
    throw UnimplementedError();
  }

  @override
  void setupStreams() {}

  @override
  Store get store => throw UnimplementedError();
}

abstract class PushFactory {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  BuildContext? context;
  late Client client;
  String? pushToken;
  L10n? l10n;
  bool clearingPushLock = false;
  bool wentToRoomOnStartup = false;
  late Map<String, int> idMap;
  void Function(String errorMsg, {Uri? link})? onPushError;

  StreamSubscription<LoginState>? onLogin;
  StreamSubscription<SyncUpdate>? onRoomSync;

  void handleLoginStateChanged(_);

  Store get store;

  Future<void> setupPush();

  void onNewPushToken(String pushToken);

  void onClearingPush({bool getFromServer = true});

  void setupStreams();

  Future<void> noPushWarning();

  String? pushNotificationsGatewayHandler();

  Future<void> setupPusher({String? gatewayUrl, Set<String?>? oldTokens});

  Future<void> loadLocale();

  Future<int> mapRoomIdToInt(String roomId);

  Future<void> loadIdMap();

  Future<void> goToRoom(NotificationResponse response);
}
