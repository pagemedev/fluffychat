import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hms_gms_availability/flutter_hms_gms_availability.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/push_factorys/firebase_push_factory.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../firebase_options.dart';
import '../logger_functions.dart';
import '../platform_infos.dart';
import 'android_push_factory.dart';
import 'huawei_push_factory.dart';
import 'ios_push_factory.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class PushSetup {
  void pushSetupClientOnly(Client client) async {
    if (PlatformInfos.isMacOS){
      Logs().i("Setting up MacOS Notifications");
      FirebasePushFactory.getInstance(client);
    }else{
      await FlutterHmsGmsAvailability.isHmsAvailable.then(
            (t) async {
          Logs().i("Huawei Services: $t");
          if (t) {
            HuaweiPushFactory.getInstance(client);
            //HuaweiPushNotifications.clientOnly(clients.first);
          } else {
            if (PlatformInfos.isIOS) {
              IOSPushFactory.getInstance(client);
            } else {
              AndroidPushFactory.getInstance(client);
            }
          }
        },
      );
    }
  }

/*
  dynamic pushSetup(Client client, BuildContext context, BuildContext navigatorContext,
      {final void Function(String errorMsg, {Uri? link})? onPushError}) async {
    dynamic backgroundPush;

    await FlutterHmsGmsAvailability.isHmsAvailable.then((t) async {
      Logs().i("Huawei Services: $t");
      if (t) {
        backgroundPush = HuaweiPushFactory(client, context,
            onPushError: (errorMsg, {Uri? link}) => Timer(
                  const Duration(seconds: 1),
                  () {
                    final banner = SnackBar(
                      content: Text(errorMsg),
                      duration: const Duration(seconds: 30),
                      action: link == null
                          ? null
                          : SnackBarAction(
                              label: L10n.of(context)!.link,
                              onPressed: () => launchUrl(Uri.parse(link.toString())),
                            ),
                    );
                    ScaffoldMessenger.of(navigatorContext).showSnackBar(banner);
                  },
                ));
      } else {
        if (PlatformInfos.isIOS) {
          backgroundPush = IOSPushFactory(
            client,
            context,
            onPushError: (errorMsg, {Uri? link}) => Timer(
              const Duration(seconds: 1),
              () {
                final banner = SnackBar(
                  content: Text(errorMsg),
                  duration: const Duration(seconds: 30),
                  action: link == null
                      ? null
                      : SnackBarAction(
                          label: L10n.of(context)!.link,
                          onPressed: () => launchUrl(Uri.parse(link.toString())),
                        ),
                );
                ScaffoldMessenger.of(navigatorContext).showSnackBar(banner);
              },
            ),
          );
        } else {
          backgroundPush = AndroidPushFactory(
            client,
            matrix,
            router,
            onPushError: (errorMsg, {Uri? link}) => Timer(
              const Duration(seconds: 1),
                  () {
                final banner = SnackBar(
                  content: Text(errorMsg),
                  duration: const Duration(seconds: 30),
                  action: link == null
                      ? null
                      : SnackBarAction(
                    label: L10n.of(context)!.link,
                    onPressed: () => launchUrl(Uri.parse(link.toString())),
                  ),
                );
                ScaffoldMessenger.of(navigatorContext).showSnackBar(banner);
              },
            ),
          );
        }
      }
    });
    return backgroundPush;
  }
*/
}
