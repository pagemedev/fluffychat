import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/push_factorys/push_factory.dart';

import '../../config/flavor_config.dart';
import '../../config/setting_keys.dart';
import '../client_manager.dart';
import '../famedlysdk_store.dart';
import '../logger_functions.dart';
import '../push_helper.dart';
import '../push_helper_new.dart';

class FirebasePushFactory extends BasePushFactory {
  static FirebasePushFactory? _instance;
  FirebaseMessaging? firebase = FirebaseMessaging.instance;

  @override
  late Client client;

  Store? _store;

  @override
  Store get store => _store ??= Store();

  @override
  FlutterLocalNotificationsPlugin get flutterLocalNotificationsPlugin => FlutterLocalNotificationsPlugin();

  FirebasePushFactory._internal({required this.client}) {
    setupStreams();
  }

  factory FirebasePushFactory.getInstance(Client client) {
    _instance ??= FirebasePushFactory._internal(
      client: client,
    );
    return _instance!;
  }

  factory FirebasePushFactory(Client client, BuildContext context, {final void Function(String errorMsg, {Uri? link})? onPushError}) {
    final instance = FirebasePushFactory.getInstance(client);
    instance.context = context;
    instance.onPushError = onPushError;
    instance.setupPush();
    return instance;
  }

  @override
  Future<void> setupPush() async {
    Logs().v('Setup firebase');
    if (client.onLoginStateChanged.value != LoginState.loggedIn) {
      Logs().v('Setup firebase - not logged in or has no context');
      return;
    }
    Logs().v('Setup firebase');
    if (pushToken?.isEmpty ?? true) {
      try {
        pushToken = await firebase?.getToken();
        //Logs().d("FCM Token: ${pushToken.toString()}");
        if (pushToken == null) throw ('PushToken is null');
      } catch (e, s) {
        Logs().w('[Push] cannot get token', e, e is String ? null : s);
        await noPushWarning();
        return;
      }
    }
    Logs().d(pushToken.toString());
    await setupPusher(
      gatewayUrl: pushNotificationsGatewayHandler(), //AppConfig.pushNotificationsGatewayUrl,
    );
    flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails().then((details) {
      if (details == null || !details.didNotificationLaunchApp || wentToRoomOnStartup ) {
        return;
      }
      wentToRoomOnStartup = true;
      if (details.notificationResponse != null) {
        goToRoom(details.notificationResponse!);
      }
    });
  }

  @override
  Future<void> noPushWarning() async {
    if (context == null) {
      return;
    }
    if (await store.getItemBool(SettingKeys.showNoGoogle, true)) {
      await loadLocale();
      onPushError?.call(l10n!.oopsPushError);

      if (null == await store.getItem(SettingKeys.showNoGoogle)) {
        await store.setItemBool(SettingKeys.showNoGoogle, false);
      }
    }
  }

  @override
  Future<void> setupPusher({String? gatewayUrl, Set<String?>? oldTokens}) async {
    final clientName = await PlatformInfos.clientName;
    Logs().i("Client Name: $clientName");
    oldTokens ??= <String>{};
    final pushers = await (client.getPushers().catchError(
          (e) {
            Logs().w('[Push] Unable to request pushers', e);
            return <Pusher>[];
          },
        )) ??
        [];
    bool setNewPusher = false;

    final String appId = '${FlavorConfig.pushNotificationsAppId}.data_message';

    if (gatewayUrl != null && pushToken != null) {
      final currentPushers = pushers.where((pusher) => pusher.pushkey == pushToken);
      if (currentPushers.length == 1 &&
          currentPushers.first.kind == 'http' &&
          currentPushers.first.appId == appId &&
          currentPushers.first.appDisplayName == clientName &&
          currentPushers.first.deviceDisplayName == client.deviceName &&
          currentPushers.first.lang == 'en' &&
          currentPushers.first.data.url.toString() == gatewayUrl &&
          currentPushers.first.data.format == FlavorConfig.pushNotificationsPusherFormat &&
          currentPushers.first.data.additionalProperties == FlavorConfig.pushNotificationsPusherIOSAddProps) {
        Logs().i('[Push] Pusher already set');
      } else {
        Logs().i('Need to set new pusher');
        oldTokens.add(pushToken);
        if (client.isLogged()) {
          setNewPusher = true;
        }
      }
    } else {
      Logs().w('[Push] Missing required push credentials');
    }
    for (final pusher in pushers) {
      if ((pushToken != null && pusher.pushkey != pushToken && appId == pusher.appId && pusher.deviceDisplayName == client.deviceName) || oldTokens.contains(pusher.pushkey)) {
        try {
          await client.deletePusher(pusher);
          Logs().i('[Push] Removed legacy pusher for this device');
        } catch (err) {
          Logs().w('[Push] Failed to remove old pusher', err);
        }
      }
    }
    if (setNewPusher) {
      try {
        await client.postPusher(
          Pusher(
            pushkey: pushToken!,
            appId: appId,
            appDisplayName: clientName,
            deviceDisplayName: client.deviceName!,
            lang: 'en',
            data: PusherData(
              url: Uri.parse(gatewayUrl!),
              format: await getNotificationEncryption()
                  ? FlavorConfig.pushNotificationsPusherFormat
                  : '',
              additionalProperties:
              FlavorConfig.pushNotificationsPusherIOSAddProps,
            ),
            kind: 'http',
          ),
          append: false,
        );
      } catch (e, s) {
        Logs().e('[Push] Unable to set pushers', e, s);
      }
    }
  }

  Future<bool> getNotificationEncryption() async {
    return await Store()
        .getItemBool(SettingKeys.iOSNotificationEncryption, true);
  }


  @override
  void setupStreams() async {
    Logs().i('Setting up Firebase Streams');
    firebase ??= FirebaseMessaging.instance;
    onLogin ??= client.onLoginStateChanged.stream.listen(handleLoginStateChanged);
    onRoomSync ??= client.onSync.stream.where((s) => s.hasRoomUpdate).listen((s) => onClearingPush(getFromServer: false));
    Logs().i('Setting up Firebase Streams - request permissions');
    final NotificationSettings settings = await firebase!.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    await firebase!.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
    if (pushToken == null){
      final token = await firebase!.getToken();
      if (token != null){
        onNewPushToken(token);
      }
    }

    await firebase!.getInitialMessage();
    Logs().i('User granted permission: ${settings.authorizationStatus}');
    Logs().i('Setting up Firebase Streams - request permissions complete');
    Logs().i('Setting up Firebase Streams - listening for token');
    firebase!.onTokenRefresh.listen((fcmToken) {
      onNewPushToken(fcmToken);
    }).onError((err) {
      Logs().e('FirebasePushFactory - $err');
    });
    Logs().i('Setting up Firebase Streams - listening for token complete');
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      Logs().v("New notification: ${message.toString()}");
      PushNotification? notification;
      notification = PushNotification(
        devices: [],
        eventId: message.data['event_id'],
        roomId: message.data['room_id'],
      );
      try {
        pushHelper(
          notification,
          client: client,
          l10n: l10n,
          activeRoomId: GoRouterState.of(context!).pathParameters['roomid'],
          onSelectNotification: (notificationResponse) async {
            if (notificationResponse != null) {
              //final String roomId = notificationResponse.payload.toString().split(' ')[1];
              await goToRoom(notificationResponse);
              //clearNotificationGroupForRoom(roomId);
            } else {
              Logs().e('[Push] Payload was empty');
            }
          },
        );
      } on Exception catch (e) {
        loggerError(logMessage: '$e');
      }
    });
    Logs().i('Setting up Firebase Streams - finished');
  }

  @override
  void onNewPushToken(String pushToken) async {
    Logs().i('Setting up Firebase Streams - new token found');
    this.pushToken = pushToken;
    await setupPush();
  }
}



/*
@pragma('vm:entry-point')
Future<void> onSelectNotificationBackground(notificationResponse) async {
  final String eventId = notificationResponse.payload.toString().split(' ')[0];
  final String roomId = notificationResponse.payload.toString().split(' ')[1];
  final String flavour = notificationResponse.payload.toString().split(' ')[2];

  FlavorConfig.initialize(flavor: (flavour ==  'Pageme Business') ? Flavor.business : Flavor.public);
  final clients = await ClientManager.getClients();
  final Client client = clients.first;
  Logs().i( 'Push helper ${notificationResponse.payload.toString()}');

  if (notificationResponse!.actionId != null) {
    if (notificationResponse.actionId == 'mark_as_read') {
      Logs().i( 'Inside mark as read');
      await client.getRoomById(roomId)?.postReceipt(
        eventId,
      );
      return;
    }
    if (notificationResponse.actionId == 'reply') {
      Logs().i( 'Inside reply');
      await client.getRoomById(roomId)?.sendTextEvent(notificationResponse.input);
      return;
    }
  }
  try {
    final GlobalKey<VRouterState> router = GlobalKey<VRouterState>();
    Logs().v('[Push] Attempting to go to room ${notificationResponse!.payload}...');
    await client.roomsLoading;
    await client.accountDataLoading;
    //router.currentState!.toSegments('/rooms/${roomId ?? ''}');
  } catch (e, s) {
    Logs().e('[Push] Failed to open room', e, s);
  }
}
*/
