import 'dart:async';
import 'dart:convert';
import 'package:convert/convert.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_apns_only/flutter_apns_only.dart';


import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/push_factorys/push_factory.dart';

import '../../config/flavor_config.dart';
import '../../config/setting_keys.dart';
import '../../widgets/matrix.dart';
import '../famedlysdk_store.dart';
import '../logger_functions.dart';
import '../push_helper.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'dart:ui' as ui;

import '../push_helper_new.dart';

class IOSPushFactory extends BasePushFactory {
  static IOSPushFactory? _instance;
  ApnsPushConnectorOnly? apnsConnector = ApnsPushConnectorOnly();
  @override
  late Client client;
  MatrixState? matrix;

  Store? _store;

  @override
  Store get store => _store ??= Store();

  @override
  FlutterLocalNotificationsPlugin get flutterLocalNotificationsPlugin => FlutterLocalNotificationsPlugin();

  IOSPushFactory._internal({required this.client}) {
    setupStreams();
  }

  factory IOSPushFactory.getInstance(Client client) {
    _instance ??= IOSPushFactory._internal(client: client);
    return _instance!;
  }

  factory IOSPushFactory(Client client, MatrixState matrix, {final void Function(String errorMsg, {Uri? link})? onPushError}) {
    final instance = IOSPushFactory.getInstance(client);
    instance.matrix = matrix;
    instance.onPushError = onPushError;
    unawaited(instance.setupPush());
    return instance;
  }

  @override
  Future<void> setupPush() async {
    Logs().v('setupPush started');
    if (client.onLoginStateChanged.value != LoginState.loggedIn || !(PlatformInfos.isMobile || PlatformInfos.isMacOS)) {
      Logs().v('setupPush ended as did not meet criteria');
      return;
    }
    Logs().v('Setup APNS');
    if (pushToken?.isEmpty ?? true) {
      try {
        if (pushToken == null) throw ('APNS PushToken is null');
      } catch (e, s) {
        Logs().w('[Push] cannot get token', e, e is String ? null : s);
        await noPushWarning();
        //return;
      }
    }
    Logs().d(pushToken.toString());
    await setupPusher(
      gatewayUrl: pushNotificationsGatewayHandler(), //AppConfig.pushNotificationsGatewayUrl,
    );

    await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails().then((details) {
      if (details == null || !details.didNotificationLaunchApp || wentToRoomOnStartup) {
        return;
      }
      wentToRoomOnStartup = true;
      if (details.notificationResponse != null) {
        goToRoom(details.notificationResponse!);
      }
    });
    Logs().v('setupPush ended');
  }

  @override
  Future<void> noPushWarning() async {
    if (context == null) {
      return;
    }
    if (await store.getItemBool(SettingKeys.showNoApple, true)) {
      await loadLocale();
      onPushError?.call(l10n!.oopsPushError);

      if (null == await store.getItem(SettingKeys.showNoApple)) {
        await store.setItemBool(SettingKeys.showNoApple, false);
      }
    }
  }

  @override
  Future<void> setupPusher({String? gatewayUrl, Set<String?>? oldTokens}) async {
    final clientName = await PlatformInfos.clientName;
    Logs().i("Client Name: $clientName");
    oldTokens ??= <String>{};
    final pushers = await (client.getPushers().catchError(
          (e) {
            Logs().w('[Push] Unable to request pushers', e);
            return <Pusher>[];
          },
        )) ??
        [];
    bool setNewPusher = false;

    final String appId = FlavorConfig.pushNotificationsAppId + ((kReleaseMode) ? '.ios_data_message' : '.ios_data_message_debug');

    if (gatewayUrl != null && pushToken != null) {
      final currentPushers = pushers.where((pusher) => pusher.pushkey == pushToken);
      if (currentPushers.length == 1 &&
          currentPushers.first.kind == 'http' &&
          currentPushers.first.appId == appId &&
          currentPushers.first.appDisplayName == clientName &&
          currentPushers.first.deviceDisplayName == client.deviceName &&
          currentPushers.first.lang == 'en' &&
          currentPushers.first.data.url.toString() == gatewayUrl &&
          currentPushers.first.data.format == FlavorConfig.pushNotificationsPusherFormat &&
          currentPushers.first.data.additionalProperties == FlavorConfig.pushNotificationsPusherIOSAddProps) {
        Logs().i('[Push] Pusher already set');
      } else {
        Logs().i('Need to set new pusher');
        oldTokens.add(pushToken);
        if (client.isLogged()) {
          setNewPusher = true;
        }
      }
    } else {
      Logs().w('[Push] Missing required push credentials');
    }
    for (final pusher in pushers) {
      if ((pushToken != null && pusher.pushkey != pushToken && appId == pusher.appId) || oldTokens.contains(pusher.pushkey)) {
        try {
          await client.deletePusher(pusher);
          Logs().i('[Push] Removed legacy pusher for this device');
        } catch (err) {
          Logs().w('[Push] Failed to remove old pusher', err);
        }
      }
    }
    if (setNewPusher) {
      try {
        await client.postPusher(
          Pusher(
            pushkey: pushToken!,
            appId: appId,
            appDisplayName: clientName,
            deviceDisplayName: client.deviceName!,
            lang: 'en',
            data: PusherData(
              url: Uri.parse(gatewayUrl!),
              format: await getNotificationEncryption() ? FlavorConfig.pushNotificationsPusherFormat : '',
              additionalProperties: FlavorConfig.pushNotificationsPusherIOSAddProps,
            ),
            kind: 'http',
          ),
          append: false,
        );
      } catch (e, s) {
        Logs().e('[Push] Unable to set pushers', e, s);
      }
    }
  }

  Future<bool> getNotificationEncryption() async {
    return await Store().getItemBool(SettingKeys.iOSNotificationEncryption, true);
  }

  @override
  Future<void> setupStreams() async {
    onLogin ??= client.onLoginStateChanged.stream.listen(handleLoginStateChanged);
    onRoomSync ??= client.onSync.stream.where((s) => s.hasRoomUpdate).listen((s) => onClearingPush(getFromServer: false));

    apnsConnector?.configureApns(
      onLaunch: (data) async => onPush('onLaunch', data),
      onResume: (data) async => goToRoom(data.payload['data']['room_id']),
      onMessage: (data) async => onPush('onMessage', data),
    );
    apnsConnector?.token.addListener(
      () {
        //Logs().i( 'Token ${apnsConnector!.token.value}');
        final List<int> result = hex.decode(apnsConnector?.token.value ?? '');
        pushToken = base64.encode(result);
        //Logs().i( "Base64 token: $_apnsToken");
        if (pushToken != apnsConnector?.token.value) {
          onNewPushToken(pushToken!);
        }
      },
    );
    final result = await apnsConnector?.requestNotificationPermissions(
      const IosNotificationSettings(sound: true, badge: true, alert: true),
    );
    
    Logs().i("apnsConnector?.requestNotificationPermissions: $result");

    apnsConnector?.shouldPresent = (apnsRemoteMessage) async {
      return false; //apnsRemoteMessage.payload['notification'] != null;
    };
  }

  Future<void> onPush(String name, ApnsRemoteMessage apnsRemoteMessage) async {
    Logs().i('$name: ${apnsRemoteMessage.payload.toString()}');

    PushNotification? notification;
    notification = PushNotification(
      devices: [],
      eventId: apnsRemoteMessage.payload['data']['event_id'],
      roomId: apnsRemoteMessage.payload['data']['room_id'],
    );
    unawaited(pushHelper(
      notification,
      client: client,
      l10n: l10n,
      activeRoomId: matrix?.activeRoomId,
      onSelectNotification: (notificationResponse) {
        if (notificationResponse != null) {
          goToRoom(notificationResponse);
          //clearNotificationsForRoom(notificationResponse);
        } else {
          Logs().e('[Push] Payload was empty');
        }
      },
    ));
    Logs().i('Notification handled');
  }

  @override
  Future<void> onNewPushToken(String pushToken) async {
    this.pushToken = pushToken;
    await setupPush();
  }
}
