import 'dart:async';

import 'package:fcm_shared_isolate/fcm_shared_isolate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pageme_app.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/push_factorys/push_factory.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config/flavor_config.dart';
import '../../config/setting_keys.dart';
import '../../cubits/sending_handler/sending_handler_bloc.dart';
import '../../widgets/matrix.dart';
import '../client_manager.dart';
import '../famedlysdk_store.dart';
import '../logger_functions.dart';
import '../push_helper.dart';
import '../push_helper_new.dart';

class AndroidPushFactory extends BasePushFactory {
  static AndroidPushFactory? _instance;
  FcmSharedIsolate? firebase = FcmSharedIsolate();

  @override
  late Client client;
  MatrixState? matrix;

  Store? _store;

  @override
  Store get store => _store ??= Store();

  @override
  FlutterLocalNotificationsPlugin get flutterLocalNotificationsPlugin => FlutterLocalNotificationsPlugin();

  AndroidPushFactory._internal({required this.client}) {
    setupStreams();
  }

  factory AndroidPushFactory.getInstance(Client client) {
    _instance ??= AndroidPushFactory._internal(
      client: client,
    );
    return _instance!;
  }

  factory AndroidPushFactory(Client client, MatrixState matrix, {final void Function(String errorMsg, {Uri? link})? onPushError}) {
    final instance = AndroidPushFactory.getInstance(client);
    instance.matrix = matrix;
    instance.onPushError = onPushError;
    unawaited(instance.setupPush());
    return instance;
  }

  @override
  Future<void> setupPush() async {
    if (client.onLoginStateChanged.value != LoginState.loggedIn || !PlatformInfos.isMobile) {
      return;
    }
    Logs().v('Setup firebase');
    if (pushToken?.isEmpty ?? true) {
      try {
        pushToken = await firebase?.getToken();
        Logs().d("FCM Token: ${pushToken.toString()}");
        if (pushToken == null) throw ('PushToken is null');
      } catch (e, s) {
        Logs().w('[Push] cannot get token', e, e is String ? null : s);
        await noPushWarning();
        return;
      }
    }
    Logs().d(pushToken.toString());
    await setupPusher(
      gatewayUrl: pushNotificationsGatewayHandler(), //AppConfig.pushNotificationsGatewayUrl,
    );
    await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails().then((details) {
      if (details == null || !details.didNotificationLaunchApp || wentToRoomOnStartup) {
        return;
      }
      wentToRoomOnStartup = true;
      if (details.notificationResponse != null) {
        goToRoom(details.notificationResponse!);
      } else {
        Logs().w("Notification response was null");
      }
    });
  }

  @override
  Future<void> noPushWarning() async {
    if (await store.getItemBool(SettingKeys.showNoGoogle, true)) {
      await loadLocale();
      onPushError?.call(l10n!.oopsPushError);

      if (null == await store.getItem(SettingKeys.showNoGoogle)) {
        await store.setItemBool(SettingKeys.showNoGoogle, false);
      }
    }
  }

  @override
  Future<void> setupPusher({String? gatewayUrl, Set<String?>? oldTokens}) async {
    final clientName = await PlatformInfos.clientName;
    Logs().i("Client Name: $clientName");
    oldTokens ??= <String>{};
    final pushers = await (client.getPushers().catchError(
          (e) {
            Logs().w('[Push] Unable to request pushers', e);
            return <Pusher>[];
          },
        )) ??
        [];
    bool setNewPusher = false;

    final String appId = '${FlavorConfig.pushNotificationsAppId}.data_message';

    if (gatewayUrl != null && pushToken != null) {
      final currentPushers = pushers.where((pusher) => pusher.pushkey == pushToken);
      if (currentPushers.length == 1 &&
          currentPushers.first.kind == 'http' &&
          currentPushers.first.appId == appId &&
          currentPushers.first.appDisplayName == clientName &&
          currentPushers.first.deviceDisplayName == client.deviceName &&
          currentPushers.first.lang == 'en' &&
          currentPushers.first.data.url.toString() == gatewayUrl &&
          currentPushers.first.data.format == FlavorConfig.pushNotificationsPusherFormat &&
          currentPushers.first.data.additionalProperties == FlavorConfig.pushNotificationsPusherIOSAddProps) {
        Logs().i('[Push] Pusher already set');
      } else {
        Logs().i('Need to set new pusher');
        oldTokens.add(pushToken);
        if (client.isLogged()) {
          setNewPusher = true;
        }
      }
    } else {
      Logs().w('[Push] Missing required push credentials');
    }
    for (final pusher in pushers) {
      if ((pushToken != null && pusher.pushkey != pushToken && appId == pusher.appId && pusher.deviceDisplayName == client.deviceName) ||
          oldTokens.contains(pusher.pushkey)) {
        try {
          await client.deletePusher(pusher);
          Logs().i('[Push] Removed legacy pusher for this device');
        } catch (err) {
          Logs().w('[Push] Failed to remove old pusher', err);
        }
      }
    }
    if (setNewPusher) {
      try {
        await client.postPusher(
          Pusher(
            pushkey: pushToken!,
            appId: appId,
            appDisplayName: clientName,
            deviceDisplayName: client.deviceName!,
            lang: 'en',
            data: PusherData(
              url: Uri.parse(gatewayUrl!),
              format: FlavorConfig.pushNotificationsPusherFormat,
            ),
            kind: 'http',
          ),
          append: false,
        );
      } catch (e, s) {
        Logs().e('[Push] Unable to set pushers', e, s);
      }
    }
  }

  @override
  void setupStreams() {
    firebase = FcmSharedIsolate();
    onLogin ??= client.onLoginStateChanged.stream.listen(handleLoginStateChanged);
    onRoomSync ??= client.onSync.stream.where((s) => s.hasRoomUpdate).listen((s) => onClearingPush(getFromServer: false));

    firebase?.setListeners(
      onMessage: (message) async {
        Logs().i(message.toString());
        PushNotification? notification;
        notification = PushNotification(
          devices: [],
          eventId: message['event_id'],
          roomId: message['room_id'],
        );
        try {
          unawaited(pushHelper(
            notification,
            client: client,
            l10n: l10n,
            activeRoomId: matrix?.activeRoomId,
            onSelectNotification: (notificationResponse) async {
              if (notificationResponse != null) {
                await goToRoom(notificationResponse);
            /*    if (notificationResponse.payload == null) {
                  return;
                }
                if (notificationResponse.payload.toString().split(' ').length < 2) {
                  return;
                }
                final String roomId = notificationResponse.payload.toString().split(' ')[1];
                await clearNotificationGroupForRoom(roomId);*/
              } else {
                Logs().e('[Push] Payload was empty');
              }
            },
          ));
        } on Exception catch (e) {
          loggerError(logMessage: '$e');
        }
      },
      onNewToken: onNewPushToken,
    );
  }

  @override
  Future<void> onNewPushToken(String pushToken) async {
    this.pushToken = pushToken;
    await setupPush();
  }
}

@pragma('vm:entry-point')
void onSelectNotificationBackground(notificationResponse) async {
  final String eventId = notificationResponse.payload.toString().split(' ')[0];
  final String roomId = notificationResponse.payload.toString().split(' ')[1];
  final String flavour = notificationResponse.payload.toString().split(' ')[2];

  FlavorConfig.initialize(flavor: (flavour == 'Pageme Business') ? Flavor.business : Flavor.public);
  final store = await SharedPreferences.getInstance();
  final clients = await ClientManager.getClients(store: store);
  final Client client = clients.first;
  Logs().i('Push helper ${notificationResponse.payload.toString()}');

  if (notificationResponse!.actionId != null) {
    if (notificationResponse.actionId == 'mark_as_read') {
      Logs().i('Inside mark as read');
      await client.getRoomById(roomId)?.setReadMarker(
            eventId,
          );
      return;
    }
    if (notificationResponse.actionId == 'reply') {
      Logs().i('Inside reply');
      await client.getRoomById(roomId)?.sendTextEvent(notificationResponse.input);
      return;
    }
  }
  try {
    Logs().v('[Push] Attempting to go to room ${notificationResponse!.payload}...');
    await client.roomsLoading;
    await client.accountDataLoading;
    PageMeApp.router.go('/rooms/$roomId');
  } catch (e, s) {
    Logs().e('[Push] Failed to open room', e, s);
  }
}
