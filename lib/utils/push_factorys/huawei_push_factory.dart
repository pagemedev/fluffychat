import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:go_router/go_router.dart';
import 'package:huawei_push/huawei_push.dart' as push;
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/push_factorys/push_factory.dart';

import '../../config/flavor_config.dart';
import '../../config/setting_keys.dart';
import '../../widgets/matrix.dart';
import '../famedlysdk_store.dart';
import '../logger_functions.dart';
import '../push_helper.dart';
import '../push_helper_new.dart';

class HuaweiPushFactory extends BasePushFactory {
  static HuaweiPushFactory? _instance;
  @override
  late Client client;
  MatrixState? matrix;
  Store? _store;

  @override
  Store get store => _store ??= Store();

  @override
  FlutterLocalNotificationsPlugin get flutterLocalNotificationsPlugin => FlutterLocalNotificationsPlugin();

  HuaweiPushFactory._internal({required this.client}) {
    unawaited(setupStreams());
  }

  factory HuaweiPushFactory.getInstance(Client client) {
    _instance ??= HuaweiPushFactory._internal(client: client);
    return _instance!;
  }

  factory HuaweiPushFactory(Client client, MatrixState matrix,
      {final void Function(String errorMsg, {Uri? link})? onPushError}) {
    final instance = HuaweiPushFactory.getInstance(client);
    instance.matrix = matrix;
    instance.onPushError = onPushError;
    unawaited(instance.setupPush());
    return instance;
  }

  @override
  Future<void> noPushWarning() async {
    if (await store.getItemBool(SettingKeys.showNoHuawei, true)) {
      await loadLocale();
      onPushError?.call(l10n!.oopsPushError);

      if (null == await store.getItem(SettingKeys.showNoHuawei)) {
        await store.setItemBool(SettingKeys.showNoHuawei, false);
      }
    }
  }

  @override
  Future<void> setupPush() async {
    Logs().i('Setup HCM');
    if (client.onLoginStateChanged.value != LoginState.loggedIn || !PlatformInfos.isMobile) {
      Logs().w('Setup HCM - Not logged in or not mobile');
      return;
    }
    if (pushToken?.isEmpty ?? true) {
      Logs().i('Setup HCM - push token was null, fetching token');
      try {
        push.Push.getToken("HCM");
        Timer(const Duration(seconds: 3), () {
          Logs().i( 'Timer finished');
          if (pushToken == null) throw ('PushToken is null');
        });
      } catch (e, s) {
        Logs().w('[Push] cannot get token', e, e is String ? null : s);
        await noPushWarning();
        return;
      }
    }
    Logs().d(pushToken.toString());
    await setupPusher(
      gatewayUrl: pushNotificationsGatewayHandler(), //AppConfig.pushNotificationsGatewayUrl,
    );
    await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails().then((details) {
      if (details == null || !details.didNotificationLaunchApp || wentToRoomOnStartup) {
        return;
      }
      wentToRoomOnStartup = true;
      if (details.notificationResponse != null) {
        goToRoom(details.notificationResponse!);
      }
    });
  }

  @override
  Future<void> setupPusher({String? gatewayUrl, Set<String?>? oldTokens}) async {
    final clientName = await PlatformInfos.clientName;
    Logs().i( "Client Name: $clientName");
    oldTokens ??= <String>{};
    final pushers = await (client.getPushers().catchError(
          (e) {
            Logs().w('[Push] Unable to request pushers', e);
            return <Pusher>[];
          },
        )) ??
        [];
    bool setNewPusher = false;
    final String appId = '${FlavorConfig.pushNotificationsAppId}.huawei_data_message';

    if (gatewayUrl != null && pushToken != null) {
      final currentPushers = pushers.where((pusher) => pusher.pushkey == pushToken);
      if (currentPushers.length == 1 &&
          currentPushers.first.kind == 'http' &&
          currentPushers.first.appId == appId &&
          currentPushers.first.appDisplayName == clientName &&
          currentPushers.first.deviceDisplayName == client.deviceName &&
          currentPushers.first.lang == 'en' &&
          currentPushers.first.data.url.toString() == gatewayUrl &&
          currentPushers.first.data.format == FlavorConfig.pushNotificationsPusherFormat &&
          currentPushers.first.data.additionalProperties == FlavorConfig.pushNotificationsPusherIOSAddProps) {
        Logs().i('[Push] Pusher already set');
      } else {
        Logs().i('Need to set new pusher');
        oldTokens.add(pushToken);
        if (client.isLogged()) {
          setNewPusher = true;
        }
      }
    } else {
      Logs().w('[Push] Missing required push credentials');
    }
    for (final pusher in pushers) {
      if ((pushToken != null && pusher.pushkey != pushToken && appId == pusher.appId) || oldTokens.contains(pusher.pushkey)) {
        try {
          await client.deletePusher(pusher);
          Logs().i('[Push] Removed legacy pusher for this device');
        } catch (err) {
          Logs().w('[Push] Failed to remove old pusher', err);
        }
      }
    }
    if (setNewPusher) {
      try {
        await client.postPusher(
          Pusher(
            pushkey: pushToken!,
            appId: appId,
            appDisplayName: clientName,
            deviceDisplayName: client.deviceName!,
            lang: 'en',
            data: PusherData(
              url: Uri.parse(gatewayUrl!),
              format: FlavorConfig.pushNotificationsPusherFormat,
            ),
            kind: 'http',
          ),
          append: false,
        );
      } catch (e, s) {
        Logs().e('[Push] Unable to set pushers', e, s);
      }
    }
  }

  @override
  Future<void> setupStreams() async {
    onLogin ??= client.onLoginStateChanged.stream.listen(handleLoginStateChanged);
    onRoomSync ??= client.onSync.stream.where((s) => s.hasRoomUpdate).listen((s) => onClearingPush(getFromServer: false));

    Logs().i("Setup Streams - Started");
    // If you want auto init enabled, after getting user agreement call this method.
    await push.Push.setAutoInitEnabled(true);

    push.Push.getTokenStream.listen(_onTokenEvent, onError: _onTokenError);
    push.Push.getIntentStream.listen(_onNewIntent, onError: _onIntentError);
    push.Push.onNotificationOpenedApp.listen(_onNotificationOpenedApp);
    push.Push.onMessageReceivedStream.listen(_onMessageReceived, onError: _onMessageReceiveError);
    push.Push.getRemoteMsgSendStatusStream.listen(_onRemoteMessageSendStatus, onError: _onRemoteMessageSendError);

    final dynamic initialNotification = await push.Push.getInitialNotification();
    _onNotificationOpenedApp(initialNotification);

    final String? intent = await push.Push.getInitialIntent();
    _onNewIntent(intent);

    final bool backgroundMessageHandler = await push.Push.registerBackgroundMessageHandler(
      isBusiness() ? backgroundMessageCallbackBusiness : backgroundMessageCallbackPublic,
    );
    debugPrint('backgroundMessageHandler registered: $backgroundMessageHandler');
    //push.Push.removeBackgroundMessageHandler();
  }

  void _onTokenEvent(String event) {
    if (pushToken != event) {
      pushToken = event;
      setupPush();
    }
    showResult('TokenEvent', pushToken);
  }

  void _onTokenError(Object error) {
    final PlatformException e = error as PlatformException;
    showResult('TokenErrorEvent', e.message!);
  }

  Future<void> _onMessageReceived(push.RemoteMessage remoteMessage) async {
    Logs().i("Message received");
    final data = jsonDecode(remoteMessage.data ?? "");

    showResult('onMessageReceived', 'Data: $data');

    PushNotification? notification;
    notification = PushNotification(
      devices: [],
      eventId: data['event_id'],
      roomId: data['room_id'],
    );
    showResult('onMessageReceived', 'Data: $data');
    try {
      await pushHelper(
        notification,
        client: client,
        l10n: l10n,
        activeRoomId: matrix?.activeRoomId,
        onSelectNotification: (notificationResponse) {
          if (notificationResponse != null) {
            goToRoom(notificationResponse);
            //clearNotificationsForRoom(notificationResponse);
          }else{
            Logs().e('[Push] Payload was empty');
          }
        },
      );
    } on Exception catch (e) {
      loggerError(logMessage: '$e');
    }
  }

  void _onMessageReceiveError(Object error) {
    showResult('onMessageReceiveError', error.toString());
  }

  void _onRemoteMessageSendStatus(String event) {
    showResult('RemoteMessageSendStatus', 'Status: $event');
  }

  void _onRemoteMessageSendError(Object error) {
    final PlatformException e = error as PlatformException;
    showResult('RemoteMessageSendError', 'Error: $e');
  }

  void _onNewIntent(String? intentString) {
    // For navigating to the custom intent page (deep link) the custom
    // intent that sent from the push kit console is:
    // app://app2
    intentString = intentString ?? '';
    if (intentString != '') {
      showResult('CustomIntentEvent: ', intentString);
    }
  }

  void showResult(
    String name, [
    String? msg = 'Button pressed.',
  ]) {
    msg ??= '';
    debugPrint('[$name]: $msg');
    //Push.showToast('[' + name + ']: ' + msg);
  }

  void _onIntentError(Object err) {
    final PlatformException e = err as PlatformException;
    debugPrint('Error on intent stream: $e');
  }

  void _onNotificationOpenedApp(dynamic initialNotification) {
    if (initialNotification != null) {
      showResult('onNotificationOpenedApp', initialNotification.toString());
    }
  }

/*  static Future<void> goToRoomHuaweiBusiness({required NotificationResponse? roomId}) async {
    try {
      FlavorConfig.initialize(flavor: Flavor.business);
      final clients = await ClientManager.getClients();
      final Client client = clients.first;
      final GlobalKey<VRouterState> router = GlobalKey<VRouterState>();
      Logs().v('[Push] Attempting to go to room $roomId...');
      await client.roomsLoading;
      await client.accountDataLoading;
      final isStory = client.getRoomById(roomId?.id.toString() ?? '')?.getState(EventTypes.RoomCreate)?.content.tryGet<String>('type') ==
          ClientStoriesExtension.storiesRoomType;
      router.currentState!.toSegments([isStory ? 'stories' : 'rooms', roomId?.id.toString() ?? '']);
    } catch (e, s) {
      Logs().e('[Push] Failed to open room', e, s);
    }
  }

  static Future<void> goToRoomHuaweiPublic({required NotificationResponse? roomId}) async {
    try {
      FlavorConfig.initialize(flavor: Flavor.public);
      final clients = await ClientManager.getClients();
      final Client client = clients.first;
      final GlobalKey<VRouterState> router = GlobalKey<VRouterState>();
      Logs().v('[Push] Attempting to go to room $roomId...');
      await client.roomsLoading;
      await client.accountDataLoading;
      final isStory = client.getRoomById(roomId?.id.toString() ?? '')?.getState(EventTypes.RoomCreate)?.content.tryGet<String>('type') ==
          ClientStoriesExtension.storiesRoomType;
      router.currentState!.toSegments([isStory ? 'stories' : 'rooms', roomId?.id.toString() ?? '']);
    } catch (e, s) {
      Logs().e('[Push] Failed to open room', e, s);
    }
  }*/

  @pragma('vm:entry-point')
  static void backgroundMessageCallbackBusiness(push.RemoteMessage remoteMessage) async {
    loggerDebug(logMessage: 'Background message is received, sending local notification.');
    FlavorConfig.initialize(flavor: Flavor.business);
    Logs().i("Message received");
    final Map<String, dynamic> data = remoteMessage.dataOfMap as Map<String, dynamic>;
    Logs().i(data.toString());

    PushNotification? notification;
    notification = PushNotification(
      devices: [],
      eventId: data['event_id'],
      roomId: data['room_id'],
    );

    try {
      final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
      await flutterLocalNotificationsPlugin.initialize(
        const InitializationSettings(
          android: AndroidInitializationSettings('@mipmap/ic_launcher'),
          iOS: DarwinInitializationSettings(),
        ),
        //onDidReceiveNotificationResponse: (roomId) => goToRoomHuaweiBusiness(roomId: roomId),
        //onDidReceiveBackgroundNotificationResponse: (roomId) => goToRoomHuaweiBusiness(roomId: roomId),
      );
      L10n? l10n;
      if (window.locale.languageCode == "und") {
        final Locale locale = Locale.fromSubtags(countryCode: "za", languageCode: 'en', scriptCode: window.locale.scriptCode);
        l10n ??= await L10n.delegate.load(locale);
      } else {
        l10n ??= await L10n.delegate.load(window.locale);
      }
      flutterLocalNotificationsPlugin.show(
        0,
        "New notification in PageMe Business",
        l10n.openAppToReadMessages,
        NotificationDetails(
          iOS: const DarwinNotificationDetails(),
          android: AndroidNotificationDetails(
            FlavorConfig.pushNotificationsChannelId,
            FlavorConfig.pushNotificationsChannelName,
            channelDescription: FlavorConfig.pushNotificationsChannelDescription,
            number: notification.counts?.unread,
            //styleInformation: (event.attachmentMxcUrl != null) ? bigPictureStyleInformation : messagingStyleInformation,
            ticker: l10n.unreadChats(notification.counts?.unread ?? 1),
            importance: Importance.max,
            priority: Priority.high,
            groupKey: notification.roomId,
            //tag: event.eventId,
            autoCancel: true,
            category: AndroidNotificationCategory.message,
            playSound: true,
            setAsGroupSummary: true,
            channelShowBadge: true,
          ),
        ),
      );
    } on Exception catch (e) {
      loggerError(logMessage: '$e');
    }
  }

  @pragma('vm:entry-point')
  static void backgroundMessageCallbackPublic(push.RemoteMessage remoteMessage) async {
    loggerDebug(logMessage: 'Background message is received, sending local notification.');
    FlavorConfig.initialize(flavor: Flavor.public);
    Logs().i("Message received");
    final Map<String, dynamic> data = remoteMessage.dataOfMap as Map<String, dynamic>;
    Logs().i(data.toString());

    PushNotification? notification;
    notification = PushNotification(
      devices: [],
      eventId: data['event_id'],
      roomId: data['room_id'],
    );

    try {
      final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
      await flutterLocalNotificationsPlugin.initialize(
        const InitializationSettings(
          android: AndroidInitializationSettings('@mipmap/ic_launcher'),
          iOS: DarwinInitializationSettings(),
        ),
        //onDidReceiveNotificationResponse: (roomId) => goToRoomHuaweiPublic(roomId: roomId),
        //onDidReceiveBackgroundNotificationResponse: (roomId) => goToRoomHuaweiPublic(roomId: roomId),
      );
      L10n? l10n;
      if (window.locale.languageCode == "und") {
        final Locale locale = Locale.fromSubtags(countryCode: "za", languageCode: 'en', scriptCode: window.locale.scriptCode);
        l10n ??= await L10n.delegate.load(locale);
      } else {
        l10n ??= await L10n.delegate.load(window.locale);
      }
      flutterLocalNotificationsPlugin.show(
        0,
        "New notification in PageMe Public",
        l10n.openAppToReadMessages,
        NotificationDetails(
            iOS: const DarwinNotificationDetails(),
            android: AndroidNotificationDetails(
              FlavorConfig.pushNotificationsChannelId,
              FlavorConfig.pushNotificationsChannelName,
              channelDescription: FlavorConfig.pushNotificationsChannelDescription,
              number: notification.counts?.unread,
              //styleInformation: (event.attachmentMxcUrl != null) ? bigPictureStyleInformation : messagingStyleInformation,
              ticker: l10n.unreadChats(notification.counts?.unread ?? 1),
              importance: Importance.max,
              priority: Priority.high,
              groupKey: notification.roomId,
              //tag: event.eventId,
              autoCancel: true,
              category: AndroidNotificationCategory.message,
              playSound: true,
              setAsGroupSummary: true,
              channelShowBadge: true,
            )),
      );
    } on Exception catch (e) {
      loggerError(logMessage: '$e');
    }
  }
}
