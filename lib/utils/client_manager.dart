import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:flutter/foundation.dart';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:matrix/encryption/utils/key_verification.dart';
import 'package:matrix/matrix.dart';
import 'package:path_provider/path_provider.dart';

import 'package:pageMe/utils/custom_image_resizer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'custom_http_client.dart';
import 'famedlysdk_store.dart';
import 'logger_functions.dart';
import 'matrix_sdk_extensions.dart/flutter_hive_collections_database.dart';

abstract class ClientManager {
  //static const String clientNamespace = 'com.pageme.pageme.store.clients';
  static final String clientNamespace = '${FlavorConfig.appId}.store.clients';

  static Future<List<Client>> getClients({bool initialize = true, required SharedPreferences store,}) async {
    if (PlatformInfos.isLinux) {
      Hive.init((await getApplicationSupportDirectory()).path);
    } else if (PlatformInfos.isWindows) {
      WidgetsFlutterBinding.ensureInitialized();
      var appDir = await getApplicationSupportDirectory();
      Logs().i(appDir.path);
      Hive.init(appDir.path);
    } else if (PlatformInfos.isIOS) {
      var appDir = await getApplicationSupportDirectory();
      Logs().i(appDir.path);
      Hive.init(appDir.path);
    } else {
      await Hive.initFlutter();
    }
    final clientNames = <String>{};
    try {
      final clientNamesList  = store.getStringList(clientNamespace) ?? [];
      clientNames.addAll(clientNamesList);
    } catch (e, s) {
      Logs().w('Client names in store are corrupted', e, s);
      await store.remove(clientNamespace);
    }
    if (clientNames.isEmpty) {
      clientNames.add(await PlatformInfos.clientName);
      await store.setStringList(clientNamespace, clientNames.toList());
    }
    final clients = clientNames.map(createClient).toList();
    if (initialize) {
      await Future.wait(clients.map((client) => client
          .init(
            waitForFirstSync: false,
            waitUntilLoadCompletedLoaded: false,
          )
          .catchError((e, s) => Logs().e('Unable to initialize client', e, s))));
    }
    if (clients.length > 1 && clients.any((c) => !c.isLogged())) {
      final loggedOutClients = clients.where((c) => !c.isLogged()).toList();
      for (final client in loggedOutClients) {
        Logs().w('Multi account is enabled but client ${client.userID} is not logged in. Removing...');
        clientNames.remove(client.clientName);
        clients.remove(client);
      }
      await store.setStringList(clientNamespace, clientNames.toList());
    }
    return clients;
  }

  static Future<void> addClientNameToStore(String clientName,  SharedPreferences store,) async {
    final clientNamesList = store.getStringList(clientNamespace) ?? [];
    clientNamesList.add(clientName);
    await store.setStringList(clientNamespace, clientNamesList);
  }

  static Future<void> removeClientNameFromStore(
      String clientName,
      SharedPreferences store,
      ) async {
    final clientNamesList = store.getStringList(clientNamespace) ?? [];
    clientNamesList.remove(clientName);
    await store.setStringList(clientNamespace, clientNamesList);
  }

  static NativeImplementations get nativeImplementations => kIsWeb
      ? const NativeImplementationsDummy()
      : NativeImplementationsIsolate(compute);

  static Client createClient(String clientName) => Client(
        clientName,
        //httpClient: PlatformInfos.isAndroid ? CustomHttpClient.createHTTPClient() : null,
        verificationMethods: {
          KeyVerificationMethod.numbers,
          if (kIsWeb || PlatformInfos.isMobile || PlatformInfos.isLinux) KeyVerificationMethod.emoji,
        },
        importantStateEvents: <String>{
          // To make room emotes work
          'im.ponies.room_emotes',
          // To check which story room we can post in
          EventTypes.RoomPowerLevels,
        },
        logLevel: kReleaseMode ? Level.error : Level.verbose,
        databaseBuilder: FlutterHiveCollectionsDatabase.databaseBuilder,
        supportedLoginTypes: {AuthenticationTypes.password, if (PlatformInfos.isMobile || PlatformInfos.isWeb || PlatformInfos.isMacOS) AuthenticationTypes.sso},
        nativeImplementations: nativeImplementations,
        customImageResizer: PlatformInfos.isMobile ? customImageResizer : null,
      );
}
