import 'dart:convert';
import 'dart:developer';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/size_string.dart';
import '../../pages/chat/chat.dart';
import 'matrix_file_extension.dart';
import 'matrix_locals.dart';

extension LocalizedBody on Event {
/*  Future<LoadingDialogResult<MatrixFile?>> _getFile(BuildContext context) async => await
      showFutureLoadingDialog(
        context: context,
        future: downloadAndDecryptAttachment,
      );*/
  Future<MatrixFile?> _getFile(BuildContext context) async => await downloadAndDecryptAttachment();

  Future<bool> saveFile(BuildContext context) async {
    try {
      final matrixFile = await _getFile(context);

      await matrixFile?.save(context);
      return true;
    } on Exception catch (e) {
      Logs().e("saveFile $e");
      return false;
    }
    //await matrixFile.result?.save(context);
  }

  void openFile(BuildContext context) async {
    final matrixFile = await _getFile(context);

    await matrixFile?.openTemporarily();
    //await matrixFile.result?.openTemporarily();
  }

  void shareFile(BuildContext context) async {
    final matrixFile = await _getFile(context);

    matrixFile?.share(context);
    //matrixFile.result?.share(context);
  }

  bool get isAttachmentSmallEnough =>
      infoMap['size'] is int &&
          infoMap['size'] < room.client.database!.maxFileSize;

  bool get isThumbnailSmallEnough =>
      thumbnailInfoMap['size'] is int &&
          thumbnailInfoMap['size'] < room.client.database!.maxFileSize;

  bool get showThumbnail =>
      [MessageTypes.Image, MessageTypes.Sticker, MessageTypes.Video]
          .contains(messageType) &&
          (kIsWeb ||
              isAttachmentSmallEnough ||
              isThumbnailSmallEnough ||
              (content['url'] is String));

  String? get sizeString =>
      content
          .tryGetMap<String, dynamic>('info')
          ?.tryGet<int>('size')
          ?.sizeString;


  Size? getImageDimensions() {
    final infoMap = this.infoMap;
    final thumbnailInfoMap = this.thumbnailInfoMap;
    MatrixFile? localFile;
    if (status.isSending) {
      Logs().v("This event is $eventId and the keys are ${room.sendingFilePlaceholders.keys.toString()}");
      if (room.sendingFilePlaceholders[eventId]!.mimeType.contains("video")){
        localFile = room.sendingFilePlaceholders[eventId] as MatrixVideoFile;
        if (localFile is MatrixVideoFile) {
          if (localFile.width ==null || localFile.height == null){
            return null;
          }
          return Size(localFile.width!.toDouble(), localFile.height!.toDouble());
        } else if (localFile is MatrixImageFile) {
          if (localFile.width ==null || localFile.height == null){
            return null;
          }
          return Size(localFile.width!.toDouble(), localFile.height!.toDouble());
        }
      }else if (room.sendingFilePlaceholders[eventId]!.mimeType.contains("image")){
        localFile = room.sendingFilePlaceholders[eventId] as MatrixImageFile;
        if (localFile is MatrixImageFile) {
          if (localFile.width ==null || localFile.height == null){
            return null;
          }
          return Size(localFile.width!.toDouble(), localFile.height!.toDouble());
        }
      }
    }

    final double? width = double.tryParse(infoMap['w'].toString()) ?? double.tryParse(thumbnailInfoMap['w'].toString());
    final double? height = double.tryParse(infoMap['h'].toString()) ?? double.tryParse(thumbnailInfoMap['h'].toString());

    if (height == null || width == null ){
      //Logs().v('Event extension - getImageDimensions: No value for height and width.');
      return null;
    }
    return Size(width, height);
  }

  String calcQuillTextToPlainText({required MatrixLocals locals, bool withSenderNamePrefix = true}) {
    final i18n = locals;
    final String? serializedDelta = content['serialized_delta'] as String?;
    String localizedBody; // Replace with the appropriate getter for the serialized delta
    if (serializedDelta != null) {
      final delta = quill.Delta.fromJson(jsonDecode(serializedDelta));
      final document = quill.Document.fromDelta(delta);
      localizedBody = document.getPlainText(0, document.length);
    } else {
      localizedBody = 'Sent a composed message';
    }
    if (withSenderNamePrefix){
      if ((!room.isDirectChat || room.directChatMatrixID != room.lastEvent?.senderId) && type == EventTypes.Message) {
        final senderNameOrYou = senderId == room.client.userID ? i18n.you : senderFromMemoryOrFallback.calcDisplayname(i18n: i18n);
        localizedBody = '$senderNameOrYou: $localizedBody';
      }
    }

    return localizedBody;
  }

/*
  Event getPageMeDisplayEvent(Timeline timeline) {
    if (redacted) {
      return this;
    }
    if (hasAggregatedEvents(timeline, RelationshipTypes.edit)) {
      // alright, we have an edit
      final allEditEvents = aggregatedEvents(timeline, RelationshipTypes.edit)
      // we only allow edits made by the original author themself
          .where((e) => e.senderId == senderId && (e.messageType== CustomMessageTypes.quillText || e.type == EventTypes.Message) )
          .toList();
      // we need to check again if it isn't empty, as we potentially removed all
      // aggregated edits
      if (allEditEvents.isNotEmpty) {
        allEditEvents.sort((a, b) => a.originServerTs.millisecondsSinceEpoch -
            b.originServerTs.millisecondsSinceEpoch >
            0
            ? 1
            : -1);
        final rawEvent = allEditEvents.last.toJson();
        // update the content of the new event to render
        Logs().v("getPageMeDisplayEvent - ${rawEvent.toString()}");
        if (rawEvent['content']['m.new_content'] is Map) {
          rawEvent['content'] = rawEvent['content']['m.new_content'];
        }
        return Event.fromJson(rawEvent, room);
      }else{
        Logs().v("getPageMeDisplayEvent - allEditEvents was empty ");
      }
    }
    return this;
  }
*/

}