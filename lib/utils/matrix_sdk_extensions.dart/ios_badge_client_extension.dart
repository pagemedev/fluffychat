import 'dart:convert';

import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:matrix/matrix.dart';

import 'package:pageMe/utils/platform_infos.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config/setting_keys.dart';
import '../../widgets/matrix.dart';

extension IosBadgeClientExtension on Client {
  void updateIosBadge() async {
    Logs().v("updateIosBadge - started");
    if (PlatformInfos.isIOS) {

      // Workaround for iOS not clearing notifications with fcm_shared_isolate
      if (!rooms.any((r) => r.membership == Membership.invite || (r.notificationCount > 0))) {
        // ignore: unawaited_futures
        Logs().v("updateIosBadge - clearing IOS and MacOS badges");
        await FlutterLocalNotificationsPlugin().cancelAll();
        await FlutterAppBadger.removeBadge();
      }
    }
    Logs().v("updateIosBadge - ended");
  }

  void updateMacOSBadge({required Room room}) async {
    Logs().v("updateMacOSBadge - started");
    if (PlatformInfos.isMacOS) {
      // Workaround for iOS not clearing notifications with fcm_shared_isolate
      if (room.membership == Membership.invite || (room.notificationCount > 0)) {
        final id = await mapRoomIdToInt(room.id);
        // ignore: unawaited_futures
        final List<Room> filteredRooms = room.client.rooms.where(
              (room) {
            return room.notificationCount != 0;
          },
        ).toList();

        int totalUnreadMessages = 0;

        for (final room in filteredRooms) {
          totalUnreadMessages += room.notificationCount;
        }

        Logs().v("updateMacOSBadge - clearing MacOS badges");
        await FlutterLocalNotificationsPlugin().cancel(id);
        if (totalUnreadMessages - room.notificationCount == 0){
          Logs().v("updateMacOSBadge - remove badge");
          await FlutterAppBadger.updateBadgeCount(totalUnreadMessages - room.notificationCount);
          await FlutterAppBadger.removeBadge();
        }else{
          Logs().v("updateMacOSBadge - decrement badge count");
          await FlutterAppBadger.updateBadgeCount(totalUnreadMessages - room.notificationCount);
        }
      }
    }
    Logs().v("updateMacOSBadge - ended");
  }

  Future<int> mapRoomIdToInt(String roomId) async {
    final store = await SharedPreferences.getInstance();
    final idMap = Map<String, int>.from(jsonDecode(store.getString(SettingKeys.notificationCurrentIds) ?? '{}'));
    int? currentInt;
    try {
      currentInt = idMap[roomId];
    } catch (_) {
      currentInt = null;
    }
    if (currentInt != null) {
      return currentInt;
    }
    var nCurrentInt = 0;
    while (idMap.values.contains(nCurrentInt)) {
      nCurrentInt++;
    }
    idMap[roomId] = nCurrentInt;
    await store.setString(SettingKeys.notificationCurrentIds, json.encode(idMap));
    return nCurrentInt;
  }


}
