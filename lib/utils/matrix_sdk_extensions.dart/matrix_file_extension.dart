import 'dart:io';
import 'dart:math';
import 'package:desktop_lifecycle/desktop_lifecycle.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_saver/file_saver.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:matrix/matrix.dart';
import 'package:open_filex/open_filex.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/utils/size_string.dart';
import 'package:pageMe/widgets/local_notifications_extension.dart';
import 'package:pageMe/widgets/matrix.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share_plus/share_plus.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:universal_html/html.dart' as html;


List<int> foregroundNotificationIds = [];

@pragma('vm:entry-point')
void _handleBackgroundNotification(NotificationResponse response) async {
  Logs().d('We got foreground response ${response.actionId}, ${response.payload}, ${response.id}, ${response.payload}, ${response.notificationResponseType.name}');
  if (response.actionId == 'OPEN_FILE') {
    Logs().d('We got ${response.actionId}');
    if (response.payload == null) {
      Logs().d('But payload is ${response.actionId}');
      return;
    }
    Logs().d('Payload is ${response.payload}');
    final OpenResult result;
    try {
      if (await Permission.manageExternalStorage.isGranted) {
        result = await OpenFilex.open(response.payload);
        Logs().d('opening file ${result.message}, ${result.type.name}');
      } else {
        await Permission.manageExternalStorage.request();
        result = await OpenFilex.open(response.payload);
        Logs().d('permissions were not set, now opening file ${result.message}, ${result.type.name}');
      }
    } on Exception catch (e) {
      Logs().e('Error opening file from background notification: $e');
    }
  } else {
    Logs().w('We unknown actionId ${response.actionId}');
  }
}

extension MatrixFileExtension on MatrixFile {
  MimeType? getMimeType(String mimeType) {
    for (final MimeType mt in MimeType.values) {
      if (mt.type == mimeType || mt.type.contains(mimeType)) {
        return mt;
      }
    }
    return null; // Return null if the MIME type is not found in the enum
  }

  Future<String?> saveToApple({required bool saveAs}) async {
    final bool status = await Permission.storage.isGranted;
    if (!status) await Permission.storage.request();
    return saveAs
        ? await FileSaver.instance.saveAs(
            name: name.split('.').first,
            bytes: bytes,
            ext: name.split('.').last,
            mimeType: getMimeType(mimeType) ?? MimeType.other,
          )
        : await FileSaver.instance.saveFile(
            name: name.split('.').first,
            bytes: bytes,
            ext: name.split('.').last,
            mimeType: getMimeType(mimeType) ?? MimeType.other,
          );
  }

  Future<String?> saveToAndroid({required bool saveAs}) async {
    final bool status = await Permission.storage.isGranted;
    if (!status) await Permission.storage.request();
    return saveAs
        ? await FileSaver.instance.saveAs(
            name: name.split('.').first,
            bytes: bytes,
            ext: name.split('.').last,
            mimeType: getMimeType(mimeType) ?? MimeType.other,
          )
        : await FileSaver.instance.saveFile(
            name: name.split('.').first,
            bytes: bytes,
            ext: name.split('.').last,
            mimeType: getMimeType(mimeType) ?? MimeType.other,
          );
  }

  Future<File?> saveToDesktop() async {
    final String? downloadPath = await FilePicker.platform.saveFile(
      dialogTitle: 'Save file',
      fileName: name,
      type: filePickerFileType,
    );
    if (downloadPath == null) return null;

    return await File(downloadPath).writeAsBytes(bytes);
  }

  Future<void> showSaveSnackBar(BuildContext context, String? downloadPath) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          'File has been saved to: $downloadPath',
          style: TextStyle(color: Theme.of(context).colorScheme.onTertiaryContainer),
        ),
        backgroundColor: Theme.of(context).colorScheme.tertiaryContainer,
        closeIconColor: Theme.of(context).colorScheme.onTertiaryContainer,
      ),
    );
    return Future.value();
  }

  int _generateNotificationId() {
    // Initialize Random with current time in milliseconds as seed
    final Random random = Random(DateTime.now().millisecondsSinceEpoch);
    // Generate random number and fit it within 32-bit integer limits
    return random.nextInt(0x7FFFFFFF);
  }


  void _handleForegroundNotification(NotificationResponse response) async {
    print('handleForegroundNotification: ${response.payload}, ${response.id}, ${response.actionId}, ${response.input}, ${response.notificationResponseType}');
    if (response.actionId == 'OPEN_FILE') {
      if (response.payload == null) {
        return;
      }
      await open(response.payload!);
    } else if (response.actionId == 'OPEN_FILE_LOCATION') {
      if (response.payload == null) {
        return;
      }
      await open(dirname(response.payload!));
    }
  }

  Future<void> _showSaveNotification(String? downloadPath, BuildContext context) async {
    Logs().v("Notification setup started");
    if (PlatformInfos.isMobile || PlatformInfos.isMacOS) {
      final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
      final List<AndroidNotificationAction> androidActions = [
        const AndroidNotificationAction('OPEN_FILE', 'Open file', showsUserInterface: true),
        //const AndroidNotificationAction('OPEN_FILE_LOCATION', 'Open file location', showsUserInterface: true)
      ];

      final List<DarwinNotificationAction> appleActions = [
        DarwinNotificationAction.plain('OPEN_FILE', 'Open file', options: {DarwinNotificationActionOption.foreground}),
        if (PlatformInfos.isMacOS) DarwinNotificationAction.plain('OPEN_FILE_LOCATION', 'Open file location', options: {DarwinNotificationActionOption.foreground})
      ];

      final List<DarwinNotificationCategory> appleNotificationCategories = [
        DarwinNotificationCategory(
          'FILE_HANDLER',
          actions: appleActions,
          options: <DarwinNotificationCategoryOption>{
            DarwinNotificationCategoryOption.hiddenPreviewShowTitle,
          },
        )
      ];
      // Initialize settings for Android
      final InitializationSettings initializationSettings = InitializationSettings(
        android: const AndroidInitializationSettings('@drawable/ic_notifications_logo'),
        iOS: DarwinInitializationSettings(notificationCategories: appleNotificationCategories),
        macOS: DarwinInitializationSettings(
          notificationCategories: appleNotificationCategories,
        ),
      );

      await flutterLocalNotificationsPlugin.initialize(
        initializationSettings,
        onDidReceiveNotificationResponse: _handleForegroundNotification,
        //onDidReceiveBackgroundNotificationResponse: handleBackgroundNotification,
      );
      // Details for Android Notification
      final AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
        FlavorConfig.pushNotificationsForegroundChannelId,
        FlavorConfig.pushNotificationsForegroundChannelId,
        channelDescription: 'Notifications whilst app in running',
        importance: Importance.max,
        priority: Priority.high,
        showWhen: true,
        playSound: true,
        enableLights: true,
        color: Colors.blue,
        channelAction: AndroidNotificationChannelAction.createIfNotExists,
        actions: androidActions,
      );

      // Notification Details
      final NotificationDetails platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        macOS: const DarwinNotificationDetails(categoryIdentifier: 'FILE_HANDLER'),
        iOS: const DarwinNotificationDetails(categoryIdentifier: 'FILE_HANDLER'),
      );

      final notificationId = _generateNotificationId();

      foregroundNotificationIds.add(notificationId);

      Logs().v("Notification setup finished");
      await flutterLocalNotificationsPlugin.show(
        notificationId, // Notification ID
        'Download Complete',
        PlatformInfos.isIOS
            ? 'Saved to Files\nLocation: ${isBusiness() ? 'PageMe Business' : 'PageMe Public'}/${downloadPath?.split('/Documents/').last}'
            : 'Saved to Files\nLocation: Download/${downloadPath?.split('/Download/').last}',
        platformChannelSpecifics, payload: downloadPath,
      );
      Logs().v("Notification shown and completed");
    }

    if (PlatformInfos.isDesktop && !PlatformInfos.isMacOS) {
      final matrix = Matrix.of(context);
      const title = "Download Complete";
      final body = "File has been saved to: $downloadPath";
      /* if (PlatformInfos.isWeb) {
          displayWebDownloadNotification(title, body, downloadPath);
        } else if (PlatformInfos.isLinux) {
          await displayLinuxDownloadNotification(title, body, downloadPath);
        } else*/
      if (PlatformInfos.isWindows) {
        await matrix.displayWindowsDownloadNotification(title, body, downloadPath);
      } /* else if (PlatformInfos.isMacOS) {
          await displayMacOSDownloadNotification(title, body, downloadPath);
        }*/
    }
  }

  Future<void> openTemporarily() async {
    Logs().v('Starting temporary file opening procedure.');
    File? tempFile;
    if (PlatformInfos.isWeb){
      _openTempFileWeb();
    }

    if (PlatformInfos.isMobile || PlatformInfos.isDesktop) {
      await _openTempFile(tempFile);
    }
  }

  Future<void> open(String downloadPath) async {
    final OpenResult result;
    try {
      if (await Permission.manageExternalStorage.isGranted) {
        result = await OpenFilex.open(downloadPath);
        print('opening file ${result.message}, ${result.type.name}');
      } else {
        await Permission.manageExternalStorage.request();
        result = await OpenFilex.open(downloadPath);
        print('opening file ${result.message}, ${result.type.name}');
      }
    } on Exception catch (e) {
      print('Error opening file from background notification: $e');
    }
  }

  Future<void> save(BuildContext context) async {
    String? downloadPath;

    if (PlatformInfos.isAndroid) {
      downloadPath = await saveToAndroid(saveAs: true);
      Logs().v('File Save - Path: $downloadPath');
    }

    if (PlatformInfos.isIOS || PlatformInfos.isMacOS) {
      downloadPath = await saveToApple(saveAs: false);
      Logs().v('File Save - Path: $downloadPath');
    }

    if (PlatformInfos.isWeb) {
      _webDownload();
      return;
    }

    if (PlatformInfos.isDesktop && !PlatformInfos.isMacOS) {
      final file = await saveToDesktop();
      downloadPath = file?.path;
    }

    if (downloadPath != null){
      await _showSaveNotification(downloadPath, context);
    }
  }

  Future<String> getDownloadPathAndroid() async {
    final directory = await getDownloadDirectoryAndroid();
    return '${directory.path}/$name';
  }

  Future<Directory> getDownloadDirectoryAndroid() async {
    final defaultDownloadDirectory = Directory('/storage/emulated/0/Download');
    if (await defaultDownloadDirectory.exists()) {
      return defaultDownloadDirectory;
    }
    return await getApplicationDocumentsDirectory();
  }

  FileType get filePickerFileType {
    if (this is MatrixImageFile) return FileType.image;
    if (this is MatrixAudioFile) return FileType.audio;
    if (this is MatrixVideoFile) return FileType.video;
    return FileType.any;
  }

  void _webDownload() {
    html.AnchorElement(
      href: html.Url.createObjectUrlFromBlob(
        html.Blob(
          [bytes],
          mimeType,
        ),
      ),
    )
      ..download = name
      ..click();
  }

  void share(BuildContext context) async {
    // Workaround for iPad from
    // https://github.com/fluttercommunity/plus_plugins/tree/main/packages/share_plus/share_plus#ipad
    final box = context.findRenderObject() as RenderBox?;

    await Share.shareXFiles(
      [XFile.fromData(bytes, name: name, mimeType: mimeType)],
      sharePositionOrigin: box == null ? null : box.localToGlobal(Offset.zero) & box.size,
    );
    return;
  }

  MatrixFile get detectFileType {
    if (msgType == MessageTypes.Image) {
      return MatrixImageFile(bytes: bytes, name: name);
    }
    if (msgType == MessageTypes.Video) {
      return MatrixVideoFile(bytes: bytes, name: name);
    }
    if (msgType == MessageTypes.Audio) {
      return MatrixAudioFile(bytes: bytes, name: name);
    }
    return this;
  }

  String get sizeString => size.sizeString;

  void _openTempFileWeb() {
    try {
      final blob = html.Blob([bytes], mimeType);
      final url = html.Url.createObjectUrlFromBlob(blob);
      html.window.open(url, name);
      return;
    } on Exception catch (e) {
      Logs().e("Failed to open temp file: $e");
    }
  }

  Future<void> _openTempFile(File? tempFile) async {
    try {
      // Get temporary directory path
      final Directory tempDir = await getTemporaryDirectory();
      final String tempPath = tempDir.path;
      Logs().v('Obtained temporary directory path: $tempPath');

      // Create a temporary file instance
      tempFile = File('$tempPath/$name');
      Logs().v('Temporary file will be created at: ${tempFile.path}');

      // Write bytes to temporary file
      await tempFile.writeAsBytes(bytes, flush: true, mode: FileMode.write).then((file) async {
        Logs().v('Successfully wrote bytes to temporary file.');

        // Open the file and log the result
        final OpenResult result = await OpenFilex.open(file.path);
        Logs().v('Opening file with result message: ${result.message}, type: ${result.type.name}');
      });
    } catch (e) {
      // Log any exceptions that occur
      Logs().e('Error in temporary file operations: $e');
    } finally {
      // Delete the temporary file
      Future.delayed(const Duration(seconds: 60), () async {
        if (tempFile != null) {
          await tempFile.delete();
        }
        Logs().v('Deleted temporary file.');
      });
    }
  }

}



