import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:tray_manager/tray_manager.dart';

import 'package:window_manager/window_manager.dart';
import 'package:windows_single_instance/windows_single_instance.dart';

import '../config/flavor_config.dart';

class WindowManagerWidget extends StatefulWidget {
  final Widget child;
  const WindowManagerWidget({super.key, required this.child});

  @override
  State<WindowManagerWidget> createState() => _WindowManagerWidgetState();

  static Future<void> initWindow({List<String>? args}) async {
    await WindowsSingleInstance.ensureSingleInstance(args ?? [], FlavorConfig.windowsPackageName, onSecondWindow: (args) {
    });
    await windowManager.ensureInitialized();

    final WindowOptions windowOptions = WindowOptions(
      size: const Size(1066, 600),
      center: true,
      backgroundColor: Colors.transparent,
      skipTaskbar: false,
      titleBarStyle: PlatformInfos.isMacOS ? TitleBarStyle.normal : TitleBarStyle.hidden,
      windowButtonVisibility: true,
    );
    await windowManager.waitUntilReadyToShow(windowOptions, () async {
      await windowManager.setPreventClose(true);
      await windowManager.maximize();
      await windowManager.show();
      await windowManager.focus();
    });
  }
}

class _WindowManagerWidgetState extends State<WindowManagerWidget> with WindowListener, TrayListener {
  static const _kIconTypeOriginal = 'original';
  String _iconType = _kIconTypeOriginal;
  static final String trayIcon = isBusiness() ? 'assets/pageme_business_logo_rounded.png' : 'assets/pageme_public_logo.png';
  static final String trayIconWindows = isBusiness() ? 'assets/pageme_business_logo_rounded.ico' : 'assets/pageme_public_logo.ico';
  @override
  void initState() {
    trayManager.addListener(this);
    windowManager.addListener(this);
    _init();
    super.initState();
  }

  @override
  void dispose() {
    trayManager.removeListener(this);
    windowManager.removeListener(this);
    super.dispose();
  }

  void _init() async {
    await trayManager.setIcon(
      PlatformInfos.isWindows ? trayIconWindows : trayIcon,
    );
    final Menu menu = Menu(
      items: [
        MenuItem(
          key: 'show_window',
          label: FlavorConfig.applicationName,
        ),
        MenuItem(
          key: 'hide_window',
          label: "Hide",
        ),
        MenuItem.separator(),
        MenuItem(
          key: 'exit_app',
          label: 'Exit',
        ),
      ],
    );
    await trayManager.setContextMenu(menu);
    setState(() {});
  }

  void _handleSetIcon(String iconType) async {
    _iconType = iconType;
    String iconPath = PlatformInfos.isWindows ? trayIconWindows : trayIcon;

    if (_iconType == 'original') {
      iconPath = PlatformInfos.isWindows ? trayIconWindows : trayIcon;
    }

    await windowManager.setIcon(iconPath);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PlatformInfos.isMacOS
          ? null
          : PreferredSize(
              preferredSize: const Size.fromHeight(kWindowCaptionHeight),
              child: WindowCaption(
                brightness: Theme.of(context).brightness,
                title: Text(FlavorConfig.applicationName),
              ),
            ),
      body: widget.child,
    );
  }

  @override
  Future<void> onTrayIconMouseDown() async {
    await windowManager.show();
  }

  @override
  Future<void> onTrayIconRightMouseDown() async {
    await trayManager.popUpContextMenu();
  }

  @override
  void onTrayMenuItemClick(MenuItem menuItem) async {
    switch (menuItem.key) {
      case 'show_window':
        await windowManager.focus();
        break;
      case 'hide_window':
        await windowManager.hide();
        break;
      case 'exit_app':
        await windowManager.destroy();
        break;
    }
  }

  @override
  void onWindow() {
    setState(() {});
  }

  @override
  void onWindowFocus() {
    setState(() {});
  }

  @override
  Future<void> onWindowClose() async {
    //context.go('rooms');
    await windowManager.hide();
  }

  @override
  void onWindowEvent(String eventName) {
    Logs().v('[WindowManager] onWindowEvent: $eventName');
  }
}
