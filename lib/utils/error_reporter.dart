import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/utils/localized_exception_extension.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io' show Platform, kIsWeb;
import 'package:url_launcher/url_launcher.dart';

class ErrorReporter {
  final BuildContext context;
  final String? message;

  const ErrorReporter(this.context, [this.message]);

  void onErrorCallback(Object error, [StackTrace? stackTrace]) async {
    Logs().e(message ?? 'Error caught', error, stackTrace);
    final consent = await showOkCancelAlertDialog(
      context: context,
      title: error.toLocalizedString(context),
      message: "Oh no. Something went wrong. Please try again later. If you want, you can report the bug to the developers.",
      okLabel: "report",
      cancelLabel: L10n.of(context)!.close,
    );
    if (consent != OkCancelResult.ok) return;
    final os = kIsWeb ? 'web' : Platform.operatingSystem;
    final version = await PlatformInfos.getVersion();
    final description = '''
- Operating system: $os
- Version: $version

### Exception
$error

### StackTrace
$stackTrace
''';
/*    launchUrl(
      FlavorConfig.newIssueUrl.resolveUri(
        Uri(
          queryParameters: {
            'title': '[BUG]: ${message ?? error.toString()}',
            'body': description,
          },
        ),
      ),
      mode: LaunchMode.externalApplication,
    );*/
  }

  Future<void> createGitlabIssue(String projectId, String title, String description) async {
    var url = 'https://gitlab.com/api/v4/projects/$projectId/issues';
    var response = await http.post(
      Uri.parse(url),
      headers: {'Content-Type': 'application/json', 'PRIVATE-TOKEN': 'YOUR_PRIVATE_TOKEN'},
      body: json.encode({
        'title': title,
        'description': description,
      }),
    );

    if (response.statusCode == 200) {
      print('Issue created successfully');
    } else {
      print('Failed to create issue');
    }
  }
}
