import 'dart:async';

import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/localized_exception_extension.dart';
import 'package:pageMe/utils/resize_image.dart';

import '../widgets/file_upload_helper.dart';
import '../widgets/matrix.dart';
import 'logger_functions.dart';

class ChatService {
  static const int minSizeToCompress = 20* 1000 * 1024;
  ChatService._privateConstructor();

  // Static instance
  static final ChatService _instance = ChatService._privateConstructor();

  // Factory constructor
  factory ChatService() {
    return _instance;
  }

  final statusUpdatesController = StreamController<FileUploadUpdate>.broadcast();

  Stream<FileUploadUpdate> get statusUpdates => statusUpdatesController.stream;

  void dispose(){
    statusUpdatesController.close();
  }

  Future<void> send({
    required List<MatrixFile> files,
    required Set<String> roomIds,
    required BuildContext context,
    required bool isSharing,
    VoidCallback? onStart,
    VoidCallback? onComplete,
  }) async {
    final client = Matrix.of(context).client;
    final scaffoldMessenger = ScaffoldMessenger.of(context);
    final Set<String> roomIdsList = Set<String>.from(roomIds);


    if (isSharing && onStart != null){
      onStart();
    }

    try {
      int participantIndex = 0;
      for (final roomId in roomIdsList) {
        participantIndex++;
        final room = client.getRoomById(roomId);
        if (room == null) continue;
        for (int i = 0; i < files.length; i++) {
          MatrixFile file = files[i];
          MatrixImageFile? thumbnail;
          if (file is MatrixVideoFile && file.bytes.length > minSizeToCompress) {
            statusUpdatesController.add(FileUploadUpdate(
                file: files[i],
                status: "Compressing",
                fileIndex: i + 1,
                totalFiles: files.length,
                participantIndex: participantIndex,
                totalParticipant: roomIdsList.length,
                currentParticipant: room));
            file = await file.resizeVideo();
            thumbnail = await file.getVideoThumbnail();
          }
          Logs().i("Sending file ${i + 1}");
          Logs().i("File: ${file.info}");
          statusUpdatesController.add(FileUploadUpdate(
              file: file,
              status: "Uploading",
              fileIndex: i + 1,
              totalFiles: files.length,
              participantIndex: participantIndex,
              totalParticipant: roomIdsList.length,
              currentParticipant: room));
          try {
            await room.sendFileEvent(
              file,
              thumbnail: thumbnail,
              //shrinkImageMaxDimension: 1600,
            );
            statusUpdatesController.add(
              FileUploadUpdate(
                  file: file,
                  status: "Upload Complete",
                  fileIndex: i + 1,
                  totalFiles: files.length,
                  participantIndex: participantIndex,
                  totalParticipant: roomIdsList.length,
                  currentParticipant: room),
            );
          } catch (e) {
            statusUpdatesController.add(FileUploadUpdate(
                file: file,
                status: "Error occurred during upload",
                fileIndex: i + 1,
                totalFiles: files.length,
                participantIndex: participantIndex,
                totalParticipant: roomIdsList.length,
                currentParticipant: room));
            participantIndex = 0;
          }
        }
      }
      participantIndex = 0;
    } catch (e, s) {
      scaffoldMessenger.clearSnackBars();
      scaffoldMessenger.showSnackBar(
        SnackBar(content: Text((e).toLocalizedString(context))),
      );
      Logs().e('Error (ChatService - send): $e, $s');
    }finally{
      if (isSharing && onComplete != null){
        onComplete();
      }
    }
  }
}