import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/utils/date_time_extension.dart';

import '../config/setting_keys.dart';
import 'famedlysdk_store.dart';
import 'logger_functions.dart';

class TimeStampSetter extends StatefulWidget {
  const TimeStampSetter({
    Key? key,
  }) : super(key: key);

  @override
  State<TimeStampSetter> createState() => _TimeStampSetterState();
}

class _TimeStampSetterState extends State<TimeStampSetter> {
  final widgetKeyLarge = GlobalKey();
  final widgetKeySmall = GlobalKey();
  Store store = Store();

  Future<void> postFrameCallback(_) async {
    if ((await store.getItem(SettingKeys.timeStampSizeLarge) == null) || (await store.getItem(SettingKeys.timeStampSizeSmall) == null)) {
      final contextLarge = widgetKeyLarge.currentContext;
      final contextSmall = widgetKeySmall.currentContext;
      if (contextLarge == null || contextSmall == null) return;

      final Size? largeSize = contextLarge.size;
      final Size? smallSize = contextSmall.size;

      try {
        await store.setItem(SettingKeys.timeStampSizeLarge, largeSize?.width.toString());
        await store.setItem(SettingKeys.timeStampSizeSmall, smallSize?.width.toString());
      } catch (e, s) {
        Logs().e( '[TimeStampSetter] Error', e, s);
      }
      Logs().i( '[TimeStampSetter] Value successfully stored: ${await store.getItem(SettingKeys.timeStampSizeLarge)}');
      Logs().i( '[TimeStampSetter] Value successfully stored: ${await store.getItem(SettingKeys.timeStampSizeSmall)}');
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.endOfFrame.then(
      (_) {
        if (mounted) postFrameCallback(context);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    const double tickSize = 12;
    return Column(
      children: [
        Row(
          key: widgetKeyLarge,
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              width: 3,
            ),
            Text(
              DateTime.now().localizedTimeOfDay(context),
              style: const TextStyle(fontSize: 11),
            ),
            const SizedBox(
              width: 3,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 3.0),
              child: Icon(
                Icons.done_all,
                size: tickSize,
                color: Theme.of(context).colorScheme.onPrimary,
              ),
            )
          ],
        ),
        Row(
          key: widgetKeySmall,
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              width: 3,
            ),
            Text(
              DateTime.now().localizedTimeOfDay(context),
              style: const TextStyle(fontSize: 11),
            ),
            const SizedBox(
              width: 3,
            ),
          ],
        )
      ],
    );
  }
}
