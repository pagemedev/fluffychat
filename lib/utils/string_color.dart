import 'package:flutter/material.dart';
import 'package:pageMe/utils/logger_functions.dart';
import 'package:universal_html/html.dart';

extension StringColor on String {
  Color get color {
    var number = 0.0;
    for (var i = 0; i < length; i++) {
      number += codeUnitAt(i);
    }
    number = (number % 12) * 25.5;
    return HSLColor.fromAHSL(1, number, 1, 0.35).toColor();
  }

  Color get darkColor {
    var number = 0.0;
    for (var i = 0; i < length; i++) {
      number += codeUnitAt(i);
    }
    number = (number % 12) * 25.5;
    return HSLColor.fromAHSL(1, number, 1, 0.2).toColor();
  }

  Color generateTextColorDark(Color backgroundColor) {
    var total = 0.0;
    for (var i = 0; i < length; i++) {
      total += codeUnitAt(i);
    }
    var hue = (total % 12) * 25.5;
    var lightness = 0.45;
    if (backgroundColor.computeLuminance() > 0.5) {
      lightness -= length / 20.0;
    } else {
      lightness += length / 20.0;
      if (lightness >= 1){
        lightness = 1;
      }
    }
    var hslColor = HSLColor.fromAHSL(1, hue, 0.5, lightness);
    return hslColor.toColor();
  }

  Color generateTextColorLight(Color backgroundColor) {
    var total = 0.0;
    for (var i = 0; i < length; i++) {
      total += codeUnitAt(i);
    }
    var hue = (total % 12) * 25.5;
    var lightness = 0.9;
    if (backgroundColor.computeLuminance() > 0.5) {
      lightness -= length / 20.0;
      if (lightness <= 0){
        lightness = 0.1;
      }
    } else {
      lightness += length / 20.0;
      if (lightness >= 1){
        lightness = 1;
      }

    }
    final hslColor = HSLColor.fromAHSL(1, hue, 0.5, lightness);
    return hslColor.toColor();
  }

  Color get lightColor {
    var number = 0.0;
    for (var i = 0; i < length; i++) {
      number += codeUnitAt(i);
    }
    number = (number % 12) * 25.5;
    return HSLColor.fromAHSL(1, number, 0.65, 0.6).toColor();
  }
}

extension ColorExtension on Color {
  /// Convert the color to a darken color based on the [percent]
  Color darken([int percent = 40]) {
    assert(1 <= percent && percent <= 100);
    final value = 1 - percent / 100;
    return Color.fromARGB(alpha, (red * value).round(), (green * value).round(),
        (blue * value).round());
  }

  Color lighten([int percent = 60]) {
    assert(1 <= percent && percent <= 100);
    final value = 1 - percent / 100;
    return Color.fromARGB(alpha, (red / value).round(), (green / value).round(),
        (blue / value).round());
  }
}
