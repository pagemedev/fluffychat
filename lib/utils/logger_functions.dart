import 'dart:io';

// ignore: unused_import
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';


bool logToFile = false;
bool logAll = false;

final PrettyPrinter debugPrinter = PrettyPrinter(
    methodCount: 2, // number of method calls to be displayed
    errorMethodCount: 1, // number of method calls if stacktrace is provided
    lineLength: 160, // width of the output
    colors: (kIsWeb) ? false : (Platform.isIOS) ? false : true, // Colorful log messages
    printEmojis: true, // Print an emoji for each log message
    printTime: false // Should each log print contain a timestamp,
    );

final Logger _loggerDebug = Logger(
  level: Level.debug,
  filter: (logAll) ? AlwaysShowDebugFilter() : null,
  printer: debugPrinter,
  output: (logToFile) ? FileOutput() : null,
);
final Logger _loggerInfo = Logger(
  level: Level.info,
  filter: (logToFile) ? AlwaysShowDebugFilter() : null,
  printer: debugPrinter,
  output: (logToFile) ? FileOutput() : null,
);
final Logger _loggerError = Logger(
  level: Level.error,
  filter: (logToFile) ? AlwaysShowDebugFilter() : null,
  printer: debugPrinter,
  output: (logToFile) ? FileOutput() : null,
);
final Logger _loggerWarning = Logger(
  level: Level.warning,
  filter: (logToFile) ? AlwaysShowDebugFilter() : null,
  printer: debugPrinter,
  output: (logToFile) ? FileOutput() : null,
);
final Logger _loggerWTF = Logger(
  level: Level.wtf,
  filter: (logToFile) ? AlwaysShowDebugFilter() : null,
  printer: debugPrinter,
  output: (logToFile) ? FileOutput() : null,
);

void loggerInfo({required String logMessage}) {
  _loggerInfo.log(Level.info, logMessage);
}

void loggerDebug({required String logMessage}) {
  _loggerDebug.log(Level.debug, logMessage);
}

void loggerError({required String logMessage}) {
  _loggerError.log(Level.error, logMessage);
}

void loggerWarning({required String logMessage}) {
  _loggerWarning.log(Level.warning, logMessage);
}

void loggerWTF({required String logMessage}) {
  _loggerWTF.log(Level.wtf, logMessage);
}

class AlwaysShowDebugFilter extends LogFilter {
  @override
  bool shouldLog(LogEvent event) {
    return true;
  }
}


class FileOutput extends LogOutput {
  FileOutput();

  late File file;

  @override
  Future<void> init() async {
    await super.init();
    final Directory directory = await getApplicationDocumentsDirectory();
    file = File('${directory.path}/logs-${DateTime.now()}.txt');
  }



  @override
  void output(OutputEvent event) async {
    if (file != null) {
      for (final line in event.lines) {
        await file.writeAsString("${line.toString()}\n",
            mode: FileMode.writeOnlyAppend);
      }
    } else {
      for (final line in event.lines) {
        print(line);
      }
    }
  }
}