/*
import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'package:file_uri_provider/file_uri_provider.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_local_notifications/src/platform_specifics/android/icon.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/flavor_config.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/event_extension.dart';
import 'package:pageMe/utils/voip/callkeep_manager.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pageMe/config/setting_keys.dart';
import 'package:pageMe/utils/client_manager.dart';
import 'package:pageMe/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import '../widgets/mxc_image_fetch.dart';
import 'famedlysdk_store.dart';

Map<String, List<Message>> notificationGroups = {};

Future<void> clearNotificationGroupForRoom(String roomId) async {
  final Store store = Store();

  if (notificationGroups.containsKey(roomId)) {
    notificationGroups.remove(roomId);

    final encodedNotificationGroups = notificationGroups.map((key, value) => MapEntry(
          key,
          value.map((message) => message.toJson()).toList(),
        ));

    await store.setItem(DataKeys.notificationGroups, json.encode(encodedNotificationGroups));
  }
}

Future<void> clearAllNotificationGroups() async {
  final Store store = Store();
  notificationGroups.clear();
  final encodedNotificationGroups = notificationGroups.map((key, value) => MapEntry(
        key,
        value.map((message) => message.toJson()).toList(),
      ));
  await store.setItem(DataKeys.notificationGroups, json.encode(encodedNotificationGroups));
}

Future<void> pushHelper(
  PushNotification notification, {
  Client? client,
  L10n? l10n,
  String? activeRoomId,
  void Function(NotificationResponse?)? onSelectNotification,
}) async {
  try {
    await _tryPushHelper(
      notification,
      client: client,
      l10n: l10n,
      activeRoomId: activeRoomId,
      onSelectNotification: onSelectNotification,
    );
  } catch (e, s) {
    Logs().wtf('Push Helper has crashed!', e, s);

    // Initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    await flutterLocalNotificationsPlugin.initialize(
        const InitializationSettings(
            android: AndroidInitializationSettings('@drawable/ic_notifications_logo'), iOS: DarwinInitializationSettings(), macOS: DarwinInitializationSettings()),
        onDidReceiveNotificationResponse: (notificationResponse) async => onSelectNotification!(notificationResponse));
    //onDidReceiveBackgroundNotificationResponse: onSelectNotificationBackground);

    unawaited(flutterLocalNotificationsPlugin.show(
      0,
      "New message in PageMe",
      l10n?.openAppToReadMessages,
      NotificationDetails(
          iOS: const DarwinNotificationDetails(
            badgeNumber: 1,
            presentBadge: true,
          ),
          android: AndroidNotificationDetails(
            FlavorConfig.pushNotificationsChannelId,
            FlavorConfig.pushNotificationsChannelName,
            channelDescription: FlavorConfig.pushNotificationsChannelDescription,
            number: notification.counts?.unread,
            ticker: l10n?.unreadChats(notification.counts?.unread ?? 1),
            importance: Importance.max,
            priority: Priority.high,
          )),
    ));
    rethrow;
  }
}

Future<void> _tryPushHelper(
  PushNotification notification, {
  Client? client,
  L10n? l10n,
  String? activeRoomId,
  void Function(NotificationResponse?)? onSelectNotification,
}) async {
  final Store store = Store();
  final String? notificationGroupsJson = await store.getItem(DataKeys.notificationGroups);
  if (notificationGroupsJson != null) {
    final Map<String, dynamic> decodedJson = json.decode(notificationGroupsJson);
    notificationGroups = decodedJson.map((key, value) => MapEntry(
          key,
          (value as List<dynamic>).map((messageJson) => MessageJson.fromJson(messageJson)).toList(),
        ));
  }

  final isBackgroundMessage = client == null;
  Logs().v(
    'Push helper has been started (background=$isBackgroundMessage).',
    notification.toJson(),
  );

  if (!isBackgroundMessage && activeRoomId == notification.roomId && activeRoomId != null && client.syncPresence == null) {
    Logs().v('Room is in foreground. Stop push helper here.');
    return;
  }

  // Initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  await flutterLocalNotificationsPlugin.initialize(
    const InitializationSettings(
      android: AndroidInitializationSettings('@drawable/ic_notifications_logo'),
      iOS: DarwinInitializationSettings(),
      macOS: DarwinInitializationSettings(),
    ),
    onDidReceiveNotificationResponse: (notificationResponse) async => onSelectNotification!(notificationResponse),
    //onDidReceiveBackgroundNotificationResponse: onSelectNotificationBackground,
  );

  client ??= (await ClientManager.getClients(initialize: false, store: await SharedPreferences.getInstance())).first;

  final event = await client.getEventByPushNotification(
    notification,
    storeInDatabase: isBackgroundMessage,
    timeoutForServerRequests: const Duration(seconds: 12),
  );

  if (event == null) {
    Logs().v('Notification is a clearing indicator.');
    if (notification.counts?.unread == 0) {
      if (notification.counts == null || notification.counts?.unread == 0) {
        await flutterLocalNotificationsPlugin.cancelAll();
        final store = await SharedPreferences.getInstance();
        await store.setString(SettingKeys.notificationCurrentIds, json.encode({}));
        await clearAllNotificationGroups();
      }
    }
    return;
  }
  Logs().v('Push helper got notification event of type ${event.type}.');

  if (event.type.startsWith('m.call')) {
    // make sure bg sync is on (needed to update hold, unhold events)
    // prevent over write from app life cycle change
    client.backgroundSync = true;
  }

  if (event.type == EventTypes.CallInvite) {
    await CallKeepManager().initialize();
  } else if (event.type == EventTypes.CallHangup) {
    client.backgroundSync = false;
  }

  if (event.type.startsWith('m.call') && event.type != EventTypes.CallInvite) {
    Logs().v('Push message is a m.call but not invite. Do not display.');
    return;
  }

  if ((event.type.startsWith('m.call') && event.type != EventTypes.CallInvite) || event.type == 'org.matrix.call.sdp_stream_metadata_changed') {
    Logs().v('Push message was for a call, but not call invite.');
    return;
  }

  Logs().w('Push helper got notification event.');
  if (window.locale.languageCode == "und") {
    final Locale locale = Locale.fromSubtags(countryCode: "za", languageCode: 'en', scriptCode: window.locale.scriptCode);
    l10n ??= await L10n.delegate.load(locale);
  } else {
    l10n ??= await L10n.delegate.load(window.locale);
  }
  final matrixLocals = MatrixLocals(l10n);

  // Calculate the body
  final body = event.type == EventTypes.Encrypted
      ? "New message in PageMe"
      : event.messageType == CustomMessageTypes.quillText
          ? event.calcQuillTextToPlainText(locals: MatrixLocals(l10n)) ?? 'Sent a composed message'
          : await event.calcLocalizedBody(
              matrixLocals,
              plaintextBody: true,
              withSenderNamePrefix: false,
              hideReply: true,
              hideEdit: true,
              removeMarkdown: true,
            );

  // The person object for the android message style notification

  final avatar = event.room.isDirectChat
      ? event.room.avatar
          ?.getThumbnail(
            client,
            width: 126,
            height: 126,
          )
          .toString()
      : event.senderFromMemoryOrFallback.avatarUrl
          ?.getThumbnail(
            client,
            width: 126,
            height: 126,
          )
          .toString();

  File? avatarFile;
  try {
    avatarFile = (avatar == null) ? null : await DefaultCacheManager().getSingleFile(avatar);
  } catch (e, s) {
    Logs().e('Unable to get avatar picture', e, s);
  }

  Future<int> mapRoomIdToInt(String roomId) async {
    final store = await SharedPreferences.getInstance();
    final idMap = Map<String, int>.from(jsonDecode(store.getString(SettingKeys.notificationCurrentIds) ?? '{}'));
    int? currentInt;
    try {
      currentInt = idMap[roomId];
    } catch (_) {
      currentInt = null;
    }
    if (currentInt != null) {
      return currentInt;
    }
    var nCurrentInt = 0;
    while (idMap.values.contains(nCurrentInt)) {
      nCurrentInt++;
    }
    idMap[roomId] = nCurrentInt;
    await store.setString(SettingKeys.notificationCurrentIds, json.encode(idMap));
    return nCurrentInt;
  }

  final id = await mapRoomIdToInt(event.room.id);

  Message newMessage;

  Future<File> saveImageDataToInternalStorage(Uint8List imageData) async {
    final tempDir = await getTemporaryDirectory();
    final fileName = Uri.encodeComponent("${event.attachmentOrThumbnailMxcUrl()!.pathSegments.last}.${event.attachmentOrThumbnailMxcUrl()!.fragment}");
    final file = File('${tempDir.path}/$fileName');
    await file.writeAsBytes(imageData);
    log(file.toString());
    return file;
  }

  if (event.attachmentMxcUrl != null && event.attachmentMimetype.contains('image')) {
    Logs().i('Push helper has Picture message');
    final Uint8List? image = await MxcImageFetch.getData(event: event, client: client, isThumbnail: true);
    final File imageFile = await saveImageDataToInternalStorage(image!);
    final String imageUri = await FileUriProvider().getUriForFile(imageFile.path, '${FlavorConfig.appId}.fileprovider');
    log("Attachment uri: ${imageUri.toString()}");

    newMessage = Message(
      body,
      event.originServerTs,
      Person(
        name: event.senderFromMemoryOrFallback.calcDisplayname(),
        icon: avatarFile == null ? null : BitmapFilePathAndroidIcon(avatarFile.path),
        bot: false,
        important: true,
      ),
      dataUri: imageUri,
      dataMimeType: event.attachmentMimetype,
    );
  } else {
    newMessage = Message(
      body,
      event.originServerTs,
      Person(
        name: event.senderFromMemoryOrFallback.calcDisplayname(),
        icon: avatarFile == null ? null : BitmapFilePathAndroidIcon(avatarFile.path),
        bot: false,
        important: true,
      ),
    );
  }

  if (notificationGroups.containsKey(event.room.id)) {
    if (notificationGroups[event.room.id]!.length >= 5) {
      notificationGroups[event.room.id]!.removeAt(0);
    }
    notificationGroups[event.room.id]!.add(newMessage);
  } else {
    notificationGroups[event.room.id] = [newMessage];
  }

  final encodedNotificationGroups = notificationGroups.map((key, value) => MapEntry(
        key,
        value.map((message) => message.toJson()).toList(),
      ));
  await store.setItem(DataKeys.notificationGroups, json.encode(encodedNotificationGroups));

  final messagingStyleInformation = MessagingStyleInformation(
    Person(
      name: event.room.getLocalizedDisplayname(),
      icon: avatarFile == null ? null : BitmapFilePathAndroidIcon(avatarFile.path),
    ),
    conversationTitle: event.room.isDirectChat ? null : event.room.getLocalizedDisplayname(),
    groupConversation: !event.room.isDirectChat,
    messages: notificationGroups.containsKey(event.room.id) ? notificationGroups[event.room.id]!.toList() : [newMessage],
  );

  final AndroidNotificationChannelGroup channelGroup = AndroidNotificationChannelGroup(
    event.roomId!,
    event.room.getLocalizedDisplayname(),
  );
  await flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()?.createNotificationChannelGroup(channelGroup);

  final androidPlatformChannelSpecifics = AndroidNotificationDetails(
    FlavorConfig.pushNotificationsChannelId,
    FlavorConfig.pushNotificationsChannelName,
    channelDescription: FlavorConfig.pushNotificationsChannelDescription,
    number: notification.counts?.unread,
    styleInformation: messagingStyleInformation,
    ticker: l10n.unreadChats(notification.counts?.unread ?? 1),
    importance: Importance.max,
    priority: Priority.max,
    largeIcon: _getLargeIcon(avatarFile),
    autoCancel: true,
    category: AndroidNotificationCategory.message,
    playSound: true,
    channelShowBadge: true,
  );
  const iOSPlatformChannelSpecifics = DarwinNotificationDetails(presentBadge: true, presentAlert: true);

  const macPlatformChannelSpecifics = DarwinNotificationDetails(presentBadge: true, presentAlert: true);
  final platformChannelSpecifics = NotificationDetails(
    android: androidPlatformChannelSpecifics,
    iOS: iOSPlatformChannelSpecifics,
    macOS: macPlatformChannelSpecifics,
  );

  await flutterLocalNotificationsPlugin.show(
    id,
    event.room.getLocalizedDisplayname(),
    body,
    platformChannelSpecifics,
    payload: '${event.eventId} ${event.roomId} ${FlavorConfig.applicationName}',
  );

  Logs().v('Push helper has been completed!');
}

FilePathAndroidBitmap? _getLargeIcon(File? avatarFile) {
  try {
    if (avatarFile == null) return null;
    if (avatarFile.path.isEmpty) return null;
    return FilePathAndroidBitmap(avatarFile.path);
  } on Exception catch (e) {
    Logs().e("[pushHelper] _getLargeIcon: $e");
  }
  return null;
}

/// Workaround for the problem that local notification IDs must be int but we
/// sort by [roomId] which is a String. To make sure that we don't have duplicated
/// IDs we map the [roomId] to a number and store this number.

extension MessageJson on Message {
  Map<String, dynamic> toJson() {
    return {
      'text': text,
      'timestamp': timestamp.toIso8601String(),
      'person': person?.toMap(),
      'dataMimeType': dataMimeType,
      'dataUri': dataUri,
    };
  }

  static Message fromJson(Map<String, dynamic> json) {
    return Message(
      json['text'],
      DateTime.parse(json['timestamp']),
      json['person'] != null ? _personFromMap(json['person']) : null,
      dataMimeType: json['dataMimeType'],
      dataUri: json['dataUri'],
    );
  }
}

Person? _personFromMap(Map<String, dynamic>? m) {
  if (m == null) {
    return null;
  }
  log(m.toString());
  return Person(
    bot: m['bot'],
    icon: _iconFromMap(m['icon']),
    important: m['important'],
    key: m['key'],
    name: m['name'],
    uri: m['uri'],
  );
}

AndroidIcon<Object>? _iconFromMap(String? iconPath) {
  try {
    if (iconPath == null) {
      return null;
    }
    return BitmapFilePathAndroidIcon(iconPath);
  } on Exception catch (e) {
    Logs().e("[pushHelper] _iconFromMap: $e");
  }
  return null;
}

extension PersonJson on Person {
  Map<String, Object?> toMap() => <String, Object?>{'bot': bot, 'important': important, 'key': key, 'name': name, 'uri': uri}..addAll(_convertIconToMap());

  Map<String, Object> _convertIconToMap() {
    if (icon == null) {
      return <String, Object>{};
    }
    return <String, Object>{
      'icon': icon!.data,
      'iconSource': icon!.source.index,
    };
  }
}
*/
