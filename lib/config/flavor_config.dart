import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';

enum Flavor { business, public }

bool isBusiness() {
  return (FlavorConfig.instance.flavor == Flavor.business);
}

bool isPublic() {
  return (FlavorConfig.instance.flavor == Flavor.public);
}

class FlavorConfig {

  final Flavor flavor;
  static late String applicationGUID;
  static late String applicationName;
  static late String appId;
  static late String appOpenUrlScheme;
  static late String deepLinkPrefix;
  static late String _eula;
  static late String _privacyUrl;
  static late String pushNotificationsAppId;
  static late FlavorConfig _instance;
  static late String pushNotificationsChannelId;
  static late String pushNotificationsForegroundChannelId;
  static late String pushNotificationsChannelName;
  static late String windowsPackageName;
  static late String windowsPackageFamilyName;

  static FlavorConfig get instance => _instance;

  FlavorConfig._init({required this.flavor}) {
    applicationGUID = (flavor == Flavor.business) ? 'f843b11f-2d5e-4782-a17e-505ed576715d' : '';
    applicationName = (flavor == Flavor.business) ? 'Pageme Business' : 'PageMe Public';
    windowsPackageName = (flavor == Flavor.business) ? 'PageMePTYLTD.PageMeBusiness' : 'PageMePTYLTD.PageMePublic';
    windowsPackageFamilyName = (flavor == Flavor.business) ? 'PageMePTYLTD.PageMeBusiness_c7cp8rr13dg9r' : 'PageMePTYLTD.PageMePublic';
    appId = (flavor == Flavor.business) ? 'com.pageme.business' : 'com.pageme.public';
    appOpenUrlScheme = (flavor == Flavor.business) ? 'com.pageme.business' : 'com.pageme.public';
    deepLinkPrefix = (flavor == Flavor.business) ? 'https://business.pageme.co.za/' : 'https://public.pageme.co.za/';
    pushNotificationsAppId = (flavor == Flavor.business) ? 'com.pageme.business' : 'com.pageme.public';
    pushNotificationsChannelId = (flavor == Flavor.business) ? 'PageMe Business' : 'PageMe Public';
    pushNotificationsForegroundChannelId = (flavor == Flavor.business) ? 'PageMe Business Foreground' : 'PageMe Public Foreground';
    pushNotificationsChannelName = (flavor == Flavor.business) ? 'PageMe Business' : "PageMe Public";
    _privacyUrl = (flavor == Flavor.business) ? 'https://business.pageme.co.za/privacy-policy.pdf' : 'https://public.pageme.co.za/privacy-policy.pdf';
    _eula = (flavor == Flavor.business) ? 'https://business.pageme.co.za/end-user-license-agreement.pdf' : 'https://public.pageme.co.za/end-user-license-agreement.pdf';
  }

  factory FlavorConfig.initialize({
    required Flavor flavor,
  }) {
    _instance = FlavorConfig._init(flavor: flavor);
    return _instance;
  }

  //The following is the same regardless of flavor.
  static String? _applicationWelcomeMessage;
  static String? windowsPublisherId = "c7cp8rr13dg9r";
  static String serverpodDevEndpoint = "https://serverpod-dev.pageme.co.za/";
  static String serverpodEndpoint = "https://serverpod.pageme.co.za/";
  static String? get applicationWelcomeMessage => _applicationWelcomeMessage;
  //static List<Map<String, String>> get homeServerOptions => _homeServerOptions;
  static String _defaultHomeserver = 'matrix.pageme.co.za';
  static String get defaultHomeserver => _defaultHomeserver;
  static double bubbleSizeFactor = 1;
  static double fontSizeFactor = 1;
  static Color chatColor = primaryColor;
  static Color? colorSchemeSeed = primaryColor;
  static const double messageFontSize = 15.75;
  static const bool allowOtherHomeservers = true;
  static const bool enableRegistration = true;
  static const String encryptionTutorial = 'https://gitlab.com/famedly/fluffychat/-/wikis/How-to-use-end-to-end-encryption-in-FluffyChat';
  static const Color primaryColor = Color(0xFF396eff);
  static const Color primaryColorMedium = Color(0xFF5aa3ff);
  static const Color primaryColorLight = Color(0xFFd2e5ff);
  static const Color secondaryColor = Color(0xFF6e6e6e);
  static String get privacyUrl => _privacyUrl;
  static String get eula => _eula;
  static const String enablePushTutorial = 'https://www.reddit.com/r/fluffychat/comments/qn6liu/enable_push_notifications_without_google_services/';
  static String _webBaseUrl = 'https://pageme.co.za/web';
  static String get webBaseUrl => _webBaseUrl;
  static const String sourceCodeUrl = 'https://gitlab.com/pagemedev/fluffychat';
  static const String supportUrl = 'https://gitlab.com/pagemedev/fluffychat/issues';
  static const bool enableSentry = true;
  static const String sentryDns = 'https://8591d0d863b646feb4f3dda7e5dcab38@o256755.ingest.sentry.io/5243143';
  static const bool isPushNoticationDebugging = false;
  static bool renderHtml = true;
  static bool launchOnStartup = true;
  static bool hideRedactedEvents = true;
  static bool hideUnknownEvents = true;
  static bool hideUnimportantStateEvents = true;
  static bool searchInChat = false;
  static bool showDirectChatsInSpaces = true;
  static bool separateChatTypes = false;
  static bool autoplayImages = true;
  static bool minimalChatListMode = false;
  static bool sendOnEnter = false;
  static bool experimentalVoip = true;
  static const bool hideTypingUsernames = false;
  static const bool hideAllStateEvents = false;
  static const String inviteLinkPrefix = 'https://matrix.to/#/';
  static const String schemePrefix = 'matrix:';
  static const String pushNotificationsChannelDescription = 'Push notifications for PageMe';
  static const String pushNotificationsEndpoint = '/_matrix/push/v1/notify'; //'https://sygnal.pageme.co.za/_matrix/push/v1/notify';
  static const String pushNotificationsPusherFormat = "event_id_only";
  static const Map<String, dynamic> pushNotificationsPusherIOSAddProps =
      {}; //{"default_payload": {"aps": {"content-available": 1, "alert": {}, /*must leave blank to have it to register onMessage but not display*/}}};
  static const String emojiFontName = 'Noto Emoji';
  static const String emojiFontUrl = 'https://github.com/googlefonts/noto-emoji/';
  static const double borderRadius = 14.0;
  static const double columnWidth = 360.0;

  static void loadFromJson(Map<String, dynamic> json) {
    if (json['chat_color'] != null) {
      try {
        chatColor = Color(json['application_name']);
      } catch (e) {
        Logs().w('Invalid color in config.json! Please make sure to define the color in this format: "0xffdd0000"', e);
      }
    }
    if (json['application_name'] is String) {
      applicationName = json['application_name'];
    }
    if (json['application_welcome_message'] is String) {
      _applicationWelcomeMessage = json['application_welcome_message'];
    }
    if (json['default_homeserver'] is String) {
      _defaultHomeserver = json['default_homeserver'];
    }
    if (json['privacy_url'] is String) {
      _webBaseUrl = json['privacy_url'];
    }
    if (json['web_base_url'] is String) {
      _privacyUrl = json['web_base_url'];
    }
    if (json['render_html'] is bool) {
      renderHtml = json['render_html'];
    }
    if (json['hide_redacted_events'] is bool) {
      hideRedactedEvents = json['hide_redacted_events'];
    }
    if (json['hide_unknown_events'] is bool) {
      hideUnknownEvents = json['hide_unknown_events'];
    }
  }


  static const List<Map<String, String>> publicServers = [
    {
      "name": "Public",
      "address": "matrix.pageme.co.za",
    },
  ];

}
