abstract class SettingKeys {
  static const String wallpaper = 'com.pageme.wallpaper';
  static const String launchAtStartup = 'com.pageme.launchAtStartup';
  static const String renderHtml = 'com.pageme.renderHtml';
  static const String minimalChatListMode = 'com.pageme.minimalChatListMode';
  static const String hideRedactedEvents = 'com.pageme.hideRedactedEvents';
  static const String hideUnknownEvents = 'com.pageme.hideUnknownEvents';
  static const String hideUnimportantStateEvents =
      'com.pageme.hideUnimportantStateEvents';
  static const String showDirectChatsInSpaces =
      'com.pageme.showDirectChatsInSpaces';
  static const String separateChatTypes = 'com.pageme.separateChatTypes';
  static const String chatColor = 'com.pageme.chat_color';
  static const String sentry = 'sentry';
  static const String theme = 'theme';
  static const String amoledEnabled = 'amoled_enabled';
  static const String codeLanguage = 'code_language';
  static const String searchInChat = 'com.pageme.beta.search_in_chat';
  static const String showNoGoogle = 'com.pageme.show_no_google';
  static const String showNoApple = 'com.pageme.show_no_apple';
  static const String showNoHuawei = 'com.pageme.show_no_huawei';
  static const String showNoHcm = 'com.pageme.show_no_hcm';
  static const String showTutorialBanner = 'com.pageme.show_tutorial_banner';
  static const String showAfterTutorialBanner = 'com.pageme.show_after_tutorial_banner';
  static const String bubbleSizeFactor = 'com.pageme.bubble_size_factor';
  static const String firstTimeUser = 'com.pageme.firstTimeUser';
  static const String fontSizeFactor = 'com.pageme.font_size_factor';
  static const String showNoPid = 'com.pageme.show_no_pid';
  static const String databasePassword = 'database-password';
  static const String appLockKey = 'com.pageme.app_lock';
  static const String unifiedPushRegistered = 'com.pageme.unifiedpush.registered';
  static const String unifiedPushEndpoint = 'com.pageme.unifiedpush.endpoint';
  static const String notificationCurrentIds = 'com.pageme.notification_ids';
  static const String ownStatusMessage = 'com.pageme.status_msg';
  static const String dontAskForBootstrapKey = 'com.pagemechat.dont_ask_bootstrap';
  static const String autoplayImages = 'com.pageme.autoplay_images';
  static const String sendOnEnter = 'com.pageme.send_on_enter';
  static const String experimentalVoip = 'com.pageme.experimental_voip';
  static const String iOSNotificationEncryption = 'com.pageme.iOSNotificationEncryption';
  static const String timeStampSizeSmall = 'com.pageme.timeStampSizeSmall';
  static const String timeStampSizeLarge = 'com.pageme.timeStampSizeLarge';
}

abstract class DataKeys {
  static const String notificationGroups = 'com.pageme.notificationGroups';

}
