/*
import 'package:flutter/material.dart';
import 'package:pageMe/pages/welcome_view/welcome_view.dart';
import 'package:pageMe/pages/onboarding/onboarding.dart';
import 'package:pageMe/pages/settings_subscriptions/settings_subscriptions_view.dart';
import 'package:pageMe/pages/subscriptions/subscriptions.dart';

import 'package:pageMe/pages/add_story/add_story.dart';
import 'package:pageMe/pages/archive/archive.dart';
import 'package:pageMe/pages/chat/chat.dart';
import 'package:pageMe/pages/chat_details/chat_details.dart';
import 'package:pageMe/pages/chat_encryption_settings/chat_encryption_settings.dart';
import 'package:pageMe/pages/chat_list/chat_list.dart';
import 'package:pageMe/pages/chat_permissions_settings/chat_permissions_settings.dart';
import 'package:pageMe/pages/device_settings/device_settings.dart';
import 'package:pageMe/pages/homeserver_picker/homeserver_picker.dart';
import 'package:pageMe/pages/invitation_selection/invitation_selection.dart';
import 'package:pageMe/pages/login/login.dart';
import 'package:pageMe/pages/new_group/new_group.dart';
import 'package:pageMe/pages/new_space/new_space.dart';
import 'package:pageMe/pages/settings/settings.dart';
import 'package:pageMe/pages/settings_3pid/settings_3pid.dart';
import 'package:pageMe/pages/settings_account/settings_account.dart';
import 'package:pageMe/pages/settings_chat/settings_chat.dart';
import 'package:pageMe/pages/settings_emotes/settings_emotes.dart';
import 'package:pageMe/pages/settings_ignore_list/settings_ignore_list.dart';
import 'package:pageMe/pages/settings_multiple_emotes/settings_multiple_emotes.dart';
import 'package:pageMe/pages/settings_notifications/settings_notifications.dart';
import 'package:pageMe/pages/settings_security/settings_security.dart';
import 'package:pageMe/pages/settings_stories/settings_stories.dart';
import 'package:pageMe/pages/settings_style/settings_appearance.dart';
import 'package:pageMe/pages/sign_up/signup.dart';
import 'package:pageMe/pages/story/story_page.dart';
import 'package:pageMe/widgets/layouts/empty_page.dart';
import 'package:pageMe/widgets/layouts/loading_view.dart';
import 'package:pageMe/widgets/layouts/side_view_layout.dart';
import 'package:pageMe/widgets/layouts/two_column_layout.dart';
import 'package:pageMe/widgets/log_view.dart';
import '../pages/new_chat/new_chat.dart';
import '../pages/onboarding_bootstrap/onboarding_bootstrap.dart';
import '../pages/onboarding_profile_picture/onboarding_profile_picture.dart';
import 'flavor_config.dart';

class AppRoutesOld {
  final bool columnMode;

  AppRoutes(this.columnMode);

  List<VRouteElement> get routes => [
        ...isBusiness() ? _homeBusinessRoutes : _homePublicRoutes,
        if (columnMode) ..._tabletRoutes,
        if (!columnMode) ..._mobileRoutes,
      ];

  List<VRouteElement> get _mobileRoutes => [
        VWidget(
          path: '/rooms',
          widget: const ChatList(),
          stackedRoutes: [
            VWidget(
              path: '/spaces/:roomid',
              widget: const ChatDetails(),
              stackedRoutes: _chatDetailsRoutes,
            ),
            VWidget(
              path: ':roomid',
              widget: const Chat(),
              stackedRoutes: [
                VWidget(
                  path: 'encryption',
                  widget: const ChatEncryptionSettings(),
                ),
                VWidget(
                  path: 'invite',
                  widget: const InvitationSelection(),
                ),
                VWidget(
                  path: 'details',
                  widget: const ChatDetails(),
                  stackedRoutes: _chatDetailsRoutes,
                ),
              ],
            ),
            VWidget(
              path: '/settings',
              widget: const Settings(),
              stackedRoutes: _settingsRoutes,
            ),
            VWidget(
              path: '/archive',
              widget: const Archive(),
            ),
            VWidget(
              path: '/newchat',
              widget: const NewChat(),
              stackedRoutes: [
                VWidget(
                  path: '/newgroup',
                  widget: const NewGroup(),
                ),
              ],
            ),
            VWidget(
              path: '/newspace',
              widget: const NewSpace(),
            ),
          ],
        ),
      ];

  List<VRouteElement> get _tabletRoutes => [
        VNester(
          path: '/rooms',
          widgetBuilder: (child) => TwoColumnLayout(
            mainView: const ChatList(),
            sideView: child,
          ),
          buildTransition: _slideTransition,
          nestedRoutes: [
            VWidget(
              path: '',
              widget: const EmptyPage(),
              buildTransition: _fadeTransition,
              stackedRoutes: [
                VWidget(
                  path: '/stories/create',
                  buildTransition: _fadeTransition,
                  widget: const AddStoryPage(),
                ),
                VWidget(
                  path: '/stories/:roomid',
                  buildTransition: _fadeTransition,
                  widget: const StoryPage(),
                  stackedRoutes: [
                    VWidget(
                      path: 'share',
                      widget: const AddStoryPage(),
                    ),
                  ],
                ),
                VWidget(
                  path: '/spaces/:roomid',
                  widget: const ChatDetails(),
                  buildTransition: _fadeTransition,
                  stackedRoutes: _chatDetailsRoutes,
                ),
                VWidget(
                  path: '/newchat',
                  widget: const NewChat(),
                  buildTransition: _fadeTransition,
                ),
                VWidget(
                  path: '/newgroup',
                  widget: const NewGroup(),
                  buildTransition: _fadeTransition,
                ),
                VWidget(
                  path: '/newspace',
                  widget: const NewSpace(),
                  buildTransition: _fadeTransition,
                ),
                VWidget(
                  path: ':roomid/large_details',
                  widget: const ChatDetails(),
                  buildTransition: _fadeTransition,
                ),
                VNester(
                  path: ':roomid',
                  widgetBuilder: (child) => SideViewLayout(
                    mainView: const Chat(),
                    sideView: child,
                  ),
                  buildTransition: _slideTransition,
                  nestedRoutes: [
                    VWidget(
                      path: '',
                      widget: const Chat(),
                      buildTransition: _fadeTransition,
                    ),
                    VWidget(
                      path: 'encryption',
                      widget: const ChatEncryptionSettings(),
                      buildTransition: _fadeTransition,
                    ),
                    VWidget(
                      path: 'details',
                      widget: const ChatDetails(),
                      buildTransition: _fadeTransition,
                      stackedRoutes: _chatDetailsRoutes,
                    ),
                    VWidget(
                      path: 'invite',
                      widget: const InvitationSelection(),
                      buildTransition: _fadeTransition,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
        VWidget(
          path: '/rooms',
          widget: const TwoColumnLayout(
            mainView: ChatList(),
            sideView: EmptyPage(),
          ),
          buildTransition: _fadeTransition,
          stackedRoutes: [
            VNester(
              path: '/settings',
              widgetBuilder: (child) => TwoColumnLayout(
                mainView: const Settings(),
                sideView: child,
              ),
              buildTransition: _dynamicTransition,
              nestedRoutes: [
                VWidget(
                  path: '',
                  widget: const EmptyPage(),
                  buildTransition: _dynamicTransition,
                  stackedRoutes: _settingsRoutes,
                ),
              ],
            ),
            VWidget(
              path: '/archive',
              widget: const TwoColumnLayout(
                mainView: Archive(),
                sideView: EmptyPage(),
              ),
              buildTransition: _fadeTransition,
            ),
          ],
        ),
      ];

  List<VRouteElement> get _homePublicRoutes => [
        VWidget(
          path: '/',
          widget: const LoadingView(),
        ),
        VWidget(
          path: '/home',
          widget: const WelcomeView(
            title: "Welcome!",
            body: "We are so glad you are here.\nLet us help you get started on our secure public server",
            isTopSafeArea: true,
            isBottomSafeArea: true,
          ),
        ),
        VWidget(
          path: '/login',
          widget: const Login(),
          buildTransition: _slideTransition,
          stackedRoutes: [
            VWidget(
              path: 'subscriptions',
              widget: const Subscriptions(firstTimeView: true),
              buildTransition: _slideTransition,
              stackedRoutes: [
                VWidget(
                  path: 'signup',
                  widget: const SignupPage(),
                  buildTransition: _slideTransition,
                )
              ],
            ),
            VWidget(
              path: 'signup',
              widget: const SignupPage(),
              buildTransition: _slideTransition,
            )

          ],
        ),
        VWidget(
          path: '/subscriptions',
          widget: const Subscriptions(firstTimeView: false),
          buildTransition: _slideTransition,
        ),
        VWidget(
          path: '/logs',
          widget: const LogViewer(),
          buildTransition: _slideTransition,
        ),
        VWidget(
          path: '/onboard',
          widget: const OnBoarding(),
          buildTransition: _fadeTransition,
          stackedRoutes: _onboardingRoutes,
        )
      ];

  List<VRouteElement> get _homeBusinessRoutes => [
        VWidget(path: '/', widget: const LoadingView()),
        VWidget(
          path: '/home',
          widget: const WelcomeView(
            title: "Welcome!",
            body: "We are so glad you are here.\nLet us help you get started on your private business server",
            isTopSafeArea: true,
            isBottomSafeArea: true,
          ),
        ),
        VWidget(
          path: '/server_select',
          widget: const HomeserverPicker(),
          buildTransition: _slideTransition,
          stackedRoutes: [
            VWidget(
              path: 'login',
              widget: const Login(),
              buildTransition: _slideTransition,
            ),
          ],
        ),
        VWidget(
          path: '/onboard',
          widget: const OnBoarding(),
          buildTransition: _fadeTransition,
          stackedRoutes: _onboardingRoutes,
        )
      ];

  List<VRouteElement> get _chatDetailsRoutes => [
        VWidget(
          path: 'permissions',
          widget: const ChatPermissionsSettings(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'invite',
          widget: const InvitationSelection(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'multiple_emotes',
          widget: const MultipleEmotesSettings(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'emotes',
          widget: const EmotesSettings(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'emotes/:state_key',
          widget: const EmotesSettings(),
          buildTransition: _dynamicTransition,
        ),
      ];

  List<VRouteElement> get _onboardingRoutes => [
        VWidget(
          path: 'onboarding_profile_picture',
          widget: const OnBoardingProfilePicture(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'onboarding_bootstrap',
          widget: const OnBoardingBootstrap(),
          buildTransition: _dynamicTransition,
        ),
      ];

  List<VRouteElement> get _settingsRoutes => [
        VWidget(
          path: 'notifications',
          widget: const SettingsNotifications(),
          buildTransition: _slideTransition,
        ),
        VWidget(
          path: 'style',
          widget: const SettingsStyle(),
          buildTransition: _slideTransition,
        ),
        VWidget(
          path: 'devices',
          widget: const DevicesSettings(),
          buildTransition: _slideTransition,
        ),
        VWidget(
          path: 'chat',
          widget: const SettingsChat(),
          buildTransition: _slideTransition,
          stackedRoutes: [
            VWidget(
              path: 'emotes',
              widget: const EmotesSettings(),
              buildTransition: _slideTransition,
            ),
          ],
        ),
        VWidget(path: 'account', widget: const SettingsAccount(), buildTransition: _slideTransition, stackedRoutes: isBusiness() ? _homeBusinessRoutes : _homePublicRoutes),
        if (!isBusiness())
          VWidget(
            path: 'settings_subscriptions',
            widget: const SettingsSubscriptionsView(),
            stackedRoutes: [
              VWidget(
                path: 'subscriptions',
                widget: const Subscriptions(
                  firstTimeView: false,
                ),
                buildTransition: _slideTransition,
              )
            ],
          ),
        VWidget(
          path: 'security',
          widget: const SettingsSecurity(),
          buildTransition: _slideTransition,
          stackedRoutes: [
            VWidget(
              path: 'stories',
              widget: const SettingsStories(),
              buildTransition: _dynamicTransition,
            ),
            VWidget(
              path: 'ignorelist',
              widget: const SettingsIgnoreList(),
              buildTransition: _dynamicTransition,
            ),
            VWidget(
              path: '3pid',
              widget: const Settings3Pid(),
              buildTransition: _dynamicTransition,
            ),
          ],
        ),
        VWidget(
          path: 'logs',
          widget: const LogViewer(),
          buildTransition: _dynamicTransition,
        ),
      ];

  FadeTransition Function(dynamic, dynamic, dynamic)? get _dynamicTransition => columnMode ? _fadeTransition : null;

  FadeTransition _fadeTransition(animation1, _, child) => FadeTransition(opacity: animation1, child: child);

  SlideTransition _slideTransition(animation1, animation2, child) => SlideTransition(
        position: Tween(
          begin: const Offset(1, 0),
          end: Offset.zero,
        ).animate(animation1),
        child: child,
      );
}

class AnimatedPage extends Page {
  final Widget enterPage;

  const AnimatedPage({
    LocalKey? key,
    String? name,
    required this.enterPage,
  }) : super(key: key, name: name);

  @override
  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
      settings: this,
      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => SlideTransition(
        position: Tween<Offset>(
          begin: const Offset(1.0, 0.0),
          end: Offset.zero,
        ).animate(animation),
        child: enterPage,
      ),
    );
  }
}
*/
