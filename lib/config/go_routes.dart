import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/config/themes.dart';
import 'package:pageMe/cubits/file_handler/file_handler_bloc.dart';
import 'package:pageMe/pages/settings_account/settings_account.dart';
import 'package:pageMe/pages/settings_subscriptions/settings_subscriptions_view.dart';

import '../pages/archive/archive.dart';
import '../pages/chat/chat.dart';
import '../pages/chat_details/chat_details.dart';
import '../pages/chat_encryption_settings/chat_encryption_settings.dart';
import '../pages/chat_list/chat_list.dart';
import '../pages/chat_members/chat_members.dart';
import '../pages/chat_permissions_settings/chat_permissions_settings.dart';
import '../pages/device_settings/device_settings.dart';
import '../pages/homeserver_picker/homeserver_picker.dart';
import '../pages/invitation_selection/invitation_selection.dart';
import '../pages/login/login.dart';
import '../pages/new_chat/new_chat.dart';
import '../pages/new_group/new_group.dart';
import '../pages/new_space/new_space.dart';
import '../pages/onboarding/onboarding.dart';
import '../pages/onboarding_bootstrap/onboarding_bootstrap.dart';
import '../pages/onboarding_profile_picture/onboarding_profile_picture.dart';
import '../pages/settings/settings.dart';
import '../pages/settings_3pid/settings_3pid.dart';
import '../pages/settings_chat/settings_chat.dart';
import '../pages/settings_emotes/settings_emotes.dart';
import '../pages/settings_ignore_list/settings_ignore_list.dart';
import '../pages/settings_multiple_emotes/settings_multiple_emotes.dart';
import '../pages/settings_notifications/settings_notifications.dart';
import '../pages/settings_security/settings_security.dart';
import '../pages/settings_stories/settings_stories.dart';
import '../pages/settings_appearance/settings_appearance.dart';
import '../pages/sign_up/signup.dart';
import '../pages/subscriptions/subscriptions.dart';
import '../pages/tasks/tasks.dart';
import '../pages/welcome_view/welcome_view.dart';
import '../views/empty_page.dart';
import '../widgets/layouts/two_column_layout.dart';
import '../widgets/log_view.dart';
import '../widgets/matrix.dart';
import 'flavor_config.dart';

abstract class AppRoutes {
  static FutureOr<String?> loggedInRedirect(
    BuildContext context,
    GoRouterState state,
  ) =>
      Matrix.of(context).client.isLogged() ? '/rooms' : null;

  static FutureOr<String?> loggedOutRedirect(
    BuildContext context,
    GoRouterState state,
  ) =>
      Matrix.of(context).client.isLogged() ? null : '/home';

  static GlobalKey chatListKey = GlobalKey();

  static FutureOr<bool> onExit(
    BuildContext context,
  ) {
    final fileHandlerBloc = BlocProvider.of<FileHandlerBloc>(context);
    if (fileHandlerBloc.state.isInitiated) {
      try {
        fileHandlerBloc.add(const FileCompletionRequested(isCancelled: true));
      } on Exception catch (e) {
        Logs().e("onExit - Could not cancel file handler: $e");
      }
    }
    return true;
  }

  AppRoutes();

  static final List<RouteBase> routes = [
    GoRoute(
      path: '/',
      redirect: (context, state) => Matrix.of(context).client.isLogged() ? '/rooms' : '/home',
    ),
    GoRoute(
      path: '/home',
      pageBuilder: (context, state) => defaultPageBuilder(
        context,
        const WelcomeView(
          title: "Welcome!",
          body: "We are so glad you are here.\nLet us help you get started on your private business server",
          isTopSafeArea: true,
          isBottomSafeArea: true,
        ),
      ),
    ),
    if (isPublic())
      GoRoute(
          path: '/login',
          pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const Login(),
              ),
          routes: [
            GoRoute(
                path: 'subscriptions',
                pageBuilder: (context, state) => defaultPageBuilder(
                      context,
                      const Subscriptions(firstTimeView: true),
                    ),
                routes: [
                  GoRoute(
                    path: 'signup',
                    pageBuilder: (context, state) => defaultPageBuilder(
                      context,
                      const SignupPage(),
                    ),
                  )
                ]),
            GoRoute(
              path: 'signup',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const SignupPage(),
              ),
            )
          ]),
    GoRoute(
        path: '/server_select',
        pageBuilder: (context, state) => defaultPageBuilder(
              context,
              const HomeserverPicker(),
            ),
        redirect: loggedInRedirect,
        routes: [
          GoRoute(
            path: 'login',
            pageBuilder: (context, state) => defaultPageBuilder(
              context,
              const Login(),
            ),
          )
        ]),
    if (isPublic())
      GoRoute(
        path: '/subscriptions',
        pageBuilder: (context, state) => defaultPageBuilder(
          context,
          const Subscriptions(firstTimeView: false),
        ),
      ),
    GoRoute(
        path: '/onboard',
        pageBuilder: (context, state) => defaultPageBuilder(
              context,
              const OnBoarding(),
            ),
        routes: [
          GoRoute(
              path: 'onboarding_profile_picture',
              pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    const OnBoardingProfilePicture(),
                  ),
              routes: [
                GoRoute(
                  path: 'onboarding_bootstrap',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    const OnBoardingBootstrap(),
                  ),
                )
              ]),
        ]),
    ShellRoute(
      pageBuilder: (context, state, child) {
        Logs().v("[GoRouter] Navigating to ShellRoute with path: ${state.fullPath}");
        final isSettingsRoute = state.fullPath?.startsWith('/settings') == true;
        return defaultPageBuilder(
          context,
          PageMeThemes.isColumnMode(context) && !isSettingsRoute
              ? TwoColumnLayout(
                  displayNavigationRail: !isSettingsRoute,
                  mainView: ChatList(
                    key: chatListKey,
                    activeChat: state.pathParameters['roomid'],
                    displayNavigationRail: !isSettingsRoute,
                  ),
                  sideView: child,
                )
              : child,
        );
      },
      routes: [
        GoRoute(
          path: '/rooms',
          redirect: loggedOutRedirect,
          pageBuilder: (context, state) {
            return defaultPageBuilder(
                context,
                !PageMeThemes.isColumnMode(context)
                    ? ChatList(
                        key: chatListKey,
                        activeChat: state.pathParameters['roomid'],
                      )
                    : const EmptyPage());
          },
          routes: [
            GoRoute(
              path: 'archive',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const Archive(),
              ),
              redirect: loggedOutRedirect,
              routes: [
                GoRoute(
                  path: ':roomid',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    ChatPage(
                      roomId: state.pathParameters['roomid']!,
                    ),
                  ),
                  redirect: loggedOutRedirect,
                ),
              ],
            ),
            GoRoute(
                path: 'newchat',
                pageBuilder: (context, state) => defaultPageBuilder(
                      context,
                      const NewChat(),
                    ),
                redirect: loggedOutRedirect,
                routes: [
                  GoRoute(
                    path: 'newgroup',
                    pageBuilder: (context, state) => defaultPageBuilder(
                      context,
                      const NewGroup(),
                    ),
                    redirect: loggedOutRedirect,
                  )
                ]),
            GoRoute(
              path: 'newgroup',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const NewGroup(),
              ),
              redirect: loggedOutRedirect,
            ),
            GoRoute(
              path: 'newspace',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const NewSpace(),
              ),
              redirect: loggedOutRedirect,
            ),
            GoRoute(
              path: ':roomid',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                ChatPage(roomId: state.pathParameters['roomid']!),
              ),
              onExit: onExit,
              redirect: loggedOutRedirect,
              routes: [
                GoRoute(
                  path: 'encryption',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    const ChatEncryptionSettings(),
                  ),
                  redirect: loggedOutRedirect,
                ),
                GoRoute(
                  path: 'invite',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    InvitationSelection(
                      roomId: state.pathParameters['roomid']!,
                    ),
                  ),
                  redirect: loggedOutRedirect,
                ),
                GoRoute(
                  path: 'details',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    ChatDetails(
                      roomId: state.pathParameters['roomid']!,
                    ),
                  ),
                  routes: [
                    GoRoute(
                      path: 'members',
                      pageBuilder: (context, state) => defaultPageBuilder(
                        context,
                        ChatMembersPage(
                          roomId: state.pathParameters['roomid']!,
                        ),
                      ),
                      redirect: loggedOutRedirect,
                    ),
                    GoRoute(
                      path: 'permissions',
                      pageBuilder: (context, state) => defaultPageBuilder(
                        context,
                        const ChatPermissionsSettings(),
                      ),
                      redirect: loggedOutRedirect,
                    ),
                    GoRoute(
                      path: 'invite',
                      pageBuilder: (context, state) => defaultPageBuilder(
                        context,
                        InvitationSelection(
                          roomId: state.pathParameters['roomid']!,
                        ),
                      ),
                      redirect: loggedOutRedirect,
                    ),
                    GoRoute(
                      path: 'multiple_emotes',
                      pageBuilder: (context, state) => defaultPageBuilder(
                        context,
                        const MultipleEmotesSettings(),
                      ),
                      redirect: loggedOutRedirect,
                    ),
                    GoRoute(
                      path: 'emotes',
                      pageBuilder: (context, state) => defaultPageBuilder(
                        context,
                        const EmotesSettings(),
                      ),
                      redirect: loggedOutRedirect,
                    ),
                    GoRoute(
                      path: 'emotes/:state_key',
                      pageBuilder: (context, state) => defaultPageBuilder(
                        context,
                        const EmotesSettings(),
                      ),
                      redirect: loggedOutRedirect,
                    ),
                  ],
                  redirect: loggedOutRedirect,
                ),
                GoRoute(
                  path: 'tasks',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    TasksPage(
                      room: Matrix.of(context).client.getRoomById(state.pathParameters['roomid']!)!,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ],
    ),
    ShellRoute(
      pageBuilder: (context, state, child) {
        Logs().v("[GoRouter] Navigating to inner ShellRoute with path: ${state.fullPath}");
        return defaultPageBuilder(
          context,
          PageMeThemes.isColumnMode(context)
              ? TwoColumnLayout(
                  mainView: const Settings(),
                  sideView: child,
                  displayNavigationRail: false,
                )
              : child,
        );
      },
      routes: [
        GoRoute(
          path: '/settings',
          pageBuilder: (context, state) => defaultPageBuilder(
            context,
            PageMeThemes.isColumnMode(context) ? const EmptyPage() : const Settings(),
          ),
          routes: [
            GoRoute(
              path: 'notifications',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const SettingsNotifications(),
              ),
              redirect: loggedOutRedirect,
            ),
            GoRoute(
              path: 'logs',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const LogViewer(),
              ),
            ),
            GoRoute(
              path: 'style',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const SettingsAppearance(),
              ),
              redirect: loggedOutRedirect,
            ),
            GoRoute(
              path: 'account',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const SettingsAccount(),
              ),
              redirect: loggedOutRedirect,
            ),
            GoRoute(
              path: 'devices',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const DevicesSettings(),
              ),
              redirect: loggedOutRedirect,
            ),
            GoRoute(
              path: 'chat',
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const SettingsChat(),
              ),
              routes: [
                GoRoute(
                  path: 'emotes',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    const EmotesSettings(),
                  ),
                ),
              ],
              redirect: loggedOutRedirect,
            ),
            GoRoute(
              path: 'addaccount',
              redirect: loggedOutRedirect,
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const HomeserverPicker(),
              ),
              routes: [
                GoRoute(
                  path: 'login',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    const Login(),
                  ),
                  redirect: loggedOutRedirect,
                ),
              ],
            ),
            if (isPublic())
              GoRoute(
                  path: 'subscription_information',
                  redirect: loggedOutRedirect,
                  pageBuilder: (context, state) => defaultPageBuilder(
                        context,
                        const SettingsSubscriptionsView(),
                      ),
                  routes: [
                    GoRoute(
                      path: 'subscriptions',
                      pageBuilder: (context, state) => defaultPageBuilder(
                        context,
                        const Subscriptions(firstTimeView: false),
                      ),
                    )
                  ]),
            GoRoute(
              path: 'security',
              redirect: loggedOutRedirect,
              pageBuilder: (context, state) => defaultPageBuilder(
                context,
                const SettingsSecurity(),
              ),
              routes: [
                GoRoute(
                  path: 'stories',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    const SettingsStories(),
                  ),
                  redirect: loggedOutRedirect,
                ),
                GoRoute(
                  path: 'ignorelist',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    const SettingsIgnoreList(),
                  ),
                  redirect: loggedOutRedirect,
                ),
                GoRoute(
                  path: '3pid',
                  pageBuilder: (context, state) => defaultPageBuilder(
                    context,
                    const Settings3Pid(),
                  ),
                  redirect: loggedOutRedirect,
                ),
              ],
            ),
          ],
          redirect: loggedOutRedirect,
        ),
      ],
    )
  ];

  static Page defaultPageBuilder(BuildContext context, Widget child) {
    Logs().v("[GoRouter] Building page for: ${child.runtimeType}");
    return CustomTransitionPage(
      child: child,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        //Logs().v("[GoRouter] Transition for: ${child.runtimeType}");
        return PageMeThemes.isColumnMode(context)
            ? FadeTransition(opacity: animation, child: child)
            : CupertinoPageTransition(
                primaryRouteAnimation: animation,
                secondaryRouteAnimation: secondaryAnimation,
                linearTransition: false,
                child: child,
              );
      },
    );
  }
}
