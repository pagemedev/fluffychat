import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pageMe/utils/platform_infos.dart';

import '../pageme_app.dart';
import 'flavor_config.dart';

class PageMeThemes with ChangeNotifier {
  static const double columnWidth = 360.0;
  static double thumbnailHeight = 148;
  static bool isColumnMode(BuildContext context) {
    if (PlatformInfos.isIOS) {
      return MediaQuery.sizeOf(context).width > (columnWidth * 3);
    } else {
      return MediaQuery.sizeOf(context).width > (columnWidth * 2);
    }
  }

  bool _isThreeColumnMode = false;

  bool get isThreeColumnMode => _isThreeColumnMode;

  void toggleThreeColumnMode(BuildContext context) {
    _isThreeColumnMode = !_isThreeColumnMode;
    notifyListeners();
  }

  static const fallbackTextStyle = TextStyle(fontFamily: 'Roboto', fontFamilyFallback: ['NotoEmoji']);
  static double thumbnailImageHeight(BuildContext context) => thumbnailHeight * MediaQuery.devicePixelRatioOf(context);
  static const TextStyle loginTextFieldStyle = TextStyle(color: Colors.black);
  static const TextStyle loginDropDownStyle = TextStyle(color: Colors.black);

  static bool getDisplayNavigationRail(BuildContext context) => !PageMeApp.router.routeInformationProvider.value.uri.path.startsWith('/settings');

  static InputDecoration loginTextFieldDecoration({
    String? errorText,
    String? labelText,
    String? hintText,
    Widget? suffixIcon,
    Widget? prefixIcon,
    TextStyle? errorStyle,
    int? errorMaxLines,
  }) =>
      InputDecoration(
        errorMaxLines: errorMaxLines,
        fillColor: Colors.white.withOpacity(0.95), //Colors.white.withAlpha(200),
        labelText: labelText,
        hintText: hintText,
        suffixIcon: suffixIcon,
        prefixIcon: prefixIcon,
        suffixIconColor: Colors.black,
        prefixIconColor: Colors.black,
        iconColor: Colors.black,
        errorText: errorText,
        errorStyle: errorStyle,
        hintStyle: TextStyle(color: Colors.grey.shade700),
        labelStyle: const TextStyle(
          color: Colors.white,
          shadows: [
            Shadow(
              color: Colors.black,
              offset: Offset(0, 0),
              blurRadius: 5,
            ),
          ],
        ),
        contentPadding: const EdgeInsets.all(16),
      );

  static Color dropScreenColor = const Color(0xFF818ba1);

  static var fallbackTextTheme = const TextTheme(
    bodyText1: fallbackTextStyle,
    bodyText2: fallbackTextStyle,
    button: fallbackTextStyle,
    caption: fallbackTextStyle,
    overline: fallbackTextStyle,
    headline1: fallbackTextStyle,
    headline2: fallbackTextStyle,
    headline3: fallbackTextStyle,
    headline4: fallbackTextStyle,
    headline5: fallbackTextStyle,
    headline6: fallbackTextStyle,
    subtitle1: fallbackTextStyle,
    subtitle2: fallbackTextStyle,
  );

  static const Duration animationDuration = Duration(milliseconds: 250);
  static const Curve animationCurve = Curves.easeInOut;

  static ThemeData get lightMaterial => ThemeData(
        visualDensity: VisualDensity.standard,
        useMaterial3: true,
        primaryColor: const Color(0xFF0150E3),
        canvasColor: const Color(0xfff2f2f2),
        colorScheme: const ColorScheme(
          brightness: Brightness.light,
          primary: Color(0xFF3f5aa9),
          onPrimary: Color(0xFFFFFFFF),
          primaryContainer: Color(0xFF3f5aa9), //Color(0xFFDCE1FF),
          onPrimaryContainer: Color(0xFFffffff), //Color(0xFF00164E),
          secondary: Color(0xFF595E72),
          onSecondary: Color(0xFFFFFFFF),
          secondaryContainer: Color(0xFFDEE1F9),
          onSecondaryContainer: Color(0xFF161B2C),
          tertiary: Color(0xFF8482FA), //Color(0xFF3f5aa9),//Color(0xFFB5C4FF),
          onTertiary: Color(0xFFFFFFFF), //Color(0xFFffffff),//Color(0xFF00287C),
          tertiaryContainer: Color(0xFFfefbff),
          onTertiaryContainer: Color(0xFF001849),
          error: Color(0xFFBA1A1A),
          errorContainer: Color(0xFFFFDAD6),
          onError: Color(0xFFFFFFFF),
          onErrorContainer: Color(0xFF410002),
          background: Color(0xFFecedf5), //f6f8fc //e7ebff
          onBackground: Color(0xFF000000),
          surface: Color(0xFFFEFBFF),
          onSurface: Color(0xFF1B1B1F),
          surfaceVariant: Color(0xFFE2E1EC),
          onSurfaceVariant: Color(0xFF45464F),
          outline: Color(0xFF767680),
          onInverseSurface: Color(0xFFF2F0F4),
          inverseSurface: Color(0xFF303034),
          inversePrimary: Color(0xFFB5C4FF),
          shadow: Color(0xFF000000),
          surfaceTint: Color(0xFF0150E3),
          outlineVariant: Color(0xFFC6C6D0),
          scrim: Color(0xFF000000),
        ),
        textTheme: PlatformInfos.isDesktop ? Typography.material2021().black.merge(fallbackTextTheme) : null,
        snackBarTheme:
            const SnackBarThemeData(behavior: SnackBarBehavior.floating, backgroundColor: Color(0xFFfefbff), contentTextStyle: TextStyle(color: Color(0xFF001849)), closeIconColor: Color(0xFF001849)),
        pageTransitionsTheme: const PageTransitionsTheme(
          builders: {
            TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
            TargetPlatform.android: ZoomPageTransitionsBuilder(),
            TargetPlatform.linux: CupertinoPageTransitionsBuilder(),
            TargetPlatform.macOS: CupertinoPageTransitionsBuilder(),
            TargetPlatform.windows: CupertinoPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          },
        ),
        fontFamilyFallback: const ['NotoEmoji'],
        fontFamily: 'Roboto',
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: Color(0xFF3f5aa9),
          foregroundColor: Color(0xFFFFFFFF),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            backgroundColor: const Color(0xFF0150E3),
            foregroundColor: const Color(0xFFFFFFFF),
            textStyle: const TextStyle(fontSize: 16),
            elevation: 6,
            shadowColor: const Color(0x44000000),
            minimumSize: const Size.fromHeight(48),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
            ),
            padding: const EdgeInsets.all(12),
          ),
        ),
        appBarTheme: const AppBarTheme(
          scrolledUnderElevation: 0,
          backgroundColor: Color(0xFFD0D4E6), //Color(0xFFd0e4ff), //D0D4E6 //e7ebff
          actionsIconTheme: IconThemeData(color: Colors.black), //Color(0xFF001849)
          elevation: 0,
          shadowColor: Colors.black,
          systemOverlayStyle: SystemUiOverlayStyle.dark,
          //backgroundColor: FlavorConfig.primaryColorLight,
          titleTextStyle: TextStyle(
            color: Colors.black,
            fontSize: 20,
          ),
          iconTheme: IconThemeData(color: Colors.black, size: 25),
        ),
      );

  static ThemeData get lightMaterialPublic => ThemeData(
        visualDensity: VisualDensity.standard,
        fontFamilyFallback: const ['NotoEmoji'],
        fontFamily: 'Roboto',
        shadowColor: Colors.black,
        useMaterial3: true,
        primaryColor: const Color(0xFF009a74),
        colorScheme: const ColorScheme(
          brightness: Brightness.light,
          primary: Color(0xFF009a74),
          onPrimary: Color(0xFFFFFFFF),
          primaryContainer: Color(0xFF477F6F),
          onPrimaryContainer: Color(0xFFFFFFFF), //Color(0xFF00164E),
          secondary: Color(0xFF546c85),
          onSecondary: Color(0xFFFFFFFF),
          secondaryContainer: Color(0xFFEAEEF4),
          onSecondaryContainer: Color(0xFF002114),
          tertiary: Color(0xFF6f86b0), //Color(0xFF3f5aa9),//Color(0xFFB5C4FF),
          onTertiary: Color(0xFFFFFFFF), //Color(0xFFffffff),//Color(0xFF00287C),
          tertiaryContainer: Color(0xFFFFFFFF),
          onTertiaryContainer: Color(0xFF002114),
          error: Color(0xFFBA1A1A),
          errorContainer: Color(0xFFFFDAD6),
          onError: Color(0xFFFFFFFF),
          onErrorContainer: Color(0xFF410002),
          background: Color(0xFFF8FBFA),
          onBackground: Color(0xFF002116),
          surface: Color(0xFFFEFBFF),
          onSurface: Color(0xFF1B1B1F),
          surfaceVariant: Color(0xFFE2E1EC),
          onSurfaceVariant: Color(0xFF45464F),
          outline: Color(0xFF767680),
          onInverseSurface: Color(0xFFF2F0F4),
          inverseSurface: Color(0xFF303034),
          inversePrimary: Color(0xFFB5C4FF),
          shadow: Color(0xFF000000),
          surfaceTint: Color(0xFF009a74),
          outlineVariant: Color(0xFFC6C6D0),
          scrim: Color(0xFF000000),
        ),
        textTheme: PlatformInfos.isDesktop ? Typography.material2021().black.merge(fallbackTextTheme) : null,
        snackBarTheme:
            const SnackBarThemeData(behavior: SnackBarBehavior.floating, backgroundColor: Color(0xFFFFFFFF), contentTextStyle: TextStyle(color: Color(0xFF002114)), closeIconColor: Color(0xFF002114)),
        pageTransitionsTheme: const PageTransitionsTheme(
          builders: {
            TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
            TargetPlatform.android: ZoomPageTransitionsBuilder(),
            TargetPlatform.linux: CupertinoPageTransitionsBuilder(),
            TargetPlatform.macOS: CupertinoPageTransitionsBuilder(),
            TargetPlatform.windows: CupertinoPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          },
        ),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: Color(0xFF599F8B),
          foregroundColor: Color(0xFFFFFFFF),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            backgroundColor: const Color(0xFF006c50),
            foregroundColor: const Color(0xFFFFFFFF),
            textStyle: const TextStyle(fontSize: 16),
            elevation: 6,
            shadowColor: const Color(0x44000000),
            minimumSize: const Size.fromHeight(48),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
            ),
            padding: const EdgeInsets.all(12),
          ),
        ),
        appBarTheme: const AppBarTheme(
          scrolledUnderElevation: 2,
          backgroundColor: Color(0xFFcce1dc), //Color(0xFFd0e4ff),
          actionsIconTheme: IconThemeData(color: Color(0xFF002116)),
          elevation: 2,
          shadowColor: Colors.black,
          systemOverlayStyle: SystemUiOverlayStyle.dark,
          //backgroundColor: FlavorConfig.primaryColorLight,
          titleTextStyle: TextStyle(
            color: Color(0xFF002116),
            fontSize: 20,
          ),
          iconTheme: IconThemeData(color: Color(0xFF002116), size: 25),
        ),
      );

  static ThemeData get darkMaterial => ThemeData(
        visualDensity: VisualDensity.standard,
        useMaterial3: true,
        fontFamilyFallback: const ['NotoEmoji'],
        fontFamily: 'Roboto',
        scaffoldBackgroundColor: const Color(0xFF1B1B1F),
        colorScheme: const ColorScheme(
          brightness: Brightness.dark,
          primary: Color(0xFFB5C4FF),
          onPrimary: Color(0xFF00287C),
          primaryContainer: Color(0xFF214290),
          onPrimaryContainer: Color(0xFFDBE1FF),
          secondary: Color(0xFFC1C5DD),
          onSecondary: Color(0xFF2B3042),
          secondaryContainer: Color(0xFF414659),
          onSecondaryContainer: Color(0xFFDEE1F9),
          tertiary: Color(0xFF357EC7),
          onTertiary: Color(0xFFFFFFFF),
          tertiaryContainer: Color(0xFF313338), //#313338
          onTertiaryContainer: Color(0xFFD0CED7), //#D0CED7
          error: Color(0xFFFFB4AB),
          errorContainer: Color(0xFF93000A),
          onError: Color(0xFF690005),
          onErrorContainer: Color(0xFFFFDAD6),
          background: Color(0xFF1E1F22), //#313338 #1B1B1F #1E1F22
          onBackground: Color(0xFFB5BAC1), //#B5BAC1 //#E4E2E6
          surface: Color(0xFF1B1B1F),
          onSurface: Color(0xFFE4E2E6),
          surfaceVariant: Color(0xFF383A40),
          onSurfaceVariant: Color(0xFFDBDEE1),
          outline: Color(0xFF8F909A),
          onInverseSurface: Color(0xFF1B1B1F),
          inverseSurface: Color(0xFFE4E2E6),
          inversePrimary: Color(0xFF0150E3),
          shadow: Color(0xFF000000),
          surfaceTint: Color(0xFFB5C4FF),
          outlineVariant: Color(0xFF45464F),
          scrim: Color(0xFF000000),
        ),
        secondaryHeaderColor: Colors.blueGrey.shade900,
        textTheme: PlatformInfos.isDesktop ? Typography.material2021().white.merge(fallbackTextTheme) : null,
        pageTransitionsTheme: const PageTransitionsTheme(
          builders: {
            TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
            TargetPlatform.android: ZoomPageTransitionsBuilder(),
            TargetPlatform.linux: CupertinoPageTransitionsBuilder(),
            TargetPlatform.macOS: CupertinoPageTransitionsBuilder(),
            TargetPlatform.windows: CupertinoPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          },
        ),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: Color(0xFF214290),
          foregroundColor: Color(0xFFDBE1FF),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            backgroundColor: Color(0xFF0150E3),
            foregroundColor: Color(0xFFFFFFFF),
            minimumSize: const Size.fromHeight(48),
            textStyle: const TextStyle(fontSize: 16),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
            ),
            padding: const EdgeInsets.all(12),
          ),
        ),
        snackBarTheme:
            const SnackBarThemeData(behavior: SnackBarBehavior.floating, backgroundColor: Color(0xFF313338), contentTextStyle: TextStyle(color: Color(0xFFD0CED7)), closeIconColor: Color(0xFFD0CED7)),
        appBarTheme: const AppBarTheme(
          scrolledUnderElevation: 0,
          /*actionsIconTheme: const IconThemeData(color: Color(0xff101a26)),*/
          elevation: 0,
          systemOverlayStyle: SystemUiOverlayStyle.light,
          //backgroundColor: const Color(0xff101a26),
          titleTextStyle: TextStyle(
            /*color: Colors.grey.shade300,*/
            fontSize: 20,
          ),
          iconTheme: IconThemeData(/*color: Colors.grey.shade300*/ size: 25),
        ),
        cupertinoOverrideTheme: const CupertinoThemeData(
          textTheme: CupertinoTextThemeData(),
        ),
      );

  static ThemeData get darkMaterialPublic => ThemeData(
        visualDensity: VisualDensity.standard,
        useMaterial3: true,
        scaffoldBackgroundColor: const Color(0xFF1B1B1F),
        colorScheme: const ColorScheme(
          brightness: Brightness.dark,
          primary: Color(0xFF4ECCA3),
          onPrimary: Color(0xFF002116),
          primaryContainer: Color(0xFF006c50),
          onPrimaryContainer: Color(0xFFffffff),
          secondary: Color(0xFFC1C5DD),
          onSecondary: Color(0xFF2B3042),
          secondaryContainer: Color(0xFF414659),
          onSecondaryContainer: Color(0xFFDEE1F9),
          tertiary: Color(0xFFB3C5FF),
          onTertiary: Color(0xFF002A76),
          tertiaryContainer: Color(0xFF45464F),
          onTertiaryContainer: Color(0xFFC6C6D0),
          error: Color(0xFFFFB4AB),
          errorContainer: Color(0xFF93000A),
          onError: Color(0xFF690005),
          onErrorContainer: Color(0xFFFFDAD6),
          background: Color(0xFF1B1B1F),
          onBackground: Color(0xFFE4E2E6),
          surface: Color(0xFF1B1B1F),
          onSurface: Color(0xFFE4E2E6),
          surfaceVariant: Color(0xFF45464F),
          onSurfaceVariant: Color(0xFFC6C6D0),
          outline: Color(0xFF8F909A),
          onInverseSurface: Color(0xFF1B1B1F),
          inverseSurface: Color(0xFFE4E2E6),
          inversePrimary: Color(0xFF0150E3),
          shadow: Color(0xFF000000),
          surfaceTint: Color(0xFF4ECCA3),
          outlineVariant: Color(0xFF45464F),
          scrim: Color(0xFF000000),
        ),
        dialogTheme: const DialogTheme(
            backgroundColor: Color(0xFF45464F),
            contentTextStyle: TextStyle(color: Color(0xFFC6C6D0)),
            iconColor: Color(0xFFC6C6D0),
            titleTextStyle: TextStyle(color: Color(0xFFC6C6D0), fontWeight: FontWeight.bold)),
        secondaryHeaderColor: Colors.blueGrey.shade900,
        textTheme: PlatformInfos.isDesktop ? Typography.material2021().white.merge(fallbackTextTheme) : null,
        pageTransitionsTheme: const PageTransitionsTheme(
          builders: {
            TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
            TargetPlatform.android: ZoomPageTransitionsBuilder(),
            TargetPlatform.linux: CupertinoPageTransitionsBuilder(),
            TargetPlatform.macOS: CupertinoPageTransitionsBuilder(),
            TargetPlatform.windows: CupertinoPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          },
        ),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: Color(0xFF006c50),
          foregroundColor: Color(0xFFFFFFFF),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            backgroundColor: Color(0xFF0150E3),
            foregroundColor: Color(0xFFFFFFFF),
            minimumSize: const Size.fromHeight(48),
            textStyle: const TextStyle(fontSize: 16),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(FlavorConfig.borderRadius),
            ),
            padding: const EdgeInsets.all(12),
          ),
        ),
        snackBarTheme:
            const SnackBarThemeData(behavior: SnackBarBehavior.floating, backgroundColor: Color(0xFF45464F), contentTextStyle: TextStyle(color: Color(0xFFC6C6D0)), closeIconColor: Color(0xFFC6C6D0)),
        appBarTheme: const AppBarTheme(
          scrolledUnderElevation: 0,
          /*actionsIconTheme: const IconThemeData(color: Color(0xff101a26)),*/
          elevation: 0,
          systemOverlayStyle: SystemUiOverlayStyle.light,
          //backgroundColor: const Color(0xff101a26),
          titleTextStyle: TextStyle(
            /*color: Colors.grey.shade300,*/
            fontSize: 20,
          ),
          iconTheme: IconThemeData(/*color: Colors.grey.shade300*/ size: 25),
        ),
        cupertinoOverrideTheme: const CupertinoThemeData(
          textTheme: CupertinoTextThemeData(),
        ),
      );

  static Color blackWhiteColor(BuildContext context) => Theme.of(context).brightness == Brightness.light ? Colors.white : Colors.black;
  static Color whiteBlackColor(BuildContext context) => Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.white;
  static Color greyBlackColor(BuildContext context) => Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.grey.shade300;

  static Color darken(Color color, [double amount = .1]) {
    assert(amount >= 0 && amount <= 1);

    final hsl = HSLColor.fromColor(color);
    final hslDark = hsl.withLightness((hsl.lightness - amount).clamp(0.0, 1.0));

    return hslDark.toColor();
  }

  static bool isDarkMode(context) {
    return Theme.of(context).brightness == Brightness.dark;
  }

  static Color lighten(Color color, [double amount = .1]) {
    assert(amount >= 0 && amount <= 1);

    final hsl = HSLColor.fromColor(color);
    final hslLight = hsl.withLightness((hsl.lightness + amount).clamp(0.0, 1.0));

    return hslLight.toColor();
  }
}
