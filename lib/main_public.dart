import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';
import 'package:matrix/matrix.dart';
import 'package:pageMe/pages/subscriptions/subscriptions_cubit.dart';
import 'package:pageMe/utils/client_manager.dart';
import 'package:pageMe/utils/platform_infos.dart';
import 'package:pageMe/utils/push_factorys/push_ultity.dart';
import 'package:pageMe/utils/sentry_controller.dart';
import 'package:pageMe/utils/window_manager.dart';
import 'package:pageMe/widgets/lock_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_html/html.dart' as html;
import 'config/flavor_config.dart';
import 'firebase_options.dart';
import 'pageme_app.dart';

void main() async {
  await runZonedGuarded(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      FlavorConfig.initialize(flavor: Flavor.public);
      if (PlatformInfos.isDesktop) {
        await WindowManagerWidget.initWindow();
      }
      if (PlatformInfos.isMacOS) {
        Logs().i("Setting up MacOS Notifications");
        await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
      }

      FlutterError.onError = (FlutterErrorDetails details) => Zone.current.handleUncaughtError(
            details.exception,
            details.stack ?? StackTrace.current,
          );


      final store = await SharedPreferences.getInstance();
      final clients = await ClientManager.getClients(store: store);
      Logs().level = kReleaseMode ? Level.warning : Level.verbose;

      if (PlatformInfos.isMobile || PlatformInfos.isMacOS) {
        Logs().i(clients.first.clientName);
        PushSetup().pushSetupClientOnly(clients.first);
      }

      final queryParameters = <String, String>{};
      if (kIsWeb) {
        queryParameters.addAll(Uri.parse(html.window.location.href).queryParameters);
      }

      runApp(BlocProvider(
              create: (context) => SubscriptionsCubit(),
              child: Builder(builder: (context) {
                if (PlatformInfos.canSubscribe){
                  BlocProvider.of<SubscriptionsCubit>(context).initialize();
                }
                return PageMeApp(
                  store: store,
                  clients: clients,
                  queryParameters: queryParameters,
                );
              }),
            ));
    },
    SentryController.captureException,
  );
}
